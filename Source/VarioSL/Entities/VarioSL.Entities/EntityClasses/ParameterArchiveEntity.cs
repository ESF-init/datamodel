﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ParameterArchive'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ParameterArchiveEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection	_parameterArchiveReleases;
		private bool	_alwaysFetchParameterArchiveReleases, _alreadyFetchedParameterArchiveReleases;
		private VarioSL.Entities.CollectionClasses.ParameterValueCollection	_parameterValues;
		private bool	_alwaysFetchParameterValues, _alreadyFetchedParameterValues;
		private VarioSL.Entities.CollectionClasses.UnitCollectionCollection	_unitCollections;
		private bool	_alwaysFetchUnitCollections, _alreadyFetchedUnitCollections;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ParameterArchiveReleases</summary>
			public static readonly string ParameterArchiveReleases = "ParameterArchiveReleases";
			/// <summary>Member name ParameterValues</summary>
			public static readonly string ParameterValues = "ParameterValues";
			/// <summary>Member name UnitCollections</summary>
			public static readonly string UnitCollections = "UnitCollections";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ParameterArchiveEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ParameterArchiveEntity() :base("ParameterArchiveEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		public ParameterArchiveEntity(System.Int64 parameterArchiveID):base("ParameterArchiveEntity")
		{
			InitClassFetch(parameterArchiveID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ParameterArchiveEntity(System.Int64 parameterArchiveID, IPrefetchPath prefetchPathToUse):base("ParameterArchiveEntity")
		{
			InitClassFetch(parameterArchiveID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="validator">The custom validator object for this ParameterArchiveEntity</param>
		public ParameterArchiveEntity(System.Int64 parameterArchiveID, IValidator validator):base("ParameterArchiveEntity")
		{
			InitClassFetch(parameterArchiveID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ParameterArchiveEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_parameterArchiveReleases = (VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection)info.GetValue("_parameterArchiveReleases", typeof(VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection));
			_alwaysFetchParameterArchiveReleases = info.GetBoolean("_alwaysFetchParameterArchiveReleases");
			_alreadyFetchedParameterArchiveReleases = info.GetBoolean("_alreadyFetchedParameterArchiveReleases");

			_parameterValues = (VarioSL.Entities.CollectionClasses.ParameterValueCollection)info.GetValue("_parameterValues", typeof(VarioSL.Entities.CollectionClasses.ParameterValueCollection));
			_alwaysFetchParameterValues = info.GetBoolean("_alwaysFetchParameterValues");
			_alreadyFetchedParameterValues = info.GetBoolean("_alreadyFetchedParameterValues");

			_unitCollections = (VarioSL.Entities.CollectionClasses.UnitCollectionCollection)info.GetValue("_unitCollections", typeof(VarioSL.Entities.CollectionClasses.UnitCollectionCollection));
			_alwaysFetchUnitCollections = info.GetBoolean("_alwaysFetchUnitCollections");
			_alreadyFetchedUnitCollections = info.GetBoolean("_alreadyFetchedUnitCollections");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ParameterArchiveFieldIndex)fieldIndex)
			{
				case ParameterArchiveFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParameterArchiveReleases = (_parameterArchiveReleases.Count > 0);
			_alreadyFetchedParameterValues = (_parameterValues.Count > 0);
			_alreadyFetchedUnitCollections = (_unitCollections.Count > 0);
			_alreadyFetchedClient = (_client != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "ParameterArchiveReleases":
					toReturn.Add(Relations.ParameterArchiveReleaseEntityUsingParameterArchiveID);
					break;
				case "ParameterValues":
					toReturn.Add(Relations.ParameterValueEntityUsingParameterArchiveID);
					break;
				case "UnitCollections":
					toReturn.Add(Relations.UnitCollectionEntityUsingParameterArchiveID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_parameterArchiveReleases", (!this.MarkedForDeletion?_parameterArchiveReleases:null));
			info.AddValue("_alwaysFetchParameterArchiveReleases", _alwaysFetchParameterArchiveReleases);
			info.AddValue("_alreadyFetchedParameterArchiveReleases", _alreadyFetchedParameterArchiveReleases);
			info.AddValue("_parameterValues", (!this.MarkedForDeletion?_parameterValues:null));
			info.AddValue("_alwaysFetchParameterValues", _alwaysFetchParameterValues);
			info.AddValue("_alreadyFetchedParameterValues", _alreadyFetchedParameterValues);
			info.AddValue("_unitCollections", (!this.MarkedForDeletion?_unitCollections:null));
			info.AddValue("_alwaysFetchUnitCollections", _alwaysFetchUnitCollections);
			info.AddValue("_alreadyFetchedUnitCollections", _alreadyFetchedUnitCollections);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "ParameterArchiveReleases":
					_alreadyFetchedParameterArchiveReleases = true;
					if(entity!=null)
					{
						this.ParameterArchiveReleases.Add((ParameterArchiveReleaseEntity)entity);
					}
					break;
				case "ParameterValues":
					_alreadyFetchedParameterValues = true;
					if(entity!=null)
					{
						this.ParameterValues.Add((ParameterValueEntity)entity);
					}
					break;
				case "UnitCollections":
					_alreadyFetchedUnitCollections = true;
					if(entity!=null)
					{
						this.UnitCollections.Add((UnitCollectionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "ParameterArchiveReleases":
					_parameterArchiveReleases.Add((ParameterArchiveReleaseEntity)relatedEntity);
					break;
				case "ParameterValues":
					_parameterValues.Add((ParameterValueEntity)relatedEntity);
					break;
				case "UnitCollections":
					_unitCollections.Add((UnitCollectionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "ParameterArchiveReleases":
					this.PerformRelatedEntityRemoval(_parameterArchiveReleases, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterValues":
					this.PerformRelatedEntityRemoval(_parameterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UnitCollections":
					this.PerformRelatedEntityRemoval(_unitCollections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_parameterArchiveReleases);
			toReturn.Add(_parameterValues);
			toReturn.Add(_unitCollections);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterArchiveID)
		{
			return FetchUsingPK(parameterArchiveID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterArchiveID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(parameterArchiveID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterArchiveID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(parameterArchiveID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 parameterArchiveID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(parameterArchiveID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParameterArchiveID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ParameterArchiveRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch)
		{
			return GetMultiParameterArchiveReleases(forceFetch, _parameterArchiveReleases.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterArchiveReleases(forceFetch, _parameterArchiveReleases.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterArchiveReleases(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterArchiveReleases || forceFetch || _alwaysFetchParameterArchiveReleases) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterArchiveReleases);
				_parameterArchiveReleases.SuppressClearInGetMulti=!forceFetch;
				_parameterArchiveReleases.EntityFactoryToUse = entityFactoryToUse;
				_parameterArchiveReleases.GetMultiManyToOne(null, this, filter);
				_parameterArchiveReleases.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterArchiveReleases = true;
			}
			return _parameterArchiveReleases;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterArchiveReleases'. These settings will be taken into account
		/// when the property ParameterArchiveReleases is requested or GetMultiParameterArchiveReleases is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterArchiveReleases(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterArchiveReleases.SortClauses=sortClauses;
			_parameterArchiveReleases.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterValues || forceFetch || _alwaysFetchParameterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterValues);
				_parameterValues.SuppressClearInGetMulti=!forceFetch;
				_parameterValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterValues.GetMultiManyToOne(null, null, null, this, null, filter);
				_parameterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterValues = true;
			}
			return _parameterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterValues'. These settings will be taken into account
		/// when the property ParameterValues is requested or GetMultiParameterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterValues.SortClauses=sortClauses;
			_parameterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch)
		{
			return GetMultiUnitCollections(forceFetch, _unitCollections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUnitCollections(forceFetch, _unitCollections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUnitCollections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUnitCollections || forceFetch || _alwaysFetchUnitCollections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_unitCollections);
				_unitCollections.SuppressClearInGetMulti=!forceFetch;
				_unitCollections.EntityFactoryToUse = entityFactoryToUse;
				_unitCollections.GetMultiManyToOne(null, this, filter);
				_unitCollections.SuppressClearInGetMulti=false;
				_alreadyFetchedUnitCollections = true;
			}
			return _unitCollections;
		}

		/// <summary> Sets the collection parameters for the collection for 'UnitCollections'. These settings will be taken into account
		/// when the property UnitCollections is requested or GetMultiUnitCollections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnitCollections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_unitCollections.SortClauses=sortClauses;
			_unitCollections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("ParameterArchiveReleases", _parameterArchiveReleases);
			toReturn.Add("ParameterValues", _parameterValues);
			toReturn.Add("UnitCollections", _unitCollections);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="validator">The validator object for this ParameterArchiveEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 parameterArchiveID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(parameterArchiveID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_parameterArchiveReleases = new VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection();
			_parameterArchiveReleases.SetContainingEntityInfo(this, "ParameterArchive");

			_parameterValues = new VarioSL.Entities.CollectionClasses.ParameterValueCollection();
			_parameterValues.SetContainingEntityInfo(this, "ParameterArchive");

			_unitCollections = new VarioSL.Entities.CollectionClasses.UnitCollectionCollection();
			_unitCollections.SetContainingEntityInfo(this, "ParameterArchive");
			_clientReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterArchiveID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticParameterArchiveRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "ParameterArchives", resetFKFields, new int[] { (int)ParameterArchiveFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticParameterArchiveRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="parameterArchiveID">PK value for ParameterArchive which data should be fetched into this ParameterArchive object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 parameterArchiveID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ParameterArchiveFieldIndex.ParameterArchiveID].ForcedCurrentValueWrite(parameterArchiveID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateParameterArchiveDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ParameterArchiveEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ParameterArchiveRelations Relations
		{
			get	{ return new ParameterArchiveRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterArchiveRelease' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterArchiveReleases
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection(), (IEntityRelation)GetRelationsForField("ParameterArchiveReleases")[0], (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, (int)VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity, 0, null, null, null, "ParameterArchiveReleases", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterValueCollection(), (IEntityRelation)GetRelationsForField("ParameterValues")[0], (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, (int)VarioSL.Entities.EntityType.ParameterValueEntity, 0, null, null, null, "ParameterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionCollection(), (IEntityRelation)GetRelationsForField("UnitCollections")[0], (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, (int)VarioSL.Entities.EntityType.UnitCollectionEntity, 0, null, null, null, "UnitCollections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ParameterArchiveEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ClientID property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)ParameterArchiveFieldIndex.ClientID, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.ClientID, value, true); }
		}

		/// <summary> The Description property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ParameterArchiveFieldIndex.Description, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.Description, value, true); }
		}

		/// <summary> The LastModified property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ParameterArchiveFieldIndex.LastModified, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ParameterArchiveFieldIndex.LastUser, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ParameterArchiveFieldIndex.Name, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.Name, value, true); }
		}

		/// <summary> The ParameterArchiveID property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."PARAMETERARCHIVEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ParameterArchiveID
		{
			get { return (System.Int64)GetValue((int)ParameterArchiveFieldIndex.ParameterArchiveID, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.ParameterArchiveID, value, true); }
		}

		/// <summary> The ReleaseCounter property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."RELEASECOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ReleaseCounter
		{
			get { return (System.Int32)GetValue((int)ParameterArchiveFieldIndex.ReleaseCounter, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.ReleaseCounter, value, true); }
		}

		/// <summary> The State property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ArchiveState State
		{
			get { return (VarioSL.Entities.Enumerations.ArchiveState)GetValue((int)ParameterArchiveFieldIndex.State, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.State, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ParameterArchiveFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)ParameterArchiveFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The Version property of the Entity ParameterArchive<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARAMETERARCHIVE"."VERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)ParameterArchiveFieldIndex.Version, true); }
			set	{ SetValue((int)ParameterArchiveFieldIndex.Version, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterArchiveReleases()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection ParameterArchiveReleases
		{
			get	{ return GetMultiParameterArchiveReleases(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterArchiveReleases. When set to true, ParameterArchiveReleases is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterArchiveReleases is accessed. You can always execute/ a forced fetch by calling GetMultiParameterArchiveReleases(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterArchiveReleases
		{
			get	{ return _alwaysFetchParameterArchiveReleases; }
			set	{ _alwaysFetchParameterArchiveReleases = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterArchiveReleases already has been fetched. Setting this property to false when ParameterArchiveReleases has been fetched
		/// will clear the ParameterArchiveReleases collection well. Setting this property to true while ParameterArchiveReleases hasn't been fetched disables lazy loading for ParameterArchiveReleases</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterArchiveReleases
		{
			get { return _alreadyFetchedParameterArchiveReleases;}
			set 
			{
				if(_alreadyFetchedParameterArchiveReleases && !value && (_parameterArchiveReleases != null))
				{
					_parameterArchiveReleases.Clear();
				}
				_alreadyFetchedParameterArchiveReleases = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection ParameterValues
		{
			get	{ return GetMultiParameterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterValues. When set to true, ParameterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterValues
		{
			get	{ return _alwaysFetchParameterValues; }
			set	{ _alwaysFetchParameterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterValues already has been fetched. Setting this property to false when ParameterValues has been fetched
		/// will clear the ParameterValues collection well. Setting this property to true while ParameterValues hasn't been fetched disables lazy loading for ParameterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterValues
		{
			get { return _alreadyFetchedParameterValues;}
			set 
			{
				if(_alreadyFetchedParameterValues && !value && (_parameterValues != null))
				{
					_parameterValues.Clear();
				}
				_alreadyFetchedParameterValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnitCollections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionCollection UnitCollections
		{
			get	{ return GetMultiUnitCollections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollections. When set to true, UnitCollections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollections is accessed. You can always execute/ a forced fetch by calling GetMultiUnitCollections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollections
		{
			get	{ return _alwaysFetchUnitCollections; }
			set	{ _alwaysFetchUnitCollections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollections already has been fetched. Setting this property to false when UnitCollections has been fetched
		/// will clear the UnitCollections collection well. Setting this property to true while UnitCollections hasn't been fetched disables lazy loading for UnitCollections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollections
		{
			get { return _alreadyFetchedUnitCollections;}
			set 
			{
				if(_alreadyFetchedUnitCollections && !value && (_unitCollections != null))
				{
					_unitCollections.Clear();
				}
				_alreadyFetchedUnitCollections = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParameterArchives", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ParameterArchiveEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
