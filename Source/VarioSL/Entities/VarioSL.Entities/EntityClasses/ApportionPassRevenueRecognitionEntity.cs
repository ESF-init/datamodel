﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ApportionPassRevenueRecognition'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ApportionPassRevenueRecognitionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ApportionPassRevenueRecognitionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ApportionPassRevenueRecognitionEntity() :base("ApportionPassRevenueRecognitionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		public ApportionPassRevenueRecognitionEntity(System.Int64 tapRevenueRecognitionID):base("ApportionPassRevenueRecognitionEntity")
		{
			InitClassFetch(tapRevenueRecognitionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ApportionPassRevenueRecognitionEntity(System.Int64 tapRevenueRecognitionID, IPrefetchPath prefetchPathToUse):base("ApportionPassRevenueRecognitionEntity")
		{
			InitClassFetch(tapRevenueRecognitionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="validator">The custom validator object for this ApportionPassRevenueRecognitionEntity</param>
		public ApportionPassRevenueRecognitionEntity(System.Int64 tapRevenueRecognitionID, IValidator validator):base("ApportionPassRevenueRecognitionEntity")
		{
			InitClassFetch(tapRevenueRecognitionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApportionPassRevenueRecognitionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tapRevenueRecognitionID)
		{
			return FetchUsingPK(tapRevenueRecognitionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tapRevenueRecognitionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(tapRevenueRecognitionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tapRevenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(tapRevenueRecognitionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 tapRevenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(tapRevenueRecognitionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TapRevenueRecognitionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ApportionPassRevenueRecognitionRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="validator">The validator object for this ApportionPassRevenueRecognitionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 tapRevenueRecognitionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(tapRevenueRecognitionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassCloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassRevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassRevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassTransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassTransitAccountID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapAppliedPassTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapBaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapBoardingGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapCancellationReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapCancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapCloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapDeviceTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapOperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapOriginalSingleFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapPurseCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapRevenueRecognitionAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapRevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapRevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapTicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapTransactionjournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TapTransactionType", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="tapRevenueRecognitionID">PK value for ApportionPassRevenueRecognition which data should be fetched into this ApportionPassRevenueRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 tapRevenueRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueRecognitionID].ForcedCurrentValueWrite(tapRevenueRecognitionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateApportionPassRevenueRecognitionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ApportionPassRevenueRecognitionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ApportionPassRevenueRecognitionRelations Relations
		{
			get	{ return new ApportionPassRevenueRecognitionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The PassAmount property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSAMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PassAmount
		{
			get { return (System.Int64)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassAmount, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassAmount, value, true); }
		}

		/// <summary> The PassCloseoutPeriodID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSCLOSEOUTPERIODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PassCloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassCloseoutPeriodID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassCloseoutPeriodID, value, true); }
		}

		/// <summary> The PassRevenueRecognitionID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSREVENUERECOGNITIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PassRevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassRevenueRecognitionID, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassRevenueRecognitionID, value, true); }
		}

		/// <summary> The PassRevenueSettlementID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSREVENUESETTLEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PassRevenueSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassRevenueSettlementID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassRevenueSettlementID, value, true); }
		}

		/// <summary> The PassTicketID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTICKETID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PassTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTicketID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTicketID, value, true); }
		}

		/// <summary> The PassTicketInternalNumber property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PassTicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTicketInternalNumber, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTicketInternalNumber, value, true); }
		}

		/// <summary> The PassTransactionJournalID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTRANSACTIONJOURNALID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PassTransactionJournalID
		{
			get { return (System.Int64)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTransactionJournalID, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTransactionJournalID, value, true); }
		}

		/// <summary> The PassTransitAccountID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTRANSITACCOUNTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PassTransitAccountID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTransitAccountID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassTransitAccountID, value, true); }
		}

		/// <summary> The PassValidFrom property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSVALIDFROM"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PassValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassValidFrom, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassValidFrom, value, true); }
		}

		/// <summary> The PassValidTo property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."PASSVALIDTO"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PassValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassValidTo, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.PassValidTo, value, true); }
		}

		/// <summary> The TapAppliedPassTicketID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPAPPLIEDPASSTICKETID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapAppliedPassTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapAppliedPassTicketID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapAppliedPassTicketID, value, true); }
		}

		/// <summary> The TapBaseFare property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPBASEFARE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapBaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapBaseFare, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapBaseFare, value, true); }
		}

		/// <summary> The TapBoardingGuid property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPBOARDINGGUID"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TapBoardingGuid
		{
			get { return (System.String)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapBoardingGuid, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapBoardingGuid, value, true); }
		}

		/// <summary> The TapCancellationReference property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCANCELLATIONREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapCancellationReference
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCancellationReference, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCancellationReference, value, true); }
		}

		/// <summary> The TapCancellationReferenceGuid property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCANCELLATIONREFERENCEGUID"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TapCancellationReferenceGuid
		{
			get { return (System.String)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCancellationReferenceGuid, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCancellationReferenceGuid, value, true); }
		}

		/// <summary> The TapCloseoutPeriodID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCLOSEOUTPERIODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapCloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCloseoutPeriodID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapCloseoutPeriodID, value, true); }
		}

		/// <summary> The TapDeviceTime property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPDEVICETIME"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TapDeviceTime
		{
			get { return (System.DateTime)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapDeviceTime, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapDeviceTime, value, true); }
		}

		/// <summary> The TapOperatorID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPOPERATORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TapOperatorID
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapOperatorID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapOperatorID, value, true); }
		}

		/// <summary> The TapOriginalSingleFare property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPORIGINALSINGLEFARE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TapOriginalSingleFare
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapOriginalSingleFare, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapOriginalSingleFare, value, true); }
		}

		/// <summary> The TapPurseCredit property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPPURSECREDIT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TapPurseCredit
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapPurseCredit, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapPurseCredit, value, true); }
		}

		/// <summary> The TapRevenueRecognitionAmount property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUERECOGNITIONAMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapRevenueRecognitionAmount
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueRecognitionAmount, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueRecognitionAmount, value, true); }
		}

		/// <summary> The TapRevenueRecognitionID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUERECOGNITIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, true, false</remarks>
		public virtual System.Int64 TapRevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueRecognitionID, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueRecognitionID, value, true); }
		}

		/// <summary> The TapRevenueSettlementID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUESETTLEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapRevenueSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueSettlementID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapRevenueSettlementID, value, true); }
		}

		/// <summary> The TapTicketID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTICKETID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTicketID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTicketID, value, true); }
		}

		/// <summary> The TapTicketNumber property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTICKETNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TapTicketNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTicketNumber, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTicketNumber, value, true); }
		}

		/// <summary> The TapTransactionjournalID property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTRANSACTIONJOURNALID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TapTransactionjournalID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTransactionjournalID, false); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTransactionjournalID, value, true); }
		}

		/// <summary> The TapTransactionType property of the Entity ApportionPassRevenueRecognition<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTRANSACTIONTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TapTransactionType
		{
			get { return (System.Int32)GetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTransactionType, true); }
			set	{ SetValue((int)ApportionPassRevenueRecognitionFieldIndex.TapTransactionType, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ApportionPassRevenueRecognitionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
