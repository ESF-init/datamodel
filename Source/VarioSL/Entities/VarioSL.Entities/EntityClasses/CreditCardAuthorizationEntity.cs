﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CreditCardAuthorization'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CreditCardAuthorizationEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection	_creditCardAuthorizationLogs;
		private bool	_alwaysFetchCreditCardAuthorizationLogs, _alreadyFetchedCreditCardAuthorizationLogs;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private CreditCardTerminalEntity _creditCardTerminal;
		private bool	_alwaysFetchCreditCardTerminal, _alreadyFetchedCreditCardTerminal, _creditCardTerminalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name CreditCardTerminal</summary>
			public static readonly string CreditCardTerminal = "CreditCardTerminal";
			/// <summary>Member name CreditCardAuthorizationLogs</summary>
			public static readonly string CreditCardAuthorizationLogs = "CreditCardAuthorizationLogs";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CreditCardAuthorizationEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CreditCardAuthorizationEntity() :base("CreditCardAuthorizationEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		public CreditCardAuthorizationEntity(System.Int64 creditCardAuthorizationID):base("CreditCardAuthorizationEntity")
		{
			InitClassFetch(creditCardAuthorizationID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CreditCardAuthorizationEntity(System.Int64 creditCardAuthorizationID, IPrefetchPath prefetchPathToUse):base("CreditCardAuthorizationEntity")
		{
			InitClassFetch(creditCardAuthorizationID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="validator">The custom validator object for this CreditCardAuthorizationEntity</param>
		public CreditCardAuthorizationEntity(System.Int64 creditCardAuthorizationID, IValidator validator):base("CreditCardAuthorizationEntity")
		{
			InitClassFetch(creditCardAuthorizationID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CreditCardAuthorizationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_creditCardAuthorizationLogs = (VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection)info.GetValue("_creditCardAuthorizationLogs", typeof(VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection));
			_alwaysFetchCreditCardAuthorizationLogs = info.GetBoolean("_alwaysFetchCreditCardAuthorizationLogs");
			_alreadyFetchedCreditCardAuthorizationLogs = info.GetBoolean("_alreadyFetchedCreditCardAuthorizationLogs");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");
			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_creditCardTerminal = (CreditCardTerminalEntity)info.GetValue("_creditCardTerminal", typeof(CreditCardTerminalEntity));
			if(_creditCardTerminal!=null)
			{
				_creditCardTerminal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_creditCardTerminalReturnsNewIfNotFound = info.GetBoolean("_creditCardTerminalReturnsNewIfNotFound");
			_alwaysFetchCreditCardTerminal = info.GetBoolean("_alwaysFetchCreditCardTerminal");
			_alreadyFetchedCreditCardTerminal = info.GetBoolean("_alreadyFetchedCreditCardTerminal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CreditCardAuthorizationFieldIndex)fieldIndex)
			{
				case CreditCardAuthorizationFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case CreditCardAuthorizationFieldIndex.CreditCardTerminalID:
					DesetupSyncCreditCardTerminal(true, false);
					_alreadyFetchedCreditCardTerminal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCreditCardAuthorizationLogs = (_creditCardAuthorizationLogs.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedCreditCardTerminal = (_creditCardTerminal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "CreditCardTerminal":
					toReturn.Add(Relations.CreditCardTerminalEntityUsingCreditCardTerminalID);
					break;
				case "CreditCardAuthorizationLogs":
					toReturn.Add(Relations.CreditCardAuthorizationLogEntityUsingCreditCardAuthorizationID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingCreditCardAuthorizationID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingCreditCardAuthorizationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_creditCardAuthorizationLogs", (!this.MarkedForDeletion?_creditCardAuthorizationLogs:null));
			info.AddValue("_alwaysFetchCreditCardAuthorizationLogs", _alwaysFetchCreditCardAuthorizationLogs);
			info.AddValue("_alreadyFetchedCreditCardAuthorizationLogs", _alreadyFetchedCreditCardAuthorizationLogs);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_creditCardTerminal", (!this.MarkedForDeletion?_creditCardTerminal:null));
			info.AddValue("_creditCardTerminalReturnsNewIfNotFound", _creditCardTerminalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCreditCardTerminal", _alwaysFetchCreditCardTerminal);
			info.AddValue("_alreadyFetchedCreditCardTerminal", _alreadyFetchedCreditCardTerminal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "CreditCardTerminal":
					_alreadyFetchedCreditCardTerminal = true;
					this.CreditCardTerminal = (CreditCardTerminalEntity)entity;
					break;
				case "CreditCardAuthorizationLogs":
					_alreadyFetchedCreditCardAuthorizationLogs = true;
					if(entity!=null)
					{
						this.CreditCardAuthorizationLogs.Add((CreditCardAuthorizationLogEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "CreditCardTerminal":
					SetupSyncCreditCardTerminal(relatedEntity);
					break;
				case "CreditCardAuthorizationLogs":
					_creditCardAuthorizationLogs.Add((CreditCardAuthorizationLogEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "CreditCardTerminal":
					DesetupSyncCreditCardTerminal(false, true);
					break;
				case "CreditCardAuthorizationLogs":
					this.PerformRelatedEntityRemoval(_creditCardAuthorizationLogs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_creditCardTerminal!=null)
			{
				toReturn.Add(_creditCardTerminal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_creditCardAuthorizationLogs);
			toReturn.Add(_sales);
			toReturn.Add(_transactionJournals);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationID)
		{
			return FetchUsingPK(creditCardAuthorizationID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(creditCardAuthorizationID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(creditCardAuthorizationID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(creditCardAuthorizationID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CreditCardAuthorizationID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CreditCardAuthorizationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationLogEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection GetMultiCreditCardAuthorizationLogs(bool forceFetch)
		{
			return GetMultiCreditCardAuthorizationLogs(forceFetch, _creditCardAuthorizationLogs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CreditCardAuthorizationLogEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection GetMultiCreditCardAuthorizationLogs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCreditCardAuthorizationLogs(forceFetch, _creditCardAuthorizationLogs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection GetMultiCreditCardAuthorizationLogs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCreditCardAuthorizationLogs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationLogEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection GetMultiCreditCardAuthorizationLogs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCreditCardAuthorizationLogs || forceFetch || _alwaysFetchCreditCardAuthorizationLogs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_creditCardAuthorizationLogs);
				_creditCardAuthorizationLogs.SuppressClearInGetMulti=!forceFetch;
				_creditCardAuthorizationLogs.EntityFactoryToUse = entityFactoryToUse;
				_creditCardAuthorizationLogs.GetMultiManyToOne(this, filter);
				_creditCardAuthorizationLogs.SuppressClearInGetMulti=false;
				_alreadyFetchedCreditCardAuthorizationLogs = true;
			}
			return _creditCardAuthorizationLogs;
		}

		/// <summary> Sets the collection parameters for the collection for 'CreditCardAuthorizationLogs'. These settings will be taken into account
		/// when the property CreditCardAuthorizationLogs is requested or GetMultiCreditCardAuthorizationLogs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCreditCardAuthorizationLogs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_creditCardAuthorizationLogs.SortClauses=sortClauses;
			_creditCardAuthorizationLogs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID);
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'CreditCardTerminalEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CreditCardTerminalEntity' which is related to this entity.</returns>
		public CreditCardTerminalEntity GetSingleCreditCardTerminal()
		{
			return GetSingleCreditCardTerminal(false);
		}

		/// <summary> Retrieves the related entity of type 'CreditCardTerminalEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CreditCardTerminalEntity' which is related to this entity.</returns>
		public virtual CreditCardTerminalEntity GetSingleCreditCardTerminal(bool forceFetch)
		{
			if( ( !_alreadyFetchedCreditCardTerminal || forceFetch || _alwaysFetchCreditCardTerminal) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CreditCardTerminalEntityUsingCreditCardTerminalID);
				CreditCardTerminalEntity newEntity = new CreditCardTerminalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CreditCardTerminalID);
				}
				if(fetchResult)
				{
					newEntity = (CreditCardTerminalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_creditCardTerminalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CreditCardTerminal = newEntity;
				_alreadyFetchedCreditCardTerminal = fetchResult;
			}
			return _creditCardTerminal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Card", _card);
			toReturn.Add("CreditCardTerminal", _creditCardTerminal);
			toReturn.Add("CreditCardAuthorizationLogs", _creditCardAuthorizationLogs);
			toReturn.Add("Sales", _sales);
			toReturn.Add("TransactionJournals", _transactionJournals);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="validator">The validator object for this CreditCardAuthorizationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 creditCardAuthorizationID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(creditCardAuthorizationID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_creditCardAuthorizationLogs = new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection();
			_creditCardAuthorizationLogs.SetContainingEntityInfo(this, "CreditCardAuthorization");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "CreditCardAuthorization");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "CreditCardAuthorization");
			_cardReturnsNewIfNotFound = false;
			_creditCardTerminalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccountState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ACI", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmountRefunded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AuthorizationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BankNetData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardLevelResult", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardSequenceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CertificationData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletionDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompletionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardTerminalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTypeIndicator", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EMVData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EncryptedData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalAuthID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalCardHash", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalCardReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsArqcRequested", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastEmvDataUpdate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocalDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResponseCode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RetryCountdown", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RiskFlags", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Status", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SystemTraceAuditNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Token", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionDateTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "CreditCardAuthorizations", resetFKFields, new int[] { (int)CreditCardAuthorizationFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _creditCardTerminal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCreditCardTerminal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _creditCardTerminal, new PropertyChangedEventHandler( OnCreditCardTerminalPropertyChanged ), "CreditCardTerminal", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationRelations.CreditCardTerminalEntityUsingCreditCardTerminalIDStatic, true, signalRelatedEntity, "CreditCardAuthorizations", resetFKFields, new int[] { (int)CreditCardAuthorizationFieldIndex.CreditCardTerminalID } );		
			_creditCardTerminal = null;
		}
		
		/// <summary> setups the sync logic for member _creditCardTerminal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCreditCardTerminal(IEntityCore relatedEntity)
		{
			if(_creditCardTerminal!=relatedEntity)
			{		
				DesetupSyncCreditCardTerminal(true, true);
				_creditCardTerminal = (CreditCardTerminalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _creditCardTerminal, new PropertyChangedEventHandler( OnCreditCardTerminalPropertyChanged ), "CreditCardTerminal", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationRelations.CreditCardTerminalEntityUsingCreditCardTerminalIDStatic, true, ref _alreadyFetchedCreditCardTerminal, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCreditCardTerminalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="creditCardAuthorizationID">PK value for CreditCardAuthorization which data should be fetched into this CreditCardAuthorization object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 creditCardAuthorizationID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CreditCardAuthorizationFieldIndex.CreditCardAuthorizationID].ForcedCurrentValueWrite(creditCardAuthorizationID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCreditCardAuthorizationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CreditCardAuthorizationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CreditCardAuthorizationRelations Relations
		{
			get	{ return new CreditCardAuthorizationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorizationLog' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorizationLogs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorizationLogs")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity, 0, null, null, null, "CreditCardAuthorizationLogs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardTerminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardTerminal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardTerminalCollection(), (IEntityRelation)GetRelationsForField("CreditCardTerminal")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, (int)VarioSL.Entities.EntityType.CreditCardTerminalEntity, 0, null, null, null, "CreditCardTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccountState property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."ACCOUNTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CreditCardAccountState> AccountState
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CreditCardAccountState>)GetValue((int)CreditCardAuthorizationFieldIndex.AccountState, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.AccountState, value, true); }
		}

		/// <summary> The ACI property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."ACI"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ACI
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.ACI, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.ACI, value, true); }
		}

		/// <summary> The Amount property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Amount
		{
			get { return (System.Int32)GetValue((int)CreditCardAuthorizationFieldIndex.Amount, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.Amount, value, true); }
		}

		/// <summary> The AmountRefunded property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."AMOUNTREFUNDED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AmountRefunded
		{
			get { return (Nullable<System.Int32>)GetValue((int)CreditCardAuthorizationFieldIndex.AmountRefunded, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.AmountRefunded, value, true); }
		}

		/// <summary> The AuthorizationType property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."AUTHORIZATIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CreditCardAuthorizationType> AuthorizationType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CreditCardAuthorizationType>)GetValue((int)CreditCardAuthorizationFieldIndex.AuthorizationType, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.AuthorizationType, value, true); }
		}

		/// <summary> The BankNetData property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."BANKNETDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 13<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BankNetData
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.BankNetData, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.BankNetData, value, true); }
		}

		/// <summary> The CardID property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CardID
		{
			get { return (System.Int64)GetValue((int)CreditCardAuthorizationFieldIndex.CardID, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CardID, value, true); }
		}

		/// <summary> The CardLevelResult property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CARDLEVELRESULT"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CardLevelResult
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.CardLevelResult, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CardLevelResult, value, true); }
		}

		/// <summary> The CardSequenceNumber property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CARDSEQUENCENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CardSequenceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)CreditCardAuthorizationFieldIndex.CardSequenceNumber, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CardSequenceNumber, value, true); }
		}

		/// <summary> The CardType property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CARDTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CardType
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.CardType, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CardType, value, true); }
		}

		/// <summary> The CertificationData property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CERTIFICATIONDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CertificationData
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.CertificationData, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CertificationData, value, true); }
		}

		/// <summary> The CompletionDateTime property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."COMPLETIONDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime CompletionDateTime
		{
			get { return (System.DateTime)GetValue((int)CreditCardAuthorizationFieldIndex.CompletionDateTime, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CompletionDateTime, value, true); }
		}

		/// <summary> The CompletionType property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."COMPLETIONTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<VarioSL.Entities.Enumerations.CreditCardCompletionType> CompletionType
		{
			get { return (Nullable<VarioSL.Entities.Enumerations.CreditCardCompletionType>)GetValue((int)CreditCardAuthorizationFieldIndex.CompletionType, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CompletionType, value, true); }
		}

		/// <summary> The CreationDate property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CreditCardAuthorizationFieldIndex.CreationDate, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The CreditCardAuthorizationID property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CREDITCARDAUTHORIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CreditCardAuthorizationID
		{
			get { return (System.Int64)GetValue((int)CreditCardAuthorizationFieldIndex.CreditCardAuthorizationID, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CreditCardAuthorizationID, value, true); }
		}

		/// <summary> The CreditCardTerminalID property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."CREDITCARDTERMINALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CreditCardTerminalID
		{
			get { return (System.Int64)GetValue((int)CreditCardAuthorizationFieldIndex.CreditCardTerminalID, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.CreditCardTerminalID, value, true); }
		}

		/// <summary> The DeviceTypeIndicator property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."DEVICETYPEINDICATOR"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceTypeIndicator
		{
			get { return (Nullable<System.Int32>)GetValue((int)CreditCardAuthorizationFieldIndex.DeviceTypeIndicator, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.DeviceTypeIndicator, value, true); }
		}

		/// <summary> The EMVData property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."EMVDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EMVData
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.EMVData, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.EMVData, value, true); }
		}

		/// <summary> The EncryptedData property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."ENCRYPTEDDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EncryptedData
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.EncryptedData, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.EncryptedData, value, true); }
		}

		/// <summary> The ExternalAuthID property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."EXTERNALAUTHID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalAuthID
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.ExternalAuthID, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.ExternalAuthID, value, true); }
		}

		/// <summary> The ExternalCardHash property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."EXTERNALCARDHASH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalCardHash
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.ExternalCardHash, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.ExternalCardHash, value, true); }
		}

		/// <summary> The ExternalCardReference property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."EXTERNALCARDREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalCardReference
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.ExternalCardReference, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.ExternalCardReference, value, true); }
		}

		/// <summary> The IsArqcRequested property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."ISARQCREQUESTED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsArqcRequested
		{
			get { return (Nullable<System.Boolean>)GetValue((int)CreditCardAuthorizationFieldIndex.IsArqcRequested, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.IsArqcRequested, value, true); }
		}

		/// <summary> The LastEmvDataUpdate property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."LASTEMVDATAUPDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastEmvDataUpdate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CreditCardAuthorizationFieldIndex.LastEmvDataUpdate, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.LastEmvDataUpdate, value, true); }
		}

		/// <summary> The LastModified property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)CreditCardAuthorizationFieldIndex.LastModified, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.LastUser, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LocalDateTime property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."LOCALDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LocalDateTime
		{
			get { return (System.DateTime)GetValue((int)CreditCardAuthorizationFieldIndex.LocalDateTime, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.LocalDateTime, value, true); }
		}

		/// <summary> The OrderNumber property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."ORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 15<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String OrderNumber
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.OrderNumber, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.OrderNumber, value, true); }
		}

		/// <summary> The PurseAmount property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."PURSEAMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PurseAmount
		{
			get { return (System.Int32)GetValue((int)CreditCardAuthorizationFieldIndex.PurseAmount, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.PurseAmount, value, true); }
		}

		/// <summary> The ResponseCode property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."RESPONSECODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String ResponseCode
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.ResponseCode, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.ResponseCode, value, true); }
		}

		/// <summary> The RetryCountdown property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."RETRYCOUNTDOWN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RetryCountdown
		{
			get { return (Nullable<System.Int32>)GetValue((int)CreditCardAuthorizationFieldIndex.RetryCountdown, false); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.RetryCountdown, value, true); }
		}

		/// <summary> The RiskFlags property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."RISKFLAGS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CreditCardRiskAnalysisFlags RiskFlags
		{
			get { return (VarioSL.Entities.Enumerations.CreditCardRiskAnalysisFlags)GetValue((int)CreditCardAuthorizationFieldIndex.RiskFlags, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.RiskFlags, value, true); }
		}

		/// <summary> The Status property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."STATUS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CreditCardAuthorizationStatus Status
		{
			get { return (VarioSL.Entities.Enumerations.CreditCardAuthorizationStatus)GetValue((int)CreditCardAuthorizationFieldIndex.Status, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.Status, value, true); }
		}

		/// <summary> The SystemTraceAuditNumber property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."SYSTEMTRACEAUDITNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SystemTraceAuditNumber
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.SystemTraceAuditNumber, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.SystemTraceAuditNumber, value, true); }
		}

		/// <summary> The Token property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."TOKEN"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Token
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.Token, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.Token, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CreditCardAuthorizationFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionDateTime property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."TRANSACTIONDATETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TransactionDateTime
		{
			get { return (System.DateTime)GetValue((int)CreditCardAuthorizationFieldIndex.TransactionDateTime, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.TransactionDateTime, value, true); }
		}

		/// <summary> The TransactionIdentifier property of the Entity CreditCardAuthorization<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATION"."TRANSACTIONIDENTIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TransactionIdentifier
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationFieldIndex.TransactionIdentifier, true); }
			set	{ SetValue((int)CreditCardAuthorizationFieldIndex.TransactionIdentifier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CreditCardAuthorizationLogEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCreditCardAuthorizationLogs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CreditCardAuthorizationLogCollection CreditCardAuthorizationLogs
		{
			get	{ return GetMultiCreditCardAuthorizationLogs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorizationLogs. When set to true, CreditCardAuthorizationLogs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorizationLogs is accessed. You can always execute/ a forced fetch by calling GetMultiCreditCardAuthorizationLogs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorizationLogs
		{
			get	{ return _alwaysFetchCreditCardAuthorizationLogs; }
			set	{ _alwaysFetchCreditCardAuthorizationLogs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorizationLogs already has been fetched. Setting this property to false when CreditCardAuthorizationLogs has been fetched
		/// will clear the CreditCardAuthorizationLogs collection well. Setting this property to true while CreditCardAuthorizationLogs hasn't been fetched disables lazy loading for CreditCardAuthorizationLogs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorizationLogs
		{
			get { return _alreadyFetchedCreditCardAuthorizationLogs;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorizationLogs && !value && (_creditCardAuthorizationLogs != null))
				{
					_creditCardAuthorizationLogs.Clear();
				}
				_alreadyFetchedCreditCardAuthorizationLogs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CreditCardAuthorizations", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CreditCardTerminalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCreditCardTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CreditCardTerminalEntity CreditCardTerminal
		{
			get	{ return GetSingleCreditCardTerminal(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCreditCardTerminal(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CreditCardAuthorizations", "CreditCardTerminal", _creditCardTerminal, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardTerminal. When set to true, CreditCardTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardTerminal is accessed. You can always execute a forced fetch by calling GetSingleCreditCardTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardTerminal
		{
			get	{ return _alwaysFetchCreditCardTerminal; }
			set	{ _alwaysFetchCreditCardTerminal = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardTerminal already has been fetched. Setting this property to false when CreditCardTerminal has been fetched
		/// will set CreditCardTerminal to null as well. Setting this property to true while CreditCardTerminal hasn't been fetched disables lazy loading for CreditCardTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardTerminal
		{
			get { return _alreadyFetchedCreditCardTerminal;}
			set 
			{
				if(_alreadyFetchedCreditCardTerminal && !value)
				{
					this.CreditCardTerminal = null;
				}
				_alreadyFetchedCreditCardTerminal = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CreditCardTerminal is not found
		/// in the database. When set to true, CreditCardTerminal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CreditCardTerminalReturnsNewIfNotFound
		{
			get	{ return _creditCardTerminalReturnsNewIfNotFound; }
			set { _creditCardTerminalReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
