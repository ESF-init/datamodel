﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BusinessRuleTypeToVariable'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BusinessRuleTypeToVariableEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private BusinessRuleTypeEntity _businessRuleType;
		private bool	_alwaysFetchBusinessRuleType, _alreadyFetchedBusinessRuleType, _businessRuleTypeReturnsNewIfNotFound;
		private BusinessRuleVariableEntity _businessRuleVariable;
		private bool	_alwaysFetchBusinessRuleVariable, _alreadyFetchedBusinessRuleVariable, _businessRuleVariableReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BusinessRuleType</summary>
			public static readonly string BusinessRuleType = "BusinessRuleType";
			/// <summary>Member name BusinessRuleVariable</summary>
			public static readonly string BusinessRuleVariable = "BusinessRuleVariable";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BusinessRuleTypeToVariableEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BusinessRuleTypeToVariableEntity() :base("BusinessRuleTypeToVariableEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		public BusinessRuleTypeToVariableEntity(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID):base("BusinessRuleTypeToVariableEntity")
		{
			InitClassFetch(businessRuleTypeID, businessRuleVariableID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BusinessRuleTypeToVariableEntity(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IPrefetchPath prefetchPathToUse):base("BusinessRuleTypeToVariableEntity")
		{
			InitClassFetch(businessRuleTypeID, businessRuleVariableID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="validator">The custom validator object for this BusinessRuleTypeToVariableEntity</param>
		public BusinessRuleTypeToVariableEntity(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IValidator validator):base("BusinessRuleTypeToVariableEntity")
		{
			InitClassFetch(businessRuleTypeID, businessRuleVariableID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinessRuleTypeToVariableEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_businessRuleType = (BusinessRuleTypeEntity)info.GetValue("_businessRuleType", typeof(BusinessRuleTypeEntity));
			if(_businessRuleType!=null)
			{
				_businessRuleType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleTypeReturnsNewIfNotFound = info.GetBoolean("_businessRuleTypeReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleType = info.GetBoolean("_alwaysFetchBusinessRuleType");
			_alreadyFetchedBusinessRuleType = info.GetBoolean("_alreadyFetchedBusinessRuleType");

			_businessRuleVariable = (BusinessRuleVariableEntity)info.GetValue("_businessRuleVariable", typeof(BusinessRuleVariableEntity));
			if(_businessRuleVariable!=null)
			{
				_businessRuleVariable.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_businessRuleVariableReturnsNewIfNotFound = info.GetBoolean("_businessRuleVariableReturnsNewIfNotFound");
			_alwaysFetchBusinessRuleVariable = info.GetBoolean("_alwaysFetchBusinessRuleVariable");
			_alreadyFetchedBusinessRuleVariable = info.GetBoolean("_alreadyFetchedBusinessRuleVariable");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((BusinessRuleTypeToVariableFieldIndex)fieldIndex)
			{
				case BusinessRuleTypeToVariableFieldIndex.BusinessRuleTypeID:
					DesetupSyncBusinessRuleType(true, false);
					_alreadyFetchedBusinessRuleType = false;
					break;
				case BusinessRuleTypeToVariableFieldIndex.BusinessRuleVariableID:
					DesetupSyncBusinessRuleVariable(true, false);
					_alreadyFetchedBusinessRuleVariable = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBusinessRuleType = (_businessRuleType != null);
			_alreadyFetchedBusinessRuleVariable = (_businessRuleVariable != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BusinessRuleType":
					toReturn.Add(Relations.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
					break;
				case "BusinessRuleVariable":
					toReturn.Add(Relations.BusinessRuleVariableEntityUsingBusinessRuleVariableID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_businessRuleType", (!this.MarkedForDeletion?_businessRuleType:null));
			info.AddValue("_businessRuleTypeReturnsNewIfNotFound", _businessRuleTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleType", _alwaysFetchBusinessRuleType);
			info.AddValue("_alreadyFetchedBusinessRuleType", _alreadyFetchedBusinessRuleType);
			info.AddValue("_businessRuleVariable", (!this.MarkedForDeletion?_businessRuleVariable:null));
			info.AddValue("_businessRuleVariableReturnsNewIfNotFound", _businessRuleVariableReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBusinessRuleVariable", _alwaysFetchBusinessRuleVariable);
			info.AddValue("_alreadyFetchedBusinessRuleVariable", _alreadyFetchedBusinessRuleVariable);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BusinessRuleType":
					_alreadyFetchedBusinessRuleType = true;
					this.BusinessRuleType = (BusinessRuleTypeEntity)entity;
					break;
				case "BusinessRuleVariable":
					_alreadyFetchedBusinessRuleVariable = true;
					this.BusinessRuleVariable = (BusinessRuleVariableEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BusinessRuleType":
					SetupSyncBusinessRuleType(relatedEntity);
					break;
				case "BusinessRuleVariable":
					SetupSyncBusinessRuleVariable(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BusinessRuleType":
					DesetupSyncBusinessRuleType(false, true);
					break;
				case "BusinessRuleVariable":
					DesetupSyncBusinessRuleVariable(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_businessRuleType!=null)
			{
				toReturn.Add(_businessRuleType);
			}
			if(_businessRuleVariable!=null)
			{
				toReturn.Add(_businessRuleVariable);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID)
		{
			return FetchUsingPK(businessRuleTypeID, businessRuleVariableID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(businessRuleTypeID, businessRuleVariableID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(businessRuleTypeID, businessRuleVariableID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(businessRuleTypeID, businessRuleVariableID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BusinessRuleTypeID, this.BusinessRuleVariableID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BusinessRuleTypeToVariableRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleTypeEntity' which is related to this entity.</returns>
		public BusinessRuleTypeEntity GetSingleBusinessRuleType()
		{
			return GetSingleBusinessRuleType(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleTypeEntity' which is related to this entity.</returns>
		public virtual BusinessRuleTypeEntity GetSingleBusinessRuleType(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleType || forceFetch || _alwaysFetchBusinessRuleType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
				BusinessRuleTypeEntity newEntity = new BusinessRuleTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessRuleTypeID);
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleType = newEntity;
				_alreadyFetchedBusinessRuleType = fetchResult;
			}
			return _businessRuleType;
		}


		/// <summary> Retrieves the related entity of type 'BusinessRuleVariableEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'BusinessRuleVariableEntity' which is related to this entity.</returns>
		public BusinessRuleVariableEntity GetSingleBusinessRuleVariable()
		{
			return GetSingleBusinessRuleVariable(false);
		}

		/// <summary> Retrieves the related entity of type 'BusinessRuleVariableEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BusinessRuleVariableEntity' which is related to this entity.</returns>
		public virtual BusinessRuleVariableEntity GetSingleBusinessRuleVariable(bool forceFetch)
		{
			if( ( !_alreadyFetchedBusinessRuleVariable || forceFetch || _alwaysFetchBusinessRuleVariable) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BusinessRuleVariableEntityUsingBusinessRuleVariableID);
				BusinessRuleVariableEntity newEntity = new BusinessRuleVariableEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.BusinessRuleVariableID);
				}
				if(fetchResult)
				{
					newEntity = (BusinessRuleVariableEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_businessRuleVariableReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BusinessRuleVariable = newEntity;
				_alreadyFetchedBusinessRuleVariable = fetchResult;
			}
			return _businessRuleVariable;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BusinessRuleType", _businessRuleType);
			toReturn.Add("BusinessRuleVariable", _businessRuleVariable);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="validator">The validator object for this BusinessRuleTypeToVariableEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(businessRuleTypeID, businessRuleVariableID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_businessRuleTypeReturnsNewIfNotFound = false;
			_businessRuleVariableReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleVariableID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Mandatory", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _businessRuleType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleType, new PropertyChangedEventHandler( OnBusinessRuleTypePropertyChanged ), "BusinessRuleType", VarioSL.Entities.RelationClasses.StaticBusinessRuleTypeToVariableRelations.BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic, true, signalRelatedEntity, "BusinessRuleTypeToVariables", resetFKFields, new int[] { (int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleTypeID } );		
			_businessRuleType = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleType(IEntityCore relatedEntity)
		{
			if(_businessRuleType!=relatedEntity)
			{		
				DesetupSyncBusinessRuleType(true, true);
				_businessRuleType = (BusinessRuleTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleType, new PropertyChangedEventHandler( OnBusinessRuleTypePropertyChanged ), "BusinessRuleType", VarioSL.Entities.RelationClasses.StaticBusinessRuleTypeToVariableRelations.BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic, true, ref _alreadyFetchedBusinessRuleType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _businessRuleVariable</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBusinessRuleVariable(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _businessRuleVariable, new PropertyChangedEventHandler( OnBusinessRuleVariablePropertyChanged ), "BusinessRuleVariable", VarioSL.Entities.RelationClasses.StaticBusinessRuleTypeToVariableRelations.BusinessRuleVariableEntityUsingBusinessRuleVariableIDStatic, true, signalRelatedEntity, "BusinessRuleTypeToVariables", resetFKFields, new int[] { (int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleVariableID } );		
			_businessRuleVariable = null;
		}
		
		/// <summary> setups the sync logic for member _businessRuleVariable</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBusinessRuleVariable(IEntityCore relatedEntity)
		{
			if(_businessRuleVariable!=relatedEntity)
			{		
				DesetupSyncBusinessRuleVariable(true, true);
				_businessRuleVariable = (BusinessRuleVariableEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _businessRuleVariable, new PropertyChangedEventHandler( OnBusinessRuleVariablePropertyChanged ), "BusinessRuleVariable", VarioSL.Entities.RelationClasses.StaticBusinessRuleTypeToVariableRelations.BusinessRuleVariableEntityUsingBusinessRuleVariableIDStatic, true, ref _alreadyFetchedBusinessRuleVariable, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBusinessRuleVariablePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="businessRuleVariableID">PK value for BusinessRuleTypeToVariable which data should be fetched into this BusinessRuleTypeToVariable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 businessRuleTypeID, System.Int64 businessRuleVariableID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleTypeID].ForcedCurrentValueWrite(businessRuleTypeID);
				this.Fields[(int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleVariableID].ForcedCurrentValueWrite(businessRuleVariableID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBusinessRuleTypeToVariableDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BusinessRuleTypeToVariableEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BusinessRuleTypeToVariableRelations Relations
		{
			get	{ return new BusinessRuleTypeToVariableRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleTypeCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleType")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity, (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, 0, null, null, null, "BusinessRuleType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleVariable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleVariable
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleVariable")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity, (int)VarioSL.Entities.EntityType.BusinessRuleVariableEntity, 0, null, null, null, "BusinessRuleVariable", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BusinessRuleTypeID property of the Entity BusinessRuleTypeToVariable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPETOVARIABLE"."BUSINESSRULETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 BusinessRuleTypeID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleTypeID, true); }
			set	{ SetValue((int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleTypeID, value, true); }
		}

		/// <summary> The BusinessRuleVariableID property of the Entity BusinessRuleTypeToVariable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPETOVARIABLE"."BUSINESSRULEVARIABLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 BusinessRuleVariableID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleVariableID, true); }
			set	{ SetValue((int)BusinessRuleTypeToVariableFieldIndex.BusinessRuleVariableID, value, true); }
		}

		/// <summary> The Mandatory property of the Entity BusinessRuleTypeToVariable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPETOVARIABLE"."MANDATORY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Mandatory
		{
			get { return (System.Boolean)GetValue((int)BusinessRuleTypeToVariableFieldIndex.Mandatory, true); }
			set	{ SetValue((int)BusinessRuleTypeToVariableFieldIndex.Mandatory, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'BusinessRuleTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleTypeEntity BusinessRuleType
		{
			get	{ return GetSingleBusinessRuleType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleTypeToVariables", "BusinessRuleType", _businessRuleType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleType. When set to true, BusinessRuleType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleType is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleType
		{
			get	{ return _alwaysFetchBusinessRuleType; }
			set	{ _alwaysFetchBusinessRuleType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleType already has been fetched. Setting this property to false when BusinessRuleType has been fetched
		/// will set BusinessRuleType to null as well. Setting this property to true while BusinessRuleType hasn't been fetched disables lazy loading for BusinessRuleType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleType
		{
			get { return _alreadyFetchedBusinessRuleType;}
			set 
			{
				if(_alreadyFetchedBusinessRuleType && !value)
				{
					this.BusinessRuleType = null;
				}
				_alreadyFetchedBusinessRuleType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleType is not found
		/// in the database. When set to true, BusinessRuleType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleTypeReturnsNewIfNotFound
		{
			get	{ return _businessRuleTypeReturnsNewIfNotFound; }
			set { _businessRuleTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BusinessRuleVariableEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBusinessRuleVariable()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BusinessRuleVariableEntity BusinessRuleVariable
		{
			get	{ return GetSingleBusinessRuleVariable(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncBusinessRuleVariable(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "BusinessRuleTypeToVariables", "BusinessRuleVariable", _businessRuleVariable, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleVariable. When set to true, BusinessRuleVariable is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleVariable is accessed. You can always execute a forced fetch by calling GetSingleBusinessRuleVariable(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleVariable
		{
			get	{ return _alwaysFetchBusinessRuleVariable; }
			set	{ _alwaysFetchBusinessRuleVariable = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleVariable already has been fetched. Setting this property to false when BusinessRuleVariable has been fetched
		/// will set BusinessRuleVariable to null as well. Setting this property to true while BusinessRuleVariable hasn't been fetched disables lazy loading for BusinessRuleVariable</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleVariable
		{
			get { return _alreadyFetchedBusinessRuleVariable;}
			set 
			{
				if(_alreadyFetchedBusinessRuleVariable && !value)
				{
					this.BusinessRuleVariable = null;
				}
				_alreadyFetchedBusinessRuleVariable = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BusinessRuleVariable is not found
		/// in the database. When set to true, BusinessRuleVariable will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BusinessRuleVariableReturnsNewIfNotFound
		{
			get	{ return _businessRuleVariableReturnsNewIfNotFound; }
			set { _businessRuleVariableReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
