﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentRecognition'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PaymentRecognitionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CloseoutPeriodEntity _closeoutPeriod;
		private bool	_alwaysFetchCloseoutPeriod, _alreadyFetchedCloseoutPeriod, _closeoutPeriodReturnsNewIfNotFound;
		private PaymentJournalEntity _paymentJournal;
		private bool	_alwaysFetchPaymentJournal, _alreadyFetchedPaymentJournal, _paymentJournalReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CloseoutPeriod</summary>
			public static readonly string CloseoutPeriod = "CloseoutPeriod";
			/// <summary>Member name PaymentJournal</summary>
			public static readonly string PaymentJournal = "PaymentJournal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PaymentRecognitionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentRecognitionEntity() :base("PaymentRecognitionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		public PaymentRecognitionEntity(System.Int64 paymentRecognitionID):base("PaymentRecognitionEntity")
		{
			InitClassFetch(paymentRecognitionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentRecognitionEntity(System.Int64 paymentRecognitionID, IPrefetchPath prefetchPathToUse):base("PaymentRecognitionEntity")
		{
			InitClassFetch(paymentRecognitionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="validator">The custom validator object for this PaymentRecognitionEntity</param>
		public PaymentRecognitionEntity(System.Int64 paymentRecognitionID, IValidator validator):base("PaymentRecognitionEntity")
		{
			InitClassFetch(paymentRecognitionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentRecognitionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_closeoutPeriod = (CloseoutPeriodEntity)info.GetValue("_closeoutPeriod", typeof(CloseoutPeriodEntity));
			if(_closeoutPeriod!=null)
			{
				_closeoutPeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_closeoutPeriodReturnsNewIfNotFound = info.GetBoolean("_closeoutPeriodReturnsNewIfNotFound");
			_alwaysFetchCloseoutPeriod = info.GetBoolean("_alwaysFetchCloseoutPeriod");
			_alreadyFetchedCloseoutPeriod = info.GetBoolean("_alreadyFetchedCloseoutPeriod");
			_paymentJournal = (PaymentJournalEntity)info.GetValue("_paymentJournal", typeof(PaymentJournalEntity));
			if(_paymentJournal!=null)
			{
				_paymentJournal.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paymentJournalReturnsNewIfNotFound = info.GetBoolean("_paymentJournalReturnsNewIfNotFound");
			_alwaysFetchPaymentJournal = info.GetBoolean("_alwaysFetchPaymentJournal");
			_alreadyFetchedPaymentJournal = info.GetBoolean("_alreadyFetchedPaymentJournal");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((PaymentRecognitionFieldIndex)fieldIndex)
			{
				case PaymentRecognitionFieldIndex.CloseoutPeriodID:
					DesetupSyncCloseoutPeriod(true, false);
					_alreadyFetchedCloseoutPeriod = false;
					break;
				case PaymentRecognitionFieldIndex.PaymentJournalID:
					DesetupSyncPaymentJournal(true, false);
					_alreadyFetchedPaymentJournal = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCloseoutPeriod = (_closeoutPeriod != null);
			_alreadyFetchedPaymentJournal = (_paymentJournal != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CloseoutPeriod":
					toReturn.Add(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
					break;
				case "PaymentJournal":
					toReturn.Add(Relations.PaymentJournalEntityUsingPaymentJournalID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_closeoutPeriod", (!this.MarkedForDeletion?_closeoutPeriod:null));
			info.AddValue("_closeoutPeriodReturnsNewIfNotFound", _closeoutPeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCloseoutPeriod", _alwaysFetchCloseoutPeriod);
			info.AddValue("_alreadyFetchedCloseoutPeriod", _alreadyFetchedCloseoutPeriod);

			info.AddValue("_paymentJournal", (!this.MarkedForDeletion?_paymentJournal:null));
			info.AddValue("_paymentJournalReturnsNewIfNotFound", _paymentJournalReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPaymentJournal", _alwaysFetchPaymentJournal);
			info.AddValue("_alreadyFetchedPaymentJournal", _alreadyFetchedPaymentJournal);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CloseoutPeriod":
					_alreadyFetchedCloseoutPeriod = true;
					this.CloseoutPeriod = (CloseoutPeriodEntity)entity;
					break;
				case "PaymentJournal":
					_alreadyFetchedPaymentJournal = true;
					this.PaymentJournal = (PaymentJournalEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					SetupSyncCloseoutPeriod(relatedEntity);
					break;
				case "PaymentJournal":
					SetupSyncPaymentJournal(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CloseoutPeriod":
					DesetupSyncCloseoutPeriod(false, true);
					break;
				case "PaymentJournal":
					DesetupSyncPaymentJournal(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_closeoutPeriod!=null)
			{
				toReturn.Add(_closeoutPeriod);
			}
			if(_paymentJournal!=null)
			{
				toReturn.Add(_paymentJournal);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(System.Int64 paymentJournalID)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCPaymentJournalID( paymentJournalID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="paymentJournalID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCPaymentJournalID(System.Int64 paymentJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((PaymentRecognitionDAO)CreateDAOInstance()).FetchPaymentRecognitionUsingUCPaymentJournalID(this, this.Transaction, paymentJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentRecognitionID)
		{
			return FetchUsingPK(paymentRecognitionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentRecognitionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paymentRecognitionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paymentRecognitionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paymentRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paymentRecognitionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PaymentRecognitionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PaymentRecognitionRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public CloseoutPeriodEntity GetSingleCloseoutPeriod()
		{
			return GetSingleCloseoutPeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'CloseoutPeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CloseoutPeriodEntity' which is related to this entity.</returns>
		public virtual CloseoutPeriodEntity GetSingleCloseoutPeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedCloseoutPeriod || forceFetch || _alwaysFetchCloseoutPeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CloseoutPeriodEntityUsingCloseoutPeriodID);
				CloseoutPeriodEntity newEntity = new CloseoutPeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CloseoutPeriodID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CloseoutPeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_closeoutPeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CloseoutPeriod = newEntity;
				_alreadyFetchedCloseoutPeriod = fetchResult;
			}
			return _closeoutPeriod;
		}

		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public PaymentJournalEntity GetSinglePaymentJournal()
		{
			return GetSinglePaymentJournal(false);
		}
		
		/// <summary> Retrieves the related entity of type 'PaymentJournalEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PaymentJournalEntity' which is related to this entity.</returns>
		public virtual PaymentJournalEntity GetSinglePaymentJournal(bool forceFetch)
		{
			if( ( !_alreadyFetchedPaymentJournal || forceFetch || _alwaysFetchPaymentJournal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PaymentJournalEntityUsingPaymentJournalID);
				PaymentJournalEntity newEntity = new PaymentJournalEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PaymentJournalID);
				}
				if(fetchResult)
				{
					newEntity = (PaymentJournalEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paymentJournalReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PaymentJournal = newEntity;
				_alreadyFetchedPaymentJournal = fetchResult;
			}
			return _paymentJournal;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CloseoutPeriod", _closeoutPeriod);
			toReturn.Add("PaymentJournal", _paymentJournal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="validator">The validator object for this PaymentRecognitionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 paymentRecognitionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paymentRecognitionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_closeoutPeriodReturnsNewIfNotFound = false;
			_paymentJournalReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PaymentRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _closeoutPeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCloseoutPeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticPaymentRecognitionRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, signalRelatedEntity, "PaymentRecognitions", resetFKFields, new int[] { (int)PaymentRecognitionFieldIndex.CloseoutPeriodID } );		
			_closeoutPeriod = null;
		}
		
		/// <summary> setups the sync logic for member _closeoutPeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCloseoutPeriod(IEntityCore relatedEntity)
		{
			if(_closeoutPeriod!=relatedEntity)
			{		
				DesetupSyncCloseoutPeriod(true, true);
				_closeoutPeriod = (CloseoutPeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _closeoutPeriod, new PropertyChangedEventHandler( OnCloseoutPeriodPropertyChanged ), "CloseoutPeriod", VarioSL.Entities.RelationClasses.StaticPaymentRecognitionRelations.CloseoutPeriodEntityUsingCloseoutPeriodIDStatic, true, ref _alreadyFetchedCloseoutPeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCloseoutPeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _paymentJournal</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPaymentJournal(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticPaymentRecognitionRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, signalRelatedEntity, "PaymentRecognition", resetFKFields, new int[] { (int)PaymentRecognitionFieldIndex.PaymentJournalID } );
			_paymentJournal = null;
		}
	
		/// <summary> setups the sync logic for member _paymentJournal</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPaymentJournal(IEntityCore relatedEntity)
		{
			if(_paymentJournal!=relatedEntity)
			{
				DesetupSyncPaymentJournal(true, true);
				_paymentJournal = (PaymentJournalEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paymentJournal, new PropertyChangedEventHandler( OnPaymentJournalPropertyChanged ), "PaymentJournal", VarioSL.Entities.RelationClasses.StaticPaymentRecognitionRelations.PaymentJournalEntityUsingPaymentJournalIDStatic, true, ref _alreadyFetchedPaymentJournal, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPaymentJournalPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paymentRecognitionID">PK value for PaymentRecognition which data should be fetched into this PaymentRecognition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 paymentRecognitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PaymentRecognitionFieldIndex.PaymentRecognitionID].ForcedCurrentValueWrite(paymentRecognitionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePaymentRecognitionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PaymentRecognitionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PaymentRecognitionRelations Relations
		{
			get	{ return new PaymentRecognitionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CloseoutPeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCloseoutPeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CloseoutPeriodCollection(), (IEntityRelation)GetRelationsForField("CloseoutPeriod")[0], (int)VarioSL.Entities.EntityType.PaymentRecognitionEntity, (int)VarioSL.Entities.EntityType.CloseoutPeriodEntity, 0, null, null, null, "CloseoutPeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournal
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournal")[0], (int)VarioSL.Entities.EntityType.PaymentRecognitionEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)PaymentRecognitionFieldIndex.Amount, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.Amount, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."CLOSEOUTPERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CloseoutPeriodID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PaymentRecognitionFieldIndex.CloseoutPeriodID, false); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."CREDITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)PaymentRecognitionFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."DEBITACCOUNTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)PaymentRecognitionFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The LastModified property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)PaymentRecognitionFieldIndex.LastModified, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)PaymentRecognitionFieldIndex.LastUser, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PaymentJournalID property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."PAYMENTJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PaymentJournalID
		{
			get { return (System.Int64)GetValue((int)PaymentRecognitionFieldIndex.PaymentJournalID, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.PaymentJournalID, value, true); }
		}

		/// <summary> The PaymentRecognitionID property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."PAYMENTRECOGNITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 PaymentRecognitionID
		{
			get { return (System.Int64)GetValue((int)PaymentRecognitionFieldIndex.PaymentRecognitionID, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.PaymentRecognitionID, value, true); }
		}

		/// <summary> The PostingDate property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."POSTINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PaymentRecognitionFieldIndex.PostingDate, false); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."POSTINGREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)PaymentRecognitionFieldIndex.PostingReference, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity PaymentRecognition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ACC_PAYMENTRECOGNITION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)PaymentRecognitionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)PaymentRecognitionFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CloseoutPeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCloseoutPeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CloseoutPeriodEntity CloseoutPeriod
		{
			get	{ return GetSingleCloseoutPeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCloseoutPeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "PaymentRecognitions", "CloseoutPeriod", _closeoutPeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CloseoutPeriod. When set to true, CloseoutPeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CloseoutPeriod is accessed. You can always execute a forced fetch by calling GetSingleCloseoutPeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCloseoutPeriod
		{
			get	{ return _alwaysFetchCloseoutPeriod; }
			set	{ _alwaysFetchCloseoutPeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CloseoutPeriod already has been fetched. Setting this property to false when CloseoutPeriod has been fetched
		/// will set CloseoutPeriod to null as well. Setting this property to true while CloseoutPeriod hasn't been fetched disables lazy loading for CloseoutPeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCloseoutPeriod
		{
			get { return _alreadyFetchedCloseoutPeriod;}
			set 
			{
				if(_alreadyFetchedCloseoutPeriod && !value)
				{
					this.CloseoutPeriod = null;
				}
				_alreadyFetchedCloseoutPeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CloseoutPeriod is not found
		/// in the database. When set to true, CloseoutPeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CloseoutPeriodReturnsNewIfNotFound
		{
			get	{ return _closeoutPeriodReturnsNewIfNotFound; }
			set { _closeoutPeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PaymentJournalEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePaymentJournal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PaymentJournalEntity PaymentJournal
		{
			get	{ return GetSinglePaymentJournal(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPaymentJournal(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_paymentJournal !=null);
						DesetupSyncPaymentJournal(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("PaymentJournal");
						}
					}
					else
					{
						if(_paymentJournal!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "PaymentRecognition");
							SetupSyncPaymentJournal(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournal. When set to true, PaymentJournal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournal is accessed. You can always execute a forced fetch by calling GetSinglePaymentJournal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournal
		{
			get	{ return _alwaysFetchPaymentJournal; }
			set	{ _alwaysFetchPaymentJournal = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournal already has been fetched. Setting this property to false when PaymentJournal has been fetched
		/// will set PaymentJournal to null as well. Setting this property to true while PaymentJournal hasn't been fetched disables lazy loading for PaymentJournal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournal
		{
			get { return _alreadyFetchedPaymentJournal;}
			set 
			{
				if(_alreadyFetchedPaymentJournal && !value)
				{
					this.PaymentJournal = null;
				}
				_alreadyFetchedPaymentJournal = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PaymentJournal is not found
		/// in the database. When set to true, PaymentJournal will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PaymentJournalReturnsNewIfNotFound
		{
			get	{ return _paymentJournalReturnsNewIfNotFound; }
			set	{ _paymentJournalReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PaymentRecognitionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
