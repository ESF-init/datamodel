﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ParaTransJournal'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ParaTransJournalEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private ParaTransJournalTypeEntity _paraTransJournalTypeNavigator;
		private bool	_alwaysFetchParaTransJournalTypeNavigator, _alreadyFetchedParaTransJournalTypeNavigator, _paraTransJournalTypeNavigatorReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ParaTransJournalTypeNavigator</summary>
			public static readonly string ParaTransJournalTypeNavigator = "ParaTransJournalTypeNavigator";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ParaTransJournalEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ParaTransJournalEntity() :base("ParaTransJournalEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		public ParaTransJournalEntity(System.Int64 paraTransJournalID):base("ParaTransJournalEntity")
		{
			InitClassFetch(paraTransJournalID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ParaTransJournalEntity(System.Int64 paraTransJournalID, IPrefetchPath prefetchPathToUse):base("ParaTransJournalEntity")
		{
			InitClassFetch(paraTransJournalID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="validator">The custom validator object for this ParaTransJournalEntity</param>
		public ParaTransJournalEntity(System.Int64 paraTransJournalID, IValidator validator):base("ParaTransJournalEntity")
		{
			InitClassFetch(paraTransJournalID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ParaTransJournalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paraTransJournalTypeNavigator = (ParaTransJournalTypeEntity)info.GetValue("_paraTransJournalTypeNavigator", typeof(ParaTransJournalTypeEntity));
			if(_paraTransJournalTypeNavigator!=null)
			{
				_paraTransJournalTypeNavigator.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_paraTransJournalTypeNavigatorReturnsNewIfNotFound = info.GetBoolean("_paraTransJournalTypeNavigatorReturnsNewIfNotFound");
			_alwaysFetchParaTransJournalTypeNavigator = info.GetBoolean("_alwaysFetchParaTransJournalTypeNavigator");
			_alreadyFetchedParaTransJournalTypeNavigator = info.GetBoolean("_alreadyFetchedParaTransJournalTypeNavigator");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ParaTransJournalFieldIndex)fieldIndex)
			{
				case ParaTransJournalFieldIndex.ParaTransJournalType:
					DesetupSyncParaTransJournalTypeNavigator(true, false);
					_alreadyFetchedParaTransJournalTypeNavigator = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedParaTransJournalTypeNavigator = (_paraTransJournalTypeNavigator != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ParaTransJournalTypeNavigator":
					toReturn.Add(Relations.ParaTransJournalTypeEntityUsingParaTransJournalType);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paraTransJournalTypeNavigator", (!this.MarkedForDeletion?_paraTransJournalTypeNavigator:null));
			info.AddValue("_paraTransJournalTypeNavigatorReturnsNewIfNotFound", _paraTransJournalTypeNavigatorReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParaTransJournalTypeNavigator", _alwaysFetchParaTransJournalTypeNavigator);
			info.AddValue("_alreadyFetchedParaTransJournalTypeNavigator", _alreadyFetchedParaTransJournalTypeNavigator);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ParaTransJournalTypeNavigator":
					_alreadyFetchedParaTransJournalTypeNavigator = true;
					this.ParaTransJournalTypeNavigator = (ParaTransJournalTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ParaTransJournalTypeNavigator":
					SetupSyncParaTransJournalTypeNavigator(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ParaTransJournalTypeNavigator":
					DesetupSyncParaTransJournalTypeNavigator(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_paraTransJournalTypeNavigator!=null)
			{
				toReturn.Add(_paraTransJournalTypeNavigator);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paraTransJournalID)
		{
			return FetchUsingPK(paraTransJournalID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paraTransJournalID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(paraTransJournalID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paraTransJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(paraTransJournalID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 paraTransJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(paraTransJournalID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ParaTransJournalID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ParaTransJournalRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ParaTransJournalTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ParaTransJournalTypeEntity' which is related to this entity.</returns>
		public ParaTransJournalTypeEntity GetSingleParaTransJournalTypeNavigator()
		{
			return GetSingleParaTransJournalTypeNavigator(false);
		}

		/// <summary> Retrieves the related entity of type 'ParaTransJournalTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ParaTransJournalTypeEntity' which is related to this entity.</returns>
		public virtual ParaTransJournalTypeEntity GetSingleParaTransJournalTypeNavigator(bool forceFetch)
		{
			if( ( !_alreadyFetchedParaTransJournalTypeNavigator || forceFetch || _alwaysFetchParaTransJournalTypeNavigator) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ParaTransJournalTypeEntityUsingParaTransJournalType);
				ParaTransJournalTypeEntity newEntity = new ParaTransJournalTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParaTransJournalType);
				}
				if(fetchResult)
				{
					newEntity = (ParaTransJournalTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_paraTransJournalTypeNavigatorReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParaTransJournalTypeNavigator = newEntity;
				_alreadyFetchedParaTransJournalTypeNavigator = fetchResult;
			}
			return _paraTransJournalTypeNavigator;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ParaTransJournalTypeNavigator", _paraTransJournalTypeNavigator);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="validator">The validator object for this ParaTransJournalEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 paraTransJournalID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(paraTransJournalID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_paraTransJournalTypeNavigatorReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalRiderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalTripID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Faremeter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Latitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Longitude", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Odometer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorCompanyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParaTransJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParaTransJournalType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParaTransPaymentType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SharedRideID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _paraTransJournalTypeNavigator</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParaTransJournalTypeNavigator(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _paraTransJournalTypeNavigator, new PropertyChangedEventHandler( OnParaTransJournalTypeNavigatorPropertyChanged ), "ParaTransJournalTypeNavigator", VarioSL.Entities.RelationClasses.StaticParaTransJournalRelations.ParaTransJournalTypeEntityUsingParaTransJournalTypeStatic, true, signalRelatedEntity, "ParaTransJournals", resetFKFields, new int[] { (int)ParaTransJournalFieldIndex.ParaTransJournalType } );		
			_paraTransJournalTypeNavigator = null;
		}
		
		/// <summary> setups the sync logic for member _paraTransJournalTypeNavigator</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParaTransJournalTypeNavigator(IEntityCore relatedEntity)
		{
			if(_paraTransJournalTypeNavigator!=relatedEntity)
			{		
				DesetupSyncParaTransJournalTypeNavigator(true, true);
				_paraTransJournalTypeNavigator = (ParaTransJournalTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _paraTransJournalTypeNavigator, new PropertyChangedEventHandler( OnParaTransJournalTypeNavigatorPropertyChanged ), "ParaTransJournalTypeNavigator", VarioSL.Entities.RelationClasses.StaticParaTransJournalRelations.ParaTransJournalTypeEntityUsingParaTransJournalTypeStatic, true, ref _alreadyFetchedParaTransJournalTypeNavigator, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParaTransJournalTypeNavigatorPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="paraTransJournalID">PK value for ParaTransJournal which data should be fetched into this ParaTransJournal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 paraTransJournalID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ParaTransJournalFieldIndex.ParaTransJournalID].ForcedCurrentValueWrite(paraTransJournalID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateParaTransJournalDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ParaTransJournalEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ParaTransJournalRelations Relations
		{
			get	{ return new ParaTransJournalRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParaTransJournalType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParaTransJournalTypeNavigator
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParaTransJournalTypeCollection(), (IEntityRelation)GetRelationsForField("ParaTransJournalTypeNavigator")[0], (int)VarioSL.Entities.EntityType.ParaTransJournalEntity, (int)VarioSL.Entities.EntityType.ParaTransJournalTypeEntity, 0, null, null, null, "ParaTransJournalTypeNavigator", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceTime property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."DEVICETIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DeviceTime
		{
			get { return (System.DateTime)GetValue((int)ParaTransJournalFieldIndex.DeviceTime, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.DeviceTime, value, true); }
		}

		/// <summary> The ExternalRiderID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."EXTERNALRIDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalRiderID
		{
			get { return (System.String)GetValue((int)ParaTransJournalFieldIndex.ExternalRiderID, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.ExternalRiderID, value, true); }
		}

		/// <summary> The ExternalTripID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."EXTERNALTRIPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalTripID
		{
			get { return (System.String)GetValue((int)ParaTransJournalFieldIndex.ExternalTripID, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.ExternalTripID, value, true); }
		}

		/// <summary> The Faremeter property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."FAREMETER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Faremeter
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParaTransJournalFieldIndex.Faremeter, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.Faremeter, value, true); }
		}

		/// <summary> The FareTransactionID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."FARETRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FareTransactionID
		{
			get { return (System.String)GetValue((int)ParaTransJournalFieldIndex.FareTransactionID, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.FareTransactionID, value, true); }
		}

		/// <summary> The LastModifier property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."LASTMODIFIER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModifier
		{
			get { return (System.DateTime)GetValue((int)ParaTransJournalFieldIndex.LastModifier, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.LastModifier, value, true); }
		}

		/// <summary> The LastUser property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ParaTransJournalFieldIndex.LastUser, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Latitude property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."LATITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 10, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Latitude
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ParaTransJournalFieldIndex.Latitude, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.Latitude, value, true); }
		}

		/// <summary> The Longitude property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."LONGITUDE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 20, 10, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> Longitude
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ParaTransJournalFieldIndex.Longitude, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.Longitude, value, true); }
		}

		/// <summary> The Odometer property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."ODOMETER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Odometer
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParaTransJournalFieldIndex.Odometer, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.Odometer, value, true); }
		}

		/// <summary> The OperatorCompanyID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."OPERATORCOMPANYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OperatorCompanyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParaTransJournalFieldIndex.OperatorCompanyID, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.OperatorCompanyID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."OPERATORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OperatorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParaTransJournalFieldIndex.OperatorID, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The ParaTransJournalID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."PARATRANSJOURNALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ParaTransJournalID
		{
			get { return (System.Int64)GetValue((int)ParaTransJournalFieldIndex.ParaTransJournalID, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.ParaTransJournalID, value, true); }
		}

		/// <summary> The ParaTransJournalType property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."PARATRANSJOURNALTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParaTransJournalType
		{
			get { return (System.Int32)GetValue((int)ParaTransJournalFieldIndex.ParaTransJournalType, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.ParaTransJournalType, value, true); }
		}

		/// <summary> The ParaTransPaymentType property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."PARATRANSPAYMENTTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParaTransPaymentType
		{
			get { return (Nullable<System.Int32>)GetValue((int)ParaTransJournalFieldIndex.ParaTransPaymentType, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.ParaTransPaymentType, value, true); }
		}

		/// <summary> The SharedRideID property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."SHAREDRIDEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SharedRideID
		{
			get { return (System.String)GetValue((int)ParaTransJournalFieldIndex.SharedRideID, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.SharedRideID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ParaTransJournalFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The VehicleNumber property of the Entity ParaTransJournal<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PARATRANSJOURNAL"."VEHICLENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VehicleNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ParaTransJournalFieldIndex.VehicleNumber, false); }
			set	{ SetValue((int)ParaTransJournalFieldIndex.VehicleNumber, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ParaTransJournalTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParaTransJournalTypeNavigator()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ParaTransJournalTypeEntity ParaTransJournalTypeNavigator
		{
			get	{ return GetSingleParaTransJournalTypeNavigator(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParaTransJournalTypeNavigator(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ParaTransJournals", "ParaTransJournalTypeNavigator", _paraTransJournalTypeNavigator, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParaTransJournalTypeNavigator. When set to true, ParaTransJournalTypeNavigator is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParaTransJournalTypeNavigator is accessed. You can always execute a forced fetch by calling GetSingleParaTransJournalTypeNavigator(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParaTransJournalTypeNavigator
		{
			get	{ return _alwaysFetchParaTransJournalTypeNavigator; }
			set	{ _alwaysFetchParaTransJournalTypeNavigator = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParaTransJournalTypeNavigator already has been fetched. Setting this property to false when ParaTransJournalTypeNavigator has been fetched
		/// will set ParaTransJournalTypeNavigator to null as well. Setting this property to true while ParaTransJournalTypeNavigator hasn't been fetched disables lazy loading for ParaTransJournalTypeNavigator</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParaTransJournalTypeNavigator
		{
			get { return _alreadyFetchedParaTransJournalTypeNavigator;}
			set 
			{
				if(_alreadyFetchedParaTransJournalTypeNavigator && !value)
				{
					this.ParaTransJournalTypeNavigator = null;
				}
				_alreadyFetchedParaTransJournalTypeNavigator = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParaTransJournalTypeNavigator is not found
		/// in the database. When set to true, ParaTransJournalTypeNavigator will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ParaTransJournalTypeNavigatorReturnsNewIfNotFound
		{
			get	{ return _paraTransJournalTypeNavigatorReturnsNewIfNotFound; }
			set { _paraTransJournalTypeNavigatorReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ParaTransJournalEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
