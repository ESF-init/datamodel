﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomAttribute'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CustomAttributeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection	_attributeToMobilityProviders;
		private bool	_alwaysFetchAttributeToMobilityProviders, _alreadyFetchedAttributeToMobilityProviders;
		private VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection	_customAttributeValues;
		private bool	_alwaysFetchCustomAttributeValues, _alreadyFetchedCustomAttributeValues;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AttributeToMobilityProviders</summary>
			public static readonly string AttributeToMobilityProviders = "AttributeToMobilityProviders";
			/// <summary>Member name CustomAttributeValues</summary>
			public static readonly string CustomAttributeValues = "CustomAttributeValues";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CustomAttributeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CustomAttributeEntity() :base("CustomAttributeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		public CustomAttributeEntity(System.Int64 customAttributeID):base("CustomAttributeEntity")
		{
			InitClassFetch(customAttributeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomAttributeEntity(System.Int64 customAttributeID, IPrefetchPath prefetchPathToUse):base("CustomAttributeEntity")
		{
			InitClassFetch(customAttributeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="validator">The custom validator object for this CustomAttributeEntity</param>
		public CustomAttributeEntity(System.Int64 customAttributeID, IValidator validator):base("CustomAttributeEntity")
		{
			InitClassFetch(customAttributeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomAttributeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeToMobilityProviders = (VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection)info.GetValue("_attributeToMobilityProviders", typeof(VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection));
			_alwaysFetchAttributeToMobilityProviders = info.GetBoolean("_alwaysFetchAttributeToMobilityProviders");
			_alreadyFetchedAttributeToMobilityProviders = info.GetBoolean("_alreadyFetchedAttributeToMobilityProviders");

			_customAttributeValues = (VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection)info.GetValue("_customAttributeValues", typeof(VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection));
			_alwaysFetchCustomAttributeValues = info.GetBoolean("_alwaysFetchCustomAttributeValues");
			_alreadyFetchedCustomAttributeValues = info.GetBoolean("_alreadyFetchedCustomAttributeValues");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeToMobilityProviders = (_attributeToMobilityProviders.Count > 0);
			_alreadyFetchedCustomAttributeValues = (_customAttributeValues.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AttributeToMobilityProviders":
					toReturn.Add(Relations.AttributeToMobilityProviderEntityUsingAttributeID);
					break;
				case "CustomAttributeValues":
					toReturn.Add(Relations.CustomAttributeValueEntityUsingCustomAttributeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeToMobilityProviders", (!this.MarkedForDeletion?_attributeToMobilityProviders:null));
			info.AddValue("_alwaysFetchAttributeToMobilityProviders", _alwaysFetchAttributeToMobilityProviders);
			info.AddValue("_alreadyFetchedAttributeToMobilityProviders", _alreadyFetchedAttributeToMobilityProviders);
			info.AddValue("_customAttributeValues", (!this.MarkedForDeletion?_customAttributeValues:null));
			info.AddValue("_alwaysFetchCustomAttributeValues", _alwaysFetchCustomAttributeValues);
			info.AddValue("_alreadyFetchedCustomAttributeValues", _alreadyFetchedCustomAttributeValues);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AttributeToMobilityProviders":
					_alreadyFetchedAttributeToMobilityProviders = true;
					if(entity!=null)
					{
						this.AttributeToMobilityProviders.Add((AttributeToMobilityProviderEntity)entity);
					}
					break;
				case "CustomAttributeValues":
					_alreadyFetchedCustomAttributeValues = true;
					if(entity!=null)
					{
						this.CustomAttributeValues.Add((CustomAttributeValueEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AttributeToMobilityProviders":
					_attributeToMobilityProviders.Add((AttributeToMobilityProviderEntity)relatedEntity);
					break;
				case "CustomAttributeValues":
					_customAttributeValues.Add((CustomAttributeValueEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AttributeToMobilityProviders":
					this.PerformRelatedEntityRemoval(_attributeToMobilityProviders, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomAttributeValues":
					this.PerformRelatedEntityRemoval(_customAttributeValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attributeToMobilityProviders);
			toReturn.Add(_customAttributeValues);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeID)
		{
			return FetchUsingPK(customAttributeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(customAttributeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(customAttributeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 customAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(customAttributeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CustomAttributeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CustomAttributeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeToMobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, _attributeToMobilityProviders.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeToMobilityProviderEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, _attributeToMobilityProviders.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeToMobilityProviders(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection GetMultiAttributeToMobilityProviders(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeToMobilityProviders || forceFetch || _alwaysFetchAttributeToMobilityProviders) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeToMobilityProviders);
				_attributeToMobilityProviders.SuppressClearInGetMulti=!forceFetch;
				_attributeToMobilityProviders.EntityFactoryToUse = entityFactoryToUse;
				_attributeToMobilityProviders.GetMultiManyToOne(this, null, filter);
				_attributeToMobilityProviders.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeToMobilityProviders = true;
			}
			return _attributeToMobilityProviders;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeToMobilityProviders'. These settings will be taken into account
		/// when the property AttributeToMobilityProviders is requested or GetMultiAttributeToMobilityProviders is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeToMobilityProviders(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeToMobilityProviders.SortClauses=sortClauses;
			_attributeToMobilityProviders.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection GetMultiCustomAttributeValues(bool forceFetch)
		{
			return GetMultiCustomAttributeValues(forceFetch, _customAttributeValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomAttributeValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection GetMultiCustomAttributeValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomAttributeValues(forceFetch, _customAttributeValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection GetMultiCustomAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomAttributeValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomAttributeValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection GetMultiCustomAttributeValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomAttributeValues || forceFetch || _alwaysFetchCustomAttributeValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customAttributeValues);
				_customAttributeValues.SuppressClearInGetMulti=!forceFetch;
				_customAttributeValues.EntityFactoryToUse = entityFactoryToUse;
				_customAttributeValues.GetMultiManyToOne(this, filter);
				_customAttributeValues.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomAttributeValues = true;
			}
			return _customAttributeValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomAttributeValues'. These settings will be taken into account
		/// when the property CustomAttributeValues is requested or GetMultiCustomAttributeValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomAttributeValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customAttributeValues.SortClauses=sortClauses;
			_customAttributeValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AttributeToMobilityProviders", _attributeToMobilityProviders);
			toReturn.Add("CustomAttributeValues", _customAttributeValues);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="validator">The validator object for this CustomAttributeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 customAttributeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(customAttributeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attributeToMobilityProviders = new VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection();
			_attributeToMobilityProviders.SetContainingEntityInfo(this, "CustomAttribute");

			_customAttributeValues = new VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection();
			_customAttributeValues.SetContainingEntityInfo(this, "CustomAttribute");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomAttributeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsMultiValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="customAttributeID">PK value for CustomAttribute which data should be fetched into this CustomAttribute object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 customAttributeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CustomAttributeFieldIndex.CustomAttributeID].ForcedCurrentValueWrite(customAttributeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomAttributeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CustomAttributeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CustomAttributeRelations Relations
		{
			get	{ return new CustomAttributeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeToMobilityProvider' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeToMobilityProviders
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection(), (IEntityRelation)GetRelationsForField("AttributeToMobilityProviders")[0], (int)VarioSL.Entities.EntityType.CustomAttributeEntity, (int)VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity, 0, null, null, null, "AttributeToMobilityProviders", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomAttributeValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomAttributeValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection(), (IEntityRelation)GetRelationsForField("CustomAttributeValues")[0], (int)VarioSL.Entities.EntityType.CustomAttributeEntity, (int)VarioSL.Entities.EntityType.CustomAttributeValueEntity, 0, null, null, null, "CustomAttributeValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CustomAttributeClassID property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."ATTRIBUTECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.CustomAttributeClass CustomAttributeClassID
		{
			get { return (VarioSL.Entities.Enumerations.CustomAttributeClass)GetValue((int)CustomAttributeFieldIndex.CustomAttributeClassID, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.CustomAttributeClassID, value, true); }
		}

		/// <summary> The CustomAttributeID property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CustomAttributeID
		{
			get { return (System.Int64)GetValue((int)CustomAttributeFieldIndex.CustomAttributeID, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.CustomAttributeID, value, true); }
		}

		/// <summary> The CustomAttributeName property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."ATTRIBUTENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CustomAttributeName
		{
			get { return (System.String)GetValue((int)CustomAttributeFieldIndex.CustomAttributeName, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.CustomAttributeName, value, true); }
		}

		/// <summary> The Description property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)CustomAttributeFieldIndex.Description, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.Description, value, true); }
		}

		/// <summary> The IsMultiValue property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."ISMULTIVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsMultiValue
		{
			get { return (System.Boolean)GetValue((int)CustomAttributeFieldIndex.IsMultiValue, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.IsMultiValue, value, true); }
		}

		/// <summary> The LastModified property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModified
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CustomAttributeFieldIndex.LastModified, false); }
			set	{ SetValue((int)CustomAttributeFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)CustomAttributeFieldIndex.LastUser, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity CustomAttribute<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_ATTRIBUTE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)CustomAttributeFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)CustomAttributeFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttributeToMobilityProviderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeToMobilityProviders()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeToMobilityProviderCollection AttributeToMobilityProviders
		{
			get	{ return GetMultiAttributeToMobilityProviders(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeToMobilityProviders. When set to true, AttributeToMobilityProviders is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeToMobilityProviders is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeToMobilityProviders(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeToMobilityProviders
		{
			get	{ return _alwaysFetchAttributeToMobilityProviders; }
			set	{ _alwaysFetchAttributeToMobilityProviders = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeToMobilityProviders already has been fetched. Setting this property to false when AttributeToMobilityProviders has been fetched
		/// will clear the AttributeToMobilityProviders collection well. Setting this property to true while AttributeToMobilityProviders hasn't been fetched disables lazy loading for AttributeToMobilityProviders</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeToMobilityProviders
		{
			get { return _alreadyFetchedAttributeToMobilityProviders;}
			set 
			{
				if(_alreadyFetchedAttributeToMobilityProviders && !value && (_attributeToMobilityProviders != null))
				{
					_attributeToMobilityProviders.Clear();
				}
				_alreadyFetchedAttributeToMobilityProviders = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomAttributeValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomAttributeValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.CustomAttributeValueCollection CustomAttributeValues
		{
			get	{ return GetMultiCustomAttributeValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomAttributeValues. When set to true, CustomAttributeValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomAttributeValues is accessed. You can always execute/ a forced fetch by calling GetMultiCustomAttributeValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomAttributeValues
		{
			get	{ return _alwaysFetchCustomAttributeValues; }
			set	{ _alwaysFetchCustomAttributeValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomAttributeValues already has been fetched. Setting this property to false when CustomAttributeValues has been fetched
		/// will clear the CustomAttributeValues collection well. Setting this property to true while CustomAttributeValues hasn't been fetched disables lazy loading for CustomAttributeValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomAttributeValues
		{
			get { return _alreadyFetchedCustomAttributeValues;}
			set 
			{
				if(_alreadyFetchedCustomAttributeValues && !value && (_customAttributeValues != null))
				{
					_customAttributeValues.Clear();
				}
				_alreadyFetchedCustomAttributeValues = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CustomAttributeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
