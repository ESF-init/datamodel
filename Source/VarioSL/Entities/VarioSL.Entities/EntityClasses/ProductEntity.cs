﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Product'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ProductEntity : CommonEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AutoloadSettingCollection	_autoloadSettings;
		private bool	_alwaysFetchAutoloadSettings, _alreadyFetchedAutoloadSettings;
		private VarioSL.Entities.CollectionClasses.DunningToProductCollection	_dunningToProducts;
		private bool	_alwaysFetchDunningToProducts, _alreadyFetchedDunningToProducts;
		private VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection	_emailEventResendLocks;
		private bool	_alwaysFetchEmailEventResendLocks, _alreadyFetchedEmailEventResendLocks;
		private VarioSL.Entities.CollectionClasses.EntitlementToProductCollection	_entitlementToProducts;
		private bool	_alwaysFetchEntitlementToProducts, _alreadyFetchedEntitlementToProducts;
		private VarioSL.Entities.CollectionClasses.OrderDetailCollection	_orderDetails;
		private bool	_alwaysFetchOrderDetails, _alreadyFetchedOrderDetails;
		private VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection	_passExpiryNotifications;
		private bool	_alwaysFetchPassExpiryNotifications, _alreadyFetchedPassExpiryNotifications;
		private VarioSL.Entities.CollectionClasses.PostingCollection	_postings;
		private bool	_alwaysFetchPostings, _alreadyFetchedPostings;
		private VarioSL.Entities.CollectionClasses.ProductCollection	_product;
		private bool	_alwaysFetchProduct, _alreadyFetchedProduct;
		private VarioSL.Entities.CollectionClasses.ProductTerminationCollection	_productTerminations;
		private bool	_alwaysFetchProductTerminations, _alreadyFetchedProductTerminations;
		private VarioSL.Entities.CollectionClasses.RuleViolationCollection	_ruleViolations;
		private bool	_alwaysFetchRuleViolations, _alreadyFetchedRuleViolations;
		private VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection	_ticketSerialNumbers;
		private bool	_alwaysFetchTicketSerialNumbers, _alreadyFetchedTicketSerialNumbers;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournalsForAppledPass;
		private bool	_alwaysFetchTransactionJournalsForAppledPass, _alreadyFetchedTransactionJournalsForAppledPass;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.DunningProcessCollection _dunningProcesses;
		private bool	_alwaysFetchDunningProcesses, _alreadyFetchedDunningProcesses;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private PointOfSaleEntity _pointOfSale;
		private bool	_alwaysFetchPointOfSale, _alreadyFetchedPointOfSale, _pointOfSaleReturnsNewIfNotFound;
		private TicketEntity _taxTicket;
		private bool	_alwaysFetchTaxTicket, _alreadyFetchedTaxTicket, _taxTicketReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private CardEntity _card;
		private bool	_alwaysFetchCard, _alreadyFetchedCard, _cardReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private OrganizationEntity _organization;
		private bool	_alwaysFetchOrganization, _alreadyFetchedOrganization, _organizationReturnsNewIfNotFound;
		private ProductEntity _referencedProduct;
		private bool	_alwaysFetchReferencedProduct, _alreadyFetchedReferencedProduct, _referencedProductReturnsNewIfNotFound;
		private RefundEntity _refund;
		private bool	_alwaysFetchRefund, _alreadyFetchedRefund, _refundReturnsNewIfNotFound;
		private VoucherEntity _voucher;
		private bool	_alwaysFetchVoucher, _alreadyFetchedVoucher, _voucherReturnsNewIfNotFound;
		private ProductRelationEntity _productRelation;
		private bool	_alwaysFetchProductRelation, _alreadyFetchedProductRelation, _productRelationReturnsNewIfNotFound;
		private SubsidyTransactionEntity _subsidyTransaction;
		private bool	_alwaysFetchSubsidyTransaction, _alreadyFetchedSubsidyTransaction, _subsidyTransactionReturnsNewIfNotFound;

        // __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name PointOfSale</summary>
			public static readonly string PointOfSale = "PointOfSale";
			/// <summary>Member name TaxTicket</summary>
			public static readonly string TaxTicket = "TaxTicket";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name Card</summary>
			public static readonly string Card = "Card";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name Organization</summary>
			public static readonly string Organization = "Organization";
			/// <summary>Member name ReferencedProduct</summary>
			public static readonly string ReferencedProduct = "ReferencedProduct";
			/// <summary>Member name Refund</summary>
			public static readonly string Refund = "Refund";
			/// <summary>Member name Voucher</summary>
			public static readonly string Voucher = "Voucher";
			/// <summary>Member name AutoloadSettings</summary>
			public static readonly string AutoloadSettings = "AutoloadSettings";
			/// <summary>Member name DunningToProducts</summary>
			public static readonly string DunningToProducts = "DunningToProducts";
			/// <summary>Member name EmailEventResendLocks</summary>
			public static readonly string EmailEventResendLocks = "EmailEventResendLocks";
			/// <summary>Member name EntitlementToProducts</summary>
			public static readonly string EntitlementToProducts = "EntitlementToProducts";
			/// <summary>Member name OrderDetails</summary>
			public static readonly string OrderDetails = "OrderDetails";
			/// <summary>Member name PassExpiryNotifications</summary>
			public static readonly string PassExpiryNotifications = "PassExpiryNotifications";
			/// <summary>Member name Postings</summary>
			public static readonly string Postings = "Postings";
			/// <summary>Member name Product</summary>
			public static readonly string Product = "Product";
			/// <summary>Member name ProductTerminations</summary>
			public static readonly string ProductTerminations = "ProductTerminations";
			/// <summary>Member name RuleViolations</summary>
			public static readonly string RuleViolations = "RuleViolations";
			/// <summary>Member name TicketSerialNumbers</summary>
			public static readonly string TicketSerialNumbers = "TicketSerialNumbers";
			/// <summary>Member name TransactionJournalsForAppledPass</summary>
			public static readonly string TransactionJournalsForAppledPass = "TransactionJournalsForAppledPass";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name DunningProcesses</summary>
			public static readonly string DunningProcesses = "DunningProcesses";
			/// <summary>Member name ProductRelation</summary>
			public static readonly string ProductRelation = "ProductRelation";
			/// <summary>Member name SubsidyTransaction</summary>
			public static readonly string SubsidyTransaction = "SubsidyTransaction";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ProductEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ProductEntity() :base("ProductEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		public ProductEntity(System.Int64 productID):base("ProductEntity")
		{
			InitClassFetch(productID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductEntity(System.Int64 productID, IPrefetchPath prefetchPathToUse):base("ProductEntity")
		{
			InitClassFetch(productID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="validator">The custom validator object for this ProductEntity</param>
		public ProductEntity(System.Int64 productID, IValidator validator):base("ProductEntity")
		{
			InitClassFetch(productID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_autoloadSettings = (VarioSL.Entities.CollectionClasses.AutoloadSettingCollection)info.GetValue("_autoloadSettings", typeof(VarioSL.Entities.CollectionClasses.AutoloadSettingCollection));
			_alwaysFetchAutoloadSettings = info.GetBoolean("_alwaysFetchAutoloadSettings");
			_alreadyFetchedAutoloadSettings = info.GetBoolean("_alreadyFetchedAutoloadSettings");

			_dunningToProducts = (VarioSL.Entities.CollectionClasses.DunningToProductCollection)info.GetValue("_dunningToProducts", typeof(VarioSL.Entities.CollectionClasses.DunningToProductCollection));
			_alwaysFetchDunningToProducts = info.GetBoolean("_alwaysFetchDunningToProducts");
			_alreadyFetchedDunningToProducts = info.GetBoolean("_alreadyFetchedDunningToProducts");

			_emailEventResendLocks = (VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection)info.GetValue("_emailEventResendLocks", typeof(VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection));
			_alwaysFetchEmailEventResendLocks = info.GetBoolean("_alwaysFetchEmailEventResendLocks");
			_alreadyFetchedEmailEventResendLocks = info.GetBoolean("_alreadyFetchedEmailEventResendLocks");

			_entitlementToProducts = (VarioSL.Entities.CollectionClasses.EntitlementToProductCollection)info.GetValue("_entitlementToProducts", typeof(VarioSL.Entities.CollectionClasses.EntitlementToProductCollection));
			_alwaysFetchEntitlementToProducts = info.GetBoolean("_alwaysFetchEntitlementToProducts");
			_alreadyFetchedEntitlementToProducts = info.GetBoolean("_alreadyFetchedEntitlementToProducts");

			_orderDetails = (VarioSL.Entities.CollectionClasses.OrderDetailCollection)info.GetValue("_orderDetails", typeof(VarioSL.Entities.CollectionClasses.OrderDetailCollection));
			_alwaysFetchOrderDetails = info.GetBoolean("_alwaysFetchOrderDetails");
			_alreadyFetchedOrderDetails = info.GetBoolean("_alreadyFetchedOrderDetails");

			_passExpiryNotifications = (VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection)info.GetValue("_passExpiryNotifications", typeof(VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection));
			_alwaysFetchPassExpiryNotifications = info.GetBoolean("_alwaysFetchPassExpiryNotifications");
			_alreadyFetchedPassExpiryNotifications = info.GetBoolean("_alreadyFetchedPassExpiryNotifications");

			_postings = (VarioSL.Entities.CollectionClasses.PostingCollection)info.GetValue("_postings", typeof(VarioSL.Entities.CollectionClasses.PostingCollection));
			_alwaysFetchPostings = info.GetBoolean("_alwaysFetchPostings");
			_alreadyFetchedPostings = info.GetBoolean("_alreadyFetchedPostings");

			_product = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_product", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProduct = info.GetBoolean("_alwaysFetchProduct");
			_alreadyFetchedProduct = info.GetBoolean("_alreadyFetchedProduct");

			_productTerminations = (VarioSL.Entities.CollectionClasses.ProductTerminationCollection)info.GetValue("_productTerminations", typeof(VarioSL.Entities.CollectionClasses.ProductTerminationCollection));
			_alwaysFetchProductTerminations = info.GetBoolean("_alwaysFetchProductTerminations");
			_alreadyFetchedProductTerminations = info.GetBoolean("_alreadyFetchedProductTerminations");

			_ruleViolations = (VarioSL.Entities.CollectionClasses.RuleViolationCollection)info.GetValue("_ruleViolations", typeof(VarioSL.Entities.CollectionClasses.RuleViolationCollection));
			_alwaysFetchRuleViolations = info.GetBoolean("_alwaysFetchRuleViolations");
			_alreadyFetchedRuleViolations = info.GetBoolean("_alreadyFetchedRuleViolations");

			_ticketSerialNumbers = (VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection)info.GetValue("_ticketSerialNumbers", typeof(VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection));
			_alwaysFetchTicketSerialNumbers = info.GetBoolean("_alwaysFetchTicketSerialNumbers");
			_alreadyFetchedTicketSerialNumbers = info.GetBoolean("_alreadyFetchedTicketSerialNumbers");

			_transactionJournalsForAppledPass = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournalsForAppledPass", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournalsForAppledPass = info.GetBoolean("_alwaysFetchTransactionJournalsForAppledPass");
			_alreadyFetchedTransactionJournalsForAppledPass = info.GetBoolean("_alreadyFetchedTransactionJournalsForAppledPass");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");
			_dunningProcesses = (VarioSL.Entities.CollectionClasses.DunningProcessCollection)info.GetValue("_dunningProcesses", typeof(VarioSL.Entities.CollectionClasses.DunningProcessCollection));
			_alwaysFetchDunningProcesses = info.GetBoolean("_alwaysFetchDunningProcesses");
			_alreadyFetchedDunningProcesses = info.GetBoolean("_alreadyFetchedDunningProcesses");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_pointOfSale = (PointOfSaleEntity)info.GetValue("_pointOfSale", typeof(PointOfSaleEntity));
			if(_pointOfSale!=null)
			{
				_pointOfSale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pointOfSaleReturnsNewIfNotFound = info.GetBoolean("_pointOfSaleReturnsNewIfNotFound");
			_alwaysFetchPointOfSale = info.GetBoolean("_alwaysFetchPointOfSale");
			_alreadyFetchedPointOfSale = info.GetBoolean("_alreadyFetchedPointOfSale");

			_taxTicket = (TicketEntity)info.GetValue("_taxTicket", typeof(TicketEntity));
			if(_taxTicket!=null)
			{
				_taxTicket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_taxTicketReturnsNewIfNotFound = info.GetBoolean("_taxTicketReturnsNewIfNotFound");
			_alwaysFetchTaxTicket = info.GetBoolean("_alwaysFetchTaxTicket");
			_alreadyFetchedTaxTicket = info.GetBoolean("_alreadyFetchedTaxTicket");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_card = (CardEntity)info.GetValue("_card", typeof(CardEntity));
			if(_card!=null)
			{
				_card.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_cardReturnsNewIfNotFound = info.GetBoolean("_cardReturnsNewIfNotFound");
			_alwaysFetchCard = info.GetBoolean("_alwaysFetchCard");
			_alreadyFetchedCard = info.GetBoolean("_alreadyFetchedCard");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_organization = (OrganizationEntity)info.GetValue("_organization", typeof(OrganizationEntity));
			if(_organization!=null)
			{
				_organization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_organizationReturnsNewIfNotFound = info.GetBoolean("_organizationReturnsNewIfNotFound");
			_alwaysFetchOrganization = info.GetBoolean("_alwaysFetchOrganization");
			_alreadyFetchedOrganization = info.GetBoolean("_alreadyFetchedOrganization");

			_referencedProduct = (ProductEntity)info.GetValue("_referencedProduct", typeof(ProductEntity));
			if(_referencedProduct!=null)
			{
				_referencedProduct.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_referencedProductReturnsNewIfNotFound = info.GetBoolean("_referencedProductReturnsNewIfNotFound");
			_alwaysFetchReferencedProduct = info.GetBoolean("_alwaysFetchReferencedProduct");
			_alreadyFetchedReferencedProduct = info.GetBoolean("_alreadyFetchedReferencedProduct");

			_refund = (RefundEntity)info.GetValue("_refund", typeof(RefundEntity));
			if(_refund!=null)
			{
				_refund.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_refundReturnsNewIfNotFound = info.GetBoolean("_refundReturnsNewIfNotFound");
			_alwaysFetchRefund = info.GetBoolean("_alwaysFetchRefund");
			_alreadyFetchedRefund = info.GetBoolean("_alreadyFetchedRefund");

			_voucher = (VoucherEntity)info.GetValue("_voucher", typeof(VoucherEntity));
			if(_voucher!=null)
			{
				_voucher.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_voucherReturnsNewIfNotFound = info.GetBoolean("_voucherReturnsNewIfNotFound");
			_alwaysFetchVoucher = info.GetBoolean("_alwaysFetchVoucher");
			_alreadyFetchedVoucher = info.GetBoolean("_alreadyFetchedVoucher");
			_productRelation = (ProductRelationEntity)info.GetValue("_productRelation", typeof(ProductRelationEntity));
			if(_productRelation!=null)
			{
				_productRelation.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productRelationReturnsNewIfNotFound = info.GetBoolean("_productRelationReturnsNewIfNotFound");
			_alwaysFetchProductRelation = info.GetBoolean("_alwaysFetchProductRelation");
			_alreadyFetchedProductRelation = info.GetBoolean("_alreadyFetchedProductRelation");

			_subsidyTransaction = (SubsidyTransactionEntity)info.GetValue("_subsidyTransaction", typeof(SubsidyTransactionEntity));
			if(_subsidyTransaction!=null)
			{
				_subsidyTransaction.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_subsidyTransactionReturnsNewIfNotFound = info.GetBoolean("_subsidyTransactionReturnsNewIfNotFound");
			_alwaysFetchSubsidyTransaction = info.GetBoolean("_alwaysFetchSubsidyTransaction");
			_alreadyFetchedSubsidyTransaction = info.GetBoolean("_alreadyFetchedSubsidyTransaction");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ProductFieldIndex)fieldIndex)
			{
				case ProductFieldIndex.CardID:
					DesetupSyncCard(true, false);
					_alreadyFetchedCard = false;
					break;
				case ProductFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case ProductFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case ProductFieldIndex.OrganizationID:
					DesetupSyncOrganization(true, false);
					_alreadyFetchedOrganization = false;
					break;
				case ProductFieldIndex.PointOfSaleID:
					DesetupSyncPointOfSale(true, false);
					_alreadyFetchedPointOfSale = false;
					break;
				case ProductFieldIndex.ReferencedProductID:
					DesetupSyncReferencedProduct(true, false);
					_alreadyFetchedReferencedProduct = false;
					break;
				case ProductFieldIndex.RefundID:
					DesetupSyncRefund(true, false);
					_alreadyFetchedRefund = false;
					break;
				case ProductFieldIndex.TaxTicketID:
					DesetupSyncTaxTicket(true, false);
					_alreadyFetchedTaxTicket = false;
					break;
				case ProductFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				case ProductFieldIndex.VoucherID:
					DesetupSyncVoucher(true, false);
					_alreadyFetchedVoucher = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAutoloadSettings = (_autoloadSettings.Count > 0);
			_alreadyFetchedDunningToProducts = (_dunningToProducts.Count > 0);
			_alreadyFetchedEmailEventResendLocks = (_emailEventResendLocks.Count > 0);
			_alreadyFetchedEntitlementToProducts = (_entitlementToProducts.Count > 0);
			_alreadyFetchedOrderDetails = (_orderDetails.Count > 0);
			_alreadyFetchedPassExpiryNotifications = (_passExpiryNotifications.Count > 0);
			_alreadyFetchedPostings = (_postings.Count > 0);
			_alreadyFetchedProduct = (_product.Count > 0);
			_alreadyFetchedProductTerminations = (_productTerminations.Count > 0);
			_alreadyFetchedRuleViolations = (_ruleViolations.Count > 0);
			_alreadyFetchedTicketSerialNumbers = (_ticketSerialNumbers.Count > 0);
			_alreadyFetchedTransactionJournalsForAppledPass = (_transactionJournalsForAppledPass.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedDunningProcesses = (_dunningProcesses.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedPointOfSale = (_pointOfSale != null);
			_alreadyFetchedTaxTicket = (_taxTicket != null);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedCard = (_card != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedOrganization = (_organization != null);
			_alreadyFetchedReferencedProduct = (_referencedProduct != null);
			_alreadyFetchedRefund = (_refund != null);
			_alreadyFetchedVoucher = (_voucher != null);
			_alreadyFetchedProductRelation = (_productRelation != null);
			_alreadyFetchedSubsidyTransaction = (_subsidyTransaction != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "PointOfSale":
					toReturn.Add(Relations.PointOfSaleEntityUsingPointOfSaleID);
					break;
				case "TaxTicket":
					toReturn.Add(Relations.TicketEntityUsingTaxTicketID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "Card":
					toReturn.Add(Relations.CardEntityUsingCardID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "Organization":
					toReturn.Add(Relations.OrganizationEntityUsingOrganizationID);
					break;
				case "ReferencedProduct":
					toReturn.Add(Relations.ProductEntityUsingProductIDReferencedProductID);
					break;
				case "Refund":
					toReturn.Add(Relations.RefundEntityUsingRefundID);
					break;
				case "Voucher":
					toReturn.Add(Relations.VoucherEntityUsingVoucherID);
					break;
				case "AutoloadSettings":
					toReturn.Add(Relations.AutoloadSettingEntityUsingProductID);
					break;
				case "DunningToProducts":
					toReturn.Add(Relations.DunningToProductEntityUsingProductID);
					break;
				case "EmailEventResendLocks":
					toReturn.Add(Relations.EmailEventResendLockEntityUsingProductID);
					break;
				case "EntitlementToProducts":
					toReturn.Add(Relations.EntitlementToProductEntityUsingProductID);
					break;
				case "OrderDetails":
					toReturn.Add(Relations.OrderDetailEntityUsingProductID);
					break;
				case "PassExpiryNotifications":
					toReturn.Add(Relations.PassExpiryNotificationEntityUsingProductID);
					break;
				case "Postings":
					toReturn.Add(Relations.PostingEntityUsingProductID);
					break;
				case "Product":
					toReturn.Add(Relations.ProductEntityUsingReferencedProductID);
					break;
				case "ProductTerminations":
					toReturn.Add(Relations.ProductTerminationEntityUsingProductID);
					break;
				case "RuleViolations":
					toReturn.Add(Relations.RuleViolationEntityUsingProductID);
					break;
				case "TicketSerialNumbers":
					toReturn.Add(Relations.TicketSerialNumberEntityUsingProductID);
					break;
				case "TransactionJournalsForAppledPass":
					toReturn.Add(Relations.TransactionJournalEntityUsingAppliedPassProductID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingProductID);
					break;
				case "DunningProcesses":
					toReturn.Add(Relations.DunningToProductEntityUsingProductID, "ProductEntity__", "DunningToProduct_", JoinHint.None);
					toReturn.Add(DunningToProductEntity.Relations.DunningProcessEntityUsingDunningProcessID, "DunningToProduct_", string.Empty, JoinHint.None);
					break;
				case "ProductRelation":
					toReturn.Add(Relations.ProductRelationEntityUsingProductID);
					break;
				case "SubsidyTransaction":
					toReturn.Add(Relations.SubsidyTransactionEntityUsingProductID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_autoloadSettings", (!this.MarkedForDeletion?_autoloadSettings:null));
			info.AddValue("_alwaysFetchAutoloadSettings", _alwaysFetchAutoloadSettings);
			info.AddValue("_alreadyFetchedAutoloadSettings", _alreadyFetchedAutoloadSettings);
			info.AddValue("_dunningToProducts", (!this.MarkedForDeletion?_dunningToProducts:null));
			info.AddValue("_alwaysFetchDunningToProducts", _alwaysFetchDunningToProducts);
			info.AddValue("_alreadyFetchedDunningToProducts", _alreadyFetchedDunningToProducts);
			info.AddValue("_emailEventResendLocks", (!this.MarkedForDeletion?_emailEventResendLocks:null));
			info.AddValue("_alwaysFetchEmailEventResendLocks", _alwaysFetchEmailEventResendLocks);
			info.AddValue("_alreadyFetchedEmailEventResendLocks", _alreadyFetchedEmailEventResendLocks);
			info.AddValue("_entitlementToProducts", (!this.MarkedForDeletion?_entitlementToProducts:null));
			info.AddValue("_alwaysFetchEntitlementToProducts", _alwaysFetchEntitlementToProducts);
			info.AddValue("_alreadyFetchedEntitlementToProducts", _alreadyFetchedEntitlementToProducts);
			info.AddValue("_orderDetails", (!this.MarkedForDeletion?_orderDetails:null));
			info.AddValue("_alwaysFetchOrderDetails", _alwaysFetchOrderDetails);
			info.AddValue("_alreadyFetchedOrderDetails", _alreadyFetchedOrderDetails);
			info.AddValue("_passExpiryNotifications", (!this.MarkedForDeletion?_passExpiryNotifications:null));
			info.AddValue("_alwaysFetchPassExpiryNotifications", _alwaysFetchPassExpiryNotifications);
			info.AddValue("_alreadyFetchedPassExpiryNotifications", _alreadyFetchedPassExpiryNotifications);
			info.AddValue("_postings", (!this.MarkedForDeletion?_postings:null));
			info.AddValue("_alwaysFetchPostings", _alwaysFetchPostings);
			info.AddValue("_alreadyFetchedPostings", _alreadyFetchedPostings);
			info.AddValue("_product", (!this.MarkedForDeletion?_product:null));
			info.AddValue("_alwaysFetchProduct", _alwaysFetchProduct);
			info.AddValue("_alreadyFetchedProduct", _alreadyFetchedProduct);
			info.AddValue("_productTerminations", (!this.MarkedForDeletion?_productTerminations:null));
			info.AddValue("_alwaysFetchProductTerminations", _alwaysFetchProductTerminations);
			info.AddValue("_alreadyFetchedProductTerminations", _alreadyFetchedProductTerminations);
			info.AddValue("_ruleViolations", (!this.MarkedForDeletion?_ruleViolations:null));
			info.AddValue("_alwaysFetchRuleViolations", _alwaysFetchRuleViolations);
			info.AddValue("_alreadyFetchedRuleViolations", _alreadyFetchedRuleViolations);
			info.AddValue("_ticketSerialNumbers", (!this.MarkedForDeletion?_ticketSerialNumbers:null));
			info.AddValue("_alwaysFetchTicketSerialNumbers", _alwaysFetchTicketSerialNumbers);
			info.AddValue("_alreadyFetchedTicketSerialNumbers", _alreadyFetchedTicketSerialNumbers);
			info.AddValue("_transactionJournalsForAppledPass", (!this.MarkedForDeletion?_transactionJournalsForAppledPass:null));
			info.AddValue("_alwaysFetchTransactionJournalsForAppledPass", _alwaysFetchTransactionJournalsForAppledPass);
			info.AddValue("_alreadyFetchedTransactionJournalsForAppledPass", _alreadyFetchedTransactionJournalsForAppledPass);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_dunningProcesses", (!this.MarkedForDeletion?_dunningProcesses:null));
			info.AddValue("_alwaysFetchDunningProcesses", _alwaysFetchDunningProcesses);
			info.AddValue("_alreadyFetchedDunningProcesses", _alreadyFetchedDunningProcesses);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_pointOfSale", (!this.MarkedForDeletion?_pointOfSale:null));
			info.AddValue("_pointOfSaleReturnsNewIfNotFound", _pointOfSaleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPointOfSale", _alwaysFetchPointOfSale);
			info.AddValue("_alreadyFetchedPointOfSale", _alreadyFetchedPointOfSale);
			info.AddValue("_taxTicket", (!this.MarkedForDeletion?_taxTicket:null));
			info.AddValue("_taxTicketReturnsNewIfNotFound", _taxTicketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTaxTicket", _alwaysFetchTaxTicket);
			info.AddValue("_alreadyFetchedTaxTicket", _alreadyFetchedTaxTicket);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_card", (!this.MarkedForDeletion?_card:null));
			info.AddValue("_cardReturnsNewIfNotFound", _cardReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCard", _alwaysFetchCard);
			info.AddValue("_alreadyFetchedCard", _alreadyFetchedCard);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_organization", (!this.MarkedForDeletion?_organization:null));
			info.AddValue("_organizationReturnsNewIfNotFound", _organizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrganization", _alwaysFetchOrganization);
			info.AddValue("_alreadyFetchedOrganization", _alreadyFetchedOrganization);
			info.AddValue("_referencedProduct", (!this.MarkedForDeletion?_referencedProduct:null));
			info.AddValue("_referencedProductReturnsNewIfNotFound", _referencedProductReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReferencedProduct", _alwaysFetchReferencedProduct);
			info.AddValue("_alreadyFetchedReferencedProduct", _alreadyFetchedReferencedProduct);
			info.AddValue("_refund", (!this.MarkedForDeletion?_refund:null));
			info.AddValue("_refundReturnsNewIfNotFound", _refundReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRefund", _alwaysFetchRefund);
			info.AddValue("_alreadyFetchedRefund", _alreadyFetchedRefund);
			info.AddValue("_voucher", (!this.MarkedForDeletion?_voucher:null));
			info.AddValue("_voucherReturnsNewIfNotFound", _voucherReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVoucher", _alwaysFetchVoucher);
			info.AddValue("_alreadyFetchedVoucher", _alreadyFetchedVoucher);

			info.AddValue("_productRelation", (!this.MarkedForDeletion?_productRelation:null));
			info.AddValue("_productRelationReturnsNewIfNotFound", _productRelationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductRelation", _alwaysFetchProductRelation);
			info.AddValue("_alreadyFetchedProductRelation", _alreadyFetchedProductRelation);

			info.AddValue("_subsidyTransaction", (!this.MarkedForDeletion?_subsidyTransaction:null));
			info.AddValue("_subsidyTransactionReturnsNewIfNotFound", _subsidyTransactionReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSubsidyTransaction", _alwaysFetchSubsidyTransaction);
			info.AddValue("_alreadyFetchedSubsidyTransaction", _alreadyFetchedSubsidyTransaction);

            // __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
            // __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "PointOfSale":
					_alreadyFetchedPointOfSale = true;
					this.PointOfSale = (PointOfSaleEntity)entity;
					break;
				case "TaxTicket":
					_alreadyFetchedTaxTicket = true;
					this.TaxTicket = (TicketEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "Card":
					_alreadyFetchedCard = true;
					this.Card = (CardEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "Organization":
					_alreadyFetchedOrganization = true;
					this.Organization = (OrganizationEntity)entity;
					break;
				case "ReferencedProduct":
					_alreadyFetchedReferencedProduct = true;
					this.ReferencedProduct = (ProductEntity)entity;
					break;
				case "Refund":
					_alreadyFetchedRefund = true;
					this.Refund = (RefundEntity)entity;
					break;
				case "Voucher":
					_alreadyFetchedVoucher = true;
					this.Voucher = (VoucherEntity)entity;
					break;
				case "AutoloadSettings":
					_alreadyFetchedAutoloadSettings = true;
					if(entity!=null)
					{
						this.AutoloadSettings.Add((AutoloadSettingEntity)entity);
					}
					break;
				case "DunningToProducts":
					_alreadyFetchedDunningToProducts = true;
					if(entity!=null)
					{
						this.DunningToProducts.Add((DunningToProductEntity)entity);
					}
					break;
				case "EmailEventResendLocks":
					_alreadyFetchedEmailEventResendLocks = true;
					if(entity!=null)
					{
						this.EmailEventResendLocks.Add((EmailEventResendLockEntity)entity);
					}
					break;
				case "EntitlementToProducts":
					_alreadyFetchedEntitlementToProducts = true;
					if(entity!=null)
					{
						this.EntitlementToProducts.Add((EntitlementToProductEntity)entity);
					}
					break;
				case "OrderDetails":
					_alreadyFetchedOrderDetails = true;
					if(entity!=null)
					{
						this.OrderDetails.Add((OrderDetailEntity)entity);
					}
					break;
				case "PassExpiryNotifications":
					_alreadyFetchedPassExpiryNotifications = true;
					if(entity!=null)
					{
						this.PassExpiryNotifications.Add((PassExpiryNotificationEntity)entity);
					}
					break;
				case "Postings":
					_alreadyFetchedPostings = true;
					if(entity!=null)
					{
						this.Postings.Add((PostingEntity)entity);
					}
					break;
				case "Product":
					_alreadyFetchedProduct = true;
					if(entity!=null)
					{
						this.Product.Add((ProductEntity)entity);
					}
					break;
				case "ProductTerminations":
					_alreadyFetchedProductTerminations = true;
					if(entity!=null)
					{
						this.ProductTerminations.Add((ProductTerminationEntity)entity);
					}
					break;
				case "RuleViolations":
					_alreadyFetchedRuleViolations = true;
					if(entity!=null)
					{
						this.RuleViolations.Add((RuleViolationEntity)entity);
					}
					break;
				case "TicketSerialNumbers":
					_alreadyFetchedTicketSerialNumbers = true;
					if(entity!=null)
					{
						this.TicketSerialNumbers.Add((TicketSerialNumberEntity)entity);
					}
					break;
				case "TransactionJournalsForAppledPass":
					_alreadyFetchedTransactionJournalsForAppledPass = true;
					if(entity!=null)
					{
						this.TransactionJournalsForAppledPass.Add((TransactionJournalEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "DunningProcesses":
					_alreadyFetchedDunningProcesses = true;
					if(entity!=null)
					{
						this.DunningProcesses.Add((DunningProcessEntity)entity);
					}
					break;
				case "ProductRelation":
					_alreadyFetchedProductRelation = true;
					this.ProductRelation = (ProductRelationEntity)entity;
					break;
				case "SubsidyTransaction":
					_alreadyFetchedSubsidyTransaction = true;
					this.SubsidyTransaction = (SubsidyTransactionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "PointOfSale":
					SetupSyncPointOfSale(relatedEntity);
					break;
				case "TaxTicket":
					SetupSyncTaxTicket(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "Card":
					SetupSyncCard(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "Organization":
					SetupSyncOrganization(relatedEntity);
					break;
				case "ReferencedProduct":
					SetupSyncReferencedProduct(relatedEntity);
					break;
				case "Refund":
					SetupSyncRefund(relatedEntity);
					break;
				case "Voucher":
					SetupSyncVoucher(relatedEntity);
					break;
				case "AutoloadSettings":
					_autoloadSettings.Add((AutoloadSettingEntity)relatedEntity);
					break;
				case "DunningToProducts":
					_dunningToProducts.Add((DunningToProductEntity)relatedEntity);
					break;
				case "EmailEventResendLocks":
					_emailEventResendLocks.Add((EmailEventResendLockEntity)relatedEntity);
					break;
				case "EntitlementToProducts":
					_entitlementToProducts.Add((EntitlementToProductEntity)relatedEntity);
					break;
				case "OrderDetails":
					_orderDetails.Add((OrderDetailEntity)relatedEntity);
					break;
				case "PassExpiryNotifications":
					_passExpiryNotifications.Add((PassExpiryNotificationEntity)relatedEntity);
					break;
				case "Postings":
					_postings.Add((PostingEntity)relatedEntity);
					break;
				case "Product":
					_product.Add((ProductEntity)relatedEntity);
					break;
				case "ProductTerminations":
					_productTerminations.Add((ProductTerminationEntity)relatedEntity);
					break;
				case "RuleViolations":
					_ruleViolations.Add((RuleViolationEntity)relatedEntity);
					break;
				case "TicketSerialNumbers":
					_ticketSerialNumbers.Add((TicketSerialNumberEntity)relatedEntity);
					break;
				case "TransactionJournalsForAppledPass":
					_transactionJournalsForAppledPass.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "ProductRelation":
					SetupSyncProductRelation(relatedEntity);
					break;
				case "SubsidyTransaction":
					SetupSyncSubsidyTransaction(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "PointOfSale":
					DesetupSyncPointOfSale(false, true);
					break;
				case "TaxTicket":
					DesetupSyncTaxTicket(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "Card":
					DesetupSyncCard(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "Organization":
					DesetupSyncOrganization(false, true);
					break;
				case "ReferencedProduct":
					DesetupSyncReferencedProduct(false, true);
					break;
				case "Refund":
					DesetupSyncRefund(false, true);
					break;
				case "Voucher":
					DesetupSyncVoucher(false, true);
					break;
				case "AutoloadSettings":
					this.PerformRelatedEntityRemoval(_autoloadSettings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DunningToProducts":
					this.PerformRelatedEntityRemoval(_dunningToProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EmailEventResendLocks":
					this.PerformRelatedEntityRemoval(_emailEventResendLocks, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "EntitlementToProducts":
					this.PerformRelatedEntityRemoval(_entitlementToProducts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderDetails":
					this.PerformRelatedEntityRemoval(_orderDetails, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PassExpiryNotifications":
					this.PerformRelatedEntityRemoval(_passExpiryNotifications, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Postings":
					this.PerformRelatedEntityRemoval(_postings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Product":
					this.PerformRelatedEntityRemoval(_product, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductTerminations":
					this.PerformRelatedEntityRemoval(_productTerminations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "RuleViolations":
					this.PerformRelatedEntityRemoval(_ruleViolations, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketSerialNumbers":
					this.PerformRelatedEntityRemoval(_ticketSerialNumbers, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournalsForAppledPass":
					this.PerformRelatedEntityRemoval(_transactionJournalsForAppledPass, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ProductRelation":
					DesetupSyncProductRelation(false, true);
					break;
				case "SubsidyTransaction":
					DesetupSyncSubsidyTransaction(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_productRelation!=null)
			{
				toReturn.Add(_productRelation);
			}
			if(_subsidyTransaction!=null)
			{
				toReturn.Add(_subsidyTransaction);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_pointOfSale!=null)
			{
				toReturn.Add(_pointOfSale);
			}
			if(_taxTicket!=null)
			{
				toReturn.Add(_taxTicket);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_card!=null)
			{
				toReturn.Add(_card);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_organization!=null)
			{
				toReturn.Add(_organization);
			}
			if(_referencedProduct!=null)
			{
				toReturn.Add(_referencedProduct);
			}
			if(_refund!=null)
			{
				toReturn.Add(_refund);
			}
			if(_voucher!=null)
			{
				toReturn.Add(_voucher);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_autoloadSettings);
			toReturn.Add(_dunningToProducts);
			toReturn.Add(_emailEventResendLocks);
			toReturn.Add(_entitlementToProducts);
			toReturn.Add(_orderDetails);
			toReturn.Add(_passExpiryNotifications);
			toReturn.Add(_postings);
			toReturn.Add(_product);
			toReturn.Add(_productTerminations);
			toReturn.Add(_ruleViolations);
			toReturn.Add(_ticketSerialNumbers);
			toReturn.Add(_transactionJournalsForAppledPass);
			toReturn.Add(_transactionJournals);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productID)
		{
			return FetchUsingPK(productID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(productID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(productID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 productID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(productID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ProductID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ProductRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch)
		{
			return GetMultiAutoloadSettings(forceFetch, _autoloadSettings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AutoloadSettingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAutoloadSettings(forceFetch, _autoloadSettings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAutoloadSettings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection GetMultiAutoloadSettings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAutoloadSettings || forceFetch || _alwaysFetchAutoloadSettings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_autoloadSettings);
				_autoloadSettings.SuppressClearInGetMulti=!forceFetch;
				_autoloadSettings.EntityFactoryToUse = entityFactoryToUse;
				_autoloadSettings.GetMultiManyToOne(null, null, this, filter);
				_autoloadSettings.SuppressClearInGetMulti=false;
				_alreadyFetchedAutoloadSettings = true;
			}
			return _autoloadSettings;
		}

		/// <summary> Sets the collection parameters for the collection for 'AutoloadSettings'. These settings will be taken into account
		/// when the property AutoloadSettings is requested or GetMultiAutoloadSettings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAutoloadSettings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_autoloadSettings.SortClauses=sortClauses;
			_autoloadSettings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch)
		{
			return GetMultiDunningToProducts(forceFetch, _dunningToProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DunningToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDunningToProducts(forceFetch, _dunningToProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDunningToProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DunningToProductCollection GetMultiDunningToProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDunningToProducts || forceFetch || _alwaysFetchDunningToProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningToProducts);
				_dunningToProducts.SuppressClearInGetMulti=!forceFetch;
				_dunningToProducts.EntityFactoryToUse = entityFactoryToUse;
				_dunningToProducts.GetMultiManyToOne(null, this, filter);
				_dunningToProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningToProducts = true;
			}
			return _dunningToProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningToProducts'. These settings will be taken into account
		/// when the property DunningToProducts is requested or GetMultiDunningToProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningToProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningToProducts.SortClauses=sortClauses;
			_dunningToProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EmailEventResendLockEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEmailEventResendLocks(forceFetch, _emailEventResendLocks.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEmailEventResendLocks(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection GetMultiEmailEventResendLocks(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEmailEventResendLocks || forceFetch || _alwaysFetchEmailEventResendLocks) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_emailEventResendLocks);
				_emailEventResendLocks.SuppressClearInGetMulti=!forceFetch;
				_emailEventResendLocks.EntityFactoryToUse = entityFactoryToUse;
				_emailEventResendLocks.GetMultiManyToOne(null, null, null, null, this, filter);
				_emailEventResendLocks.SuppressClearInGetMulti=false;
				_alreadyFetchedEmailEventResendLocks = true;
			}
			return _emailEventResendLocks;
		}

		/// <summary> Sets the collection parameters for the collection for 'EmailEventResendLocks'. These settings will be taken into account
		/// when the property EmailEventResendLocks is requested or GetMultiEmailEventResendLocks is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEmailEventResendLocks(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_emailEventResendLocks.SortClauses=sortClauses;
			_emailEventResendLocks.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch)
		{
			return GetMultiEntitlementToProducts(forceFetch, _entitlementToProducts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EntitlementToProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEntitlementToProducts(forceFetch, _entitlementToProducts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEntitlementToProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementToProductCollection GetMultiEntitlementToProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEntitlementToProducts || forceFetch || _alwaysFetchEntitlementToProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entitlementToProducts);
				_entitlementToProducts.SuppressClearInGetMulti=!forceFetch;
				_entitlementToProducts.EntityFactoryToUse = entityFactoryToUse;
				_entitlementToProducts.GetMultiManyToOne(null, this, filter);
				_entitlementToProducts.SuppressClearInGetMulti=false;
				_alreadyFetchedEntitlementToProducts = true;
			}
			return _entitlementToProducts;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntitlementToProducts'. These settings will be taken into account
		/// when the property EntitlementToProducts is requested or GetMultiEntitlementToProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntitlementToProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entitlementToProducts.SortClauses=sortClauses;
			_entitlementToProducts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderDetailEntity'</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderDetails(forceFetch, _orderDetails.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderDetails(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection GetMultiOrderDetails(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderDetails || forceFetch || _alwaysFetchOrderDetails) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderDetails);
				_orderDetails.SuppressClearInGetMulti=!forceFetch;
				_orderDetails.EntityFactoryToUse = entityFactoryToUse;
				_orderDetails.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_orderDetails.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderDetails = true;
			}
			return _orderDetails;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderDetails'. These settings will be taken into account
		/// when the property OrderDetails is requested or GetMultiOrderDetails is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderDetails(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderDetails.SortClauses=sortClauses;
			_orderDetails.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PassExpiryNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PassExpiryNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection GetMultiPassExpiryNotifications(bool forceFetch)
		{
			return GetMultiPassExpiryNotifications(forceFetch, _passExpiryNotifications.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PassExpiryNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PassExpiryNotificationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection GetMultiPassExpiryNotifications(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPassExpiryNotifications(forceFetch, _passExpiryNotifications.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PassExpiryNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection GetMultiPassExpiryNotifications(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPassExpiryNotifications(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PassExpiryNotificationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection GetMultiPassExpiryNotifications(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPassExpiryNotifications || forceFetch || _alwaysFetchPassExpiryNotifications) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_passExpiryNotifications);
				_passExpiryNotifications.SuppressClearInGetMulti=!forceFetch;
				_passExpiryNotifications.EntityFactoryToUse = entityFactoryToUse;
				_passExpiryNotifications.GetMultiManyToOne(this, filter);
				_passExpiryNotifications.SuppressClearInGetMulti=false;
				_alreadyFetchedPassExpiryNotifications = true;
			}
			return _passExpiryNotifications;
		}

		/// <summary> Sets the collection parameters for the collection for 'PassExpiryNotifications'. These settings will be taken into account
		/// when the property PassExpiryNotifications is requested or GetMultiPassExpiryNotifications is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPassExpiryNotifications(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_passExpiryNotifications.SortClauses=sortClauses;
			_passExpiryNotifications.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PostingEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPostings(forceFetch, _postings.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPostings(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection GetMultiPostings(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPostings || forceFetch || _alwaysFetchPostings) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_postings);
				_postings.SuppressClearInGetMulti=!forceFetch;
				_postings.EntityFactoryToUse = entityFactoryToUse;
				_postings.GetMultiManyToOne(null, null, null, null, null, null, this, filter);
				_postings.SuppressClearInGetMulti=false;
				_alreadyFetchedPostings = true;
			}
			return _postings;
		}

		/// <summary> Sets the collection parameters for the collection for 'Postings'. These settings will be taken into account
		/// when the property Postings is requested or GetMultiPostings is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPostings(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_postings.SortClauses=sortClauses;
			_postings.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProduct(bool forceFetch)
		{
			return GetMultiProduct(forceFetch, _product.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProduct(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProduct(forceFetch, _product.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProduct(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProduct(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProduct(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProduct || forceFetch || _alwaysFetchProduct) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_product);
				_product.SuppressClearInGetMulti=!forceFetch;
				_product.EntityFactoryToUse = entityFactoryToUse;
				_product.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, filter);
				_product.SuppressClearInGetMulti=false;
				_alreadyFetchedProduct = true;
			}
			return _product;
		}

		/// <summary> Sets the collection parameters for the collection for 'Product'. These settings will be taken into account
		/// when the property Product is requested or GetMultiProduct is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProduct(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_product.SortClauses=sortClauses;
			_product.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductTerminationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProductTerminations(forceFetch, _productTerminations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProductTerminations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection GetMultiProductTerminations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProductTerminations || forceFetch || _alwaysFetchProductTerminations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productTerminations);
				_productTerminations.SuppressClearInGetMulti=!forceFetch;
				_productTerminations.EntityFactoryToUse = entityFactoryToUse;
				_productTerminations.GetMultiManyToOne(null, null, null, this, null, filter);
				_productTerminations.SuppressClearInGetMulti=false;
				_alreadyFetchedProductTerminations = true;
			}
			return _productTerminations;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductTerminations'. These settings will be taken into account
		/// when the property ProductTerminations is requested or GetMultiProductTerminations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductTerminations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productTerminations.SortClauses=sortClauses;
			_productTerminations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RuleViolationEntity'</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRuleViolations(forceFetch, _ruleViolations.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRuleViolations(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection GetMultiRuleViolations(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRuleViolations || forceFetch || _alwaysFetchRuleViolations) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ruleViolations);
				_ruleViolations.SuppressClearInGetMulti=!forceFetch;
				_ruleViolations.EntityFactoryToUse = entityFactoryToUse;
				_ruleViolations.GetMultiManyToOne(null, null, this, filter);
				_ruleViolations.SuppressClearInGetMulti=false;
				_alreadyFetchedRuleViolations = true;
			}
			return _ruleViolations;
		}

		/// <summary> Sets the collection parameters for the collection for 'RuleViolations'. These settings will be taken into account
		/// when the property RuleViolations is requested or GetMultiRuleViolations is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRuleViolations(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ruleViolations.SortClauses=sortClauses;
			_ruleViolations.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketSerialNumberEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketSerialNumbers(forceFetch, _ticketSerialNumbers.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketSerialNumbers(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection GetMultiTicketSerialNumbers(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketSerialNumbers || forceFetch || _alwaysFetchTicketSerialNumbers) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketSerialNumbers);
				_ticketSerialNumbers.SuppressClearInGetMulti=!forceFetch;
				_ticketSerialNumbers.EntityFactoryToUse = entityFactoryToUse;
				_ticketSerialNumbers.GetMultiManyToOne(null, null, this, filter);
				_ticketSerialNumbers.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketSerialNumbers = true;
			}
			return _ticketSerialNumbers;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketSerialNumbers'. These settings will be taken into account
		/// when the property TicketSerialNumbers is requested or GetMultiTicketSerialNumbers is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketSerialNumbers(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketSerialNumbers.SortClauses=sortClauses;
			_ticketSerialNumbers.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppledPass(bool forceFetch)
		{
			return GetMultiTransactionJournalsForAppledPass(forceFetch, _transactionJournalsForAppledPass.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppledPass(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournalsForAppledPass(forceFetch, _transactionJournalsForAppledPass.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppledPass(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournalsForAppledPass(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournalsForAppledPass(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournalsForAppledPass || forceFetch || _alwaysFetchTransactionJournalsForAppledPass) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournalsForAppledPass);
				_transactionJournalsForAppledPass.SuppressClearInGetMulti=!forceFetch;
				_transactionJournalsForAppledPass.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournalsForAppledPass.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, null, filter);
				_transactionJournalsForAppledPass.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournalsForAppledPass = true;
			}
			return _transactionJournalsForAppledPass;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournalsForAppledPass'. These settings will be taken into account
		/// when the property TransactionJournalsForAppledPass is requested or GetMultiTransactionJournalsForAppledPass is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournalsForAppledPass(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournalsForAppledPass.SortClauses=sortClauses;
			_transactionJournalsForAppledPass.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DunningProcessEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch)
		{
			return GetMultiDunningProcesses(forceFetch, _dunningProcesses.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DunningProcessCollection GetMultiDunningProcesses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDunningProcesses || forceFetch || _alwaysFetchDunningProcesses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_dunningProcesses);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(ProductFields.ProductID, ComparisonOperator.Equal, this.ProductID, "ProductEntity__"));
				_dunningProcesses.SuppressClearInGetMulti=!forceFetch;
				_dunningProcesses.EntityFactoryToUse = entityFactoryToUse;
				_dunningProcesses.GetMulti(filter, GetRelationsForField("DunningProcesses"));
				_dunningProcesses.SuppressClearInGetMulti=false;
				_alreadyFetchedDunningProcesses = true;
			}
			return _dunningProcesses;
		}

		/// <summary> Sets the collection parameters for the collection for 'DunningProcesses'. These settings will be taken into account
		/// when the property DunningProcesses is requested or GetMultiDunningProcesses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDunningProcesses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_dunningProcesses.SortClauses=sortClauses;
			_dunningProcesses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'PointOfSaleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PointOfSaleEntity' which is related to this entity.</returns>
		public PointOfSaleEntity GetSinglePointOfSale()
		{
			return GetSinglePointOfSale(false);
		}

		/// <summary> Retrieves the related entity of type 'PointOfSaleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PointOfSaleEntity' which is related to this entity.</returns>
		public virtual PointOfSaleEntity GetSinglePointOfSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedPointOfSale || forceFetch || _alwaysFetchPointOfSale) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PointOfSaleEntityUsingPointOfSaleID);
				PointOfSaleEntity newEntity = new PointOfSaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PointOfSaleID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PointOfSaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pointOfSaleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PointOfSale = newEntity;
				_alreadyFetchedPointOfSale = fetchResult;
			}
			return _pointOfSale;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTaxTicket()
		{
			return GetSingleTaxTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTaxTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTaxTicket || forceFetch || _alwaysFetchTaxTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTaxTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TaxTicketID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_taxTicketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TaxTicket = newEntity;
				_alreadyFetchedTaxTicket = fetchResult;
			}
			return _taxTicket;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public CardEntity GetSingleCard()
		{
			return GetSingleCard(false);
		}

		/// <summary> Retrieves the related entity of type 'CardEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CardEntity' which is related to this entity.</returns>
		public virtual CardEntity GetSingleCard(bool forceFetch)
		{
			if( ( !_alreadyFetchedCard || forceFetch || _alwaysFetchCard) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CardEntityUsingCardID);
				CardEntity newEntity = new CardEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CardID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CardEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_cardReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Card = newEntity;
				_alreadyFetchedCard = fetchResult;
			}
			return _card;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public OrganizationEntity GetSingleOrganization()
		{
			return GetSingleOrganization(false);
		}

		/// <summary> Retrieves the related entity of type 'OrganizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrganizationEntity' which is related to this entity.</returns>
		public virtual OrganizationEntity GetSingleOrganization(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrganization || forceFetch || _alwaysFetchOrganization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrganizationEntityUsingOrganizationID);
				OrganizationEntity newEntity = new OrganizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrganizationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrganizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_organizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Organization = newEntity;
				_alreadyFetchedOrganization = fetchResult;
			}
			return _organization;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleReferencedProduct()
		{
			return GetSingleReferencedProduct(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleReferencedProduct(bool forceFetch)
		{
			if( ( !_alreadyFetchedReferencedProduct || forceFetch || _alwaysFetchReferencedProduct) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductIDReferencedProductID);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReferencedProductID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_referencedProductReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReferencedProduct = newEntity;
				_alreadyFetchedReferencedProduct = fetchResult;
			}
			return _referencedProduct;
		}


		/// <summary> Retrieves the related entity of type 'RefundEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RefundEntity' which is related to this entity.</returns>
		public RefundEntity GetSingleRefund()
		{
			return GetSingleRefund(false);
		}

		/// <summary> Retrieves the related entity of type 'RefundEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RefundEntity' which is related to this entity.</returns>
		public virtual RefundEntity GetSingleRefund(bool forceFetch)
		{
			if( ( !_alreadyFetchedRefund || forceFetch || _alwaysFetchRefund) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RefundEntityUsingRefundID);
				RefundEntity newEntity = new RefundEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RefundID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (RefundEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_refundReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Refund = newEntity;
				_alreadyFetchedRefund = fetchResult;
			}
			return _refund;
		}


		/// <summary> Retrieves the related entity of type 'VoucherEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VoucherEntity' which is related to this entity.</returns>
		public VoucherEntity GetSingleVoucher()
		{
			return GetSingleVoucher(false);
		}

		/// <summary> Retrieves the related entity of type 'VoucherEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VoucherEntity' which is related to this entity.</returns>
		public virtual VoucherEntity GetSingleVoucher(bool forceFetch)
		{
			if( ( !_alreadyFetchedVoucher || forceFetch || _alwaysFetchVoucher) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VoucherEntityUsingVoucherID);
				VoucherEntity newEntity = new VoucherEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VoucherID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VoucherEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_voucherReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Voucher = newEntity;
				_alreadyFetchedVoucher = fetchResult;
			}
			return _voucher;
		}

		/// <summary> Retrieves the related entity of type 'ProductRelationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'ProductRelationEntity' which is related to this entity.</returns>
		public ProductRelationEntity GetSingleProductRelation()
		{
			return GetSingleProductRelation(false);
		}
		
		/// <summary> Retrieves the related entity of type 'ProductRelationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductRelationEntity' which is related to this entity.</returns>
		public virtual ProductRelationEntity GetSingleProductRelation(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductRelation || forceFetch || _alwaysFetchProductRelation) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductRelationEntityUsingProductID);
				ProductRelationEntity newEntity = new ProductRelationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCProductID(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (ProductRelationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productRelationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductRelation = newEntity;
				_alreadyFetchedProductRelation = fetchResult;
			}
			return _productRelation;
		}

		/// <summary> Retrieves the related entity of type 'SubsidyTransactionEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'SubsidyTransactionEntity' which is related to this entity.</returns>
		public SubsidyTransactionEntity GetSingleSubsidyTransaction()
		{
			return GetSingleSubsidyTransaction(false);
		}
		
		/// <summary> Retrieves the related entity of type 'SubsidyTransactionEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SubsidyTransactionEntity' which is related to this entity.</returns>
		public virtual SubsidyTransactionEntity GetSingleSubsidyTransaction(bool forceFetch)
		{
			if( ( !_alreadyFetchedSubsidyTransaction || forceFetch || _alwaysFetchSubsidyTransaction) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SubsidyTransactionEntityUsingProductID);
				SubsidyTransactionEntity newEntity = new SubsidyTransactionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCProductID(this.ProductID);
				}
				if(fetchResult)
				{
					newEntity = (SubsidyTransactionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_subsidyTransactionReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SubsidyTransaction = newEntity;
				_alreadyFetchedSubsidyTransaction = fetchResult;
			}
			return _subsidyTransaction;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("PointOfSale", _pointOfSale);
			toReturn.Add("TaxTicket", _taxTicket);
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("Card", _card);
			toReturn.Add("Contract", _contract);
			toReturn.Add("Organization", _organization);
			toReturn.Add("ReferencedProduct", _referencedProduct);
			toReturn.Add("Refund", _refund);
			toReturn.Add("Voucher", _voucher);
			toReturn.Add("AutoloadSettings", _autoloadSettings);
			toReturn.Add("DunningToProducts", _dunningToProducts);
			toReturn.Add("EmailEventResendLocks", _emailEventResendLocks);
			toReturn.Add("EntitlementToProducts", _entitlementToProducts);
			toReturn.Add("OrderDetails", _orderDetails);
			toReturn.Add("PassExpiryNotifications", _passExpiryNotifications);
			toReturn.Add("Postings", _postings);
			toReturn.Add("Product", _product);
			toReturn.Add("ProductTerminations", _productTerminations);
			toReturn.Add("RuleViolations", _ruleViolations);
			toReturn.Add("TicketSerialNumbers", _ticketSerialNumbers);
			toReturn.Add("TransactionJournalsForAppledPass", _transactionJournalsForAppledPass);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("DunningProcesses", _dunningProcesses);
			toReturn.Add("ProductRelation", _productRelation);
			toReturn.Add("SubsidyTransaction", _subsidyTransaction);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="validator">The validator object for this ProductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 productID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(productID, prefetchPathToUse, null, null);

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_autoloadSettings = new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection();
			_autoloadSettings.SetContainingEntityInfo(this, "Product");

			_dunningToProducts = new VarioSL.Entities.CollectionClasses.DunningToProductCollection();
			_dunningToProducts.SetContainingEntityInfo(this, "Product");

			_emailEventResendLocks = new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection();
			_emailEventResendLocks.SetContainingEntityInfo(this, "Product");

			_entitlementToProducts = new VarioSL.Entities.CollectionClasses.EntitlementToProductCollection();
			_entitlementToProducts.SetContainingEntityInfo(this, "Product");

			_orderDetails = new VarioSL.Entities.CollectionClasses.OrderDetailCollection();
			_orderDetails.SetContainingEntityInfo(this, "Product");

			_passExpiryNotifications = new VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection();
			_passExpiryNotifications.SetContainingEntityInfo(this, "Product");

			_postings = new VarioSL.Entities.CollectionClasses.PostingCollection();
			_postings.SetContainingEntityInfo(this, "Product");

			_product = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_product.SetContainingEntityInfo(this, "ReferencedProduct");

			_productTerminations = new VarioSL.Entities.CollectionClasses.ProductTerminationCollection();
			_productTerminations.SetContainingEntityInfo(this, "Product");

			_ruleViolations = new VarioSL.Entities.CollectionClasses.RuleViolationCollection();
			_ruleViolations.SetContainingEntityInfo(this, "Product");

			_ticketSerialNumbers = new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection();
			_ticketSerialNumbers.SetContainingEntityInfo(this, "Product");

			_transactionJournalsForAppledPass = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournalsForAppledPass.SetContainingEntityInfo(this, "AppliedPassProduct");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "Product");
			_dunningProcesses = new VarioSL.Entities.CollectionClasses.DunningProcessCollection();
			_clientReturnsNewIfNotFound = false;
			_pointOfSaleReturnsNewIfNotFound = false;
			_taxTicketReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			_cardReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_organizationReturnsNewIfNotFound = false;
			_referencedProductReturnsNewIfNotFound = false;
			_refundReturnsNewIfNotFound = false;
			_voucherReturnsNewIfNotFound = false;
			_productRelationReturnsNewIfNotFound = false;
			_subsidyTransactionReturnsNewIfNotFound = false;
			PerformDependencyInjection();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
            // __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReason", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BlockingReasonEnum", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Expiry", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExportState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FillLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupSize", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InstanceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsBlocked", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCanceled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCleared", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsLoaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsPaid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinimumTerm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfSaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReferencedProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RefundID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RelationTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RemainingValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxLocationcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripSerialNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidateWhenSelling", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvEntitlementData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvEntNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VoucherID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticProductRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticProductRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pointOfSale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPointOfSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pointOfSale, new PropertyChangedEventHandler( OnPointOfSalePropertyChanged ), "PointOfSale", VarioSL.Entities.RelationClasses.StaticProductRelations.PointOfSaleEntityUsingPointOfSaleIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.PointOfSaleID } );		
			_pointOfSale = null;
		}
		
		/// <summary> setups the sync logic for member _pointOfSale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPointOfSale(IEntityCore relatedEntity)
		{
			if(_pointOfSale!=relatedEntity)
			{		
				DesetupSyncPointOfSale(true, true);
				_pointOfSale = (PointOfSaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pointOfSale, new PropertyChangedEventHandler( OnPointOfSalePropertyChanged ), "PointOfSale", VarioSL.Entities.RelationClasses.StaticProductRelations.PointOfSaleEntityUsingPointOfSaleIDStatic, true, ref _alreadyFetchedPointOfSale, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPointOfSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _taxTicket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTaxTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _taxTicket, new PropertyChangedEventHandler( OnTaxTicketPropertyChanged ), "TaxTicket", VarioSL.Entities.RelationClasses.StaticProductRelations.TicketEntityUsingTaxTicketIDStatic, true, signalRelatedEntity, "TaxProducts", resetFKFields, new int[] { (int)ProductFieldIndex.TaxTicketID } );		
			_taxTicket = null;
		}
		
		/// <summary> setups the sync logic for member _taxTicket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTaxTicket(IEntityCore relatedEntity)
		{
			if(_taxTicket!=relatedEntity)
			{		
				DesetupSyncTaxTicket(true, true);
				_taxTicket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _taxTicket, new PropertyChangedEventHandler( OnTaxTicketPropertyChanged ), "TaxTicket", VarioSL.Entities.RelationClasses.StaticProductRelations.TicketEntityUsingTaxTicketIDStatic, true, ref _alreadyFetchedTaxTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTaxTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticProductRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticProductRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _card</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCard(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticProductRelations.CardEntityUsingCardIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.CardID } );		
			_card = null;
		}
		
		/// <summary> setups the sync logic for member _card</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCard(IEntityCore relatedEntity)
		{
			if(_card!=relatedEntity)
			{		
				DesetupSyncCard(true, true);
				_card = (CardEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _card, new PropertyChangedEventHandler( OnCardPropertyChanged ), "Card", VarioSL.Entities.RelationClasses.StaticProductRelations.CardEntityUsingCardIDStatic, true, ref _alreadyFetchedCard, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCardPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticProductRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticProductRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _organization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrganization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticProductRelations.OrganizationEntityUsingOrganizationIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.OrganizationID } );		
			_organization = null;
		}
		
		/// <summary> setups the sync logic for member _organization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrganization(IEntityCore relatedEntity)
		{
			if(_organization!=relatedEntity)
			{		
				DesetupSyncOrganization(true, true);
				_organization = (OrganizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _organization, new PropertyChangedEventHandler( OnOrganizationPropertyChanged ), "Organization", VarioSL.Entities.RelationClasses.StaticProductRelations.OrganizationEntityUsingOrganizationIDStatic, true, ref _alreadyFetchedOrganization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrganizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _referencedProduct</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReferencedProduct(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _referencedProduct, new PropertyChangedEventHandler( OnReferencedProductPropertyChanged ), "ReferencedProduct", VarioSL.Entities.RelationClasses.StaticProductRelations.ProductEntityUsingProductIDReferencedProductIDStatic, true, signalRelatedEntity, "Product", resetFKFields, new int[] { (int)ProductFieldIndex.ReferencedProductID } );		
			_referencedProduct = null;
		}
		
		/// <summary> setups the sync logic for member _referencedProduct</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReferencedProduct(IEntityCore relatedEntity)
		{
			if(_referencedProduct!=relatedEntity)
			{		
				DesetupSyncReferencedProduct(true, true);
				_referencedProduct = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _referencedProduct, new PropertyChangedEventHandler( OnReferencedProductPropertyChanged ), "ReferencedProduct", VarioSL.Entities.RelationClasses.StaticProductRelations.ProductEntityUsingProductIDReferencedProductIDStatic, true, ref _alreadyFetchedReferencedProduct, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReferencedProductPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _refund</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRefund(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _refund, new PropertyChangedEventHandler( OnRefundPropertyChanged ), "Refund", VarioSL.Entities.RelationClasses.StaticProductRelations.RefundEntityUsingRefundIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.RefundID } );		
			_refund = null;
		}
		
		/// <summary> setups the sync logic for member _refund</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRefund(IEntityCore relatedEntity)
		{
			if(_refund!=relatedEntity)
			{		
				DesetupSyncRefund(true, true);
				_refund = (RefundEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _refund, new PropertyChangedEventHandler( OnRefundPropertyChanged ), "Refund", VarioSL.Entities.RelationClasses.StaticProductRelations.RefundEntityUsingRefundIDStatic, true, ref _alreadyFetchedRefund, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRefundPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _voucher</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVoucher(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _voucher, new PropertyChangedEventHandler( OnVoucherPropertyChanged ), "Voucher", VarioSL.Entities.RelationClasses.StaticProductRelations.VoucherEntityUsingVoucherIDStatic, true, signalRelatedEntity, "Products", resetFKFields, new int[] { (int)ProductFieldIndex.VoucherID } );		
			_voucher = null;
		}
		
		/// <summary> setups the sync logic for member _voucher</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVoucher(IEntityCore relatedEntity)
		{
			if(_voucher!=relatedEntity)
			{		
				DesetupSyncVoucher(true, true);
				_voucher = (VoucherEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _voucher, new PropertyChangedEventHandler( OnVoucherPropertyChanged ), "Voucher", VarioSL.Entities.RelationClasses.StaticProductRelations.VoucherEntityUsingVoucherIDStatic, true, ref _alreadyFetchedVoucher, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVoucherPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productRelation</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductRelation(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productRelation, new PropertyChangedEventHandler( OnProductRelationPropertyChanged ), "ProductRelation", VarioSL.Entities.RelationClasses.StaticProductRelations.ProductRelationEntityUsingProductIDStatic, false, signalRelatedEntity, "Product", false, new int[] { (int)ProductFieldIndex.ProductID } );
			_productRelation = null;
		}
	
		/// <summary> setups the sync logic for member _productRelation</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductRelation(IEntityCore relatedEntity)
		{
			if(_productRelation!=relatedEntity)
			{
				DesetupSyncProductRelation(true, true);
				_productRelation = (ProductRelationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productRelation, new PropertyChangedEventHandler( OnProductRelationPropertyChanged ), "ProductRelation", VarioSL.Entities.RelationClasses.StaticProductRelations.ProductRelationEntityUsingProductIDStatic, false, ref _alreadyFetchedProductRelation, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductRelationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _subsidyTransaction</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSubsidyTransaction(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _subsidyTransaction, new PropertyChangedEventHandler( OnSubsidyTransactionPropertyChanged ), "SubsidyTransaction", VarioSL.Entities.RelationClasses.StaticProductRelations.SubsidyTransactionEntityUsingProductIDStatic, false, signalRelatedEntity, "Product", false, new int[] { (int)ProductFieldIndex.ProductID } );
			_subsidyTransaction = null;
		}
	
		/// <summary> setups the sync logic for member _subsidyTransaction</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSubsidyTransaction(IEntityCore relatedEntity)
		{
			if(_subsidyTransaction!=relatedEntity)
			{
				DesetupSyncSubsidyTransaction(true, true);
				_subsidyTransaction = (SubsidyTransactionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _subsidyTransaction, new PropertyChangedEventHandler( OnSubsidyTransactionPropertyChanged ), "SubsidyTransaction", VarioSL.Entities.RelationClasses.StaticProductRelations.SubsidyTransactionEntityUsingProductIDStatic, false, ref _alreadyFetchedSubsidyTransaction, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSubsidyTransactionPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="productID">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 productID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ProductFieldIndex.ProductID].ForcedCurrentValueWrite(productID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateProductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ProductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ProductRelations Relations
		{
			get	{ return new ProductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AutoloadSetting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAutoloadSettings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AutoloadSettingCollection(), (IEntityRelation)GetRelationsForField("AutoloadSettings")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.AutoloadSettingEntity, 0, null, null, null, "AutoloadSettings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningToProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningToProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningToProductCollection(), (IEntityRelation)GetRelationsForField("DunningToProducts")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.DunningToProductEntity, 0, null, null, null, "DunningToProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EmailEventResendLock' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEmailEventResendLocks
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection(), (IEntityRelation)GetRelationsForField("EmailEventResendLocks")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.EmailEventResendLockEntity, 0, null, null, null, "EmailEventResendLocks", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntitlementToProduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntitlementToProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EntitlementToProductCollection(), (IEntityRelation)GetRelationsForField("EntitlementToProducts")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.EntitlementToProductEntity, 0, null, null, null, "EntitlementToProducts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderDetail' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderDetails
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderDetailCollection(), (IEntityRelation)GetRelationsForField("OrderDetails")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.OrderDetailEntity, 0, null, null, null, "OrderDetails", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PassExpiryNotification' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPassExpiryNotifications
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection(), (IEntityRelation)GetRelationsForField("PassExpiryNotifications")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.PassExpiryNotificationEntity, 0, null, null, null, "PassExpiryNotifications", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Posting' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostings
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingCollection(), (IEntityRelation)GetRelationsForField("Postings")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.PostingEntity, 0, null, null, null, "Postings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProduct
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Product")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Product", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductTermination' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductTerminations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductTerminationCollection(), (IEntityRelation)GetRelationsForField("ProductTerminations")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ProductTerminationEntity, 0, null, null, null, "ProductTerminations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RuleViolation' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRuleViolations
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RuleViolationCollection(), (IEntityRelation)GetRelationsForField("RuleViolations")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.RuleViolationEntity, 0, null, null, null, "RuleViolations", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketSerialNumber' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketSerialNumbers
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection(), (IEntityRelation)GetRelationsForField("TicketSerialNumbers")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.TicketSerialNumberEntity, 0, null, null, null, "TicketSerialNumbers", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournalsForAppledPass
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournalsForAppledPass")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournalsForAppledPass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningProcess'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningProcesses
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.DunningToProductEntityUsingProductID;
				intermediateRelation.SetAliases(string.Empty, "DunningToProduct_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningProcessCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.DunningProcessEntity, 0, null, null, GetRelationsForField("DunningProcesses"), "DunningProcesses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfSale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PointOfSaleCollection(), (IEntityRelation)GetRelationsForField("PointOfSale")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.PointOfSaleEntity, 0, null, null, null, "PointOfSale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTaxTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TaxTicket")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TaxTicket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Card'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCard
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CardCollection(), (IEntityRelation)GetRelationsForField("Card")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.CardEntity, 0, null, null, null, "Card", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Organization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrganization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrganizationCollection(), (IEntityRelation)GetRelationsForField("Organization")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.OrganizationEntity, 0, null, null, null, "Organization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReferencedProduct
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ReferencedProduct")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "ReferencedProduct", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Refund'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRefund
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RefundCollection(), (IEntityRelation)GetRelationsForField("Refund")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.RefundEntity, 0, null, null, null, "Refund", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Voucher'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVoucher
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VoucherCollection(), (IEntityRelation)GetRelationsForField("Voucher")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.VoucherEntity, 0, null, null, null, "Voucher", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductRelation'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductRelation
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductRelationCollection(), (IEntityRelation)GetRelationsForField("ProductRelation")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.ProductRelationEntity, 0, null, null, null, "ProductRelation", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SubsidyTransaction'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSubsidyTransaction
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SubsidyTransactionCollection(), (IEntityRelation)GetRelationsForField("SubsidyTransaction")[0], (int)VarioSL.Entities.EntityType.ProductEntity, (int)VarioSL.Entities.EntityType.SubsidyTransactionEntity, 0, null, null, null, "SubsidyTransaction", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The BlockingDate property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."BLOCKINGDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> BlockingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.BlockingDate, false); }
			set	{ SetValue((int)ProductFieldIndex.BlockingDate, value, true); }
		}

		/// <summary> The BlockingReason property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."BLOCKINGREASON"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BlockingReason
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.BlockingReason, true); }
			set	{ SetValue((int)ProductFieldIndex.BlockingReason, value, true); }
		}

		/// <summary> The BlockingReasonEnum property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."BLOCKINGREASONENUM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 BlockingReasonEnum
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.BlockingReasonEnum, true); }
			set	{ SetValue((int)ProductFieldIndex.BlockingReasonEnum, value, true); }
		}

		/// <summary> The CancellationDate property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."CANCELLATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CancellationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.CancellationDate, false); }
			set	{ SetValue((int)ProductFieldIndex.CancellationDate, value, true); }
		}

		/// <summary> The CardID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."CARDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CardID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.CardID, false); }
			set	{ SetValue((int)ProductFieldIndex.CardID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.ClientID, false); }
			set	{ SetValue((int)ProductFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.ContractID, false); }
			set	{ SetValue((int)ProductFieldIndex.ContractID, value, true); }
		}

		/// <summary> The Distance property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."DISTANCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> Distance
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.Distance, false); }
			set	{ SetValue((int)ProductFieldIndex.Distance, value, true); }
		}

		/// <summary> The Expiry property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."EXPIRY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Expiry
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.Expiry, false); }
			set	{ SetValue((int)ProductFieldIndex.Expiry, value, true); }
		}

		/// <summary> The ExportState property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."EXPORTSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 ExportState
		{
			get { return (System.Int16)GetValue((int)ProductFieldIndex.ExportState, true); }
			set	{ SetValue((int)ProductFieldIndex.ExportState, value, true); }
		}

		/// <summary> The FillLevel property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."FILLLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> FillLevel
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.FillLevel, false); }
			set	{ SetValue((int)ProductFieldIndex.FillLevel, value, true); }
		}

		/// <summary> The GroupSize property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."GROUPSIZE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> GroupSize
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.GroupSize, false); }
			set	{ SetValue((int)ProductFieldIndex.GroupSize, value, true); }
		}

		/// <summary> The InstanceNumber property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."INSTANCENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> InstanceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.InstanceNumber, false); }
			set	{ SetValue((int)ProductFieldIndex.InstanceNumber, value, true); }
		}

		/// <summary> The IsBlocked property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ISBLOCKED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsBlocked
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ProductFieldIndex.IsBlocked, false); }
			set	{ SetValue((int)ProductFieldIndex.IsBlocked, value, true); }
		}

		/// <summary> The IsCanceled property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ISCANCELED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCanceled
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsCanceled, true); }
			set	{ SetValue((int)ProductFieldIndex.IsCanceled, value, true); }
		}

		/// <summary> The IsCleared property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ISCLEARED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCleared
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsCleared, true); }
			set	{ SetValue((int)ProductFieldIndex.IsCleared, value, true); }
		}

		/// <summary> The IsLoaded property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ISLOADED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsLoaded
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsLoaded, true); }
			set	{ SetValue((int)ProductFieldIndex.IsLoaded, value, true); }
		}

		/// <summary> The IsPaid property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ISPAID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsPaid
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.IsPaid, true); }
			set	{ SetValue((int)ProductFieldIndex.IsPaid, value, true); }
		}

		/// <summary> The LastModified property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ProductFieldIndex.LastModified, true); }
			set	{ SetValue((int)ProductFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ProductFieldIndex.LastUser, true); }
			set	{ SetValue((int)ProductFieldIndex.LastUser, value, true); }
		}

		/// <summary> The MinimumTerm property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."MINIMUMTERM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MinimumTerm
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.MinimumTerm, false); }
			set	{ SetValue((int)ProductFieldIndex.MinimumTerm, value, true); }
		}

		/// <summary> The OrganizationID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."ORGANIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrganizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.OrganizationID, false); }
			set	{ SetValue((int)ProductFieldIndex.OrganizationID, value, true); }
		}

		/// <summary> The PointOfSaleID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."POINTOFSALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PointOfSaleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.PointOfSaleID, false); }
			set	{ SetValue((int)ProductFieldIndex.PointOfSaleID, value, true); }
		}

		/// <summary> The Price property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."PRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Price
		{
			get { return (System.Int32)GetValue((int)ProductFieldIndex.Price, true); }
			set	{ SetValue((int)ProductFieldIndex.Price, value, true); }
		}

		/// <summary> The ProductID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."PRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ProductID
		{
			get { return (System.Int64)GetValue((int)ProductFieldIndex.ProductID, true); }
			set	{ SetValue((int)ProductFieldIndex.ProductID, value, true); }
		}

		/// <summary> The ReferencedProductID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."REFERENCEDPRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ReferencedProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.ReferencedProductID, false); }
			set	{ SetValue((int)ProductFieldIndex.ReferencedProductID, value, true); }
		}

		/// <summary> The RefundID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."REFUNDID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RefundID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.RefundID, false); }
			set	{ SetValue((int)ProductFieldIndex.RefundID, value, true); }
		}

		/// <summary> The RelationFrom property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."RELATIONFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RelationFrom
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.RelationFrom, false); }
			set	{ SetValue((int)ProductFieldIndex.RelationFrom, value, true); }
		}

		/// <summary> The RelationTo property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."RELATIONTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RelationTo
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.RelationTo, false); }
			set	{ SetValue((int)ProductFieldIndex.RelationTo, value, true); }
		}

		/// <summary> The RemainingValue property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."REMAININGVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RemainingValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.RemainingValue, false); }
			set	{ SetValue((int)ProductFieldIndex.RemainingValue, value, true); }
		}

		/// <summary> The SubValidFrom property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."SUBVALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SubValidFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.SubValidFrom, false); }
			set	{ SetValue((int)ProductFieldIndex.SubValidFrom, value, true); }
		}

		/// <summary> The SubValidTo property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."SUBVALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SubValidTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ProductFieldIndex.SubValidTo, false); }
			set	{ SetValue((int)ProductFieldIndex.SubValidTo, value, true); }
		}

		/// <summary> The TaxLocationcode property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TAXLOCATIONCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TaxLocationcode
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.TaxLocationcode, false); }
			set	{ SetValue((int)ProductFieldIndex.TaxLocationcode, value, true); }
		}

		/// <summary> The TaxTicketID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TAXTICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TaxTicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.TaxTicketID, false); }
			set	{ SetValue((int)ProductFieldIndex.TaxTicketID, value, true); }
		}

		/// <summary> The TicketID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)ProductFieldIndex.TicketID, true); }
			set	{ SetValue((int)ProductFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketType property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TICKETTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TicketType TicketType
		{
			get { return (VarioSL.Entities.Enumerations.TicketType)GetValue((int)ProductFieldIndex.TicketType, true); }
			set	{ SetValue((int)ProductFieldIndex.TicketType, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ProductFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ProductFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TripSerialNumber property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."TRIPSERIALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TripSerialNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.TripSerialNumber, false); }
			set	{ SetValue((int)ProductFieldIndex.TripSerialNumber, value, true); }
		}

		/// <summary> The ValidateWhenSelling property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VALIDATEWHENSELLING"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ValidateWhenSelling
		{
			get { return (System.Boolean)GetValue((int)ProductFieldIndex.ValidateWhenSelling, true); }
			set	{ SetValue((int)ProductFieldIndex.ValidateWhenSelling, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)ProductFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)ProductFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidTo property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VALIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidTo
		{
			get { return (System.DateTime)GetValue((int)ProductFieldIndex.ValidTo, true); }
			set	{ SetValue((int)ProductFieldIndex.ValidTo, value, true); }
		}

		/// <summary> The VdvEntitlementData property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VDVENTITLEMENTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Blob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] VdvEntitlementData
		{
			get { return (System.Byte[])GetValue((int)ProductFieldIndex.VdvEntitlementData, true); }
			set	{ SetValue((int)ProductFieldIndex.VdvEntitlementData, value, true); }
		}

		/// <summary> The VdvEntNumber property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VDVENTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> VdvEntNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)ProductFieldIndex.VdvEntNumber, false); }
			set	{ SetValue((int)ProductFieldIndex.VdvEntNumber, value, true); }
		}

		/// <summary> The VoucherID property of the Entity Product<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_PRODUCT"."VOUCHERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VoucherID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ProductFieldIndex.VoucherID, false); }
			set	{ SetValue((int)ProductFieldIndex.VoucherID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AutoloadSettingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAutoloadSettings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AutoloadSettingCollection AutoloadSettings
		{
			get	{ return GetMultiAutoloadSettings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AutoloadSettings. When set to true, AutoloadSettings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AutoloadSettings is accessed. You can always execute/ a forced fetch by calling GetMultiAutoloadSettings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAutoloadSettings
		{
			get	{ return _alwaysFetchAutoloadSettings; }
			set	{ _alwaysFetchAutoloadSettings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AutoloadSettings already has been fetched. Setting this property to false when AutoloadSettings has been fetched
		/// will clear the AutoloadSettings collection well. Setting this property to true while AutoloadSettings hasn't been fetched disables lazy loading for AutoloadSettings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAutoloadSettings
		{
			get { return _alreadyFetchedAutoloadSettings;}
			set 
			{
				if(_alreadyFetchedAutoloadSettings && !value && (_autoloadSettings != null))
				{
					_autoloadSettings.Clear();
				}
				_alreadyFetchedAutoloadSettings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DunningToProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningToProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningToProductCollection DunningToProducts
		{
			get	{ return GetMultiDunningToProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningToProducts. When set to true, DunningToProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningToProducts is accessed. You can always execute/ a forced fetch by calling GetMultiDunningToProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningToProducts
		{
			get	{ return _alwaysFetchDunningToProducts; }
			set	{ _alwaysFetchDunningToProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningToProducts already has been fetched. Setting this property to false when DunningToProducts has been fetched
		/// will clear the DunningToProducts collection well. Setting this property to true while DunningToProducts hasn't been fetched disables lazy loading for DunningToProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningToProducts
		{
			get { return _alreadyFetchedDunningToProducts;}
			set 
			{
				if(_alreadyFetchedDunningToProducts && !value && (_dunningToProducts != null))
				{
					_dunningToProducts.Clear();
				}
				_alreadyFetchedDunningToProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EmailEventResendLockEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEmailEventResendLocks()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EmailEventResendLockCollection EmailEventResendLocks
		{
			get	{ return GetMultiEmailEventResendLocks(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EmailEventResendLocks. When set to true, EmailEventResendLocks is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EmailEventResendLocks is accessed. You can always execute/ a forced fetch by calling GetMultiEmailEventResendLocks(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEmailEventResendLocks
		{
			get	{ return _alwaysFetchEmailEventResendLocks; }
			set	{ _alwaysFetchEmailEventResendLocks = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EmailEventResendLocks already has been fetched. Setting this property to false when EmailEventResendLocks has been fetched
		/// will clear the EmailEventResendLocks collection well. Setting this property to true while EmailEventResendLocks hasn't been fetched disables lazy loading for EmailEventResendLocks</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEmailEventResendLocks
		{
			get { return _alreadyFetchedEmailEventResendLocks;}
			set 
			{
				if(_alreadyFetchedEmailEventResendLocks && !value && (_emailEventResendLocks != null))
				{
					_emailEventResendLocks.Clear();
				}
				_alreadyFetchedEmailEventResendLocks = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EntitlementToProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntitlementToProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EntitlementToProductCollection EntitlementToProducts
		{
			get	{ return GetMultiEntitlementToProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntitlementToProducts. When set to true, EntitlementToProducts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntitlementToProducts is accessed. You can always execute/ a forced fetch by calling GetMultiEntitlementToProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntitlementToProducts
		{
			get	{ return _alwaysFetchEntitlementToProducts; }
			set	{ _alwaysFetchEntitlementToProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntitlementToProducts already has been fetched. Setting this property to false when EntitlementToProducts has been fetched
		/// will clear the EntitlementToProducts collection well. Setting this property to true while EntitlementToProducts hasn't been fetched disables lazy loading for EntitlementToProducts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntitlementToProducts
		{
			get { return _alreadyFetchedEntitlementToProducts;}
			set 
			{
				if(_alreadyFetchedEntitlementToProducts && !value && (_entitlementToProducts != null))
				{
					_entitlementToProducts.Clear();
				}
				_alreadyFetchedEntitlementToProducts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderDetailEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderDetails()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.OrderDetailCollection OrderDetails
		{
			get	{ return GetMultiOrderDetails(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderDetails. When set to true, OrderDetails is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderDetails is accessed. You can always execute/ a forced fetch by calling GetMultiOrderDetails(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderDetails
		{
			get	{ return _alwaysFetchOrderDetails; }
			set	{ _alwaysFetchOrderDetails = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderDetails already has been fetched. Setting this property to false when OrderDetails has been fetched
		/// will clear the OrderDetails collection well. Setting this property to true while OrderDetails hasn't been fetched disables lazy loading for OrderDetails</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderDetails
		{
			get { return _alreadyFetchedOrderDetails;}
			set 
			{
				if(_alreadyFetchedOrderDetails && !value && (_orderDetails != null))
				{
					_orderDetails.Clear();
				}
				_alreadyFetchedOrderDetails = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PassExpiryNotificationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPassExpiryNotifications()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PassExpiryNotificationCollection PassExpiryNotifications
		{
			get	{ return GetMultiPassExpiryNotifications(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PassExpiryNotifications. When set to true, PassExpiryNotifications is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PassExpiryNotifications is accessed. You can always execute/ a forced fetch by calling GetMultiPassExpiryNotifications(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPassExpiryNotifications
		{
			get	{ return _alwaysFetchPassExpiryNotifications; }
			set	{ _alwaysFetchPassExpiryNotifications = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PassExpiryNotifications already has been fetched. Setting this property to false when PassExpiryNotifications has been fetched
		/// will clear the PassExpiryNotifications collection well. Setting this property to true while PassExpiryNotifications hasn't been fetched disables lazy loading for PassExpiryNotifications</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPassExpiryNotifications
		{
			get { return _alreadyFetchedPassExpiryNotifications;}
			set 
			{
				if(_alreadyFetchedPassExpiryNotifications && !value && (_passExpiryNotifications != null))
				{
					_passExpiryNotifications.Clear();
				}
				_alreadyFetchedPassExpiryNotifications = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PostingEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPostings()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PostingCollection Postings
		{
			get	{ return GetMultiPostings(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Postings. When set to true, Postings is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Postings is accessed. You can always execute/ a forced fetch by calling GetMultiPostings(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostings
		{
			get	{ return _alwaysFetchPostings; }
			set	{ _alwaysFetchPostings = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Postings already has been fetched. Setting this property to false when Postings has been fetched
		/// will clear the Postings collection well. Setting this property to true while Postings hasn't been fetched disables lazy loading for Postings</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostings
		{
			get { return _alreadyFetchedPostings;}
			set 
			{
				if(_alreadyFetchedPostings && !value && (_postings != null))
				{
					_postings.Clear();
				}
				_alreadyFetchedPostings = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Product
		{
			get	{ return GetMultiProduct(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Product. When set to true, Product is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Product is accessed. You can always execute/ a forced fetch by calling GetMultiProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProduct
		{
			get	{ return _alwaysFetchProduct; }
			set	{ _alwaysFetchProduct = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Product already has been fetched. Setting this property to false when Product has been fetched
		/// will clear the Product collection well. Setting this property to true while Product hasn't been fetched disables lazy loading for Product</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProduct
		{
			get { return _alreadyFetchedProduct;}
			set 
			{
				if(_alreadyFetchedProduct && !value && (_product != null))
				{
					_product.Clear();
				}
				_alreadyFetchedProduct = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ProductTerminationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductTerminations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductTerminationCollection ProductTerminations
		{
			get	{ return GetMultiProductTerminations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductTerminations. When set to true, ProductTerminations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductTerminations is accessed. You can always execute/ a forced fetch by calling GetMultiProductTerminations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductTerminations
		{
			get	{ return _alwaysFetchProductTerminations; }
			set	{ _alwaysFetchProductTerminations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductTerminations already has been fetched. Setting this property to false when ProductTerminations has been fetched
		/// will clear the ProductTerminations collection well. Setting this property to true while ProductTerminations hasn't been fetched disables lazy loading for ProductTerminations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductTerminations
		{
			get { return _alreadyFetchedProductTerminations;}
			set 
			{
				if(_alreadyFetchedProductTerminations && !value && (_productTerminations != null))
				{
					_productTerminations.Clear();
				}
				_alreadyFetchedProductTerminations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'RuleViolationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRuleViolations()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.RuleViolationCollection RuleViolations
		{
			get	{ return GetMultiRuleViolations(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RuleViolations. When set to true, RuleViolations is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RuleViolations is accessed. You can always execute/ a forced fetch by calling GetMultiRuleViolations(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRuleViolations
		{
			get	{ return _alwaysFetchRuleViolations; }
			set	{ _alwaysFetchRuleViolations = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RuleViolations already has been fetched. Setting this property to false when RuleViolations has been fetched
		/// will clear the RuleViolations collection well. Setting this property to true while RuleViolations hasn't been fetched disables lazy loading for RuleViolations</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRuleViolations
		{
			get { return _alreadyFetchedRuleViolations;}
			set 
			{
				if(_alreadyFetchedRuleViolations && !value && (_ruleViolations != null))
				{
					_ruleViolations.Clear();
				}
				_alreadyFetchedRuleViolations = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketSerialNumberEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketSerialNumbers()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketSerialNumberCollection TicketSerialNumbers
		{
			get	{ return GetMultiTicketSerialNumbers(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketSerialNumbers. When set to true, TicketSerialNumbers is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketSerialNumbers is accessed. You can always execute/ a forced fetch by calling GetMultiTicketSerialNumbers(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketSerialNumbers
		{
			get	{ return _alwaysFetchTicketSerialNumbers; }
			set	{ _alwaysFetchTicketSerialNumbers = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketSerialNumbers already has been fetched. Setting this property to false when TicketSerialNumbers has been fetched
		/// will clear the TicketSerialNumbers collection well. Setting this property to true while TicketSerialNumbers hasn't been fetched disables lazy loading for TicketSerialNumbers</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketSerialNumbers
		{
			get { return _alreadyFetchedTicketSerialNumbers;}
			set 
			{
				if(_alreadyFetchedTicketSerialNumbers && !value && (_ticketSerialNumbers != null))
				{
					_ticketSerialNumbers.Clear();
				}
				_alreadyFetchedTicketSerialNumbers = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournalsForAppledPass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournalsForAppledPass
		{
			get	{ return GetMultiTransactionJournalsForAppledPass(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournalsForAppledPass. When set to true, TransactionJournalsForAppledPass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournalsForAppledPass is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournalsForAppledPass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournalsForAppledPass
		{
			get	{ return _alwaysFetchTransactionJournalsForAppledPass; }
			set	{ _alwaysFetchTransactionJournalsForAppledPass = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournalsForAppledPass already has been fetched. Setting this property to false when TransactionJournalsForAppledPass has been fetched
		/// will clear the TransactionJournalsForAppledPass collection well. Setting this property to true while TransactionJournalsForAppledPass hasn't been fetched disables lazy loading for TransactionJournalsForAppledPass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournalsForAppledPass
		{
			get { return _alreadyFetchedTransactionJournalsForAppledPass;}
			set 
			{
				if(_alreadyFetchedTransactionJournalsForAppledPass && !value && (_transactionJournalsForAppledPass != null))
				{
					_transactionJournalsForAppledPass.Clear();
				}
				_alreadyFetchedTransactionJournalsForAppledPass = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DunningProcessEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDunningProcesses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DunningProcessCollection DunningProcesses
		{
			get { return GetMultiDunningProcesses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DunningProcesses. When set to true, DunningProcesses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningProcesses is accessed. You can always execute a forced fetch by calling GetMultiDunningProcesses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningProcesses
		{
			get	{ return _alwaysFetchDunningProcesses; }
			set	{ _alwaysFetchDunningProcesses = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningProcesses already has been fetched. Setting this property to false when DunningProcesses has been fetched
		/// will clear the DunningProcesses collection well. Setting this property to true while DunningProcesses hasn't been fetched disables lazy loading for DunningProcesses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningProcesses
		{
			get { return _alreadyFetchedDunningProcesses;}
			set 
			{
				if(_alreadyFetchedDunningProcesses && !value && (_dunningProcesses != null))
				{
					_dunningProcesses.Clear();
				}
				_alreadyFetchedDunningProcesses = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PointOfSaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePointOfSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PointOfSaleEntity PointOfSale
		{
			get	{ return GetSinglePointOfSale(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPointOfSale(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "PointOfSale", _pointOfSale, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfSale. When set to true, PointOfSale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfSale is accessed. You can always execute a forced fetch by calling GetSinglePointOfSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfSale
		{
			get	{ return _alwaysFetchPointOfSale; }
			set	{ _alwaysFetchPointOfSale = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfSale already has been fetched. Setting this property to false when PointOfSale has been fetched
		/// will set PointOfSale to null as well. Setting this property to true while PointOfSale hasn't been fetched disables lazy loading for PointOfSale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfSale
		{
			get { return _alreadyFetchedPointOfSale;}
			set 
			{
				if(_alreadyFetchedPointOfSale && !value)
				{
					this.PointOfSale = null;
				}
				_alreadyFetchedPointOfSale = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PointOfSale is not found
		/// in the database. When set to true, PointOfSale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PointOfSaleReturnsNewIfNotFound
		{
			get	{ return _pointOfSaleReturnsNewIfNotFound; }
			set { _pointOfSaleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTaxTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity TaxTicket
		{
			get	{ return GetSingleTaxTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTaxTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TaxProducts", "TaxTicket", _taxTicket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TaxTicket. When set to true, TaxTicket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TaxTicket is accessed. You can always execute a forced fetch by calling GetSingleTaxTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTaxTicket
		{
			get	{ return _alwaysFetchTaxTicket; }
			set	{ _alwaysFetchTaxTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TaxTicket already has been fetched. Setting this property to false when TaxTicket has been fetched
		/// will set TaxTicket to null as well. Setting this property to true while TaxTicket hasn't been fetched disables lazy loading for TaxTicket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTaxTicket
		{
			get { return _alreadyFetchedTaxTicket;}
			set 
			{
				if(_alreadyFetchedTaxTicket && !value)
				{
					this.TaxTicket = null;
				}
				_alreadyFetchedTaxTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TaxTicket is not found
		/// in the database. When set to true, TaxTicket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TaxTicketReturnsNewIfNotFound
		{
			get	{ return _taxTicketReturnsNewIfNotFound; }
			set { _taxTicketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CardEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCard()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CardEntity Card
		{
			get	{ return GetSingleCard(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCard(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Card", _card, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Card. When set to true, Card is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Card is accessed. You can always execute a forced fetch by calling GetSingleCard(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCard
		{
			get	{ return _alwaysFetchCard; }
			set	{ _alwaysFetchCard = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Card already has been fetched. Setting this property to false when Card has been fetched
		/// will set Card to null as well. Setting this property to true while Card hasn't been fetched disables lazy loading for Card</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCard
		{
			get { return _alreadyFetchedCard;}
			set 
			{
				if(_alreadyFetchedCard && !value)
				{
					this.Card = null;
				}
				_alreadyFetchedCard = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Card is not found
		/// in the database. When set to true, Card will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CardReturnsNewIfNotFound
		{
			get	{ return _cardReturnsNewIfNotFound; }
			set { _cardReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrganizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrganization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrganizationEntity Organization
		{
			get	{ return GetSingleOrganization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrganization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Organization", _organization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Organization. When set to true, Organization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Organization is accessed. You can always execute a forced fetch by calling GetSingleOrganization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrganization
		{
			get	{ return _alwaysFetchOrganization; }
			set	{ _alwaysFetchOrganization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Organization already has been fetched. Setting this property to false when Organization has been fetched
		/// will set Organization to null as well. Setting this property to true while Organization hasn't been fetched disables lazy loading for Organization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrganization
		{
			get { return _alreadyFetchedOrganization;}
			set 
			{
				if(_alreadyFetchedOrganization && !value)
				{
					this.Organization = null;
				}
				_alreadyFetchedOrganization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Organization is not found
		/// in the database. When set to true, Organization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrganizationReturnsNewIfNotFound
		{
			get	{ return _organizationReturnsNewIfNotFound; }
			set { _organizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReferencedProduct()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductEntity ReferencedProduct
		{
			get	{ return GetSingleReferencedProduct(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReferencedProduct(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Product", "ReferencedProduct", _referencedProduct, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReferencedProduct. When set to true, ReferencedProduct is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReferencedProduct is accessed. You can always execute a forced fetch by calling GetSingleReferencedProduct(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReferencedProduct
		{
			get	{ return _alwaysFetchReferencedProduct; }
			set	{ _alwaysFetchReferencedProduct = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReferencedProduct already has been fetched. Setting this property to false when ReferencedProduct has been fetched
		/// will set ReferencedProduct to null as well. Setting this property to true while ReferencedProduct hasn't been fetched disables lazy loading for ReferencedProduct</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReferencedProduct
		{
			get { return _alreadyFetchedReferencedProduct;}
			set 
			{
				if(_alreadyFetchedReferencedProduct && !value)
				{
					this.ReferencedProduct = null;
				}
				_alreadyFetchedReferencedProduct = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReferencedProduct is not found
		/// in the database. When set to true, ReferencedProduct will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ReferencedProductReturnsNewIfNotFound
		{
			get	{ return _referencedProductReturnsNewIfNotFound; }
			set { _referencedProductReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RefundEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRefund()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RefundEntity Refund
		{
			get	{ return GetSingleRefund(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRefund(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Refund", _refund, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Refund. When set to true, Refund is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Refund is accessed. You can always execute a forced fetch by calling GetSingleRefund(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRefund
		{
			get	{ return _alwaysFetchRefund; }
			set	{ _alwaysFetchRefund = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Refund already has been fetched. Setting this property to false when Refund has been fetched
		/// will set Refund to null as well. Setting this property to true while Refund hasn't been fetched disables lazy loading for Refund</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRefund
		{
			get { return _alreadyFetchedRefund;}
			set 
			{
				if(_alreadyFetchedRefund && !value)
				{
					this.Refund = null;
				}
				_alreadyFetchedRefund = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Refund is not found
		/// in the database. When set to true, Refund will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RefundReturnsNewIfNotFound
		{
			get	{ return _refundReturnsNewIfNotFound; }
			set { _refundReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VoucherEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVoucher()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VoucherEntity Voucher
		{
			get	{ return GetSingleVoucher(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVoucher(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Products", "Voucher", _voucher, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Voucher. When set to true, Voucher is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Voucher is accessed. You can always execute a forced fetch by calling GetSingleVoucher(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVoucher
		{
			get	{ return _alwaysFetchVoucher; }
			set	{ _alwaysFetchVoucher = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Voucher already has been fetched. Setting this property to false when Voucher has been fetched
		/// will set Voucher to null as well. Setting this property to true while Voucher hasn't been fetched disables lazy loading for Voucher</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVoucher
		{
			get { return _alreadyFetchedVoucher;}
			set 
			{
				if(_alreadyFetchedVoucher && !value)
				{
					this.Voucher = null;
				}
				_alreadyFetchedVoucher = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Voucher is not found
		/// in the database. When set to true, Voucher will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VoucherReturnsNewIfNotFound
		{
			get	{ return _voucherReturnsNewIfNotFound; }
			set { _voucherReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductRelationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductRelation()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ProductRelationEntity ProductRelation
		{
			get	{ return GetSingleProductRelation(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncProductRelation(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_productRelation !=null);
						DesetupSyncProductRelation(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("ProductRelation");
						}
					}
					else
					{
						if(_productRelation!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Product");
							SetupSyncProductRelation(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductRelation. When set to true, ProductRelation is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductRelation is accessed. You can always execute a forced fetch by calling GetSingleProductRelation(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductRelation
		{
			get	{ return _alwaysFetchProductRelation; }
			set	{ _alwaysFetchProductRelation = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property ProductRelation already has been fetched. Setting this property to false when ProductRelation has been fetched
		/// will set ProductRelation to null as well. Setting this property to true while ProductRelation hasn't been fetched disables lazy loading for ProductRelation</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductRelation
		{
			get { return _alreadyFetchedProductRelation;}
			set 
			{
				if(_alreadyFetchedProductRelation && !value)
				{
					this.ProductRelation = null;
				}
				_alreadyFetchedProductRelation = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductRelation is not found
		/// in the database. When set to true, ProductRelation will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ProductRelationReturnsNewIfNotFound
		{
			get	{ return _productRelationReturnsNewIfNotFound; }
			set	{ _productRelationReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'SubsidyTransactionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSubsidyTransaction()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SubsidyTransactionEntity SubsidyTransaction
		{
			get	{ return GetSingleSubsidyTransaction(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncSubsidyTransaction(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_subsidyTransaction !=null);
						DesetupSyncSubsidyTransaction(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("SubsidyTransaction");
						}
					}
					else
					{
						if(_subsidyTransaction!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Product");
							SetupSyncSubsidyTransaction(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SubsidyTransaction. When set to true, SubsidyTransaction is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SubsidyTransaction is accessed. You can always execute a forced fetch by calling GetSingleSubsidyTransaction(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSubsidyTransaction
		{
			get	{ return _alwaysFetchSubsidyTransaction; }
			set	{ _alwaysFetchSubsidyTransaction = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property SubsidyTransaction already has been fetched. Setting this property to false when SubsidyTransaction has been fetched
		/// will set SubsidyTransaction to null as well. Setting this property to true while SubsidyTransaction hasn't been fetched disables lazy loading for SubsidyTransaction</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSubsidyTransaction
		{
			get { return _alreadyFetchedSubsidyTransaction;}
			set 
			{
				if(_alreadyFetchedSubsidyTransaction && !value)
				{
					this.SubsidyTransaction = null;
				}
				_alreadyFetchedSubsidyTransaction = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SubsidyTransaction is not found
		/// in the database. When set to true, SubsidyTransaction will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SubsidyTransactionReturnsNewIfNotFound
		{
			get	{ return _subsidyTransactionReturnsNewIfNotFound; }
			set	{ _subsidyTransactionReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ProductEntity; }
		}

		#endregion


		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
