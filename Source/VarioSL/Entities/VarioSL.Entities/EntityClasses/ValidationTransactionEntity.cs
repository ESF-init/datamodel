﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ValidationTransaction'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ValidationTransactionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ValidationTransactionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ValidationTransactionEntity() :base("ValidationTransactionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		public ValidationTransactionEntity(System.Int64 validationTransactionID):base("ValidationTransactionEntity")
		{
			InitClassFetch(validationTransactionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ValidationTransactionEntity(System.Int64 validationTransactionID, IPrefetchPath prefetchPathToUse):base("ValidationTransactionEntity")
		{
			InitClassFetch(validationTransactionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="validator">The custom validator object for this ValidationTransactionEntity</param>
		public ValidationTransactionEntity(System.Int64 validationTransactionID, IValidator validator):base("ValidationTransactionEntity")
		{
			InitClassFetch(validationTransactionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ValidationTransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationTransactionID)
		{
			return FetchUsingPK(validationTransactionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationTransactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(validationTransactionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(validationTransactionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 validationTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(validationTransactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ValidationTransactionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ValidationTransactionRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="validator">The validator object for this ValidationTransactionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 validationTransactionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(validationTransactionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataRowCreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCritical", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Resolved", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule10IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule11IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule1IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule2IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule3IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule4IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule5IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule6IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule7IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule8IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rule9IsValid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RuleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationResultDetail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationRunID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidationTransactionID", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="validationTransactionID">PK value for ValidationTransaction which data should be fetched into this ValidationTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 validationTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ValidationTransactionFieldIndex.ValidationTransactionID].ForcedCurrentValueWrite(validationTransactionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateValidationTransactionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ValidationTransactionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ValidationTransactionRelations Relations
		{
			get	{ return new ValidationTransactionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DataRowCreationDate property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."DATAROWCREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DataRowCreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationTransactionFieldIndex.DataRowCreationDate, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.DataRowCreationDate, value, true); }
		}

		/// <summary> The IsCritical property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."ISCRITICAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsCritical
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ValidationTransactionFieldIndex.IsCritical, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.IsCritical, value, true); }
		}

		/// <summary> The IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsValid
		{
			get { return (Nullable<System.Boolean>)GetValue((int)ValidationTransactionFieldIndex.IsValid, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.IsValid, value, true); }
		}

		/// <summary> The LastModified property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastModified
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationTransactionFieldIndex.LastModified, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ValidationTransactionFieldIndex.LastUser, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Resolved property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RESOLVED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Resolved
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ValidationTransactionFieldIndex.Resolved, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Resolved, value, true); }
		}

		/// <summary> The Rule10IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE10_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule10IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule10IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule10IsValid, value, true); }
		}

		/// <summary> The Rule11IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE11_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule11IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule11IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule11IsValid, value, true); }
		}

		/// <summary> The Rule1IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE1_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule1IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule1IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule1IsValid, value, true); }
		}

		/// <summary> The Rule2IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE2_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule2IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule2IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule2IsValid, value, true); }
		}

		/// <summary> The Rule3IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE3_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule3IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule3IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule3IsValid, value, true); }
		}

		/// <summary> The Rule4IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE4_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule4IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule4IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule4IsValid, value, true); }
		}

		/// <summary> The Rule5IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE5_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule5IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule5IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule5IsValid, value, true); }
		}

		/// <summary> The Rule6IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE6_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule6IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule6IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule6IsValid, value, true); }
		}

		/// <summary> The Rule7IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE7_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule7IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule7IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule7IsValid, value, true); }
		}

		/// <summary> The Rule8IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE8_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule8IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule8IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule8IsValid, value, true); }
		}

		/// <summary> The Rule9IsValid property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULE9_ISVALID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Rule9IsValid
		{
			get { return (System.Boolean)GetValue((int)ValidationTransactionFieldIndex.Rule9IsValid, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.Rule9IsValid, value, true); }
		}

		/// <summary> The RuleID property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."RULEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RuleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ValidationTransactionFieldIndex.RuleID, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.RuleID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> TransactionCounter
		{
			get { return (Nullable<System.Decimal>)GetValue((int)ValidationTransactionFieldIndex.TransactionCounter, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> The TransactionID property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."TRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ValidationTransactionFieldIndex.TransactionID, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.TransactionID, value, true); }
		}

		/// <summary> The ValidationResultDetail property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."VALIDATION_RESULT_DETAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Clob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ValidationResultDetail
		{
			get { return (System.String)GetValue((int)ValidationTransactionFieldIndex.ValidationResultDetail, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.ValidationResultDetail, value, true); }
		}

		/// <summary> The ValidationRunID property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."VALIDATIONRUNID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ValidationRunID
		{
			get { return (Nullable<System.Int64>)GetValue((int)ValidationTransactionFieldIndex.ValidationRunID, false); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.ValidationRunID, value, true); }
		}

		/// <summary> The ValidationTransactionID property of the Entity ValidationTransaction<br/><br/></summary>
		/// <remarks>Mapped on  table field: "CH_VALIDATIONTRANSACTION"."VALIDATIONTRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ValidationTransactionID
		{
			get { return (System.Int64)GetValue((int)ValidationTransactionFieldIndex.ValidationTransactionID, true); }
			set	{ SetValue((int)ValidationTransactionFieldIndex.ValidationTransactionID, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ValidationTransactionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
