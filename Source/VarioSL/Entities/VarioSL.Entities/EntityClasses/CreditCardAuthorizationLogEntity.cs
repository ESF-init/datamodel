﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CreditCardAuthorizationLog'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class CreditCardAuthorizationLogEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private CreditCardAuthorizationEntity _creditCardAuthorization;
		private bool	_alwaysFetchCreditCardAuthorization, _alreadyFetchedCreditCardAuthorization, _creditCardAuthorizationReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CreditCardAuthorization</summary>
			public static readonly string CreditCardAuthorization = "CreditCardAuthorization";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CreditCardAuthorizationLogEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public CreditCardAuthorizationLogEntity() :base("CreditCardAuthorizationLogEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		public CreditCardAuthorizationLogEntity(System.Int64 creditCardAuthorizationLogID):base("CreditCardAuthorizationLogEntity")
		{
			InitClassFetch(creditCardAuthorizationLogID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CreditCardAuthorizationLogEntity(System.Int64 creditCardAuthorizationLogID, IPrefetchPath prefetchPathToUse):base("CreditCardAuthorizationLogEntity")
		{
			InitClassFetch(creditCardAuthorizationLogID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="validator">The custom validator object for this CreditCardAuthorizationLogEntity</param>
		public CreditCardAuthorizationLogEntity(System.Int64 creditCardAuthorizationLogID, IValidator validator):base("CreditCardAuthorizationLogEntity")
		{
			InitClassFetch(creditCardAuthorizationLogID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CreditCardAuthorizationLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_creditCardAuthorization = (CreditCardAuthorizationEntity)info.GetValue("_creditCardAuthorization", typeof(CreditCardAuthorizationEntity));
			if(_creditCardAuthorization!=null)
			{
				_creditCardAuthorization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_creditCardAuthorizationReturnsNewIfNotFound = info.GetBoolean("_creditCardAuthorizationReturnsNewIfNotFound");
			_alwaysFetchCreditCardAuthorization = info.GetBoolean("_alwaysFetchCreditCardAuthorization");
			_alreadyFetchedCreditCardAuthorization = info.GetBoolean("_alreadyFetchedCreditCardAuthorization");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((CreditCardAuthorizationLogFieldIndex)fieldIndex)
			{
				case CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationID:
					DesetupSyncCreditCardAuthorization(true, false);
					_alreadyFetchedCreditCardAuthorization = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCreditCardAuthorization = (_creditCardAuthorization != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CreditCardAuthorization":
					toReturn.Add(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_creditCardAuthorization", (!this.MarkedForDeletion?_creditCardAuthorization:null));
			info.AddValue("_creditCardAuthorizationReturnsNewIfNotFound", _creditCardAuthorizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCreditCardAuthorization", _alwaysFetchCreditCardAuthorization);
			info.AddValue("_alreadyFetchedCreditCardAuthorization", _alreadyFetchedCreditCardAuthorization);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CreditCardAuthorization":
					_alreadyFetchedCreditCardAuthorization = true;
					this.CreditCardAuthorization = (CreditCardAuthorizationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CreditCardAuthorization":
					SetupSyncCreditCardAuthorization(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CreditCardAuthorization":
					DesetupSyncCreditCardAuthorization(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_creditCardAuthorization!=null)
			{
				toReturn.Add(_creditCardAuthorization);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationLogID)
		{
			return FetchUsingPK(creditCardAuthorizationLogID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationLogID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(creditCardAuthorizationLogID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationLogID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(creditCardAuthorizationLogID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 creditCardAuthorizationLogID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(creditCardAuthorizationLogID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CreditCardAuthorizationLogID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CreditCardAuthorizationLogRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public CreditCardAuthorizationEntity GetSingleCreditCardAuthorization()
		{
			return GetSingleCreditCardAuthorization(false);
		}

		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public virtual CreditCardAuthorizationEntity GetSingleCreditCardAuthorization(bool forceFetch)
		{
			if( ( !_alreadyFetchedCreditCardAuthorization || forceFetch || _alwaysFetchCreditCardAuthorization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
				CreditCardAuthorizationEntity newEntity = new CreditCardAuthorizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CreditCardAuthorizationID);
				}
				if(fetchResult)
				{
					newEntity = (CreditCardAuthorizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_creditCardAuthorizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CreditCardAuthorization = newEntity;
				_alreadyFetchedCreditCardAuthorization = fetchResult;
			}
			return _creditCardAuthorization;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CreditCardAuthorization", _creditCardAuthorization);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="validator">The validator object for this CreditCardAuthorizationLogEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 creditCardAuthorizationLogID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(creditCardAuthorizationLogID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_creditCardAuthorizationReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardAuthorizationLogID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InsertTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Request", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Response", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _creditCardAuthorization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCreditCardAuthorization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationLogRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, signalRelatedEntity, "CreditCardAuthorizationLogs", resetFKFields, new int[] { (int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationID } );		
			_creditCardAuthorization = null;
		}
		
		/// <summary> setups the sync logic for member _creditCardAuthorization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCreditCardAuthorization(IEntityCore relatedEntity)
		{
			if(_creditCardAuthorization!=relatedEntity)
			{		
				DesetupSyncCreditCardAuthorization(true, true);
				_creditCardAuthorization = (CreditCardAuthorizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticCreditCardAuthorizationLogRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, ref _alreadyFetchedCreditCardAuthorization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCreditCardAuthorizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="creditCardAuthorizationLogID">PK value for CreditCardAuthorizationLog which data should be fetched into this CreditCardAuthorizationLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 creditCardAuthorizationLogID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationLogID].ForcedCurrentValueWrite(creditCardAuthorizationLogID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCreditCardAuthorizationLogDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CreditCardAuthorizationLogEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CreditCardAuthorizationLogRelations Relations
		{
			get	{ return new CreditCardAuthorizationLogRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorization")[0], (int)VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, 0, null, null, null, "CreditCardAuthorization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreditCardAuthorizationID property of the Entity CreditCardAuthorizationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATIONLOG"."CREDITCARDAUTHORIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CreditCardAuthorizationID
		{
			get { return (System.Int64)GetValue((int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationID, true); }
			set	{ SetValue((int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationID, value, true); }
		}

		/// <summary> The CreditCardAuthorizationLogID property of the Entity CreditCardAuthorizationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATIONLOG"."CREDITCARDAUTHORIZATIONLOGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 CreditCardAuthorizationLogID
		{
			get { return (System.Int64)GetValue((int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationLogID, true); }
			set	{ SetValue((int)CreditCardAuthorizationLogFieldIndex.CreditCardAuthorizationLogID, value, true); }
		}

		/// <summary> The InsertTime property of the Entity CreditCardAuthorizationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATIONLOG"."INSERTTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime InsertTime
		{
			get { return (System.DateTime)GetValue((int)CreditCardAuthorizationLogFieldIndex.InsertTime, true); }
			set	{ SetValue((int)CreditCardAuthorizationLogFieldIndex.InsertTime, value, true); }
		}

		/// <summary> The Request property of the Entity CreditCardAuthorizationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATIONLOG"."REQUEST"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Request
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationLogFieldIndex.Request, true); }
			set	{ SetValue((int)CreditCardAuthorizationLogFieldIndex.Request, value, true); }
		}

		/// <summary> The Response property of the Entity CreditCardAuthorizationLog<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CREDITCARDAUTHORIZATIONLOG"."RESPONSE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Response
		{
			get { return (System.String)GetValue((int)CreditCardAuthorizationLogFieldIndex.Response, true); }
			set	{ SetValue((int)CreditCardAuthorizationLogFieldIndex.Response, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'CreditCardAuthorizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCreditCardAuthorization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CreditCardAuthorizationEntity CreditCardAuthorization
		{
			get	{ return GetSingleCreditCardAuthorization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCreditCardAuthorization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "CreditCardAuthorizationLogs", "CreditCardAuthorization", _creditCardAuthorization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorization. When set to true, CreditCardAuthorization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorization is accessed. You can always execute a forced fetch by calling GetSingleCreditCardAuthorization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorization
		{
			get	{ return _alwaysFetchCreditCardAuthorization; }
			set	{ _alwaysFetchCreditCardAuthorization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorization already has been fetched. Setting this property to false when CreditCardAuthorization has been fetched
		/// will set CreditCardAuthorization to null as well. Setting this property to true while CreditCardAuthorization hasn't been fetched disables lazy loading for CreditCardAuthorization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorization
		{
			get { return _alreadyFetchedCreditCardAuthorization;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorization && !value)
				{
					this.CreditCardAuthorization = null;
				}
				_alreadyFetchedCreditCardAuthorization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CreditCardAuthorization is not found
		/// in the database. When set to true, CreditCardAuthorization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CreditCardAuthorizationReturnsNewIfNotFound
		{
			get	{ return _creditCardAuthorizationReturnsNewIfNotFound; }
			set { _creditCardAuthorizationReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
