﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Net'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class NetEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.LineCollection	_lines;
		private bool	_alwaysFetchLines, _alreadyFetchedLines;
		private VarioSL.Entities.CollectionClasses.StopCollection	_stops;
		private bool	_alwaysFetchStops, _alreadyFetchedStops;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_tariffs;
		private bool	_alwaysFetchTariffs, _alreadyFetchedTariffs;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Lines</summary>
			public static readonly string Lines = "Lines";
			/// <summary>Member name Stops</summary>
			public static readonly string Stops = "Stops";
			/// <summary>Member name Tariffs</summary>
			public static readonly string Tariffs = "Tariffs";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static NetEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public NetEntity() :base("NetEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		public NetEntity(System.Int64 netID):base("NetEntity")
		{
			InitClassFetch(netID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NetEntity(System.Int64 netID, IPrefetchPath prefetchPathToUse):base("NetEntity")
		{
			InitClassFetch(netID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="validator">The custom validator object for this NetEntity</param>
		public NetEntity(System.Int64 netID, IValidator validator):base("NetEntity")
		{
			InitClassFetch(netID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_lines = (VarioSL.Entities.CollectionClasses.LineCollection)info.GetValue("_lines", typeof(VarioSL.Entities.CollectionClasses.LineCollection));
			_alwaysFetchLines = info.GetBoolean("_alwaysFetchLines");
			_alreadyFetchedLines = info.GetBoolean("_alreadyFetchedLines");

			_stops = (VarioSL.Entities.CollectionClasses.StopCollection)info.GetValue("_stops", typeof(VarioSL.Entities.CollectionClasses.StopCollection));
			_alwaysFetchStops = info.GetBoolean("_alwaysFetchStops");
			_alreadyFetchedStops = info.GetBoolean("_alreadyFetchedStops");

			_tariffs = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_tariffs", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchTariffs = info.GetBoolean("_alwaysFetchTariffs");
			_alreadyFetchedTariffs = info.GetBoolean("_alreadyFetchedTariffs");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedLines = (_lines.Count > 0);
			_alreadyFetchedStops = (_stops.Count > 0);
			_alreadyFetchedTariffs = (_tariffs.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Lines":
					toReturn.Add(Relations.LineEntityUsingNetID);
					break;
				case "Stops":
					toReturn.Add(Relations.StopEntityUsingNetId);
					break;
				case "Tariffs":
					toReturn.Add(Relations.TariffEntityUsingNetID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_lines", (!this.MarkedForDeletion?_lines:null));
			info.AddValue("_alwaysFetchLines", _alwaysFetchLines);
			info.AddValue("_alreadyFetchedLines", _alreadyFetchedLines);
			info.AddValue("_stops", (!this.MarkedForDeletion?_stops:null));
			info.AddValue("_alwaysFetchStops", _alwaysFetchStops);
			info.AddValue("_alreadyFetchedStops", _alreadyFetchedStops);
			info.AddValue("_tariffs", (!this.MarkedForDeletion?_tariffs:null));
			info.AddValue("_alwaysFetchTariffs", _alwaysFetchTariffs);
			info.AddValue("_alreadyFetchedTariffs", _alreadyFetchedTariffs);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Lines":
					_alreadyFetchedLines = true;
					if(entity!=null)
					{
						this.Lines.Add((LineEntity)entity);
					}
					break;
				case "Stops":
					_alreadyFetchedStops = true;
					if(entity!=null)
					{
						this.Stops.Add((StopEntity)entity);
					}
					break;
				case "Tariffs":
					_alreadyFetchedTariffs = true;
					if(entity!=null)
					{
						this.Tariffs.Add((TariffEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Lines":
					_lines.Add((LineEntity)relatedEntity);
					break;
				case "Stops":
					_stops.Add((StopEntity)relatedEntity);
					break;
				case "Tariffs":
					_tariffs.Add((TariffEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Lines":
					this.PerformRelatedEntityRemoval(_lines, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Stops":
					this.PerformRelatedEntityRemoval(_stops, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tariffs":
					this.PerformRelatedEntityRemoval(_tariffs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_lines);
			toReturn.Add(_stops);
			toReturn.Add(_tariffs);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 netID)
		{
			return FetchUsingPK(netID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 netID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(netID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 netID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(netID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 netID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(netID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.NetID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new NetRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LineEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineCollection GetMultiLines(bool forceFetch)
		{
			return GetMultiLines(forceFetch, _lines.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'LineEntity'</returns>
		public VarioSL.Entities.CollectionClasses.LineCollection GetMultiLines(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiLines(forceFetch, _lines.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.LineCollection GetMultiLines(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiLines(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.LineCollection GetMultiLines(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedLines || forceFetch || _alwaysFetchLines) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_lines);
				_lines.SuppressClearInGetMulti=!forceFetch;
				_lines.EntityFactoryToUse = entityFactoryToUse;
				_lines.GetMultiManyToOne(this, filter);
				_lines.SuppressClearInGetMulti=false;
				_alreadyFetchedLines = true;
			}
			return _lines;
		}

		/// <summary> Sets the collection parameters for the collection for 'Lines'. These settings will be taken into account
		/// when the property Lines is requested or GetMultiLines is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLines(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_lines.SortClauses=sortClauses;
			_lines.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'StopEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch)
		{
			return GetMultiStops(forceFetch, _stops.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'StopEntity'</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiStops(forceFetch, _stops.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiStops(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.StopCollection GetMultiStops(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedStops || forceFetch || _alwaysFetchStops) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_stops);
				_stops.SuppressClearInGetMulti=!forceFetch;
				_stops.EntityFactoryToUse = entityFactoryToUse;
				_stops.GetMultiManyToOne(this, null, filter);
				_stops.SuppressClearInGetMulti=false;
				_alreadyFetchedStops = true;
			}
			return _stops;
		}

		/// <summary> Sets the collection parameters for the collection for 'Stops'. These settings will be taken into account
		/// when the property Stops is requested or GetMultiStops is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersStops(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_stops.SortClauses=sortClauses;
			_stops.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch)
		{
			return GetMultiTariffs(forceFetch, _tariffs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffs(forceFetch, _tariffs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffs || forceFetch || _alwaysFetchTariffs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffs);
				_tariffs.SuppressClearInGetMulti=!forceFetch;
				_tariffs.EntityFactoryToUse = entityFactoryToUse;
				_tariffs.GetMultiManyToOne(null, null, null, this, null, null, filter);
				_tariffs.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffs = true;
			}
			return _tariffs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tariffs'. These settings will be taken into account
		/// when the property Tariffs is requested or GetMultiTariffs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffs.SortClauses=sortClauses;
			_tariffs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Lines", _lines);
			toReturn.Add("Stops", _stops);
			toReturn.Add("Tariffs", _tariffs);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="validator">The validator object for this NetEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 netID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(netID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_lines = new VarioSL.Entities.CollectionClasses.LineCollection();
			_lines.SetContainingEntityInfo(this, "Net");

			_stops = new VarioSL.Entities.CollectionClasses.StopCollection();
			_stops.SetContainingEntityInfo(this, "Net");

			_tariffs = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_tariffs.SetContainingEntityInfo(this, "Net");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AutoUpdate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreationTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetworkVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Note", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeTableVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="netID">PK value for Net which data should be fetched into this Net object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 netID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)NetFieldIndex.NetID].ForcedCurrentValueWrite(netID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateNetDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new NetEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static NetRelations Relations
		{
			get	{ return new NetRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Line' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLines
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.LineCollection(), (IEntityRelation)GetRelationsForField("Lines")[0], (int)VarioSL.Entities.EntityType.NetEntity, (int)VarioSL.Entities.EntityType.LineEntity, 0, null, null, null, "Lines", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Stop' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathStops
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.StopCollection(), (IEntityRelation)GetRelationsForField("Stops")[0], (int)VarioSL.Entities.EntityType.NetEntity, (int)VarioSL.Entities.EntityType.StopEntity, 0, null, null, null, "Stops", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariffs")[0], (int)VarioSL.Entities.EntityType.NetEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariffs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AutoUpdate property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."AUTOUPDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> AutoUpdate
		{
			get { return (Nullable<System.Boolean>)GetValue((int)NetFieldIndex.AutoUpdate, false); }
			set	{ SetValue((int)NetFieldIndex.AutoUpdate, value, true); }
		}

		/// <summary> The BaseVersion property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."BASEVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BaseVersion
		{
			get { return (System.String)GetValue((int)NetFieldIndex.BaseVersion, true); }
			set	{ SetValue((int)NetFieldIndex.BaseVersion, value, true); }
		}

		/// <summary> The ClientID property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> ClientID
		{
			get { return (Nullable<System.Decimal>)GetValue((int)NetFieldIndex.ClientID, false); }
			set	{ SetValue((int)NetFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CreationDate property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."CREATIONDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetFieldIndex.CreationDate, false); }
			set	{ SetValue((int)NetFieldIndex.CreationDate, value, true); }
		}

		/// <summary> The CreationTime property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."CREATIONTIME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreationTime
		{
			get { return (Nullable<System.DateTime>)GetValue((int)NetFieldIndex.CreationTime, false); }
			set	{ SetValue((int)NetFieldIndex.CreationTime, value, true); }
		}

		/// <summary> The NetID property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."NETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 NetID
		{
			get { return (System.Int64)GetValue((int)NetFieldIndex.NetID, true); }
			set	{ SetValue((int)NetFieldIndex.NetID, value, true); }
		}

		/// <summary> The NetworkVersion property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."NETWORKVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NetworkVersion
		{
			get { return (System.String)GetValue((int)NetFieldIndex.NetworkVersion, true); }
			set	{ SetValue((int)NetFieldIndex.NetworkVersion, value, true); }
		}

		/// <summary> The Note property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."NOTE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Note
		{
			get { return (System.String)GetValue((int)NetFieldIndex.Note, true); }
			set	{ SetValue((int)NetFieldIndex.Note, value, true); }
		}

		/// <summary> The TimeTableVersion property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."TIMETABLEVERSION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TimeTableVersion
		{
			get { return (System.String)GetValue((int)NetFieldIndex.TimeTableVersion, true); }
			set	{ SetValue((int)NetFieldIndex.TimeTableVersion, value, true); }
		}

		/// <summary> The ValidFrom property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."VALIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidFrom
		{
			get { return (System.DateTime)GetValue((int)NetFieldIndex.ValidFrom, true); }
			set	{ SetValue((int)NetFieldIndex.ValidFrom, value, true); }
		}

		/// <summary> The ValidUntil property of the Entity Net<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_NET"."VALIDUNTIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime ValidUntil
		{
			get { return (System.DateTime)GetValue((int)NetFieldIndex.ValidUntil, true); }
			set	{ SetValue((int)NetFieldIndex.ValidUntil, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'LineEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLines()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.LineCollection Lines
		{
			get	{ return GetMultiLines(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Lines. When set to true, Lines is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Lines is accessed. You can always execute/ a forced fetch by calling GetMultiLines(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLines
		{
			get	{ return _alwaysFetchLines; }
			set	{ _alwaysFetchLines = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Lines already has been fetched. Setting this property to false when Lines has been fetched
		/// will clear the Lines collection well. Setting this property to true while Lines hasn't been fetched disables lazy loading for Lines</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLines
		{
			get { return _alreadyFetchedLines;}
			set 
			{
				if(_alreadyFetchedLines && !value && (_lines != null))
				{
					_lines.Clear();
				}
				_alreadyFetchedLines = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'StopEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiStops()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.StopCollection Stops
		{
			get	{ return GetMultiStops(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Stops. When set to true, Stops is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Stops is accessed. You can always execute/ a forced fetch by calling GetMultiStops(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchStops
		{
			get	{ return _alwaysFetchStops; }
			set	{ _alwaysFetchStops = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Stops already has been fetched. Setting this property to false when Stops has been fetched
		/// will clear the Stops collection well. Setting this property to true while Stops hasn't been fetched disables lazy loading for Stops</summary>
		[Browsable(false)]
		public bool AlreadyFetchedStops
		{
			get { return _alreadyFetchedStops;}
			set 
			{
				if(_alreadyFetchedStops && !value && (_stops != null))
				{
					_stops.Clear();
				}
				_alreadyFetchedStops = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection Tariffs
		{
			get	{ return GetMultiTariffs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tariffs. When set to true, Tariffs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariffs is accessed. You can always execute/ a forced fetch by calling GetMultiTariffs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffs
		{
			get	{ return _alwaysFetchTariffs; }
			set	{ _alwaysFetchTariffs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariffs already has been fetched. Setting this property to false when Tariffs has been fetched
		/// will clear the Tariffs collection well. Setting this property to true while Tariffs hasn't been fetched disables lazy loading for Tariffs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffs
		{
			get { return _alreadyFetchedTariffs;}
			set 
			{
				if(_alreadyFetchedTariffs && !value && (_tariffs != null))
				{
					_tariffs.Clear();
				}
				_alreadyFetchedTariffs = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.NetEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
