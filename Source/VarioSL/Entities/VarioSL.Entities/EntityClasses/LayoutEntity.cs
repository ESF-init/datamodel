﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Layout'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class LayoutEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection	_attributeBitmapLayoutObjects;
		private bool	_alwaysFetchAttributeBitmapLayoutObjects, _alreadyFetchedAttributeBitmapLayoutObjects;
		private VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection	_attributeTextLayoutObjects;
		private bool	_alwaysFetchAttributeTextLayoutObjects, _alreadyFetchedAttributeTextLayoutObjects;
		private VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection	_condintionalSubpageLayoutObjects;
		private bool	_alwaysFetchCondintionalSubpageLayoutObjects, _alreadyFetchedCondintionalSubpageLayoutObjects;
		private VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection	_contintionalSubPageLayoutObjectsPage;
		private bool	_alwaysFetchContintionalSubPageLayoutObjectsPage, _alreadyFetchedContintionalSubPageLayoutObjectsPage;
		private VarioSL.Entities.CollectionClasses.EticketCollection	_etickets;
		private bool	_alwaysFetchEtickets, _alreadyFetchedEtickets;
		private VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection	_fixedBitmapLayoutObjects;
		private bool	_alwaysFetchFixedBitmapLayoutObjects, _alreadyFetchedFixedBitmapLayoutObjects;
		private VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection	_fixedTextLayoutObjects;
		private bool	_alwaysFetchFixedTextLayoutObjects, _alreadyFetchedFixedTextLayoutObjects;
		private VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection	_listLayoutObjects;
		private bool	_alwaysFetchListLayoutObjects, _alreadyFetchedListLayoutObjects;
		private VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection	_listLayoutObjectsPage;
		private bool	_alwaysFetchListLayoutObjectsPage, _alreadyFetchedListLayoutObjectsPage;
		private VarioSL.Entities.CollectionClasses.SpecialReceiptCollection	_specialReceipts;
		private bool	_alwaysFetchSpecialReceipts, _alreadyFetchedSpecialReceipts;
		private VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection	_systemFieldBarcodeLayoutObjects;
		private bool	_alwaysFetchSystemFieldBarcodeLayoutObjects, _alreadyFetchedSystemFieldBarcodeLayoutObjects;
		private VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection	_systemFieldTextLayoutoObjects;
		private bool	_alwaysFetchSystemFieldTextLayoutoObjects, _alreadyFetchedSystemFieldTextLayoutoObjects;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsCancellationLayout;
		private bool	_alwaysFetchTicketsCancellationLayout, _alreadyFetchedTicketsCancellationLayout;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsGroupLayout;
		private bool	_alwaysFetchTicketsGroupLayout, _alreadyFetchedTicketsGroupLayout;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsPrintLayout;
		private bool	_alwaysFetchTicketsPrintLayout, _alreadyFetchedTicketsPrintLayout;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsReceiptLayout;
		private bool	_alwaysFetchTicketsReceiptLayout, _alreadyFetchedTicketsReceiptLayout;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesCancellationLayout;
		private bool	_alwaysFetchTicketDeviceclassesCancellationLayout, _alreadyFetchedTicketDeviceclassesCancellationLayout;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesGroupLayout;
		private bool	_alwaysFetchTicketDeviceclassesGroupLayout, _alreadyFetchedTicketDeviceclassesGroupLayout;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesPrintLayout;
		private bool	_alwaysFetchTicketDeviceclassesPrintLayout, _alreadyFetchedTicketDeviceclassesPrintLayout;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceclassesReceiptLayout;
		private bool	_alwaysFetchTicketDeviceclassesReceiptLayout, _alreadyFetchedTicketDeviceclassesReceiptLayout;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private OutputDeviceEntity _outputDevice;
		private bool	_alwaysFetchOutputDevice, _alreadyFetchedOutputDevice, _outputDeviceReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;
		private FormEntity _form;
		private bool	_alwaysFetchForm, _alreadyFetchedForm, _formReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name OutputDevice</summary>
			public static readonly string OutputDevice = "OutputDevice";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name Form</summary>
			public static readonly string Form = "Form";
			/// <summary>Member name AttributeBitmapLayoutObjects</summary>
			public static readonly string AttributeBitmapLayoutObjects = "AttributeBitmapLayoutObjects";
			/// <summary>Member name AttributeTextLayoutObjects</summary>
			public static readonly string AttributeTextLayoutObjects = "AttributeTextLayoutObjects";
			/// <summary>Member name CondintionalSubpageLayoutObjects</summary>
			public static readonly string CondintionalSubpageLayoutObjects = "CondintionalSubpageLayoutObjects";
			/// <summary>Member name ContintionalSubPageLayoutObjectsPage</summary>
			public static readonly string ContintionalSubPageLayoutObjectsPage = "ContintionalSubPageLayoutObjectsPage";
			/// <summary>Member name Etickets</summary>
			public static readonly string Etickets = "Etickets";
			/// <summary>Member name FixedBitmapLayoutObjects</summary>
			public static readonly string FixedBitmapLayoutObjects = "FixedBitmapLayoutObjects";
			/// <summary>Member name FixedTextLayoutObjects</summary>
			public static readonly string FixedTextLayoutObjects = "FixedTextLayoutObjects";
			/// <summary>Member name ListLayoutObjects</summary>
			public static readonly string ListLayoutObjects = "ListLayoutObjects";
			/// <summary>Member name ListLayoutObjectsPage</summary>
			public static readonly string ListLayoutObjectsPage = "ListLayoutObjectsPage";
			/// <summary>Member name SpecialReceipts</summary>
			public static readonly string SpecialReceipts = "SpecialReceipts";
			/// <summary>Member name SystemFieldBarcodeLayoutObjects</summary>
			public static readonly string SystemFieldBarcodeLayoutObjects = "SystemFieldBarcodeLayoutObjects";
			/// <summary>Member name SystemFieldTextLayoutoObjects</summary>
			public static readonly string SystemFieldTextLayoutoObjects = "SystemFieldTextLayoutoObjects";
			/// <summary>Member name TicketsCancellationLayout</summary>
			public static readonly string TicketsCancellationLayout = "TicketsCancellationLayout";
			/// <summary>Member name TicketsGroupLayout</summary>
			public static readonly string TicketsGroupLayout = "TicketsGroupLayout";
			/// <summary>Member name TicketsPrintLayout</summary>
			public static readonly string TicketsPrintLayout = "TicketsPrintLayout";
			/// <summary>Member name TicketsReceiptLayout</summary>
			public static readonly string TicketsReceiptLayout = "TicketsReceiptLayout";
			/// <summary>Member name TicketDeviceclassesCancellationLayout</summary>
			public static readonly string TicketDeviceclassesCancellationLayout = "TicketDeviceclassesCancellationLayout";
			/// <summary>Member name TicketDeviceclassesGroupLayout</summary>
			public static readonly string TicketDeviceclassesGroupLayout = "TicketDeviceclassesGroupLayout";
			/// <summary>Member name TicketDeviceclassesPrintLayout</summary>
			public static readonly string TicketDeviceclassesPrintLayout = "TicketDeviceclassesPrintLayout";
			/// <summary>Member name TicketDeviceclassesReceiptLayout</summary>
			public static readonly string TicketDeviceclassesReceiptLayout = "TicketDeviceclassesReceiptLayout";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LayoutEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public LayoutEntity() :base("LayoutEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		public LayoutEntity(System.Int64 layoutID):base("LayoutEntity")
		{
			InitClassFetch(layoutID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LayoutEntity(System.Int64 layoutID, IPrefetchPath prefetchPathToUse):base("LayoutEntity")
		{
			InitClassFetch(layoutID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="validator">The custom validator object for this LayoutEntity</param>
		public LayoutEntity(System.Int64 layoutID, IValidator validator):base("LayoutEntity")
		{
			InitClassFetch(layoutID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LayoutEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attributeBitmapLayoutObjects = (VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection)info.GetValue("_attributeBitmapLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection));
			_alwaysFetchAttributeBitmapLayoutObjects = info.GetBoolean("_alwaysFetchAttributeBitmapLayoutObjects");
			_alreadyFetchedAttributeBitmapLayoutObjects = info.GetBoolean("_alreadyFetchedAttributeBitmapLayoutObjects");

			_attributeTextLayoutObjects = (VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection)info.GetValue("_attributeTextLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection));
			_alwaysFetchAttributeTextLayoutObjects = info.GetBoolean("_alwaysFetchAttributeTextLayoutObjects");
			_alreadyFetchedAttributeTextLayoutObjects = info.GetBoolean("_alreadyFetchedAttributeTextLayoutObjects");

			_condintionalSubpageLayoutObjects = (VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection)info.GetValue("_condintionalSubpageLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection));
			_alwaysFetchCondintionalSubpageLayoutObjects = info.GetBoolean("_alwaysFetchCondintionalSubpageLayoutObjects");
			_alreadyFetchedCondintionalSubpageLayoutObjects = info.GetBoolean("_alreadyFetchedCondintionalSubpageLayoutObjects");

			_contintionalSubPageLayoutObjectsPage = (VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection)info.GetValue("_contintionalSubPageLayoutObjectsPage", typeof(VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection));
			_alwaysFetchContintionalSubPageLayoutObjectsPage = info.GetBoolean("_alwaysFetchContintionalSubPageLayoutObjectsPage");
			_alreadyFetchedContintionalSubPageLayoutObjectsPage = info.GetBoolean("_alreadyFetchedContintionalSubPageLayoutObjectsPage");

			_etickets = (VarioSL.Entities.CollectionClasses.EticketCollection)info.GetValue("_etickets", typeof(VarioSL.Entities.CollectionClasses.EticketCollection));
			_alwaysFetchEtickets = info.GetBoolean("_alwaysFetchEtickets");
			_alreadyFetchedEtickets = info.GetBoolean("_alreadyFetchedEtickets");

			_fixedBitmapLayoutObjects = (VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection)info.GetValue("_fixedBitmapLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection));
			_alwaysFetchFixedBitmapLayoutObjects = info.GetBoolean("_alwaysFetchFixedBitmapLayoutObjects");
			_alreadyFetchedFixedBitmapLayoutObjects = info.GetBoolean("_alreadyFetchedFixedBitmapLayoutObjects");

			_fixedTextLayoutObjects = (VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection)info.GetValue("_fixedTextLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection));
			_alwaysFetchFixedTextLayoutObjects = info.GetBoolean("_alwaysFetchFixedTextLayoutObjects");
			_alreadyFetchedFixedTextLayoutObjects = info.GetBoolean("_alreadyFetchedFixedTextLayoutObjects");

			_listLayoutObjects = (VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection)info.GetValue("_listLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection));
			_alwaysFetchListLayoutObjects = info.GetBoolean("_alwaysFetchListLayoutObjects");
			_alreadyFetchedListLayoutObjects = info.GetBoolean("_alreadyFetchedListLayoutObjects");

			_listLayoutObjectsPage = (VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection)info.GetValue("_listLayoutObjectsPage", typeof(VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection));
			_alwaysFetchListLayoutObjectsPage = info.GetBoolean("_alwaysFetchListLayoutObjectsPage");
			_alreadyFetchedListLayoutObjectsPage = info.GetBoolean("_alreadyFetchedListLayoutObjectsPage");

			_specialReceipts = (VarioSL.Entities.CollectionClasses.SpecialReceiptCollection)info.GetValue("_specialReceipts", typeof(VarioSL.Entities.CollectionClasses.SpecialReceiptCollection));
			_alwaysFetchSpecialReceipts = info.GetBoolean("_alwaysFetchSpecialReceipts");
			_alreadyFetchedSpecialReceipts = info.GetBoolean("_alreadyFetchedSpecialReceipts");

			_systemFieldBarcodeLayoutObjects = (VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection)info.GetValue("_systemFieldBarcodeLayoutObjects", typeof(VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection));
			_alwaysFetchSystemFieldBarcodeLayoutObjects = info.GetBoolean("_alwaysFetchSystemFieldBarcodeLayoutObjects");
			_alreadyFetchedSystemFieldBarcodeLayoutObjects = info.GetBoolean("_alreadyFetchedSystemFieldBarcodeLayoutObjects");

			_systemFieldTextLayoutoObjects = (VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection)info.GetValue("_systemFieldTextLayoutoObjects", typeof(VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection));
			_alwaysFetchSystemFieldTextLayoutoObjects = info.GetBoolean("_alwaysFetchSystemFieldTextLayoutoObjects");
			_alreadyFetchedSystemFieldTextLayoutoObjects = info.GetBoolean("_alreadyFetchedSystemFieldTextLayoutoObjects");

			_ticketsCancellationLayout = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsCancellationLayout", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsCancellationLayout = info.GetBoolean("_alwaysFetchTicketsCancellationLayout");
			_alreadyFetchedTicketsCancellationLayout = info.GetBoolean("_alreadyFetchedTicketsCancellationLayout");

			_ticketsGroupLayout = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsGroupLayout", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsGroupLayout = info.GetBoolean("_alwaysFetchTicketsGroupLayout");
			_alreadyFetchedTicketsGroupLayout = info.GetBoolean("_alreadyFetchedTicketsGroupLayout");

			_ticketsPrintLayout = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsPrintLayout", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsPrintLayout = info.GetBoolean("_alwaysFetchTicketsPrintLayout");
			_alreadyFetchedTicketsPrintLayout = info.GetBoolean("_alreadyFetchedTicketsPrintLayout");

			_ticketsReceiptLayout = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsReceiptLayout", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsReceiptLayout = info.GetBoolean("_alwaysFetchTicketsReceiptLayout");
			_alreadyFetchedTicketsReceiptLayout = info.GetBoolean("_alreadyFetchedTicketsReceiptLayout");

			_ticketDeviceclassesCancellationLayout = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesCancellationLayout", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesCancellationLayout = info.GetBoolean("_alwaysFetchTicketDeviceclassesCancellationLayout");
			_alreadyFetchedTicketDeviceclassesCancellationLayout = info.GetBoolean("_alreadyFetchedTicketDeviceclassesCancellationLayout");

			_ticketDeviceclassesGroupLayout = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesGroupLayout", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesGroupLayout = info.GetBoolean("_alwaysFetchTicketDeviceclassesGroupLayout");
			_alreadyFetchedTicketDeviceclassesGroupLayout = info.GetBoolean("_alreadyFetchedTicketDeviceclassesGroupLayout");

			_ticketDeviceclassesPrintLayout = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesPrintLayout", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesPrintLayout = info.GetBoolean("_alwaysFetchTicketDeviceclassesPrintLayout");
			_alreadyFetchedTicketDeviceclassesPrintLayout = info.GetBoolean("_alreadyFetchedTicketDeviceclassesPrintLayout");

			_ticketDeviceclassesReceiptLayout = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceclassesReceiptLayout", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceclassesReceiptLayout = info.GetBoolean("_alwaysFetchTicketDeviceclassesReceiptLayout");
			_alreadyFetchedTicketDeviceclassesReceiptLayout = info.GetBoolean("_alreadyFetchedTicketDeviceclassesReceiptLayout");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_outputDevice = (OutputDeviceEntity)info.GetValue("_outputDevice", typeof(OutputDeviceEntity));
			if(_outputDevice!=null)
			{
				_outputDevice.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_outputDeviceReturnsNewIfNotFound = info.GetBoolean("_outputDeviceReturnsNewIfNotFound");
			_alwaysFetchOutputDevice = info.GetBoolean("_alwaysFetchOutputDevice");
			_alreadyFetchedOutputDevice = info.GetBoolean("_alreadyFetchedOutputDevice");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");

			_form = (FormEntity)info.GetValue("_form", typeof(FormEntity));
			if(_form!=null)
			{
				_form.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_formReturnsNewIfNotFound = info.GetBoolean("_formReturnsNewIfNotFound");
			_alwaysFetchForm = info.GetBoolean("_alwaysFetchForm");
			_alreadyFetchedForm = info.GetBoolean("_alreadyFetchedForm");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LayoutFieldIndex)fieldIndex)
			{
				case LayoutFieldIndex.Formid:
					DesetupSyncForm(true, false);
					_alreadyFetchedForm = false;
					break;
				case LayoutFieldIndex.OutDeviceID:
					DesetupSyncOutputDevice(true, false);
					_alreadyFetchedOutputDevice = false;
					break;
				case LayoutFieldIndex.OwnerClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case LayoutFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttributeBitmapLayoutObjects = (_attributeBitmapLayoutObjects.Count > 0);
			_alreadyFetchedAttributeTextLayoutObjects = (_attributeTextLayoutObjects.Count > 0);
			_alreadyFetchedCondintionalSubpageLayoutObjects = (_condintionalSubpageLayoutObjects.Count > 0);
			_alreadyFetchedContintionalSubPageLayoutObjectsPage = (_contintionalSubPageLayoutObjectsPage.Count > 0);
			_alreadyFetchedEtickets = (_etickets.Count > 0);
			_alreadyFetchedFixedBitmapLayoutObjects = (_fixedBitmapLayoutObjects.Count > 0);
			_alreadyFetchedFixedTextLayoutObjects = (_fixedTextLayoutObjects.Count > 0);
			_alreadyFetchedListLayoutObjects = (_listLayoutObjects.Count > 0);
			_alreadyFetchedListLayoutObjectsPage = (_listLayoutObjectsPage.Count > 0);
			_alreadyFetchedSpecialReceipts = (_specialReceipts.Count > 0);
			_alreadyFetchedSystemFieldBarcodeLayoutObjects = (_systemFieldBarcodeLayoutObjects.Count > 0);
			_alreadyFetchedSystemFieldTextLayoutoObjects = (_systemFieldTextLayoutoObjects.Count > 0);
			_alreadyFetchedTicketsCancellationLayout = (_ticketsCancellationLayout.Count > 0);
			_alreadyFetchedTicketsGroupLayout = (_ticketsGroupLayout.Count > 0);
			_alreadyFetchedTicketsPrintLayout = (_ticketsPrintLayout.Count > 0);
			_alreadyFetchedTicketsReceiptLayout = (_ticketsReceiptLayout.Count > 0);
			_alreadyFetchedTicketDeviceclassesCancellationLayout = (_ticketDeviceclassesCancellationLayout.Count > 0);
			_alreadyFetchedTicketDeviceclassesGroupLayout = (_ticketDeviceclassesGroupLayout.Count > 0);
			_alreadyFetchedTicketDeviceclassesPrintLayout = (_ticketDeviceclassesPrintLayout.Count > 0);
			_alreadyFetchedTicketDeviceclassesReceiptLayout = (_ticketDeviceclassesReceiptLayout.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedOutputDevice = (_outputDevice != null);
			_alreadyFetchedTariff = (_tariff != null);
			_alreadyFetchedForm = (_form != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientID);
					break;
				case "OutputDevice":
					toReturn.Add(Relations.OutputDeviceEntityUsingOutDeviceID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "Form":
					toReturn.Add(Relations.FormEntityUsingFormid);
					break;
				case "AttributeBitmapLayoutObjects":
					toReturn.Add(Relations.AttributeBitmapLayoutObjectEntityUsingLayoutID);
					break;
				case "AttributeTextLayoutObjects":
					toReturn.Add(Relations.AttributeTextLayoutObjectEntityUsingLayoutID);
					break;
				case "CondintionalSubpageLayoutObjects":
					toReturn.Add(Relations.ConditionalSubPageLayoutObjectEntityUsingLayoutID);
					break;
				case "ContintionalSubPageLayoutObjectsPage":
					toReturn.Add(Relations.ConditionalSubPageLayoutObjectEntityUsingPageID);
					break;
				case "Etickets":
					toReturn.Add(Relations.EticketEntityUsingEtickLayoutID);
					break;
				case "FixedBitmapLayoutObjects":
					toReturn.Add(Relations.FixedBitmapLayoutObjectEntityUsingLayoutID);
					break;
				case "FixedTextLayoutObjects":
					toReturn.Add(Relations.FixedTextLayoutObjectEntityUsingLayoutID);
					break;
				case "ListLayoutObjects":
					toReturn.Add(Relations.ListLayoutObjectEntityUsingLayoutID);
					break;
				case "ListLayoutObjectsPage":
					toReturn.Add(Relations.ListLayoutObjectEntityUsingPageID);
					break;
				case "SpecialReceipts":
					toReturn.Add(Relations.SpecialReceiptEntityUsingLayoutID);
					break;
				case "SystemFieldBarcodeLayoutObjects":
					toReturn.Add(Relations.SystemFieldBarcodeLayoutObjectEntityUsingLayoutID);
					break;
				case "SystemFieldTextLayoutoObjects":
					toReturn.Add(Relations.SystemFieldTextLayoutObjectEntityUsingLayoutID);
					break;
				case "TicketsCancellationLayout":
					toReturn.Add(Relations.TicketEntityUsingCancellationLayoutID);
					break;
				case "TicketsGroupLayout":
					toReturn.Add(Relations.TicketEntityUsingGroupLayoutID);
					break;
				case "TicketsPrintLayout":
					toReturn.Add(Relations.TicketEntityUsingPrintLayoutID);
					break;
				case "TicketsReceiptLayout":
					toReturn.Add(Relations.TicketEntityUsingReceiptLayoutID);
					break;
				case "TicketDeviceclassesCancellationLayout":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingCancellationLayoutID);
					break;
				case "TicketDeviceclassesGroupLayout":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingGroupLayoutID);
					break;
				case "TicketDeviceclassesPrintLayout":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingPrintLayoutID);
					break;
				case "TicketDeviceclassesReceiptLayout":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingReceiptLayoutID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attributeBitmapLayoutObjects", (!this.MarkedForDeletion?_attributeBitmapLayoutObjects:null));
			info.AddValue("_alwaysFetchAttributeBitmapLayoutObjects", _alwaysFetchAttributeBitmapLayoutObjects);
			info.AddValue("_alreadyFetchedAttributeBitmapLayoutObjects", _alreadyFetchedAttributeBitmapLayoutObjects);
			info.AddValue("_attributeTextLayoutObjects", (!this.MarkedForDeletion?_attributeTextLayoutObjects:null));
			info.AddValue("_alwaysFetchAttributeTextLayoutObjects", _alwaysFetchAttributeTextLayoutObjects);
			info.AddValue("_alreadyFetchedAttributeTextLayoutObjects", _alreadyFetchedAttributeTextLayoutObjects);
			info.AddValue("_condintionalSubpageLayoutObjects", (!this.MarkedForDeletion?_condintionalSubpageLayoutObjects:null));
			info.AddValue("_alwaysFetchCondintionalSubpageLayoutObjects", _alwaysFetchCondintionalSubpageLayoutObjects);
			info.AddValue("_alreadyFetchedCondintionalSubpageLayoutObjects", _alreadyFetchedCondintionalSubpageLayoutObjects);
			info.AddValue("_contintionalSubPageLayoutObjectsPage", (!this.MarkedForDeletion?_contintionalSubPageLayoutObjectsPage:null));
			info.AddValue("_alwaysFetchContintionalSubPageLayoutObjectsPage", _alwaysFetchContintionalSubPageLayoutObjectsPage);
			info.AddValue("_alreadyFetchedContintionalSubPageLayoutObjectsPage", _alreadyFetchedContintionalSubPageLayoutObjectsPage);
			info.AddValue("_etickets", (!this.MarkedForDeletion?_etickets:null));
			info.AddValue("_alwaysFetchEtickets", _alwaysFetchEtickets);
			info.AddValue("_alreadyFetchedEtickets", _alreadyFetchedEtickets);
			info.AddValue("_fixedBitmapLayoutObjects", (!this.MarkedForDeletion?_fixedBitmapLayoutObjects:null));
			info.AddValue("_alwaysFetchFixedBitmapLayoutObjects", _alwaysFetchFixedBitmapLayoutObjects);
			info.AddValue("_alreadyFetchedFixedBitmapLayoutObjects", _alreadyFetchedFixedBitmapLayoutObjects);
			info.AddValue("_fixedTextLayoutObjects", (!this.MarkedForDeletion?_fixedTextLayoutObjects:null));
			info.AddValue("_alwaysFetchFixedTextLayoutObjects", _alwaysFetchFixedTextLayoutObjects);
			info.AddValue("_alreadyFetchedFixedTextLayoutObjects", _alreadyFetchedFixedTextLayoutObjects);
			info.AddValue("_listLayoutObjects", (!this.MarkedForDeletion?_listLayoutObjects:null));
			info.AddValue("_alwaysFetchListLayoutObjects", _alwaysFetchListLayoutObjects);
			info.AddValue("_alreadyFetchedListLayoutObjects", _alreadyFetchedListLayoutObjects);
			info.AddValue("_listLayoutObjectsPage", (!this.MarkedForDeletion?_listLayoutObjectsPage:null));
			info.AddValue("_alwaysFetchListLayoutObjectsPage", _alwaysFetchListLayoutObjectsPage);
			info.AddValue("_alreadyFetchedListLayoutObjectsPage", _alreadyFetchedListLayoutObjectsPage);
			info.AddValue("_specialReceipts", (!this.MarkedForDeletion?_specialReceipts:null));
			info.AddValue("_alwaysFetchSpecialReceipts", _alwaysFetchSpecialReceipts);
			info.AddValue("_alreadyFetchedSpecialReceipts", _alreadyFetchedSpecialReceipts);
			info.AddValue("_systemFieldBarcodeLayoutObjects", (!this.MarkedForDeletion?_systemFieldBarcodeLayoutObjects:null));
			info.AddValue("_alwaysFetchSystemFieldBarcodeLayoutObjects", _alwaysFetchSystemFieldBarcodeLayoutObjects);
			info.AddValue("_alreadyFetchedSystemFieldBarcodeLayoutObjects", _alreadyFetchedSystemFieldBarcodeLayoutObjects);
			info.AddValue("_systemFieldTextLayoutoObjects", (!this.MarkedForDeletion?_systemFieldTextLayoutoObjects:null));
			info.AddValue("_alwaysFetchSystemFieldTextLayoutoObjects", _alwaysFetchSystemFieldTextLayoutoObjects);
			info.AddValue("_alreadyFetchedSystemFieldTextLayoutoObjects", _alreadyFetchedSystemFieldTextLayoutoObjects);
			info.AddValue("_ticketsCancellationLayout", (!this.MarkedForDeletion?_ticketsCancellationLayout:null));
			info.AddValue("_alwaysFetchTicketsCancellationLayout", _alwaysFetchTicketsCancellationLayout);
			info.AddValue("_alreadyFetchedTicketsCancellationLayout", _alreadyFetchedTicketsCancellationLayout);
			info.AddValue("_ticketsGroupLayout", (!this.MarkedForDeletion?_ticketsGroupLayout:null));
			info.AddValue("_alwaysFetchTicketsGroupLayout", _alwaysFetchTicketsGroupLayout);
			info.AddValue("_alreadyFetchedTicketsGroupLayout", _alreadyFetchedTicketsGroupLayout);
			info.AddValue("_ticketsPrintLayout", (!this.MarkedForDeletion?_ticketsPrintLayout:null));
			info.AddValue("_alwaysFetchTicketsPrintLayout", _alwaysFetchTicketsPrintLayout);
			info.AddValue("_alreadyFetchedTicketsPrintLayout", _alreadyFetchedTicketsPrintLayout);
			info.AddValue("_ticketsReceiptLayout", (!this.MarkedForDeletion?_ticketsReceiptLayout:null));
			info.AddValue("_alwaysFetchTicketsReceiptLayout", _alwaysFetchTicketsReceiptLayout);
			info.AddValue("_alreadyFetchedTicketsReceiptLayout", _alreadyFetchedTicketsReceiptLayout);
			info.AddValue("_ticketDeviceclassesCancellationLayout", (!this.MarkedForDeletion?_ticketDeviceclassesCancellationLayout:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesCancellationLayout", _alwaysFetchTicketDeviceclassesCancellationLayout);
			info.AddValue("_alreadyFetchedTicketDeviceclassesCancellationLayout", _alreadyFetchedTicketDeviceclassesCancellationLayout);
			info.AddValue("_ticketDeviceclassesGroupLayout", (!this.MarkedForDeletion?_ticketDeviceclassesGroupLayout:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesGroupLayout", _alwaysFetchTicketDeviceclassesGroupLayout);
			info.AddValue("_alreadyFetchedTicketDeviceclassesGroupLayout", _alreadyFetchedTicketDeviceclassesGroupLayout);
			info.AddValue("_ticketDeviceclassesPrintLayout", (!this.MarkedForDeletion?_ticketDeviceclassesPrintLayout:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesPrintLayout", _alwaysFetchTicketDeviceclassesPrintLayout);
			info.AddValue("_alreadyFetchedTicketDeviceclassesPrintLayout", _alreadyFetchedTicketDeviceclassesPrintLayout);
			info.AddValue("_ticketDeviceclassesReceiptLayout", (!this.MarkedForDeletion?_ticketDeviceclassesReceiptLayout:null));
			info.AddValue("_alwaysFetchTicketDeviceclassesReceiptLayout", _alwaysFetchTicketDeviceclassesReceiptLayout);
			info.AddValue("_alreadyFetchedTicketDeviceclassesReceiptLayout", _alreadyFetchedTicketDeviceclassesReceiptLayout);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_outputDevice", (!this.MarkedForDeletion?_outputDevice:null));
			info.AddValue("_outputDeviceReturnsNewIfNotFound", _outputDeviceReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOutputDevice", _alwaysFetchOutputDevice);
			info.AddValue("_alreadyFetchedOutputDevice", _alreadyFetchedOutputDevice);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);
			info.AddValue("_form", (!this.MarkedForDeletion?_form:null));
			info.AddValue("_formReturnsNewIfNotFound", _formReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchForm", _alwaysFetchForm);
			info.AddValue("_alreadyFetchedForm", _alreadyFetchedForm);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "OutputDevice":
					_alreadyFetchedOutputDevice = true;
					this.OutputDevice = (OutputDeviceEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "Form":
					_alreadyFetchedForm = true;
					this.Form = (FormEntity)entity;
					break;
				case "AttributeBitmapLayoutObjects":
					_alreadyFetchedAttributeBitmapLayoutObjects = true;
					if(entity!=null)
					{
						this.AttributeBitmapLayoutObjects.Add((AttributeBitmapLayoutObjectEntity)entity);
					}
					break;
				case "AttributeTextLayoutObjects":
					_alreadyFetchedAttributeTextLayoutObjects = true;
					if(entity!=null)
					{
						this.AttributeTextLayoutObjects.Add((AttributeTextLayoutObjectEntity)entity);
					}
					break;
				case "CondintionalSubpageLayoutObjects":
					_alreadyFetchedCondintionalSubpageLayoutObjects = true;
					if(entity!=null)
					{
						this.CondintionalSubpageLayoutObjects.Add((ConditionalSubPageLayoutObjectEntity)entity);
					}
					break;
				case "ContintionalSubPageLayoutObjectsPage":
					_alreadyFetchedContintionalSubPageLayoutObjectsPage = true;
					if(entity!=null)
					{
						this.ContintionalSubPageLayoutObjectsPage.Add((ConditionalSubPageLayoutObjectEntity)entity);
					}
					break;
				case "Etickets":
					_alreadyFetchedEtickets = true;
					if(entity!=null)
					{
						this.Etickets.Add((EticketEntity)entity);
					}
					break;
				case "FixedBitmapLayoutObjects":
					_alreadyFetchedFixedBitmapLayoutObjects = true;
					if(entity!=null)
					{
						this.FixedBitmapLayoutObjects.Add((FixedBitmapLayoutObjectEntity)entity);
					}
					break;
				case "FixedTextLayoutObjects":
					_alreadyFetchedFixedTextLayoutObjects = true;
					if(entity!=null)
					{
						this.FixedTextLayoutObjects.Add((FixedTextLayoutObjectEntity)entity);
					}
					break;
				case "ListLayoutObjects":
					_alreadyFetchedListLayoutObjects = true;
					if(entity!=null)
					{
						this.ListLayoutObjects.Add((ListLayoutObjectEntity)entity);
					}
					break;
				case "ListLayoutObjectsPage":
					_alreadyFetchedListLayoutObjectsPage = true;
					if(entity!=null)
					{
						this.ListLayoutObjectsPage.Add((ListLayoutObjectEntity)entity);
					}
					break;
				case "SpecialReceipts":
					_alreadyFetchedSpecialReceipts = true;
					if(entity!=null)
					{
						this.SpecialReceipts.Add((SpecialReceiptEntity)entity);
					}
					break;
				case "SystemFieldBarcodeLayoutObjects":
					_alreadyFetchedSystemFieldBarcodeLayoutObjects = true;
					if(entity!=null)
					{
						this.SystemFieldBarcodeLayoutObjects.Add((SystemFieldBarcodeLayoutObjectEntity)entity);
					}
					break;
				case "SystemFieldTextLayoutoObjects":
					_alreadyFetchedSystemFieldTextLayoutoObjects = true;
					if(entity!=null)
					{
						this.SystemFieldTextLayoutoObjects.Add((SystemFieldTextLayoutObjectEntity)entity);
					}
					break;
				case "TicketsCancellationLayout":
					_alreadyFetchedTicketsCancellationLayout = true;
					if(entity!=null)
					{
						this.TicketsCancellationLayout.Add((TicketEntity)entity);
					}
					break;
				case "TicketsGroupLayout":
					_alreadyFetchedTicketsGroupLayout = true;
					if(entity!=null)
					{
						this.TicketsGroupLayout.Add((TicketEntity)entity);
					}
					break;
				case "TicketsPrintLayout":
					_alreadyFetchedTicketsPrintLayout = true;
					if(entity!=null)
					{
						this.TicketsPrintLayout.Add((TicketEntity)entity);
					}
					break;
				case "TicketsReceiptLayout":
					_alreadyFetchedTicketsReceiptLayout = true;
					if(entity!=null)
					{
						this.TicketsReceiptLayout.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceclassesCancellationLayout":
					_alreadyFetchedTicketDeviceclassesCancellationLayout = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesCancellationLayout.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "TicketDeviceclassesGroupLayout":
					_alreadyFetchedTicketDeviceclassesGroupLayout = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesGroupLayout.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "TicketDeviceclassesPrintLayout":
					_alreadyFetchedTicketDeviceclassesPrintLayout = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesPrintLayout.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "TicketDeviceclassesReceiptLayout":
					_alreadyFetchedTicketDeviceclassesReceiptLayout = true;
					if(entity!=null)
					{
						this.TicketDeviceclassesReceiptLayout.Add((TicketDeviceClassEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "OutputDevice":
					SetupSyncOutputDevice(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "Form":
					SetupSyncForm(relatedEntity);
					break;
				case "AttributeBitmapLayoutObjects":
					_attributeBitmapLayoutObjects.Add((AttributeBitmapLayoutObjectEntity)relatedEntity);
					break;
				case "AttributeTextLayoutObjects":
					_attributeTextLayoutObjects.Add((AttributeTextLayoutObjectEntity)relatedEntity);
					break;
				case "CondintionalSubpageLayoutObjects":
					_condintionalSubpageLayoutObjects.Add((ConditionalSubPageLayoutObjectEntity)relatedEntity);
					break;
				case "ContintionalSubPageLayoutObjectsPage":
					_contintionalSubPageLayoutObjectsPage.Add((ConditionalSubPageLayoutObjectEntity)relatedEntity);
					break;
				case "Etickets":
					_etickets.Add((EticketEntity)relatedEntity);
					break;
				case "FixedBitmapLayoutObjects":
					_fixedBitmapLayoutObjects.Add((FixedBitmapLayoutObjectEntity)relatedEntity);
					break;
				case "FixedTextLayoutObjects":
					_fixedTextLayoutObjects.Add((FixedTextLayoutObjectEntity)relatedEntity);
					break;
				case "ListLayoutObjects":
					_listLayoutObjects.Add((ListLayoutObjectEntity)relatedEntity);
					break;
				case "ListLayoutObjectsPage":
					_listLayoutObjectsPage.Add((ListLayoutObjectEntity)relatedEntity);
					break;
				case "SpecialReceipts":
					_specialReceipts.Add((SpecialReceiptEntity)relatedEntity);
					break;
				case "SystemFieldBarcodeLayoutObjects":
					_systemFieldBarcodeLayoutObjects.Add((SystemFieldBarcodeLayoutObjectEntity)relatedEntity);
					break;
				case "SystemFieldTextLayoutoObjects":
					_systemFieldTextLayoutoObjects.Add((SystemFieldTextLayoutObjectEntity)relatedEntity);
					break;
				case "TicketsCancellationLayout":
					_ticketsCancellationLayout.Add((TicketEntity)relatedEntity);
					break;
				case "TicketsGroupLayout":
					_ticketsGroupLayout.Add((TicketEntity)relatedEntity);
					break;
				case "TicketsPrintLayout":
					_ticketsPrintLayout.Add((TicketEntity)relatedEntity);
					break;
				case "TicketsReceiptLayout":
					_ticketsReceiptLayout.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceclassesCancellationLayout":
					_ticketDeviceclassesCancellationLayout.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "TicketDeviceclassesGroupLayout":
					_ticketDeviceclassesGroupLayout.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "TicketDeviceclassesPrintLayout":
					_ticketDeviceclassesPrintLayout.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "TicketDeviceclassesReceiptLayout":
					_ticketDeviceclassesReceiptLayout.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "OutputDevice":
					DesetupSyncOutputDevice(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "Form":
					DesetupSyncForm(false, true);
					break;
				case "AttributeBitmapLayoutObjects":
					this.PerformRelatedEntityRemoval(_attributeBitmapLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AttributeTextLayoutObjects":
					this.PerformRelatedEntityRemoval(_attributeTextLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CondintionalSubpageLayoutObjects":
					this.PerformRelatedEntityRemoval(_condintionalSubpageLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ContintionalSubPageLayoutObjectsPage":
					this.PerformRelatedEntityRemoval(_contintionalSubPageLayoutObjectsPage, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Etickets":
					this.PerformRelatedEntityRemoval(_etickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FixedBitmapLayoutObjects":
					this.PerformRelatedEntityRemoval(_fixedBitmapLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FixedTextLayoutObjects":
					this.PerformRelatedEntityRemoval(_fixedTextLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ListLayoutObjects":
					this.PerformRelatedEntityRemoval(_listLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ListLayoutObjectsPage":
					this.PerformRelatedEntityRemoval(_listLayoutObjectsPage, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SpecialReceipts":
					this.PerformRelatedEntityRemoval(_specialReceipts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SystemFieldBarcodeLayoutObjects":
					this.PerformRelatedEntityRemoval(_systemFieldBarcodeLayoutObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SystemFieldTextLayoutoObjects":
					this.PerformRelatedEntityRemoval(_systemFieldTextLayoutoObjects, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsCancellationLayout":
					this.PerformRelatedEntityRemoval(_ticketsCancellationLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsGroupLayout":
					this.PerformRelatedEntityRemoval(_ticketsGroupLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsPrintLayout":
					this.PerformRelatedEntityRemoval(_ticketsPrintLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsReceiptLayout":
					this.PerformRelatedEntityRemoval(_ticketsReceiptLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesCancellationLayout":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesCancellationLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesGroupLayout":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesGroupLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesPrintLayout":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesPrintLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceclassesReceiptLayout":
					this.PerformRelatedEntityRemoval(_ticketDeviceclassesReceiptLayout, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_outputDevice!=null)
			{
				toReturn.Add(_outputDevice);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			if(_form!=null)
			{
				toReturn.Add(_form);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_attributeBitmapLayoutObjects);
			toReturn.Add(_attributeTextLayoutObjects);
			toReturn.Add(_condintionalSubpageLayoutObjects);
			toReturn.Add(_contintionalSubPageLayoutObjectsPage);
			toReturn.Add(_etickets);
			toReturn.Add(_fixedBitmapLayoutObjects);
			toReturn.Add(_fixedTextLayoutObjects);
			toReturn.Add(_listLayoutObjects);
			toReturn.Add(_listLayoutObjectsPage);
			toReturn.Add(_specialReceipts);
			toReturn.Add(_systemFieldBarcodeLayoutObjects);
			toReturn.Add(_systemFieldTextLayoutoObjects);
			toReturn.Add(_ticketsCancellationLayout);
			toReturn.Add(_ticketsGroupLayout);
			toReturn.Add(_ticketsPrintLayout);
			toReturn.Add(_ticketsReceiptLayout);
			toReturn.Add(_ticketDeviceclassesCancellationLayout);
			toReturn.Add(_ticketDeviceclassesGroupLayout);
			toReturn.Add(_ticketDeviceclassesPrintLayout);
			toReturn.Add(_ticketDeviceclassesReceiptLayout);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID)
		{
			return FetchUsingPK(layoutID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(layoutID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(layoutID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(layoutID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.LayoutID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LayoutRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, _attributeBitmapLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, _attributeBitmapLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeBitmapLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection GetMultiAttributeBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeBitmapLayoutObjects || forceFetch || _alwaysFetchAttributeBitmapLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeBitmapLayoutObjects);
				_attributeBitmapLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_attributeBitmapLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_attributeBitmapLayoutObjects.GetMultiManyToOne(null, this, filter);
				_attributeBitmapLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeBitmapLayoutObjects = true;
			}
			return _attributeBitmapLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeBitmapLayoutObjects'. These settings will be taken into account
		/// when the property AttributeBitmapLayoutObjects is requested or GetMultiAttributeBitmapLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeBitmapLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeBitmapLayoutObjects.SortClauses=sortClauses;
			_attributeBitmapLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AttributeTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, _attributeTextLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AttributeTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, _attributeTextLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAttributeTextLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection GetMultiAttributeTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAttributeTextLayoutObjects || forceFetch || _alwaysFetchAttributeTextLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_attributeTextLayoutObjects);
				_attributeTextLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_attributeTextLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_attributeTextLayoutObjects.GetMultiManyToOne(null, this, filter);
				_attributeTextLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedAttributeTextLayoutObjects = true;
			}
			return _attributeTextLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'AttributeTextLayoutObjects'. These settings will be taken into account
		/// when the property AttributeTextLayoutObjects is requested or GetMultiAttributeTextLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAttributeTextLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_attributeTextLayoutObjects.SortClauses=sortClauses;
			_attributeTextLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ConditionalSubPageLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiCondintionalSubpageLayoutObjects(bool forceFetch)
		{
			return GetMultiCondintionalSubpageLayoutObjects(forceFetch, _condintionalSubpageLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ConditionalSubPageLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiCondintionalSubpageLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCondintionalSubpageLayoutObjects(forceFetch, _condintionalSubpageLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiCondintionalSubpageLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCondintionalSubpageLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiCondintionalSubpageLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCondintionalSubpageLayoutObjects || forceFetch || _alwaysFetchCondintionalSubpageLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_condintionalSubpageLayoutObjects);
				_condintionalSubpageLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_condintionalSubpageLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_condintionalSubpageLayoutObjects.GetMultiManyToOne(this, null, filter);
				_condintionalSubpageLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedCondintionalSubpageLayoutObjects = true;
			}
			return _condintionalSubpageLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'CondintionalSubpageLayoutObjects'. These settings will be taken into account
		/// when the property CondintionalSubpageLayoutObjects is requested or GetMultiCondintionalSubpageLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCondintionalSubpageLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_condintionalSubpageLayoutObjects.SortClauses=sortClauses;
			_condintionalSubpageLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ConditionalSubPageLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiContintionalSubPageLayoutObjectsPage(bool forceFetch)
		{
			return GetMultiContintionalSubPageLayoutObjectsPage(forceFetch, _contintionalSubPageLayoutObjectsPage.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ConditionalSubPageLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiContintionalSubPageLayoutObjectsPage(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiContintionalSubPageLayoutObjectsPage(forceFetch, _contintionalSubPageLayoutObjectsPage.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiContintionalSubPageLayoutObjectsPage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiContintionalSubPageLayoutObjectsPage(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection GetMultiContintionalSubPageLayoutObjectsPage(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedContintionalSubPageLayoutObjectsPage || forceFetch || _alwaysFetchContintionalSubPageLayoutObjectsPage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_contintionalSubPageLayoutObjectsPage);
				_contintionalSubPageLayoutObjectsPage.SuppressClearInGetMulti=!forceFetch;
				_contintionalSubPageLayoutObjectsPage.EntityFactoryToUse = entityFactoryToUse;
				_contintionalSubPageLayoutObjectsPage.GetMultiManyToOne(null, this, filter);
				_contintionalSubPageLayoutObjectsPage.SuppressClearInGetMulti=false;
				_alreadyFetchedContintionalSubPageLayoutObjectsPage = true;
			}
			return _contintionalSubPageLayoutObjectsPage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ContintionalSubPageLayoutObjectsPage'. These settings will be taken into account
		/// when the property ContintionalSubPageLayoutObjectsPage is requested or GetMultiContintionalSubPageLayoutObjectsPage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersContintionalSubPageLayoutObjectsPage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_contintionalSubPageLayoutObjectsPage.SortClauses=sortClauses;
			_contintionalSubPageLayoutObjectsPage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EticketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch)
		{
			return GetMultiEtickets(forceFetch, _etickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'EticketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiEtickets(forceFetch, _etickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiEtickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.EticketCollection GetMultiEtickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedEtickets || forceFetch || _alwaysFetchEtickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_etickets);
				_etickets.SuppressClearInGetMulti=!forceFetch;
				_etickets.EntityFactoryToUse = entityFactoryToUse;
				_etickets.GetMultiManyToOne(this, null, filter);
				_etickets.SuppressClearInGetMulti=false;
				_alreadyFetchedEtickets = true;
			}
			return _etickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Etickets'. These settings will be taken into account
		/// when the property Etickets is requested or GetMultiEtickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEtickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_etickets.SortClauses=sortClauses;
			_etickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FixedBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, _fixedBitmapLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FixedBitmapLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, _fixedBitmapLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFixedBitmapLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection GetMultiFixedBitmapLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFixedBitmapLayoutObjects || forceFetch || _alwaysFetchFixedBitmapLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fixedBitmapLayoutObjects);
				_fixedBitmapLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_fixedBitmapLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_fixedBitmapLayoutObjects.GetMultiManyToOne(this, null, filter);
				_fixedBitmapLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedFixedBitmapLayoutObjects = true;
			}
			return _fixedBitmapLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'FixedBitmapLayoutObjects'. These settings will be taken into account
		/// when the property FixedBitmapLayoutObjects is requested or GetMultiFixedBitmapLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFixedBitmapLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fixedBitmapLayoutObjects.SortClauses=sortClauses;
			_fixedBitmapLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FixedTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FixedTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection GetMultiFixedTextLayoutObjects(bool forceFetch)
		{
			return GetMultiFixedTextLayoutObjects(forceFetch, _fixedTextLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FixedTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection GetMultiFixedTextLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFixedTextLayoutObjects(forceFetch, _fixedTextLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FixedTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection GetMultiFixedTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFixedTextLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FixedTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection GetMultiFixedTextLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFixedTextLayoutObjects || forceFetch || _alwaysFetchFixedTextLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fixedTextLayoutObjects);
				_fixedTextLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_fixedTextLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_fixedTextLayoutObjects.GetMultiManyToOne(this, filter);
				_fixedTextLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedFixedTextLayoutObjects = true;
			}
			return _fixedTextLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'FixedTextLayoutObjects'. These settings will be taken into account
		/// when the property FixedTextLayoutObjects is requested or GetMultiFixedTextLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFixedTextLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fixedTextLayoutObjects.SortClauses=sortClauses;
			_fixedTextLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch)
		{
			return GetMultiListLayoutObjects(forceFetch, _listLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiListLayoutObjects(forceFetch, _listLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiListLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedListLayoutObjects || forceFetch || _alwaysFetchListLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_listLayoutObjects);
				_listLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_listLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_listLayoutObjects.GetMultiManyToOne(this, null, null, filter);
				_listLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedListLayoutObjects = true;
			}
			return _listLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'ListLayoutObjects'. These settings will be taken into account
		/// when the property ListLayoutObjects is requested or GetMultiListLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersListLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_listLayoutObjects.SortClauses=sortClauses;
			_listLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjectsPage(bool forceFetch)
		{
			return GetMultiListLayoutObjectsPage(forceFetch, _listLayoutObjectsPage.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ListLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjectsPage(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiListLayoutObjectsPage(forceFetch, _listLayoutObjectsPage.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjectsPage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiListLayoutObjectsPage(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection GetMultiListLayoutObjectsPage(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedListLayoutObjectsPage || forceFetch || _alwaysFetchListLayoutObjectsPage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_listLayoutObjectsPage);
				_listLayoutObjectsPage.SuppressClearInGetMulti=!forceFetch;
				_listLayoutObjectsPage.EntityFactoryToUse = entityFactoryToUse;
				_listLayoutObjectsPage.GetMultiManyToOne(null, this, null, filter);
				_listLayoutObjectsPage.SuppressClearInGetMulti=false;
				_alreadyFetchedListLayoutObjectsPage = true;
			}
			return _listLayoutObjectsPage;
		}

		/// <summary> Sets the collection parameters for the collection for 'ListLayoutObjectsPage'. These settings will be taken into account
		/// when the property ListLayoutObjectsPage is requested or GetMultiListLayoutObjectsPage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersListLayoutObjectsPage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_listLayoutObjectsPage.SortClauses=sortClauses;
			_listLayoutObjectsPage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSpecialReceipts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSpecialReceipts || forceFetch || _alwaysFetchSpecialReceipts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_specialReceipts);
				_specialReceipts.SuppressClearInGetMulti=!forceFetch;
				_specialReceipts.EntityFactoryToUse = entityFactoryToUse;
				_specialReceipts.GetMultiManyToOne(null, this, null, filter);
				_specialReceipts.SuppressClearInGetMulti=false;
				_alreadyFetchedSpecialReceipts = true;
			}
			return _specialReceipts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SpecialReceipts'. These settings will be taken into account
		/// when the property SpecialReceipts is requested or GetMultiSpecialReceipts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSpecialReceipts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_specialReceipts.SortClauses=sortClauses;
			_specialReceipts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldBarcodeLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, _systemFieldBarcodeLayoutObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldBarcodeLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, _systemFieldBarcodeLayoutObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSystemFieldBarcodeLayoutObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection GetMultiSystemFieldBarcodeLayoutObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSystemFieldBarcodeLayoutObjects || forceFetch || _alwaysFetchSystemFieldBarcodeLayoutObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_systemFieldBarcodeLayoutObjects);
				_systemFieldBarcodeLayoutObjects.SuppressClearInGetMulti=!forceFetch;
				_systemFieldBarcodeLayoutObjects.EntityFactoryToUse = entityFactoryToUse;
				_systemFieldBarcodeLayoutObjects.GetMultiManyToOne(this, null, filter);
				_systemFieldBarcodeLayoutObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedSystemFieldBarcodeLayoutObjects = true;
			}
			return _systemFieldBarcodeLayoutObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'SystemFieldBarcodeLayoutObjects'. These settings will be taken into account
		/// when the property SystemFieldBarcodeLayoutObjects is requested or GetMultiSystemFieldBarcodeLayoutObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSystemFieldBarcodeLayoutObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_systemFieldBarcodeLayoutObjects.SortClauses=sortClauses;
			_systemFieldBarcodeLayoutObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutoObjects(bool forceFetch)
		{
			return GetMultiSystemFieldTextLayoutoObjects(forceFetch, _systemFieldTextLayoutoObjects.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SystemFieldTextLayoutObjectEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutoObjects(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSystemFieldTextLayoutoObjects(forceFetch, _systemFieldTextLayoutoObjects.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutoObjects(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSystemFieldTextLayoutoObjects(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection GetMultiSystemFieldTextLayoutoObjects(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSystemFieldTextLayoutoObjects || forceFetch || _alwaysFetchSystemFieldTextLayoutoObjects) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_systemFieldTextLayoutoObjects);
				_systemFieldTextLayoutoObjects.SuppressClearInGetMulti=!forceFetch;
				_systemFieldTextLayoutoObjects.EntityFactoryToUse = entityFactoryToUse;
				_systemFieldTextLayoutoObjects.GetMultiManyToOne(this, null, filter);
				_systemFieldTextLayoutoObjects.SuppressClearInGetMulti=false;
				_alreadyFetchedSystemFieldTextLayoutoObjects = true;
			}
			return _systemFieldTextLayoutoObjects;
		}

		/// <summary> Sets the collection parameters for the collection for 'SystemFieldTextLayoutoObjects'. These settings will be taken into account
		/// when the property SystemFieldTextLayoutoObjects is requested or GetMultiSystemFieldTextLayoutoObjects is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSystemFieldTextLayoutoObjects(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_systemFieldTextLayoutoObjects.SortClauses=sortClauses;
			_systemFieldTextLayoutoObjects.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsCancellationLayout(bool forceFetch)
		{
			return GetMultiTicketsCancellationLayout(forceFetch, _ticketsCancellationLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsCancellationLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsCancellationLayout(forceFetch, _ticketsCancellationLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsCancellationLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsCancellationLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsCancellationLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsCancellationLayout || forceFetch || _alwaysFetchTicketsCancellationLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsCancellationLayout);
				_ticketsCancellationLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketsCancellationLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketsCancellationLayout.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsCancellationLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsCancellationLayout = true;
			}
			return _ticketsCancellationLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsCancellationLayout'. These settings will be taken into account
		/// when the property TicketsCancellationLayout is requested or GetMultiTicketsCancellationLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsCancellationLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsCancellationLayout.SortClauses=sortClauses;
			_ticketsCancellationLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsGroupLayout(bool forceFetch)
		{
			return GetMultiTicketsGroupLayout(forceFetch, _ticketsGroupLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsGroupLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsGroupLayout(forceFetch, _ticketsGroupLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsGroupLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsGroupLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsGroupLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsGroupLayout || forceFetch || _alwaysFetchTicketsGroupLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsGroupLayout);
				_ticketsGroupLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketsGroupLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketsGroupLayout.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsGroupLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsGroupLayout = true;
			}
			return _ticketsGroupLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsGroupLayout'. These settings will be taken into account
		/// when the property TicketsGroupLayout is requested or GetMultiTicketsGroupLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsGroupLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsGroupLayout.SortClauses=sortClauses;
			_ticketsGroupLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsPrintLayout(bool forceFetch)
		{
			return GetMultiTicketsPrintLayout(forceFetch, _ticketsPrintLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsPrintLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsPrintLayout(forceFetch, _ticketsPrintLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsPrintLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsPrintLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsPrintLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsPrintLayout || forceFetch || _alwaysFetchTicketsPrintLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsPrintLayout);
				_ticketsPrintLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketsPrintLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketsPrintLayout.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsPrintLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsPrintLayout = true;
			}
			return _ticketsPrintLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsPrintLayout'. These settings will be taken into account
		/// when the property TicketsPrintLayout is requested or GetMultiTicketsPrintLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsPrintLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsPrintLayout.SortClauses=sortClauses;
			_ticketsPrintLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsReceiptLayout(bool forceFetch)
		{
			return GetMultiTicketsReceiptLayout(forceFetch, _ticketsReceiptLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsReceiptLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsReceiptLayout(forceFetch, _ticketsReceiptLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsReceiptLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsReceiptLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsReceiptLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsReceiptLayout || forceFetch || _alwaysFetchTicketsReceiptLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsReceiptLayout);
				_ticketsReceiptLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketsReceiptLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketsReceiptLayout.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsReceiptLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsReceiptLayout = true;
			}
			return _ticketsReceiptLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsReceiptLayout'. These settings will be taken into account
		/// when the property TicketsReceiptLayout is requested or GetMultiTicketsReceiptLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsReceiptLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsReceiptLayout.SortClauses=sortClauses;
			_ticketsReceiptLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesCancellationLayout(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesCancellationLayout(forceFetch, _ticketDeviceclassesCancellationLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesCancellationLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesCancellationLayout(forceFetch, _ticketDeviceclassesCancellationLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesCancellationLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesCancellationLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesCancellationLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesCancellationLayout || forceFetch || _alwaysFetchTicketDeviceclassesCancellationLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesCancellationLayout);
				_ticketDeviceclassesCancellationLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesCancellationLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesCancellationLayout.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, filter);
				_ticketDeviceclassesCancellationLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesCancellationLayout = true;
			}
			return _ticketDeviceclassesCancellationLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesCancellationLayout'. These settings will be taken into account
		/// when the property TicketDeviceclassesCancellationLayout is requested or GetMultiTicketDeviceclassesCancellationLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesCancellationLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesCancellationLayout.SortClauses=sortClauses;
			_ticketDeviceclassesCancellationLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesGroupLayout(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesGroupLayout(forceFetch, _ticketDeviceclassesGroupLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesGroupLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesGroupLayout(forceFetch, _ticketDeviceclassesGroupLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesGroupLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesGroupLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesGroupLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesGroupLayout || forceFetch || _alwaysFetchTicketDeviceclassesGroupLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesGroupLayout);
				_ticketDeviceclassesGroupLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesGroupLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesGroupLayout.GetMultiManyToOne(null, null, null, null, null, this, null, null, null, null, null, null, null, filter);
				_ticketDeviceclassesGroupLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesGroupLayout = true;
			}
			return _ticketDeviceclassesGroupLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesGroupLayout'. These settings will be taken into account
		/// when the property TicketDeviceclassesGroupLayout is requested or GetMultiTicketDeviceclassesGroupLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesGroupLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesGroupLayout.SortClauses=sortClauses;
			_ticketDeviceclassesGroupLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesPrintLayout(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesPrintLayout(forceFetch, _ticketDeviceclassesPrintLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesPrintLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesPrintLayout(forceFetch, _ticketDeviceclassesPrintLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesPrintLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesPrintLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesPrintLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesPrintLayout || forceFetch || _alwaysFetchTicketDeviceclassesPrintLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesPrintLayout);
				_ticketDeviceclassesPrintLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesPrintLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesPrintLayout.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, null, filter);
				_ticketDeviceclassesPrintLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesPrintLayout = true;
			}
			return _ticketDeviceclassesPrintLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesPrintLayout'. These settings will be taken into account
		/// when the property TicketDeviceclassesPrintLayout is requested or GetMultiTicketDeviceclassesPrintLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesPrintLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesPrintLayout.SortClauses=sortClauses;
			_ticketDeviceclassesPrintLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesReceiptLayout(bool forceFetch)
		{
			return GetMultiTicketDeviceclassesReceiptLayout(forceFetch, _ticketDeviceclassesReceiptLayout.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesReceiptLayout(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceclassesReceiptLayout(forceFetch, _ticketDeviceclassesReceiptLayout.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesReceiptLayout(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceclassesReceiptLayout(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceclassesReceiptLayout(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceclassesReceiptLayout || forceFetch || _alwaysFetchTicketDeviceclassesReceiptLayout) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceclassesReceiptLayout);
				_ticketDeviceclassesReceiptLayout.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceclassesReceiptLayout.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceclassesReceiptLayout.GetMultiManyToOne(null, null, null, null, null, null, null, this, null, null, null, null, null, filter);
				_ticketDeviceclassesReceiptLayout.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceclassesReceiptLayout = true;
			}
			return _ticketDeviceclassesReceiptLayout;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceclassesReceiptLayout'. These settings will be taken into account
		/// when the property TicketDeviceclassesReceiptLayout is requested or GetMultiTicketDeviceclassesReceiptLayout is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceclassesReceiptLayout(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceclassesReceiptLayout.SortClauses=sortClauses;
			_ticketDeviceclassesReceiptLayout.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'OutputDeviceEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OutputDeviceEntity' which is related to this entity.</returns>
		public OutputDeviceEntity GetSingleOutputDevice()
		{
			return GetSingleOutputDevice(false);
		}

		/// <summary> Retrieves the related entity of type 'OutputDeviceEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OutputDeviceEntity' which is related to this entity.</returns>
		public virtual OutputDeviceEntity GetSingleOutputDevice(bool forceFetch)
		{
			if( ( !_alreadyFetchedOutputDevice || forceFetch || _alwaysFetchOutputDevice) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OutputDeviceEntityUsingOutDeviceID);
				OutputDeviceEntity newEntity = new OutputDeviceEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OutDeviceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OutputDeviceEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_outputDeviceReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OutputDevice = newEntity;
				_alreadyFetchedOutputDevice = fetchResult;
			}
			return _outputDevice;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public FormEntity GetSingleForm()
		{
			return GetSingleForm(false);
		}

		/// <summary> Retrieves the related entity of type 'FormEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FormEntity' which is related to this entity.</returns>
		public virtual FormEntity GetSingleForm(bool forceFetch)
		{
			if( ( !_alreadyFetchedForm || forceFetch || _alwaysFetchForm) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FormEntityUsingFormid);
				FormEntity newEntity = new FormEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.Formid.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FormEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_formReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Form = newEntity;
				_alreadyFetchedForm = fetchResult;
			}
			return _form;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("OutputDevice", _outputDevice);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("Form", _form);
			toReturn.Add("AttributeBitmapLayoutObjects", _attributeBitmapLayoutObjects);
			toReturn.Add("AttributeTextLayoutObjects", _attributeTextLayoutObjects);
			toReturn.Add("CondintionalSubpageLayoutObjects", _condintionalSubpageLayoutObjects);
			toReturn.Add("ContintionalSubPageLayoutObjectsPage", _contintionalSubPageLayoutObjectsPage);
			toReturn.Add("Etickets", _etickets);
			toReturn.Add("FixedBitmapLayoutObjects", _fixedBitmapLayoutObjects);
			toReturn.Add("FixedTextLayoutObjects", _fixedTextLayoutObjects);
			toReturn.Add("ListLayoutObjects", _listLayoutObjects);
			toReturn.Add("ListLayoutObjectsPage", _listLayoutObjectsPage);
			toReturn.Add("SpecialReceipts", _specialReceipts);
			toReturn.Add("SystemFieldBarcodeLayoutObjects", _systemFieldBarcodeLayoutObjects);
			toReturn.Add("SystemFieldTextLayoutoObjects", _systemFieldTextLayoutoObjects);
			toReturn.Add("TicketsCancellationLayout", _ticketsCancellationLayout);
			toReturn.Add("TicketsGroupLayout", _ticketsGroupLayout);
			toReturn.Add("TicketsPrintLayout", _ticketsPrintLayout);
			toReturn.Add("TicketsReceiptLayout", _ticketsReceiptLayout);
			toReturn.Add("TicketDeviceclassesCancellationLayout", _ticketDeviceclassesCancellationLayout);
			toReturn.Add("TicketDeviceclassesGroupLayout", _ticketDeviceclassesGroupLayout);
			toReturn.Add("TicketDeviceclassesPrintLayout", _ticketDeviceclassesPrintLayout);
			toReturn.Add("TicketDeviceclassesReceiptLayout", _ticketDeviceclassesReceiptLayout);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="validator">The validator object for this LayoutEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 layoutID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(layoutID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_attributeBitmapLayoutObjects = new VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection();
			_attributeBitmapLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_attributeTextLayoutObjects = new VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection();
			_attributeTextLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_condintionalSubpageLayoutObjects = new VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection();
			_condintionalSubpageLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_contintionalSubPageLayoutObjectsPage = new VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection();
			_contintionalSubPageLayoutObjectsPage.SetContainingEntityInfo(this, "Page");

			_etickets = new VarioSL.Entities.CollectionClasses.EticketCollection();
			_etickets.SetContainingEntityInfo(this, "Layout");

			_fixedBitmapLayoutObjects = new VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection();
			_fixedBitmapLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_fixedTextLayoutObjects = new VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection();
			_fixedTextLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_listLayoutObjects = new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection();
			_listLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_listLayoutObjectsPage = new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection();
			_listLayoutObjectsPage.SetContainingEntityInfo(this, "Page");

			_specialReceipts = new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection();
			_specialReceipts.SetContainingEntityInfo(this, "Layout");

			_systemFieldBarcodeLayoutObjects = new VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection();
			_systemFieldBarcodeLayoutObjects.SetContainingEntityInfo(this, "Layout");

			_systemFieldTextLayoutoObjects = new VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection();
			_systemFieldTextLayoutoObjects.SetContainingEntityInfo(this, "Layout");

			_ticketsCancellationLayout = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsCancellationLayout.SetContainingEntityInfo(this, "CancellationLayout");

			_ticketsGroupLayout = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsGroupLayout.SetContainingEntityInfo(this, "GroupLayout");

			_ticketsPrintLayout = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsPrintLayout.SetContainingEntityInfo(this, "PrintLayout");

			_ticketsReceiptLayout = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsReceiptLayout.SetContainingEntityInfo(this, "ReceiptLayout");

			_ticketDeviceclassesCancellationLayout = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesCancellationLayout.SetContainingEntityInfo(this, "CancellationLayout");

			_ticketDeviceclassesGroupLayout = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesGroupLayout.SetContainingEntityInfo(this, "GroupLayout");

			_ticketDeviceclassesPrintLayout = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesPrintLayout.SetContainingEntityInfo(this, "PrintLayout");

			_ticketDeviceclassesReceiptLayout = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceclassesReceiptLayout.SetContainingEntityInfo(this, "ReceiptLayout");
			_clientReturnsNewIfNotFound = false;
			_outputDeviceReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			_formReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevMask", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Formid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutKind", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LayoutNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Length", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OutDeviceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Rotation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticLayoutRelations.ClientEntityUsingOwnerClientIDStatic, true, signalRelatedEntity, "Layouts", resetFKFields, new int[] { (int)LayoutFieldIndex.OwnerClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticLayoutRelations.ClientEntityUsingOwnerClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _outputDevice</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOutputDevice(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _outputDevice, new PropertyChangedEventHandler( OnOutputDevicePropertyChanged ), "OutputDevice", VarioSL.Entities.RelationClasses.StaticLayoutRelations.OutputDeviceEntityUsingOutDeviceIDStatic, true, signalRelatedEntity, "Layouts", resetFKFields, new int[] { (int)LayoutFieldIndex.OutDeviceID } );		
			_outputDevice = null;
		}
		
		/// <summary> setups the sync logic for member _outputDevice</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOutputDevice(IEntityCore relatedEntity)
		{
			if(_outputDevice!=relatedEntity)
			{		
				DesetupSyncOutputDevice(true, true);
				_outputDevice = (OutputDeviceEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _outputDevice, new PropertyChangedEventHandler( OnOutputDevicePropertyChanged ), "OutputDevice", VarioSL.Entities.RelationClasses.StaticLayoutRelations.OutputDeviceEntityUsingOutDeviceIDStatic, true, ref _alreadyFetchedOutputDevice, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOutputDevicePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLayoutRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "Layouts", resetFKFields, new int[] { (int)LayoutFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticLayoutRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _form</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncForm(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticLayoutRelations.FormEntityUsingFormidStatic, true, signalRelatedEntity, "Layouts", resetFKFields, new int[] { (int)LayoutFieldIndex.Formid } );		
			_form = null;
		}
		
		/// <summary> setups the sync logic for member _form</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncForm(IEntityCore relatedEntity)
		{
			if(_form!=relatedEntity)
			{		
				DesetupSyncForm(true, true);
				_form = (FormEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _form, new PropertyChangedEventHandler( OnFormPropertyChanged ), "Form", VarioSL.Entities.RelationClasses.StaticLayoutRelations.FormEntityUsingFormidStatic, true, ref _alreadyFetchedForm, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFormPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="layoutID">PK value for Layout which data should be fetched into this Layout object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 layoutID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)LayoutFieldIndex.LayoutID].ForcedCurrentValueWrite(layoutID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateLayoutDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new LayoutEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LayoutRelations Relations
		{
			get	{ return new LayoutRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeBitmapLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeBitmapLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("AttributeBitmapLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity, 0, null, null, null, "AttributeBitmapLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeTextLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeTextLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("AttributeTextLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity, 0, null, null, null, "AttributeTextLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConditionalSubPageLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCondintionalSubpageLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("CondintionalSubpageLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity, 0, null, null, null, "CondintionalSubpageLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConditionalSubPageLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContintionalSubPageLayoutObjectsPage
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ContintionalSubPageLayoutObjectsPage")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity, 0, null, null, null, "ContintionalSubPageLayoutObjectsPage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Eticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEtickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.EticketCollection(), (IEntityRelation)GetRelationsForField("Etickets")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.EticketEntity, 0, null, null, null, "Etickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FixedBitmapLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFixedBitmapLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("FixedBitmapLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity, 0, null, null, null, "FixedBitmapLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FixedTextLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFixedTextLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("FixedTextLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity, 0, null, null, null, "FixedTextLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ListLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathListLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ListLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, 0, null, null, null, "ListLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ListLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathListLayoutObjectsPage
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("ListLayoutObjectsPage")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.ListLayoutObjectEntity, 0, null, null, null, "ListLayoutObjectsPage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SpecialReceipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSpecialReceipts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection(), (IEntityRelation)GetRelationsForField("SpecialReceipts")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.SpecialReceiptEntity, 0, null, null, null, "SpecialReceipts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SystemFieldBarcodeLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemFieldBarcodeLayoutObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("SystemFieldBarcodeLayoutObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity, 0, null, null, null, "SystemFieldBarcodeLayoutObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SystemFieldTextLayoutObject' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSystemFieldTextLayoutoObjects
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection(), (IEntityRelation)GetRelationsForField("SystemFieldTextLayoutoObjects")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity, 0, null, null, null, "SystemFieldTextLayoutoObjects", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsCancellationLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsCancellationLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsCancellationLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsGroupLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsGroupLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsGroupLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsPrintLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsPrintLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsPrintLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsReceiptLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsReceiptLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsReceiptLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesCancellationLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesCancellationLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesCancellationLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesGroupLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesGroupLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesGroupLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesPrintLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesPrintLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesPrintLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceclassesReceiptLayout
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceclassesReceiptLayout")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceclassesReceiptLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OutputDevice'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOutputDevice
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OutputDeviceCollection(), (IEntityRelation)GetRelationsForField("OutputDevice")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.OutputDeviceEntity, 0, null, null, null, "OutputDevice", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Form'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathForm
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FormCollection(), (IEntityRelation)GetRelationsForField("Form")[0], (int)VarioSL.Entities.EntityType.LayoutEntity, (int)VarioSL.Entities.EntityType.FormEntity, 0, null, null, null, "Form", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)LayoutFieldIndex.Description, true); }
			set	{ SetValue((int)LayoutFieldIndex.Description, value, true); }
		}

		/// <summary> The DevMask property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."DEVMASK"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DevMask
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.DevMask, false); }
			set	{ SetValue((int)LayoutFieldIndex.DevMask, value, true); }
		}

		/// <summary> The Formid property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."FORMID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Formid
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.Formid, false); }
			set	{ SetValue((int)LayoutFieldIndex.Formid, value, true); }
		}

		/// <summary> The LayoutID property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."LAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 LayoutID
		{
			get { return (System.Int64)GetValue((int)LayoutFieldIndex.LayoutID, true); }
			set	{ SetValue((int)LayoutFieldIndex.LayoutID, value, true); }
		}

		/// <summary> The LayoutKind property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."LAYOUTKIND"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 3, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> LayoutKind
		{
			get { return (Nullable<System.Int16>)GetValue((int)LayoutFieldIndex.LayoutKind, false); }
			set	{ SetValue((int)LayoutFieldIndex.LayoutKind, value, true); }
		}

		/// <summary> The LayoutNumber property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."LAYOUTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LayoutNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.LayoutNumber, false); }
			set	{ SetValue((int)LayoutFieldIndex.LayoutNumber, value, true); }
		}

		/// <summary> The Length property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."LENGTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 4, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 Length
		{
			get { return (System.Int16)GetValue((int)LayoutFieldIndex.Length, true); }
			set	{ SetValue((int)LayoutFieldIndex.Length, value, true); }
		}

		/// <summary> The Name property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)LayoutFieldIndex.Name, true); }
			set	{ SetValue((int)LayoutFieldIndex.Name, value, true); }
		}

		/// <summary> The OutDeviceID property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."OUTDEVICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OutDeviceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.OutDeviceID, false); }
			set	{ SetValue((int)LayoutFieldIndex.OutDeviceID, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerClientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.OwnerClientID, false); }
			set	{ SetValue((int)LayoutFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The Rotation property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."ROTATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> Rotation
		{
			get { return (Nullable<System.Int16>)GetValue((int)LayoutFieldIndex.Rotation, false); }
			set	{ SetValue((int)LayoutFieldIndex.Rotation, value, true); }
		}

		/// <summary> The TariffID property of the Entity Layout<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_LAYOUT"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)LayoutFieldIndex.TariffID, false); }
			set	{ SetValue((int)LayoutFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AttributeBitmapLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeBitmapLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeBitmapLayoutObjectCollection AttributeBitmapLayoutObjects
		{
			get	{ return GetMultiAttributeBitmapLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeBitmapLayoutObjects. When set to true, AttributeBitmapLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeBitmapLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeBitmapLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeBitmapLayoutObjects
		{
			get	{ return _alwaysFetchAttributeBitmapLayoutObjects; }
			set	{ _alwaysFetchAttributeBitmapLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeBitmapLayoutObjects already has been fetched. Setting this property to false when AttributeBitmapLayoutObjects has been fetched
		/// will clear the AttributeBitmapLayoutObjects collection well. Setting this property to true while AttributeBitmapLayoutObjects hasn't been fetched disables lazy loading for AttributeBitmapLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeBitmapLayoutObjects
		{
			get { return _alreadyFetchedAttributeBitmapLayoutObjects;}
			set 
			{
				if(_alreadyFetchedAttributeBitmapLayoutObjects && !value && (_attributeBitmapLayoutObjects != null))
				{
					_attributeBitmapLayoutObjects.Clear();
				}
				_alreadyFetchedAttributeBitmapLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AttributeTextLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAttributeTextLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AttributeTextLayoutObjectCollection AttributeTextLayoutObjects
		{
			get	{ return GetMultiAttributeTextLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeTextLayoutObjects. When set to true, AttributeTextLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeTextLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiAttributeTextLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeTextLayoutObjects
		{
			get	{ return _alwaysFetchAttributeTextLayoutObjects; }
			set	{ _alwaysFetchAttributeTextLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeTextLayoutObjects already has been fetched. Setting this property to false when AttributeTextLayoutObjects has been fetched
		/// will clear the AttributeTextLayoutObjects collection well. Setting this property to true while AttributeTextLayoutObjects hasn't been fetched disables lazy loading for AttributeTextLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeTextLayoutObjects
		{
			get { return _alreadyFetchedAttributeTextLayoutObjects;}
			set 
			{
				if(_alreadyFetchedAttributeTextLayoutObjects && !value && (_attributeTextLayoutObjects != null))
				{
					_attributeTextLayoutObjects.Clear();
				}
				_alreadyFetchedAttributeTextLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCondintionalSubpageLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection CondintionalSubpageLayoutObjects
		{
			get	{ return GetMultiCondintionalSubpageLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CondintionalSubpageLayoutObjects. When set to true, CondintionalSubpageLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CondintionalSubpageLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiCondintionalSubpageLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCondintionalSubpageLayoutObjects
		{
			get	{ return _alwaysFetchCondintionalSubpageLayoutObjects; }
			set	{ _alwaysFetchCondintionalSubpageLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CondintionalSubpageLayoutObjects already has been fetched. Setting this property to false when CondintionalSubpageLayoutObjects has been fetched
		/// will clear the CondintionalSubpageLayoutObjects collection well. Setting this property to true while CondintionalSubpageLayoutObjects hasn't been fetched disables lazy loading for CondintionalSubpageLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCondintionalSubpageLayoutObjects
		{
			get { return _alreadyFetchedCondintionalSubpageLayoutObjects;}
			set 
			{
				if(_alreadyFetchedCondintionalSubpageLayoutObjects && !value && (_condintionalSubpageLayoutObjects != null))
				{
					_condintionalSubpageLayoutObjects.Clear();
				}
				_alreadyFetchedCondintionalSubpageLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ConditionalSubPageLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiContintionalSubPageLayoutObjectsPage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ConditionalSubPageLayoutObjectCollection ContintionalSubPageLayoutObjectsPage
		{
			get	{ return GetMultiContintionalSubPageLayoutObjectsPage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ContintionalSubPageLayoutObjectsPage. When set to true, ContintionalSubPageLayoutObjectsPage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ContintionalSubPageLayoutObjectsPage is accessed. You can always execute/ a forced fetch by calling GetMultiContintionalSubPageLayoutObjectsPage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContintionalSubPageLayoutObjectsPage
		{
			get	{ return _alwaysFetchContintionalSubPageLayoutObjectsPage; }
			set	{ _alwaysFetchContintionalSubPageLayoutObjectsPage = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ContintionalSubPageLayoutObjectsPage already has been fetched. Setting this property to false when ContintionalSubPageLayoutObjectsPage has been fetched
		/// will clear the ContintionalSubPageLayoutObjectsPage collection well. Setting this property to true while ContintionalSubPageLayoutObjectsPage hasn't been fetched disables lazy loading for ContintionalSubPageLayoutObjectsPage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContintionalSubPageLayoutObjectsPage
		{
			get { return _alreadyFetchedContintionalSubPageLayoutObjectsPage;}
			set 
			{
				if(_alreadyFetchedContintionalSubPageLayoutObjectsPage && !value && (_contintionalSubPageLayoutObjectsPage != null))
				{
					_contintionalSubPageLayoutObjectsPage.Clear();
				}
				_alreadyFetchedContintionalSubPageLayoutObjectsPage = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'EticketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEtickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.EticketCollection Etickets
		{
			get	{ return GetMultiEtickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Etickets. When set to true, Etickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Etickets is accessed. You can always execute/ a forced fetch by calling GetMultiEtickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEtickets
		{
			get	{ return _alwaysFetchEtickets; }
			set	{ _alwaysFetchEtickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Etickets already has been fetched. Setting this property to false when Etickets has been fetched
		/// will clear the Etickets collection well. Setting this property to true while Etickets hasn't been fetched disables lazy loading for Etickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEtickets
		{
			get { return _alreadyFetchedEtickets;}
			set 
			{
				if(_alreadyFetchedEtickets && !value && (_etickets != null))
				{
					_etickets.Clear();
				}
				_alreadyFetchedEtickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FixedBitmapLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFixedBitmapLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FixedBitmapLayoutObjectCollection FixedBitmapLayoutObjects
		{
			get	{ return GetMultiFixedBitmapLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FixedBitmapLayoutObjects. When set to true, FixedBitmapLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FixedBitmapLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiFixedBitmapLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFixedBitmapLayoutObjects
		{
			get	{ return _alwaysFetchFixedBitmapLayoutObjects; }
			set	{ _alwaysFetchFixedBitmapLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FixedBitmapLayoutObjects already has been fetched. Setting this property to false when FixedBitmapLayoutObjects has been fetched
		/// will clear the FixedBitmapLayoutObjects collection well. Setting this property to true while FixedBitmapLayoutObjects hasn't been fetched disables lazy loading for FixedBitmapLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFixedBitmapLayoutObjects
		{
			get { return _alreadyFetchedFixedBitmapLayoutObjects;}
			set 
			{
				if(_alreadyFetchedFixedBitmapLayoutObjects && !value && (_fixedBitmapLayoutObjects != null))
				{
					_fixedBitmapLayoutObjects.Clear();
				}
				_alreadyFetchedFixedBitmapLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FixedTextLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFixedTextLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FixedTextLayoutObjectCollection FixedTextLayoutObjects
		{
			get	{ return GetMultiFixedTextLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FixedTextLayoutObjects. When set to true, FixedTextLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FixedTextLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiFixedTextLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFixedTextLayoutObjects
		{
			get	{ return _alwaysFetchFixedTextLayoutObjects; }
			set	{ _alwaysFetchFixedTextLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FixedTextLayoutObjects already has been fetched. Setting this property to false when FixedTextLayoutObjects has been fetched
		/// will clear the FixedTextLayoutObjects collection well. Setting this property to true while FixedTextLayoutObjects hasn't been fetched disables lazy loading for FixedTextLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFixedTextLayoutObjects
		{
			get { return _alreadyFetchedFixedTextLayoutObjects;}
			set 
			{
				if(_alreadyFetchedFixedTextLayoutObjects && !value && (_fixedTextLayoutObjects != null))
				{
					_fixedTextLayoutObjects.Clear();
				}
				_alreadyFetchedFixedTextLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiListLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection ListLayoutObjects
		{
			get	{ return GetMultiListLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ListLayoutObjects. When set to true, ListLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ListLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiListLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchListLayoutObjects
		{
			get	{ return _alwaysFetchListLayoutObjects; }
			set	{ _alwaysFetchListLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ListLayoutObjects already has been fetched. Setting this property to false when ListLayoutObjects has been fetched
		/// will clear the ListLayoutObjects collection well. Setting this property to true while ListLayoutObjects hasn't been fetched disables lazy loading for ListLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedListLayoutObjects
		{
			get { return _alreadyFetchedListLayoutObjects;}
			set 
			{
				if(_alreadyFetchedListLayoutObjects && !value && (_listLayoutObjects != null))
				{
					_listLayoutObjects.Clear();
				}
				_alreadyFetchedListLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ListLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiListLayoutObjectsPage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ListLayoutObjectCollection ListLayoutObjectsPage
		{
			get	{ return GetMultiListLayoutObjectsPage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ListLayoutObjectsPage. When set to true, ListLayoutObjectsPage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ListLayoutObjectsPage is accessed. You can always execute/ a forced fetch by calling GetMultiListLayoutObjectsPage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchListLayoutObjectsPage
		{
			get	{ return _alwaysFetchListLayoutObjectsPage; }
			set	{ _alwaysFetchListLayoutObjectsPage = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ListLayoutObjectsPage already has been fetched. Setting this property to false when ListLayoutObjectsPage has been fetched
		/// will clear the ListLayoutObjectsPage collection well. Setting this property to true while ListLayoutObjectsPage hasn't been fetched disables lazy loading for ListLayoutObjectsPage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedListLayoutObjectsPage
		{
			get { return _alreadyFetchedListLayoutObjectsPage;}
			set 
			{
				if(_alreadyFetchedListLayoutObjectsPage && !value && (_listLayoutObjectsPage != null))
				{
					_listLayoutObjectsPage.Clear();
				}
				_alreadyFetchedListLayoutObjectsPage = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSpecialReceipts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection SpecialReceipts
		{
			get	{ return GetMultiSpecialReceipts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SpecialReceipts. When set to true, SpecialReceipts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SpecialReceipts is accessed. You can always execute/ a forced fetch by calling GetMultiSpecialReceipts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSpecialReceipts
		{
			get	{ return _alwaysFetchSpecialReceipts; }
			set	{ _alwaysFetchSpecialReceipts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SpecialReceipts already has been fetched. Setting this property to false when SpecialReceipts has been fetched
		/// will clear the SpecialReceipts collection well. Setting this property to true while SpecialReceipts hasn't been fetched disables lazy loading for SpecialReceipts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSpecialReceipts
		{
			get { return _alreadyFetchedSpecialReceipts;}
			set 
			{
				if(_alreadyFetchedSpecialReceipts && !value && (_specialReceipts != null))
				{
					_specialReceipts.Clear();
				}
				_alreadyFetchedSpecialReceipts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SystemFieldBarcodeLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSystemFieldBarcodeLayoutObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldBarcodeLayoutObjectCollection SystemFieldBarcodeLayoutObjects
		{
			get	{ return GetMultiSystemFieldBarcodeLayoutObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SystemFieldBarcodeLayoutObjects. When set to true, SystemFieldBarcodeLayoutObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemFieldBarcodeLayoutObjects is accessed. You can always execute/ a forced fetch by calling GetMultiSystemFieldBarcodeLayoutObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemFieldBarcodeLayoutObjects
		{
			get	{ return _alwaysFetchSystemFieldBarcodeLayoutObjects; }
			set	{ _alwaysFetchSystemFieldBarcodeLayoutObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemFieldBarcodeLayoutObjects already has been fetched. Setting this property to false when SystemFieldBarcodeLayoutObjects has been fetched
		/// will clear the SystemFieldBarcodeLayoutObjects collection well. Setting this property to true while SystemFieldBarcodeLayoutObjects hasn't been fetched disables lazy loading for SystemFieldBarcodeLayoutObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemFieldBarcodeLayoutObjects
		{
			get { return _alreadyFetchedSystemFieldBarcodeLayoutObjects;}
			set 
			{
				if(_alreadyFetchedSystemFieldBarcodeLayoutObjects && !value && (_systemFieldBarcodeLayoutObjects != null))
				{
					_systemFieldBarcodeLayoutObjects.Clear();
				}
				_alreadyFetchedSystemFieldBarcodeLayoutObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SystemFieldTextLayoutObjectEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSystemFieldTextLayoutoObjects()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SystemFieldTextLayoutObjectCollection SystemFieldTextLayoutoObjects
		{
			get	{ return GetMultiSystemFieldTextLayoutoObjects(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SystemFieldTextLayoutoObjects. When set to true, SystemFieldTextLayoutoObjects is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SystemFieldTextLayoutoObjects is accessed. You can always execute/ a forced fetch by calling GetMultiSystemFieldTextLayoutoObjects(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSystemFieldTextLayoutoObjects
		{
			get	{ return _alwaysFetchSystemFieldTextLayoutoObjects; }
			set	{ _alwaysFetchSystemFieldTextLayoutoObjects = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SystemFieldTextLayoutoObjects already has been fetched. Setting this property to false when SystemFieldTextLayoutoObjects has been fetched
		/// will clear the SystemFieldTextLayoutoObjects collection well. Setting this property to true while SystemFieldTextLayoutoObjects hasn't been fetched disables lazy loading for SystemFieldTextLayoutoObjects</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSystemFieldTextLayoutoObjects
		{
			get { return _alreadyFetchedSystemFieldTextLayoutoObjects;}
			set 
			{
				if(_alreadyFetchedSystemFieldTextLayoutoObjects && !value && (_systemFieldTextLayoutoObjects != null))
				{
					_systemFieldTextLayoutoObjects.Clear();
				}
				_alreadyFetchedSystemFieldTextLayoutoObjects = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsCancellationLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsCancellationLayout
		{
			get	{ return GetMultiTicketsCancellationLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsCancellationLayout. When set to true, TicketsCancellationLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsCancellationLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsCancellationLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsCancellationLayout
		{
			get	{ return _alwaysFetchTicketsCancellationLayout; }
			set	{ _alwaysFetchTicketsCancellationLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsCancellationLayout already has been fetched. Setting this property to false when TicketsCancellationLayout has been fetched
		/// will clear the TicketsCancellationLayout collection well. Setting this property to true while TicketsCancellationLayout hasn't been fetched disables lazy loading for TicketsCancellationLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsCancellationLayout
		{
			get { return _alreadyFetchedTicketsCancellationLayout;}
			set 
			{
				if(_alreadyFetchedTicketsCancellationLayout && !value && (_ticketsCancellationLayout != null))
				{
					_ticketsCancellationLayout.Clear();
				}
				_alreadyFetchedTicketsCancellationLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsGroupLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsGroupLayout
		{
			get	{ return GetMultiTicketsGroupLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsGroupLayout. When set to true, TicketsGroupLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsGroupLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsGroupLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsGroupLayout
		{
			get	{ return _alwaysFetchTicketsGroupLayout; }
			set	{ _alwaysFetchTicketsGroupLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsGroupLayout already has been fetched. Setting this property to false when TicketsGroupLayout has been fetched
		/// will clear the TicketsGroupLayout collection well. Setting this property to true while TicketsGroupLayout hasn't been fetched disables lazy loading for TicketsGroupLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsGroupLayout
		{
			get { return _alreadyFetchedTicketsGroupLayout;}
			set 
			{
				if(_alreadyFetchedTicketsGroupLayout && !value && (_ticketsGroupLayout != null))
				{
					_ticketsGroupLayout.Clear();
				}
				_alreadyFetchedTicketsGroupLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsPrintLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsPrintLayout
		{
			get	{ return GetMultiTicketsPrintLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsPrintLayout. When set to true, TicketsPrintLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsPrintLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsPrintLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsPrintLayout
		{
			get	{ return _alwaysFetchTicketsPrintLayout; }
			set	{ _alwaysFetchTicketsPrintLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsPrintLayout already has been fetched. Setting this property to false when TicketsPrintLayout has been fetched
		/// will clear the TicketsPrintLayout collection well. Setting this property to true while TicketsPrintLayout hasn't been fetched disables lazy loading for TicketsPrintLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsPrintLayout
		{
			get { return _alreadyFetchedTicketsPrintLayout;}
			set 
			{
				if(_alreadyFetchedTicketsPrintLayout && !value && (_ticketsPrintLayout != null))
				{
					_ticketsPrintLayout.Clear();
				}
				_alreadyFetchedTicketsPrintLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsReceiptLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsReceiptLayout
		{
			get	{ return GetMultiTicketsReceiptLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsReceiptLayout. When set to true, TicketsReceiptLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsReceiptLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsReceiptLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsReceiptLayout
		{
			get	{ return _alwaysFetchTicketsReceiptLayout; }
			set	{ _alwaysFetchTicketsReceiptLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsReceiptLayout already has been fetched. Setting this property to false when TicketsReceiptLayout has been fetched
		/// will clear the TicketsReceiptLayout collection well. Setting this property to true while TicketsReceiptLayout hasn't been fetched disables lazy loading for TicketsReceiptLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsReceiptLayout
		{
			get { return _alreadyFetchedTicketsReceiptLayout;}
			set 
			{
				if(_alreadyFetchedTicketsReceiptLayout && !value && (_ticketsReceiptLayout != null))
				{
					_ticketsReceiptLayout.Clear();
				}
				_alreadyFetchedTicketsReceiptLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesCancellationLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesCancellationLayout
		{
			get	{ return GetMultiTicketDeviceclassesCancellationLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesCancellationLayout. When set to true, TicketDeviceclassesCancellationLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesCancellationLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesCancellationLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesCancellationLayout
		{
			get	{ return _alwaysFetchTicketDeviceclassesCancellationLayout; }
			set	{ _alwaysFetchTicketDeviceclassesCancellationLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesCancellationLayout already has been fetched. Setting this property to false when TicketDeviceclassesCancellationLayout has been fetched
		/// will clear the TicketDeviceclassesCancellationLayout collection well. Setting this property to true while TicketDeviceclassesCancellationLayout hasn't been fetched disables lazy loading for TicketDeviceclassesCancellationLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesCancellationLayout
		{
			get { return _alreadyFetchedTicketDeviceclassesCancellationLayout;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesCancellationLayout && !value && (_ticketDeviceclassesCancellationLayout != null))
				{
					_ticketDeviceclassesCancellationLayout.Clear();
				}
				_alreadyFetchedTicketDeviceclassesCancellationLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesGroupLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesGroupLayout
		{
			get	{ return GetMultiTicketDeviceclassesGroupLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesGroupLayout. When set to true, TicketDeviceclassesGroupLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesGroupLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesGroupLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesGroupLayout
		{
			get	{ return _alwaysFetchTicketDeviceclassesGroupLayout; }
			set	{ _alwaysFetchTicketDeviceclassesGroupLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesGroupLayout already has been fetched. Setting this property to false when TicketDeviceclassesGroupLayout has been fetched
		/// will clear the TicketDeviceclassesGroupLayout collection well. Setting this property to true while TicketDeviceclassesGroupLayout hasn't been fetched disables lazy loading for TicketDeviceclassesGroupLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesGroupLayout
		{
			get { return _alreadyFetchedTicketDeviceclassesGroupLayout;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesGroupLayout && !value && (_ticketDeviceclassesGroupLayout != null))
				{
					_ticketDeviceclassesGroupLayout.Clear();
				}
				_alreadyFetchedTicketDeviceclassesGroupLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesPrintLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesPrintLayout
		{
			get	{ return GetMultiTicketDeviceclassesPrintLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesPrintLayout. When set to true, TicketDeviceclassesPrintLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesPrintLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesPrintLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesPrintLayout
		{
			get	{ return _alwaysFetchTicketDeviceclassesPrintLayout; }
			set	{ _alwaysFetchTicketDeviceclassesPrintLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesPrintLayout already has been fetched. Setting this property to false when TicketDeviceclassesPrintLayout has been fetched
		/// will clear the TicketDeviceclassesPrintLayout collection well. Setting this property to true while TicketDeviceclassesPrintLayout hasn't been fetched disables lazy loading for TicketDeviceclassesPrintLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesPrintLayout
		{
			get { return _alreadyFetchedTicketDeviceclassesPrintLayout;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesPrintLayout && !value && (_ticketDeviceclassesPrintLayout != null))
				{
					_ticketDeviceclassesPrintLayout.Clear();
				}
				_alreadyFetchedTicketDeviceclassesPrintLayout = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceclassesReceiptLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceclassesReceiptLayout
		{
			get	{ return GetMultiTicketDeviceclassesReceiptLayout(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceclassesReceiptLayout. When set to true, TicketDeviceclassesReceiptLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceclassesReceiptLayout is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceclassesReceiptLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceclassesReceiptLayout
		{
			get	{ return _alwaysFetchTicketDeviceclassesReceiptLayout; }
			set	{ _alwaysFetchTicketDeviceclassesReceiptLayout = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceclassesReceiptLayout already has been fetched. Setting this property to false when TicketDeviceclassesReceiptLayout has been fetched
		/// will clear the TicketDeviceclassesReceiptLayout collection well. Setting this property to true while TicketDeviceclassesReceiptLayout hasn't been fetched disables lazy loading for TicketDeviceclassesReceiptLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceclassesReceiptLayout
		{
			get { return _alreadyFetchedTicketDeviceclassesReceiptLayout;}
			set 
			{
				if(_alreadyFetchedTicketDeviceclassesReceiptLayout && !value && (_ticketDeviceclassesReceiptLayout != null))
				{
					_ticketDeviceclassesReceiptLayout.Clear();
				}
				_alreadyFetchedTicketDeviceclassesReceiptLayout = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Layouts", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OutputDeviceEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOutputDevice()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OutputDeviceEntity OutputDevice
		{
			get	{ return GetSingleOutputDevice(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOutputDevice(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Layouts", "OutputDevice", _outputDevice, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OutputDevice. When set to true, OutputDevice is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OutputDevice is accessed. You can always execute a forced fetch by calling GetSingleOutputDevice(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOutputDevice
		{
			get	{ return _alwaysFetchOutputDevice; }
			set	{ _alwaysFetchOutputDevice = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OutputDevice already has been fetched. Setting this property to false when OutputDevice has been fetched
		/// will set OutputDevice to null as well. Setting this property to true while OutputDevice hasn't been fetched disables lazy loading for OutputDevice</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOutputDevice
		{
			get { return _alreadyFetchedOutputDevice;}
			set 
			{
				if(_alreadyFetchedOutputDevice && !value)
				{
					this.OutputDevice = null;
				}
				_alreadyFetchedOutputDevice = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OutputDevice is not found
		/// in the database. When set to true, OutputDevice will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OutputDeviceReturnsNewIfNotFound
		{
			get	{ return _outputDeviceReturnsNewIfNotFound; }
			set { _outputDeviceReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Layouts", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FormEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleForm()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FormEntity Form
		{
			get	{ return GetSingleForm(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncForm(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Layouts", "Form", _form, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Form. When set to true, Form is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Form is accessed. You can always execute a forced fetch by calling GetSingleForm(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchForm
		{
			get	{ return _alwaysFetchForm; }
			set	{ _alwaysFetchForm = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Form already has been fetched. Setting this property to false when Form has been fetched
		/// will set Form to null as well. Setting this property to true while Form hasn't been fetched disables lazy loading for Form</summary>
		[Browsable(false)]
		public bool AlreadyFetchedForm
		{
			get { return _alreadyFetchedForm;}
			set 
			{
				if(_alreadyFetchedForm && !value)
				{
					this.Form = null;
				}
				_alreadyFetchedForm = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Form is not found
		/// in the database. When set to true, Form will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FormReturnsNewIfNotFound
		{
			get	{ return _formReturnsNewIfNotFound; }
			set { _formReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.LayoutEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
