﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DunningLevelPostingKey'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DunningLevelPostingKeyEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DunningLevelConfigurationEntity _dunningLevel;
		private bool	_alwaysFetchDunningLevel, _alreadyFetchedDunningLevel, _dunningLevelReturnsNewIfNotFound;
		private PostingKeyEntity _postingKey;
		private bool	_alwaysFetchPostingKey, _alreadyFetchedPostingKey, _postingKeyReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DunningLevel</summary>
			public static readonly string DunningLevel = "DunningLevel";
			/// <summary>Member name PostingKey</summary>
			public static readonly string PostingKey = "PostingKey";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DunningLevelPostingKeyEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DunningLevelPostingKeyEntity() :base("DunningLevelPostingKeyEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		public DunningLevelPostingKeyEntity(System.Int64 dunningLevelPostingKeyID):base("DunningLevelPostingKeyEntity")
		{
			InitClassFetch(dunningLevelPostingKeyID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DunningLevelPostingKeyEntity(System.Int64 dunningLevelPostingKeyID, IPrefetchPath prefetchPathToUse):base("DunningLevelPostingKeyEntity")
		{
			InitClassFetch(dunningLevelPostingKeyID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="validator">The custom validator object for this DunningLevelPostingKeyEntity</param>
		public DunningLevelPostingKeyEntity(System.Int64 dunningLevelPostingKeyID, IValidator validator):base("DunningLevelPostingKeyEntity")
		{
			InitClassFetch(dunningLevelPostingKeyID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DunningLevelPostingKeyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_dunningLevel = (DunningLevelConfigurationEntity)info.GetValue("_dunningLevel", typeof(DunningLevelConfigurationEntity));
			if(_dunningLevel!=null)
			{
				_dunningLevel.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_dunningLevelReturnsNewIfNotFound = info.GetBoolean("_dunningLevelReturnsNewIfNotFound");
			_alwaysFetchDunningLevel = info.GetBoolean("_alwaysFetchDunningLevel");
			_alreadyFetchedDunningLevel = info.GetBoolean("_alreadyFetchedDunningLevel");

			_postingKey = (PostingKeyEntity)info.GetValue("_postingKey", typeof(PostingKeyEntity));
			if(_postingKey!=null)
			{
				_postingKey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_postingKeyReturnsNewIfNotFound = info.GetBoolean("_postingKeyReturnsNewIfNotFound");
			_alwaysFetchPostingKey = info.GetBoolean("_alwaysFetchPostingKey");
			_alreadyFetchedPostingKey = info.GetBoolean("_alreadyFetchedPostingKey");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DunningLevelPostingKeyFieldIndex)fieldIndex)
			{
				case DunningLevelPostingKeyFieldIndex.DunningLevelID:
					DesetupSyncDunningLevel(true, false);
					_alreadyFetchedDunningLevel = false;
					break;
				case DunningLevelPostingKeyFieldIndex.PostingKeyID:
					DesetupSyncPostingKey(true, false);
					_alreadyFetchedPostingKey = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDunningLevel = (_dunningLevel != null);
			_alreadyFetchedPostingKey = (_postingKey != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DunningLevel":
					toReturn.Add(Relations.DunningLevelConfigurationEntityUsingDunningLevelID);
					break;
				case "PostingKey":
					toReturn.Add(Relations.PostingKeyEntityUsingPostingKeyID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_dunningLevel", (!this.MarkedForDeletion?_dunningLevel:null));
			info.AddValue("_dunningLevelReturnsNewIfNotFound", _dunningLevelReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDunningLevel", _alwaysFetchDunningLevel);
			info.AddValue("_alreadyFetchedDunningLevel", _alreadyFetchedDunningLevel);
			info.AddValue("_postingKey", (!this.MarkedForDeletion?_postingKey:null));
			info.AddValue("_postingKeyReturnsNewIfNotFound", _postingKeyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPostingKey", _alwaysFetchPostingKey);
			info.AddValue("_alreadyFetchedPostingKey", _alreadyFetchedPostingKey);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DunningLevel":
					_alreadyFetchedDunningLevel = true;
					this.DunningLevel = (DunningLevelConfigurationEntity)entity;
					break;
				case "PostingKey":
					_alreadyFetchedPostingKey = true;
					this.PostingKey = (PostingKeyEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DunningLevel":
					SetupSyncDunningLevel(relatedEntity);
					break;
				case "PostingKey":
					SetupSyncPostingKey(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DunningLevel":
					DesetupSyncDunningLevel(false, true);
					break;
				case "PostingKey":
					DesetupSyncPostingKey(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_dunningLevel!=null)
			{
				toReturn.Add(_dunningLevel);
			}
			if(_postingKey!=null)
			{
				toReturn.Add(_postingKey);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelPostingKeyID)
		{
			return FetchUsingPK(dunningLevelPostingKeyID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelPostingKeyID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(dunningLevelPostingKeyID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelPostingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(dunningLevelPostingKeyID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 dunningLevelPostingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(dunningLevelPostingKeyID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DunningLevelPostingKeyID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DunningLevelPostingKeyRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public DunningLevelConfigurationEntity GetSingleDunningLevel()
		{
			return GetSingleDunningLevel(false);
		}

		/// <summary> Retrieves the related entity of type 'DunningLevelConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DunningLevelConfigurationEntity' which is related to this entity.</returns>
		public virtual DunningLevelConfigurationEntity GetSingleDunningLevel(bool forceFetch)
		{
			if( ( !_alreadyFetchedDunningLevel || forceFetch || _alwaysFetchDunningLevel) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DunningLevelConfigurationEntityUsingDunningLevelID);
				DunningLevelConfigurationEntity newEntity = new DunningLevelConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DunningLevelID);
				}
				if(fetchResult)
				{
					newEntity = (DunningLevelConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_dunningLevelReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DunningLevel = newEntity;
				_alreadyFetchedDunningLevel = fetchResult;
			}
			return _dunningLevel;
		}


		/// <summary> Retrieves the related entity of type 'PostingKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PostingKeyEntity' which is related to this entity.</returns>
		public PostingKeyEntity GetSinglePostingKey()
		{
			return GetSinglePostingKey(false);
		}

		/// <summary> Retrieves the related entity of type 'PostingKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PostingKeyEntity' which is related to this entity.</returns>
		public virtual PostingKeyEntity GetSinglePostingKey(bool forceFetch)
		{
			if( ( !_alreadyFetchedPostingKey || forceFetch || _alwaysFetchPostingKey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PostingKeyEntityUsingPostingKeyID);
				PostingKeyEntity newEntity = new PostingKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PostingKeyID);
				}
				if(fetchResult)
				{
					newEntity = (PostingKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_postingKeyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PostingKey = newEntity;
				_alreadyFetchedPostingKey = fetchResult;
			}
			return _postingKey;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DunningLevel", _dunningLevel);
			toReturn.Add("PostingKey", _postingKey);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="validator">The validator object for this DunningLevelPostingKeyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 dunningLevelPostingKeyID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(dunningLevelPostingKeyID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_dunningLevelReturnsNewIfNotFound = false;
			_postingKeyReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DunningLevelPostingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InitalDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingKeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _dunningLevel</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDunningLevel(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _dunningLevel, new PropertyChangedEventHandler( OnDunningLevelPropertyChanged ), "DunningLevel", VarioSL.Entities.RelationClasses.StaticDunningLevelPostingKeyRelations.DunningLevelConfigurationEntityUsingDunningLevelIDStatic, true, signalRelatedEntity, "DunningLevelPostingKeys", resetFKFields, new int[] { (int)DunningLevelPostingKeyFieldIndex.DunningLevelID } );		
			_dunningLevel = null;
		}
		
		/// <summary> setups the sync logic for member _dunningLevel</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDunningLevel(IEntityCore relatedEntity)
		{
			if(_dunningLevel!=relatedEntity)
			{		
				DesetupSyncDunningLevel(true, true);
				_dunningLevel = (DunningLevelConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _dunningLevel, new PropertyChangedEventHandler( OnDunningLevelPropertyChanged ), "DunningLevel", VarioSL.Entities.RelationClasses.StaticDunningLevelPostingKeyRelations.DunningLevelConfigurationEntityUsingDunningLevelIDStatic, true, ref _alreadyFetchedDunningLevel, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDunningLevelPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _postingKey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPostingKey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _postingKey, new PropertyChangedEventHandler( OnPostingKeyPropertyChanged ), "PostingKey", VarioSL.Entities.RelationClasses.StaticDunningLevelPostingKeyRelations.PostingKeyEntityUsingPostingKeyIDStatic, true, signalRelatedEntity, "DunningLevelPostingKeys", resetFKFields, new int[] { (int)DunningLevelPostingKeyFieldIndex.PostingKeyID } );		
			_postingKey = null;
		}
		
		/// <summary> setups the sync logic for member _postingKey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPostingKey(IEntityCore relatedEntity)
		{
			if(_postingKey!=relatedEntity)
			{		
				DesetupSyncPostingKey(true, true);
				_postingKey = (PostingKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _postingKey, new PropertyChangedEventHandler( OnPostingKeyPropertyChanged ), "PostingKey", VarioSL.Entities.RelationClasses.StaticDunningLevelPostingKeyRelations.PostingKeyEntityUsingPostingKeyIDStatic, true, ref _alreadyFetchedPostingKey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPostingKeyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="dunningLevelPostingKeyID">PK value for DunningLevelPostingKey which data should be fetched into this DunningLevelPostingKey object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 dunningLevelPostingKeyID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DunningLevelPostingKeyFieldIndex.DunningLevelPostingKeyID].ForcedCurrentValueWrite(dunningLevelPostingKeyID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDunningLevelPostingKeyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DunningLevelPostingKeyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DunningLevelPostingKeyRelations Relations
		{
			get	{ return new DunningLevelPostingKeyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DunningLevelConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDunningLevel
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DunningLevelConfigurationCollection(), (IEntityRelation)GetRelationsForField("DunningLevel")[0], (int)VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity, (int)VarioSL.Entities.EntityType.DunningLevelConfigurationEntity, 0, null, null, null, "DunningLevel", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PostingKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPostingKey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PostingKeyCollection(), (IEntityRelation)GetRelationsForField("PostingKey")[0], (int)VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity, (int)VarioSL.Entities.EntityType.PostingKeyEntity, 0, null, null, null, "PostingKey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."AMOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)DunningLevelPostingKeyFieldIndex.Amount, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.Amount, value, true); }
		}

		/// <summary> The DunningLevelID property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."DUNNINGLEVELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DunningLevelID
		{
			get { return (System.Int64)GetValue((int)DunningLevelPostingKeyFieldIndex.DunningLevelID, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.DunningLevelID, value, true); }
		}

		/// <summary> The DunningLevelPostingKeyID property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."DUNNINGLEVELPOSTINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DunningLevelPostingKeyID
		{
			get { return (System.Int64)GetValue((int)DunningLevelPostingKeyFieldIndex.DunningLevelPostingKeyID, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.DunningLevelPostingKeyID, value, true); }
		}

		/// <summary> The InitalDescription property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."INITALDESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InitalDescription
		{
			get { return (System.String)GetValue((int)DunningLevelPostingKeyFieldIndex.InitalDescription, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.InitalDescription, value, true); }
		}

		/// <summary> The LastModified property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)DunningLevelPostingKeyFieldIndex.LastModified, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)DunningLevelPostingKeyFieldIndex.LastUser, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.LastUser, value, true); }
		}

		/// <summary> The PostingKeyID property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."POSTINGKEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PostingKeyID
		{
			get { return (System.Int64)GetValue((int)DunningLevelPostingKeyFieldIndex.PostingKeyID, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.PostingKeyID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity DunningLevelPostingKey<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_DUNNINGLEVELPOSTINGKEY"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)DunningLevelPostingKeyFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)DunningLevelPostingKeyFieldIndex.TransactionCounter, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'DunningLevelConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDunningLevel()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DunningLevelConfigurationEntity DunningLevel
		{
			get	{ return GetSingleDunningLevel(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDunningLevel(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningLevelPostingKeys", "DunningLevel", _dunningLevel, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DunningLevel. When set to true, DunningLevel is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DunningLevel is accessed. You can always execute a forced fetch by calling GetSingleDunningLevel(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDunningLevel
		{
			get	{ return _alwaysFetchDunningLevel; }
			set	{ _alwaysFetchDunningLevel = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DunningLevel already has been fetched. Setting this property to false when DunningLevel has been fetched
		/// will set DunningLevel to null as well. Setting this property to true while DunningLevel hasn't been fetched disables lazy loading for DunningLevel</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDunningLevel
		{
			get { return _alreadyFetchedDunningLevel;}
			set 
			{
				if(_alreadyFetchedDunningLevel && !value)
				{
					this.DunningLevel = null;
				}
				_alreadyFetchedDunningLevel = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DunningLevel is not found
		/// in the database. When set to true, DunningLevel will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DunningLevelReturnsNewIfNotFound
		{
			get	{ return _dunningLevelReturnsNewIfNotFound; }
			set { _dunningLevelReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PostingKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePostingKey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PostingKeyEntity PostingKey
		{
			get	{ return GetSinglePostingKey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPostingKey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DunningLevelPostingKeys", "PostingKey", _postingKey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PostingKey. When set to true, PostingKey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PostingKey is accessed. You can always execute a forced fetch by calling GetSinglePostingKey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPostingKey
		{
			get	{ return _alwaysFetchPostingKey; }
			set	{ _alwaysFetchPostingKey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PostingKey already has been fetched. Setting this property to false when PostingKey has been fetched
		/// will set PostingKey to null as well. Setting this property to true while PostingKey hasn't been fetched disables lazy loading for PostingKey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPostingKey
		{
			get { return _alreadyFetchedPostingKey;}
			set 
			{
				if(_alreadyFetchedPostingKey && !value)
				{
					this.PostingKey = null;
				}
				_alreadyFetchedPostingKey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PostingKey is not found
		/// in the database. When set to true, PostingKey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PostingKeyReturnsNewIfNotFound
		{
			get	{ return _postingKeyReturnsNewIfNotFound; }
			set { _postingKeyReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
