﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Device'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.SamModuleCollection	_samModules;
		private bool	_alwaysFetchSamModules, _alreadyFetchedSamModules;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private UnitEntity _unit;
		private bool	_alwaysFetchUnit, _alreadyFetchedUnit, _unitReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Unit</summary>
			public static readonly string Unit = "Unit";
			/// <summary>Member name SamModules</summary>
			public static readonly string SamModules = "SamModules";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceEntity() :base("DeviceEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		public DeviceEntity(System.Int64 deviceID):base("DeviceEntity")
		{
			InitClassFetch(deviceID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceEntity(System.Int64 deviceID, IPrefetchPath prefetchPathToUse):base("DeviceEntity")
		{
			InitClassFetch(deviceID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The custom validator object for this DeviceEntity</param>
		public DeviceEntity(System.Int64 deviceID, IValidator validator):base("DeviceEntity")
		{
			InitClassFetch(deviceID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_samModules = (VarioSL.Entities.CollectionClasses.SamModuleCollection)info.GetValue("_samModules", typeof(VarioSL.Entities.CollectionClasses.SamModuleCollection));
			_alwaysFetchSamModules = info.GetBoolean("_alwaysFetchSamModules");
			_alreadyFetchedSamModules = info.GetBoolean("_alreadyFetchedSamModules");
			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_unit = (UnitEntity)info.GetValue("_unit", typeof(UnitEntity));
			if(_unit!=null)
			{
				_unit.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_unitReturnsNewIfNotFound = info.GetBoolean("_unitReturnsNewIfNotFound");
			_alwaysFetchUnit = info.GetBoolean("_alwaysFetchUnit");
			_alreadyFetchedUnit = info.GetBoolean("_alreadyFetchedUnit");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeviceFieldIndex)fieldIndex)
			{
				case DeviceFieldIndex.DeviceClassID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case DeviceFieldIndex.UnitID:
					DesetupSyncUnit(true, false);
					_alreadyFetchedUnit = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSamModules = (_samModules.Count > 0);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedUnit = (_unit != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingDeviceClassID);
					break;
				case "Unit":
					toReturn.Add(Relations.UnitEntityUsingUnitID);
					break;
				case "SamModules":
					toReturn.Add(Relations.SamModuleEntityUsingDeviceID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_samModules", (!this.MarkedForDeletion?_samModules:null));
			info.AddValue("_alwaysFetchSamModules", _alwaysFetchSamModules);
			info.AddValue("_alreadyFetchedSamModules", _alreadyFetchedSamModules);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_unit", (!this.MarkedForDeletion?_unit:null));
			info.AddValue("_unitReturnsNewIfNotFound", _unitReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUnit", _alwaysFetchUnit);
			info.AddValue("_alreadyFetchedUnit", _alreadyFetchedUnit);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Unit":
					_alreadyFetchedUnit = true;
					this.Unit = (UnitEntity)entity;
					break;
				case "SamModules":
					_alreadyFetchedSamModules = true;
					if(entity!=null)
					{
						this.SamModules.Add((SamModuleEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Unit":
					SetupSyncUnit(relatedEntity);
					break;
				case "SamModules":
					_samModules.Add((SamModuleEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Unit":
					DesetupSyncUnit(false, true);
					break;
				case "SamModules":
					this.PerformRelatedEntityRemoval(_samModules, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_unit!=null)
			{
				toReturn.Add(_unit);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_samModules);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceID)
		{
			return FetchUsingPK(deviceID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SamModuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SamModuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SamModuleCollection GetMultiSamModules(bool forceFetch)
		{
			return GetMultiSamModules(forceFetch, _samModules.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SamModuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SamModuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SamModuleCollection GetMultiSamModules(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSamModules(forceFetch, _samModules.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SamModuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SamModuleCollection GetMultiSamModules(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSamModules(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SamModuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SamModuleCollection GetMultiSamModules(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSamModules || forceFetch || _alwaysFetchSamModules) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_samModules);
				_samModules.SuppressClearInGetMulti=!forceFetch;
				_samModules.EntityFactoryToUse = entityFactoryToUse;
				_samModules.GetMultiManyToOne(this, filter);
				_samModules.SuppressClearInGetMulti=false;
				_alreadyFetchedSamModules = true;
			}
			return _samModules;
		}

		/// <summary> Sets the collection parameters for the collection for 'SamModules'. These settings will be taken into account
		/// when the property SamModules is requested or GetMultiSamModules is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSamModules(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_samModules.SortClauses=sortClauses;
			_samModules.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingDeviceClassID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DeviceClassID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public UnitEntity GetSingleUnit()
		{
			return GetSingleUnit(false);
		}

		/// <summary> Retrieves the related entity of type 'UnitEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UnitEntity' which is related to this entity.</returns>
		public virtual UnitEntity GetSingleUnit(bool forceFetch)
		{
			if( ( !_alreadyFetchedUnit || forceFetch || _alwaysFetchUnit) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UnitEntityUsingUnitID);
				UnitEntity newEntity = new UnitEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UnitID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (UnitEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_unitReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Unit = newEntity;
				_alreadyFetchedUnit = fetchResult;
			}
			return _unit;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Unit", _unit);
			toReturn.Add("SamModules", _samModules);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The validator object for this DeviceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_samModules = new VarioSL.Entities.CollectionClasses.SamModuleCollection();
			_samModules.SetContainingEntityInfo(this, "Device");
			_deviceClassReturnsNewIfNotFound = false;
			_unitReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalDeviceNo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("InventoryNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastData", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LockState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MountingPlate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RequestedLockState", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnitID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticDeviceRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, signalRelatedEntity, "UmDevices", resetFKFields, new int[] { (int)DeviceFieldIndex.DeviceClassID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticDeviceRelations.DeviceClassEntityUsingDeviceClassIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _unit</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUnit(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticDeviceRelations.UnitEntityUsingUnitIDStatic, true, signalRelatedEntity, "Devices", resetFKFields, new int[] { (int)DeviceFieldIndex.UnitID } );		
			_unit = null;
		}
		
		/// <summary> setups the sync logic for member _unit</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUnit(IEntityCore relatedEntity)
		{
			if(_unit!=relatedEntity)
			{		
				DesetupSyncUnit(true, true);
				_unit = (UnitEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _unit, new PropertyChangedEventHandler( OnUnitPropertyChanged ), "Unit", VarioSL.Entities.RelationClasses.StaticDeviceRelations.UnitEntityUsingUnitIDStatic, true, ref _alreadyFetchedUnit, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUnitPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceID">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceFieldIndex.DeviceID].ForcedCurrentValueWrite(deviceID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceRelations Relations
		{
			get	{ return new DeviceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SamModule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSamModules
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SamModuleCollection(), (IEntityRelation)GetRelationsForField("SamModules")[0], (int)VarioSL.Entities.EntityType.DeviceEntity, (int)VarioSL.Entities.EntityType.SamModuleEntity, 0, null, null, null, "SamModules", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.DeviceEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Unit'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnit
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollection(), (IEntityRelation)GetRelationsForField("Unit")[0], (int)VarioSL.Entities.EntityType.DeviceEntity, (int)VarioSL.Entities.EntityType.UnitEntity, 0, null, null, null, "Unit", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Description, true); }
			set	{ SetValue((int)DeviceFieldIndex.Description, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceClassID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.DeviceClassID, false); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The DeviceID property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."DEVICEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 DeviceID
		{
			get { return (System.Int64)GetValue((int)DeviceFieldIndex.DeviceID, true); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceID, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."DEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DeviceNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The ExternalDeviceNo property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."EXTERNALDEVICENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalDeviceNo
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.ExternalDeviceNo, true); }
			set	{ SetValue((int)DeviceFieldIndex.ExternalDeviceNo, value, true); }
		}

		/// <summary> The InventoryNumber property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."INVENTORYNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String InventoryNumber
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.InventoryNumber, true); }
			set	{ SetValue((int)DeviceFieldIndex.InventoryNumber, value, true); }
		}

		/// <summary> The LastData property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."LASTDATA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastData
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastData, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastData, value, true); }
		}

		/// <summary> The LockState property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."LOCKSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> LockState
		{
			get { return (Nullable<System.Int16>)GetValue((int)DeviceFieldIndex.LockState, false); }
			set	{ SetValue((int)DeviceFieldIndex.LockState, value, true); }
		}

		/// <summary> The MountingPlate property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."MOUNTINGPLATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MountingPlate
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.MountingPlate, false); }
			set	{ SetValue((int)DeviceFieldIndex.MountingPlate, value, true); }
		}

		/// <summary> The Name property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Name, true); }
			set	{ SetValue((int)DeviceFieldIndex.Name, value, true); }
		}

		/// <summary> The RequestedLockState property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."REQUESTEDLOCKSTATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> RequestedLockState
		{
			get { return (Nullable<System.Int16>)GetValue((int)DeviceFieldIndex.RequestedLockState, false); }
			set	{ SetValue((int)DeviceFieldIndex.RequestedLockState, value, true); }
		}

		/// <summary> The State property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."STATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 State
		{
			get { return (System.Int64)GetValue((int)DeviceFieldIndex.State, true); }
			set	{ SetValue((int)DeviceFieldIndex.State, value, true); }
		}

		/// <summary> The UnitID property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UM_DEVICE"."UNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> UnitID
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.UnitID, false); }
			set	{ SetValue((int)DeviceFieldIndex.UnitID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SamModuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSamModules()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SamModuleCollection SamModules
		{
			get	{ return GetMultiSamModules(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SamModules. When set to true, SamModules is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SamModules is accessed. You can always execute/ a forced fetch by calling GetMultiSamModules(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSamModules
		{
			get	{ return _alwaysFetchSamModules; }
			set	{ _alwaysFetchSamModules = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SamModules already has been fetched. Setting this property to false when SamModules has been fetched
		/// will clear the SamModules collection well. Setting this property to true while SamModules hasn't been fetched disables lazy loading for SamModules</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSamModules
		{
			get { return _alreadyFetchedSamModules;}
			set 
			{
				if(_alreadyFetchedSamModules && !value && (_samModules != null))
				{
					_samModules.Clear();
				}
				_alreadyFetchedSamModules = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UmDevices", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UnitEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUnit()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual UnitEntity Unit
		{
			get	{ return GetSingleUnit(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUnit(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Devices", "Unit", _unit, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Unit. When set to true, Unit is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Unit is accessed. You can always execute a forced fetch by calling GetSingleUnit(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnit
		{
			get	{ return _alwaysFetchUnit; }
			set	{ _alwaysFetchUnit = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Unit already has been fetched. Setting this property to false when Unit has been fetched
		/// will set Unit to null as well. Setting this property to true while Unit hasn't been fetched disables lazy loading for Unit</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnit
		{
			get { return _alreadyFetchedUnit;}
			set 
			{
				if(_alreadyFetchedUnit && !value)
				{
					this.Unit = null;
				}
				_alreadyFetchedUnit = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Unit is not found
		/// in the database. When set to true, Unit will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool UnitReturnsNewIfNotFound
		{
			get	{ return _unitReturnsNewIfNotFound; }
			set { _unitReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
