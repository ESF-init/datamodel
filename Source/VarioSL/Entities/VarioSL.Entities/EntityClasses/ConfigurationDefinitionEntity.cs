﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ConfigurationDefinition'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class ConfigurationDefinitionEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection	_configurationOverwrites;
		private bool	_alwaysFetchConfigurationOverwrites, _alreadyFetchedConfigurationOverwrites;
		private VarioSL.Entities.CollectionClasses.FilterElementCollection	_filterElements;
		private bool	_alwaysFetchFilterElements, _alreadyFetchedFilterElements;
		private VarioSL.Entities.CollectionClasses.ReportToClientCollection	_reportToClients;
		private bool	_alwaysFetchReportToClients, _alreadyFetchedReportToClients;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name ConfigurationOverwrites</summary>
			public static readonly string ConfigurationOverwrites = "ConfigurationOverwrites";
			/// <summary>Member name FilterElements</summary>
			public static readonly string FilterElements = "FilterElements";
			/// <summary>Member name ReportToClients</summary>
			public static readonly string ReportToClients = "ReportToClients";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ConfigurationDefinitionEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ConfigurationDefinitionEntity() :base("ConfigurationDefinitionEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		public ConfigurationDefinitionEntity(System.Int64 configurationDefinitionID):base("ConfigurationDefinitionEntity")
		{
			InitClassFetch(configurationDefinitionID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ConfigurationDefinitionEntity(System.Int64 configurationDefinitionID, IPrefetchPath prefetchPathToUse):base("ConfigurationDefinitionEntity")
		{
			InitClassFetch(configurationDefinitionID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="validator">The custom validator object for this ConfigurationDefinitionEntity</param>
		public ConfigurationDefinitionEntity(System.Int64 configurationDefinitionID, IValidator validator):base("ConfigurationDefinitionEntity")
		{
			InitClassFetch(configurationDefinitionID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ConfigurationDefinitionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_configurationOverwrites = (VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection)info.GetValue("_configurationOverwrites", typeof(VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection));
			_alwaysFetchConfigurationOverwrites = info.GetBoolean("_alwaysFetchConfigurationOverwrites");
			_alreadyFetchedConfigurationOverwrites = info.GetBoolean("_alreadyFetchedConfigurationOverwrites");

			_filterElements = (VarioSL.Entities.CollectionClasses.FilterElementCollection)info.GetValue("_filterElements", typeof(VarioSL.Entities.CollectionClasses.FilterElementCollection));
			_alwaysFetchFilterElements = info.GetBoolean("_alwaysFetchFilterElements");
			_alreadyFetchedFilterElements = info.GetBoolean("_alreadyFetchedFilterElements");

			_reportToClients = (VarioSL.Entities.CollectionClasses.ReportToClientCollection)info.GetValue("_reportToClients", typeof(VarioSL.Entities.CollectionClasses.ReportToClientCollection));
			_alwaysFetchReportToClients = info.GetBoolean("_alwaysFetchReportToClients");
			_alreadyFetchedReportToClients = info.GetBoolean("_alreadyFetchedReportToClients");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ConfigurationDefinitionFieldIndex)fieldIndex)
			{
				case ConfigurationDefinitionFieldIndex.OwnerClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedConfigurationOverwrites = (_configurationOverwrites.Count > 0);
			_alreadyFetchedFilterElements = (_filterElements.Count > 0);
			_alreadyFetchedReportToClients = (_reportToClients.Count > 0);
			_alreadyFetchedClient = (_client != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerClientID);
					break;
				case "ConfigurationOverwrites":
					toReturn.Add(Relations.ConfigurationOverwriteEntityUsingConfigurationDefinitionID);
					break;
				case "FilterElements":
					toReturn.Add(Relations.FilterElementEntityUsingValueListDataSourceID);
					break;
				case "ReportToClients":
					toReturn.Add(Relations.ReportToClientEntityUsingConfigurationID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_configurationOverwrites", (!this.MarkedForDeletion?_configurationOverwrites:null));
			info.AddValue("_alwaysFetchConfigurationOverwrites", _alwaysFetchConfigurationOverwrites);
			info.AddValue("_alreadyFetchedConfigurationOverwrites", _alreadyFetchedConfigurationOverwrites);
			info.AddValue("_filterElements", (!this.MarkedForDeletion?_filterElements:null));
			info.AddValue("_alwaysFetchFilterElements", _alwaysFetchFilterElements);
			info.AddValue("_alreadyFetchedFilterElements", _alreadyFetchedFilterElements);
			info.AddValue("_reportToClients", (!this.MarkedForDeletion?_reportToClients:null));
			info.AddValue("_alwaysFetchReportToClients", _alwaysFetchReportToClients);
			info.AddValue("_alreadyFetchedReportToClients", _alreadyFetchedReportToClients);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "ConfigurationOverwrites":
					_alreadyFetchedConfigurationOverwrites = true;
					if(entity!=null)
					{
						this.ConfigurationOverwrites.Add((ConfigurationOverwriteEntity)entity);
					}
					break;
				case "FilterElements":
					_alreadyFetchedFilterElements = true;
					if(entity!=null)
					{
						this.FilterElements.Add((FilterElementEntity)entity);
					}
					break;
				case "ReportToClients":
					_alreadyFetchedReportToClients = true;
					if(entity!=null)
					{
						this.ReportToClients.Add((ReportToClientEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "ConfigurationOverwrites":
					_configurationOverwrites.Add((ConfigurationOverwriteEntity)relatedEntity);
					break;
				case "FilterElements":
					_filterElements.Add((FilterElementEntity)relatedEntity);
					break;
				case "ReportToClients":
					_reportToClients.Add((ReportToClientEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "ConfigurationOverwrites":
					this.PerformRelatedEntityRemoval(_configurationOverwrites, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FilterElements":
					this.PerformRelatedEntityRemoval(_filterElements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportToClients":
					this.PerformRelatedEntityRemoval(_reportToClients, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_configurationOverwrites);
			toReturn.Add(_filterElements);
			toReturn.Add(_reportToClients);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name)
		{
			return FetchUsingUCName( name, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCName( name, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCName( name, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="name">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCName(System.String name, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((ConfigurationDefinitionDAO)CreateDAOInstance()).FetchConfigurationDefinitionUsingUCName(this, this.Transaction, name, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationDefinitionID)
		{
			return FetchUsingPK(configurationDefinitionID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationDefinitionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(configurationDefinitionID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationDefinitionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(configurationDefinitionID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 configurationDefinitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(configurationDefinitionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ConfigurationDefinitionID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ConfigurationDefinitionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationOverwriteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch)
		{
			return GetMultiConfigurationOverwrites(forceFetch, _configurationOverwrites.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ConfigurationOverwriteEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiConfigurationOverwrites(forceFetch, _configurationOverwrites.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiConfigurationOverwrites(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection GetMultiConfigurationOverwrites(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedConfigurationOverwrites || forceFetch || _alwaysFetchConfigurationOverwrites) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_configurationOverwrites);
				_configurationOverwrites.SuppressClearInGetMulti=!forceFetch;
				_configurationOverwrites.EntityFactoryToUse = entityFactoryToUse;
				_configurationOverwrites.GetMultiManyToOne(null, this, filter);
				_configurationOverwrites.SuppressClearInGetMulti=false;
				_alreadyFetchedConfigurationOverwrites = true;
			}
			return _configurationOverwrites;
		}

		/// <summary> Sets the collection parameters for the collection for 'ConfigurationOverwrites'. These settings will be taken into account
		/// when the property ConfigurationOverwrites is requested or GetMultiConfigurationOverwrites is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersConfigurationOverwrites(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_configurationOverwrites.SortClauses=sortClauses;
			_configurationOverwrites.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FilterElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterElementCollection GetMultiFilterElements(bool forceFetch)
		{
			return GetMultiFilterElements(forceFetch, _filterElements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterElementCollection GetMultiFilterElements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterElements(forceFetch, _filterElements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterElementCollection GetMultiFilterElements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterElements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterElementCollection GetMultiFilterElements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterElements || forceFetch || _alwaysFetchFilterElements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterElements);
				_filterElements.SuppressClearInGetMulti=!forceFetch;
				_filterElements.EntityFactoryToUse = entityFactoryToUse;
				_filterElements.GetMultiManyToOne(null, this, null, filter);
				_filterElements.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterElements = true;
			}
			return _filterElements;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterElements'. These settings will be taken into account
		/// when the property FilterElements is requested or GetMultiFilterElements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterElements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterElements.SortClauses=sortClauses;
			_filterElements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportToClientEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportToClients(forceFetch, _reportToClients.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportToClients(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection GetMultiReportToClients(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportToClients || forceFetch || _alwaysFetchReportToClients) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportToClients);
				_reportToClients.SuppressClearInGetMulti=!forceFetch;
				_reportToClients.EntityFactoryToUse = entityFactoryToUse;
				_reportToClients.GetMultiManyToOne(null, null, this, null, filter);
				_reportToClients.SuppressClearInGetMulti=false;
				_alreadyFetchedReportToClients = true;
			}
			return _reportToClients;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportToClients'. These settings will be taken into account
		/// when the property ReportToClients is requested or GetMultiReportToClients is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportToClients(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportToClients.SortClauses=sortClauses;
			_reportToClients.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("ConfigurationOverwrites", _configurationOverwrites);
			toReturn.Add("FilterElements", _filterElements);
			toReturn.Add("ReportToClients", _reportToClients);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="validator">The validator object for this ConfigurationDefinitionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 configurationDefinitionID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(configurationDefinitionID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_configurationOverwrites = new VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection();
			_configurationOverwrites.SetContainingEntityInfo(this, "ConfigurationDefinition");

			_filterElements = new VarioSL.Entities.CollectionClasses.FilterElementCollection();
			_filterElements.SetContainingEntityInfo(this, "ConfigurationDefinition");

			_reportToClients = new VarioSL.Entities.CollectionClasses.ReportToClientCollection();
			_reportToClients.SetContainingEntityInfo(this, "ConfigurationDefinition");
			_clientReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AccessLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ConfigurationDefinitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DataType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GroupKey", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDefaultConfigurable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Scope", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticConfigurationDefinitionRelations.ClientEntityUsingOwnerClientIDStatic, true, signalRelatedEntity, "ConfigurationDefinitions", resetFKFields, new int[] { (int)ConfigurationDefinitionFieldIndex.OwnerClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticConfigurationDefinitionRelations.ClientEntityUsingOwnerClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="configurationDefinitionID">PK value for ConfigurationDefinition which data should be fetched into this ConfigurationDefinition object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 configurationDefinitionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ConfigurationDefinitionFieldIndex.ConfigurationDefinitionID].ForcedCurrentValueWrite(configurationDefinitionID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateConfigurationDefinitionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ConfigurationDefinitionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ConfigurationDefinitionRelations Relations
		{
			get	{ return new ConfigurationDefinitionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ConfigurationOverwrite' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathConfigurationOverwrites
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection(), (IEntityRelation)GetRelationsForField("ConfigurationOverwrites")[0], (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, (int)VarioSL.Entities.EntityType.ConfigurationOverwriteEntity, 0, null, null, null, "ConfigurationOverwrites", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterElements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterElementCollection(), (IEntityRelation)GetRelationsForField("FilterElements")[0], (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, (int)VarioSL.Entities.EntityType.FilterElementEntity, 0, null, null, null, "FilterElements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportToClient' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportToClients
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportToClientCollection(), (IEntityRelation)GetRelationsForField("ReportToClients")[0], (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, (int)VarioSL.Entities.EntityType.ReportToClientEntity, 0, null, null, null, "ReportToClients", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AccessLevel property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."ACCESSLEVEL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 AccessLevel
		{
			get { return (System.Int16)GetValue((int)ConfigurationDefinitionFieldIndex.AccessLevel, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.AccessLevel, value, true); }
		}

		/// <summary> The ConfigurationDefinitionID property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."CONFIGURATIONDEFINITIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 ConfigurationDefinitionID
		{
			get { return (System.Int64)GetValue((int)ConfigurationDefinitionFieldIndex.ConfigurationDefinitionID, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.ConfigurationDefinitionID, value, true); }
		}

		/// <summary> The DataType property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."DATATYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.ConfigurationDataType DataType
		{
			get { return (VarioSL.Entities.Enumerations.ConfigurationDataType)GetValue((int)ConfigurationDefinitionFieldIndex.DataType, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.DataType, value, true); }
		}

		/// <summary> The DefaultValue property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."DEFAULTVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NClob, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultValue
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.DefaultValue, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.DefaultValue, value, true); }
		}

		/// <summary> The Description property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.Description, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.Description, value, true); }
		}

		/// <summary> The GroupKey property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."GROUPKEY"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String GroupKey
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.GroupKey, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.GroupKey, value, true); }
		}

		/// <summary> The IsDefaultConfigurable property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."ISDEFAULTCONFIGURABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDefaultConfigurable
		{
			get { return (System.Boolean)GetValue((int)ConfigurationDefinitionFieldIndex.IsDefaultConfigurable, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.IsDefaultConfigurable, value, true); }
		}

		/// <summary> The LastModified property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)ConfigurationDefinitionFieldIndex.LastModified, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.LastUser, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.LastUser, value, true); }
		}

		/// <summary> The Name property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.Name, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.Name, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OwnerClientID
		{
			get { return (System.Int64)GetValue((int)ConfigurationDefinitionFieldIndex.OwnerClientID, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The Scope property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."SCOPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Scope
		{
			get { return (System.String)GetValue((int)ConfigurationDefinitionFieldIndex.Scope, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.Scope, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity ConfigurationDefinition<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_CONFIGURATIONDEFINITION"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)ConfigurationDefinitionFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)ConfigurationDefinitionFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ConfigurationOverwriteEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiConfigurationOverwrites()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ConfigurationOverwriteCollection ConfigurationOverwrites
		{
			get	{ return GetMultiConfigurationOverwrites(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ConfigurationOverwrites. When set to true, ConfigurationOverwrites is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ConfigurationOverwrites is accessed. You can always execute/ a forced fetch by calling GetMultiConfigurationOverwrites(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchConfigurationOverwrites
		{
			get	{ return _alwaysFetchConfigurationOverwrites; }
			set	{ _alwaysFetchConfigurationOverwrites = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ConfigurationOverwrites already has been fetched. Setting this property to false when ConfigurationOverwrites has been fetched
		/// will clear the ConfigurationOverwrites collection well. Setting this property to true while ConfigurationOverwrites hasn't been fetched disables lazy loading for ConfigurationOverwrites</summary>
		[Browsable(false)]
		public bool AlreadyFetchedConfigurationOverwrites
		{
			get { return _alreadyFetchedConfigurationOverwrites;}
			set 
			{
				if(_alreadyFetchedConfigurationOverwrites && !value && (_configurationOverwrites != null))
				{
					_configurationOverwrites.Clear();
				}
				_alreadyFetchedConfigurationOverwrites = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FilterElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterElements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterElementCollection FilterElements
		{
			get	{ return GetMultiFilterElements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterElements. When set to true, FilterElements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterElements is accessed. You can always execute/ a forced fetch by calling GetMultiFilterElements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterElements
		{
			get	{ return _alwaysFetchFilterElements; }
			set	{ _alwaysFetchFilterElements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterElements already has been fetched. Setting this property to false when FilterElements has been fetched
		/// will clear the FilterElements collection well. Setting this property to true while FilterElements hasn't been fetched disables lazy loading for FilterElements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterElements
		{
			get { return _alreadyFetchedFilterElements;}
			set 
			{
				if(_alreadyFetchedFilterElements && !value && (_filterElements != null))
				{
					_filterElements.Clear();
				}
				_alreadyFetchedFilterElements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportToClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportToClients()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportToClientCollection ReportToClients
		{
			get	{ return GetMultiReportToClients(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportToClients. When set to true, ReportToClients is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportToClients is accessed. You can always execute/ a forced fetch by calling GetMultiReportToClients(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportToClients
		{
			get	{ return _alwaysFetchReportToClients; }
			set	{ _alwaysFetchReportToClients = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportToClients already has been fetched. Setting this property to false when ReportToClients has been fetched
		/// will clear the ReportToClients collection well. Setting this property to true while ReportToClients hasn't been fetched disables lazy loading for ReportToClients</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportToClients
		{
			get { return _alreadyFetchedReportToClients;}
			set 
			{
				if(_alreadyFetchedReportToClients && !value && (_reportToClients != null))
				{
					_reportToClients.Clear();
				}
				_alreadyFetchedReportToClients = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ConfigurationDefinitions", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.ConfigurationDefinitionEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
