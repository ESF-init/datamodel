﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'KeyAttributeTransfrom'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class KeyAttributeTransfromEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AttributeEntity _attribute;
		private bool	_alwaysFetchAttribute, _alreadyFetchedAttribute, _attributeReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueFrom;
		private bool	_alwaysFetchAttributeValueFrom, _alreadyFetchedAttributeValueFrom, _attributeValueFromReturnsNewIfNotFound;
		private AttributeValueEntity _attributeValueTo;
		private bool	_alwaysFetchAttributeValueTo, _alreadyFetchedAttributeValueTo, _attributeValueToReturnsNewIfNotFound;
		private PredefinedKeyEntity _predefinedKey;
		private bool	_alwaysFetchPredefinedKey, _alreadyFetchedPredefinedKey, _predefinedKeyReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Attribute</summary>
			public static readonly string Attribute = "Attribute";
			/// <summary>Member name AttributeValueFrom</summary>
			public static readonly string AttributeValueFrom = "AttributeValueFrom";
			/// <summary>Member name AttributeValueTo</summary>
			public static readonly string AttributeValueTo = "AttributeValueTo";
			/// <summary>Member name PredefinedKey</summary>
			public static readonly string PredefinedKey = "PredefinedKey";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static KeyAttributeTransfromEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public KeyAttributeTransfromEntity() :base("KeyAttributeTransfromEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		public KeyAttributeTransfromEntity(System.Int64 keyAttributeTransfromId):base("KeyAttributeTransfromEntity")
		{
			InitClassFetch(keyAttributeTransfromId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public KeyAttributeTransfromEntity(System.Int64 keyAttributeTransfromId, IPrefetchPath prefetchPathToUse):base("KeyAttributeTransfromEntity")
		{
			InitClassFetch(keyAttributeTransfromId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="validator">The custom validator object for this KeyAttributeTransfromEntity</param>
		public KeyAttributeTransfromEntity(System.Int64 keyAttributeTransfromId, IValidator validator):base("KeyAttributeTransfromEntity")
		{
			InitClassFetch(keyAttributeTransfromId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected KeyAttributeTransfromEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_attribute = (AttributeEntity)info.GetValue("_attribute", typeof(AttributeEntity));
			if(_attribute!=null)
			{
				_attribute.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeReturnsNewIfNotFound = info.GetBoolean("_attributeReturnsNewIfNotFound");
			_alwaysFetchAttribute = info.GetBoolean("_alwaysFetchAttribute");
			_alreadyFetchedAttribute = info.GetBoolean("_alreadyFetchedAttribute");

			_attributeValueFrom = (AttributeValueEntity)info.GetValue("_attributeValueFrom", typeof(AttributeValueEntity));
			if(_attributeValueFrom!=null)
			{
				_attributeValueFrom.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueFromReturnsNewIfNotFound = info.GetBoolean("_attributeValueFromReturnsNewIfNotFound");
			_alwaysFetchAttributeValueFrom = info.GetBoolean("_alwaysFetchAttributeValueFrom");
			_alreadyFetchedAttributeValueFrom = info.GetBoolean("_alreadyFetchedAttributeValueFrom");

			_attributeValueTo = (AttributeValueEntity)info.GetValue("_attributeValueTo", typeof(AttributeValueEntity));
			if(_attributeValueTo!=null)
			{
				_attributeValueTo.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_attributeValueToReturnsNewIfNotFound = info.GetBoolean("_attributeValueToReturnsNewIfNotFound");
			_alwaysFetchAttributeValueTo = info.GetBoolean("_alwaysFetchAttributeValueTo");
			_alreadyFetchedAttributeValueTo = info.GetBoolean("_alreadyFetchedAttributeValueTo");

			_predefinedKey = (PredefinedKeyEntity)info.GetValue("_predefinedKey", typeof(PredefinedKeyEntity));
			if(_predefinedKey!=null)
			{
				_predefinedKey.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_predefinedKeyReturnsNewIfNotFound = info.GetBoolean("_predefinedKeyReturnsNewIfNotFound");
			_alwaysFetchPredefinedKey = info.GetBoolean("_alwaysFetchPredefinedKey");
			_alreadyFetchedPredefinedKey = info.GetBoolean("_alreadyFetchedPredefinedKey");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((KeyAttributeTransfromFieldIndex)fieldIndex)
			{
				case KeyAttributeTransfromFieldIndex.AttributeID:
					DesetupSyncAttribute(true, false);
					_alreadyFetchedAttribute = false;
					break;
				case KeyAttributeTransfromFieldIndex.AttributeValueFromID:
					DesetupSyncAttributeValueFrom(true, false);
					_alreadyFetchedAttributeValueFrom = false;
					break;
				case KeyAttributeTransfromFieldIndex.AttributeValueToID:
					DesetupSyncAttributeValueTo(true, false);
					_alreadyFetchedAttributeValueTo = false;
					break;
				case KeyAttributeTransfromFieldIndex.KeyID:
					DesetupSyncPredefinedKey(true, false);
					_alreadyFetchedPredefinedKey = false;
					break;
				case KeyAttributeTransfromFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAttribute = (_attribute != null);
			_alreadyFetchedAttributeValueFrom = (_attributeValueFrom != null);
			_alreadyFetchedAttributeValueTo = (_attributeValueTo != null);
			_alreadyFetchedPredefinedKey = (_predefinedKey != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Attribute":
					toReturn.Add(Relations.AttributeEntityUsingAttributeID);
					break;
				case "AttributeValueFrom":
					toReturn.Add(Relations.AttributeValueEntityUsingAttributeValueFromID);
					break;
				case "AttributeValueTo":
					toReturn.Add(Relations.AttributeValueEntityUsingAttributeValueToID);
					break;
				case "PredefinedKey":
					toReturn.Add(Relations.PredefinedKeyEntityUsingKeyID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_attribute", (!this.MarkedForDeletion?_attribute:null));
			info.AddValue("_attributeReturnsNewIfNotFound", _attributeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttribute", _alwaysFetchAttribute);
			info.AddValue("_alreadyFetchedAttribute", _alreadyFetchedAttribute);
			info.AddValue("_attributeValueFrom", (!this.MarkedForDeletion?_attributeValueFrom:null));
			info.AddValue("_attributeValueFromReturnsNewIfNotFound", _attributeValueFromReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueFrom", _alwaysFetchAttributeValueFrom);
			info.AddValue("_alreadyFetchedAttributeValueFrom", _alreadyFetchedAttributeValueFrom);
			info.AddValue("_attributeValueTo", (!this.MarkedForDeletion?_attributeValueTo:null));
			info.AddValue("_attributeValueToReturnsNewIfNotFound", _attributeValueToReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAttributeValueTo", _alwaysFetchAttributeValueTo);
			info.AddValue("_alreadyFetchedAttributeValueTo", _alreadyFetchedAttributeValueTo);
			info.AddValue("_predefinedKey", (!this.MarkedForDeletion?_predefinedKey:null));
			info.AddValue("_predefinedKeyReturnsNewIfNotFound", _predefinedKeyReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPredefinedKey", _alwaysFetchPredefinedKey);
			info.AddValue("_alreadyFetchedPredefinedKey", _alreadyFetchedPredefinedKey);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Attribute":
					_alreadyFetchedAttribute = true;
					this.Attribute = (AttributeEntity)entity;
					break;
				case "AttributeValueFrom":
					_alreadyFetchedAttributeValueFrom = true;
					this.AttributeValueFrom = (AttributeValueEntity)entity;
					break;
				case "AttributeValueTo":
					_alreadyFetchedAttributeValueTo = true;
					this.AttributeValueTo = (AttributeValueEntity)entity;
					break;
				case "PredefinedKey":
					_alreadyFetchedPredefinedKey = true;
					this.PredefinedKey = (PredefinedKeyEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Attribute":
					SetupSyncAttribute(relatedEntity);
					break;
				case "AttributeValueFrom":
					SetupSyncAttributeValueFrom(relatedEntity);
					break;
				case "AttributeValueTo":
					SetupSyncAttributeValueTo(relatedEntity);
					break;
				case "PredefinedKey":
					SetupSyncPredefinedKey(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Attribute":
					DesetupSyncAttribute(false, true);
					break;
				case "AttributeValueFrom":
					DesetupSyncAttributeValueFrom(false, true);
					break;
				case "AttributeValueTo":
					DesetupSyncAttributeValueTo(false, true);
					break;
				case "PredefinedKey":
					DesetupSyncPredefinedKey(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_attribute!=null)
			{
				toReturn.Add(_attribute);
			}
			if(_attributeValueFrom!=null)
			{
				toReturn.Add(_attributeValueFrom);
			}
			if(_attributeValueTo!=null)
			{
				toReturn.Add(_attributeValueTo);
			}
			if(_predefinedKey!=null)
			{
				toReturn.Add(_predefinedKey);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyAttributeTransfromId)
		{
			return FetchUsingPK(keyAttributeTransfromId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyAttributeTransfromId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(keyAttributeTransfromId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyAttributeTransfromId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(keyAttributeTransfromId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 keyAttributeTransfromId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(keyAttributeTransfromId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.KeyAttributeTransfromId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new KeyAttributeTransfromRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public AttributeEntity GetSingleAttribute()
		{
			return GetSingleAttribute(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeEntity' which is related to this entity.</returns>
		public virtual AttributeEntity GetSingleAttribute(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttribute || forceFetch || _alwaysFetchAttribute) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeEntityUsingAttributeID);
				AttributeEntity newEntity = new AttributeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Attribute = newEntity;
				_alreadyFetchedAttribute = fetchResult;
			}
			return _attribute;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueFrom()
		{
			return GetSingleAttributeValueFrom(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueFrom(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueFrom || forceFetch || _alwaysFetchAttributeValueFrom) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingAttributeValueFromID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeValueFromID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueFromReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueFrom = newEntity;
				_alreadyFetchedAttributeValueFrom = fetchResult;
			}
			return _attributeValueFrom;
		}


		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleAttributeValueTo()
		{
			return GetSingleAttributeValueTo(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleAttributeValueTo(bool forceFetch)
		{
			if( ( !_alreadyFetchedAttributeValueTo || forceFetch || _alwaysFetchAttributeValueTo) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingAttributeValueToID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AttributeValueToID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_attributeValueToReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AttributeValueTo = newEntity;
				_alreadyFetchedAttributeValueTo = fetchResult;
			}
			return _attributeValueTo;
		}


		/// <summary> Retrieves the related entity of type 'PredefinedKeyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PredefinedKeyEntity' which is related to this entity.</returns>
		public PredefinedKeyEntity GetSinglePredefinedKey()
		{
			return GetSinglePredefinedKey(false);
		}

		/// <summary> Retrieves the related entity of type 'PredefinedKeyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PredefinedKeyEntity' which is related to this entity.</returns>
		public virtual PredefinedKeyEntity GetSinglePredefinedKey(bool forceFetch)
		{
			if( ( !_alreadyFetchedPredefinedKey || forceFetch || _alwaysFetchPredefinedKey) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PredefinedKeyEntityUsingKeyID);
				PredefinedKeyEntity newEntity = new PredefinedKeyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.KeyID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PredefinedKeyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_predefinedKeyReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PredefinedKey = newEntity;
				_alreadyFetchedPredefinedKey = fetchResult;
			}
			return _predefinedKey;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Attribute", _attribute);
			toReturn.Add("AttributeValueFrom", _attributeValueFrom);
			toReturn.Add("AttributeValueTo", _attributeValueTo);
			toReturn.Add("PredefinedKey", _predefinedKey);
			toReturn.Add("Tariff", _tariff);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="validator">The validator object for this KeyAttributeTransfromEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 keyAttributeTransfromId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(keyAttributeTransfromId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_attributeReturnsNewIfNotFound = false;
			_attributeValueFromReturnsNewIfNotFound = false;
			_attributeValueToReturnsNewIfNotFound = false;
			_predefinedKeyReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueFromID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttributeValueToID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyAttributeTransfromId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _attribute</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttribute(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeEntityUsingAttributeIDStatic, true, signalRelatedEntity, "KeyAttributeTransforms", resetFKFields, new int[] { (int)KeyAttributeTransfromFieldIndex.AttributeID } );		
			_attribute = null;
		}
		
		/// <summary> setups the sync logic for member _attribute</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttribute(IEntityCore relatedEntity)
		{
			if(_attribute!=relatedEntity)
			{		
				DesetupSyncAttribute(true, true);
				_attribute = (AttributeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attribute, new PropertyChangedEventHandler( OnAttributePropertyChanged ), "Attribute", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeEntityUsingAttributeIDStatic, true, ref _alreadyFetchedAttribute, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueFrom</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueFrom(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueFrom, new PropertyChangedEventHandler( OnAttributeValueFromPropertyChanged ), "AttributeValueFrom", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeValueEntityUsingAttributeValueFromIDStatic, true, signalRelatedEntity, "Key1AttributeTransforms", resetFKFields, new int[] { (int)KeyAttributeTransfromFieldIndex.AttributeValueFromID } );		
			_attributeValueFrom = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueFrom</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueFrom(IEntityCore relatedEntity)
		{
			if(_attributeValueFrom!=relatedEntity)
			{		
				DesetupSyncAttributeValueFrom(true, true);
				_attributeValueFrom = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueFrom, new PropertyChangedEventHandler( OnAttributeValueFromPropertyChanged ), "AttributeValueFrom", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeValueEntityUsingAttributeValueFromIDStatic, true, ref _alreadyFetchedAttributeValueFrom, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueFromPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _attributeValueTo</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAttributeValueTo(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _attributeValueTo, new PropertyChangedEventHandler( OnAttributeValueToPropertyChanged ), "AttributeValueTo", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeValueEntityUsingAttributeValueToIDStatic, true, signalRelatedEntity, "Key2AttributeTransforms", resetFKFields, new int[] { (int)KeyAttributeTransfromFieldIndex.AttributeValueToID } );		
			_attributeValueTo = null;
		}
		
		/// <summary> setups the sync logic for member _attributeValueTo</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAttributeValueTo(IEntityCore relatedEntity)
		{
			if(_attributeValueTo!=relatedEntity)
			{		
				DesetupSyncAttributeValueTo(true, true);
				_attributeValueTo = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _attributeValueTo, new PropertyChangedEventHandler( OnAttributeValueToPropertyChanged ), "AttributeValueTo", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.AttributeValueEntityUsingAttributeValueToIDStatic, true, ref _alreadyFetchedAttributeValueTo, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAttributeValueToPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _predefinedKey</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPredefinedKey(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _predefinedKey, new PropertyChangedEventHandler( OnPredefinedKeyPropertyChanged ), "PredefinedKey", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.PredefinedKeyEntityUsingKeyIDStatic, true, signalRelatedEntity, "KeyAttributeTransform", resetFKFields, new int[] { (int)KeyAttributeTransfromFieldIndex.KeyID } );		
			_predefinedKey = null;
		}
		
		/// <summary> setups the sync logic for member _predefinedKey</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPredefinedKey(IEntityCore relatedEntity)
		{
			if(_predefinedKey!=relatedEntity)
			{		
				DesetupSyncPredefinedKey(true, true);
				_predefinedKey = (PredefinedKeyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _predefinedKey, new PropertyChangedEventHandler( OnPredefinedKeyPropertyChanged ), "PredefinedKey", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.PredefinedKeyEntityUsingKeyIDStatic, true, ref _alreadyFetchedPredefinedKey, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPredefinedKeyPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "KeyAttributeTransforms", resetFKFields, new int[] { (int)KeyAttributeTransfromFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticKeyAttributeTransfromRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="keyAttributeTransfromId">PK value for KeyAttributeTransfrom which data should be fetched into this KeyAttributeTransfrom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 keyAttributeTransfromId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)KeyAttributeTransfromFieldIndex.KeyAttributeTransfromId].ForcedCurrentValueWrite(keyAttributeTransfromId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateKeyAttributeTransfromDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new KeyAttributeTransfromEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static KeyAttributeTransfromRelations Relations
		{
			get	{ return new KeyAttributeTransfromRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Attribute'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttribute
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeCollection(), (IEntityRelation)GetRelationsForField("Attribute")[0], (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, (int)VarioSL.Entities.EntityType.AttributeEntity, 0, null, null, null, "Attribute", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueFrom
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueFrom")[0], (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueFrom", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAttributeValueTo
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("AttributeValueTo")[0], (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "AttributeValueTo", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PredefinedKey'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPredefinedKey
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PredefinedKeyCollection(), (IEntityRelation)GetRelationsForField("PredefinedKey")[0], (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, (int)VarioSL.Entities.EntityType.PredefinedKeyEntity, 0, null, null, null, "PredefinedKey", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AttributeID property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."ATTRIBUTEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)KeyAttributeTransfromFieldIndex.AttributeID, false); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.AttributeID, value, true); }
		}

		/// <summary> The AttributeValueFromID property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."ATTRIBUTEVALUEIDFROM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueFromID
		{
			get { return (Nullable<System.Int64>)GetValue((int)KeyAttributeTransfromFieldIndex.AttributeValueFromID, false); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.AttributeValueFromID, value, true); }
		}

		/// <summary> The AttributeValueToID property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."ATTRIBUTEVALUEIDTO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AttributeValueToID
		{
			get { return (Nullable<System.Int64>)GetValue((int)KeyAttributeTransfromFieldIndex.AttributeValueToID, false); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.AttributeValueToID, value, true); }
		}

		/// <summary> The KeyAttributeTransfromId property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 KeyAttributeTransfromId
		{
			get { return (System.Int64)GetValue((int)KeyAttributeTransfromFieldIndex.KeyAttributeTransfromId, true); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.KeyAttributeTransfromId, value, true); }
		}

		/// <summary> The KeyID property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."KEYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> KeyID
		{
			get { return (Nullable<System.Int64>)GetValue((int)KeyAttributeTransfromFieldIndex.KeyID, false); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.KeyID, value, true); }
		}

		/// <summary> The TariffID property of the Entity KeyAttributeTransfrom<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_KEYATTRIBUTETRANSFORM"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TariffID
		{
			get { return (Nullable<System.Int64>)GetValue((int)KeyAttributeTransfromFieldIndex.TariffID, false); }
			set	{ SetValue((int)KeyAttributeTransfromFieldIndex.TariffID, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AttributeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttribute()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeEntity Attribute
		{
			get	{ return GetSingleAttribute(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttribute(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "KeyAttributeTransforms", "Attribute", _attribute, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Attribute. When set to true, Attribute is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Attribute is accessed. You can always execute a forced fetch by calling GetSingleAttribute(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttribute
		{
			get	{ return _alwaysFetchAttribute; }
			set	{ _alwaysFetchAttribute = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Attribute already has been fetched. Setting this property to false when Attribute has been fetched
		/// will set Attribute to null as well. Setting this property to true while Attribute hasn't been fetched disables lazy loading for Attribute</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttribute
		{
			get { return _alreadyFetchedAttribute;}
			set 
			{
				if(_alreadyFetchedAttribute && !value)
				{
					this.Attribute = null;
				}
				_alreadyFetchedAttribute = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Attribute is not found
		/// in the database. When set to true, Attribute will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeReturnsNewIfNotFound
		{
			get	{ return _attributeReturnsNewIfNotFound; }
			set { _attributeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueFrom()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueFrom
		{
			get	{ return GetSingleAttributeValueFrom(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueFrom(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Key1AttributeTransforms", "AttributeValueFrom", _attributeValueFrom, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueFrom. When set to true, AttributeValueFrom is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueFrom is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueFrom(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueFrom
		{
			get	{ return _alwaysFetchAttributeValueFrom; }
			set	{ _alwaysFetchAttributeValueFrom = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueFrom already has been fetched. Setting this property to false when AttributeValueFrom has been fetched
		/// will set AttributeValueFrom to null as well. Setting this property to true while AttributeValueFrom hasn't been fetched disables lazy loading for AttributeValueFrom</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueFrom
		{
			get { return _alreadyFetchedAttributeValueFrom;}
			set 
			{
				if(_alreadyFetchedAttributeValueFrom && !value)
				{
					this.AttributeValueFrom = null;
				}
				_alreadyFetchedAttributeValueFrom = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueFrom is not found
		/// in the database. When set to true, AttributeValueFrom will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueFromReturnsNewIfNotFound
		{
			get	{ return _attributeValueFromReturnsNewIfNotFound; }
			set { _attributeValueFromReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAttributeValueTo()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity AttributeValueTo
		{
			get	{ return GetSingleAttributeValueTo(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAttributeValueTo(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Key2AttributeTransforms", "AttributeValueTo", _attributeValueTo, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AttributeValueTo. When set to true, AttributeValueTo is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AttributeValueTo is accessed. You can always execute a forced fetch by calling GetSingleAttributeValueTo(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAttributeValueTo
		{
			get	{ return _alwaysFetchAttributeValueTo; }
			set	{ _alwaysFetchAttributeValueTo = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AttributeValueTo already has been fetched. Setting this property to false when AttributeValueTo has been fetched
		/// will set AttributeValueTo to null as well. Setting this property to true while AttributeValueTo hasn't been fetched disables lazy loading for AttributeValueTo</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAttributeValueTo
		{
			get { return _alreadyFetchedAttributeValueTo;}
			set 
			{
				if(_alreadyFetchedAttributeValueTo && !value)
				{
					this.AttributeValueTo = null;
				}
				_alreadyFetchedAttributeValueTo = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AttributeValueTo is not found
		/// in the database. When set to true, AttributeValueTo will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool AttributeValueToReturnsNewIfNotFound
		{
			get	{ return _attributeValueToReturnsNewIfNotFound; }
			set { _attributeValueToReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PredefinedKeyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePredefinedKey()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PredefinedKeyEntity PredefinedKey
		{
			get	{ return GetSinglePredefinedKey(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPredefinedKey(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "KeyAttributeTransform", "PredefinedKey", _predefinedKey, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PredefinedKey. When set to true, PredefinedKey is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PredefinedKey is accessed. You can always execute a forced fetch by calling GetSinglePredefinedKey(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPredefinedKey
		{
			get	{ return _alwaysFetchPredefinedKey; }
			set	{ _alwaysFetchPredefinedKey = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PredefinedKey already has been fetched. Setting this property to false when PredefinedKey has been fetched
		/// will set PredefinedKey to null as well. Setting this property to true while PredefinedKey hasn't been fetched disables lazy loading for PredefinedKey</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPredefinedKey
		{
			get { return _alreadyFetchedPredefinedKey;}
			set 
			{
				if(_alreadyFetchedPredefinedKey && !value)
				{
					this.PredefinedKey = null;
				}
				_alreadyFetchedPredefinedKey = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PredefinedKey is not found
		/// in the database. When set to true, PredefinedKey will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PredefinedKeyReturnsNewIfNotFound
		{
			get	{ return _predefinedKeyReturnsNewIfNotFound; }
			set { _predefinedKeyReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "KeyAttributeTransforms", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.KeyAttributeTransfromEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
