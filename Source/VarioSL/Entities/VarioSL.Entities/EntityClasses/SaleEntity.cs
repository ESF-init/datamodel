﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Sale'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class SaleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.PaymentJournalCollection	_paymentJournals;
		private bool	_alwaysFetchPaymentJournals, _alreadyFetchedPaymentJournals;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_refunds;
		private bool	_alwaysFetchRefunds, _alreadyFetchedRefunds;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private DeviceClassEntity _deviceClass;
		private bool	_alwaysFetchDeviceClass, _alreadyFetchedDeviceClass, _deviceClassReturnsNewIfNotFound;
		private ContractEntity _contract;
		private bool	_alwaysFetchContract, _alreadyFetchedContract, _contractReturnsNewIfNotFound;
		private ContractEntity _secondaryContract;
		private bool	_alwaysFetchSecondaryContract, _alreadyFetchedSecondaryContract, _secondaryContractReturnsNewIfNotFound;
		private CreditCardAuthorizationEntity _creditCardAuthorization;
		private bool	_alwaysFetchCreditCardAuthorization, _alreadyFetchedCreditCardAuthorization, _creditCardAuthorizationReturnsNewIfNotFound;
		private PersonEntity _person;
		private bool	_alwaysFetchPerson, _alreadyFetchedPerson, _personReturnsNewIfNotFound;
		private SaleEntity _refundedSale;
		private bool	_alwaysFetchRefundedSale, _alreadyFetchedRefundedSale, _refundedSaleReturnsNewIfNotFound;
		private SaleEntity _sale;
		private bool	_alwaysFetchSale, _alreadyFetchedSale, _saleReturnsNewIfNotFound;
		private BankStatementVerificationEntity _bankStatementVerification;
		private bool	_alwaysFetchBankStatementVerification, _alreadyFetchedBankStatementVerification, _bankStatementVerificationReturnsNewIfNotFound;
		private OrderEntity _order;
		private bool	_alwaysFetchOrder, _alreadyFetchedOrder, _orderReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name DeviceClass</summary>
			public static readonly string DeviceClass = "DeviceClass";
			/// <summary>Member name Contract</summary>
			public static readonly string Contract = "Contract";
			/// <summary>Member name SecondaryContract</summary>
			public static readonly string SecondaryContract = "SecondaryContract";
			/// <summary>Member name CreditCardAuthorization</summary>
			public static readonly string CreditCardAuthorization = "CreditCardAuthorization";
			/// <summary>Member name Person</summary>
			public static readonly string Person = "Person";
			/// <summary>Member name RefundedSale</summary>
			public static readonly string RefundedSale = "RefundedSale";
			/// <summary>Member name Sale</summary>
			public static readonly string Sale = "Sale";
			/// <summary>Member name PaymentJournals</summary>
			public static readonly string PaymentJournals = "PaymentJournals";
			/// <summary>Member name Refunds</summary>
			public static readonly string Refunds = "Refunds";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name BankStatementVerification</summary>
			public static readonly string BankStatementVerification = "BankStatementVerification";
			/// <summary>Member name Order</summary>
			public static readonly string Order = "Order";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SaleEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SaleEntity() :base("SaleEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		public SaleEntity(System.Int64 saleID):base("SaleEntity")
		{
			InitClassFetch(saleID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SaleEntity(System.Int64 saleID, IPrefetchPath prefetchPathToUse):base("SaleEntity")
		{
			InitClassFetch(saleID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="validator">The custom validator object for this SaleEntity</param>
		public SaleEntity(System.Int64 saleID, IValidator validator):base("SaleEntity")
		{
			InitClassFetch(saleID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SaleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_paymentJournals = (VarioSL.Entities.CollectionClasses.PaymentJournalCollection)info.GetValue("_paymentJournals", typeof(VarioSL.Entities.CollectionClasses.PaymentJournalCollection));
			_alwaysFetchPaymentJournals = info.GetBoolean("_alwaysFetchPaymentJournals");
			_alreadyFetchedPaymentJournals = info.GetBoolean("_alreadyFetchedPaymentJournals");

			_refunds = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_refunds", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchRefunds = info.GetBoolean("_alwaysFetchRefunds");
			_alreadyFetchedRefunds = info.GetBoolean("_alreadyFetchedRefunds");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_deviceClass = (DeviceClassEntity)info.GetValue("_deviceClass", typeof(DeviceClassEntity));
			if(_deviceClass!=null)
			{
				_deviceClass.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_deviceClassReturnsNewIfNotFound = info.GetBoolean("_deviceClassReturnsNewIfNotFound");
			_alwaysFetchDeviceClass = info.GetBoolean("_alwaysFetchDeviceClass");
			_alreadyFetchedDeviceClass = info.GetBoolean("_alreadyFetchedDeviceClass");

			_contract = (ContractEntity)info.GetValue("_contract", typeof(ContractEntity));
			if(_contract!=null)
			{
				_contract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_contractReturnsNewIfNotFound = info.GetBoolean("_contractReturnsNewIfNotFound");
			_alwaysFetchContract = info.GetBoolean("_alwaysFetchContract");
			_alreadyFetchedContract = info.GetBoolean("_alreadyFetchedContract");

			_secondaryContract = (ContractEntity)info.GetValue("_secondaryContract", typeof(ContractEntity));
			if(_secondaryContract!=null)
			{
				_secondaryContract.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_secondaryContractReturnsNewIfNotFound = info.GetBoolean("_secondaryContractReturnsNewIfNotFound");
			_alwaysFetchSecondaryContract = info.GetBoolean("_alwaysFetchSecondaryContract");
			_alreadyFetchedSecondaryContract = info.GetBoolean("_alreadyFetchedSecondaryContract");

			_creditCardAuthorization = (CreditCardAuthorizationEntity)info.GetValue("_creditCardAuthorization", typeof(CreditCardAuthorizationEntity));
			if(_creditCardAuthorization!=null)
			{
				_creditCardAuthorization.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_creditCardAuthorizationReturnsNewIfNotFound = info.GetBoolean("_creditCardAuthorizationReturnsNewIfNotFound");
			_alwaysFetchCreditCardAuthorization = info.GetBoolean("_alwaysFetchCreditCardAuthorization");
			_alreadyFetchedCreditCardAuthorization = info.GetBoolean("_alreadyFetchedCreditCardAuthorization");

			_person = (PersonEntity)info.GetValue("_person", typeof(PersonEntity));
			if(_person!=null)
			{
				_person.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_personReturnsNewIfNotFound = info.GetBoolean("_personReturnsNewIfNotFound");
			_alwaysFetchPerson = info.GetBoolean("_alwaysFetchPerson");
			_alreadyFetchedPerson = info.GetBoolean("_alreadyFetchedPerson");

			_refundedSale = (SaleEntity)info.GetValue("_refundedSale", typeof(SaleEntity));
			if(_refundedSale!=null)
			{
				_refundedSale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_refundedSaleReturnsNewIfNotFound = info.GetBoolean("_refundedSaleReturnsNewIfNotFound");
			_alwaysFetchRefundedSale = info.GetBoolean("_alwaysFetchRefundedSale");
			_alreadyFetchedRefundedSale = info.GetBoolean("_alreadyFetchedRefundedSale");

			_sale = (SaleEntity)info.GetValue("_sale", typeof(SaleEntity));
			if(_sale!=null)
			{
				_sale.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_saleReturnsNewIfNotFound = info.GetBoolean("_saleReturnsNewIfNotFound");
			_alwaysFetchSale = info.GetBoolean("_alwaysFetchSale");
			_alreadyFetchedSale = info.GetBoolean("_alreadyFetchedSale");
			_bankStatementVerification = (BankStatementVerificationEntity)info.GetValue("_bankStatementVerification", typeof(BankStatementVerificationEntity));
			if(_bankStatementVerification!=null)
			{
				_bankStatementVerification.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_bankStatementVerificationReturnsNewIfNotFound = info.GetBoolean("_bankStatementVerificationReturnsNewIfNotFound");
			_alwaysFetchBankStatementVerification = info.GetBoolean("_alwaysFetchBankStatementVerification");
			_alreadyFetchedBankStatementVerification = info.GetBoolean("_alreadyFetchedBankStatementVerification");

			_order = (OrderEntity)info.GetValue("_order", typeof(OrderEntity));
			if(_order!=null)
			{
				_order.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderReturnsNewIfNotFound = info.GetBoolean("_orderReturnsNewIfNotFound");
			_alwaysFetchOrder = info.GetBoolean("_alwaysFetchOrder");
			_alreadyFetchedOrder = info.GetBoolean("_alreadyFetchedOrder");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SaleFieldIndex)fieldIndex)
			{
				case SaleFieldIndex.CancellationReferenceID:
					DesetupSyncSale(true, false);
					_alreadyFetchedSale = false;
					break;
				case SaleFieldIndex.ClientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case SaleFieldIndex.ContractID:
					DesetupSyncContract(true, false);
					_alreadyFetchedContract = false;
					break;
				case SaleFieldIndex.CreditCardAuthorizationID:
					DesetupSyncCreditCardAuthorization(true, false);
					_alreadyFetchedCreditCardAuthorization = false;
					break;
				case SaleFieldIndex.OrderID:
					DesetupSyncOrder(true, false);
					_alreadyFetchedOrder = false;
					break;
				case SaleFieldIndex.PersonID:
					DesetupSyncPerson(true, false);
					_alreadyFetchedPerson = false;
					break;
				case SaleFieldIndex.RefundReferenceID:
					DesetupSyncRefundedSale(true, false);
					_alreadyFetchedRefundedSale = false;
					break;
				case SaleFieldIndex.SalesChannelID:
					DesetupSyncDeviceClass(true, false);
					_alreadyFetchedDeviceClass = false;
					break;
				case SaleFieldIndex.SecondaryContractID:
					DesetupSyncSecondaryContract(true, false);
					_alreadyFetchedSecondaryContract = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedPaymentJournals = (_paymentJournals.Count > 0);
			_alreadyFetchedRefunds = (_refunds.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedDeviceClass = (_deviceClass != null);
			_alreadyFetchedContract = (_contract != null);
			_alreadyFetchedSecondaryContract = (_secondaryContract != null);
			_alreadyFetchedCreditCardAuthorization = (_creditCardAuthorization != null);
			_alreadyFetchedPerson = (_person != null);
			_alreadyFetchedRefundedSale = (_refundedSale != null);
			_alreadyFetchedSale = (_sale != null);
			_alreadyFetchedBankStatementVerification = (_bankStatementVerification != null);
			_alreadyFetchedOrder = (_order != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingClientID);
					break;
				case "DeviceClass":
					toReturn.Add(Relations.DeviceClassEntityUsingSalesChannelID);
					break;
				case "Contract":
					toReturn.Add(Relations.ContractEntityUsingContractID);
					break;
				case "SecondaryContract":
					toReturn.Add(Relations.ContractEntityUsingSecondaryContractID);
					break;
				case "CreditCardAuthorization":
					toReturn.Add(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
					break;
				case "Person":
					toReturn.Add(Relations.PersonEntityUsingPersonID);
					break;
				case "RefundedSale":
					toReturn.Add(Relations.SaleEntityUsingSaleIDRefundReferenceID);
					break;
				case "Sale":
					toReturn.Add(Relations.SaleEntityUsingSaleIDCancellationReferenceID);
					break;
				case "PaymentJournals":
					toReturn.Add(Relations.PaymentJournalEntityUsingSaleID);
					break;
				case "Refunds":
					toReturn.Add(Relations.SaleEntityUsingRefundReferenceID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingCancellationReferenceID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingSaleID);
					break;
				case "BankStatementVerification":
					toReturn.Add(Relations.BankStatementVerificationEntityUsingSaleID);
					break;
				case "Order":
					toReturn.Add(Relations.OrderEntityUsingOrderID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_paymentJournals", (!this.MarkedForDeletion?_paymentJournals:null));
			info.AddValue("_alwaysFetchPaymentJournals", _alwaysFetchPaymentJournals);
			info.AddValue("_alreadyFetchedPaymentJournals", _alreadyFetchedPaymentJournals);
			info.AddValue("_refunds", (!this.MarkedForDeletion?_refunds:null));
			info.AddValue("_alwaysFetchRefunds", _alwaysFetchRefunds);
			info.AddValue("_alreadyFetchedRefunds", _alreadyFetchedRefunds);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_deviceClass", (!this.MarkedForDeletion?_deviceClass:null));
			info.AddValue("_deviceClassReturnsNewIfNotFound", _deviceClassReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDeviceClass", _alwaysFetchDeviceClass);
			info.AddValue("_alreadyFetchedDeviceClass", _alreadyFetchedDeviceClass);
			info.AddValue("_contract", (!this.MarkedForDeletion?_contract:null));
			info.AddValue("_contractReturnsNewIfNotFound", _contractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchContract", _alwaysFetchContract);
			info.AddValue("_alreadyFetchedContract", _alreadyFetchedContract);
			info.AddValue("_secondaryContract", (!this.MarkedForDeletion?_secondaryContract:null));
			info.AddValue("_secondaryContractReturnsNewIfNotFound", _secondaryContractReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSecondaryContract", _alwaysFetchSecondaryContract);
			info.AddValue("_alreadyFetchedSecondaryContract", _alreadyFetchedSecondaryContract);
			info.AddValue("_creditCardAuthorization", (!this.MarkedForDeletion?_creditCardAuthorization:null));
			info.AddValue("_creditCardAuthorizationReturnsNewIfNotFound", _creditCardAuthorizationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCreditCardAuthorization", _alwaysFetchCreditCardAuthorization);
			info.AddValue("_alreadyFetchedCreditCardAuthorization", _alreadyFetchedCreditCardAuthorization);
			info.AddValue("_person", (!this.MarkedForDeletion?_person:null));
			info.AddValue("_personReturnsNewIfNotFound", _personReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPerson", _alwaysFetchPerson);
			info.AddValue("_alreadyFetchedPerson", _alreadyFetchedPerson);
			info.AddValue("_refundedSale", (!this.MarkedForDeletion?_refundedSale:null));
			info.AddValue("_refundedSaleReturnsNewIfNotFound", _refundedSaleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRefundedSale", _alwaysFetchRefundedSale);
			info.AddValue("_alreadyFetchedRefundedSale", _alreadyFetchedRefundedSale);
			info.AddValue("_sale", (!this.MarkedForDeletion?_sale:null));
			info.AddValue("_saleReturnsNewIfNotFound", _saleReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSale", _alwaysFetchSale);
			info.AddValue("_alreadyFetchedSale", _alreadyFetchedSale);

			info.AddValue("_bankStatementVerification", (!this.MarkedForDeletion?_bankStatementVerification:null));
			info.AddValue("_bankStatementVerificationReturnsNewIfNotFound", _bankStatementVerificationReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchBankStatementVerification", _alwaysFetchBankStatementVerification);
			info.AddValue("_alreadyFetchedBankStatementVerification", _alreadyFetchedBankStatementVerification);

			info.AddValue("_order", (!this.MarkedForDeletion?_order:null));
			info.AddValue("_orderReturnsNewIfNotFound", _orderReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrder", _alwaysFetchOrder);
			info.AddValue("_alreadyFetchedOrder", _alreadyFetchedOrder);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "DeviceClass":
					_alreadyFetchedDeviceClass = true;
					this.DeviceClass = (DeviceClassEntity)entity;
					break;
				case "Contract":
					_alreadyFetchedContract = true;
					this.Contract = (ContractEntity)entity;
					break;
				case "SecondaryContract":
					_alreadyFetchedSecondaryContract = true;
					this.SecondaryContract = (ContractEntity)entity;
					break;
				case "CreditCardAuthorization":
					_alreadyFetchedCreditCardAuthorization = true;
					this.CreditCardAuthorization = (CreditCardAuthorizationEntity)entity;
					break;
				case "Person":
					_alreadyFetchedPerson = true;
					this.Person = (PersonEntity)entity;
					break;
				case "RefundedSale":
					_alreadyFetchedRefundedSale = true;
					this.RefundedSale = (SaleEntity)entity;
					break;
				case "Sale":
					_alreadyFetchedSale = true;
					this.Sale = (SaleEntity)entity;
					break;
				case "PaymentJournals":
					_alreadyFetchedPaymentJournals = true;
					if(entity!=null)
					{
						this.PaymentJournals.Add((PaymentJournalEntity)entity);
					}
					break;
				case "Refunds":
					_alreadyFetchedRefunds = true;
					if(entity!=null)
					{
						this.Refunds.Add((SaleEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "BankStatementVerification":
					_alreadyFetchedBankStatementVerification = true;
					this.BankStatementVerification = (BankStatementVerificationEntity)entity;
					break;
				case "Order":
					_alreadyFetchedOrder = true;
					this.Order = (OrderEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "DeviceClass":
					SetupSyncDeviceClass(relatedEntity);
					break;
				case "Contract":
					SetupSyncContract(relatedEntity);
					break;
				case "SecondaryContract":
					SetupSyncSecondaryContract(relatedEntity);
					break;
				case "CreditCardAuthorization":
					SetupSyncCreditCardAuthorization(relatedEntity);
					break;
				case "Person":
					SetupSyncPerson(relatedEntity);
					break;
				case "RefundedSale":
					SetupSyncRefundedSale(relatedEntity);
					break;
				case "Sale":
					SetupSyncSale(relatedEntity);
					break;
				case "PaymentJournals":
					_paymentJournals.Add((PaymentJournalEntity)relatedEntity);
					break;
				case "Refunds":
					_refunds.Add((SaleEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "BankStatementVerification":
					SetupSyncBankStatementVerification(relatedEntity);
					break;
				case "Order":
					SetupSyncOrder(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "DeviceClass":
					DesetupSyncDeviceClass(false, true);
					break;
				case "Contract":
					DesetupSyncContract(false, true);
					break;
				case "SecondaryContract":
					DesetupSyncSecondaryContract(false, true);
					break;
				case "CreditCardAuthorization":
					DesetupSyncCreditCardAuthorization(false, true);
					break;
				case "Person":
					DesetupSyncPerson(false, true);
					break;
				case "RefundedSale":
					DesetupSyncRefundedSale(false, true);
					break;
				case "Sale":
					DesetupSyncSale(false, true);
					break;
				case "PaymentJournals":
					this.PerformRelatedEntityRemoval(_paymentJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Refunds":
					this.PerformRelatedEntityRemoval(_refunds, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BankStatementVerification":
					DesetupSyncBankStatementVerification(false, true);
					break;
				case "Order":
					DesetupSyncOrder(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_bankStatementVerification!=null)
			{
				toReturn.Add(_bankStatementVerification);
			}
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_deviceClass!=null)
			{
				toReturn.Add(_deviceClass);
			}
			if(_contract!=null)
			{
				toReturn.Add(_contract);
			}
			if(_secondaryContract!=null)
			{
				toReturn.Add(_secondaryContract);
			}
			if(_creditCardAuthorization!=null)
			{
				toReturn.Add(_creditCardAuthorization);
			}
			if(_person!=null)
			{
				toReturn.Add(_person);
			}
			if(_refundedSale!=null)
			{
				toReturn.Add(_refundedSale);
			}
			if(_sale!=null)
			{
				toReturn.Add(_sale);
			}
			if(_order!=null)
			{
				toReturn.Add(_order);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_paymentJournals);
			toReturn.Add(_refunds);
			toReturn.Add(_sales);
			toReturn.Add(_transactionJournals);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleTransactionID(System.String saleTransactionID)
		{
			return FetchUsingUCSaleTransactionID( saleTransactionID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleTransactionID(System.String saleTransactionID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCSaleTransactionID( saleTransactionID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleTransactionID(System.String saleTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCSaleTransactionID( saleTransactionID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="saleTransactionID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCSaleTransactionID(System.String saleTransactionID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((SaleDAO)CreateDAOInstance()).FetchSaleUsingUCSaleTransactionID(this, this.Transaction, saleTransactionID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="orderID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCOrderID(Nullable<System.Int64> orderID)
		{
			return FetchUsingUCOrderID( orderID, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="orderID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCOrderID(Nullable<System.Int64> orderID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCOrderID( orderID, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="orderID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCOrderID(Nullable<System.Int64> orderID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCOrderID( orderID, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="orderID">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCOrderID(Nullable<System.Int64> orderID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((SaleDAO)CreateDAOInstance()).FetchSaleUsingUCOrderID(this, this.Transaction, orderID, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 saleID)
		{
			return FetchUsingPK(saleID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 saleID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(saleID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 saleID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(saleID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 saleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(saleID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SaleID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SaleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PaymentJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPaymentJournals(forceFetch, _paymentJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPaymentJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection GetMultiPaymentJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPaymentJournals || forceFetch || _alwaysFetchPaymentJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_paymentJournals);
				_paymentJournals.SuppressClearInGetMulti=!forceFetch;
				_paymentJournals.EntityFactoryToUse = entityFactoryToUse;
				_paymentJournals.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_paymentJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedPaymentJournals = true;
			}
			return _paymentJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'PaymentJournals'. These settings will be taken into account
		/// when the property PaymentJournals is requested or GetMultiPaymentJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPaymentJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_paymentJournals.SortClauses=sortClauses;
			_paymentJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiRefunds(bool forceFetch)
		{
			return GetMultiRefunds(forceFetch, _refunds.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiRefunds(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRefunds(forceFetch, _refunds.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiRefunds(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRefunds(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiRefunds(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRefunds || forceFetch || _alwaysFetchRefunds) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_refunds);
				_refunds.SuppressClearInGetMulti=!forceFetch;
				_refunds.EntityFactoryToUse = entityFactoryToUse;
				_refunds.GetMultiManyToOne(null, null, null, null, null, null, this, null, filter);
				_refunds.SuppressClearInGetMulti=false;
				_alreadyFetchedRefunds = true;
			}
			return _refunds;
		}

		/// <summary> Sets the collection parameters for the collection for 'Refunds'. These settings will be taken into account
		/// when the property Refunds is requested or GetMultiRefunds is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRefunds(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_refunds.SortClauses=sortClauses;
			_refunds.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(null, null, null, null, null, null, null, this, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientID);
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public DeviceClassEntity GetSingleDeviceClass()
		{
			return GetSingleDeviceClass(false);
		}

		/// <summary> Retrieves the related entity of type 'DeviceClassEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'DeviceClassEntity' which is related to this entity.</returns>
		public virtual DeviceClassEntity GetSingleDeviceClass(bool forceFetch)
		{
			if( ( !_alreadyFetchedDeviceClass || forceFetch || _alwaysFetchDeviceClass) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.DeviceClassEntityUsingSalesChannelID);
				DeviceClassEntity newEntity = new DeviceClassEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SalesChannelID);
				}
				if(fetchResult)
				{
					newEntity = (DeviceClassEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_deviceClassReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DeviceClass = newEntity;
				_alreadyFetchedDeviceClass = fetchResult;
			}
			return _deviceClass;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleContract()
		{
			return GetSingleContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedContract || forceFetch || _alwaysFetchContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_contractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Contract = newEntity;
				_alreadyFetchedContract = fetchResult;
			}
			return _contract;
		}


		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public ContractEntity GetSingleSecondaryContract()
		{
			return GetSingleSecondaryContract(false);
		}

		/// <summary> Retrieves the related entity of type 'ContractEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ContractEntity' which is related to this entity.</returns>
		public virtual ContractEntity GetSingleSecondaryContract(bool forceFetch)
		{
			if( ( !_alreadyFetchedSecondaryContract || forceFetch || _alwaysFetchSecondaryContract) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ContractEntityUsingSecondaryContractID);
				ContractEntity newEntity = new ContractEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SecondaryContractID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ContractEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_secondaryContractReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SecondaryContract = newEntity;
				_alreadyFetchedSecondaryContract = fetchResult;
			}
			return _secondaryContract;
		}


		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public CreditCardAuthorizationEntity GetSingleCreditCardAuthorization()
		{
			return GetSingleCreditCardAuthorization(false);
		}

		/// <summary> Retrieves the related entity of type 'CreditCardAuthorizationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CreditCardAuthorizationEntity' which is related to this entity.</returns>
		public virtual CreditCardAuthorizationEntity GetSingleCreditCardAuthorization(bool forceFetch)
		{
			if( ( !_alreadyFetchedCreditCardAuthorization || forceFetch || _alwaysFetchCreditCardAuthorization) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
				CreditCardAuthorizationEntity newEntity = new CreditCardAuthorizationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CreditCardAuthorizationID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CreditCardAuthorizationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_creditCardAuthorizationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CreditCardAuthorization = newEntity;
				_alreadyFetchedCreditCardAuthorization = fetchResult;
			}
			return _creditCardAuthorization;
		}


		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public PersonEntity GetSinglePerson()
		{
			return GetSinglePerson(false);
		}

		/// <summary> Retrieves the related entity of type 'PersonEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PersonEntity' which is related to this entity.</returns>
		public virtual PersonEntity GetSinglePerson(bool forceFetch)
		{
			if( ( !_alreadyFetchedPerson || forceFetch || _alwaysFetchPerson) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PersonEntityUsingPersonID);
				PersonEntity newEntity = (PersonEntity)GeneralEntityFactory.Create(VarioSL.Entities.EntityType.PersonEntity);
				bool fetchResult = false;
				if(performLazyLoading)
				{
					newEntity = PersonEntity.FetchPolymorphic(this.Transaction, this.PersonID.GetValueOrDefault(), this.ActiveContext);
					fetchResult = (newEntity.Fields.State==EntityState.Fetched);
				}
				if(fetchResult)
				{
					newEntity = (PersonEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_personReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Person = newEntity;
				_alreadyFetchedPerson = fetchResult;
			}
			return _person;
		}


		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleRefundedSale()
		{
			return GetSingleRefundedSale(false);
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleRefundedSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedRefundedSale || forceFetch || _alwaysFetchRefundedSale) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingSaleIDRefundReferenceID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.RefundReferenceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_refundedSaleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RefundedSale = newEntity;
				_alreadyFetchedRefundedSale = fetchResult;
			}
			return _refundedSale;
		}


		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public SaleEntity GetSingleSale()
		{
			return GetSingleSale(false);
		}

		/// <summary> Retrieves the related entity of type 'SaleEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SaleEntity' which is related to this entity.</returns>
		public virtual SaleEntity GetSingleSale(bool forceFetch)
		{
			if( ( !_alreadyFetchedSale || forceFetch || _alwaysFetchSale) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SaleEntityUsingSaleIDCancellationReferenceID);
				SaleEntity newEntity = new SaleEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CancellationReferenceID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SaleEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_saleReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Sale = newEntity;
				_alreadyFetchedSale = fetchResult;
			}
			return _sale;
		}

		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public BankStatementVerificationEntity GetSingleBankStatementVerification()
		{
			return GetSingleBankStatementVerification(false);
		}
		
		/// <summary> Retrieves the related entity of type 'BankStatementVerificationEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'BankStatementVerificationEntity' which is related to this entity.</returns>
		public virtual BankStatementVerificationEntity GetSingleBankStatementVerification(bool forceFetch)
		{
			if( ( !_alreadyFetchedBankStatementVerification || forceFetch || _alwaysFetchBankStatementVerification) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.BankStatementVerificationEntityUsingSaleID);
				BankStatementVerificationEntity newEntity = new BankStatementVerificationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingUCSaleID(this.SaleID);
				}
				if(fetchResult)
				{
					newEntity = (BankStatementVerificationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_bankStatementVerificationReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.BankStatementVerification = newEntity;
				_alreadyFetchedBankStatementVerification = fetchResult;
			}
			return _bankStatementVerification;
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type '1:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrder()
		{
			return GetSingleOrder(false);
		}
		
		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type '1:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrder(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrder || forceFetch || _alwaysFetchOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode )
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderID);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Order = newEntity;
				_alreadyFetchedOrder = fetchResult;
			}
			return _order;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("DeviceClass", _deviceClass);
			toReturn.Add("Contract", _contract);
			toReturn.Add("SecondaryContract", _secondaryContract);
			toReturn.Add("CreditCardAuthorization", _creditCardAuthorization);
			toReturn.Add("Person", _person);
			toReturn.Add("RefundedSale", _refundedSale);
			toReturn.Add("Sale", _sale);
			toReturn.Add("PaymentJournals", _paymentJournals);
			toReturn.Add("Refunds", _refunds);
			toReturn.Add("Sales", _sales);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("BankStatementVerification", _bankStatementVerification);
			toReturn.Add("Order", _order);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="validator">The validator object for this SaleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 saleID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(saleID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_paymentJournals = new VarioSL.Entities.CollectionClasses.PaymentJournalCollection();
			_paymentJournals.SetContainingEntityInfo(this, "Sale");

			_refunds = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_refunds.SetContainingEntityInfo(this, "RefundedSale");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "Sale");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "Sale");
			_clientReturnsNewIfNotFound = false;
			_deviceClassReturnsNewIfNotFound = false;
			_contractReturnsNewIfNotFound = false;
			_secondaryContractReturnsNewIfNotFound = false;
			_creditCardAuthorizationReturnsNewIfNotFound = false;
			_personReturnsNewIfNotFound = false;
			_refundedSaleReturnsNewIfNotFound = false;
			_saleReturnsNewIfNotFound = false;
			_bankStatementVerificationReturnsNewIfNotFound = false;
			_orderReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReferenceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalOrderNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsClosed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsOrganizational", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MerchantRetailer", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NetworkReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReceiptReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RefundReferenceID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesPersonEmail", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesPersonNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleTransactionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SecondaryContractID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShiftInventoryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticSaleRelations.ClientEntityUsingClientIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.ClientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticSaleRelations.ClientEntityUsingClientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _deviceClass</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDeviceClass(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticSaleRelations.DeviceClassEntityUsingSalesChannelIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.SalesChannelID } );		
			_deviceClass = null;
		}
		
		/// <summary> setups the sync logic for member _deviceClass</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDeviceClass(IEntityCore relatedEntity)
		{
			if(_deviceClass!=relatedEntity)
			{		
				DesetupSyncDeviceClass(true, true);
				_deviceClass = (DeviceClassEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _deviceClass, new PropertyChangedEventHandler( OnDeviceClassPropertyChanged ), "DeviceClass", VarioSL.Entities.RelationClasses.StaticSaleRelations.DeviceClassEntityUsingSalesChannelIDStatic, true, ref _alreadyFetchedDeviceClass, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDeviceClassPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _contract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticSaleRelations.ContractEntityUsingContractIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.ContractID } );		
			_contract = null;
		}
		
		/// <summary> setups the sync logic for member _contract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncContract(IEntityCore relatedEntity)
		{
			if(_contract!=relatedEntity)
			{		
				DesetupSyncContract(true, true);
				_contract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _contract, new PropertyChangedEventHandler( OnContractPropertyChanged ), "Contract", VarioSL.Entities.RelationClasses.StaticSaleRelations.ContractEntityUsingContractIDStatic, true, ref _alreadyFetchedContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _secondaryContract</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSecondaryContract(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _secondaryContract, new PropertyChangedEventHandler( OnSecondaryContractPropertyChanged ), "SecondaryContract", VarioSL.Entities.RelationClasses.StaticSaleRelations.ContractEntityUsingSecondaryContractIDStatic, true, signalRelatedEntity, "SecondarySales", resetFKFields, new int[] { (int)SaleFieldIndex.SecondaryContractID } );		
			_secondaryContract = null;
		}
		
		/// <summary> setups the sync logic for member _secondaryContract</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSecondaryContract(IEntityCore relatedEntity)
		{
			if(_secondaryContract!=relatedEntity)
			{		
				DesetupSyncSecondaryContract(true, true);
				_secondaryContract = (ContractEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _secondaryContract, new PropertyChangedEventHandler( OnSecondaryContractPropertyChanged ), "SecondaryContract", VarioSL.Entities.RelationClasses.StaticSaleRelations.ContractEntityUsingSecondaryContractIDStatic, true, ref _alreadyFetchedSecondaryContract, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSecondaryContractPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _creditCardAuthorization</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCreditCardAuthorization(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticSaleRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.CreditCardAuthorizationID } );		
			_creditCardAuthorization = null;
		}
		
		/// <summary> setups the sync logic for member _creditCardAuthorization</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCreditCardAuthorization(IEntityCore relatedEntity)
		{
			if(_creditCardAuthorization!=relatedEntity)
			{		
				DesetupSyncCreditCardAuthorization(true, true);
				_creditCardAuthorization = (CreditCardAuthorizationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _creditCardAuthorization, new PropertyChangedEventHandler( OnCreditCardAuthorizationPropertyChanged ), "CreditCardAuthorization", VarioSL.Entities.RelationClasses.StaticSaleRelations.CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic, true, ref _alreadyFetchedCreditCardAuthorization, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCreditCardAuthorizationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _person</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPerson(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticSaleRelations.PersonEntityUsingPersonIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.PersonID } );		
			_person = null;
		}
		
		/// <summary> setups the sync logic for member _person</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPerson(IEntityCore relatedEntity)
		{
			if(_person!=relatedEntity)
			{		
				DesetupSyncPerson(true, true);
				_person = (PersonEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _person, new PropertyChangedEventHandler( OnPersonPropertyChanged ), "Person", VarioSL.Entities.RelationClasses.StaticSaleRelations.PersonEntityUsingPersonIDStatic, true, ref _alreadyFetchedPerson, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPersonPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _refundedSale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRefundedSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _refundedSale, new PropertyChangedEventHandler( OnRefundedSalePropertyChanged ), "RefundedSale", VarioSL.Entities.RelationClasses.StaticSaleRelations.SaleEntityUsingSaleIDRefundReferenceIDStatic, true, signalRelatedEntity, "Refunds", resetFKFields, new int[] { (int)SaleFieldIndex.RefundReferenceID } );		
			_refundedSale = null;
		}
		
		/// <summary> setups the sync logic for member _refundedSale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRefundedSale(IEntityCore relatedEntity)
		{
			if(_refundedSale!=relatedEntity)
			{		
				DesetupSyncRefundedSale(true, true);
				_refundedSale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _refundedSale, new PropertyChangedEventHandler( OnRefundedSalePropertyChanged ), "RefundedSale", VarioSL.Entities.RelationClasses.StaticSaleRelations.SaleEntityUsingSaleIDRefundReferenceIDStatic, true, ref _alreadyFetchedRefundedSale, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRefundedSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _sale</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSale(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticSaleRelations.SaleEntityUsingSaleIDCancellationReferenceIDStatic, true, signalRelatedEntity, "Sales", resetFKFields, new int[] { (int)SaleFieldIndex.CancellationReferenceID } );		
			_sale = null;
		}
		
		/// <summary> setups the sync logic for member _sale</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSale(IEntityCore relatedEntity)
		{
			if(_sale!=relatedEntity)
			{		
				DesetupSyncSale(true, true);
				_sale = (SaleEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _sale, new PropertyChangedEventHandler( OnSalePropertyChanged ), "Sale", VarioSL.Entities.RelationClasses.StaticSaleRelations.SaleEntityUsingSaleIDCancellationReferenceIDStatic, true, ref _alreadyFetchedSale, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSalePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _bankStatementVerification</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncBankStatementVerification(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticSaleRelations.BankStatementVerificationEntityUsingSaleIDStatic, false, signalRelatedEntity, "Sale", false, new int[] { (int)SaleFieldIndex.SaleID } );
			_bankStatementVerification = null;
		}
	
		/// <summary> setups the sync logic for member _bankStatementVerification</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncBankStatementVerification(IEntityCore relatedEntity)
		{
			if(_bankStatementVerification!=relatedEntity)
			{
				DesetupSyncBankStatementVerification(true, true);
				_bankStatementVerification = (BankStatementVerificationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _bankStatementVerification, new PropertyChangedEventHandler( OnBankStatementVerificationPropertyChanged ), "BankStatementVerification", VarioSL.Entities.RelationClasses.StaticSaleRelations.BankStatementVerificationEntityUsingSaleIDStatic, false, ref _alreadyFetchedBankStatementVerification, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnBankStatementVerificationPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _order</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrder(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _order, new PropertyChangedEventHandler( OnOrderPropertyChanged ), "Order", VarioSL.Entities.RelationClasses.StaticSaleRelations.OrderEntityUsingOrderIDStatic, true, signalRelatedEntity, "Sale", resetFKFields, new int[] { (int)SaleFieldIndex.OrderID } );
			_order = null;
		}
	
		/// <summary> setups the sync logic for member _order</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrder(IEntityCore relatedEntity)
		{
			if(_order!=relatedEntity)
			{
				DesetupSyncOrder(true, true);
				_order = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _order, new PropertyChangedEventHandler( OnOrderPropertyChanged ), "Order", VarioSL.Entities.RelationClasses.StaticSaleRelations.OrderEntityUsingOrderIDStatic, true, ref _alreadyFetchedOrder, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="saleID">PK value for Sale which data should be fetched into this Sale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 saleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SaleFieldIndex.SaleID].ForcedCurrentValueWrite(saleID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSaleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SaleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SaleRelations Relations
		{
			get	{ return new SaleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PaymentJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPaymentJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PaymentJournalCollection(), (IEntityRelation)GetRelationsForField("PaymentJournals")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.PaymentJournalEntity, 0, null, null, null, "PaymentJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRefunds
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Refunds")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Refunds", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceClass'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceClass
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceClassCollection(), (IEntityRelation)GetRelationsForField("DeviceClass")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.DeviceClassEntity, 0, null, null, null, "DeviceClass", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("Contract")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "Contract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Contract'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSecondaryContract
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ContractCollection(), (IEntityRelation)GetRelationsForField("SecondaryContract")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.ContractEntity, 0, null, null, null, "SecondaryContract", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CreditCardAuthorization'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCreditCardAuthorization
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.CreditCardAuthorizationCollection(), (IEntityRelation)GetRelationsForField("CreditCardAuthorization")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.CreditCardAuthorizationEntity, 0, null, null, null, "CreditCardAuthorization", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Person'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPerson
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PersonCollection(), (IEntityRelation)GetRelationsForField("Person")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.PersonEntity, 0, null, null, null, "Person", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRefundedSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("RefundedSale")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "RefundedSale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSale
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sale")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sale", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BankStatementVerification'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBankStatementVerification
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BankStatementVerificationCollection(), (IEntityRelation)GetRelationsForField("BankStatementVerification")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.BankStatementVerificationEntity, 0, null, null, null, "BankStatementVerification", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrder
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("Order")[0], (int)VarioSL.Entities.EntityType.SaleEntity, (int)VarioSL.Entities.EntityType.OrderEntity, 0, null, null, null, "Order", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne);	}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CancellationReferenceID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."CANCELLATIONREFERENCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationReferenceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.CancellationReferenceID, false); }
			set	{ SetValue((int)SaleFieldIndex.CancellationReferenceID, value, true); }
		}

		/// <summary> The ClientID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)SaleFieldIndex.ClientID, true); }
			set	{ SetValue((int)SaleFieldIndex.ClientID, value, true); }
		}

		/// <summary> The ContractID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."CONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.ContractID, false); }
			set	{ SetValue((int)SaleFieldIndex.ContractID, value, true); }
		}

		/// <summary> The CreditCardAuthorizationID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."CREDITCARDAUTHORIZATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreditCardAuthorizationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.CreditCardAuthorizationID, false); }
			set	{ SetValue((int)SaleFieldIndex.CreditCardAuthorizationID, value, true); }
		}

		/// <summary> The DeviceNumber property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."DEVICENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeviceNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)SaleFieldIndex.DeviceNumber, false); }
			set	{ SetValue((int)SaleFieldIndex.DeviceNumber, value, true); }
		}

		/// <summary> The ExternalOrderNumber property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."EXTERNALORDERNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalOrderNumber
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.ExternalOrderNumber, true); }
			set	{ SetValue((int)SaleFieldIndex.ExternalOrderNumber, value, true); }
		}

		/// <summary> The IsClosed property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."ISCLOSED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsClosed
		{
			get { return (Nullable<System.Boolean>)GetValue((int)SaleFieldIndex.IsClosed, false); }
			set	{ SetValue((int)SaleFieldIndex.IsClosed, value, true); }
		}

		/// <summary> The IsOrganizational property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."ISORGANIZATIONAL"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsOrganizational
		{
			get { return (System.Boolean)GetValue((int)SaleFieldIndex.IsOrganizational, true); }
			set	{ SetValue((int)SaleFieldIndex.IsOrganizational, value, true); }
		}

		/// <summary> The LastModified property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)SaleFieldIndex.LastModified, true); }
			set	{ SetValue((int)SaleFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.LastUser, true); }
			set	{ SetValue((int)SaleFieldIndex.LastUser, value, true); }
		}

		/// <summary> The LocationNumber property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."LOCATIONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> LocationNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)SaleFieldIndex.LocationNumber, false); }
			set	{ SetValue((int)SaleFieldIndex.LocationNumber, value, true); }
		}

		/// <summary> The MerchantName property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."MERCHANTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantName
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.MerchantName, true); }
			set	{ SetValue((int)SaleFieldIndex.MerchantName, value, true); }
		}

		/// <summary> The MerchantNumber property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."MERCHANTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MerchantNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)SaleFieldIndex.MerchantNumber, false); }
			set	{ SetValue((int)SaleFieldIndex.MerchantNumber, value, true); }
		}

		/// <summary> The MerchantRetailer property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."MERCHANTRETAILER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MerchantRetailer
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.MerchantRetailer, true); }
			set	{ SetValue((int)SaleFieldIndex.MerchantRetailer, value, true); }
		}

		/// <summary> The NetworkReference property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."NETWORKREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NetworkReference
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.NetworkReference, true); }
			set	{ SetValue((int)SaleFieldIndex.NetworkReference, value, true); }
		}

		/// <summary> The Notes property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."NOTES"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.Notes, true); }
			set	{ SetValue((int)SaleFieldIndex.Notes, value, true); }
		}

		/// <summary> The OrderID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."ORDERID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrderID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.OrderID, false); }
			set	{ SetValue((int)SaleFieldIndex.OrderID, value, true); }
		}

		/// <summary> The PersonID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."PERSONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PersonID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.PersonID, false); }
			set	{ SetValue((int)SaleFieldIndex.PersonID, value, true); }
		}

		/// <summary> The ReceiptReference property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."RECEIPTREFERENCE"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReceiptReference
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.ReceiptReference, true); }
			set	{ SetValue((int)SaleFieldIndex.ReceiptReference, value, true); }
		}

		/// <summary> The RefundReferenceID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."REFUNDREFERENCEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RefundReferenceID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.RefundReferenceID, false); }
			set	{ SetValue((int)SaleFieldIndex.RefundReferenceID, value, true); }
		}

		/// <summary> The SaleDate property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALEDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime SaleDate
		{
			get { return (System.DateTime)GetValue((int)SaleFieldIndex.SaleDate, true); }
			set	{ SetValue((int)SaleFieldIndex.SaleDate, value, true); }
		}

		/// <summary> The SaleID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 SaleID
		{
			get { return (System.Int64)GetValue((int)SaleFieldIndex.SaleID, true); }
			set	{ SetValue((int)SaleFieldIndex.SaleID, value, true); }
		}

		/// <summary> The SalesChannelID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALESCHANNELID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SalesChannelID
		{
			get { return (System.Int64)GetValue((int)SaleFieldIndex.SalesChannelID, true); }
			set	{ SetValue((int)SaleFieldIndex.SalesChannelID, value, true); }
		}

		/// <summary> The SalesPersonEmail property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALESPERSONEMAIL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SalesPersonEmail
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.SalesPersonEmail, true); }
			set	{ SetValue((int)SaleFieldIndex.SalesPersonEmail, value, true); }
		}

		/// <summary> The SalesPersonNumber property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALESPERSONNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SalesPersonNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)SaleFieldIndex.SalesPersonNumber, false); }
			set	{ SetValue((int)SaleFieldIndex.SalesPersonNumber, value, true); }
		}

		/// <summary> The SaleTransactionID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALETRANSACTIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String SaleTransactionID
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.SaleTransactionID, true); }
			set	{ SetValue((int)SaleFieldIndex.SaleTransactionID, value, true); }
		}

		/// <summary> The SaleType property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SALETYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.SaleType SaleType
		{
			get { return (VarioSL.Entities.Enumerations.SaleType)GetValue((int)SaleFieldIndex.SaleType, true); }
			set	{ SetValue((int)SaleFieldIndex.SaleType, value, true); }
		}

		/// <summary> The SecondaryContractID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SECONDARYCONTRACTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SecondaryContractID
		{
			get { return (Nullable<System.Int64>)GetValue((int)SaleFieldIndex.SecondaryContractID, false); }
			set	{ SetValue((int)SaleFieldIndex.SecondaryContractID, value, true); }
		}

		/// <summary> The ShiftInventoryID property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."SHIFTINVENTORYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 32<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShiftInventoryID
		{
			get { return (System.String)GetValue((int)SaleFieldIndex.ShiftInventoryID, true); }
			set	{ SetValue((int)SaleFieldIndex.ShiftInventoryID, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity Sale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SL_SALE"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)SaleFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)SaleFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'PaymentJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPaymentJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PaymentJournalCollection PaymentJournals
		{
			get	{ return GetMultiPaymentJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PaymentJournals. When set to true, PaymentJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PaymentJournals is accessed. You can always execute/ a forced fetch by calling GetMultiPaymentJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPaymentJournals
		{
			get	{ return _alwaysFetchPaymentJournals; }
			set	{ _alwaysFetchPaymentJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PaymentJournals already has been fetched. Setting this property to false when PaymentJournals has been fetched
		/// will clear the PaymentJournals collection well. Setting this property to true while PaymentJournals hasn't been fetched disables lazy loading for PaymentJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPaymentJournals
		{
			get { return _alreadyFetchedPaymentJournals;}
			set 
			{
				if(_alreadyFetchedPaymentJournals && !value && (_paymentJournals != null))
				{
					_paymentJournals.Clear();
				}
				_alreadyFetchedPaymentJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRefunds()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Refunds
		{
			get	{ return GetMultiRefunds(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Refunds. When set to true, Refunds is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Refunds is accessed. You can always execute/ a forced fetch by calling GetMultiRefunds(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRefunds
		{
			get	{ return _alwaysFetchRefunds; }
			set	{ _alwaysFetchRefunds = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Refunds already has been fetched. Setting this property to false when Refunds has been fetched
		/// will clear the Refunds collection well. Setting this property to true while Refunds hasn't been fetched disables lazy loading for Refunds</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRefunds
		{
			get { return _alreadyFetchedRefunds;}
			set 
			{
				if(_alreadyFetchedRefunds && !value && (_refunds != null))
				{
					_refunds.Clear();
				}
				_alreadyFetchedRefunds = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'DeviceClassEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDeviceClass()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual DeviceClassEntity DeviceClass
		{
			get	{ return GetSingleDeviceClass(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDeviceClass(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "DeviceClass", _deviceClass, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceClass. When set to true, DeviceClass is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceClass is accessed. You can always execute a forced fetch by calling GetSingleDeviceClass(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceClass
		{
			get	{ return _alwaysFetchDeviceClass; }
			set	{ _alwaysFetchDeviceClass = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceClass already has been fetched. Setting this property to false when DeviceClass has been fetched
		/// will set DeviceClass to null as well. Setting this property to true while DeviceClass hasn't been fetched disables lazy loading for DeviceClass</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceClass
		{
			get { return _alreadyFetchedDeviceClass;}
			set 
			{
				if(_alreadyFetchedDeviceClass && !value)
				{
					this.DeviceClass = null;
				}
				_alreadyFetchedDeviceClass = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DeviceClass is not found
		/// in the database. When set to true, DeviceClass will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DeviceClassReturnsNewIfNotFound
		{
			get	{ return _deviceClassReturnsNewIfNotFound; }
			set { _deviceClassReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity Contract
		{
			get	{ return GetSingleContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "Contract", _contract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Contract. When set to true, Contract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Contract is accessed. You can always execute a forced fetch by calling GetSingleContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchContract
		{
			get	{ return _alwaysFetchContract; }
			set	{ _alwaysFetchContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Contract already has been fetched. Setting this property to false when Contract has been fetched
		/// will set Contract to null as well. Setting this property to true while Contract hasn't been fetched disables lazy loading for Contract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedContract
		{
			get { return _alreadyFetchedContract;}
			set 
			{
				if(_alreadyFetchedContract && !value)
				{
					this.Contract = null;
				}
				_alreadyFetchedContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Contract is not found
		/// in the database. When set to true, Contract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ContractReturnsNewIfNotFound
		{
			get	{ return _contractReturnsNewIfNotFound; }
			set { _contractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ContractEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSecondaryContract()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ContractEntity SecondaryContract
		{
			get	{ return GetSingleSecondaryContract(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSecondaryContract(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SecondarySales", "SecondaryContract", _secondaryContract, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SecondaryContract. When set to true, SecondaryContract is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SecondaryContract is accessed. You can always execute a forced fetch by calling GetSingleSecondaryContract(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSecondaryContract
		{
			get	{ return _alwaysFetchSecondaryContract; }
			set	{ _alwaysFetchSecondaryContract = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SecondaryContract already has been fetched. Setting this property to false when SecondaryContract has been fetched
		/// will set SecondaryContract to null as well. Setting this property to true while SecondaryContract hasn't been fetched disables lazy loading for SecondaryContract</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSecondaryContract
		{
			get { return _alreadyFetchedSecondaryContract;}
			set 
			{
				if(_alreadyFetchedSecondaryContract && !value)
				{
					this.SecondaryContract = null;
				}
				_alreadyFetchedSecondaryContract = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SecondaryContract is not found
		/// in the database. When set to true, SecondaryContract will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SecondaryContractReturnsNewIfNotFound
		{
			get	{ return _secondaryContractReturnsNewIfNotFound; }
			set { _secondaryContractReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CreditCardAuthorizationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCreditCardAuthorization()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual CreditCardAuthorizationEntity CreditCardAuthorization
		{
			get	{ return GetSingleCreditCardAuthorization(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCreditCardAuthorization(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "CreditCardAuthorization", _creditCardAuthorization, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CreditCardAuthorization. When set to true, CreditCardAuthorization is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CreditCardAuthorization is accessed. You can always execute a forced fetch by calling GetSingleCreditCardAuthorization(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCreditCardAuthorization
		{
			get	{ return _alwaysFetchCreditCardAuthorization; }
			set	{ _alwaysFetchCreditCardAuthorization = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CreditCardAuthorization already has been fetched. Setting this property to false when CreditCardAuthorization has been fetched
		/// will set CreditCardAuthorization to null as well. Setting this property to true while CreditCardAuthorization hasn't been fetched disables lazy loading for CreditCardAuthorization</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCreditCardAuthorization
		{
			get { return _alreadyFetchedCreditCardAuthorization;}
			set 
			{
				if(_alreadyFetchedCreditCardAuthorization && !value)
				{
					this.CreditCardAuthorization = null;
				}
				_alreadyFetchedCreditCardAuthorization = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CreditCardAuthorization is not found
		/// in the database. When set to true, CreditCardAuthorization will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool CreditCardAuthorizationReturnsNewIfNotFound
		{
			get	{ return _creditCardAuthorizationReturnsNewIfNotFound; }
			set { _creditCardAuthorizationReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PersonEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePerson()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual PersonEntity Person
		{
			get	{ return GetSinglePerson(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPerson(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "Person", _person, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Person. When set to true, Person is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Person is accessed. You can always execute a forced fetch by calling GetSinglePerson(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPerson
		{
			get	{ return _alwaysFetchPerson; }
			set	{ _alwaysFetchPerson = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Person already has been fetched. Setting this property to false when Person has been fetched
		/// will set Person to null as well. Setting this property to true while Person hasn't been fetched disables lazy loading for Person</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPerson
		{
			get { return _alreadyFetchedPerson;}
			set 
			{
				if(_alreadyFetchedPerson && !value)
				{
					this.Person = null;
				}
				_alreadyFetchedPerson = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Person is not found
		/// in the database. When set to true, Person will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PersonReturnsNewIfNotFound
		{
			get	{ return _personReturnsNewIfNotFound; }
			set { _personReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRefundedSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity RefundedSale
		{
			get	{ return GetSingleRefundedSale(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRefundedSale(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Refunds", "RefundedSale", _refundedSale, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RefundedSale. When set to true, RefundedSale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RefundedSale is accessed. You can always execute a forced fetch by calling GetSingleRefundedSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRefundedSale
		{
			get	{ return _alwaysFetchRefundedSale; }
			set	{ _alwaysFetchRefundedSale = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RefundedSale already has been fetched. Setting this property to false when RefundedSale has been fetched
		/// will set RefundedSale to null as well. Setting this property to true while RefundedSale hasn't been fetched disables lazy loading for RefundedSale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRefundedSale
		{
			get { return _alreadyFetchedRefundedSale;}
			set 
			{
				if(_alreadyFetchedRefundedSale && !value)
				{
					this.RefundedSale = null;
				}
				_alreadyFetchedRefundedSale = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RefundedSale is not found
		/// in the database. When set to true, RefundedSale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RefundedSaleReturnsNewIfNotFound
		{
			get	{ return _refundedSaleReturnsNewIfNotFound; }
			set { _refundedSaleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SaleEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSale()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual SaleEntity Sale
		{
			get	{ return GetSingleSale(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSale(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Sales", "Sale", _sale, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Sale. When set to true, Sale is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sale is accessed. You can always execute a forced fetch by calling GetSingleSale(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSale
		{
			get	{ return _alwaysFetchSale; }
			set	{ _alwaysFetchSale = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sale already has been fetched. Setting this property to false when Sale has been fetched
		/// will set Sale to null as well. Setting this property to true while Sale hasn't been fetched disables lazy loading for Sale</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSale
		{
			get { return _alreadyFetchedSale;}
			set 
			{
				if(_alreadyFetchedSale && !value)
				{
					this.Sale = null;
				}
				_alreadyFetchedSale = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Sale is not found
		/// in the database. When set to true, Sale will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool SaleReturnsNewIfNotFound
		{
			get	{ return _saleReturnsNewIfNotFound; }
			set { _saleReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'BankStatementVerificationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleBankStatementVerification()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual BankStatementVerificationEntity BankStatementVerification
		{
			get	{ return GetSingleBankStatementVerification(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncBankStatementVerification(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_bankStatementVerification !=null);
						DesetupSyncBankStatementVerification(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("BankStatementVerification");
						}
					}
					else
					{
						if(_bankStatementVerification!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Sale");
							SetupSyncBankStatementVerification(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for BankStatementVerification. When set to true, BankStatementVerification is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BankStatementVerification is accessed. You can always execute a forced fetch by calling GetSingleBankStatementVerification(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBankStatementVerification
		{
			get	{ return _alwaysFetchBankStatementVerification; }
			set	{ _alwaysFetchBankStatementVerification = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property BankStatementVerification already has been fetched. Setting this property to false when BankStatementVerification has been fetched
		/// will set BankStatementVerification to null as well. Setting this property to true while BankStatementVerification hasn't been fetched disables lazy loading for BankStatementVerification</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBankStatementVerification
		{
			get { return _alreadyFetchedBankStatementVerification;}
			set 
			{
				if(_alreadyFetchedBankStatementVerification && !value)
				{
					this.BankStatementVerification = null;
				}
				_alreadyFetchedBankStatementVerification = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property BankStatementVerification is not found
		/// in the database. When set to true, BankStatementVerification will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool BankStatementVerificationReturnsNewIfNotFound
		{
			get	{ return _bankStatementVerificationReturnsNewIfNotFound; }
			set	{ _bankStatementVerificationReturnsNewIfNotFound = value; }	
		}
		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/></summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual OrderEntity Order
		{
			get	{ return GetSingleOrder(false); }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncOrder(value);
				}
				else
				{
					if(value==null)
					{
						bool raisePropertyChanged = (_order !=null);
						DesetupSyncOrder(true, true);
						if(raisePropertyChanged)
						{
							OnPropertyChanged("Order");
						}
					}
					else
					{
						if(_order!=value)
						{
							((IEntity)value).SetRelatedEntity(this, "Sale");
							SetupSyncOrder(value);
						}
					}
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Order. When set to true, Order is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Order is accessed. You can always execute a forced fetch by calling GetSingleOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrder
		{
			get	{ return _alwaysFetchOrder; }
			set	{ _alwaysFetchOrder = value; }	
		}
		
		/// <summary>Gets / Sets the lazy loading flag if the property Order already has been fetched. Setting this property to false when Order has been fetched
		/// will set Order to null as well. Setting this property to true while Order hasn't been fetched disables lazy loading for Order</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrder
		{
			get { return _alreadyFetchedOrder;}
			set 
			{
				if(_alreadyFetchedOrder && !value)
				{
					this.Order = null;
				}
				_alreadyFetchedOrder = value;
			}
		}
		
		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Order is not found
		/// in the database. When set to true, Order will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool OrderReturnsNewIfNotFound
		{
			get	{ return _orderReturnsNewIfNotFound; }
			set	{ _orderReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.SaleEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
