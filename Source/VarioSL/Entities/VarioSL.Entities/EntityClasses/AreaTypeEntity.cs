﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AreaType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AreaTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.AreaListCollection	_areaListType;
		private bool	_alwaysFetchAreaListType, _alreadyFetchedAreaListType;
		private VarioSL.Entities.CollectionClasses.AreaListElementCollection	_areaListElement;
		private bool	_alwaysFetchAreaListElement, _alreadyFetchedAreaListElement;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AreaListType</summary>
			public static readonly string AreaListType = "AreaListType";
			/// <summary>Member name AreaListElement</summary>
			public static readonly string AreaListElement = "AreaListElement";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AreaTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AreaTypeEntity() :base("AreaTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		public AreaTypeEntity(System.Int64 typeID):base("AreaTypeEntity")
		{
			InitClassFetch(typeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AreaTypeEntity(System.Int64 typeID, IPrefetchPath prefetchPathToUse):base("AreaTypeEntity")
		{
			InitClassFetch(typeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="validator">The custom validator object for this AreaTypeEntity</param>
		public AreaTypeEntity(System.Int64 typeID, IValidator validator):base("AreaTypeEntity")
		{
			InitClassFetch(typeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AreaTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_areaListType = (VarioSL.Entities.CollectionClasses.AreaListCollection)info.GetValue("_areaListType", typeof(VarioSL.Entities.CollectionClasses.AreaListCollection));
			_alwaysFetchAreaListType = info.GetBoolean("_alwaysFetchAreaListType");
			_alreadyFetchedAreaListType = info.GetBoolean("_alreadyFetchedAreaListType");

			_areaListElement = (VarioSL.Entities.CollectionClasses.AreaListElementCollection)info.GetValue("_areaListElement", typeof(VarioSL.Entities.CollectionClasses.AreaListElementCollection));
			_alwaysFetchAreaListElement = info.GetBoolean("_alwaysFetchAreaListElement");
			_alreadyFetchedAreaListElement = info.GetBoolean("_alreadyFetchedAreaListElement");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAreaListType = (_areaListType.Count > 0);
			_alreadyFetchedAreaListElement = (_areaListElement.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AreaListType":
					toReturn.Add(Relations.AreaListEntityUsingAreaListTypeID);
					break;
				case "AreaListElement":
					toReturn.Add(Relations.AreaListElementEntityUsingAreaInstanceTypeID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_areaListType", (!this.MarkedForDeletion?_areaListType:null));
			info.AddValue("_alwaysFetchAreaListType", _alwaysFetchAreaListType);
			info.AddValue("_alreadyFetchedAreaListType", _alreadyFetchedAreaListType);
			info.AddValue("_areaListElement", (!this.MarkedForDeletion?_areaListElement:null));
			info.AddValue("_alwaysFetchAreaListElement", _alwaysFetchAreaListElement);
			info.AddValue("_alreadyFetchedAreaListElement", _alreadyFetchedAreaListElement);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AreaListType":
					_alreadyFetchedAreaListType = true;
					if(entity!=null)
					{
						this.AreaListType.Add((AreaListEntity)entity);
					}
					break;
				case "AreaListElement":
					_alreadyFetchedAreaListElement = true;
					if(entity!=null)
					{
						this.AreaListElement.Add((AreaListElementEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AreaListType":
					_areaListType.Add((AreaListEntity)relatedEntity);
					break;
				case "AreaListElement":
					_areaListElement.Add((AreaListElementEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AreaListType":
					this.PerformRelatedEntityRemoval(_areaListType, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "AreaListElement":
					this.PerformRelatedEntityRemoval(_areaListElement, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_areaListType);
			toReturn.Add(_areaListElement);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID)
		{
			return FetchUsingPK(typeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(typeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(typeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AreaTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaListType(bool forceFetch)
		{
			return GetMultiAreaListType(forceFetch, _areaListType.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaListType(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaListType(forceFetch, _areaListType.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaListType(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaListType(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection GetMultiAreaListType(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaListType || forceFetch || _alwaysFetchAreaListType) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaListType);
				_areaListType.SuppressClearInGetMulti=!forceFetch;
				_areaListType.EntityFactoryToUse = entityFactoryToUse;
				_areaListType.GetMultiManyToOne(null, this, null, null, filter);
				_areaListType.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaListType = true;
			}
			return _areaListType;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaListType'. These settings will be taken into account
		/// when the property AreaListType is requested or GetMultiAreaListType is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaListType(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaListType.SortClauses=sortClauses;
			_areaListType.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch)
		{
			return GetMultiAreaListElement(forceFetch, _areaListElement.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AreaListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAreaListElement(forceFetch, _areaListElement.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAreaListElement(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection GetMultiAreaListElement(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAreaListElement || forceFetch || _alwaysFetchAreaListElement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_areaListElement);
				_areaListElement.SuppressClearInGetMulti=!forceFetch;
				_areaListElement.EntityFactoryToUse = entityFactoryToUse;
				_areaListElement.GetMultiManyToOne(null, this, null, null, filter);
				_areaListElement.SuppressClearInGetMulti=false;
				_alreadyFetchedAreaListElement = true;
			}
			return _areaListElement;
		}

		/// <summary> Sets the collection parameters for the collection for 'AreaListElement'. These settings will be taken into account
		/// when the property AreaListElement is requested or GetMultiAreaListElement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAreaListElement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_areaListElement.SortClauses=sortClauses;
			_areaListElement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AreaListType", _areaListType);
			toReturn.Add("AreaListElement", _areaListElement);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="validator">The validator object for this AreaTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 typeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(typeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_areaListType = new VarioSL.Entities.CollectionClasses.AreaListCollection();
			_areaListType.SetContainingEntityInfo(this, "AreaListType");

			_areaListElement = new VarioSL.Entities.CollectionClasses.AreaListElementCollection();
			_areaListElement.SetContainingEntityInfo(this, "AreaTypeElement");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Number", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="typeID">PK value for AreaType which data should be fetched into this AreaType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 typeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AreaTypeFieldIndex.TypeID].ForcedCurrentValueWrite(typeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAreaTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AreaTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AreaTypeRelations Relations
		{
			get	{ return new AreaTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaList' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListType
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListCollection(), (IEntityRelation)GetRelationsForField("AreaListType")[0], (int)VarioSL.Entities.EntityType.AreaTypeEntity, (int)VarioSL.Entities.EntityType.AreaListEntity, 0, null, null, null, "AreaListType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AreaListElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAreaListElement
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AreaListElementCollection(), (IEntityRelation)GetRelationsForField("AreaListElement")[0], (int)VarioSL.Entities.EntityType.AreaTypeEntity, (int)VarioSL.Entities.EntityType.AreaListElementEntity, 0, null, null, null, "AreaListElement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity AreaType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREATYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 400<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)AreaTypeFieldIndex.Description, true); }
			set	{ SetValue((int)AreaTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The Name property of the Entity AreaType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREATYPE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AreaTypeFieldIndex.Name, true); }
			set	{ SetValue((int)AreaTypeFieldIndex.Name, value, true); }
		}

		/// <summary> The Number property of the Entity AreaType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREATYPE"."NUMBER_"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffAreaListType Number
		{
			get { return (VarioSL.Entities.Enumerations.TariffAreaListType)GetValue((int)AreaTypeFieldIndex.Number, true); }
			set	{ SetValue((int)AreaTypeFieldIndex.Number, value, true); }
		}

		/// <summary> The TypeGroupID property of the Entity AreaType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREATYPE"."TYPEGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffAreaTypeGroup TypeGroupID
		{
			get { return (VarioSL.Entities.Enumerations.TariffAreaTypeGroup)GetValue((int)AreaTypeFieldIndex.TypeGroupID, true); }
			set	{ SetValue((int)AreaTypeFieldIndex.TypeGroupID, value, true); }
		}

		/// <summary> The TypeID property of the Entity AreaType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_AREATYPE"."TYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TypeID
		{
			get { return (System.Int64)GetValue((int)AreaTypeFieldIndex.TypeID, true); }
			set	{ SetValue((int)AreaTypeFieldIndex.TypeID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AreaListEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaListType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListCollection AreaListType
		{
			get	{ return GetMultiAreaListType(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListType. When set to true, AreaListType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListType is accessed. You can always execute/ a forced fetch by calling GetMultiAreaListType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListType
		{
			get	{ return _alwaysFetchAreaListType; }
			set	{ _alwaysFetchAreaListType = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListType already has been fetched. Setting this property to false when AreaListType has been fetched
		/// will clear the AreaListType collection well. Setting this property to true while AreaListType hasn't been fetched disables lazy loading for AreaListType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListType
		{
			get { return _alreadyFetchedAreaListType;}
			set 
			{
				if(_alreadyFetchedAreaListType && !value && (_areaListType != null))
				{
					_areaListType.Clear();
				}
				_alreadyFetchedAreaListType = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'AreaListElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAreaListElement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.AreaListElementCollection AreaListElement
		{
			get	{ return GetMultiAreaListElement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AreaListElement. When set to true, AreaListElement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AreaListElement is accessed. You can always execute/ a forced fetch by calling GetMultiAreaListElement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAreaListElement
		{
			get	{ return _alwaysFetchAreaListElement; }
			set	{ _alwaysFetchAreaListElement = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AreaListElement already has been fetched. Setting this property to false when AreaListElement has been fetched
		/// will clear the AreaListElement collection well. Setting this property to true while AreaListElement hasn't been fetched disables lazy loading for AreaListElement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAreaListElement
		{
			get { return _alreadyFetchedAreaListElement;}
			set 
			{
				if(_alreadyFetchedAreaListElement && !value && (_areaListElement != null))
				{
					_areaListElement.Clear();
				}
				_alreadyFetchedAreaListElement = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AreaTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
