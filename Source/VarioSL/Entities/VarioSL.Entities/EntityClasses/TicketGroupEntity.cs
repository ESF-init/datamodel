﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TicketGroup'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class TicketGroupEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection	_fareMatrixEntry;
		private bool	_alwaysFetchFareMatrixEntry, _alreadyFetchedFareMatrixEntry;
		private VarioSL.Entities.CollectionClasses.TicketToGroupCollection	_ticketToGroups;
		private bool	_alwaysFetchTicketToGroups, _alreadyFetchedTicketToGroups;
		private VarioSL.Entities.CollectionClasses.TicketCollection _ticketCollectionViaTicketToGroup;
		private bool	_alwaysFetchTicketCollectionViaTicketToGroup, _alreadyFetchedTicketCollectionViaTicketToGroup;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name FareMatrixEntry</summary>
			public static readonly string FareMatrixEntry = "FareMatrixEntry";
			/// <summary>Member name TicketToGroups</summary>
			public static readonly string TicketToGroups = "TicketToGroups";
			/// <summary>Member name TicketCollectionViaTicketToGroup</summary>
			public static readonly string TicketCollectionViaTicketToGroup = "TicketCollectionViaTicketToGroup";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static TicketGroupEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TicketGroupEntity() :base("TicketGroupEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		public TicketGroupEntity(System.Int64 ticketGroupID):base("TicketGroupEntity")
		{
			InitClassFetch(ticketGroupID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TicketGroupEntity(System.Int64 ticketGroupID, IPrefetchPath prefetchPathToUse):base("TicketGroupEntity")
		{
			InitClassFetch(ticketGroupID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="validator">The custom validator object for this TicketGroupEntity</param>
		public TicketGroupEntity(System.Int64 ticketGroupID, IValidator validator):base("TicketGroupEntity")
		{
			InitClassFetch(ticketGroupID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketGroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareMatrixEntry = (VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection)info.GetValue("_fareMatrixEntry", typeof(VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection));
			_alwaysFetchFareMatrixEntry = info.GetBoolean("_alwaysFetchFareMatrixEntry");
			_alreadyFetchedFareMatrixEntry = info.GetBoolean("_alreadyFetchedFareMatrixEntry");

			_ticketToGroups = (VarioSL.Entities.CollectionClasses.TicketToGroupCollection)info.GetValue("_ticketToGroups", typeof(VarioSL.Entities.CollectionClasses.TicketToGroupCollection));
			_alwaysFetchTicketToGroups = info.GetBoolean("_alwaysFetchTicketToGroups");
			_alreadyFetchedTicketToGroups = info.GetBoolean("_alreadyFetchedTicketToGroups");
			_ticketCollectionViaTicketToGroup = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketCollectionViaTicketToGroup", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketCollectionViaTicketToGroup = info.GetBoolean("_alwaysFetchTicketCollectionViaTicketToGroup");
			_alreadyFetchedTicketCollectionViaTicketToGroup = info.GetBoolean("_alreadyFetchedTicketCollectionViaTicketToGroup");
			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((TicketGroupFieldIndex)fieldIndex)
			{
				case TicketGroupFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareMatrixEntry = (_fareMatrixEntry.Count > 0);
			_alreadyFetchedTicketToGroups = (_ticketToGroups.Count > 0);
			_alreadyFetchedTicketCollectionViaTicketToGroup = (_ticketCollectionViaTicketToGroup.Count > 0);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "FareMatrixEntry":
					toReturn.Add(Relations.FareMatrixEntryEntityUsingTicketGroupID);
					break;
				case "TicketToGroups":
					toReturn.Add(Relations.TicketToGroupEntityUsingTicketGroupID);
					break;
				case "TicketCollectionViaTicketToGroup":
					toReturn.Add(Relations.TicketToGroupEntityUsingTicketGroupID, "TicketGroupEntity__", "TicketToGroup_", JoinHint.None);
					toReturn.Add(TicketToGroupEntity.Relations.TicketEntityUsingTicketID, "TicketToGroup_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareMatrixEntry", (!this.MarkedForDeletion?_fareMatrixEntry:null));
			info.AddValue("_alwaysFetchFareMatrixEntry", _alwaysFetchFareMatrixEntry);
			info.AddValue("_alreadyFetchedFareMatrixEntry", _alreadyFetchedFareMatrixEntry);
			info.AddValue("_ticketToGroups", (!this.MarkedForDeletion?_ticketToGroups:null));
			info.AddValue("_alwaysFetchTicketToGroups", _alwaysFetchTicketToGroups);
			info.AddValue("_alreadyFetchedTicketToGroups", _alreadyFetchedTicketToGroups);
			info.AddValue("_ticketCollectionViaTicketToGroup", (!this.MarkedForDeletion?_ticketCollectionViaTicketToGroup:null));
			info.AddValue("_alwaysFetchTicketCollectionViaTicketToGroup", _alwaysFetchTicketCollectionViaTicketToGroup);
			info.AddValue("_alreadyFetchedTicketCollectionViaTicketToGroup", _alreadyFetchedTicketCollectionViaTicketToGroup);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "FareMatrixEntry":
					_alreadyFetchedFareMatrixEntry = true;
					if(entity!=null)
					{
						this.FareMatrixEntry.Add((FareMatrixEntryEntity)entity);
					}
					break;
				case "TicketToGroups":
					_alreadyFetchedTicketToGroups = true;
					if(entity!=null)
					{
						this.TicketToGroups.Add((TicketToGroupEntity)entity);
					}
					break;
				case "TicketCollectionViaTicketToGroup":
					_alreadyFetchedTicketCollectionViaTicketToGroup = true;
					if(entity!=null)
					{
						this.TicketCollectionViaTicketToGroup.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "FareMatrixEntry":
					_fareMatrixEntry.Add((FareMatrixEntryEntity)relatedEntity);
					break;
				case "TicketToGroups":
					_ticketToGroups.Add((TicketToGroupEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "FareMatrixEntry":
					this.PerformRelatedEntityRemoval(_fareMatrixEntry, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketToGroups":
					this.PerformRelatedEntityRemoval(_ticketToGroups, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareMatrixEntry);
			toReturn.Add(_ticketToGroups);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketGroupID)
		{
			return FetchUsingPK(ticketGroupID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketGroupID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(ticketGroupID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(ticketGroupID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 ticketGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(ticketGroupID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.TicketGroupID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new TicketGroupRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch)
		{
			return GetMultiFareMatrixEntry(forceFetch, _fareMatrixEntry.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareMatrixEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareMatrixEntry(forceFetch, _fareMatrixEntry.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareMatrixEntry(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection GetMultiFareMatrixEntry(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareMatrixEntry || forceFetch || _alwaysFetchFareMatrixEntry) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareMatrixEntry);
				_fareMatrixEntry.SuppressClearInGetMulti=!forceFetch;
				_fareMatrixEntry.EntityFactoryToUse = entityFactoryToUse;
				_fareMatrixEntry.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_fareMatrixEntry.SuppressClearInGetMulti=false;
				_alreadyFetchedFareMatrixEntry = true;
			}
			return _fareMatrixEntry;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareMatrixEntry'. These settings will be taken into account
		/// when the property FareMatrixEntry is requested or GetMultiFareMatrixEntry is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareMatrixEntry(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareMatrixEntry.SortClauses=sortClauses;
			_fareMatrixEntry.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketToGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch)
		{
			return GetMultiTicketToGroups(forceFetch, _ticketToGroups.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketToGroupEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketToGroups(forceFetch, _ticketToGroups.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketToGroups(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketToGroupCollection GetMultiTicketToGroups(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketToGroups || forceFetch || _alwaysFetchTicketToGroups) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketToGroups);
				_ticketToGroups.SuppressClearInGetMulti=!forceFetch;
				_ticketToGroups.EntityFactoryToUse = entityFactoryToUse;
				_ticketToGroups.GetMultiManyToOne(null, this, filter);
				_ticketToGroups.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketToGroups = true;
			}
			return _ticketToGroups;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketToGroups'. These settings will be taken into account
		/// when the property TicketToGroups is requested or GetMultiTicketToGroups is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketToGroups(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketToGroups.SortClauses=sortClauses;
			_ticketToGroups.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketToGroup(bool forceFetch)
		{
			return GetMultiTicketCollectionViaTicketToGroup(forceFetch, _ticketCollectionViaTicketToGroup.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketCollectionViaTicketToGroup(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTicketCollectionViaTicketToGroup || forceFetch || _alwaysFetchTicketCollectionViaTicketToGroup) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketCollectionViaTicketToGroup);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(TicketGroupFields.TicketGroupID, ComparisonOperator.Equal, this.TicketGroupID, "TicketGroupEntity__"));
				_ticketCollectionViaTicketToGroup.SuppressClearInGetMulti=!forceFetch;
				_ticketCollectionViaTicketToGroup.EntityFactoryToUse = entityFactoryToUse;
				_ticketCollectionViaTicketToGroup.GetMulti(filter, GetRelationsForField("TicketCollectionViaTicketToGroup"));
				_ticketCollectionViaTicketToGroup.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketCollectionViaTicketToGroup = true;
			}
			return _ticketCollectionViaTicketToGroup;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketCollectionViaTicketToGroup'. These settings will be taken into account
		/// when the property TicketCollectionViaTicketToGroup is requested or GetMultiTicketCollectionViaTicketToGroup is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketCollectionViaTicketToGroup(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketCollectionViaTicketToGroup.SortClauses=sortClauses;
			_ticketCollectionViaTicketToGroup.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("FareMatrixEntry", _fareMatrixEntry);
			toReturn.Add("TicketToGroups", _ticketToGroups);
			toReturn.Add("TicketCollectionViaTicketToGroup", _ticketCollectionViaTicketToGroup);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="validator">The validator object for this TicketGroupEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 ticketGroupID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(ticketGroupID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareMatrixEntry = new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection();
			_fareMatrixEntry.SetContainingEntityInfo(this, "TicketGroup");

			_ticketToGroups = new VarioSL.Entities.CollectionClasses.TicketToGroupCollection();
			_ticketToGroups.SetContainingEntityInfo(this, "TicketGroup");
			_ticketCollectionViaTicketToGroup = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketGroupID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticTicketGroupRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "TicketGroups", resetFKFields, new int[] { (int)TicketGroupFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticTicketGroupRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="ticketGroupID">PK value for TicketGroup which data should be fetched into this TicketGroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 ticketGroupID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)TicketGroupFieldIndex.TicketGroupID].ForcedCurrentValueWrite(ticketGroupID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketGroupDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new TicketGroupEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static TicketGroupRelations Relations
		{
			get	{ return new TicketGroupRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareMatrixEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareMatrixEntry
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection(), (IEntityRelation)GetRelationsForField("FareMatrixEntry")[0], (int)VarioSL.Entities.EntityType.TicketGroupEntity, (int)VarioSL.Entities.EntityType.FareMatrixEntryEntity, 0, null, null, null, "FareMatrixEntry", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketToGroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketToGroups
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketToGroupCollection(), (IEntityRelation)GetRelationsForField("TicketToGroups")[0], (int)VarioSL.Entities.EntityType.TicketGroupEntity, (int)VarioSL.Entities.EntityType.TicketToGroupEntity, 0, null, null, null, "TicketToGroups", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketCollectionViaTicketToGroup
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TicketToGroupEntityUsingTicketGroupID;
				intermediateRelation.SetAliases(string.Empty, "TicketToGroup_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.TicketGroupEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, GetRelationsForField("TicketCollectionViaTicketToGroup"), "TicketCollectionViaTicketToGroup", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.TicketGroupEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity TicketGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETGROUP"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)TicketGroupFieldIndex.Description, true); }
			set	{ SetValue((int)TicketGroupFieldIndex.Description, value, true); }
		}

		/// <summary> The ExternalNumber property of the Entity TicketGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETGROUP"."EXTERNALNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ExternalNumber
		{
			get { return (Nullable<System.Int64>)GetValue((int)TicketGroupFieldIndex.ExternalNumber, false); }
			set	{ SetValue((int)TicketGroupFieldIndex.ExternalNumber, value, true); }
		}

		/// <summary> The TariffID property of the Entity TicketGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETGROUP"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)TicketGroupFieldIndex.TariffID, true); }
			set	{ SetValue((int)TicketGroupFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TicketGroupID property of the Entity TicketGroup<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_TICKETGROUP"."TICKETGROUPID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 TicketGroupID
		{
			get { return (System.Int64)GetValue((int)TicketGroupFieldIndex.TicketGroupID, true); }
			set	{ SetValue((int)TicketGroupFieldIndex.TicketGroupID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareMatrixEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareMatrixEntry()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareMatrixEntryCollection FareMatrixEntry
		{
			get	{ return GetMultiFareMatrixEntry(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareMatrixEntry. When set to true, FareMatrixEntry is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareMatrixEntry is accessed. You can always execute/ a forced fetch by calling GetMultiFareMatrixEntry(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareMatrixEntry
		{
			get	{ return _alwaysFetchFareMatrixEntry; }
			set	{ _alwaysFetchFareMatrixEntry = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareMatrixEntry already has been fetched. Setting this property to false when FareMatrixEntry has been fetched
		/// will clear the FareMatrixEntry collection well. Setting this property to true while FareMatrixEntry hasn't been fetched disables lazy loading for FareMatrixEntry</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareMatrixEntry
		{
			get { return _alreadyFetchedFareMatrixEntry;}
			set 
			{
				if(_alreadyFetchedFareMatrixEntry && !value && (_fareMatrixEntry != null))
				{
					_fareMatrixEntry.Clear();
				}
				_alreadyFetchedFareMatrixEntry = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketToGroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketToGroups()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketToGroupCollection TicketToGroups
		{
			get	{ return GetMultiTicketToGroups(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketToGroups. When set to true, TicketToGroups is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketToGroups is accessed. You can always execute/ a forced fetch by calling GetMultiTicketToGroups(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketToGroups
		{
			get	{ return _alwaysFetchTicketToGroups; }
			set	{ _alwaysFetchTicketToGroups = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketToGroups already has been fetched. Setting this property to false when TicketToGroups has been fetched
		/// will clear the TicketToGroups collection well. Setting this property to true while TicketToGroups hasn't been fetched disables lazy loading for TicketToGroups</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketToGroups
		{
			get { return _alreadyFetchedTicketToGroups;}
			set 
			{
				if(_alreadyFetchedTicketToGroups && !value && (_ticketToGroups != null))
				{
					_ticketToGroups.Clear();
				}
				_alreadyFetchedTicketToGroups = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketCollectionViaTicketToGroup()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketCollectionViaTicketToGroup
		{
			get { return GetMultiTicketCollectionViaTicketToGroup(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketCollectionViaTicketToGroup. When set to true, TicketCollectionViaTicketToGroup is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketCollectionViaTicketToGroup is accessed. You can always execute a forced fetch by calling GetMultiTicketCollectionViaTicketToGroup(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketCollectionViaTicketToGroup
		{
			get	{ return _alwaysFetchTicketCollectionViaTicketToGroup; }
			set	{ _alwaysFetchTicketCollectionViaTicketToGroup = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketCollectionViaTicketToGroup already has been fetched. Setting this property to false when TicketCollectionViaTicketToGroup has been fetched
		/// will clear the TicketCollectionViaTicketToGroup collection well. Setting this property to true while TicketCollectionViaTicketToGroup hasn't been fetched disables lazy loading for TicketCollectionViaTicketToGroup</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketCollectionViaTicketToGroup
		{
			get { return _alreadyFetchedTicketCollectionViaTicketToGroup;}
			set 
			{
				if(_alreadyFetchedTicketCollectionViaTicketToGroup && !value && (_ticketCollectionViaTicketToGroup != null))
				{
					_ticketCollectionViaTicketToGroup.Clear();
				}
				_alreadyFetchedTicketCollectionViaTicketToGroup = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "TicketGroups", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.TicketGroupEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
