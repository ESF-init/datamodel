﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PointOfSale'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class PointOfSaleEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ProductCollection	_products;
		private bool	_alwaysFetchProducts, _alreadyFetchedProducts;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Products</summary>
			public static readonly string Products = "Products";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static PointOfSaleEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PointOfSaleEntity() :base("PointOfSaleEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		public PointOfSaleEntity(System.Int64 pointOfSaleID):base("PointOfSaleEntity")
		{
			InitClassFetch(pointOfSaleID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PointOfSaleEntity(System.Int64 pointOfSaleID, IPrefetchPath prefetchPathToUse):base("PointOfSaleEntity")
		{
			InitClassFetch(pointOfSaleID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="validator">The custom validator object for this PointOfSaleEntity</param>
		public PointOfSaleEntity(System.Int64 pointOfSaleID, IValidator validator):base("PointOfSaleEntity")
		{
			InitClassFetch(pointOfSaleID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfSaleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_products = (VarioSL.Entities.CollectionClasses.ProductCollection)info.GetValue("_products", typeof(VarioSL.Entities.CollectionClasses.ProductCollection));
			_alwaysFetchProducts = info.GetBoolean("_alwaysFetchProducts");
			_alreadyFetchedProducts = info.GetBoolean("_alreadyFetchedProducts");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedProducts = (_products.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Products":
					toReturn.Add(Relations.ProductEntityUsingPointOfSaleID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_products", (!this.MarkedForDeletion?_products:null));
			info.AddValue("_alwaysFetchProducts", _alwaysFetchProducts);
			info.AddValue("_alreadyFetchedProducts", _alreadyFetchedProducts);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Products":
					_alreadyFetchedProducts = true;
					if(entity!=null)
					{
						this.Products.Add((ProductEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Products":
					_products.Add((ProductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Products":
					this.PerformRelatedEntityRemoval(_products, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_products);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pointOfSaleID)
		{
			return FetchUsingPK(pointOfSaleID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pointOfSaleID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(pointOfSaleID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pointOfSaleID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(pointOfSaleID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 pointOfSaleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(pointOfSaleID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.PointOfSaleID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new PointOfSaleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiProducts(forceFetch, _products.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiProducts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection GetMultiProducts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedProducts || forceFetch || _alwaysFetchProducts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_products);
				_products.SuppressClearInGetMulti=!forceFetch;
				_products.EntityFactoryToUse = entityFactoryToUse;
				_products.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, filter);
				_products.SuppressClearInGetMulti=false;
				_alreadyFetchedProducts = true;
			}
			return _products;
		}

		/// <summary> Sets the collection parameters for the collection for 'Products'. These settings will be taken into account
		/// when the property Products is requested or GetMultiProducts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProducts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_products.SortClauses=sortClauses;
			_products.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Products", _products);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="validator">The validator object for this PointOfSaleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 pointOfSaleID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(pointOfSaleID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_products = new VarioSL.Entities.CollectionClasses.ProductCollection();
			_products.SetContainingEntityInfo(this, "PointOfSale");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionBasePercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionDeduction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionEndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionFixed", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionIntervalTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionMax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionMin", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionStartDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionTargetRevenue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommissionTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractEndDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ContractStartDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CostCenter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CountryID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebtorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EmailAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastAdvancePaymentDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OpeningHours", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrganisationID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PhoneNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfSaleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointofSaleLocation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfSaleName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfSaleNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PointOfSaleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesTaxID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Surname", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TitleID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatTypeID", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="pointOfSaleID">PK value for PointOfSale which data should be fetched into this PointOfSale object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 pointOfSaleID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)PointOfSaleFieldIndex.PointOfSaleID].ForcedCurrentValueWrite(pointOfSaleID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreatePointOfSaleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new PointOfSaleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static PointOfSaleRelations Relations
		{
			get	{ return new PointOfSaleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProducts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("Products")[0], (int)VarioSL.Entities.EntityType.PointOfSaleEntity, (int)VarioSL.Entities.EntityType.ProductEntity, 0, null, null, null, "Products", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AddressID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."ADDRESSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> AddressID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.AddressID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.AddressID, value, true); }
		}

		/// <summary> The ClientID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."CLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)PointOfSaleFieldIndex.ClientID, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CommissionBasePercentage property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONBASEPERCENTAGE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionBasePercentage
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionBasePercentage, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionBasePercentage, value, true); }
		}

		/// <summary> The CommissionDeduction property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONDEDUCTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionDeduction
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionDeduction, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionDeduction, value, true); }
		}

		/// <summary> The CommissionEndDate property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONENDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CommissionEndDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfSaleFieldIndex.CommissionEndDate, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionEndDate, value, true); }
		}

		/// <summary> The CommissionFixed property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONFIXED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionFixed
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionFixed, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionFixed, value, true); }
		}

		/// <summary> The CommissionIntervalTypeID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONINTERVALTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionIntervalTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionIntervalTypeID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionIntervalTypeID, value, true); }
		}

		/// <summary> The CommissionMax property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONMAX"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionMax
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionMax, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionMax, value, true); }
		}

		/// <summary> The CommissionMin property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONMIN"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionMin
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionMin, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionMin, value, true); }
		}

		/// <summary> The CommissionStartDate property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONSTARTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CommissionStartDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfSaleFieldIndex.CommissionStartDate, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionStartDate, value, true); }
		}

		/// <summary> The CommissionTargetRevenue property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONTARGETREVENUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionTargetRevenue
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionTargetRevenue, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionTargetRevenue, value, true); }
		}

		/// <summary> The CommissionTypeID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COMMISSIONTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CommissionTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CommissionTypeID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CommissionTypeID, value, true); }
		}

		/// <summary> The ContractEndDate property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."CONTRACTENDDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ContractEndDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfSaleFieldIndex.ContractEndDate, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.ContractEndDate, value, true); }
		}

		/// <summary> The ContractorID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."CONTRACTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ContractorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.ContractorID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.ContractorID, value, true); }
		}

		/// <summary> The ContractStartDate property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."CONTRACTSTARTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> ContractStartDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfSaleFieldIndex.ContractStartDate, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.ContractStartDate, value, true); }
		}

		/// <summary> The CostCenter property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COSTCENTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CostCenter
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.CostCenter, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CostCenter, value, true); }
		}

		/// <summary> The CountryID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."COUNTRYID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CountryID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CountryID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CountryID, value, true); }
		}

		/// <summary> The CreditorID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."CREDITORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CreditorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.CreditorID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.CreditorID, value, true); }
		}

		/// <summary> The DebtorID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."DEBTORID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> DebtorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.DebtorID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.DebtorID, value, true); }
		}

		/// <summary> The Description property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.Description, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.Description, value, true); }
		}

		/// <summary> The EmailAddress property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."EMAILADDRESS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EmailAddress
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.EmailAddress, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.EmailAddress, value, true); }
		}

		/// <summary> The FirstName property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."FIRSTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FirstName
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.FirstName, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.FirstName, value, true); }
		}

		/// <summary> The LastAdvancePaymentDate property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."LASTADVANCEPAYMENTDATE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastAdvancePaymentDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)PointOfSaleFieldIndex.LastAdvancePaymentDate, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.LastAdvancePaymentDate, value, true); }
		}

		/// <summary> The Notes property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."NOTES"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.Notes, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.Notes, value, true); }
		}

		/// <summary> The OpeningHours property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."OPENINGHOURS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OpeningHours
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.OpeningHours, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.OpeningHours, value, true); }
		}

		/// <summary> The OrganisationID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."ORGANISATIONID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OrganisationID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.OrganisationID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.OrganisationID, value, true); }
		}

		/// <summary> The PhoneNumber property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."PHONENUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PhoneNumber
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.PhoneNumber, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PhoneNumber, value, true); }
		}

		/// <summary> The PointOfSaleID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."POINTOFSALEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 PointOfSaleID
		{
			get { return (System.Int64)GetValue((int)PointOfSaleFieldIndex.PointOfSaleID, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PointOfSaleID, value, true); }
		}

		/// <summary> The PointofSaleLocation property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."POINTOFSALELOCATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PointofSaleLocation
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.PointofSaleLocation, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PointofSaleLocation, value, true); }
		}

		/// <summary> The PointOfSaleName property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."POINTOFSALENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String PointOfSaleName
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.PointOfSaleName, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PointOfSaleName, value, true); }
		}

		/// <summary> The PointOfSaleNumber property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."POINTOFSALENO"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PointOfSaleNumber
		{
			get { return (System.Int64)GetValue((int)PointOfSaleFieldIndex.PointOfSaleNumber, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PointOfSaleNumber, value, true); }
		}

		/// <summary> The PointOfSaleTypeID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."POINTOFSALETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> PointOfSaleTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.PointOfSaleTypeID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.PointOfSaleTypeID, value, true); }
		}

		/// <summary> The SalesTaxID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."SALESTAXID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> SalesTaxID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.SalesTaxID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.SalesTaxID, value, true); }
		}

		/// <summary> The Surname property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."SURNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Surname
		{
			get { return (System.String)GetValue((int)PointOfSaleFieldIndex.Surname, true); }
			set	{ SetValue((int)PointOfSaleFieldIndex.Surname, value, true); }
		}

		/// <summary> The TitleID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."TITLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TitleID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.TitleID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.TitleID, value, true); }
		}

		/// <summary> The VatTypeID property of the Entity PointOfSale<br/><br/></summary>
		/// <remarks>Mapped on  table field: "POS_POINTOFSALE"."VATTYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VatTypeID
		{
			get { return (Nullable<System.Int64>)GetValue((int)PointOfSaleFieldIndex.VatTypeID, false); }
			set	{ SetValue((int)PointOfSaleFieldIndex.VatTypeID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProducts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ProductCollection Products
		{
			get	{ return GetMultiProducts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Products. When set to true, Products is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Products is accessed. You can always execute/ a forced fetch by calling GetMultiProducts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProducts
		{
			get	{ return _alwaysFetchProducts; }
			set	{ _alwaysFetchProducts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Products already has been fetched. Setting this property to false when Products has been fetched
		/// will clear the Products collection well. Setting this property to true while Products hasn't been fetched disables lazy loading for Products</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProducts
		{
			get { return _alreadyFetchedProducts;}
			set 
			{
				if(_alreadyFetchedProducts && !value && (_products != null))
				{
					_products.Clear();
				}
				_alreadyFetchedProducts = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.PointOfSaleEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
