﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'BusinessRuleType'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class BusinessRuleTypeEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.BusinessRuleCollection	_businessRules;
		private bool	_alwaysFetchBusinessRules, _alreadyFetchedBusinessRules;
		private VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection	_businessRuleCondition;
		private bool	_alwaysFetchBusinessRuleCondition, _alreadyFetchedBusinessRuleCondition;
		private VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection	_businessRuleResult;
		private bool	_alwaysFetchBusinessRuleResult, _alreadyFetchedBusinessRuleResult;
		private VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection	_businessRuleTypeToVariables;
		private bool	_alwaysFetchBusinessRuleTypeToVariables, _alreadyFetchedBusinessRuleTypeToVariables;
		private VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection _assignedBusinessRuleVariables;
		private bool	_alwaysFetchAssignedBusinessRuleVariables, _alreadyFetchedAssignedBusinessRuleVariables;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name BusinessRules</summary>
			public static readonly string BusinessRules = "BusinessRules";
			/// <summary>Member name BusinessRuleCondition</summary>
			public static readonly string BusinessRuleCondition = "BusinessRuleCondition";
			/// <summary>Member name BusinessRuleResult</summary>
			public static readonly string BusinessRuleResult = "BusinessRuleResult";
			/// <summary>Member name BusinessRuleTypeToVariables</summary>
			public static readonly string BusinessRuleTypeToVariables = "BusinessRuleTypeToVariables";
			/// <summary>Member name AssignedBusinessRuleVariables</summary>
			public static readonly string AssignedBusinessRuleVariables = "AssignedBusinessRuleVariables";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static BusinessRuleTypeEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public BusinessRuleTypeEntity() :base("BusinessRuleTypeEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		public BusinessRuleTypeEntity(System.Int64 businessRuleTypeID):base("BusinessRuleTypeEntity")
		{
			InitClassFetch(businessRuleTypeID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BusinessRuleTypeEntity(System.Int64 businessRuleTypeID, IPrefetchPath prefetchPathToUse):base("BusinessRuleTypeEntity")
		{
			InitClassFetch(businessRuleTypeID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="validator">The custom validator object for this BusinessRuleTypeEntity</param>
		public BusinessRuleTypeEntity(System.Int64 businessRuleTypeID, IValidator validator):base("BusinessRuleTypeEntity")
		{
			InitClassFetch(businessRuleTypeID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinessRuleTypeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_businessRules = (VarioSL.Entities.CollectionClasses.BusinessRuleCollection)info.GetValue("_businessRules", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleCollection));
			_alwaysFetchBusinessRules = info.GetBoolean("_alwaysFetchBusinessRules");
			_alreadyFetchedBusinessRules = info.GetBoolean("_alreadyFetchedBusinessRules");

			_businessRuleCondition = (VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection)info.GetValue("_businessRuleCondition", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection));
			_alwaysFetchBusinessRuleCondition = info.GetBoolean("_alwaysFetchBusinessRuleCondition");
			_alreadyFetchedBusinessRuleCondition = info.GetBoolean("_alreadyFetchedBusinessRuleCondition");

			_businessRuleResult = (VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection)info.GetValue("_businessRuleResult", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection));
			_alwaysFetchBusinessRuleResult = info.GetBoolean("_alwaysFetchBusinessRuleResult");
			_alreadyFetchedBusinessRuleResult = info.GetBoolean("_alreadyFetchedBusinessRuleResult");

			_businessRuleTypeToVariables = (VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection)info.GetValue("_businessRuleTypeToVariables", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection));
			_alwaysFetchBusinessRuleTypeToVariables = info.GetBoolean("_alwaysFetchBusinessRuleTypeToVariables");
			_alreadyFetchedBusinessRuleTypeToVariables = info.GetBoolean("_alreadyFetchedBusinessRuleTypeToVariables");
			_assignedBusinessRuleVariables = (VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection)info.GetValue("_assignedBusinessRuleVariables", typeof(VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection));
			_alwaysFetchAssignedBusinessRuleVariables = info.GetBoolean("_alwaysFetchAssignedBusinessRuleVariables");
			_alreadyFetchedAssignedBusinessRuleVariables = info.GetBoolean("_alreadyFetchedAssignedBusinessRuleVariables");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedBusinessRules = (_businessRules.Count > 0);
			_alreadyFetchedBusinessRuleCondition = (_businessRuleCondition.Count > 0);
			_alreadyFetchedBusinessRuleResult = (_businessRuleResult.Count > 0);
			_alreadyFetchedBusinessRuleTypeToVariables = (_businessRuleTypeToVariables.Count > 0);
			_alreadyFetchedAssignedBusinessRuleVariables = (_assignedBusinessRuleVariables.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "BusinessRules":
					toReturn.Add(Relations.BusinessRuleEntityUsingBusinessRuleTypeID);
					break;
				case "BusinessRuleCondition":
					toReturn.Add(Relations.BusinessRuleConditionEntityUsingBusinessRuleTypeID);
					break;
				case "BusinessRuleResult":
					toReturn.Add(Relations.BusinessRuleResultEntityUsingBusinessRuleTypeID);
					break;
				case "BusinessRuleTypeToVariables":
					toReturn.Add(Relations.BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID);
					break;
				case "AssignedBusinessRuleVariables":
					toReturn.Add(Relations.BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID, "BusinessRuleTypeEntity__", "BusinessRuleTypeToVariable_", JoinHint.None);
					toReturn.Add(BusinessRuleTypeToVariableEntity.Relations.BusinessRuleVariableEntityUsingBusinessRuleVariableID, "BusinessRuleTypeToVariable_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_businessRules", (!this.MarkedForDeletion?_businessRules:null));
			info.AddValue("_alwaysFetchBusinessRules", _alwaysFetchBusinessRules);
			info.AddValue("_alreadyFetchedBusinessRules", _alreadyFetchedBusinessRules);
			info.AddValue("_businessRuleCondition", (!this.MarkedForDeletion?_businessRuleCondition:null));
			info.AddValue("_alwaysFetchBusinessRuleCondition", _alwaysFetchBusinessRuleCondition);
			info.AddValue("_alreadyFetchedBusinessRuleCondition", _alreadyFetchedBusinessRuleCondition);
			info.AddValue("_businessRuleResult", (!this.MarkedForDeletion?_businessRuleResult:null));
			info.AddValue("_alwaysFetchBusinessRuleResult", _alwaysFetchBusinessRuleResult);
			info.AddValue("_alreadyFetchedBusinessRuleResult", _alreadyFetchedBusinessRuleResult);
			info.AddValue("_businessRuleTypeToVariables", (!this.MarkedForDeletion?_businessRuleTypeToVariables:null));
			info.AddValue("_alwaysFetchBusinessRuleTypeToVariables", _alwaysFetchBusinessRuleTypeToVariables);
			info.AddValue("_alreadyFetchedBusinessRuleTypeToVariables", _alreadyFetchedBusinessRuleTypeToVariables);
			info.AddValue("_assignedBusinessRuleVariables", (!this.MarkedForDeletion?_assignedBusinessRuleVariables:null));
			info.AddValue("_alwaysFetchAssignedBusinessRuleVariables", _alwaysFetchAssignedBusinessRuleVariables);
			info.AddValue("_alreadyFetchedAssignedBusinessRuleVariables", _alreadyFetchedAssignedBusinessRuleVariables);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "BusinessRules":
					_alreadyFetchedBusinessRules = true;
					if(entity!=null)
					{
						this.BusinessRules.Add((BusinessRuleEntity)entity);
					}
					break;
				case "BusinessRuleCondition":
					_alreadyFetchedBusinessRuleCondition = true;
					if(entity!=null)
					{
						this.BusinessRuleCondition.Add((BusinessRuleConditionEntity)entity);
					}
					break;
				case "BusinessRuleResult":
					_alreadyFetchedBusinessRuleResult = true;
					if(entity!=null)
					{
						this.BusinessRuleResult.Add((BusinessRuleResultEntity)entity);
					}
					break;
				case "BusinessRuleTypeToVariables":
					_alreadyFetchedBusinessRuleTypeToVariables = true;
					if(entity!=null)
					{
						this.BusinessRuleTypeToVariables.Add((BusinessRuleTypeToVariableEntity)entity);
					}
					break;
				case "AssignedBusinessRuleVariables":
					_alreadyFetchedAssignedBusinessRuleVariables = true;
					if(entity!=null)
					{
						this.AssignedBusinessRuleVariables.Add((BusinessRuleVariableEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "BusinessRules":
					_businessRules.Add((BusinessRuleEntity)relatedEntity);
					break;
				case "BusinessRuleCondition":
					_businessRuleCondition.Add((BusinessRuleConditionEntity)relatedEntity);
					break;
				case "BusinessRuleResult":
					_businessRuleResult.Add((BusinessRuleResultEntity)relatedEntity);
					break;
				case "BusinessRuleTypeToVariables":
					_businessRuleTypeToVariables.Add((BusinessRuleTypeToVariableEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "BusinessRules":
					this.PerformRelatedEntityRemoval(_businessRules, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleCondition":
					this.PerformRelatedEntityRemoval(_businessRuleCondition, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleResult":
					this.PerformRelatedEntityRemoval(_businessRuleResult, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "BusinessRuleTypeToVariables":
					this.PerformRelatedEntityRemoval(_businessRuleTypeToVariables, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_businessRules);
			toReturn.Add(_businessRuleCondition);
			toReturn.Add(_businessRuleResult);
			toReturn.Add(_businessRuleTypeToVariables);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID)
		{
			return FetchUsingPK(businessRuleTypeID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(businessRuleTypeID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(businessRuleTypeID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 businessRuleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(businessRuleTypeID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.BusinessRuleTypeID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new BusinessRuleTypeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRules(bool forceFetch)
		{
			return GetMultiBusinessRules(forceFetch, _businessRules.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRules(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRules(forceFetch, _businessRules.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRules(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRules(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleCollection GetMultiBusinessRules(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRules || forceFetch || _alwaysFetchBusinessRules) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRules);
				_businessRules.SuppressClearInGetMulti=!forceFetch;
				_businessRules.EntityFactoryToUse = entityFactoryToUse;
				_businessRules.GetMultiManyToOne(this, null, filter);
				_businessRules.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRules = true;
			}
			return _businessRules;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRules'. These settings will be taken into account
		/// when the property BusinessRules is requested or GetMultiBusinessRules is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRules(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRules.SortClauses=sortClauses;
			_businessRules.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleConditionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch)
		{
			return GetMultiBusinessRuleCondition(forceFetch, _businessRuleCondition.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleConditionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleCondition(forceFetch, _businessRuleCondition.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleCondition(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection GetMultiBusinessRuleCondition(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleCondition || forceFetch || _alwaysFetchBusinessRuleCondition) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleCondition);
				_businessRuleCondition.SuppressClearInGetMulti=!forceFetch;
				_businessRuleCondition.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleCondition.GetMultiManyToOne(this, null, filter);
				_businessRuleCondition.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleCondition = true;
			}
			return _businessRuleCondition;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleCondition'. These settings will be taken into account
		/// when the property BusinessRuleCondition is requested or GetMultiBusinessRuleCondition is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleCondition(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleCondition.SortClauses=sortClauses;
			_businessRuleCondition.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleResult(forceFetch, _businessRuleResult.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleResult(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection GetMultiBusinessRuleResult(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleResult || forceFetch || _alwaysFetchBusinessRuleResult) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleResult);
				_businessRuleResult.SuppressClearInGetMulti=!forceFetch;
				_businessRuleResult.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleResult.GetMultiManyToOne(null, this, null, null, null, filter);
				_businessRuleResult.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleResult = true;
			}
			return _businessRuleResult;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleResult'. These settings will be taken into account
		/// when the property BusinessRuleResult is requested or GetMultiBusinessRuleResult is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleResult(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleResult.SortClauses=sortClauses;
			_businessRuleResult.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleTypeToVariableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleTypeToVariableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection GetMultiBusinessRuleTypeToVariables(bool forceFetch)
		{
			return GetMultiBusinessRuleTypeToVariables(forceFetch, _businessRuleTypeToVariables.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleTypeToVariableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleTypeToVariableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection GetMultiBusinessRuleTypeToVariables(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiBusinessRuleTypeToVariables(forceFetch, _businessRuleTypeToVariables.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleTypeToVariableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection GetMultiBusinessRuleTypeToVariables(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiBusinessRuleTypeToVariables(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleTypeToVariableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection GetMultiBusinessRuleTypeToVariables(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedBusinessRuleTypeToVariables || forceFetch || _alwaysFetchBusinessRuleTypeToVariables) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_businessRuleTypeToVariables);
				_businessRuleTypeToVariables.SuppressClearInGetMulti=!forceFetch;
				_businessRuleTypeToVariables.EntityFactoryToUse = entityFactoryToUse;
				_businessRuleTypeToVariables.GetMultiManyToOne(this, null, filter);
				_businessRuleTypeToVariables.SuppressClearInGetMulti=false;
				_alreadyFetchedBusinessRuleTypeToVariables = true;
			}
			return _businessRuleTypeToVariables;
		}

		/// <summary> Sets the collection parameters for the collection for 'BusinessRuleTypeToVariables'. These settings will be taken into account
		/// when the property BusinessRuleTypeToVariables is requested or GetMultiBusinessRuleTypeToVariables is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersBusinessRuleTypeToVariables(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_businessRuleTypeToVariables.SortClauses=sortClauses;
			_businessRuleTypeToVariables.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleVariableEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'BusinessRuleVariableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection GetMultiAssignedBusinessRuleVariables(bool forceFetch)
		{
			return GetMultiAssignedBusinessRuleVariables(forceFetch, _assignedBusinessRuleVariables.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleVariableEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection GetMultiAssignedBusinessRuleVariables(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedAssignedBusinessRuleVariables || forceFetch || _alwaysFetchAssignedBusinessRuleVariables) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_assignedBusinessRuleVariables);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(BusinessRuleTypeFields.BusinessRuleTypeID, ComparisonOperator.Equal, this.BusinessRuleTypeID, "BusinessRuleTypeEntity__"));
				_assignedBusinessRuleVariables.SuppressClearInGetMulti=!forceFetch;
				_assignedBusinessRuleVariables.EntityFactoryToUse = entityFactoryToUse;
				_assignedBusinessRuleVariables.GetMulti(filter, GetRelationsForField("AssignedBusinessRuleVariables"));
				_assignedBusinessRuleVariables.SuppressClearInGetMulti=false;
				_alreadyFetchedAssignedBusinessRuleVariables = true;
			}
			return _assignedBusinessRuleVariables;
		}

		/// <summary> Sets the collection parameters for the collection for 'AssignedBusinessRuleVariables'. These settings will be taken into account
		/// when the property AssignedBusinessRuleVariables is requested or GetMultiAssignedBusinessRuleVariables is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAssignedBusinessRuleVariables(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_assignedBusinessRuleVariables.SortClauses=sortClauses;
			_assignedBusinessRuleVariables.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("BusinessRules", _businessRules);
			toReturn.Add("BusinessRuleCondition", _businessRuleCondition);
			toReturn.Add("BusinessRuleResult", _businessRuleResult);
			toReturn.Add("BusinessRuleTypeToVariables", _businessRuleTypeToVariables);
			toReturn.Add("AssignedBusinessRuleVariables", _assignedBusinessRuleVariables);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="validator">The validator object for this BusinessRuleTypeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 businessRuleTypeID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(businessRuleTypeID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_businessRules = new VarioSL.Entities.CollectionClasses.BusinessRuleCollection();
			_businessRules.SetContainingEntityInfo(this, "BusinessRuleType");

			_businessRuleCondition = new VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection();
			_businessRuleCondition.SetContainingEntityInfo(this, "BusinessRuleType");

			_businessRuleResult = new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection();
			_businessRuleResult.SetContainingEntityInfo(this, "BusinessRuleType");

			_businessRuleTypeToVariables = new VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection();
			_businessRuleTypeToVariables.SetContainingEntityInfo(this, "BusinessRuleType");
			_assignedBusinessRuleVariables = new VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllowAccumulation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleTypeGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleTypeID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BusinessRuleTypeName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("StandAlone", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="businessRuleTypeID">PK value for BusinessRuleType which data should be fetched into this BusinessRuleType object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 businessRuleTypeID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)BusinessRuleTypeFieldIndex.BusinessRuleTypeID].ForcedCurrentValueWrite(businessRuleTypeID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateBusinessRuleTypeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new BusinessRuleTypeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static BusinessRuleTypeRelations Relations
		{
			get	{ return new BusinessRuleTypeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRule' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRules
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleCollection(), (IEntityRelation)GetRelationsForField("BusinessRules")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, (int)VarioSL.Entities.EntityType.BusinessRuleEntity, 0, null, null, null, "BusinessRules", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleCondition' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleCondition
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleCondition")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, (int)VarioSL.Entities.EntityType.BusinessRuleConditionEntity, 0, null, null, null, "BusinessRuleCondition", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleResult
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleResult")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, (int)VarioSL.Entities.EntityType.BusinessRuleResultEntity, 0, null, null, null, "BusinessRuleResult", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleTypeToVariable' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathBusinessRuleTypeToVariables
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection(), (IEntityRelation)GetRelationsForField("BusinessRuleTypeToVariables")[0], (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, (int)VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity, 0, null, null, null, "BusinessRuleTypeToVariables", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'BusinessRuleVariable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAssignedBusinessRuleVariables
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID;
				intermediateRelation.SetAliases(string.Empty, "BusinessRuleTypeToVariable_");
				return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection(), intermediateRelation,	(int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity, (int)VarioSL.Entities.EntityType.BusinessRuleVariableEntity, 0, null, null, GetRelationsForField("AssignedBusinessRuleVariables"), "AssignedBusinessRuleVariables", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AllowAccumulation property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."ALLOWACCUMULATION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 2, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllowAccumulation
		{
			get { return (System.Boolean)GetValue((int)BusinessRuleTypeFieldIndex.AllowAccumulation, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.AllowAccumulation, value, true); }
		}

		/// <summary> The BusinessRuleTypeGroup property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."BUSINESSRULETYPEGROUPNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual VarioSL.Entities.Enumerations.TariffBusinessRuleTypeGroup BusinessRuleTypeGroup
		{
			get { return (VarioSL.Entities.Enumerations.TariffBusinessRuleTypeGroup)GetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeGroup, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeGroup, value, true); }
		}

		/// <summary> The BusinessRuleTypeID property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."BUSINESSRULETYPEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 BusinessRuleTypeID
		{
			get { return (System.Int64)GetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeID, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeID, value, true); }
		}

		/// <summary> The BusinessRuleTypeName property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."BUSINESSRULETYPENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String BusinessRuleTypeName
		{
			get { return (System.String)GetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeName, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.BusinessRuleTypeName, value, true); }
		}

		/// <summary> The Description property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 2000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)BusinessRuleTypeFieldIndex.Description, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.Description, value, true); }
		}

		/// <summary> The StandAlone property of the Entity BusinessRuleType<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_BUSINESSRULETYPE"."STANDALONE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean StandAlone
		{
			get { return (System.Boolean)GetValue((int)BusinessRuleTypeFieldIndex.StandAlone, true); }
			set	{ SetValue((int)BusinessRuleTypeFieldIndex.StandAlone, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRules()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleCollection BusinessRules
		{
			get	{ return GetMultiBusinessRules(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRules. When set to true, BusinessRules is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRules is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRules(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRules
		{
			get	{ return _alwaysFetchBusinessRules; }
			set	{ _alwaysFetchBusinessRules = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRules already has been fetched. Setting this property to false when BusinessRules has been fetched
		/// will clear the BusinessRules collection well. Setting this property to true while BusinessRules hasn't been fetched disables lazy loading for BusinessRules</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRules
		{
			get { return _alreadyFetchedBusinessRules;}
			set 
			{
				if(_alreadyFetchedBusinessRules && !value && (_businessRules != null))
				{
					_businessRules.Clear();
				}
				_alreadyFetchedBusinessRules = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleConditionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleCondition()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleConditionCollection BusinessRuleCondition
		{
			get	{ return GetMultiBusinessRuleCondition(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleCondition. When set to true, BusinessRuleCondition is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleCondition is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleCondition(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleCondition
		{
			get	{ return _alwaysFetchBusinessRuleCondition; }
			set	{ _alwaysFetchBusinessRuleCondition = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleCondition already has been fetched. Setting this property to false when BusinessRuleCondition has been fetched
		/// will clear the BusinessRuleCondition collection well. Setting this property to true while BusinessRuleCondition hasn't been fetched disables lazy loading for BusinessRuleCondition</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleCondition
		{
			get { return _alreadyFetchedBusinessRuleCondition;}
			set 
			{
				if(_alreadyFetchedBusinessRuleCondition && !value && (_businessRuleCondition != null))
				{
					_businessRuleCondition.Clear();
				}
				_alreadyFetchedBusinessRuleCondition = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleResult()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleResultCollection BusinessRuleResult
		{
			get	{ return GetMultiBusinessRuleResult(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleResult. When set to true, BusinessRuleResult is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleResult is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleResult(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleResult
		{
			get	{ return _alwaysFetchBusinessRuleResult; }
			set	{ _alwaysFetchBusinessRuleResult = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleResult already has been fetched. Setting this property to false when BusinessRuleResult has been fetched
		/// will clear the BusinessRuleResult collection well. Setting this property to true while BusinessRuleResult hasn't been fetched disables lazy loading for BusinessRuleResult</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleResult
		{
			get { return _alreadyFetchedBusinessRuleResult;}
			set 
			{
				if(_alreadyFetchedBusinessRuleResult && !value && (_businessRuleResult != null))
				{
					_businessRuleResult.Clear();
				}
				_alreadyFetchedBusinessRuleResult = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'BusinessRuleTypeToVariableEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiBusinessRuleTypeToVariables()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleTypeToVariableCollection BusinessRuleTypeToVariables
		{
			get	{ return GetMultiBusinessRuleTypeToVariables(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for BusinessRuleTypeToVariables. When set to true, BusinessRuleTypeToVariables is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time BusinessRuleTypeToVariables is accessed. You can always execute/ a forced fetch by calling GetMultiBusinessRuleTypeToVariables(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchBusinessRuleTypeToVariables
		{
			get	{ return _alwaysFetchBusinessRuleTypeToVariables; }
			set	{ _alwaysFetchBusinessRuleTypeToVariables = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property BusinessRuleTypeToVariables already has been fetched. Setting this property to false when BusinessRuleTypeToVariables has been fetched
		/// will clear the BusinessRuleTypeToVariables collection well. Setting this property to true while BusinessRuleTypeToVariables hasn't been fetched disables lazy loading for BusinessRuleTypeToVariables</summary>
		[Browsable(false)]
		public bool AlreadyFetchedBusinessRuleTypeToVariables
		{
			get { return _alreadyFetchedBusinessRuleTypeToVariables;}
			set 
			{
				if(_alreadyFetchedBusinessRuleTypeToVariables && !value && (_businessRuleTypeToVariables != null))
				{
					_businessRuleTypeToVariables.Clear();
				}
				_alreadyFetchedBusinessRuleTypeToVariables = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'BusinessRuleVariableEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAssignedBusinessRuleVariables()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.BusinessRuleVariableCollection AssignedBusinessRuleVariables
		{
			get { return GetMultiAssignedBusinessRuleVariables(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AssignedBusinessRuleVariables. When set to true, AssignedBusinessRuleVariables is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AssignedBusinessRuleVariables is accessed. You can always execute a forced fetch by calling GetMultiAssignedBusinessRuleVariables(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAssignedBusinessRuleVariables
		{
			get	{ return _alwaysFetchAssignedBusinessRuleVariables; }
			set	{ _alwaysFetchAssignedBusinessRuleVariables = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AssignedBusinessRuleVariables already has been fetched. Setting this property to false when AssignedBusinessRuleVariables has been fetched
		/// will clear the AssignedBusinessRuleVariables collection well. Setting this property to true while AssignedBusinessRuleVariables hasn't been fetched disables lazy loading for AssignedBusinessRuleVariables</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAssignedBusinessRuleVariables
		{
			get { return _alreadyFetchedAssignedBusinessRuleVariables;}
			set 
			{
				if(_alreadyFetchedAssignedBusinessRuleVariables && !value && (_assignedBusinessRuleVariables != null))
				{
					_assignedBusinessRuleVariables.Clear();
				}
				_alreadyFetchedAssignedBusinessRuleVariables = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.BusinessRuleTypeEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
