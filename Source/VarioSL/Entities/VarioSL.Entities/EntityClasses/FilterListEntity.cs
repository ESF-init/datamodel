﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FilterList'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FilterListEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FilterListElementCollection	_filterListElements;
		private bool	_alwaysFetchFilterListElements, _alreadyFetchedFilterListElements;
		private VarioSL.Entities.CollectionClasses.FilterValueSetCollection	_filterValueSets;
		private bool	_alwaysFetchFilterValueSets, _alreadyFetchedFilterValueSets;
		private VarioSL.Entities.CollectionClasses.ReportCollection	_reports;
		private bool	_alwaysFetchReports, _alreadyFetchedReports;
		private VarioSL.Entities.CollectionClasses.ReportCategoryCollection	_reportCategories;
		private bool	_alwaysFetchReportCategories, _alreadyFetchedReportCategories;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name FilterListElements</summary>
			public static readonly string FilterListElements = "FilterListElements";
			/// <summary>Member name FilterValueSets</summary>
			public static readonly string FilterValueSets = "FilterValueSets";
			/// <summary>Member name Reports</summary>
			public static readonly string Reports = "Reports";
			/// <summary>Member name ReportCategories</summary>
			public static readonly string ReportCategories = "ReportCategories";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FilterListEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilterListEntity() :base("FilterListEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		public FilterListEntity(System.Int64 filterListID):base("FilterListEntity")
		{
			InitClassFetch(filterListID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FilterListEntity(System.Int64 filterListID, IPrefetchPath prefetchPathToUse):base("FilterListEntity")
		{
			InitClassFetch(filterListID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="validator">The custom validator object for this FilterListEntity</param>
		public FilterListEntity(System.Int64 filterListID, IValidator validator):base("FilterListEntity")
		{
			InitClassFetch(filterListID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilterListEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_filterListElements = (VarioSL.Entities.CollectionClasses.FilterListElementCollection)info.GetValue("_filterListElements", typeof(VarioSL.Entities.CollectionClasses.FilterListElementCollection));
			_alwaysFetchFilterListElements = info.GetBoolean("_alwaysFetchFilterListElements");
			_alreadyFetchedFilterListElements = info.GetBoolean("_alreadyFetchedFilterListElements");

			_filterValueSets = (VarioSL.Entities.CollectionClasses.FilterValueSetCollection)info.GetValue("_filterValueSets", typeof(VarioSL.Entities.CollectionClasses.FilterValueSetCollection));
			_alwaysFetchFilterValueSets = info.GetBoolean("_alwaysFetchFilterValueSets");
			_alreadyFetchedFilterValueSets = info.GetBoolean("_alreadyFetchedFilterValueSets");

			_reports = (VarioSL.Entities.CollectionClasses.ReportCollection)info.GetValue("_reports", typeof(VarioSL.Entities.CollectionClasses.ReportCollection));
			_alwaysFetchReports = info.GetBoolean("_alwaysFetchReports");
			_alreadyFetchedReports = info.GetBoolean("_alreadyFetchedReports");

			_reportCategories = (VarioSL.Entities.CollectionClasses.ReportCategoryCollection)info.GetValue("_reportCategories", typeof(VarioSL.Entities.CollectionClasses.ReportCategoryCollection));
			_alwaysFetchReportCategories = info.GetBoolean("_alwaysFetchReportCategories");
			_alreadyFetchedReportCategories = info.GetBoolean("_alreadyFetchedReportCategories");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFilterListElements = (_filterListElements.Count > 0);
			_alreadyFetchedFilterValueSets = (_filterValueSets.Count > 0);
			_alreadyFetchedReports = (_reports.Count > 0);
			_alreadyFetchedReportCategories = (_reportCategories.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "FilterListElements":
					toReturn.Add(Relations.FilterListElementEntityUsingFilterListID);
					break;
				case "FilterValueSets":
					toReturn.Add(Relations.FilterValueSetEntityUsingFilterListID);
					break;
				case "Reports":
					toReturn.Add(Relations.ReportEntityUsingFilterListID);
					break;
				case "ReportCategories":
					toReturn.Add(Relations.ReportCategoryEntityUsingCategoryFilterListID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_filterListElements", (!this.MarkedForDeletion?_filterListElements:null));
			info.AddValue("_alwaysFetchFilterListElements", _alwaysFetchFilterListElements);
			info.AddValue("_alreadyFetchedFilterListElements", _alreadyFetchedFilterListElements);
			info.AddValue("_filterValueSets", (!this.MarkedForDeletion?_filterValueSets:null));
			info.AddValue("_alwaysFetchFilterValueSets", _alwaysFetchFilterValueSets);
			info.AddValue("_alreadyFetchedFilterValueSets", _alreadyFetchedFilterValueSets);
			info.AddValue("_reports", (!this.MarkedForDeletion?_reports:null));
			info.AddValue("_alwaysFetchReports", _alwaysFetchReports);
			info.AddValue("_alreadyFetchedReports", _alreadyFetchedReports);
			info.AddValue("_reportCategories", (!this.MarkedForDeletion?_reportCategories:null));
			info.AddValue("_alwaysFetchReportCategories", _alwaysFetchReportCategories);
			info.AddValue("_alreadyFetchedReportCategories", _alreadyFetchedReportCategories);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "FilterListElements":
					_alreadyFetchedFilterListElements = true;
					if(entity!=null)
					{
						this.FilterListElements.Add((FilterListElementEntity)entity);
					}
					break;
				case "FilterValueSets":
					_alreadyFetchedFilterValueSets = true;
					if(entity!=null)
					{
						this.FilterValueSets.Add((FilterValueSetEntity)entity);
					}
					break;
				case "Reports":
					_alreadyFetchedReports = true;
					if(entity!=null)
					{
						this.Reports.Add((ReportEntity)entity);
					}
					break;
				case "ReportCategories":
					_alreadyFetchedReportCategories = true;
					if(entity!=null)
					{
						this.ReportCategories.Add((ReportCategoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "FilterListElements":
					_filterListElements.Add((FilterListElementEntity)relatedEntity);
					break;
				case "FilterValueSets":
					_filterValueSets.Add((FilterValueSetEntity)relatedEntity);
					break;
				case "Reports":
					_reports.Add((ReportEntity)relatedEntity);
					break;
				case "ReportCategories":
					_reportCategories.Add((ReportCategoryEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "FilterListElements":
					this.PerformRelatedEntityRemoval(_filterListElements, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FilterValueSets":
					this.PerformRelatedEntityRemoval(_filterValueSets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Reports":
					this.PerformRelatedEntityRemoval(_reports, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ReportCategories":
					this.PerformRelatedEntityRemoval(_reportCategories, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_filterListElements);
			toReturn.Add(_filterValueSets);
			toReturn.Add(_reports);
			toReturn.Add(_reportCategories);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListID)
		{
			return FetchUsingPK(filterListID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(filterListID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(filterListID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 filterListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(filterListID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FilterListID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FilterListRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch)
		{
			return GetMultiFilterListElements(forceFetch, _filterListElements.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterListElementEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterListElements(forceFetch, _filterListElements.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterListElements(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterListElementCollection GetMultiFilterListElements(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterListElements || forceFetch || _alwaysFetchFilterListElements) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterListElements);
				_filterListElements.SuppressClearInGetMulti=!forceFetch;
				_filterListElements.EntityFactoryToUse = entityFactoryToUse;
				_filterListElements.GetMultiManyToOne(null, null, this, filter);
				_filterListElements.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterListElements = true;
			}
			return _filterListElements;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterListElements'. These settings will be taken into account
		/// when the property FilterListElements is requested or GetMultiFilterListElements is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterListElements(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterListElements.SortClauses=sortClauses;
			_filterListElements.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FilterValueSetEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFilterValueSets(forceFetch, _filterValueSets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFilterValueSets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection GetMultiFilterValueSets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFilterValueSets || forceFetch || _alwaysFetchFilterValueSets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_filterValueSets);
				_filterValueSets.SuppressClearInGetMulti=!forceFetch;
				_filterValueSets.EntityFactoryToUse = entityFactoryToUse;
				_filterValueSets.GetMultiManyToOne(null, null, null, this, filter);
				_filterValueSets.SuppressClearInGetMulti=false;
				_alreadyFetchedFilterValueSets = true;
			}
			return _filterValueSets;
		}

		/// <summary> Sets the collection parameters for the collection for 'FilterValueSets'. These settings will be taken into account
		/// when the property FilterValueSets is requested or GetMultiFilterValueSets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFilterValueSets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_filterValueSets.SortClauses=sortClauses;
			_filterValueSets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReports(forceFetch, _reports.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReports(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection GetMultiReports(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReports || forceFetch || _alwaysFetchReports) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reports);
				_reports.SuppressClearInGetMulti=!forceFetch;
				_reports.EntityFactoryToUse = entityFactoryToUse;
				_reports.GetMultiManyToOne(null, this, null, null, filter);
				_reports.SuppressClearInGetMulti=false;
				_alreadyFetchedReports = true;
			}
			return _reports;
		}

		/// <summary> Sets the collection parameters for the collection for 'Reports'. These settings will be taken into account
		/// when the property Reports is requested or GetMultiReports is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReports(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reports.SortClauses=sortClauses;
			_reports.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch)
		{
			return GetMultiReportCategories(forceFetch, _reportCategories.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ReportCategoryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiReportCategories(forceFetch, _reportCategories.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiReportCategories(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection GetMultiReportCategories(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedReportCategories || forceFetch || _alwaysFetchReportCategories) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_reportCategories);
				_reportCategories.SuppressClearInGetMulti=!forceFetch;
				_reportCategories.EntityFactoryToUse = entityFactoryToUse;
				_reportCategories.GetMultiManyToOne(null, this, null, filter);
				_reportCategories.SuppressClearInGetMulti=false;
				_alreadyFetchedReportCategories = true;
			}
			return _reportCategories;
		}

		/// <summary> Sets the collection parameters for the collection for 'ReportCategories'. These settings will be taken into account
		/// when the property ReportCategories is requested or GetMultiReportCategories is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersReportCategories(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_reportCategories.SortClauses=sortClauses;
			_reportCategories.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("FilterListElements", _filterListElements);
			toReturn.Add("FilterValueSets", _filterValueSets);
			toReturn.Add("Reports", _reports);
			toReturn.Add("ReportCategories", _reportCategories);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="validator">The validator object for this FilterListEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 filterListID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(filterListID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_filterListElements = new VarioSL.Entities.CollectionClasses.FilterListElementCollection();
			_filterListElements.SetContainingEntityInfo(this, "FilterList");

			_filterValueSets = new VarioSL.Entities.CollectionClasses.FilterValueSetCollection();
			_filterValueSets.SetContainingEntityInfo(this, "FilterList");

			_reports = new VarioSL.Entities.CollectionClasses.ReportCollection();
			_reports.SetContainingEntityInfo(this, "FilterList");

			_reportCategories = new VarioSL.Entities.CollectionClasses.ReportCategoryCollection();
			_reportCategories.SetContainingEntityInfo(this, "FilterList");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DisplayName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FilterListID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="filterListID">PK value for FilterList which data should be fetched into this FilterList object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 filterListID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FilterListFieldIndex.FilterListID].ForcedCurrentValueWrite(filterListID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFilterListDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FilterListEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FilterListRelations Relations
		{
			get	{ return new FilterListRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterListElement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterListElements
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterListElementCollection(), (IEntityRelation)GetRelationsForField("FilterListElements")[0], (int)VarioSL.Entities.EntityType.FilterListEntity, (int)VarioSL.Entities.EntityType.FilterListElementEntity, 0, null, null, null, "FilterListElements", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FilterValueSet' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFilterValueSets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FilterValueSetCollection(), (IEntityRelation)GetRelationsForField("FilterValueSets")[0], (int)VarioSL.Entities.EntityType.FilterListEntity, (int)VarioSL.Entities.EntityType.FilterValueSetEntity, 0, null, null, null, "FilterValueSets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Report' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReports
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCollection(), (IEntityRelation)GetRelationsForField("Reports")[0], (int)VarioSL.Entities.EntityType.FilterListEntity, (int)VarioSL.Entities.EntityType.ReportEntity, 0, null, null, null, "Reports", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportCategory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportCategories
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ReportCategoryCollection(), (IEntityRelation)GetRelationsForField("ReportCategories")[0], (int)VarioSL.Entities.EntityType.FilterListEntity, (int)VarioSL.Entities.EntityType.ReportCategoryEntity, 0, null, null, null, "ReportCategories", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FilterListFieldIndex.Description, true); }
			set	{ SetValue((int)FilterListFieldIndex.Description, value, true); }
		}

		/// <summary> The DisplayName property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."DISPLAYNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DisplayName
		{
			get { return (System.String)GetValue((int)FilterListFieldIndex.DisplayName, true); }
			set	{ SetValue((int)FilterListFieldIndex.DisplayName, value, true); }
		}

		/// <summary> The FilterListID property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."FILTERLISTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 FilterListID
		{
			get { return (System.Int64)GetValue((int)FilterListFieldIndex.FilterListID, true); }
			set	{ SetValue((int)FilterListFieldIndex.FilterListID, value, true); }
		}

		/// <summary> The LastModified property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."LASTMODIFIED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)FilterListFieldIndex.LastModified, true); }
			set	{ SetValue((int)FilterListFieldIndex.LastModified, value, true); }
		}

		/// <summary> The LastUser property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."LASTUSER"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String LastUser
		{
			get { return (System.String)GetValue((int)FilterListFieldIndex.LastUser, true); }
			set	{ SetValue((int)FilterListFieldIndex.LastUser, value, true); }
		}

		/// <summary> The TransactionCounter property of the Entity FilterList<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VAS_FILTERLIST"."TRANSACTIONCOUNTER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 38, 38, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TransactionCounter
		{
			get { return (System.Decimal)GetValue((int)FilterListFieldIndex.TransactionCounter, true); }
			set	{ SetValue((int)FilterListFieldIndex.TransactionCounter, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FilterListElementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterListElements()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterListElementCollection FilterListElements
		{
			get	{ return GetMultiFilterListElements(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterListElements. When set to true, FilterListElements is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterListElements is accessed. You can always execute/ a forced fetch by calling GetMultiFilterListElements(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterListElements
		{
			get	{ return _alwaysFetchFilterListElements; }
			set	{ _alwaysFetchFilterListElements = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterListElements already has been fetched. Setting this property to false when FilterListElements has been fetched
		/// will clear the FilterListElements collection well. Setting this property to true while FilterListElements hasn't been fetched disables lazy loading for FilterListElements</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterListElements
		{
			get { return _alreadyFetchedFilterListElements;}
			set 
			{
				if(_alreadyFetchedFilterListElements && !value && (_filterListElements != null))
				{
					_filterListElements.Clear();
				}
				_alreadyFetchedFilterListElements = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FilterValueSetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFilterValueSets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FilterValueSetCollection FilterValueSets
		{
			get	{ return GetMultiFilterValueSets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FilterValueSets. When set to true, FilterValueSets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FilterValueSets is accessed. You can always execute/ a forced fetch by calling GetMultiFilterValueSets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFilterValueSets
		{
			get	{ return _alwaysFetchFilterValueSets; }
			set	{ _alwaysFetchFilterValueSets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FilterValueSets already has been fetched. Setting this property to false when FilterValueSets has been fetched
		/// will clear the FilterValueSets collection well. Setting this property to true while FilterValueSets hasn't been fetched disables lazy loading for FilterValueSets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFilterValueSets
		{
			get { return _alreadyFetchedFilterValueSets;}
			set 
			{
				if(_alreadyFetchedFilterValueSets && !value && (_filterValueSets != null))
				{
					_filterValueSets.Clear();
				}
				_alreadyFetchedFilterValueSets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReports()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCollection Reports
		{
			get	{ return GetMultiReports(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Reports. When set to true, Reports is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Reports is accessed. You can always execute/ a forced fetch by calling GetMultiReports(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReports
		{
			get	{ return _alwaysFetchReports; }
			set	{ _alwaysFetchReports = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Reports already has been fetched. Setting this property to false when Reports has been fetched
		/// will clear the Reports collection well. Setting this property to true while Reports hasn't been fetched disables lazy loading for Reports</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReports
		{
			get { return _alreadyFetchedReports;}
			set 
			{
				if(_alreadyFetchedReports && !value && (_reports != null))
				{
					_reports.Clear();
				}
				_alreadyFetchedReports = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ReportCategoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiReportCategories()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ReportCategoryCollection ReportCategories
		{
			get	{ return GetMultiReportCategories(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ReportCategories. When set to true, ReportCategories is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportCategories is accessed. You can always execute/ a forced fetch by calling GetMultiReportCategories(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportCategories
		{
			get	{ return _alwaysFetchReportCategories; }
			set	{ _alwaysFetchReportCategories = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportCategories already has been fetched. Setting this property to false when ReportCategories has been fetched
		/// will clear the ReportCategories collection well. Setting this property to true while ReportCategories hasn't been fetched disables lazy loading for ReportCategories</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportCategories
		{
			get { return _alreadyFetchedReportCategories;}
			set 
			{
				if(_alreadyFetchedReportCategories && !value && (_reportCategories != null))
				{
					_reportCategories.Clear();
				}
				_alreadyFetchedReportCategories = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FilterListEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
