﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AccountingRecognizedTap'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class AccountingRecognizedTapEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AccountingRecognizedTapEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AccountingRecognizedTapEntity() :base("AccountingRecognizedTapEntity")
		{
			InitClassEmpty(null);
		}
		


		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccountingRecognizedTapEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}




				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AccountingRecognizedTapRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		



		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BoardingGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CancelledTap", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloseoutType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodFrom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodTo", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Premium", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionOperatorID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
		}
		#endregion

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. </summary>
		/// <returns>true</returns>
		public override bool Refetch()
		{
			return true;
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAccountingRecognizedTapDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AccountingRecognizedTapEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AccountingRecognizedTapRelations Relations
		{
			get	{ return new AccountingRecognizedTapRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Amount property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."AMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 Amount
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.Amount, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.Amount, value, true); }
		}

		/// <summary> The BaseFare property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."BASEFARE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> BaseFare
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.BaseFare, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.BaseFare, value, true); }
		}

		/// <summary> The BoardingGuid property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."BOARDINGGUID"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String BoardingGuid
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.BoardingGuid, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.BoardingGuid, value, true); }
		}

		/// <summary> The CancellationReference property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CANCELLATIONREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> CancellationReference
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.CancellationReference, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CancellationReference, value, true); }
		}

		/// <summary> The CancellationReferenceGuid property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CANCELLATIONREFERENCEGUID"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CancellationReferenceGuid
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.CancellationReferenceGuid, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CancellationReferenceGuid, value, true); }
		}

		/// <summary> The CancelledTap property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CANCELLEDTAP"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CancelledTap
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.CancelledTap, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CancelledTap, value, true); }
		}

		/// <summary> The ClientID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CLIENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ClientID
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.ClientID, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.ClientID, value, true); }
		}

		/// <summary> The CloseoutPeriodID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CLOSEOUTPERIODID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CloseoutPeriodID
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.CloseoutPeriodID, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CloseoutPeriodID, value, true); }
		}

		/// <summary> The CloseoutType property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CLOSEOUTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CloseoutType
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.CloseoutType, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CloseoutType, value, true); }
		}

		/// <summary> The CreditAccountNumber property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CREDITACCOUNTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CreditAccountNumber
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.CreditAccountNumber, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CreditAccountNumber, value, true); }
		}

		/// <summary> The CustomerGroup property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."CUSTOMERGROUP"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerGroup
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.CustomerGroup, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.CustomerGroup, value, true); }
		}

		/// <summary> The DebitAccountNumber property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."DEBITACCOUNTNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String DebitAccountNumber
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.DebitAccountNumber, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.DebitAccountNumber, value, true); }
		}

		/// <summary> The DeviceTime property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."DEVICETIME"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime DeviceTime
		{
			get { return (System.DateTime)GetValue((int)AccountingRecognizedTapFieldIndex.DeviceTime, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.DeviceTime, value, true); }
		}

		/// <summary> The FareAmount property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."FAREAMOUNT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FareAmount
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.FareAmount, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.FareAmount, value, true); }
		}

		/// <summary> The LastModified property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."LASTMODIFIED"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModified
		{
			get { return (System.DateTime)GetValue((int)AccountingRecognizedTapFieldIndex.LastModified, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.LastModified, value, true); }
		}

		/// <summary> The Line property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."LINE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Line
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.Line, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.Line, value, true); }
		}

		/// <summary> The LineGroupID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."LINEGROUPID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LineGroupID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.LineGroupID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.LineGroupID, value, true); }
		}

		/// <summary> The OperatorID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."OPERATORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OperatorID
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.OperatorID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.OperatorID, value, true); }
		}

		/// <summary> The PeriodFrom property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PERIODFROM"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodFrom
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingRecognizedTapFieldIndex.PeriodFrom, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PeriodFrom, value, true); }
		}

		/// <summary> The PeriodTo property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PERIODTO"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PeriodTo
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingRecognizedTapFieldIndex.PeriodTo, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PeriodTo, value, true); }
		}

		/// <summary> The PostingDate property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."POSTINGDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> PostingDate
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AccountingRecognizedTapFieldIndex.PostingDate, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PostingDate, value, true); }
		}

		/// <summary> The PostingReference property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."POSTINGREFERENCE"<br/>
		/// View field type characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PostingReference
		{
			get { return (System.String)GetValue((int)AccountingRecognizedTapFieldIndex.PostingReference, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PostingReference, value, true); }
		}

		/// <summary> The Premium property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PREMIUM"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Premium
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.Premium, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.Premium, value, true); }
		}

		/// <summary> The ProductID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PRODUCTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> ProductID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.ProductID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.ProductID, value, true); }
		}

		/// <summary> The PurseBalance property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PURSEBALANCE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseBalance
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.PurseBalance, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PurseBalance, value, true); }
		}

		/// <summary> The PurseCredit property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."PURSECREDIT"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PurseCredit
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.PurseCredit, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.PurseCredit, value, true); }
		}

		/// <summary> The ResultType property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."RESULTTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ResultType
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.ResultType, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.ResultType, value, true); }
		}

		/// <summary> The RevenueRecognitionID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."REVENUERECOGNITIONID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 RevenueRecognitionID
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.RevenueRecognitionID, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.RevenueRecognitionID, value, true); }
		}

		/// <summary> The RevenueSettlementID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."REVENUESETTLEMENTID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> RevenueSettlementID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.RevenueSettlementID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.RevenueSettlementID, value, true); }
		}

		/// <summary> The SalesChannelID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."SALESCHANNELID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SalesChannelID
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.SalesChannelID, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.SalesChannelID, value, true); }
		}

		/// <summary> The State property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."STATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 State
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.State, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.State, value, true); }
		}

		/// <summary> The TariffDate property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TARIFFDATE"<br/>
		/// View field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TariffDate
		{
			get { return (System.DateTime)GetValue((int)AccountingRecognizedTapFieldIndex.TariffDate, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TariffDate, value, true); }
		}

		/// <summary> The TariffVersion property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TARIFFVERSION"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TariffVersion
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.TariffVersion, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TariffVersion, value, true); }
		}

		/// <summary> The TicketID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TICKETID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TicketID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.TicketID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TicketInternalNumber property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.TicketInternalNumber, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TicketInternalNumber, value, true); }
		}

		/// <summary> The TicketNumber property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TICKETNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TicketNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.TicketNumber, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TicketNumber, value, true); }
		}

		/// <summary> The TransactionJournalID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TRANSACTIONJOURNALID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransactionJournalID
		{
			get { return (System.Int64)GetValue((int)AccountingRecognizedTapFieldIndex.TransactionJournalID, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TransactionJournalID, value, true); }
		}

		/// <summary> The TransactionOperatorID property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TRANSACTIONOPERATORID"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> TransactionOperatorID
		{
			get { return (Nullable<System.Int64>)GetValue((int)AccountingRecognizedTapFieldIndex.TransactionOperatorID, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TransactionOperatorID, value, true); }
		}

		/// <summary> The TransactionType property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TRANSACTIONTYPE"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TransactionType
		{
			get { return (System.Int32)GetValue((int)AccountingRecognizedTapFieldIndex.TransactionType, true); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TransactionType, value, true); }
		}

		/// <summary> The TripTicketInternalNumber property of the Entity AccountingRecognizedTap<br/><br/></summary>
		/// <remarks>Mapped on  view field: "ACC_RECOGNIZEDTAP"."TRIPTICKETINTERNALNUMBER"<br/>
		/// View field type characteristics (type, precision, scale, length): Decimal, 9, 0, 0<br/>
		/// View field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TripTicketInternalNumber
		{
			get { return (Nullable<System.Int32>)GetValue((int)AccountingRecognizedTapFieldIndex.TripTicketInternalNumber, false); }
			set	{ SetValue((int)AccountingRecognizedTapFieldIndex.TripTicketInternalNumber, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.AccountingRecognizedTapEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
