﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'FareTable'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class FareTableEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.FareTableCollection	_fareTables;
		private bool	_alwaysFetchFareTables, _alreadyFetchedFareTables;
		private VarioSL.Entities.CollectionClasses.FareTableEntryCollection	_fareTableEntries;
		private bool	_alwaysFetchFareTableEntries, _alreadyFetchedFareTableEntries;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsFareTable1;
		private bool	_alwaysFetchTicketsFareTable1, _alreadyFetchedTicketsFareTable1;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_ticketsFareTable2;
		private bool	_alwaysFetchTicketsFareTable2, _alreadyFetchedTicketsFareTable2;
		private ClientEntity _client;
		private bool	_alwaysFetchClient, _alreadyFetchedClient, _clientReturnsNewIfNotFound;
		private FareTableEntity _faretable;
		private bool	_alwaysFetchFaretable, _alreadyFetchedFaretable, _faretableReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Client</summary>
			public static readonly string Client = "Client";
			/// <summary>Member name Faretable</summary>
			public static readonly string Faretable = "Faretable";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name FareTables</summary>
			public static readonly string FareTables = "FareTables";
			/// <summary>Member name FareTableEntries</summary>
			public static readonly string FareTableEntries = "FareTableEntries";
			/// <summary>Member name TicketsFareTable1</summary>
			public static readonly string TicketsFareTable1 = "TicketsFareTable1";
			/// <summary>Member name TicketsFareTable2</summary>
			public static readonly string TicketsFareTable2 = "TicketsFareTable2";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static FareTableEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FareTableEntity() :base("FareTableEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		public FareTableEntity(System.Int64 fareTableID):base("FareTableEntity")
		{
			InitClassFetch(fareTableID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public FareTableEntity(System.Int64 fareTableID, IPrefetchPath prefetchPathToUse):base("FareTableEntity")
		{
			InitClassFetch(fareTableID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="validator">The custom validator object for this FareTableEntity</param>
		public FareTableEntity(System.Int64 fareTableID, IValidator validator):base("FareTableEntity")
		{
			InitClassFetch(fareTableID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FareTableEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_fareTables = (VarioSL.Entities.CollectionClasses.FareTableCollection)info.GetValue("_fareTables", typeof(VarioSL.Entities.CollectionClasses.FareTableCollection));
			_alwaysFetchFareTables = info.GetBoolean("_alwaysFetchFareTables");
			_alreadyFetchedFareTables = info.GetBoolean("_alreadyFetchedFareTables");

			_fareTableEntries = (VarioSL.Entities.CollectionClasses.FareTableEntryCollection)info.GetValue("_fareTableEntries", typeof(VarioSL.Entities.CollectionClasses.FareTableEntryCollection));
			_alwaysFetchFareTableEntries = info.GetBoolean("_alwaysFetchFareTableEntries");
			_alreadyFetchedFareTableEntries = info.GetBoolean("_alreadyFetchedFareTableEntries");

			_ticketsFareTable1 = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsFareTable1", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsFareTable1 = info.GetBoolean("_alwaysFetchTicketsFareTable1");
			_alreadyFetchedTicketsFareTable1 = info.GetBoolean("_alreadyFetchedTicketsFareTable1");

			_ticketsFareTable2 = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_ticketsFareTable2", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTicketsFareTable2 = info.GetBoolean("_alwaysFetchTicketsFareTable2");
			_alreadyFetchedTicketsFareTable2 = info.GetBoolean("_alreadyFetchedTicketsFareTable2");
			_client = (ClientEntity)info.GetValue("_client", typeof(ClientEntity));
			if(_client!=null)
			{
				_client.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientReturnsNewIfNotFound = info.GetBoolean("_clientReturnsNewIfNotFound");
			_alwaysFetchClient = info.GetBoolean("_alwaysFetchClient");
			_alreadyFetchedClient = info.GetBoolean("_alreadyFetchedClient");

			_faretable = (FareTableEntity)info.GetValue("_faretable", typeof(FareTableEntity));
			if(_faretable!=null)
			{
				_faretable.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_faretableReturnsNewIfNotFound = info.GetBoolean("_faretableReturnsNewIfNotFound");
			_alwaysFetchFaretable = info.GetBoolean("_alwaysFetchFaretable");
			_alreadyFetchedFaretable = info.GetBoolean("_alreadyFetchedFaretable");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((FareTableFieldIndex)fieldIndex)
			{
				case FareTableFieldIndex.MasterFareTableID:
					DesetupSyncFaretable(true, false);
					_alreadyFetchedFaretable = false;
					break;
				case FareTableFieldIndex.OwnerCLientID:
					DesetupSyncClient(true, false);
					_alreadyFetchedClient = false;
					break;
				case FareTableFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedFareTables = (_fareTables.Count > 0);
			_alreadyFetchedFareTableEntries = (_fareTableEntries.Count > 0);
			_alreadyFetchedTicketsFareTable1 = (_ticketsFareTable1.Count > 0);
			_alreadyFetchedTicketsFareTable2 = (_ticketsFareTable2.Count > 0);
			_alreadyFetchedClient = (_client != null);
			_alreadyFetchedFaretable = (_faretable != null);
			_alreadyFetchedTariff = (_tariff != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Client":
					toReturn.Add(Relations.ClientEntityUsingOwnerCLientID);
					break;
				case "Faretable":
					toReturn.Add(Relations.FareTableEntityUsingFareTableIDMasterFareTableID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "FareTables":
					toReturn.Add(Relations.FareTableEntityUsingMasterFareTableID);
					break;
				case "FareTableEntries":
					toReturn.Add(Relations.FareTableEntryEntityUsingFareTableID);
					break;
				case "TicketsFareTable1":
					toReturn.Add(Relations.TicketEntityUsingFareTableID);
					break;
				case "TicketsFareTable2":
					toReturn.Add(Relations.TicketEntityUsingFareTable2ID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_fareTables", (!this.MarkedForDeletion?_fareTables:null));
			info.AddValue("_alwaysFetchFareTables", _alwaysFetchFareTables);
			info.AddValue("_alreadyFetchedFareTables", _alreadyFetchedFareTables);
			info.AddValue("_fareTableEntries", (!this.MarkedForDeletion?_fareTableEntries:null));
			info.AddValue("_alwaysFetchFareTableEntries", _alwaysFetchFareTableEntries);
			info.AddValue("_alreadyFetchedFareTableEntries", _alreadyFetchedFareTableEntries);
			info.AddValue("_ticketsFareTable1", (!this.MarkedForDeletion?_ticketsFareTable1:null));
			info.AddValue("_alwaysFetchTicketsFareTable1", _alwaysFetchTicketsFareTable1);
			info.AddValue("_alreadyFetchedTicketsFareTable1", _alreadyFetchedTicketsFareTable1);
			info.AddValue("_ticketsFareTable2", (!this.MarkedForDeletion?_ticketsFareTable2:null));
			info.AddValue("_alwaysFetchTicketsFareTable2", _alwaysFetchTicketsFareTable2);
			info.AddValue("_alreadyFetchedTicketsFareTable2", _alreadyFetchedTicketsFareTable2);
			info.AddValue("_client", (!this.MarkedForDeletion?_client:null));
			info.AddValue("_clientReturnsNewIfNotFound", _clientReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClient", _alwaysFetchClient);
			info.AddValue("_alreadyFetchedClient", _alreadyFetchedClient);
			info.AddValue("_faretable", (!this.MarkedForDeletion?_faretable:null));
			info.AddValue("_faretableReturnsNewIfNotFound", _faretableReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchFaretable", _alwaysFetchFaretable);
			info.AddValue("_alreadyFetchedFaretable", _alreadyFetchedFaretable);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Client":
					_alreadyFetchedClient = true;
					this.Client = (ClientEntity)entity;
					break;
				case "Faretable":
					_alreadyFetchedFaretable = true;
					this.Faretable = (FareTableEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "FareTables":
					_alreadyFetchedFareTables = true;
					if(entity!=null)
					{
						this.FareTables.Add((FareTableEntity)entity);
					}
					break;
				case "FareTableEntries":
					_alreadyFetchedFareTableEntries = true;
					if(entity!=null)
					{
						this.FareTableEntries.Add((FareTableEntryEntity)entity);
					}
					break;
				case "TicketsFareTable1":
					_alreadyFetchedTicketsFareTable1 = true;
					if(entity!=null)
					{
						this.TicketsFareTable1.Add((TicketEntity)entity);
					}
					break;
				case "TicketsFareTable2":
					_alreadyFetchedTicketsFareTable2 = true;
					if(entity!=null)
					{
						this.TicketsFareTable2.Add((TicketEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Client":
					SetupSyncClient(relatedEntity);
					break;
				case "Faretable":
					SetupSyncFaretable(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "FareTables":
					_fareTables.Add((FareTableEntity)relatedEntity);
					break;
				case "FareTableEntries":
					_fareTableEntries.Add((FareTableEntryEntity)relatedEntity);
					break;
				case "TicketsFareTable1":
					_ticketsFareTable1.Add((TicketEntity)relatedEntity);
					break;
				case "TicketsFareTable2":
					_ticketsFareTable2.Add((TicketEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Client":
					DesetupSyncClient(false, true);
					break;
				case "Faretable":
					DesetupSyncFaretable(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "FareTables":
					this.PerformRelatedEntityRemoval(_fareTables, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "FareTableEntries":
					this.PerformRelatedEntityRemoval(_fareTableEntries, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsFareTable1":
					this.PerformRelatedEntityRemoval(_ticketsFareTable1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketsFareTable2":
					this.PerformRelatedEntityRemoval(_ticketsFareTable2, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_client!=null)
			{
				toReturn.Add(_client);
			}
			if(_faretable!=null)
			{
				toReturn.Add(_faretable);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_fareTables);
			toReturn.Add(_fareTableEntries);
			toReturn.Add(_ticketsFareTable1);
			toReturn.Add(_ticketsFareTable2);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableID)
		{
			return FetchUsingPK(fareTableID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(fareTableID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(fareTableID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 fareTableID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(fareTableID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.FareTableID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new FareTableRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTables(forceFetch, _fareTables.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTables(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection GetMultiFareTables(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTables || forceFetch || _alwaysFetchFareTables) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTables);
				_fareTables.SuppressClearInGetMulti=!forceFetch;
				_fareTables.EntityFactoryToUse = entityFactoryToUse;
				_fareTables.GetMultiManyToOne(null, this, null, filter);
				_fareTables.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTables = true;
			}
			return _fareTables;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTables'. These settings will be taken into account
		/// when the property FareTables is requested or GetMultiFareTables is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTables(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTables.SortClauses=sortClauses;
			_fareTables.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'FareTableEntryEntity'</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiFareTableEntries(forceFetch, _fareTableEntries.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiFareTableEntries(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection GetMultiFareTableEntries(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedFareTableEntries || forceFetch || _alwaysFetchFareTableEntries) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_fareTableEntries);
				_fareTableEntries.SuppressClearInGetMulti=!forceFetch;
				_fareTableEntries.EntityFactoryToUse = entityFactoryToUse;
				_fareTableEntries.GetMultiManyToOne(null, this, null, filter);
				_fareTableEntries.SuppressClearInGetMulti=false;
				_alreadyFetchedFareTableEntries = true;
			}
			return _fareTableEntries;
		}

		/// <summary> Sets the collection parameters for the collection for 'FareTableEntries'. These settings will be taken into account
		/// when the property FareTableEntries is requested or GetMultiFareTableEntries is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersFareTableEntries(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_fareTableEntries.SortClauses=sortClauses;
			_fareTableEntries.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable1(bool forceFetch)
		{
			return GetMultiTicketsFareTable1(forceFetch, _ticketsFareTable1.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable1(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsFareTable1(forceFetch, _ticketsFareTable1.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable1(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsFareTable1(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable1(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsFareTable1 || forceFetch || _alwaysFetchTicketsFareTable1) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsFareTable1);
				_ticketsFareTable1.SuppressClearInGetMulti=!forceFetch;
				_ticketsFareTable1.EntityFactoryToUse = entityFactoryToUse;
				_ticketsFareTable1.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsFareTable1.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsFareTable1 = true;
			}
			return _ticketsFareTable1;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsFareTable1'. These settings will be taken into account
		/// when the property TicketsFareTable1 is requested or GetMultiTicketsFareTable1 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsFareTable1(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsFareTable1.SortClauses=sortClauses;
			_ticketsFareTable1.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable2(bool forceFetch)
		{
			return GetMultiTicketsFareTable2(forceFetch, _ticketsFareTable2.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable2(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketsFareTable2(forceFetch, _ticketsFareTable2.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable2(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketsFareTable2(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTicketsFareTable2(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketsFareTable2 || forceFetch || _alwaysFetchTicketsFareTable2) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketsFareTable2);
				_ticketsFareTable2.SuppressClearInGetMulti=!forceFetch;
				_ticketsFareTable2.EntityFactoryToUse = entityFactoryToUse;
				_ticketsFareTable2.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketsFareTable2.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketsFareTable2 = true;
			}
			return _ticketsFareTable2;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketsFareTable2'. These settings will be taken into account
		/// when the property TicketsFareTable2 is requested or GetMultiTicketsFareTable2 is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketsFareTable2(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketsFareTable2.SortClauses=sortClauses;
			_ticketsFareTable2.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClient()
		{
			return GetSingleClient(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClient(bool forceFetch)
		{
			if( ( !_alreadyFetchedClient || forceFetch || _alwaysFetchClient) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingOwnerCLientID);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OwnerCLientID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Client = newEntity;
				_alreadyFetchedClient = fetchResult;
			}
			return _client;
		}


		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public FareTableEntity GetSingleFaretable()
		{
			return GetSingleFaretable(false);
		}

		/// <summary> Retrieves the related entity of type 'FareTableEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'FareTableEntity' which is related to this entity.</returns>
		public virtual FareTableEntity GetSingleFaretable(bool forceFetch)
		{
			if( ( !_alreadyFetchedFaretable || forceFetch || _alwaysFetchFaretable) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.FareTableEntityUsingFareTableIDMasterFareTableID);
				FareTableEntity newEntity = new FareTableEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MasterFareTableID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (FareTableEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_faretableReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Faretable = newEntity;
				_alreadyFetchedFaretable = fetchResult;
			}
			return _faretable;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Client", _client);
			toReturn.Add("Faretable", _faretable);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("FareTables", _fareTables);
			toReturn.Add("FareTableEntries", _fareTableEntries);
			toReturn.Add("TicketsFareTable1", _ticketsFareTable1);
			toReturn.Add("TicketsFareTable2", _ticketsFareTable2);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="validator">The validator object for this FareTableEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 fareTableID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(fareTableID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_fareTables = new VarioSL.Entities.CollectionClasses.FareTableCollection();
			_fareTables.SetContainingEntityInfo(this, "Faretable");

			_fareTableEntries = new VarioSL.Entities.CollectionClasses.FareTableEntryCollection();
			_fareTableEntries.SetContainingEntityInfo(this, "FareTable");

			_ticketsFareTable1 = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsFareTable1.SetContainingEntityInfo(this, "FareTable1");

			_ticketsFareTable2 = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_ticketsFareTable2.SetContainingEntityInfo(this, "FareTable2");
			_clientReturnsNewIfNotFound = false;
			_faretableReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareTableID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Formula", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MasterFareTableID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MinValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerCLientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _client</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClient(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareTableRelations.ClientEntityUsingOwnerCLientIDStatic, true, signalRelatedEntity, "FareTables", resetFKFields, new int[] { (int)FareTableFieldIndex.OwnerCLientID } );		
			_client = null;
		}
		
		/// <summary> setups the sync logic for member _client</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClient(IEntityCore relatedEntity)
		{
			if(_client!=relatedEntity)
			{		
				DesetupSyncClient(true, true);
				_client = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _client, new PropertyChangedEventHandler( OnClientPropertyChanged ), "Client", VarioSL.Entities.RelationClasses.StaticFareTableRelations.ClientEntityUsingOwnerCLientIDStatic, true, ref _alreadyFetchedClient, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _faretable</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncFaretable(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _faretable, new PropertyChangedEventHandler( OnFaretablePropertyChanged ), "Faretable", VarioSL.Entities.RelationClasses.StaticFareTableRelations.FareTableEntityUsingFareTableIDMasterFareTableIDStatic, true, signalRelatedEntity, "FareTables", resetFKFields, new int[] { (int)FareTableFieldIndex.MasterFareTableID } );		
			_faretable = null;
		}
		
		/// <summary> setups the sync logic for member _faretable</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncFaretable(IEntityCore relatedEntity)
		{
			if(_faretable!=relatedEntity)
			{		
				DesetupSyncFaretable(true, true);
				_faretable = (FareTableEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _faretable, new PropertyChangedEventHandler( OnFaretablePropertyChanged ), "Faretable", VarioSL.Entities.RelationClasses.StaticFareTableRelations.FareTableEntityUsingFareTableIDMasterFareTableIDStatic, true, ref _alreadyFetchedFaretable, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnFaretablePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareTableRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "FareTables", resetFKFields, new int[] { (int)FareTableFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticFareTableRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="fareTableID">PK value for FareTable which data should be fetched into this FareTable object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 fareTableID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)FareTableFieldIndex.FareTableID].ForcedCurrentValueWrite(fareTableID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateFareTableDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new FareTableEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static FareTableRelations Relations
		{
			get	{ return new FareTableRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTables
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("FareTables")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "FareTables", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTableEntry' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFareTableEntries
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableEntryCollection(), (IEntityRelation)GetRelationsForField("FareTableEntries")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.FareTableEntryEntity, 0, null, null, null, "FareTableEntries", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsFareTable1
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsFareTable1")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsFareTable1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketsFareTable2
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("TicketsFareTable2")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "TicketsFareTable2", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClient
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("Client")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.ClientEntity, 0, null, null, null, "Client", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'FareTable'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathFaretable
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.FareTableCollection(), (IEntityRelation)GetRelationsForField("Faretable")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.FareTableEntity, 0, null, null, null, "Faretable", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.FareTableEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Description property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)FareTableFieldIndex.Description, true); }
			set	{ SetValue((int)FareTableFieldIndex.Description, value, true); }
		}

		/// <summary> The FareTableID property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."FARETABLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 FareTableID
		{
			get { return (System.Int64)GetValue((int)FareTableFieldIndex.FareTableID, true); }
			set	{ SetValue((int)FareTableFieldIndex.FareTableID, value, true); }
		}

		/// <summary> The Formula property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."FORMULA"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Formula
		{
			get { return (System.String)GetValue((int)FareTableFieldIndex.Formula, true); }
			set	{ SetValue((int)FareTableFieldIndex.Formula, value, true); }
		}

		/// <summary> The MasterFareTableID property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."MASTERFARETABLEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MasterFareTableID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableFieldIndex.MasterFareTableID, false); }
			set	{ SetValue((int)FareTableFieldIndex.MasterFareTableID, value, true); }
		}

		/// <summary> The MaxValue property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."MAXVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MaxValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableFieldIndex.MaxValue, false); }
			set	{ SetValue((int)FareTableFieldIndex.MaxValue, value, true); }
		}

		/// <summary> The MinValue property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."MINVALUE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> MinValue
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableFieldIndex.MinValue, false); }
			set	{ SetValue((int)FareTableFieldIndex.MinValue, value, true); }
		}

		/// <summary> The Name property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)FareTableFieldIndex.Name, true); }
			set	{ SetValue((int)FareTableFieldIndex.Name, value, true); }
		}

		/// <summary> The OwnerCLientID property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> OwnerCLientID
		{
			get { return (Nullable<System.Int64>)GetValue((int)FareTableFieldIndex.OwnerCLientID, false); }
			set	{ SetValue((int)FareTableFieldIndex.OwnerCLientID, value, true); }
		}

		/// <summary> The TariffID property of the Entity FareTable<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_FARETABLE"."TARIFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)FareTableFieldIndex.TariffID, true); }
			set	{ SetValue((int)FareTableFieldIndex.TariffID, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'FareTableEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTables()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableCollection FareTables
		{
			get	{ return GetMultiFareTables(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTables. When set to true, FareTables is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTables is accessed. You can always execute/ a forced fetch by calling GetMultiFareTables(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTables
		{
			get	{ return _alwaysFetchFareTables; }
			set	{ _alwaysFetchFareTables = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTables already has been fetched. Setting this property to false when FareTables has been fetched
		/// will clear the FareTables collection well. Setting this property to true while FareTables hasn't been fetched disables lazy loading for FareTables</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTables
		{
			get { return _alreadyFetchedFareTables;}
			set 
			{
				if(_alreadyFetchedFareTables && !value && (_fareTables != null))
				{
					_fareTables.Clear();
				}
				_alreadyFetchedFareTables = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'FareTableEntryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiFareTableEntries()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.FareTableEntryCollection FareTableEntries
		{
			get	{ return GetMultiFareTableEntries(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for FareTableEntries. When set to true, FareTableEntries is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time FareTableEntries is accessed. You can always execute/ a forced fetch by calling GetMultiFareTableEntries(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFareTableEntries
		{
			get	{ return _alwaysFetchFareTableEntries; }
			set	{ _alwaysFetchFareTableEntries = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property FareTableEntries already has been fetched. Setting this property to false when FareTableEntries has been fetched
		/// will clear the FareTableEntries collection well. Setting this property to true while FareTableEntries hasn't been fetched disables lazy loading for FareTableEntries</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFareTableEntries
		{
			get { return _alreadyFetchedFareTableEntries;}
			set 
			{
				if(_alreadyFetchedFareTableEntries && !value && (_fareTableEntries != null))
				{
					_fareTableEntries.Clear();
				}
				_alreadyFetchedFareTableEntries = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsFareTable1()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsFareTable1
		{
			get	{ return GetMultiTicketsFareTable1(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsFareTable1. When set to true, TicketsFareTable1 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsFareTable1 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsFareTable1(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsFareTable1
		{
			get	{ return _alwaysFetchTicketsFareTable1; }
			set	{ _alwaysFetchTicketsFareTable1 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsFareTable1 already has been fetched. Setting this property to false when TicketsFareTable1 has been fetched
		/// will clear the TicketsFareTable1 collection well. Setting this property to true while TicketsFareTable1 hasn't been fetched disables lazy loading for TicketsFareTable1</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsFareTable1
		{
			get { return _alreadyFetchedTicketsFareTable1;}
			set 
			{
				if(_alreadyFetchedTicketsFareTable1 && !value && (_ticketsFareTable1 != null))
				{
					_ticketsFareTable1.Clear();
				}
				_alreadyFetchedTicketsFareTable1 = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketsFareTable2()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection TicketsFareTable2
		{
			get	{ return GetMultiTicketsFareTable2(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketsFareTable2. When set to true, TicketsFareTable2 is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketsFareTable2 is accessed. You can always execute/ a forced fetch by calling GetMultiTicketsFareTable2(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketsFareTable2
		{
			get	{ return _alwaysFetchTicketsFareTable2; }
			set	{ _alwaysFetchTicketsFareTable2 = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketsFareTable2 already has been fetched. Setting this property to false when TicketsFareTable2 has been fetched
		/// will clear the TicketsFareTable2 collection well. Setting this property to true while TicketsFareTable2 hasn't been fetched disables lazy loading for TicketsFareTable2</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketsFareTable2
		{
			get { return _alreadyFetchedTicketsFareTable2;}
			set 
			{
				if(_alreadyFetchedTicketsFareTable2 && !value && (_ticketsFareTable2 != null))
				{
					_ticketsFareTable2.Clear();
				}
				_alreadyFetchedTicketsFareTable2 = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual ClientEntity Client
		{
			get	{ return GetSingleClient(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClient(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTables", "Client", _client, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Client. When set to true, Client is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Client is accessed. You can always execute a forced fetch by calling GetSingleClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClient
		{
			get	{ return _alwaysFetchClient; }
			set	{ _alwaysFetchClient = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Client already has been fetched. Setting this property to false when Client has been fetched
		/// will set Client to null as well. Setting this property to true while Client hasn't been fetched disables lazy loading for Client</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClient
		{
			get { return _alreadyFetchedClient;}
			set 
			{
				if(_alreadyFetchedClient && !value)
				{
					this.Client = null;
				}
				_alreadyFetchedClient = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Client is not found
		/// in the database. When set to true, Client will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool ClientReturnsNewIfNotFound
		{
			get	{ return _clientReturnsNewIfNotFound; }
			set { _clientReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'FareTableEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleFaretable()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual FareTableEntity Faretable
		{
			get	{ return GetSingleFaretable(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncFaretable(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTables", "Faretable", _faretable, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Faretable. When set to true, Faretable is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Faretable is accessed. You can always execute a forced fetch by calling GetSingleFaretable(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchFaretable
		{
			get	{ return _alwaysFetchFaretable; }
			set	{ _alwaysFetchFaretable = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Faretable already has been fetched. Setting this property to false when Faretable has been fetched
		/// will set Faretable to null as well. Setting this property to true while Faretable hasn't been fetched disables lazy loading for Faretable</summary>
		[Browsable(false)]
		public bool AlreadyFetchedFaretable
		{
			get { return _alreadyFetchedFaretable;}
			set 
			{
				if(_alreadyFetchedFaretable && !value)
				{
					this.Faretable = null;
				}
				_alreadyFetchedFaretable = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Faretable is not found
		/// in the database. When set to true, Faretable will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool FaretableReturnsNewIfNotFound
		{
			get	{ return _faretableReturnsNewIfNotFound; }
			set { _faretableReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "FareTables", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.FareTableEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
