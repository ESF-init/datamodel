﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceClass'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class DeviceClassEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private VarioSL.Entities.CollectionClasses.ClearingResultCollection	_clearingResults;
		private bool	_alwaysFetchClearingResults, _alreadyFetchedClearingResults;
		private VarioSL.Entities.CollectionClasses.DeviceCollection	_umDevices;
		private bool	_alwaysFetchUmDevices, _alreadyFetchedUmDevices;
		private VarioSL.Entities.CollectionClasses.GuiDefCollection	_guiDefs;
		private bool	_alwaysFetchGuiDefs, _alreadyFetchedGuiDefs;
		private VarioSL.Entities.CollectionClasses.PanelCollection	_panels;
		private bool	_alwaysFetchPanels, _alreadyFetchedPanels;
		private VarioSL.Entities.CollectionClasses.SpecialReceiptCollection	_specialReceipts;
		private bool	_alwaysFetchSpecialReceipts, _alreadyFetchedSpecialReceipts;
		private VarioSL.Entities.CollectionClasses.TariffCollection	_tariffs;
		private bool	_alwaysFetchTariffs, _alreadyFetchedTariffs;
		private VarioSL.Entities.CollectionClasses.TariffReleaseCollection	_tariffRelease;
		private bool	_alwaysFetchTariffRelease, _alreadyFetchedTariffRelease;
		private VarioSL.Entities.CollectionClasses.TicketCollection	_tickets;
		private bool	_alwaysFetchTickets, _alreadyFetchedTickets;
		private VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection	_ticketDeviceClasses;
		private bool	_alwaysFetchTicketDeviceClasses, _alreadyFetchedTicketDeviceClasses;
		private VarioSL.Entities.CollectionClasses.ParameterCollection	_parameters;
		private bool	_alwaysFetchParameters, _alreadyFetchedParameters;
		private VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection	_parameterArchiveReleases;
		private bool	_alwaysFetchParameterArchiveReleases, _alreadyFetchedParameterArchiveReleases;
		private VarioSL.Entities.CollectionClasses.ParameterValueCollection	_parameterValues;
		private bool	_alwaysFetchParameterValues, _alreadyFetchedParameterValues;
		private VarioSL.Entities.CollectionClasses.SaleCollection	_sales;
		private bool	_alwaysFetchSales, _alreadyFetchedSales;
		private VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection	_salesChannelToPaymentMethods;
		private bool	_alwaysFetchSalesChannelToPaymentMethods, _alreadyFetchedSalesChannelToPaymentMethods;
		private VarioSL.Entities.CollectionClasses.TransactionJournalCollection	_transactionJournals;
		private bool	_alwaysFetchTransactionJournals, _alreadyFetchedTransactionJournals;
		private VarioSL.Entities.CollectionClasses.UnitCollectionCollection	_unitCollections;
		private bool	_alwaysFetchUnitCollections, _alreadyFetchedUnitCollections;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClearingResults</summary>
			public static readonly string ClearingResults = "ClearingResults";
			/// <summary>Member name UmDevices</summary>
			public static readonly string UmDevices = "UmDevices";
			/// <summary>Member name GuiDefs</summary>
			public static readonly string GuiDefs = "GuiDefs";
			/// <summary>Member name Panels</summary>
			public static readonly string Panels = "Panels";
			/// <summary>Member name SpecialReceipts</summary>
			public static readonly string SpecialReceipts = "SpecialReceipts";
			/// <summary>Member name Tariffs</summary>
			public static readonly string Tariffs = "Tariffs";
			/// <summary>Member name TariffRelease</summary>
			public static readonly string TariffRelease = "TariffRelease";
			/// <summary>Member name Tickets</summary>
			public static readonly string Tickets = "Tickets";
			/// <summary>Member name TicketDeviceClasses</summary>
			public static readonly string TicketDeviceClasses = "TicketDeviceClasses";
			/// <summary>Member name Parameters</summary>
			public static readonly string Parameters = "Parameters";
			/// <summary>Member name ParameterArchiveReleases</summary>
			public static readonly string ParameterArchiveReleases = "ParameterArchiveReleases";
			/// <summary>Member name ParameterValues</summary>
			public static readonly string ParameterValues = "ParameterValues";
			/// <summary>Member name Sales</summary>
			public static readonly string Sales = "Sales";
			/// <summary>Member name SalesChannelToPaymentMethods</summary>
			public static readonly string SalesChannelToPaymentMethods = "SalesChannelToPaymentMethods";
			/// <summary>Member name TransactionJournals</summary>
			public static readonly string TransactionJournals = "TransactionJournals";
			/// <summary>Member name UnitCollections</summary>
			public static readonly string UnitCollections = "UnitCollections";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceClassEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DeviceClassEntity() :base("DeviceClassEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		public DeviceClassEntity(System.Int64 deviceClassID):base("DeviceClassEntity")
		{
			InitClassFetch(deviceClassID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceClassEntity(System.Int64 deviceClassID, IPrefetchPath prefetchPathToUse):base("DeviceClassEntity")
		{
			InitClassFetch(deviceClassID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="validator">The custom validator object for this DeviceClassEntity</param>
		public DeviceClassEntity(System.Int64 deviceClassID, IValidator validator):base("DeviceClassEntity")
		{
			InitClassFetch(deviceClassID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceClassEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clearingResults = (VarioSL.Entities.CollectionClasses.ClearingResultCollection)info.GetValue("_clearingResults", typeof(VarioSL.Entities.CollectionClasses.ClearingResultCollection));
			_alwaysFetchClearingResults = info.GetBoolean("_alwaysFetchClearingResults");
			_alreadyFetchedClearingResults = info.GetBoolean("_alreadyFetchedClearingResults");

			_umDevices = (VarioSL.Entities.CollectionClasses.DeviceCollection)info.GetValue("_umDevices", typeof(VarioSL.Entities.CollectionClasses.DeviceCollection));
			_alwaysFetchUmDevices = info.GetBoolean("_alwaysFetchUmDevices");
			_alreadyFetchedUmDevices = info.GetBoolean("_alreadyFetchedUmDevices");

			_guiDefs = (VarioSL.Entities.CollectionClasses.GuiDefCollection)info.GetValue("_guiDefs", typeof(VarioSL.Entities.CollectionClasses.GuiDefCollection));
			_alwaysFetchGuiDefs = info.GetBoolean("_alwaysFetchGuiDefs");
			_alreadyFetchedGuiDefs = info.GetBoolean("_alreadyFetchedGuiDefs");

			_panels = (VarioSL.Entities.CollectionClasses.PanelCollection)info.GetValue("_panels", typeof(VarioSL.Entities.CollectionClasses.PanelCollection));
			_alwaysFetchPanels = info.GetBoolean("_alwaysFetchPanels");
			_alreadyFetchedPanels = info.GetBoolean("_alreadyFetchedPanels");

			_specialReceipts = (VarioSL.Entities.CollectionClasses.SpecialReceiptCollection)info.GetValue("_specialReceipts", typeof(VarioSL.Entities.CollectionClasses.SpecialReceiptCollection));
			_alwaysFetchSpecialReceipts = info.GetBoolean("_alwaysFetchSpecialReceipts");
			_alreadyFetchedSpecialReceipts = info.GetBoolean("_alreadyFetchedSpecialReceipts");

			_tariffs = (VarioSL.Entities.CollectionClasses.TariffCollection)info.GetValue("_tariffs", typeof(VarioSL.Entities.CollectionClasses.TariffCollection));
			_alwaysFetchTariffs = info.GetBoolean("_alwaysFetchTariffs");
			_alreadyFetchedTariffs = info.GetBoolean("_alreadyFetchedTariffs");

			_tariffRelease = (VarioSL.Entities.CollectionClasses.TariffReleaseCollection)info.GetValue("_tariffRelease", typeof(VarioSL.Entities.CollectionClasses.TariffReleaseCollection));
			_alwaysFetchTariffRelease = info.GetBoolean("_alwaysFetchTariffRelease");
			_alreadyFetchedTariffRelease = info.GetBoolean("_alreadyFetchedTariffRelease");

			_tickets = (VarioSL.Entities.CollectionClasses.TicketCollection)info.GetValue("_tickets", typeof(VarioSL.Entities.CollectionClasses.TicketCollection));
			_alwaysFetchTickets = info.GetBoolean("_alwaysFetchTickets");
			_alreadyFetchedTickets = info.GetBoolean("_alreadyFetchedTickets");

			_ticketDeviceClasses = (VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection)info.GetValue("_ticketDeviceClasses", typeof(VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection));
			_alwaysFetchTicketDeviceClasses = info.GetBoolean("_alwaysFetchTicketDeviceClasses");
			_alreadyFetchedTicketDeviceClasses = info.GetBoolean("_alreadyFetchedTicketDeviceClasses");

			_parameters = (VarioSL.Entities.CollectionClasses.ParameterCollection)info.GetValue("_parameters", typeof(VarioSL.Entities.CollectionClasses.ParameterCollection));
			_alwaysFetchParameters = info.GetBoolean("_alwaysFetchParameters");
			_alreadyFetchedParameters = info.GetBoolean("_alreadyFetchedParameters");

			_parameterArchiveReleases = (VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection)info.GetValue("_parameterArchiveReleases", typeof(VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection));
			_alwaysFetchParameterArchiveReleases = info.GetBoolean("_alwaysFetchParameterArchiveReleases");
			_alreadyFetchedParameterArchiveReleases = info.GetBoolean("_alreadyFetchedParameterArchiveReleases");

			_parameterValues = (VarioSL.Entities.CollectionClasses.ParameterValueCollection)info.GetValue("_parameterValues", typeof(VarioSL.Entities.CollectionClasses.ParameterValueCollection));
			_alwaysFetchParameterValues = info.GetBoolean("_alwaysFetchParameterValues");
			_alreadyFetchedParameterValues = info.GetBoolean("_alreadyFetchedParameterValues");

			_sales = (VarioSL.Entities.CollectionClasses.SaleCollection)info.GetValue("_sales", typeof(VarioSL.Entities.CollectionClasses.SaleCollection));
			_alwaysFetchSales = info.GetBoolean("_alwaysFetchSales");
			_alreadyFetchedSales = info.GetBoolean("_alreadyFetchedSales");

			_salesChannelToPaymentMethods = (VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection)info.GetValue("_salesChannelToPaymentMethods", typeof(VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection));
			_alwaysFetchSalesChannelToPaymentMethods = info.GetBoolean("_alwaysFetchSalesChannelToPaymentMethods");
			_alreadyFetchedSalesChannelToPaymentMethods = info.GetBoolean("_alreadyFetchedSalesChannelToPaymentMethods");

			_transactionJournals = (VarioSL.Entities.CollectionClasses.TransactionJournalCollection)info.GetValue("_transactionJournals", typeof(VarioSL.Entities.CollectionClasses.TransactionJournalCollection));
			_alwaysFetchTransactionJournals = info.GetBoolean("_alwaysFetchTransactionJournals");
			_alreadyFetchedTransactionJournals = info.GetBoolean("_alreadyFetchedTransactionJournals");

			_unitCollections = (VarioSL.Entities.CollectionClasses.UnitCollectionCollection)info.GetValue("_unitCollections", typeof(VarioSL.Entities.CollectionClasses.UnitCollectionCollection));
			_alwaysFetchUnitCollections = info.GetBoolean("_alwaysFetchUnitCollections");
			_alreadyFetchedUnitCollections = info.GetBoolean("_alreadyFetchedUnitCollections");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClearingResults = (_clearingResults.Count > 0);
			_alreadyFetchedUmDevices = (_umDevices.Count > 0);
			_alreadyFetchedGuiDefs = (_guiDefs.Count > 0);
			_alreadyFetchedPanels = (_panels.Count > 0);
			_alreadyFetchedSpecialReceipts = (_specialReceipts.Count > 0);
			_alreadyFetchedTariffs = (_tariffs.Count > 0);
			_alreadyFetchedTariffRelease = (_tariffRelease.Count > 0);
			_alreadyFetchedTickets = (_tickets.Count > 0);
			_alreadyFetchedTicketDeviceClasses = (_ticketDeviceClasses.Count > 0);
			_alreadyFetchedParameters = (_parameters.Count > 0);
			_alreadyFetchedParameterArchiveReleases = (_parameterArchiveReleases.Count > 0);
			_alreadyFetchedParameterValues = (_parameterValues.Count > 0);
			_alreadyFetchedSales = (_sales.Count > 0);
			_alreadyFetchedSalesChannelToPaymentMethods = (_salesChannelToPaymentMethods.Count > 0);
			_alreadyFetchedTransactionJournals = (_transactionJournals.Count > 0);
			_alreadyFetchedUnitCollections = (_unitCollections.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClearingResults":
					toReturn.Add(Relations.ClearingResultEntityUsingDeviceClassID);
					break;
				case "UmDevices":
					toReturn.Add(Relations.DeviceEntityUsingDeviceClassID);
					break;
				case "GuiDefs":
					toReturn.Add(Relations.GuiDefEntityUsingDeviceClassID);
					break;
				case "Panels":
					toReturn.Add(Relations.PanelEntityUsingDeviceClassID);
					break;
				case "SpecialReceipts":
					toReturn.Add(Relations.SpecialReceiptEntityUsingDeviceClassID);
					break;
				case "Tariffs":
					toReturn.Add(Relations.TariffEntityUsingDeviceClassID);
					break;
				case "TariffRelease":
					toReturn.Add(Relations.TariffReleaseEntityUsingDeviceClassID);
					break;
				case "Tickets":
					toReturn.Add(Relations.TicketEntityUsingDeviceClassID);
					break;
				case "TicketDeviceClasses":
					toReturn.Add(Relations.TicketDeviceClassEntityUsingDeviceClassID);
					break;
				case "Parameters":
					toReturn.Add(Relations.ParameterEntityUsingDeviceClassID);
					break;
				case "ParameterArchiveReleases":
					toReturn.Add(Relations.ParameterArchiveReleaseEntityUsingDeviceClassID);
					break;
				case "ParameterValues":
					toReturn.Add(Relations.ParameterValueEntityUsingDeviceClassID);
					break;
				case "Sales":
					toReturn.Add(Relations.SaleEntityUsingSalesChannelID);
					break;
				case "SalesChannelToPaymentMethods":
					toReturn.Add(Relations.SalesChannelToPaymentMethodEntityUsingDeviceClassID);
					break;
				case "TransactionJournals":
					toReturn.Add(Relations.TransactionJournalEntityUsingSalesChannelID);
					break;
				case "UnitCollections":
					toReturn.Add(Relations.UnitCollectionEntityUsingDeviceClassID);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clearingResults", (!this.MarkedForDeletion?_clearingResults:null));
			info.AddValue("_alwaysFetchClearingResults", _alwaysFetchClearingResults);
			info.AddValue("_alreadyFetchedClearingResults", _alreadyFetchedClearingResults);
			info.AddValue("_umDevices", (!this.MarkedForDeletion?_umDevices:null));
			info.AddValue("_alwaysFetchUmDevices", _alwaysFetchUmDevices);
			info.AddValue("_alreadyFetchedUmDevices", _alreadyFetchedUmDevices);
			info.AddValue("_guiDefs", (!this.MarkedForDeletion?_guiDefs:null));
			info.AddValue("_alwaysFetchGuiDefs", _alwaysFetchGuiDefs);
			info.AddValue("_alreadyFetchedGuiDefs", _alreadyFetchedGuiDefs);
			info.AddValue("_panels", (!this.MarkedForDeletion?_panels:null));
			info.AddValue("_alwaysFetchPanels", _alwaysFetchPanels);
			info.AddValue("_alreadyFetchedPanels", _alreadyFetchedPanels);
			info.AddValue("_specialReceipts", (!this.MarkedForDeletion?_specialReceipts:null));
			info.AddValue("_alwaysFetchSpecialReceipts", _alwaysFetchSpecialReceipts);
			info.AddValue("_alreadyFetchedSpecialReceipts", _alreadyFetchedSpecialReceipts);
			info.AddValue("_tariffs", (!this.MarkedForDeletion?_tariffs:null));
			info.AddValue("_alwaysFetchTariffs", _alwaysFetchTariffs);
			info.AddValue("_alreadyFetchedTariffs", _alreadyFetchedTariffs);
			info.AddValue("_tariffRelease", (!this.MarkedForDeletion?_tariffRelease:null));
			info.AddValue("_alwaysFetchTariffRelease", _alwaysFetchTariffRelease);
			info.AddValue("_alreadyFetchedTariffRelease", _alreadyFetchedTariffRelease);
			info.AddValue("_tickets", (!this.MarkedForDeletion?_tickets:null));
			info.AddValue("_alwaysFetchTickets", _alwaysFetchTickets);
			info.AddValue("_alreadyFetchedTickets", _alreadyFetchedTickets);
			info.AddValue("_ticketDeviceClasses", (!this.MarkedForDeletion?_ticketDeviceClasses:null));
			info.AddValue("_alwaysFetchTicketDeviceClasses", _alwaysFetchTicketDeviceClasses);
			info.AddValue("_alreadyFetchedTicketDeviceClasses", _alreadyFetchedTicketDeviceClasses);
			info.AddValue("_parameters", (!this.MarkedForDeletion?_parameters:null));
			info.AddValue("_alwaysFetchParameters", _alwaysFetchParameters);
			info.AddValue("_alreadyFetchedParameters", _alreadyFetchedParameters);
			info.AddValue("_parameterArchiveReleases", (!this.MarkedForDeletion?_parameterArchiveReleases:null));
			info.AddValue("_alwaysFetchParameterArchiveReleases", _alwaysFetchParameterArchiveReleases);
			info.AddValue("_alreadyFetchedParameterArchiveReleases", _alreadyFetchedParameterArchiveReleases);
			info.AddValue("_parameterValues", (!this.MarkedForDeletion?_parameterValues:null));
			info.AddValue("_alwaysFetchParameterValues", _alwaysFetchParameterValues);
			info.AddValue("_alreadyFetchedParameterValues", _alreadyFetchedParameterValues);
			info.AddValue("_sales", (!this.MarkedForDeletion?_sales:null));
			info.AddValue("_alwaysFetchSales", _alwaysFetchSales);
			info.AddValue("_alreadyFetchedSales", _alreadyFetchedSales);
			info.AddValue("_salesChannelToPaymentMethods", (!this.MarkedForDeletion?_salesChannelToPaymentMethods:null));
			info.AddValue("_alwaysFetchSalesChannelToPaymentMethods", _alwaysFetchSalesChannelToPaymentMethods);
			info.AddValue("_alreadyFetchedSalesChannelToPaymentMethods", _alreadyFetchedSalesChannelToPaymentMethods);
			info.AddValue("_transactionJournals", (!this.MarkedForDeletion?_transactionJournals:null));
			info.AddValue("_alwaysFetchTransactionJournals", _alwaysFetchTransactionJournals);
			info.AddValue("_alreadyFetchedTransactionJournals", _alreadyFetchedTransactionJournals);
			info.AddValue("_unitCollections", (!this.MarkedForDeletion?_unitCollections:null));
			info.AddValue("_alwaysFetchUnitCollections", _alwaysFetchUnitCollections);
			info.AddValue("_alreadyFetchedUnitCollections", _alreadyFetchedUnitCollections);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClearingResults":
					_alreadyFetchedClearingResults = true;
					if(entity!=null)
					{
						this.ClearingResults.Add((ClearingResultEntity)entity);
					}
					break;
				case "UmDevices":
					_alreadyFetchedUmDevices = true;
					if(entity!=null)
					{
						this.UmDevices.Add((DeviceEntity)entity);
					}
					break;
				case "GuiDefs":
					_alreadyFetchedGuiDefs = true;
					if(entity!=null)
					{
						this.GuiDefs.Add((GuiDefEntity)entity);
					}
					break;
				case "Panels":
					_alreadyFetchedPanels = true;
					if(entity!=null)
					{
						this.Panels.Add((PanelEntity)entity);
					}
					break;
				case "SpecialReceipts":
					_alreadyFetchedSpecialReceipts = true;
					if(entity!=null)
					{
						this.SpecialReceipts.Add((SpecialReceiptEntity)entity);
					}
					break;
				case "Tariffs":
					_alreadyFetchedTariffs = true;
					if(entity!=null)
					{
						this.Tariffs.Add((TariffEntity)entity);
					}
					break;
				case "TariffRelease":
					_alreadyFetchedTariffRelease = true;
					if(entity!=null)
					{
						this.TariffRelease.Add((TariffReleaseEntity)entity);
					}
					break;
				case "Tickets":
					_alreadyFetchedTickets = true;
					if(entity!=null)
					{
						this.Tickets.Add((TicketEntity)entity);
					}
					break;
				case "TicketDeviceClasses":
					_alreadyFetchedTicketDeviceClasses = true;
					if(entity!=null)
					{
						this.TicketDeviceClasses.Add((TicketDeviceClassEntity)entity);
					}
					break;
				case "Parameters":
					_alreadyFetchedParameters = true;
					if(entity!=null)
					{
						this.Parameters.Add((ParameterEntity)entity);
					}
					break;
				case "ParameterArchiveReleases":
					_alreadyFetchedParameterArchiveReleases = true;
					if(entity!=null)
					{
						this.ParameterArchiveReleases.Add((ParameterArchiveReleaseEntity)entity);
					}
					break;
				case "ParameterValues":
					_alreadyFetchedParameterValues = true;
					if(entity!=null)
					{
						this.ParameterValues.Add((ParameterValueEntity)entity);
					}
					break;
				case "Sales":
					_alreadyFetchedSales = true;
					if(entity!=null)
					{
						this.Sales.Add((SaleEntity)entity);
					}
					break;
				case "SalesChannelToPaymentMethods":
					_alreadyFetchedSalesChannelToPaymentMethods = true;
					if(entity!=null)
					{
						this.SalesChannelToPaymentMethods.Add((SalesChannelToPaymentMethodEntity)entity);
					}
					break;
				case "TransactionJournals":
					_alreadyFetchedTransactionJournals = true;
					if(entity!=null)
					{
						this.TransactionJournals.Add((TransactionJournalEntity)entity);
					}
					break;
				case "UnitCollections":
					_alreadyFetchedUnitCollections = true;
					if(entity!=null)
					{
						this.UnitCollections.Add((UnitCollectionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClearingResults":
					_clearingResults.Add((ClearingResultEntity)relatedEntity);
					break;
				case "UmDevices":
					_umDevices.Add((DeviceEntity)relatedEntity);
					break;
				case "GuiDefs":
					_guiDefs.Add((GuiDefEntity)relatedEntity);
					break;
				case "Panels":
					_panels.Add((PanelEntity)relatedEntity);
					break;
				case "SpecialReceipts":
					_specialReceipts.Add((SpecialReceiptEntity)relatedEntity);
					break;
				case "Tariffs":
					_tariffs.Add((TariffEntity)relatedEntity);
					break;
				case "TariffRelease":
					_tariffRelease.Add((TariffReleaseEntity)relatedEntity);
					break;
				case "Tickets":
					_tickets.Add((TicketEntity)relatedEntity);
					break;
				case "TicketDeviceClasses":
					_ticketDeviceClasses.Add((TicketDeviceClassEntity)relatedEntity);
					break;
				case "Parameters":
					_parameters.Add((ParameterEntity)relatedEntity);
					break;
				case "ParameterArchiveReleases":
					_parameterArchiveReleases.Add((ParameterArchiveReleaseEntity)relatedEntity);
					break;
				case "ParameterValues":
					_parameterValues.Add((ParameterValueEntity)relatedEntity);
					break;
				case "Sales":
					_sales.Add((SaleEntity)relatedEntity);
					break;
				case "SalesChannelToPaymentMethods":
					_salesChannelToPaymentMethods.Add((SalesChannelToPaymentMethodEntity)relatedEntity);
					break;
				case "TransactionJournals":
					_transactionJournals.Add((TransactionJournalEntity)relatedEntity);
					break;
				case "UnitCollections":
					_unitCollections.Add((UnitCollectionEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClearingResults":
					this.PerformRelatedEntityRemoval(_clearingResults, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UmDevices":
					this.PerformRelatedEntityRemoval(_umDevices, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GuiDefs":
					this.PerformRelatedEntityRemoval(_guiDefs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Panels":
					this.PerformRelatedEntityRemoval(_panels, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SpecialReceipts":
					this.PerformRelatedEntityRemoval(_specialReceipts, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tariffs":
					this.PerformRelatedEntityRemoval(_tariffs, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TariffRelease":
					this.PerformRelatedEntityRemoval(_tariffRelease, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Tickets":
					this.PerformRelatedEntityRemoval(_tickets, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TicketDeviceClasses":
					this.PerformRelatedEntityRemoval(_ticketDeviceClasses, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Parameters":
					this.PerformRelatedEntityRemoval(_parameters, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterArchiveReleases":
					this.PerformRelatedEntityRemoval(_parameterArchiveReleases, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ParameterValues":
					this.PerformRelatedEntityRemoval(_parameterValues, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Sales":
					this.PerformRelatedEntityRemoval(_sales, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SalesChannelToPaymentMethods":
					this.PerformRelatedEntityRemoval(_salesChannelToPaymentMethods, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TransactionJournals":
					this.PerformRelatedEntityRemoval(_transactionJournals, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UnitCollections":
					this.PerformRelatedEntityRemoval(_unitCollections, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clearingResults);
			toReturn.Add(_umDevices);
			toReturn.Add(_guiDefs);
			toReturn.Add(_panels);
			toReturn.Add(_specialReceipts);
			toReturn.Add(_tariffs);
			toReturn.Add(_tariffRelease);
			toReturn.Add(_tickets);
			toReturn.Add(_ticketDeviceClasses);
			toReturn.Add(_parameters);
			toReturn.Add(_parameterArchiveReleases);
			toReturn.Add(_parameterValues);
			toReturn.Add(_sales);
			toReturn.Add(_salesChannelToPaymentMethods);
			toReturn.Add(_transactionJournals);
			toReturn.Add(_unitCollections);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID)
		{
			return FetchUsingPK(deviceClassID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceClassID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceClassID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 deviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceClassID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceClassID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceClassRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClearingResultEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClearingResults(forceFetch, _clearingResults.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClearingResults(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection GetMultiClearingResults(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClearingResults || forceFetch || _alwaysFetchClearingResults) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clearingResults);
				_clearingResults.SuppressClearInGetMulti=!forceFetch;
				_clearingResults.EntityFactoryToUse = entityFactoryToUse;
				_clearingResults.GetMultiManyToOne(null, null, null, null, null, this, null, null, filter);
				_clearingResults.SuppressClearInGetMulti=false;
				_alreadyFetchedClearingResults = true;
			}
			return _clearingResults;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClearingResults'. These settings will be taken into account
		/// when the property ClearingResults is requested or GetMultiClearingResults is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClearingResults(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clearingResults.SortClauses=sortClauses;
			_clearingResults.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiUmDevices(bool forceFetch)
		{
			return GetMultiUmDevices(forceFetch, _umDevices.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceEntity'</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiUmDevices(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUmDevices(forceFetch, _umDevices.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiUmDevices(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUmDevices(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCollection GetMultiUmDevices(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUmDevices || forceFetch || _alwaysFetchUmDevices) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_umDevices);
				_umDevices.SuppressClearInGetMulti=!forceFetch;
				_umDevices.EntityFactoryToUse = entityFactoryToUse;
				_umDevices.GetMultiManyToOne(this, null, filter);
				_umDevices.SuppressClearInGetMulti=false;
				_alreadyFetchedUmDevices = true;
			}
			return _umDevices;
		}

		/// <summary> Sets the collection parameters for the collection for 'UmDevices'. These settings will be taken into account
		/// when the property UmDevices is requested or GetMultiUmDevices is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUmDevices(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_umDevices.SortClauses=sortClauses;
			_umDevices.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GuiDefEntity'</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGuiDefs(forceFetch, _guiDefs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGuiDefs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GetMultiGuiDefs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGuiDefs || forceFetch || _alwaysFetchGuiDefs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_guiDefs);
				_guiDefs.SuppressClearInGetMulti=!forceFetch;
				_guiDefs.EntityFactoryToUse = entityFactoryToUse;
				_guiDefs.GetMultiManyToOne(this, null, null, filter);
				_guiDefs.SuppressClearInGetMulti=false;
				_alreadyFetchedGuiDefs = true;
			}
			return _guiDefs;
		}

		/// <summary> Sets the collection parameters for the collection for 'GuiDefs'. These settings will be taken into account
		/// when the property GuiDefs is requested or GetMultiGuiDefs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGuiDefs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_guiDefs.SortClauses=sortClauses;
			_guiDefs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PanelEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PanelEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PanelCollection GetMultiPanels(bool forceFetch)
		{
			return GetMultiPanels(forceFetch, _panels.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PanelEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PanelEntity'</returns>
		public VarioSL.Entities.CollectionClasses.PanelCollection GetMultiPanels(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPanels(forceFetch, _panels.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PanelEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.PanelCollection GetMultiPanels(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPanels(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PanelEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.PanelCollection GetMultiPanels(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPanels || forceFetch || _alwaysFetchPanels) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_panels);
				_panels.SuppressClearInGetMulti=!forceFetch;
				_panels.EntityFactoryToUse = entityFactoryToUse;
				_panels.GetMultiManyToOne(this, filter);
				_panels.SuppressClearInGetMulti=false;
				_alreadyFetchedPanels = true;
			}
			return _panels;
		}

		/// <summary> Sets the collection parameters for the collection for 'Panels'. These settings will be taken into account
		/// when the property Panels is requested or GetMultiPanels is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPanels(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_panels.SortClauses=sortClauses;
			_panels.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SpecialReceiptEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSpecialReceipts(forceFetch, _specialReceipts.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSpecialReceipts(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection GetMultiSpecialReceipts(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSpecialReceipts || forceFetch || _alwaysFetchSpecialReceipts) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_specialReceipts);
				_specialReceipts.SuppressClearInGetMulti=!forceFetch;
				_specialReceipts.EntityFactoryToUse = entityFactoryToUse;
				_specialReceipts.GetMultiManyToOne(this, null, null, filter);
				_specialReceipts.SuppressClearInGetMulti=false;
				_alreadyFetchedSpecialReceipts = true;
			}
			return _specialReceipts;
		}

		/// <summary> Sets the collection parameters for the collection for 'SpecialReceipts'. These settings will be taken into account
		/// when the property SpecialReceipts is requested or GetMultiSpecialReceipts is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSpecialReceipts(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_specialReceipts.SortClauses=sortClauses;
			_specialReceipts.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch)
		{
			return GetMultiTariffs(forceFetch, _tariffs.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffs(forceFetch, _tariffs.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffs(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection GetMultiTariffs(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffs || forceFetch || _alwaysFetchTariffs) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffs);
				_tariffs.SuppressClearInGetMulti=!forceFetch;
				_tariffs.EntityFactoryToUse = entityFactoryToUse;
				_tariffs.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_tariffs.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffs = true;
			}
			return _tariffs;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tariffs'. These settings will be taken into account
		/// when the property Tariffs is requested or GetMultiTariffs is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffs(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffs.SortClauses=sortClauses;
			_tariffs.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TariffReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch)
		{
			return GetMultiTariffRelease(forceFetch, _tariffRelease.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TariffReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTariffRelease(forceFetch, _tariffRelease.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTariffRelease(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TariffReleaseCollection GetMultiTariffRelease(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTariffRelease || forceFetch || _alwaysFetchTariffRelease) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tariffRelease);
				_tariffRelease.SuppressClearInGetMulti=!forceFetch;
				_tariffRelease.EntityFactoryToUse = entityFactoryToUse;
				_tariffRelease.GetMultiManyToOne(this, null, filter);
				_tariffRelease.SuppressClearInGetMulti=false;
				_alreadyFetchedTariffRelease = true;
			}
			return _tariffRelease;
		}

		/// <summary> Sets the collection parameters for the collection for 'TariffRelease'. These settings will be taken into account
		/// when the property TariffRelease is requested or GetMultiTariffRelease is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTariffRelease(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tariffRelease.SortClauses=sortClauses;
			_tariffRelease.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTickets(forceFetch, _tickets.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTickets(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection GetMultiTickets(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTickets || forceFetch || _alwaysFetchTickets) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_tickets);
				_tickets.SuppressClearInGetMulti=!forceFetch;
				_tickets.EntityFactoryToUse = entityFactoryToUse;
				_tickets.GetMultiManyToOne(null, null, null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_tickets.SuppressClearInGetMulti=false;
				_alreadyFetchedTickets = true;
			}
			return _tickets;
		}

		/// <summary> Sets the collection parameters for the collection for 'Tickets'. These settings will be taken into account
		/// when the property Tickets is requested or GetMultiTickets is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTickets(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_tickets.SortClauses=sortClauses;
			_tickets.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch)
		{
			return GetMultiTicketDeviceClasses(forceFetch, _ticketDeviceClasses.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TicketDeviceClassEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTicketDeviceClasses(forceFetch, _ticketDeviceClasses.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTicketDeviceClasses(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection GetMultiTicketDeviceClasses(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTicketDeviceClasses || forceFetch || _alwaysFetchTicketDeviceClasses) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_ticketDeviceClasses);
				_ticketDeviceClasses.SuppressClearInGetMulti=!forceFetch;
				_ticketDeviceClasses.EntityFactoryToUse = entityFactoryToUse;
				_ticketDeviceClasses.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, filter);
				_ticketDeviceClasses.SuppressClearInGetMulti=false;
				_alreadyFetchedTicketDeviceClasses = true;
			}
			return _ticketDeviceClasses;
		}

		/// <summary> Sets the collection parameters for the collection for 'TicketDeviceClasses'. These settings will be taken into account
		/// when the property TicketDeviceClasses is requested or GetMultiTicketDeviceClasses is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTicketDeviceClasses(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_ticketDeviceClasses.SortClauses=sortClauses;
			_ticketDeviceClasses.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterCollection GetMultiParameters(bool forceFetch)
		{
			return GetMultiParameters(forceFetch, _parameters.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterCollection GetMultiParameters(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameters(forceFetch, _parameters.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterCollection GetMultiParameters(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameters(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterCollection GetMultiParameters(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameters || forceFetch || _alwaysFetchParameters) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameters);
				_parameters.SuppressClearInGetMulti=!forceFetch;
				_parameters.EntityFactoryToUse = entityFactoryToUse;
				_parameters.GetMultiManyToOne(this, filter);
				_parameters.SuppressClearInGetMulti=false;
				_alreadyFetchedParameters = true;
			}
			return _parameters;
		}

		/// <summary> Sets the collection parameters for the collection for 'Parameters'. These settings will be taken into account
		/// when the property Parameters is requested or GetMultiParameters is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameters(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameters.SortClauses=sortClauses;
			_parameters.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch)
		{
			return GetMultiParameterArchiveReleases(forceFetch, _parameterArchiveReleases.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterArchiveReleaseEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterArchiveReleases(forceFetch, _parameterArchiveReleases.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterArchiveReleases(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection GetMultiParameterArchiveReleases(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterArchiveReleases || forceFetch || _alwaysFetchParameterArchiveReleases) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterArchiveReleases);
				_parameterArchiveReleases.SuppressClearInGetMulti=!forceFetch;
				_parameterArchiveReleases.EntityFactoryToUse = entityFactoryToUse;
				_parameterArchiveReleases.GetMultiManyToOne(this, null, filter);
				_parameterArchiveReleases.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterArchiveReleases = true;
			}
			return _parameterArchiveReleases;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterArchiveReleases'. These settings will be taken into account
		/// when the property ParameterArchiveReleases is requested or GetMultiParameterArchiveReleases is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterArchiveReleases(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterArchiveReleases.SortClauses=sortClauses;
			_parameterArchiveReleases.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ParameterValueEntity'</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiParameterValues(forceFetch, _parameterValues.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiParameterValues(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection GetMultiParameterValues(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedParameterValues || forceFetch || _alwaysFetchParameterValues) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_parameterValues);
				_parameterValues.SuppressClearInGetMulti=!forceFetch;
				_parameterValues.EntityFactoryToUse = entityFactoryToUse;
				_parameterValues.GetMultiManyToOne(this, null, null, null, null, filter);
				_parameterValues.SuppressClearInGetMulti=false;
				_alreadyFetchedParameterValues = true;
			}
			return _parameterValues;
		}

		/// <summary> Sets the collection parameters for the collection for 'ParameterValues'. These settings will be taken into account
		/// when the property ParameterValues is requested or GetMultiParameterValues is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersParameterValues(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_parameterValues.SortClauses=sortClauses;
			_parameterValues.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SaleEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSales(forceFetch, _sales.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSales(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection GetMultiSales(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSales || forceFetch || _alwaysFetchSales) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_sales);
				_sales.SuppressClearInGetMulti=!forceFetch;
				_sales.EntityFactoryToUse = entityFactoryToUse;
				_sales.GetMultiManyToOne(null, this, null, null, null, null, null, null, filter);
				_sales.SuppressClearInGetMulti=false;
				_alreadyFetchedSales = true;
			}
			return _sales;
		}

		/// <summary> Sets the collection parameters for the collection for 'Sales'. These settings will be taken into account
		/// when the property Sales is requested or GetMultiSales is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSales(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_sales.SortClauses=sortClauses;
			_sales.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SalesChannelToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, _salesChannelToPaymentMethods.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SalesChannelToPaymentMethodEntity'</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, _salesChannelToPaymentMethods.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSalesChannelToPaymentMethods(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection GetMultiSalesChannelToPaymentMethods(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSalesChannelToPaymentMethods || forceFetch || _alwaysFetchSalesChannelToPaymentMethods) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_salesChannelToPaymentMethods);
				_salesChannelToPaymentMethods.SuppressClearInGetMulti=!forceFetch;
				_salesChannelToPaymentMethods.EntityFactoryToUse = entityFactoryToUse;
				_salesChannelToPaymentMethods.GetMultiManyToOne(this, null, filter);
				_salesChannelToPaymentMethods.SuppressClearInGetMulti=false;
				_alreadyFetchedSalesChannelToPaymentMethods = true;
			}
			return _salesChannelToPaymentMethods;
		}

		/// <summary> Sets the collection parameters for the collection for 'SalesChannelToPaymentMethods'. These settings will be taken into account
		/// when the property SalesChannelToPaymentMethods is requested or GetMultiSalesChannelToPaymentMethods is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSalesChannelToPaymentMethods(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_salesChannelToPaymentMethods.SortClauses=sortClauses;
			_salesChannelToPaymentMethods.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TransactionJournalEntity'</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTransactionJournals(forceFetch, _transactionJournals.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTransactionJournals(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection GetMultiTransactionJournals(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTransactionJournals || forceFetch || _alwaysFetchTransactionJournals) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_transactionJournals);
				_transactionJournals.SuppressClearInGetMulti=!forceFetch;
				_transactionJournals.EntityFactoryToUse = entityFactoryToUse;
				_transactionJournals.GetMultiManyToOne(null, this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_transactionJournals.SuppressClearInGetMulti=false;
				_alreadyFetchedTransactionJournals = true;
			}
			return _transactionJournals;
		}

		/// <summary> Sets the collection parameters for the collection for 'TransactionJournals'. These settings will be taken into account
		/// when the property TransactionJournals is requested or GetMultiTransactionJournals is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTransactionJournals(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_transactionJournals.SortClauses=sortClauses;
			_transactionJournals.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch)
		{
			return GetMultiUnitCollections(forceFetch, _unitCollections.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UnitCollectionEntity'</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUnitCollections(forceFetch, _unitCollections.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUnitCollections(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionCollection GetMultiUnitCollections(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUnitCollections || forceFetch || _alwaysFetchUnitCollections) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_unitCollections);
				_unitCollections.SuppressClearInGetMulti=!forceFetch;
				_unitCollections.EntityFactoryToUse = entityFactoryToUse;
				_unitCollections.GetMultiManyToOne(this, null, filter);
				_unitCollections.SuppressClearInGetMulti=false;
				_alreadyFetchedUnitCollections = true;
			}
			return _unitCollections;
		}

		/// <summary> Sets the collection parameters for the collection for 'UnitCollections'. These settings will be taken into account
		/// when the property UnitCollections is requested or GetMultiUnitCollections is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUnitCollections(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_unitCollections.SortClauses=sortClauses;
			_unitCollections.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClearingResults", _clearingResults);
			toReturn.Add("UmDevices", _umDevices);
			toReturn.Add("GuiDefs", _guiDefs);
			toReturn.Add("Panels", _panels);
			toReturn.Add("SpecialReceipts", _specialReceipts);
			toReturn.Add("Tariffs", _tariffs);
			toReturn.Add("TariffRelease", _tariffRelease);
			toReturn.Add("Tickets", _tickets);
			toReturn.Add("TicketDeviceClasses", _ticketDeviceClasses);
			toReturn.Add("Parameters", _parameters);
			toReturn.Add("ParameterArchiveReleases", _parameterArchiveReleases);
			toReturn.Add("ParameterValues", _parameterValues);
			toReturn.Add("Sales", _sales);
			toReturn.Add("SalesChannelToPaymentMethods", _salesChannelToPaymentMethods);
			toReturn.Add("TransactionJournals", _transactionJournals);
			toReturn.Add("UnitCollections", _unitCollections);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="validator">The validator object for this DeviceClassEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 deviceClassID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceClassID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clearingResults = new VarioSL.Entities.CollectionClasses.ClearingResultCollection();
			_clearingResults.SetContainingEntityInfo(this, "DeviceClass");

			_umDevices = new VarioSL.Entities.CollectionClasses.DeviceCollection();
			_umDevices.SetContainingEntityInfo(this, "DeviceClass");

			_guiDefs = new VarioSL.Entities.CollectionClasses.GuiDefCollection();
			_guiDefs.SetContainingEntityInfo(this, "DeviceClass");

			_panels = new VarioSL.Entities.CollectionClasses.PanelCollection();
			_panels.SetContainingEntityInfo(this, "DeviceClass");

			_specialReceipts = new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection();
			_specialReceipts.SetContainingEntityInfo(this, "DeviceClass");

			_tariffs = new VarioSL.Entities.CollectionClasses.TariffCollection();
			_tariffs.SetContainingEntityInfo(this, "DeviceClass");

			_tariffRelease = new VarioSL.Entities.CollectionClasses.TariffReleaseCollection();
			_tariffRelease.SetContainingEntityInfo(this, "DeviceClass");

			_tickets = new VarioSL.Entities.CollectionClasses.TicketCollection();
			_tickets.SetContainingEntityInfo(this, "DeviceClass");

			_ticketDeviceClasses = new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection();
			_ticketDeviceClasses.SetContainingEntityInfo(this, "DeviceClass");

			_parameters = new VarioSL.Entities.CollectionClasses.ParameterCollection();
			_parameters.SetContainingEntityInfo(this, "DeviceClass");

			_parameterArchiveReleases = new VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection();
			_parameterArchiveReleases.SetContainingEntityInfo(this, "DeviceClass");

			_parameterValues = new VarioSL.Entities.CollectionClasses.ParameterValueCollection();
			_parameterValues.SetContainingEntityInfo(this, "DeviceClass");

			_sales = new VarioSL.Entities.CollectionClasses.SaleCollection();
			_sales.SetContainingEntityInfo(this, "DeviceClass");

			_salesChannelToPaymentMethods = new VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection();
			_salesChannelToPaymentMethods.SetContainingEntityInfo(this, "DeviceClass");

			_transactionJournals = new VarioSL.Entities.CollectionClasses.TransactionJournalCollection();
			_transactionJournals.SetContainingEntityInfo(this, "DeviceClass");

			_unitCollections = new VarioSL.Entities.CollectionClasses.UnitCollectionCollection();
			_unitCollections.SetContainingEntityInfo(this, "DeviceClass");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateLineDefDat", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateLs", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateParameterArchive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreateTariffArchive", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CryptoReleaseVisibility", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceClassID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("GneralAccount", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImageFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ImsApplicationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Lockable", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParameterReleaseVisibility", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReleaseVisibility", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SaleDevice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShortName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TmVisibility", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TypeOfUnitID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UnzipEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceClassID">PK value for DeviceClass which data should be fetched into this DeviceClass object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 deviceClassID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceClassFieldIndex.DeviceClassID].ForcedCurrentValueWrite(deviceClassID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceClassDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceClassEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceClassRelations Relations
		{
			get	{ return new DeviceClassRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClearingResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClearingResults
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ClearingResultCollection(), (IEntityRelation)GetRelationsForField("ClearingResults")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.ClearingResultEntity, 0, null, null, null, "ClearingResults", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Device' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUmDevices
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.DeviceCollection(), (IEntityRelation)GetRelationsForField("UmDevices")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.DeviceEntity, 0, null, null, null, "UmDevices", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'GuiDef' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGuiDefs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.GuiDefCollection(), (IEntityRelation)GetRelationsForField("GuiDefs")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.GuiDefEntity, 0, null, null, null, "GuiDefs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Panel' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPanels
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.PanelCollection(), (IEntityRelation)GetRelationsForField("Panels")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.PanelEntity, 0, null, null, null, "Panels", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SpecialReceipt' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSpecialReceipts
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SpecialReceiptCollection(), (IEntityRelation)GetRelationsForField("SpecialReceipts")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.SpecialReceiptEntity, 0, null, null, null, "SpecialReceipts", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffs
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariffs")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariffs", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TariffRelease' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariffRelease
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffReleaseCollection(), (IEntityRelation)GetRelationsForField("TariffRelease")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.TariffReleaseEntity, 0, null, null, null, "TariffRelease", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTickets
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Tickets")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Tickets", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TicketDeviceClass' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicketDeviceClasses
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection(), (IEntityRelation)GetRelationsForField("TicketDeviceClasses")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.TicketDeviceClassEntity, 0, null, null, null, "TicketDeviceClasses", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Parameter' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameters
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterCollection(), (IEntityRelation)GetRelationsForField("Parameters")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.ParameterEntity, 0, null, null, null, "Parameters", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterArchiveRelease' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterArchiveReleases
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection(), (IEntityRelation)GetRelationsForField("ParameterArchiveReleases")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity, 0, null, null, null, "ParameterArchiveReleases", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ParameterValue' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParameterValues
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.ParameterValueCollection(), (IEntityRelation)GetRelationsForField("ParameterValues")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.ParameterValueEntity, 0, null, null, null, "ParameterValues", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Sale' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSales
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SaleCollection(), (IEntityRelation)GetRelationsForField("Sales")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.SaleEntity, 0, null, null, null, "Sales", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SalesChannelToPaymentMethod' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSalesChannelToPaymentMethods
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection(), (IEntityRelation)GetRelationsForField("SalesChannelToPaymentMethods")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity, 0, null, null, null, "SalesChannelToPaymentMethods", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TransactionJournal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTransactionJournals
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TransactionJournalCollection(), (IEntityRelation)GetRelationsForField("TransactionJournals")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.TransactionJournalEntity, 0, null, null, null, "TransactionJournals", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UnitCollection' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUnitCollections
		{
			get { return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.UnitCollectionCollection(), (IEntityRelation)GetRelationsForField("UnitCollections")[0], (int)VarioSL.Entities.EntityType.DeviceClassEntity, (int)VarioSL.Entities.EntityType.UnitCollectionEntity, 0, null, null, null, "UnitCollections", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CreateLineDefDat property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."CREATELINEDEFDAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> CreateLineDefDat
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.CreateLineDefDat, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.CreateLineDefDat, value, true); }
		}

		/// <summary> The CreateLs property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."CREATELS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> CreateLs
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.CreateLs, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.CreateLs, value, true); }
		}

		/// <summary> The CreateParameterArchive property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."CREATEPARAMETERARCHIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CreateParameterArchive
		{
			get { return (System.Boolean)GetValue((int)DeviceClassFieldIndex.CreateParameterArchive, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.CreateParameterArchive, value, true); }
		}

		/// <summary> The CreateTariffArchive property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."CREATETARIFFARCHIVE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> CreateTariffArchive
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.CreateTariffArchive, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.CreateTariffArchive, value, true); }
		}

		/// <summary> The CryptoReleaseVisibility property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."CRYPTORELEASEVISIBILITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean CryptoReleaseVisibility
		{
			get { return (System.Boolean)GetValue((int)DeviceClassFieldIndex.CryptoReleaseVisibility, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.CryptoReleaseVisibility, value, true); }
		}

		/// <summary> The DeviceClassID property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."DEVICECLASSID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 DeviceClassID
		{
			get { return (System.Int64)GetValue((int)DeviceClassFieldIndex.DeviceClassID, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.DeviceClassID, value, true); }
		}

		/// <summary> The GneralAccount property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."GENERALACCOUNT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> GneralAccount
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceClassFieldIndex.GneralAccount, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.GneralAccount, value, true); }
		}

		/// <summary> The ImageFile property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."IMAGEFILE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ImageFile
		{
			get { return (System.String)GetValue((int)DeviceClassFieldIndex.ImageFile, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.ImageFile, value, true); }
		}

		/// <summary> The ImsApplicationName property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."IMSAPPLICATIONNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ImsApplicationName
		{
			get { return (System.String)GetValue((int)DeviceClassFieldIndex.ImsApplicationName, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.ImsApplicationName, value, true); }
		}

		/// <summary> The Lockable property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."LOCKABLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Lockable
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.Lockable, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.Lockable, value, true); }
		}

		/// <summary> The Name property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."NAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)DeviceClassFieldIndex.Name, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.Name, value, true); }
		}

		/// <summary> The ParameterReleaseVisibility property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."PARAMETERRELEASEVISIBILITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> ParameterReleaseVisibility
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.ParameterReleaseVisibility, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.ParameterReleaseVisibility, value, true); }
		}

		/// <summary> The ReleaseName property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."RELEASENAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ReleaseName
		{
			get { return (System.String)GetValue((int)DeviceClassFieldIndex.ReleaseName, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.ReleaseName, value, true); }
		}

		/// <summary> The ReleaseVisibility property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."RELEASEVISIBILITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> ReleaseVisibility
		{
			get { return (Nullable<System.Int16>)GetValue((int)DeviceClassFieldIndex.ReleaseVisibility, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.ReleaseVisibility, value, true); }
		}

		/// <summary> The SaleDevice property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."SALEDEVICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> SaleDevice
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.SaleDevice, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.SaleDevice, value, true); }
		}

		/// <summary> The ShortName property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."SHORTNAME"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShortName
		{
			get { return (System.String)GetValue((int)DeviceClassFieldIndex.ShortName, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.ShortName, value, true); }
		}

		/// <summary> The TmVisibility property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."TMVISIBILITY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TmVisibility
		{
			get { return (Nullable<System.Int16>)GetValue((int)DeviceClassFieldIndex.TmVisibility, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.TmVisibility, value, true); }
		}

		/// <summary> The TypeOfUnitID property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."TYPEOFUNITID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int16> TypeOfUnitID
		{
			get { return (Nullable<System.Int16>)GetValue((int)DeviceClassFieldIndex.TypeOfUnitID, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.TypeOfUnitID, value, true); }
		}

		/// <summary> The UnzipEnabled property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."UNZIPENABLED"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UnzipEnabled
		{
			get { return (System.Boolean)GetValue((int)DeviceClassFieldIndex.UnzipEnabled, true); }
			set	{ SetValue((int)DeviceClassFieldIndex.UnzipEnabled, value, true); }
		}

		/// <summary> The Visible property of the Entity DeviceClass<br/><br/></summary>
		/// <remarks>Mapped on  table field: "VARIO_DEVICECLASS"."VISIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> Visible
		{
			get { return (Nullable<System.Boolean>)GetValue((int)DeviceClassFieldIndex.Visible, false); }
			set	{ SetValue((int)DeviceClassFieldIndex.Visible, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClearingResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClearingResults()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ClearingResultCollection ClearingResults
		{
			get	{ return GetMultiClearingResults(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClearingResults. When set to true, ClearingResults is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClearingResults is accessed. You can always execute/ a forced fetch by calling GetMultiClearingResults(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClearingResults
		{
			get	{ return _alwaysFetchClearingResults; }
			set	{ _alwaysFetchClearingResults = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClearingResults already has been fetched. Setting this property to false when ClearingResults has been fetched
		/// will clear the ClearingResults collection well. Setting this property to true while ClearingResults hasn't been fetched disables lazy loading for ClearingResults</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClearingResults
		{
			get { return _alreadyFetchedClearingResults;}
			set 
			{
				if(_alreadyFetchedClearingResults && !value && (_clearingResults != null))
				{
					_clearingResults.Clear();
				}
				_alreadyFetchedClearingResults = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUmDevices()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.DeviceCollection UmDevices
		{
			get	{ return GetMultiUmDevices(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UmDevices. When set to true, UmDevices is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UmDevices is accessed. You can always execute/ a forced fetch by calling GetMultiUmDevices(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUmDevices
		{
			get	{ return _alwaysFetchUmDevices; }
			set	{ _alwaysFetchUmDevices = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UmDevices already has been fetched. Setting this property to false when UmDevices has been fetched
		/// will clear the UmDevices collection well. Setting this property to true while UmDevices hasn't been fetched disables lazy loading for UmDevices</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUmDevices
		{
			get { return _alreadyFetchedUmDevices;}
			set 
			{
				if(_alreadyFetchedUmDevices && !value && (_umDevices != null))
				{
					_umDevices.Clear();
				}
				_alreadyFetchedUmDevices = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GuiDefEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGuiDefs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.GuiDefCollection GuiDefs
		{
			get	{ return GetMultiGuiDefs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GuiDefs. When set to true, GuiDefs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GuiDefs is accessed. You can always execute/ a forced fetch by calling GetMultiGuiDefs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGuiDefs
		{
			get	{ return _alwaysFetchGuiDefs; }
			set	{ _alwaysFetchGuiDefs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GuiDefs already has been fetched. Setting this property to false when GuiDefs has been fetched
		/// will clear the GuiDefs collection well. Setting this property to true while GuiDefs hasn't been fetched disables lazy loading for GuiDefs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGuiDefs
		{
			get { return _alreadyFetchedGuiDefs;}
			set 
			{
				if(_alreadyFetchedGuiDefs && !value && (_guiDefs != null))
				{
					_guiDefs.Clear();
				}
				_alreadyFetchedGuiDefs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PanelEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPanels()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.PanelCollection Panels
		{
			get	{ return GetMultiPanels(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Panels. When set to true, Panels is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Panels is accessed. You can always execute/ a forced fetch by calling GetMultiPanels(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPanels
		{
			get	{ return _alwaysFetchPanels; }
			set	{ _alwaysFetchPanels = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Panels already has been fetched. Setting this property to false when Panels has been fetched
		/// will clear the Panels collection well. Setting this property to true while Panels hasn't been fetched disables lazy loading for Panels</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPanels
		{
			get { return _alreadyFetchedPanels;}
			set 
			{
				if(_alreadyFetchedPanels && !value && (_panels != null))
				{
					_panels.Clear();
				}
				_alreadyFetchedPanels = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SpecialReceiptEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSpecialReceipts()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SpecialReceiptCollection SpecialReceipts
		{
			get	{ return GetMultiSpecialReceipts(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SpecialReceipts. When set to true, SpecialReceipts is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SpecialReceipts is accessed. You can always execute/ a forced fetch by calling GetMultiSpecialReceipts(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSpecialReceipts
		{
			get	{ return _alwaysFetchSpecialReceipts; }
			set	{ _alwaysFetchSpecialReceipts = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SpecialReceipts already has been fetched. Setting this property to false when SpecialReceipts has been fetched
		/// will clear the SpecialReceipts collection well. Setting this property to true while SpecialReceipts hasn't been fetched disables lazy loading for SpecialReceipts</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSpecialReceipts
		{
			get { return _alreadyFetchedSpecialReceipts;}
			set 
			{
				if(_alreadyFetchedSpecialReceipts && !value && (_specialReceipts != null))
				{
					_specialReceipts.Clear();
				}
				_alreadyFetchedSpecialReceipts = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffs()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffCollection Tariffs
		{
			get	{ return GetMultiTariffs(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tariffs. When set to true, Tariffs is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariffs is accessed. You can always execute/ a forced fetch by calling GetMultiTariffs(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffs
		{
			get	{ return _alwaysFetchTariffs; }
			set	{ _alwaysFetchTariffs = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariffs already has been fetched. Setting this property to false when Tariffs has been fetched
		/// will clear the Tariffs collection well. Setting this property to true while Tariffs hasn't been fetched disables lazy loading for Tariffs</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffs
		{
			get { return _alreadyFetchedTariffs;}
			set 
			{
				if(_alreadyFetchedTariffs && !value && (_tariffs != null))
				{
					_tariffs.Clear();
				}
				_alreadyFetchedTariffs = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TariffReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTariffRelease()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TariffReleaseCollection TariffRelease
		{
			get	{ return GetMultiTariffRelease(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TariffRelease. When set to true, TariffRelease is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TariffRelease is accessed. You can always execute/ a forced fetch by calling GetMultiTariffRelease(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariffRelease
		{
			get	{ return _alwaysFetchTariffRelease; }
			set	{ _alwaysFetchTariffRelease = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TariffRelease already has been fetched. Setting this property to false when TariffRelease has been fetched
		/// will clear the TariffRelease collection well. Setting this property to true while TariffRelease hasn't been fetched disables lazy loading for TariffRelease</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariffRelease
		{
			get { return _alreadyFetchedTariffRelease;}
			set 
			{
				if(_alreadyFetchedTariffRelease && !value && (_tariffRelease != null))
				{
					_tariffRelease.Clear();
				}
				_alreadyFetchedTariffRelease = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTickets()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketCollection Tickets
		{
			get	{ return GetMultiTickets(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Tickets. When set to true, Tickets is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tickets is accessed. You can always execute/ a forced fetch by calling GetMultiTickets(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTickets
		{
			get	{ return _alwaysFetchTickets; }
			set	{ _alwaysFetchTickets = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tickets already has been fetched. Setting this property to false when Tickets has been fetched
		/// will clear the Tickets collection well. Setting this property to true while Tickets hasn't been fetched disables lazy loading for Tickets</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTickets
		{
			get { return _alreadyFetchedTickets;}
			set 
			{
				if(_alreadyFetchedTickets && !value && (_tickets != null))
				{
					_tickets.Clear();
				}
				_alreadyFetchedTickets = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TicketDeviceClassEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTicketDeviceClasses()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TicketDeviceClassCollection TicketDeviceClasses
		{
			get	{ return GetMultiTicketDeviceClasses(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TicketDeviceClasses. When set to true, TicketDeviceClasses is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TicketDeviceClasses is accessed. You can always execute/ a forced fetch by calling GetMultiTicketDeviceClasses(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicketDeviceClasses
		{
			get	{ return _alwaysFetchTicketDeviceClasses; }
			set	{ _alwaysFetchTicketDeviceClasses = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TicketDeviceClasses already has been fetched. Setting this property to false when TicketDeviceClasses has been fetched
		/// will clear the TicketDeviceClasses collection well. Setting this property to true while TicketDeviceClasses hasn't been fetched disables lazy loading for TicketDeviceClasses</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicketDeviceClasses
		{
			get { return _alreadyFetchedTicketDeviceClasses;}
			set 
			{
				if(_alreadyFetchedTicketDeviceClasses && !value && (_ticketDeviceClasses != null))
				{
					_ticketDeviceClasses.Clear();
				}
				_alreadyFetchedTicketDeviceClasses = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameters()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterCollection Parameters
		{
			get	{ return GetMultiParameters(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Parameters. When set to true, Parameters is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Parameters is accessed. You can always execute/ a forced fetch by calling GetMultiParameters(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameters
		{
			get	{ return _alwaysFetchParameters; }
			set	{ _alwaysFetchParameters = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Parameters already has been fetched. Setting this property to false when Parameters has been fetched
		/// will clear the Parameters collection well. Setting this property to true while Parameters hasn't been fetched disables lazy loading for Parameters</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameters
		{
			get { return _alreadyFetchedParameters;}
			set 
			{
				if(_alreadyFetchedParameters && !value && (_parameters != null))
				{
					_parameters.Clear();
				}
				_alreadyFetchedParameters = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterArchiveReleaseEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterArchiveReleases()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterArchiveReleaseCollection ParameterArchiveReleases
		{
			get	{ return GetMultiParameterArchiveReleases(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterArchiveReleases. When set to true, ParameterArchiveReleases is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterArchiveReleases is accessed. You can always execute/ a forced fetch by calling GetMultiParameterArchiveReleases(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterArchiveReleases
		{
			get	{ return _alwaysFetchParameterArchiveReleases; }
			set	{ _alwaysFetchParameterArchiveReleases = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterArchiveReleases already has been fetched. Setting this property to false when ParameterArchiveReleases has been fetched
		/// will clear the ParameterArchiveReleases collection well. Setting this property to true while ParameterArchiveReleases hasn't been fetched disables lazy loading for ParameterArchiveReleases</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterArchiveReleases
		{
			get { return _alreadyFetchedParameterArchiveReleases;}
			set 
			{
				if(_alreadyFetchedParameterArchiveReleases && !value && (_parameterArchiveReleases != null))
				{
					_parameterArchiveReleases.Clear();
				}
				_alreadyFetchedParameterArchiveReleases = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ParameterValueEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiParameterValues()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.ParameterValueCollection ParameterValues
		{
			get	{ return GetMultiParameterValues(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ParameterValues. When set to true, ParameterValues is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParameterValues is accessed. You can always execute/ a forced fetch by calling GetMultiParameterValues(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParameterValues
		{
			get	{ return _alwaysFetchParameterValues; }
			set	{ _alwaysFetchParameterValues = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParameterValues already has been fetched. Setting this property to false when ParameterValues has been fetched
		/// will clear the ParameterValues collection well. Setting this property to true while ParameterValues hasn't been fetched disables lazy loading for ParameterValues</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParameterValues
		{
			get { return _alreadyFetchedParameterValues;}
			set 
			{
				if(_alreadyFetchedParameterValues && !value && (_parameterValues != null))
				{
					_parameterValues.Clear();
				}
				_alreadyFetchedParameterValues = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SaleEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSales()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SaleCollection Sales
		{
			get	{ return GetMultiSales(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for Sales. When set to true, Sales is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Sales is accessed. You can always execute/ a forced fetch by calling GetMultiSales(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSales
		{
			get	{ return _alwaysFetchSales; }
			set	{ _alwaysFetchSales = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property Sales already has been fetched. Setting this property to false when Sales has been fetched
		/// will clear the Sales collection well. Setting this property to true while Sales hasn't been fetched disables lazy loading for Sales</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSales
		{
			get { return _alreadyFetchedSales;}
			set 
			{
				if(_alreadyFetchedSales && !value && (_sales != null))
				{
					_sales.Clear();
				}
				_alreadyFetchedSales = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SalesChannelToPaymentMethodEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSalesChannelToPaymentMethods()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.SalesChannelToPaymentMethodCollection SalesChannelToPaymentMethods
		{
			get	{ return GetMultiSalesChannelToPaymentMethods(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SalesChannelToPaymentMethods. When set to true, SalesChannelToPaymentMethods is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SalesChannelToPaymentMethods is accessed. You can always execute/ a forced fetch by calling GetMultiSalesChannelToPaymentMethods(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSalesChannelToPaymentMethods
		{
			get	{ return _alwaysFetchSalesChannelToPaymentMethods; }
			set	{ _alwaysFetchSalesChannelToPaymentMethods = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SalesChannelToPaymentMethods already has been fetched. Setting this property to false when SalesChannelToPaymentMethods has been fetched
		/// will clear the SalesChannelToPaymentMethods collection well. Setting this property to true while SalesChannelToPaymentMethods hasn't been fetched disables lazy loading for SalesChannelToPaymentMethods</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSalesChannelToPaymentMethods
		{
			get { return _alreadyFetchedSalesChannelToPaymentMethods;}
			set 
			{
				if(_alreadyFetchedSalesChannelToPaymentMethods && !value && (_salesChannelToPaymentMethods != null))
				{
					_salesChannelToPaymentMethods.Clear();
				}
				_alreadyFetchedSalesChannelToPaymentMethods = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TransactionJournalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTransactionJournals()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.TransactionJournalCollection TransactionJournals
		{
			get	{ return GetMultiTransactionJournals(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TransactionJournals. When set to true, TransactionJournals is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TransactionJournals is accessed. You can always execute/ a forced fetch by calling GetMultiTransactionJournals(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTransactionJournals
		{
			get	{ return _alwaysFetchTransactionJournals; }
			set	{ _alwaysFetchTransactionJournals = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TransactionJournals already has been fetched. Setting this property to false when TransactionJournals has been fetched
		/// will clear the TransactionJournals collection well. Setting this property to true while TransactionJournals hasn't been fetched disables lazy loading for TransactionJournals</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTransactionJournals
		{
			get { return _alreadyFetchedTransactionJournals;}
			set 
			{
				if(_alreadyFetchedTransactionJournals && !value && (_transactionJournals != null))
				{
					_transactionJournals.Clear();
				}
				_alreadyFetchedTransactionJournals = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UnitCollectionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUnitCollections()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual VarioSL.Entities.CollectionClasses.UnitCollectionCollection UnitCollections
		{
			get	{ return GetMultiUnitCollections(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UnitCollections. When set to true, UnitCollections is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UnitCollections is accessed. You can always execute/ a forced fetch by calling GetMultiUnitCollections(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUnitCollections
		{
			get	{ return _alwaysFetchUnitCollections; }
			set	{ _alwaysFetchUnitCollections = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UnitCollections already has been fetched. Setting this property to false when UnitCollections has been fetched
		/// will clear the UnitCollections collection well. Setting this property to true while UnitCollections hasn't been fetched disables lazy loading for UnitCollections</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUnitCollections
		{
			get { return _alreadyFetchedUnitCollections;}
			set 
			{
				if(_alreadyFetchedUnitCollections && !value && (_unitCollections != null))
				{
					_unitCollections.Clear();
				}
				_alreadyFetchedUnitCollections = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.DeviceClassEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
