﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Data;
using System.Xml.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VdvProduct'. <br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public partial class VdvProductEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private AttributeValueEntity _distanceAttributeValue;
		private bool	_alwaysFetchDistanceAttributeValue, _alreadyFetchedDistanceAttributeValue, _distanceAttributeValueReturnsNewIfNotFound;
		private RulePeriodEntity _rulePeriod;
		private bool	_alwaysFetchRulePeriod, _alreadyFetchedRulePeriod, _rulePeriodReturnsNewIfNotFound;
		private TariffEntity _tariff;
		private bool	_alwaysFetchTariff, _alreadyFetchedTariff, _tariffReturnsNewIfNotFound;
		private TicketEntity _ticket;
		private bool	_alwaysFetchTicket, _alreadyFetchedTicket, _ticketReturnsNewIfNotFound;
		private VdvLayoutEntity _vdvLayout;
		private bool	_alwaysFetchVdvLayout, _alreadyFetchedVdvLayout, _vdvLayoutReturnsNewIfNotFound;
		private VdvTypeEntity _issueModeNmType;
		private bool	_alwaysFetchIssueModeNmType, _alreadyFetchedIssueModeNmType, _issueModeNmTypeReturnsNewIfNotFound;
		private VdvTypeEntity _issueModeSeType;
		private bool	_alwaysFetchIssueModeSeType, _alreadyFetchedIssueModeSeType, _issueModeSeTypeReturnsNewIfNotFound;
		private VdvTypeEntity _priorityModeType;
		private bool	_alwaysFetchPriorityModeType, _alreadyFetchedPriorityModeType, _priorityModeTypeReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name DistanceAttributeValue</summary>
			public static readonly string DistanceAttributeValue = "DistanceAttributeValue";
			/// <summary>Member name RulePeriod</summary>
			public static readonly string RulePeriod = "RulePeriod";
			/// <summary>Member name Tariff</summary>
			public static readonly string Tariff = "Tariff";
			/// <summary>Member name Ticket</summary>
			public static readonly string Ticket = "Ticket";
			/// <summary>Member name VdvLayout</summary>
			public static readonly string VdvLayout = "VdvLayout";
			/// <summary>Member name IssueModeNmType</summary>
			public static readonly string IssueModeNmType = "IssueModeNmType";
			/// <summary>Member name IssueModeSeType</summary>
			public static readonly string IssueModeSeType = "IssueModeSeType";
			/// <summary>Member name PriorityModeType</summary>
			public static readonly string PriorityModeType = "PriorityModeType";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static VdvProductEntity()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public VdvProductEntity() :base("VdvProductEntity")
		{
			InitClassEmpty(null);
		}
		
		/// <summary>CTor</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		public VdvProductEntity(System.Int64 vdvProductID):base("VdvProductEntity")
		{
			InitClassFetch(vdvProductID, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VdvProductEntity(System.Int64 vdvProductID, IPrefetchPath prefetchPathToUse):base("VdvProductEntity")
		{
			InitClassFetch(vdvProductID, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="validator">The custom validator object for this VdvProductEntity</param>
		public VdvProductEntity(System.Int64 vdvProductID, IValidator validator):base("VdvProductEntity")
		{
			InitClassFetch(vdvProductID, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VdvProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_distanceAttributeValue = (AttributeValueEntity)info.GetValue("_distanceAttributeValue", typeof(AttributeValueEntity));
			if(_distanceAttributeValue!=null)
			{
				_distanceAttributeValue.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_distanceAttributeValueReturnsNewIfNotFound = info.GetBoolean("_distanceAttributeValueReturnsNewIfNotFound");
			_alwaysFetchDistanceAttributeValue = info.GetBoolean("_alwaysFetchDistanceAttributeValue");
			_alreadyFetchedDistanceAttributeValue = info.GetBoolean("_alreadyFetchedDistanceAttributeValue");

			_rulePeriod = (RulePeriodEntity)info.GetValue("_rulePeriod", typeof(RulePeriodEntity));
			if(_rulePeriod!=null)
			{
				_rulePeriod.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_rulePeriodReturnsNewIfNotFound = info.GetBoolean("_rulePeriodReturnsNewIfNotFound");
			_alwaysFetchRulePeriod = info.GetBoolean("_alwaysFetchRulePeriod");
			_alreadyFetchedRulePeriod = info.GetBoolean("_alreadyFetchedRulePeriod");

			_tariff = (TariffEntity)info.GetValue("_tariff", typeof(TariffEntity));
			if(_tariff!=null)
			{
				_tariff.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_tariffReturnsNewIfNotFound = info.GetBoolean("_tariffReturnsNewIfNotFound");
			_alwaysFetchTariff = info.GetBoolean("_alwaysFetchTariff");
			_alreadyFetchedTariff = info.GetBoolean("_alreadyFetchedTariff");

			_ticket = (TicketEntity)info.GetValue("_ticket", typeof(TicketEntity));
			if(_ticket!=null)
			{
				_ticket.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_ticketReturnsNewIfNotFound = info.GetBoolean("_ticketReturnsNewIfNotFound");
			_alwaysFetchTicket = info.GetBoolean("_alwaysFetchTicket");
			_alreadyFetchedTicket = info.GetBoolean("_alreadyFetchedTicket");

			_vdvLayout = (VdvLayoutEntity)info.GetValue("_vdvLayout", typeof(VdvLayoutEntity));
			if(_vdvLayout!=null)
			{
				_vdvLayout.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_vdvLayoutReturnsNewIfNotFound = info.GetBoolean("_vdvLayoutReturnsNewIfNotFound");
			_alwaysFetchVdvLayout = info.GetBoolean("_alwaysFetchVdvLayout");
			_alreadyFetchedVdvLayout = info.GetBoolean("_alreadyFetchedVdvLayout");

			_issueModeNmType = (VdvTypeEntity)info.GetValue("_issueModeNmType", typeof(VdvTypeEntity));
			if(_issueModeNmType!=null)
			{
				_issueModeNmType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_issueModeNmTypeReturnsNewIfNotFound = info.GetBoolean("_issueModeNmTypeReturnsNewIfNotFound");
			_alwaysFetchIssueModeNmType = info.GetBoolean("_alwaysFetchIssueModeNmType");
			_alreadyFetchedIssueModeNmType = info.GetBoolean("_alreadyFetchedIssueModeNmType");

			_issueModeSeType = (VdvTypeEntity)info.GetValue("_issueModeSeType", typeof(VdvTypeEntity));
			if(_issueModeSeType!=null)
			{
				_issueModeSeType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_issueModeSeTypeReturnsNewIfNotFound = info.GetBoolean("_issueModeSeTypeReturnsNewIfNotFound");
			_alwaysFetchIssueModeSeType = info.GetBoolean("_alwaysFetchIssueModeSeType");
			_alreadyFetchedIssueModeSeType = info.GetBoolean("_alreadyFetchedIssueModeSeType");

			_priorityModeType = (VdvTypeEntity)info.GetValue("_priorityModeType", typeof(VdvTypeEntity));
			if(_priorityModeType!=null)
			{
				_priorityModeType.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priorityModeTypeReturnsNewIfNotFound = info.GetBoolean("_priorityModeTypeReturnsNewIfNotFound");
			_alwaysFetchPriorityModeType = info.GetBoolean("_alwaysFetchPriorityModeType");
			_alreadyFetchedPriorityModeType = info.GetBoolean("_alreadyFetchedPriorityModeType");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((VdvProductFieldIndex)fieldIndex)
			{
				case VdvProductFieldIndex.DistanceAttributeValueID:
					DesetupSyncDistanceAttributeValue(true, false);
					_alreadyFetchedDistanceAttributeValue = false;
					break;
				case VdvProductFieldIndex.IssueModeNm:
					DesetupSyncIssueModeNmType(true, false);
					_alreadyFetchedIssueModeNmType = false;
					break;
				case VdvProductFieldIndex.IssueModeSe:
					DesetupSyncIssueModeSeType(true, false);
					_alreadyFetchedIssueModeSeType = false;
					break;
				case VdvProductFieldIndex.PeriodID:
					DesetupSyncRulePeriod(true, false);
					_alreadyFetchedRulePeriod = false;
					break;
				case VdvProductFieldIndex.PriorityMode:
					DesetupSyncPriorityModeType(true, false);
					_alreadyFetchedPriorityModeType = false;
					break;
				case VdvProductFieldIndex.TariffID:
					DesetupSyncTariff(true, false);
					_alreadyFetchedTariff = false;
					break;
				case VdvProductFieldIndex.TicketID:
					DesetupSyncTicket(true, false);
					_alreadyFetchedTicket = false;
					break;
				case VdvProductFieldIndex.VdvLayoutID:
					DesetupSyncVdvLayout(true, false);
					_alreadyFetchedVdvLayout = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedDistanceAttributeValue = (_distanceAttributeValue != null);
			_alreadyFetchedRulePeriod = (_rulePeriod != null);
			_alreadyFetchedTariff = (_tariff != null);
			_alreadyFetchedTicket = (_ticket != null);
			_alreadyFetchedVdvLayout = (_vdvLayout != null);
			_alreadyFetchedIssueModeNmType = (_issueModeNmType != null);
			_alreadyFetchedIssueModeSeType = (_issueModeSeType != null);
			_alreadyFetchedPriorityModeType = (_priorityModeType != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "DistanceAttributeValue":
					toReturn.Add(Relations.AttributeValueEntityUsingDistanceAttributeValueID);
					break;
				case "RulePeriod":
					toReturn.Add(Relations.RulePeriodEntityUsingPeriodID);
					break;
				case "Tariff":
					toReturn.Add(Relations.TariffEntityUsingTariffID);
					break;
				case "Ticket":
					toReturn.Add(Relations.TicketEntityUsingTicketID);
					break;
				case "VdvLayout":
					toReturn.Add(Relations.VdvLayoutEntityUsingVdvLayoutID);
					break;
				case "IssueModeNmType":
					toReturn.Add(Relations.VdvTypeEntityUsingIssueModeNm);
					break;
				case "IssueModeSeType":
					toReturn.Add(Relations.VdvTypeEntityUsingIssueModeSe);
					break;
				case "PriorityModeType":
					toReturn.Add(Relations.VdvTypeEntityUsingPriorityMode);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_distanceAttributeValue", (!this.MarkedForDeletion?_distanceAttributeValue:null));
			info.AddValue("_distanceAttributeValueReturnsNewIfNotFound", _distanceAttributeValueReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchDistanceAttributeValue", _alwaysFetchDistanceAttributeValue);
			info.AddValue("_alreadyFetchedDistanceAttributeValue", _alreadyFetchedDistanceAttributeValue);
			info.AddValue("_rulePeriod", (!this.MarkedForDeletion?_rulePeriod:null));
			info.AddValue("_rulePeriodReturnsNewIfNotFound", _rulePeriodReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchRulePeriod", _alwaysFetchRulePeriod);
			info.AddValue("_alreadyFetchedRulePeriod", _alreadyFetchedRulePeriod);
			info.AddValue("_tariff", (!this.MarkedForDeletion?_tariff:null));
			info.AddValue("_tariffReturnsNewIfNotFound", _tariffReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTariff", _alwaysFetchTariff);
			info.AddValue("_alreadyFetchedTariff", _alreadyFetchedTariff);
			info.AddValue("_ticket", (!this.MarkedForDeletion?_ticket:null));
			info.AddValue("_ticketReturnsNewIfNotFound", _ticketReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTicket", _alwaysFetchTicket);
			info.AddValue("_alreadyFetchedTicket", _alreadyFetchedTicket);
			info.AddValue("_vdvLayout", (!this.MarkedForDeletion?_vdvLayout:null));
			info.AddValue("_vdvLayoutReturnsNewIfNotFound", _vdvLayoutReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchVdvLayout", _alwaysFetchVdvLayout);
			info.AddValue("_alreadyFetchedVdvLayout", _alreadyFetchedVdvLayout);
			info.AddValue("_issueModeNmType", (!this.MarkedForDeletion?_issueModeNmType:null));
			info.AddValue("_issueModeNmTypeReturnsNewIfNotFound", _issueModeNmTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIssueModeNmType", _alwaysFetchIssueModeNmType);
			info.AddValue("_alreadyFetchedIssueModeNmType", _alreadyFetchedIssueModeNmType);
			info.AddValue("_issueModeSeType", (!this.MarkedForDeletion?_issueModeSeType:null));
			info.AddValue("_issueModeSeTypeReturnsNewIfNotFound", _issueModeSeTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchIssueModeSeType", _alwaysFetchIssueModeSeType);
			info.AddValue("_alreadyFetchedIssueModeSeType", _alreadyFetchedIssueModeSeType);
			info.AddValue("_priorityModeType", (!this.MarkedForDeletion?_priorityModeType:null));
			info.AddValue("_priorityModeTypeReturnsNewIfNotFound", _priorityModeTypeReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriorityModeType", _alwaysFetchPriorityModeType);
			info.AddValue("_alreadyFetchedPriorityModeType", _alreadyFetchedPriorityModeType);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "DistanceAttributeValue":
					_alreadyFetchedDistanceAttributeValue = true;
					this.DistanceAttributeValue = (AttributeValueEntity)entity;
					break;
				case "RulePeriod":
					_alreadyFetchedRulePeriod = true;
					this.RulePeriod = (RulePeriodEntity)entity;
					break;
				case "Tariff":
					_alreadyFetchedTariff = true;
					this.Tariff = (TariffEntity)entity;
					break;
				case "Ticket":
					_alreadyFetchedTicket = true;
					this.Ticket = (TicketEntity)entity;
					break;
				case "VdvLayout":
					_alreadyFetchedVdvLayout = true;
					this.VdvLayout = (VdvLayoutEntity)entity;
					break;
				case "IssueModeNmType":
					_alreadyFetchedIssueModeNmType = true;
					this.IssueModeNmType = (VdvTypeEntity)entity;
					break;
				case "IssueModeSeType":
					_alreadyFetchedIssueModeSeType = true;
					this.IssueModeSeType = (VdvTypeEntity)entity;
					break;
				case "PriorityModeType":
					_alreadyFetchedPriorityModeType = true;
					this.PriorityModeType = (VdvTypeEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "DistanceAttributeValue":
					SetupSyncDistanceAttributeValue(relatedEntity);
					break;
				case "RulePeriod":
					SetupSyncRulePeriod(relatedEntity);
					break;
				case "Tariff":
					SetupSyncTariff(relatedEntity);
					break;
				case "Ticket":
					SetupSyncTicket(relatedEntity);
					break;
				case "VdvLayout":
					SetupSyncVdvLayout(relatedEntity);
					break;
				case "IssueModeNmType":
					SetupSyncIssueModeNmType(relatedEntity);
					break;
				case "IssueModeSeType":
					SetupSyncIssueModeSeType(relatedEntity);
					break;
				case "PriorityModeType":
					SetupSyncPriorityModeType(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "DistanceAttributeValue":
					DesetupSyncDistanceAttributeValue(false, true);
					break;
				case "RulePeriod":
					DesetupSyncRulePeriod(false, true);
					break;
				case "Tariff":
					DesetupSyncTariff(false, true);
					break;
				case "Ticket":
					DesetupSyncTicket(false, true);
					break;
				case "VdvLayout":
					DesetupSyncVdvLayout(false, true);
					break;
				case "IssueModeNmType":
					DesetupSyncIssueModeNmType(false, true);
					break;
				case "IssueModeSeType":
					DesetupSyncIssueModeSeType(false, true);
					break;
				case "PriorityModeType":
					DesetupSyncPriorityModeType(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_distanceAttributeValue!=null)
			{
				toReturn.Add(_distanceAttributeValue);
			}
			if(_rulePeriod!=null)
			{
				toReturn.Add(_rulePeriod);
			}
			if(_tariff!=null)
			{
				toReturn.Add(_tariff);
			}
			if(_ticket!=null)
			{
				toReturn.Add(_ticket);
			}
			if(_vdvLayout!=null)
			{
				toReturn.Add(_vdvLayout);
			}
			if(_issueModeNmType!=null)
			{
				toReturn.Add(_issueModeNmType);
			}
			if(_issueModeSeType!=null)
			{
				toReturn.Add(_issueModeSeType);
			}
			if(_priorityModeType!=null)
			{
				toReturn.Add(_priorityModeType);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 vdvProductID)
		{
			return FetchUsingPK(vdvProductID, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 vdvProductID, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(vdvProductID, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 vdvProductID, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(vdvProductID, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 vdvProductID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(vdvProductID, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.VdvProductID, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new VdvProductRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public AttributeValueEntity GetSingleDistanceAttributeValue()
		{
			return GetSingleDistanceAttributeValue(false);
		}

		/// <summary> Retrieves the related entity of type 'AttributeValueEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AttributeValueEntity' which is related to this entity.</returns>
		public virtual AttributeValueEntity GetSingleDistanceAttributeValue(bool forceFetch)
		{
			if( ( !_alreadyFetchedDistanceAttributeValue || forceFetch || _alwaysFetchDistanceAttributeValue) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AttributeValueEntityUsingDistanceAttributeValueID);
				AttributeValueEntity newEntity = new AttributeValueEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.DistanceAttributeValueID);
				}
				if(fetchResult)
				{
					newEntity = (AttributeValueEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_distanceAttributeValueReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.DistanceAttributeValue = newEntity;
				_alreadyFetchedDistanceAttributeValue = fetchResult;
			}
			return _distanceAttributeValue;
		}


		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public RulePeriodEntity GetSingleRulePeriod()
		{
			return GetSingleRulePeriod(false);
		}

		/// <summary> Retrieves the related entity of type 'RulePeriodEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'RulePeriodEntity' which is related to this entity.</returns>
		public virtual RulePeriodEntity GetSingleRulePeriod(bool forceFetch)
		{
			if( ( !_alreadyFetchedRulePeriod || forceFetch || _alwaysFetchRulePeriod) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.RulePeriodEntityUsingPeriodID);
				RulePeriodEntity newEntity = new RulePeriodEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PeriodID);
				}
				if(fetchResult)
				{
					newEntity = (RulePeriodEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_rulePeriodReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.RulePeriod = newEntity;
				_alreadyFetchedRulePeriod = fetchResult;
			}
			return _rulePeriod;
		}


		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public TariffEntity GetSingleTariff()
		{
			return GetSingleTariff(false);
		}

		/// <summary> Retrieves the related entity of type 'TariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TariffEntity' which is related to this entity.</returns>
		public virtual TariffEntity GetSingleTariff(bool forceFetch)
		{
			if( ( !_alreadyFetchedTariff || forceFetch || _alwaysFetchTariff) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TariffEntityUsingTariffID);
				TariffEntity newEntity = new TariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TariffID);
				}
				if(fetchResult)
				{
					newEntity = (TariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_tariffReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Tariff = newEntity;
				_alreadyFetchedTariff = fetchResult;
			}
			return _tariff;
		}


		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public TicketEntity GetSingleTicket()
		{
			return GetSingleTicket(false);
		}

		/// <summary> Retrieves the related entity of type 'TicketEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TicketEntity' which is related to this entity.</returns>
		public virtual TicketEntity GetSingleTicket(bool forceFetch)
		{
			if( ( !_alreadyFetchedTicket || forceFetch || _alwaysFetchTicket) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TicketEntityUsingTicketID);
				TicketEntity newEntity = new TicketEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TicketID);
				}
				if(fetchResult)
				{
					newEntity = (TicketEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_ticketReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.Ticket = newEntity;
				_alreadyFetchedTicket = fetchResult;
			}
			return _ticket;
		}


		/// <summary> Retrieves the related entity of type 'VdvLayoutEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvLayoutEntity' which is related to this entity.</returns>
		public VdvLayoutEntity GetSingleVdvLayout()
		{
			return GetSingleVdvLayout(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvLayoutEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvLayoutEntity' which is related to this entity.</returns>
		public virtual VdvLayoutEntity GetSingleVdvLayout(bool forceFetch)
		{
			if( ( !_alreadyFetchedVdvLayout || forceFetch || _alwaysFetchVdvLayout) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvLayoutEntityUsingVdvLayoutID);
				VdvLayoutEntity newEntity = new VdvLayoutEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.VdvLayoutID.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (VdvLayoutEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_vdvLayoutReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.VdvLayout = newEntity;
				_alreadyFetchedVdvLayout = fetchResult;
			}
			return _vdvLayout;
		}


		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public VdvTypeEntity GetSingleIssueModeNmType()
		{
			return GetSingleIssueModeNmType(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public virtual VdvTypeEntity GetSingleIssueModeNmType(bool forceFetch)
		{
			if( ( !_alreadyFetchedIssueModeNmType || forceFetch || _alwaysFetchIssueModeNmType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvTypeEntityUsingIssueModeNm);
				VdvTypeEntity newEntity = new VdvTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IssueModeNm);
				}
				if(fetchResult)
				{
					newEntity = (VdvTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_issueModeNmTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IssueModeNmType = newEntity;
				_alreadyFetchedIssueModeNmType = fetchResult;
			}
			return _issueModeNmType;
		}


		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public VdvTypeEntity GetSingleIssueModeSeType()
		{
			return GetSingleIssueModeSeType(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public virtual VdvTypeEntity GetSingleIssueModeSeType(bool forceFetch)
		{
			if( ( !_alreadyFetchedIssueModeSeType || forceFetch || _alwaysFetchIssueModeSeType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvTypeEntityUsingIssueModeSe);
				VdvTypeEntity newEntity = new VdvTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.IssueModeSe);
				}
				if(fetchResult)
				{
					newEntity = (VdvTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_issueModeSeTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.IssueModeSeType = newEntity;
				_alreadyFetchedIssueModeSeType = fetchResult;
			}
			return _issueModeSeType;
		}


		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public VdvTypeEntity GetSinglePriorityModeType()
		{
			return GetSinglePriorityModeType(false);
		}

		/// <summary> Retrieves the related entity of type 'VdvTypeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'VdvTypeEntity' which is related to this entity.</returns>
		public virtual VdvTypeEntity GetSinglePriorityModeType(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriorityModeType || forceFetch || _alwaysFetchPriorityModeType) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.VdvTypeEntityUsingPriorityMode);
				VdvTypeEntity newEntity = new VdvTypeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriorityMode);
				}
				if(fetchResult)
				{
					newEntity = (VdvTypeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priorityModeTypeReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriorityModeType = newEntity;
				_alreadyFetchedPriorityModeType = fetchResult;
			}
			return _priorityModeType;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("DistanceAttributeValue", _distanceAttributeValue);
			toReturn.Add("RulePeriod", _rulePeriod);
			toReturn.Add("Tariff", _tariff);
			toReturn.Add("Ticket", _ticket);
			toReturn.Add("VdvLayout", _vdvLayout);
			toReturn.Add("IssueModeNmType", _issueModeNmType);
			toReturn.Add("IssueModeSeType", _issueModeSeType);
			toReturn.Add("PriorityModeType", _priorityModeType);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="validator">The validator object for this VdvProductEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 vdvProductID, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(vdvProductID, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_distanceAttributeValueReturnsNewIfNotFound = false;
			_rulePeriodReturnsNewIfNotFound = false;
			_tariffReturnsNewIfNotFound = false;
			_ticketReturnsNewIfNotFound = false;
			_vdvLayoutReturnsNewIfNotFound = false;
			_issueModeNmTypeReturnsNewIfNotFound = false;
			_issueModeSeTypeReturnsNewIfNotFound = false;
			_priorityModeTypeReturnsNewIfNotFound = false;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Brtype2Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanionType1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanionType2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Description", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DistanceAttributeValueID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FareScale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssueBarcode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssueChip", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssueModeNm", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IssueModeSe", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyProductOrgID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyVersionAuth", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyVersionKVP", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("KeyVersionPV", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxCompanions1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MaxCompanions2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OwnerClientID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PassengerType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PeriodID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriorityMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductResponsible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SellProductNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceClass", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TransportCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvInfoText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvLayoutID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VdvProductID", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WritePrice", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WriteVAT", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _distanceAttributeValue</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncDistanceAttributeValue(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _distanceAttributeValue, new PropertyChangedEventHandler( OnDistanceAttributeValuePropertyChanged ), "DistanceAttributeValue", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.AttributeValueEntityUsingDistanceAttributeValueIDStatic, true, signalRelatedEntity, "VdvProduct", resetFKFields, new int[] { (int)VdvProductFieldIndex.DistanceAttributeValueID } );		
			_distanceAttributeValue = null;
		}
		
		/// <summary> setups the sync logic for member _distanceAttributeValue</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncDistanceAttributeValue(IEntityCore relatedEntity)
		{
			if(_distanceAttributeValue!=relatedEntity)
			{		
				DesetupSyncDistanceAttributeValue(true, true);
				_distanceAttributeValue = (AttributeValueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _distanceAttributeValue, new PropertyChangedEventHandler( OnDistanceAttributeValuePropertyChanged ), "DistanceAttributeValue", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.AttributeValueEntityUsingDistanceAttributeValueIDStatic, true, ref _alreadyFetchedDistanceAttributeValue, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnDistanceAttributeValuePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _rulePeriod</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncRulePeriod(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.RulePeriodEntityUsingPeriodIDStatic, true, signalRelatedEntity, "VdvProduct", resetFKFields, new int[] { (int)VdvProductFieldIndex.PeriodID } );		
			_rulePeriod = null;
		}
		
		/// <summary> setups the sync logic for member _rulePeriod</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncRulePeriod(IEntityCore relatedEntity)
		{
			if(_rulePeriod!=relatedEntity)
			{		
				DesetupSyncRulePeriod(true, true);
				_rulePeriod = (RulePeriodEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _rulePeriod, new PropertyChangedEventHandler( OnRulePeriodPropertyChanged ), "RulePeriod", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.RulePeriodEntityUsingPeriodIDStatic, true, ref _alreadyFetchedRulePeriod, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnRulePeriodPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _tariff</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTariff(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.TariffEntityUsingTariffIDStatic, true, signalRelatedEntity, "VdvProduct", resetFKFields, new int[] { (int)VdvProductFieldIndex.TariffID } );		
			_tariff = null;
		}
		
		/// <summary> setups the sync logic for member _tariff</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTariff(IEntityCore relatedEntity)
		{
			if(_tariff!=relatedEntity)
			{		
				DesetupSyncTariff(true, true);
				_tariff = (TariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _tariff, new PropertyChangedEventHandler( OnTariffPropertyChanged ), "Tariff", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.TariffEntityUsingTariffIDStatic, true, ref _alreadyFetchedTariff, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTariffPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _ticket</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTicket(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.TicketEntityUsingTicketIDStatic, true, signalRelatedEntity, "VdvProduct", resetFKFields, new int[] { (int)VdvProductFieldIndex.TicketID } );		
			_ticket = null;
		}
		
		/// <summary> setups the sync logic for member _ticket</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTicket(IEntityCore relatedEntity)
		{
			if(_ticket!=relatedEntity)
			{		
				DesetupSyncTicket(true, true);
				_ticket = (TicketEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _ticket, new PropertyChangedEventHandler( OnTicketPropertyChanged ), "Ticket", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.TicketEntityUsingTicketIDStatic, true, ref _alreadyFetchedTicket, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTicketPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _vdvLayout</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncVdvLayout(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _vdvLayout, new PropertyChangedEventHandler( OnVdvLayoutPropertyChanged ), "VdvLayout", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvLayoutEntityUsingVdvLayoutIDStatic, true, signalRelatedEntity, "VdvProducts", resetFKFields, new int[] { (int)VdvProductFieldIndex.VdvLayoutID } );		
			_vdvLayout = null;
		}
		
		/// <summary> setups the sync logic for member _vdvLayout</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncVdvLayout(IEntityCore relatedEntity)
		{
			if(_vdvLayout!=relatedEntity)
			{		
				DesetupSyncVdvLayout(true, true);
				_vdvLayout = (VdvLayoutEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _vdvLayout, new PropertyChangedEventHandler( OnVdvLayoutPropertyChanged ), "VdvLayout", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvLayoutEntityUsingVdvLayoutIDStatic, true, ref _alreadyFetchedVdvLayout, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnVdvLayoutPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _issueModeNmType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIssueModeNmType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _issueModeNmType, new PropertyChangedEventHandler( OnIssueModeNmTypePropertyChanged ), "IssueModeNmType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingIssueModeNmStatic, true, signalRelatedEntity, "VdvProductNmType", resetFKFields, new int[] { (int)VdvProductFieldIndex.IssueModeNm } );		
			_issueModeNmType = null;
		}
		
		/// <summary> setups the sync logic for member _issueModeNmType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIssueModeNmType(IEntityCore relatedEntity)
		{
			if(_issueModeNmType!=relatedEntity)
			{		
				DesetupSyncIssueModeNmType(true, true);
				_issueModeNmType = (VdvTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _issueModeNmType, new PropertyChangedEventHandler( OnIssueModeNmTypePropertyChanged ), "IssueModeNmType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingIssueModeNmStatic, true, ref _alreadyFetchedIssueModeNmType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIssueModeNmTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _issueModeSeType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncIssueModeSeType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _issueModeSeType, new PropertyChangedEventHandler( OnIssueModeSeTypePropertyChanged ), "IssueModeSeType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingIssueModeSeStatic, true, signalRelatedEntity, "VdvProductSeType", resetFKFields, new int[] { (int)VdvProductFieldIndex.IssueModeSe } );		
			_issueModeSeType = null;
		}
		
		/// <summary> setups the sync logic for member _issueModeSeType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncIssueModeSeType(IEntityCore relatedEntity)
		{
			if(_issueModeSeType!=relatedEntity)
			{		
				DesetupSyncIssueModeSeType(true, true);
				_issueModeSeType = (VdvTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _issueModeSeType, new PropertyChangedEventHandler( OnIssueModeSeTypePropertyChanged ), "IssueModeSeType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingIssueModeSeStatic, true, ref _alreadyFetchedIssueModeSeType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnIssueModeSeTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priorityModeType</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriorityModeType(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priorityModeType, new PropertyChangedEventHandler( OnPriorityModeTypePropertyChanged ), "PriorityModeType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingPriorityModeStatic, true, signalRelatedEntity, "VdvProductsPriority", resetFKFields, new int[] { (int)VdvProductFieldIndex.PriorityMode } );		
			_priorityModeType = null;
		}
		
		/// <summary> setups the sync logic for member _priorityModeType</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriorityModeType(IEntityCore relatedEntity)
		{
			if(_priorityModeType!=relatedEntity)
			{		
				DesetupSyncPriorityModeType(true, true);
				_priorityModeType = (VdvTypeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priorityModeType, new PropertyChangedEventHandler( OnPriorityModeTypePropertyChanged ), "PriorityModeType", VarioSL.Entities.RelationClasses.StaticVdvProductRelations.VdvTypeEntityUsingPriorityModeStatic, true, ref _alreadyFetchedPriorityModeType, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriorityModeTypePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="vdvProductID">PK value for VdvProduct which data should be fetched into this VdvProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 vdvProductID, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)VdvProductFieldIndex.VdvProductID].ForcedCurrentValueWrite(vdvProductID);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateVdvProductDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new VdvProductEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static VdvProductRelations Relations
		{
			get	{ return new VdvProductRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AttributeValue'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDistanceAttributeValue
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.AttributeValueCollection(), (IEntityRelation)GetRelationsForField("DistanceAttributeValue")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.AttributeValueEntity, 0, null, null, null, "DistanceAttributeValue", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RulePeriod'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRulePeriod
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.RulePeriodCollection(), (IEntityRelation)GetRelationsForField("RulePeriod")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.RulePeriodEntity, 0, null, null, null, "RulePeriod", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Tariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTariff
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TariffCollection(), (IEntityRelation)GetRelationsForField("Tariff")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.TariffEntity, 0, null, null, null, "Tariff", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Ticket'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTicket
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.TicketCollection(), (IEntityRelation)GetRelationsForField("Ticket")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.TicketEntity, 0, null, null, null, "Ticket", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvLayout'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathVdvLayout
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvLayoutCollection(), (IEntityRelation)GetRelationsForField("VdvLayout")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.VdvLayoutEntity, 0, null, null, null, "VdvLayout", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIssueModeNmType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvTypeCollection(), (IEntityRelation)GetRelationsForField("IssueModeNmType")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.VdvTypeEntity, 0, null, null, null, "IssueModeNmType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIssueModeSeType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvTypeCollection(), (IEntityRelation)GetRelationsForField("IssueModeSeType")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.VdvTypeEntity, 0, null, null, null, "IssueModeSeType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'VdvType'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriorityModeType
		{
			get	{ return new PrefetchPathElement(new VarioSL.Entities.CollectionClasses.VdvTypeCollection(), (IEntityRelation)GetRelationsForField("PriorityModeType")[0], (int)VarioSL.Entities.EntityType.VdvProductEntity, (int)VarioSL.Entities.EntityType.VdvTypeEntity, 0, null, null, null, "PriorityModeType", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Brtype2Id property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."BRTYPE2ID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> Brtype2Id
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvProductFieldIndex.Brtype2Id, false); }
			set	{ SetValue((int)VdvProductFieldIndex.Brtype2Id, value, true); }
		}

		/// <summary> The CompanionType1 property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."COMPANIONTYPE1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CompanionType1
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.CompanionType1, true); }
			set	{ SetValue((int)VdvProductFieldIndex.CompanionType1, value, true); }
		}

		/// <summary> The CompanionType2 property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."COMPANIONTYPE2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 CompanionType2
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.CompanionType2, true); }
			set	{ SetValue((int)VdvProductFieldIndex.CompanionType2, value, true); }
		}

		/// <summary> The Description property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."DESCRIPTION"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 300<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Description
		{
			get { return (System.String)GetValue((int)VdvProductFieldIndex.Description, true); }
			set	{ SetValue((int)VdvProductFieldIndex.Description, value, true); }
		}

		/// <summary> The DistanceAttributeValueID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."DISTANCEATTRIBUTEVALUEID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 DistanceAttributeValueID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.DistanceAttributeValueID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.DistanceAttributeValueID, value, true); }
		}

		/// <summary> The FareScale property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."FARESCALE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 FareScale
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.FareScale, true); }
			set	{ SetValue((int)VdvProductFieldIndex.FareScale, value, true); }
		}

		/// <summary> The IssueBarcode property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."ISSUEBARCODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IssueBarcode
		{
			get { return (System.Boolean)GetValue((int)VdvProductFieldIndex.IssueBarcode, true); }
			set	{ SetValue((int)VdvProductFieldIndex.IssueBarcode, value, true); }
		}

		/// <summary> The IssueChip property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."ISSUECHIP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IssueChip
		{
			get { return (System.Boolean)GetValue((int)VdvProductFieldIndex.IssueChip, true); }
			set	{ SetValue((int)VdvProductFieldIndex.IssueChip, value, true); }
		}

		/// <summary> The IssueModeNm property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."ISSUEMODENM"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 IssueModeNm
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.IssueModeNm, true); }
			set	{ SetValue((int)VdvProductFieldIndex.IssueModeNm, value, true); }
		}

		/// <summary> The IssueModeSe property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."ISSUEMODESE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 IssueModeSe
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.IssueModeSe, true); }
			set	{ SetValue((int)VdvProductFieldIndex.IssueModeSe, value, true); }
		}

		/// <summary> The KeyProductOrgID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."KEYPRODUCTORGID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyProductOrgID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.KeyProductOrgID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.KeyProductOrgID, value, true); }
		}

		/// <summary> The KeyVersionAuth property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."KEYVERSIONAUTH"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyVersionAuth
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.KeyVersionAuth, true); }
			set	{ SetValue((int)VdvProductFieldIndex.KeyVersionAuth, value, true); }
		}

		/// <summary> The KeyVersionKVP property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."KEYVERSIONKVP"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyVersionKVP
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.KeyVersionKVP, true); }
			set	{ SetValue((int)VdvProductFieldIndex.KeyVersionKVP, value, true); }
		}

		/// <summary> The KeyVersionPV property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."KEYVERSIONPV"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 KeyVersionPV
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.KeyVersionPV, true); }
			set	{ SetValue((int)VdvProductFieldIndex.KeyVersionPV, value, true); }
		}

		/// <summary> The MaxCompanions1 property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."MAXCOMPANIONS1"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 MaxCompanions1
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.MaxCompanions1, true); }
			set	{ SetValue((int)VdvProductFieldIndex.MaxCompanions1, value, true); }
		}

		/// <summary> The MaxCompanions2 property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."MAXCOMPANIONS2"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 MaxCompanions2
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.MaxCompanions2, true); }
			set	{ SetValue((int)VdvProductFieldIndex.MaxCompanions2, value, true); }
		}

		/// <summary> The OwnerClientID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."OWNERCLIENTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 OwnerClientID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.OwnerClientID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.OwnerClientID, value, true); }
		}

		/// <summary> The PassengerType property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."PASSENGERTYPE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PassengerType
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.PassengerType, true); }
			set	{ SetValue((int)VdvProductFieldIndex.PassengerType, value, true); }
		}

		/// <summary> The PeriodID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."PERIODID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PeriodID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.PeriodID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.PeriodID, value, true); }
		}

		/// <summary> The PriorityMode property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."PRIORITYMODE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 PriorityMode
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.PriorityMode, true); }
			set	{ SetValue((int)VdvProductFieldIndex.PriorityMode, value, true); }
		}

		/// <summary> The ProductNumber property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."PRODUCTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductNumber
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.ProductNumber, true); }
			set	{ SetValue((int)VdvProductFieldIndex.ProductNumber, value, true); }
		}

		/// <summary> The ProductResponsible property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."PRODUCTRESPONSIBLE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ProductResponsible
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.ProductResponsible, true); }
			set	{ SetValue((int)VdvProductFieldIndex.ProductResponsible, value, true); }
		}

		/// <summary> The SellProductNumber property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."SELLPRODUCTNUMBER"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 SellProductNumber
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.SellProductNumber, true); }
			set	{ SetValue((int)VdvProductFieldIndex.SellProductNumber, value, true); }
		}

		/// <summary> The ServiceClass property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."SERVICECLASS"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 ServiceClass
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.ServiceClass, true); }
			set	{ SetValue((int)VdvProductFieldIndex.ServiceClass, value, true); }
		}

		/// <summary> The TariffID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."TARIFFID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TariffID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.TariffID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.TariffID, value, true); }
		}

		/// <summary> The TicketID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."TICKETID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TicketID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.TicketID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.TicketID, value, true); }
		}

		/// <summary> The TransportCategory property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."TRANSPORTCATEGORY"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int64 TransportCategory
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.TransportCategory, true); }
			set	{ SetValue((int)VdvProductFieldIndex.TransportCategory, value, true); }
		}

		/// <summary> The VdvInfoText property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."VDVINFOTEXT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Varchar2, 0, 0, 1000<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String VdvInfoText
		{
			get { return (System.String)GetValue((int)VdvProductFieldIndex.VdvInfoText, true); }
			set	{ SetValue((int)VdvProductFieldIndex.VdvInfoText, value, true); }
		}

		/// <summary> The VdvLayoutID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."VDVLAYOUTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> VdvLayoutID
		{
			get { return (Nullable<System.Int64>)GetValue((int)VdvProductFieldIndex.VdvLayoutID, false); }
			set	{ SetValue((int)VdvProductFieldIndex.VdvLayoutID, value, true); }
		}

		/// <summary> The VdvProductID property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."VDVPRODUCTID"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 18, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, false</remarks>
		public virtual System.Int64 VdvProductID
		{
			get { return (System.Int64)GetValue((int)VdvProductFieldIndex.VdvProductID, true); }
			set	{ SetValue((int)VdvProductFieldIndex.VdvProductID, value, true); }
		}

		/// <summary> The WritePrice property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."WRITEPRICE"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean WritePrice
		{
			get { return (System.Boolean)GetValue((int)VdvProductFieldIndex.WritePrice, true); }
			set	{ SetValue((int)VdvProductFieldIndex.WritePrice, value, true); }
		}

		/// <summary> The WriteVAT property of the Entity VdvProduct<br/><br/></summary>
		/// <remarks>Mapped on  table field: "TM_VDV_PRODUCT"."WRITEVAT"<br/>
		/// Table field type characteristics (type, precision, scale, length): Decimal, 1, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean WriteVAT
		{
			get { return (System.Boolean)GetValue((int)VdvProductFieldIndex.WriteVAT, true); }
			set	{ SetValue((int)VdvProductFieldIndex.WriteVAT, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AttributeValueEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleDistanceAttributeValue()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual AttributeValueEntity DistanceAttributeValue
		{
			get	{ return GetSingleDistanceAttributeValue(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncDistanceAttributeValue(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProduct", "DistanceAttributeValue", _distanceAttributeValue, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for DistanceAttributeValue. When set to true, DistanceAttributeValue is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DistanceAttributeValue is accessed. You can always execute a forced fetch by calling GetSingleDistanceAttributeValue(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDistanceAttributeValue
		{
			get	{ return _alwaysFetchDistanceAttributeValue; }
			set	{ _alwaysFetchDistanceAttributeValue = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DistanceAttributeValue already has been fetched. Setting this property to false when DistanceAttributeValue has been fetched
		/// will set DistanceAttributeValue to null as well. Setting this property to true while DistanceAttributeValue hasn't been fetched disables lazy loading for DistanceAttributeValue</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDistanceAttributeValue
		{
			get { return _alreadyFetchedDistanceAttributeValue;}
			set 
			{
				if(_alreadyFetchedDistanceAttributeValue && !value)
				{
					this.DistanceAttributeValue = null;
				}
				_alreadyFetchedDistanceAttributeValue = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property DistanceAttributeValue is not found
		/// in the database. When set to true, DistanceAttributeValue will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool DistanceAttributeValueReturnsNewIfNotFound
		{
			get	{ return _distanceAttributeValueReturnsNewIfNotFound; }
			set { _distanceAttributeValueReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'RulePeriodEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleRulePeriod()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual RulePeriodEntity RulePeriod
		{
			get	{ return GetSingleRulePeriod(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncRulePeriod(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProduct", "RulePeriod", _rulePeriod, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for RulePeriod. When set to true, RulePeriod is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RulePeriod is accessed. You can always execute a forced fetch by calling GetSingleRulePeriod(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRulePeriod
		{
			get	{ return _alwaysFetchRulePeriod; }
			set	{ _alwaysFetchRulePeriod = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RulePeriod already has been fetched. Setting this property to false when RulePeriod has been fetched
		/// will set RulePeriod to null as well. Setting this property to true while RulePeriod hasn't been fetched disables lazy loading for RulePeriod</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRulePeriod
		{
			get { return _alreadyFetchedRulePeriod;}
			set 
			{
				if(_alreadyFetchedRulePeriod && !value)
				{
					this.RulePeriod = null;
				}
				_alreadyFetchedRulePeriod = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property RulePeriod is not found
		/// in the database. When set to true, RulePeriod will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool RulePeriodReturnsNewIfNotFound
		{
			get	{ return _rulePeriodReturnsNewIfNotFound; }
			set { _rulePeriodReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTariff()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TariffEntity Tariff
		{
			get	{ return GetSingleTariff(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTariff(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProduct", "Tariff", _tariff, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Tariff. When set to true, Tariff is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Tariff is accessed. You can always execute a forced fetch by calling GetSingleTariff(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTariff
		{
			get	{ return _alwaysFetchTariff; }
			set	{ _alwaysFetchTariff = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Tariff already has been fetched. Setting this property to false when Tariff has been fetched
		/// will set Tariff to null as well. Setting this property to true while Tariff hasn't been fetched disables lazy loading for Tariff</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTariff
		{
			get { return _alreadyFetchedTariff;}
			set 
			{
				if(_alreadyFetchedTariff && !value)
				{
					this.Tariff = null;
				}
				_alreadyFetchedTariff = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Tariff is not found
		/// in the database. When set to true, Tariff will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TariffReturnsNewIfNotFound
		{
			get	{ return _tariffReturnsNewIfNotFound; }
			set { _tariffReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TicketEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTicket()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual TicketEntity Ticket
		{
			get	{ return GetSingleTicket(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTicket(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProduct", "Ticket", _ticket, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for Ticket. When set to true, Ticket is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time Ticket is accessed. You can always execute a forced fetch by calling GetSingleTicket(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTicket
		{
			get	{ return _alwaysFetchTicket; }
			set	{ _alwaysFetchTicket = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property Ticket already has been fetched. Setting this property to false when Ticket has been fetched
		/// will set Ticket to null as well. Setting this property to true while Ticket hasn't been fetched disables lazy loading for Ticket</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTicket
		{
			get { return _alreadyFetchedTicket;}
			set 
			{
				if(_alreadyFetchedTicket && !value)
				{
					this.Ticket = null;
				}
				_alreadyFetchedTicket = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property Ticket is not found
		/// in the database. When set to true, Ticket will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool TicketReturnsNewIfNotFound
		{
			get	{ return _ticketReturnsNewIfNotFound; }
			set { _ticketReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvLayoutEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleVdvLayout()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvLayoutEntity VdvLayout
		{
			get	{ return GetSingleVdvLayout(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncVdvLayout(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProducts", "VdvLayout", _vdvLayout, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for VdvLayout. When set to true, VdvLayout is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time VdvLayout is accessed. You can always execute a forced fetch by calling GetSingleVdvLayout(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchVdvLayout
		{
			get	{ return _alwaysFetchVdvLayout; }
			set	{ _alwaysFetchVdvLayout = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property VdvLayout already has been fetched. Setting this property to false when VdvLayout has been fetched
		/// will set VdvLayout to null as well. Setting this property to true while VdvLayout hasn't been fetched disables lazy loading for VdvLayout</summary>
		[Browsable(false)]
		public bool AlreadyFetchedVdvLayout
		{
			get { return _alreadyFetchedVdvLayout;}
			set 
			{
				if(_alreadyFetchedVdvLayout && !value)
				{
					this.VdvLayout = null;
				}
				_alreadyFetchedVdvLayout = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property VdvLayout is not found
		/// in the database. When set to true, VdvLayout will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool VdvLayoutReturnsNewIfNotFound
		{
			get	{ return _vdvLayoutReturnsNewIfNotFound; }
			set { _vdvLayoutReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIssueModeNmType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvTypeEntity IssueModeNmType
		{
			get	{ return GetSingleIssueModeNmType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIssueModeNmType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProductNmType", "IssueModeNmType", _issueModeNmType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IssueModeNmType. When set to true, IssueModeNmType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IssueModeNmType is accessed. You can always execute a forced fetch by calling GetSingleIssueModeNmType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIssueModeNmType
		{
			get	{ return _alwaysFetchIssueModeNmType; }
			set	{ _alwaysFetchIssueModeNmType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IssueModeNmType already has been fetched. Setting this property to false when IssueModeNmType has been fetched
		/// will set IssueModeNmType to null as well. Setting this property to true while IssueModeNmType hasn't been fetched disables lazy loading for IssueModeNmType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIssueModeNmType
		{
			get { return _alreadyFetchedIssueModeNmType;}
			set 
			{
				if(_alreadyFetchedIssueModeNmType && !value)
				{
					this.IssueModeNmType = null;
				}
				_alreadyFetchedIssueModeNmType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IssueModeNmType is not found
		/// in the database. When set to true, IssueModeNmType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool IssueModeNmTypeReturnsNewIfNotFound
		{
			get	{ return _issueModeNmTypeReturnsNewIfNotFound; }
			set { _issueModeNmTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleIssueModeSeType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvTypeEntity IssueModeSeType
		{
			get	{ return GetSingleIssueModeSeType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncIssueModeSeType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProductSeType", "IssueModeSeType", _issueModeSeType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for IssueModeSeType. When set to true, IssueModeSeType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IssueModeSeType is accessed. You can always execute a forced fetch by calling GetSingleIssueModeSeType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIssueModeSeType
		{
			get	{ return _alwaysFetchIssueModeSeType; }
			set	{ _alwaysFetchIssueModeSeType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IssueModeSeType already has been fetched. Setting this property to false when IssueModeSeType has been fetched
		/// will set IssueModeSeType to null as well. Setting this property to true while IssueModeSeType hasn't been fetched disables lazy loading for IssueModeSeType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIssueModeSeType
		{
			get { return _alreadyFetchedIssueModeSeType;}
			set 
			{
				if(_alreadyFetchedIssueModeSeType && !value)
				{
					this.IssueModeSeType = null;
				}
				_alreadyFetchedIssueModeSeType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property IssueModeSeType is not found
		/// in the database. When set to true, IssueModeSeType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool IssueModeSeTypeReturnsNewIfNotFound
		{
			get	{ return _issueModeSeTypeReturnsNewIfNotFound; }
			set { _issueModeSeTypeReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'VdvTypeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriorityModeType()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(false)]
		public virtual VdvTypeEntity PriorityModeType
		{
			get	{ return GetSinglePriorityModeType(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriorityModeType(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "VdvProductsPriority", "PriorityModeType", _priorityModeType, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriorityModeType. When set to true, PriorityModeType is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriorityModeType is accessed. You can always execute a forced fetch by calling GetSinglePriorityModeType(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriorityModeType
		{
			get	{ return _alwaysFetchPriorityModeType; }
			set	{ _alwaysFetchPriorityModeType = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriorityModeType already has been fetched. Setting this property to false when PriorityModeType has been fetched
		/// will set PriorityModeType to null as well. Setting this property to true while PriorityModeType hasn't been fetched disables lazy loading for PriorityModeType</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriorityModeType
		{
			get { return _alreadyFetchedPriorityModeType;}
			set 
			{
				if(_alreadyFetchedPriorityModeType && !value)
				{
					this.PriorityModeType = null;
				}
				_alreadyFetchedPriorityModeType = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriorityModeType is not found
		/// in the database. When set to true, PriorityModeType will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: false.</summary>
		[Browsable(false)]
		public bool PriorityModeTypeReturnsNewIfNotFound
		{
			get	{ return _priorityModeTypeReturnsNewIfNotFound; }
			set { _priorityModeTypeReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the VarioSL.Entities.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)VarioSL.Entities.EntityType.VdvProductEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
