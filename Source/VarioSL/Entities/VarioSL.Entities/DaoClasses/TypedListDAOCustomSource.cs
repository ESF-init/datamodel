﻿
namespace VarioSL.Entities.DaoClasses
{
	public class TypedListDAOCustomSource : TypedListDAO
	{

		private string _connectionString;

		public string ConnectionString
		{
			get { return _connectionString; }
			set { _connectionString = value; }
		}

		public TypedListDAOCustomSource()
			: base()
		{
			;
		}

		public TypedListDAOCustomSource(string connectionString)
			: this()
		{
			_connectionString = connectionString;
		}

		protected override string GetConnectionString()
		{
			return _connectionString ?? base.GetConnectionString();
		}

	}
}
