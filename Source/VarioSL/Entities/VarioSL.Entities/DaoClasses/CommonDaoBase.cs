﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.Oracle;

namespace VarioSL.Entities.DaoClasses
{
	/// <summary>DAO base class, used for all generated DAO classes.</summary>
	public partial class CommonDaoBase : DaoBase
	{
		#region Class Member Definitions
		/// <summary>The connection string to use in the SelfServicing code. This is a global setting and is automatically set with the value read from the config file.</summary>
		public static string ActualConnectionString = string.Empty;
		#endregion
	
		#region Constants
		private const string connectionKeyString = "ConnectionString.Oracle (ODP.NET)";
		#endregion
		
		/// <summary>CTor</summary>
		/// <remarks>CTor to use for non-entity specific dao usage</remarks>
		public CommonDaoBase() : this(InheritanceHierarchyType.None, string.Empty, null)
		{
		}
	
		/// <summary>CTor</summary>
		/// <param name="typeOfInheritance">Type of inheritance the entity which owns this Dao is in.</param>
		/// <param name="entityName">Name of the entity owning this Dao</param>
		/// <param name="entityFactory">Entity factory for the entity owning this Dao.</param>
		public CommonDaoBase(InheritanceHierarchyType typeOfInheritance, string entityName, IEntityFactory entityFactory)
				: base(InheritanceInfoProviderSingleton.GetInstance(), new DynamicQueryEngine(), typeOfInheritance, entityName, entityFactory)
		{
		}
		
		/// <summary>Sets the default compatibility level used by the DQE. Default is Oracle9i10g11g. This is a global setting.
		/// Compatibility level influences the query generated for paging and identity sequence usage in inserts. </summary>
		/// <remarks>Setting this property will overrule a similar setting in the .config file. Don't set this property when queries are executed as
		/// it might switch factories for ADO.NET elements which could result in undefined behavior so set this property at startup of your application</remarks>
		public static void SetOracleCompatibilityLevel(OracleCompatibilityLevel compatibilityLevel)
		{
			DynamicQueryEngine.DefaultCompatibilityLevel = compatibilityLevel;
		} 

		/// <summary>Gets the connection string.</summary>
		/// <returns>the connection string as set by code or in the config file.</returns>
		protected override string GetConnectionString()
		{
			if(String.IsNullOrEmpty(ActualConnectionString))
			{
				ActualConnectionString = ConfigFileHelper.ReadConnectionStringFromConfig(connectionKeyString);
			}
			return ActualConnectionString;
		}
		
		#region Included Code

		#endregion
	}
}
