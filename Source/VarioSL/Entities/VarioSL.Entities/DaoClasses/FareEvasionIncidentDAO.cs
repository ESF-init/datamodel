﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.Oracle;

namespace VarioSL.Entities.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the FareEvasionIncident Entity. It will perform database oriented actions for a entity of type 'FareEvasionIncidentEntity'.</summary>
	public partial class FareEvasionIncidentDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public FareEvasionIncidentDAO() : base(InheritanceHierarchyType.None, "FareEvasionIncidentEntity", new FareEvasionIncidentEntityFactory())
		{
		}


		/// <summary>Reads an entity based on a unique constraint. Which entity is read is determined from the passed in fields for the UniqueConstraint.</summary>
		/// <param name="entityToFetch">The entity to fetch. Contained data will be overwritten.</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="incidentNumber">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="contextToUse">The context to fetch the prefetch path with.</param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		public void FetchFareEvasionIncidentUsingUCIncidentNumber(IEntity entityToFetch, ITransaction containingTransaction, System.String incidentNumber, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(entityToFetch.Fields[(int)FareEvasionIncidentFieldIndex.IncidentNumber], ComparisonOperator.Equal, incidentNumber)); 
			this.PerformFetchEntityAction(entityToFetch, containingTransaction, selectFilter, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}


		/// <summary>Retrieves in the calling FareEvasionIncidentCollection object all FareEvasionIncidentEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="binaryDataInstance">BinaryDataEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="debtorInstance">DebtorEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="fareEvasionReasonInstance">FareEvasionReasonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="contractInstance">ContractEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="fareChangeCauseInstance">FareChangeCauseEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="fareEvasionBehaviourInstance">FareEvasionBehaviourEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="identificationTypeInstance">IdentificationTypeEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="inspectionReportInstance">InspectionReportEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="guardianInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="personInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="transportCompanyInstance">TransportCompanyEntity instance to use as a filter for the FareEvasionIncidentEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity binaryDataInstance, IEntity clientInstance, IEntity debtorInstance, IEntity fareEvasionCategoryInstance, IEntity fareEvasionReasonInstance, IEntity contractInstance, IEntity fareChangeCauseInstance, IEntity fareEvasionBehaviourInstance, IEntity identificationTypeInstance, IEntity inspectionReportInstance, IEntity guardianInstance, IEntity personInstance, IEntity transportCompanyInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.FareEvasionIncidentEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(binaryDataInstance, clientInstance, debtorInstance, fareEvasionCategoryInstance, fareEvasionReasonInstance, contractInstance, fareChangeCauseInstance, fareEvasionBehaviourInstance, identificationTypeInstance, inspectionReportInstance, guardianInstance, personInstance, transportCompanyInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'FareEvasionIncident' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="binaryDataInstance">BinaryDataEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="debtorInstance">DebtorEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="fareEvasionReasonInstance">FareEvasionReasonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="contractInstance">ContractEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="fareChangeCauseInstance">FareChangeCauseEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="fareEvasionBehaviourInstance">FareEvasionBehaviourEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="identificationTypeInstance">IdentificationTypeEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="inspectionReportInstance">InspectionReportEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="guardianInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="personInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <param name="transportCompanyInstance">TransportCompanyEntity instance to use as a filter for the FareEvasionIncidentEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity binaryDataInstance, IEntity clientInstance, IEntity debtorInstance, IEntity fareEvasionCategoryInstance, IEntity fareEvasionReasonInstance, IEntity contractInstance, IEntity fareChangeCauseInstance, IEntity fareEvasionBehaviourInstance, IEntity identificationTypeInstance, IEntity inspectionReportInstance, IEntity guardianInstance, IEntity personInstance, IEntity transportCompanyInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.FareEvasionIncidentEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(binaryDataInstance, clientInstance, debtorInstance, fareEvasionCategoryInstance, fareEvasionReasonInstance, contractInstance, fareChangeCauseInstance, fareEvasionBehaviourInstance, identificationTypeInstance, inspectionReportInstance, guardianInstance, personInstance, transportCompanyInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="binaryDataInstance">BinaryDataEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="debtorInstance">DebtorEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="fareEvasionReasonInstance">FareEvasionReasonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="contractInstance">ContractEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="fareChangeCauseInstance">FareChangeCauseEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="fareEvasionBehaviourInstance">FareEvasionBehaviourEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="identificationTypeInstance">IdentificationTypeEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="inspectionReportInstance">InspectionReportEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="guardianInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="personInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <param name="transportCompanyInstance">TransportCompanyEntity instance to use as a filter for the FareEvasionIncidentEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity binaryDataInstance, IEntity clientInstance, IEntity debtorInstance, IEntity fareEvasionCategoryInstance, IEntity fareEvasionReasonInstance, IEntity contractInstance, IEntity fareChangeCauseInstance, IEntity fareEvasionBehaviourInstance, IEntity identificationTypeInstance, IEntity inspectionReportInstance, IEntity guardianInstance, IEntity personInstance, IEntity transportCompanyInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.FareEvasionIncidentEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(binaryDataInstance, clientInstance, debtorInstance, fareEvasionCategoryInstance, fareEvasionReasonInstance, contractInstance, fareChangeCauseInstance, fareEvasionBehaviourInstance, identificationTypeInstance, inspectionReportInstance, guardianInstance, personInstance, transportCompanyInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="binaryDataInstance">BinaryDataEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="debtorInstance">DebtorEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="fareEvasionReasonInstance">FareEvasionReasonEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="contractInstance">ContractEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="fareChangeCauseInstance">FareChangeCauseEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="fareEvasionBehaviourInstance">FareEvasionBehaviourEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="identificationTypeInstance">IdentificationTypeEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="inspectionReportInstance">InspectionReportEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="guardianInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="personInstance">PersonEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="transportCompanyInstance">TransportCompanyEntity instance to use as a filter for the FareEvasionIncidentEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity binaryDataInstance, IEntity clientInstance, IEntity debtorInstance, IEntity fareEvasionCategoryInstance, IEntity fareEvasionReasonInstance, IEntity contractInstance, IEntity fareChangeCauseInstance, IEntity fareEvasionBehaviourInstance, IEntity identificationTypeInstance, IEntity inspectionReportInstance, IEntity guardianInstance, IEntity personInstance, IEntity transportCompanyInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(binaryDataInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.BinaryDataID], ComparisonOperator.Equal, ((BinaryDataEntity)binaryDataInstance).BinaryDataID));
			}
			if(clientInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.ClientID], ComparisonOperator.Equal, ((ClientEntity)clientInstance).ClientID));
			}
			if(debtorInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.DebtorID], ComparisonOperator.Equal, ((DebtorEntity)debtorInstance).DebtorID));
			}
			if(fareEvasionCategoryInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.FareEvasionCategoryID], ComparisonOperator.Equal, ((FareEvasionCategoryEntity)fareEvasionCategoryInstance).FareEvasionCategoryID));
			}
			if(fareEvasionReasonInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.FareEvasionReasonID], ComparisonOperator.Equal, ((FareEvasionReasonEntity)fareEvasionReasonInstance).FareEvasionReasonID));
			}
			if(contractInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.ContractID], ComparisonOperator.Equal, ((ContractEntity)contractInstance).ContractID));
			}
			if(fareChangeCauseInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.FareChangeCauseID], ComparisonOperator.Equal, ((FareChangeCauseEntity)fareChangeCauseInstance).FareChangeCauseID));
			}
			if(fareEvasionBehaviourInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.FareEvasionBehaviourID], ComparisonOperator.Equal, ((FareEvasionBehaviourEntity)fareEvasionBehaviourInstance).FareEvasionBehaviourID));
			}
			if(identificationTypeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.IdentificationTypeID], ComparisonOperator.Equal, ((IdentificationTypeEntity)identificationTypeInstance).IdentificationTypeID));
			}
			if(inspectionReportInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.InspectionReportID], ComparisonOperator.Equal, ((InspectionReportEntity)inspectionReportInstance).InspectionReportID));
			}
			if(guardianInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.GuardianID], ComparisonOperator.Equal, ((PersonEntity)guardianInstance).PersonID));
			}
			if(personInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.PersonID], ComparisonOperator.Equal, ((PersonEntity)personInstance).PersonID));
			}
			if(transportCompanyInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)FareEvasionIncidentFieldIndex.TransportCompanyID], ComparisonOperator.Equal, ((TransportCompanyEntity)transportCompanyInstance).TransportCompanyID));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
