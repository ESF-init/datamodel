﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.Oracle;

namespace VarioSL.Entities.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Ticket Entity. It will perform database oriented actions for a entity of type 'TicketEntity'.</summary>
	public partial class TicketDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public TicketDAO() : base(InheritanceHierarchyType.None, "TicketEntity", new TicketEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in AttributeValueEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingAttributevalueCollectionViaAttributeValueList(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity attributeValueInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.AttributeValueListEntityUsingTicketID, "AttributeValueList_");
			relations.Add(AttributeValueListEntity.Relations.AttributeValueEntityUsingAttributeValueID, "AttributeValueList_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(attributeValueInstance.Fields[(int)AttributeValueFieldIndex.AttributeValueID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in CardChipTypeEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCardChipTypeCollectionViaTicketPhysicalCardType(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity cardChipTypeInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketPhysicalCardTypeEntityUsingTicketID, "TicketPhysicalCardType_");
			relations.Add(TicketPhysicalCardTypeEntity.Relations.CardChipTypeEntityUsingPhysicalCardTypeID, "TicketPhysicalCardType_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(cardChipTypeInstance.Fields[(int)CardChipTypeFieldIndex.CardChipTypeID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in CardTicketEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingCardAdditionsTicketIsUsedIn(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity cardTicketInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.CardTicketToTicketEntityUsingTicketID, "CardTicketToTicket_");
			relations.Add(CardTicketToTicketEntity.Relations.CardTicketEntityUsingCardTicketID, "CardTicketToTicket_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(cardTicketInstance.Fields[(int)CardTicketFieldIndex.CardTicketID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in ClientEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingClientCollectionViaTicketVendingClient(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity clientInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketVendingClientEntityUsingTicketid, "TicketVendingClient_");
			relations.Add(TicketVendingClientEntity.Relations.ClientEntityUsingClientid, "TicketVendingClient_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(clientInstance.Fields[(int)ClientFieldIndex.ClientID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in LineEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingLineCollectionViaTicketServicesPermitted(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity lineInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketServicesPermittedEntityUsingTicketID, "TicketServicesPermitted_");
			relations.Add(TicketServicesPermittedEntity.Relations.LineEntityUsingLineID, "TicketServicesPermitted_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(lineInstance.Fields[(int)LineFieldIndex.LineID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in PaymentIntervalEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingPaymentIntervalCollectionViaTicketPaymentInterval(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity paymentIntervalInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketPaymentIntervalEntityUsingTicketID, "TicketPaymentInterval_");
			relations.Add(TicketPaymentIntervalEntity.Relations.PaymentIntervalEntityUsingPaymentIntervalID, "TicketPaymentInterval_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(paymentIntervalInstance.Fields[(int)PaymentIntervalFieldIndex.PaymentIntervalID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in RuleCappingEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingRuleCappingCollectionViaRuleCappingToTicket(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity ruleCappingInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.RuleCappingToTicketEntityUsingTicketID, "RuleCappingToTicket_");
			relations.Add(RuleCappingToTicketEntity.Relations.RuleCappingEntityUsingRuleCappingID, "RuleCappingToTicket_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(ruleCappingInstance.Fields[(int)RuleCappingFieldIndex.RuleCappingID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in TicketGroupEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingTicketGroupCollectionViaTicketToGroup(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity ticketGroupInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketToGroupEntityUsingTicketID, "TicketToGroup_");
			relations.Add(TicketToGroupEntity.Relations.TicketGroupEntityUsingTicketGroupID, "TicketToGroup_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(ticketGroupInstance.Fields[(int)TicketGroupFieldIndex.TicketGroupID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in OrganizationEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingOrganizationCollectionViaTicketOrganization(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity organizationInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.TicketOrganizationEntityUsingTicketID, "TicketOrganization_");
			relations.Add(TicketOrganizationEntity.Relations.OrganizationEntityUsingOrganizationID, "TicketOrganization_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(organizationInstance.Fields[(int)OrganizationFieldIndex.OrganizationID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketCollection object all TicketEntity objects which are related via a relation of type 'm:n' with the passed in SettlementQuerySettingEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingSettlementQuerySettings(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity settlementQuerySettingInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketEntity.Relations.SettlementQuerySettingToTicketEntityUsingTicketID, "SettlementQuerySettingToTicket_");
			relations.Add(SettlementQuerySettingToTicketEntity.Relations.SettlementQuerySettingEntityUsingSettlementQuerySettingID, "SettlementQuerySettingToTicket_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(settlementQuerySettingInstance.Fields[(int)SettlementQuerySettingFieldIndex.SettlementQuerySettingID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Deletes from the persistent storage all 'Ticket' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(businessRuleInspectionInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.BrType10ID], ComparisonOperator.Equal, ((BusinessRuleEntity)businessRuleInspectionInstance).BusinessRuleID));
			}
			if(businessRuleTestBoardingInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.BrType2ID], ComparisonOperator.Equal, ((BusinessRuleEntity)businessRuleTestBoardingInstance).BusinessRuleID));
			}
			if(businessRuleInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.BrType1ID], ComparisonOperator.Equal, ((BusinessRuleEntity)businessRuleInstance).BusinessRuleID));
			}
			if(businessRuleTicketAssignmentInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.BrType9ID], ComparisonOperator.Equal, ((BusinessRuleEntity)businessRuleTicketAssignmentInstance).BusinessRuleID));
			}
			if(calendarInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.CalendarID], ComparisonOperator.Equal, ((CalendarEntity)calendarInstance).CalendarID));
			}
			if(clientInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.OwnerClientID], ComparisonOperator.Equal, ((ClientEntity)clientInstance).ClientID));
			}
			if(deviceClassInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.DeviceClassID], ComparisonOperator.Equal, ((DeviceClassEntity)deviceClassInstance).DeviceClassID));
			}
			if(eticketInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.EtickID], ComparisonOperator.Equal, ((EticketEntity)eticketInstance).EtickID));
			}
			if(fareEvasionCategoryInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.FareEvasionCategoryID], ComparisonOperator.Equal, ((FareEvasionCategoryEntity)fareEvasionCategoryInstance).FareEvasionCategoryID));
			}
			if(fareMatrixInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.MatrixID], ComparisonOperator.Equal, ((FareMatrixEntity)fareMatrixInstance).FareMatrixID));
			}
			if(fareTable1Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.FareTableID], ComparisonOperator.Equal, ((FareTableEntity)fareTable1Instance).FareTableID));
			}
			if(fareTable2Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.FareTable2ID], ComparisonOperator.Equal, ((FareTableEntity)fareTable2Instance).FareTableID));
			}
			if(cancellationLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.CancellationLayoutID], ComparisonOperator.Equal, ((LayoutEntity)cancellationLayoutInstance).LayoutID));
			}
			if(groupLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.GroupLayoutID], ComparisonOperator.Equal, ((LayoutEntity)groupLayoutInstance).LayoutID));
			}
			if(printLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.PrintLayoutID], ComparisonOperator.Equal, ((LayoutEntity)printLayoutInstance).LayoutID));
			}
			if(receiptLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.ReceiptLayoutID], ComparisonOperator.Equal, ((LayoutEntity)receiptLayoutInstance).LayoutID));
			}
			if(lineGroupInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.LineGroupID], ComparisonOperator.Equal, ((LineGroupEntity)lineGroupInstance).LineGroupID));
			}
			if(pageContentGroupInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.PageContentGroupID], ComparisonOperator.Equal, ((PageContentGroupEntity)pageContentGroupInstance).PageContentGroupID));
			}
			if(priceTypeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.PriceTypeID], ComparisonOperator.Equal, ((PriceTypeEntity)priceTypeInstance).PriceTypeID));
			}
			if(rulePeriodInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.RulePeriodID], ComparisonOperator.Equal, ((RulePeriodEntity)rulePeriodInstance).RulePeriodID));
			}
			if(tariffInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.TarifID], ComparisonOperator.Equal, ((TariffEntity)tariffInstance).TarifID));
			}
			if(temporalTypeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.TemporalTypeID], ComparisonOperator.Equal, ((TemporalTypeEntity)temporalTypeInstance).TemporalTypeID));
			}
			if(ticketCancellationTypeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.TicketCancellationTypeID], ComparisonOperator.Equal, ((TicketCancellationTypeEntity)ticketCancellationTypeInstance).TicketCancellationTypeID));
			}
			if(ticketCategoryInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.CategoryID], ComparisonOperator.Equal, ((TicketCategoryEntity)ticketCategoryInstance).TicketCategoryID));
			}
			if(ticketSelectionModeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.SelectionMode], ComparisonOperator.Equal, ((TicketSelectionModeEntity)ticketSelectionModeInstance).SelectionModeNumber));
			}
			if(ticketTypeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.TicketTypeID], ComparisonOperator.Equal, ((TicketTypeEntity)ticketTypeInstance).TicketTypeID));
			}
			if(userKey1Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.Key1], ComparisonOperator.Equal, ((UserKeyEntity)userKey1Instance).KeyID));
			}
			if(userKey2Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketFieldIndex.Key2], ComparisonOperator.Equal, ((UserKeyEntity)userKey2Instance).KeyID));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
