﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.Oracle;

namespace VarioSL.Entities.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the TicketDeviceClass Entity. It will perform database oriented actions for a entity of type 'TicketDeviceClassEntity'.</summary>
	public partial class TicketDeviceClassDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public TicketDeviceClassDAO() : base(InheritanceHierarchyType.None, "TicketDeviceClassEntity", new TicketDeviceClassEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling TicketDeviceClassCollection object all TicketDeviceClassEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketDeviceClassEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}


		/// <summary>Retrieves in the calling TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a relation of type 'm:n' with the passed in DevicePaymentMethodEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingDevicePaymentMethodCollection(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity devicePaymentMethodInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketDeviceClassEntity.Relations.TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID, "TicketDeviceClassPaymentMethod_");
			relations.Add(TicketDeviceClassPaymentMethodEntity.Relations.DevicePaymentMethodEntityUsingDevicePaymentMethodID, "TicketDeviceClassPaymentMethod_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(devicePaymentMethodInstance.Fields[(int)DevicePaymentMethodFieldIndex.DevicePaymentMethodID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}

		/// <summary>Retrieves in the calling TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a relation of type 'm:n' with the passed in OutputDeviceEntity.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiUsingOutputDevices(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IEntity outputDeviceInstance, IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
		{
			RelationCollection relations = new RelationCollection();
			relations.Add(TicketDeviceClassEntity.Relations.TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID, "TicketDeviceClassOutputDevice_");
			relations.Add(TicketDeviceClassOutputDeviceEntity.Relations.OutputDeviceEntityUsingOutputDeviceID, "TicketDeviceClassOutputDevice_", string.Empty, JoinHint.None);
			IPredicateExpression selectFilter = new PredicateExpression();
			selectFilter.Add(new FieldCompareValuePredicate(outputDeviceInstance.Fields[(int)OutputDeviceFieldIndex.OutputDeviceID], ComparisonOperator.Equal));
			return this.GetMulti(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, entityFactoryToUse, selectFilter, relations, prefetchPathToUse, pageNumber, pageSize);
		}


		/// <summary>Deletes from the persistent storage all 'TicketDeviceClass' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketDeviceClassEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TicketDeviceClassEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(calendarInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.CalendarID], ComparisonOperator.Equal, ((CalendarEntity)calendarInstance).CalendarID));
			}
			if(deviceClassInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.DeviceClassID], ComparisonOperator.Equal, ((DeviceClassEntity)deviceClassInstance).DeviceClassID));
			}
			if(eticketInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.EtickID], ComparisonOperator.Equal, ((EticketEntity)eticketInstance).EtickID));
			}
			if(fareMatrixInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.MatrixID], ComparisonOperator.Equal, ((FareMatrixEntity)fareMatrixInstance).FareMatrixID));
			}
			if(cancellationLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.CancellationLayoutID], ComparisonOperator.Equal, ((LayoutEntity)cancellationLayoutInstance).LayoutID));
			}
			if(groupLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.GroupLayoutID], ComparisonOperator.Equal, ((LayoutEntity)groupLayoutInstance).LayoutID));
			}
			if(printLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.PrintLayoutID], ComparisonOperator.Equal, ((LayoutEntity)printLayoutInstance).LayoutID));
			}
			if(receiptLayoutInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.ReceiptLayoutID], ComparisonOperator.Equal, ((LayoutEntity)receiptLayoutInstance).LayoutID));
			}
			if(rulePeriodInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.RulePeriodID], ComparisonOperator.Equal, ((RulePeriodEntity)rulePeriodInstance).RulePeriodID));
			}
			if(ticketInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.TicketID], ComparisonOperator.Equal, ((TicketEntity)ticketInstance).TicketID));
			}
			if(ticketSelectionModeInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.SelectionMode], ComparisonOperator.Equal, ((TicketSelectionModeEntity)ticketSelectionModeInstance).SelectionModeNumber));
			}
			if(userKey1Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.Key1], ComparisonOperator.Equal, ((UserKeyEntity)userKey1Instance).KeyID));
			}
			if(userKey2Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TicketDeviceClassFieldIndex.Key2], ComparisonOperator.Equal, ((UserKeyEntity)userKey2Instance).KeyID));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
