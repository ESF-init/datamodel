﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.Oracle;

namespace VarioSL.Entities.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the TransactionJournal Entity. It will perform database oriented actions for a entity of type 'TransactionJournalEntity'.</summary>
	public partial class TransactionJournalDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public TransactionJournalDAO() : base(InheritanceHierarchyType.None, "TransactionJournalEntity", new TransactionJournalEntityFactory())
		{
		}



		/// <summary>Retrieves in the calling TransactionJournalCollection object all TransactionJournalEntity objects which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter. </summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="collectionToFill">Collection to fill with the entity objects retrieved</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query. When set to 0, no limitations are specified.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		public bool GetMulti(ITransaction containingTransaction, IEntityCollection collectionToFill, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IEntityFactory entityFactoryToUse, IPredicateExpression filter, IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance, int pageNumber, int pageSize)
		{
			this.EntityFactoryToUse = entityFactoryToUse;
			IEntityFields fieldsToReturn = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TransactionJournalEntity);
			IPredicateExpression selectFilter = CreateFilterUsingForeignKeys(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, fieldsToReturn);
			if(filter!=null)
			{
				selectFilter.AddWithAnd(filter);
			}
			return this.PerformGetMultiAction(containingTransaction, collectionToFill, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, null, null, pageNumber, pageSize);
		}




		/// <summary>Deletes from the persistent storage all 'TransactionJournal' entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to delete</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMulti(ITransaction containingTransaction, IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TransactionJournalEntity);
			IPredicateExpression deleteFilter = CreateFilterUsingForeignKeys(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, fields);
			return this.DeleteMulti(containingTransaction, deleteFilter);
		}

		/// <summary>Updates all entities of the same type or subtype of the entity <i>entityWithNewValues</i> directly in the persistent storage if they match the filter
		/// supplied in <i>filterBucket</i>. Only the fields changed in entityWithNewValues are updated for these fields. Entities of a subtype of the type
		/// of <i>entityWithNewValues</i> which are affected by the filterBucket's filter will thus also be updated.</summary>
		/// <param name="entityWithNewValues">IEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="containingTransaction">A containing transaction, if caller is added to a transaction, or null if not.</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to update</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMulti(IEntity entityWithNewValues, ITransaction containingTransaction, IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance)
		{
			IEntityFields fields = EntityFieldsFactory.CreateEntityFieldsObject(VarioSL.Entities.EntityType.TransactionJournalEntity);
			IPredicateExpression updateFilter = CreateFilterUsingForeignKeys(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, fields);
			return this.UpdateMulti(entityWithNewValues, containingTransaction, updateFilter);
		}

		/// <summary>Creates a PredicateExpression which should be used as a filter when any combination of available foreign keys is specified.</summary>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects</param>
		/// <param name="fieldsToReturn">IEntityFields implementation which forms the definition of the fieldset of the target entity.</param>
		/// <returns>A ready to use PredicateExpression based on the passed in foreign key value holders.</returns>
		private IPredicateExpression CreateFilterUsingForeignKeys(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance, IEntityFields fieldsToReturn)
		{
			IPredicateExpression selectFilter = new PredicateExpression();
			
			if(clientInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.ClientID], ComparisonOperator.Equal, ((ClientEntity)clientInstance).ClientID));
			}
			if(deviceClassInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.SalesChannelID], ComparisonOperator.Equal, ((DeviceClassEntity)deviceClassInstance).DeviceClassID));
			}
			if(appliedPassTicketInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.AppliedPassTicketID], ComparisonOperator.Equal, ((TicketEntity)appliedPassTicketInstance).TicketID));
			}
			if(ticketInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.TicketID], ComparisonOperator.Equal, ((TicketEntity)ticketInstance).TicketID));
			}
			if(ticket_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.TaxTicketID], ComparisonOperator.Equal, ((TicketEntity)ticket_Instance).TicketID));
			}
			if(cardInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.TransitAccountID], ComparisonOperator.Equal, ((CardEntity)cardInstance).CardID));
			}
			if(card_Instance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.Transferredfromcardid], ComparisonOperator.Equal, ((CardEntity)card_Instance).CardID));
			}
			if(tokenCardInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.TokenCardID], ComparisonOperator.Equal, ((CardEntity)tokenCardInstance).CardID));
			}
			if(cardHolderInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.CardHolderID], ComparisonOperator.Equal, ((CardHolderEntity)cardHolderInstance).PersonID));
			}
			if(creditCardAuthorizationInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.CreditCardAuthorizationID], ComparisonOperator.Equal, ((CreditCardAuthorizationEntity)creditCardAuthorizationInstance).CreditCardAuthorizationID));
			}
			if(appliedPassProductInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.AppliedPassProductID], ComparisonOperator.Equal, ((ProductEntity)appliedPassProductInstance).ProductID));
			}
			if(productInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.ProductID], ComparisonOperator.Equal, ((ProductEntity)productInstance).ProductID));
			}
			if(saleInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.SaleID], ComparisonOperator.Equal, ((SaleEntity)saleInstance).SaleID));
			}
			if(cancellationReferenceTransactionInstance != null)
			{
				selectFilter.Add(new FieldCompareValuePredicate(fieldsToReturn[(int)TransactionJournalFieldIndex.CancellationReference], ComparisonOperator.Equal, ((TransactionJournalEntity)cancellationReferenceTransactionInstance).TransactionJournalID));
			}
			return selectFilter;
		}
		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
