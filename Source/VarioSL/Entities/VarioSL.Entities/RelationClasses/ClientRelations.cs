﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Client. </summary>
	public partial class ClientRelations
	{
		/// <summary>CTor</summary>
		public ClientRelations()
		{
		}

		/// <summary>Gets all relations of the ClientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccountEntryEntityUsingClientID);
			toReturn.Add(this.ApportionmentResultEntityUsingAcquirerID);
			toReturn.Add(this.ApportionmentResultEntityUsingFromClientID);
			toReturn.Add(this.ApportionmentResultEntityUsingToClientID);
			toReturn.Add(this.CalendarEntityUsingOwnerClientId);
			toReturn.Add(this.ClearingResultEntityUsingCardIssuerID);
			toReturn.Add(this.ClearingResultEntityUsingClientID);
			toReturn.Add(this.ClearingResultEntityUsingFromClient);
			toReturn.Add(this.ClearingResultEntityUsingToClient);
			toReturn.Add(this.ClientAdaptedLayoutObjectEntityUsingClientID);
			toReturn.Add(this.DebtorEntityUsingClientID);
			toReturn.Add(this.DepotEntityUsingClientID);
			toReturn.Add(this.FareMatrixEntityUsingOwnerClientID);
			toReturn.Add(this.FareTableEntityUsingOwnerCLientID);
			toReturn.Add(this.LayoutEntityUsingOwnerClientID);
			toReturn.Add(this.ResponsibilityEntityUsingClientID);
			toReturn.Add(this.ResponsibilityEntityUsingResponsibleClientID);
			toReturn.Add(this.TariffEntityUsingClientID);
			toReturn.Add(this.TariffEntityUsingOwnerClientID);
			toReturn.Add(this.TicketEntityUsingOwnerClientID);
			toReturn.Add(this.TicketCategoryEntityUsingClientID);
			toReturn.Add(this.TicketVendingClientEntityUsingClientid);
			toReturn.Add(this.TransactionEntityUsingCardIssuerID);
			toReturn.Add(this.UnitEntityUsingClientID);
			toReturn.Add(this.UserListEntityUsingClientID);
			toReturn.Add(this.VarioSettlementEntityUsingClientID);
			toReturn.Add(this.VdvKeySetEntityUsingClientID);
			toReturn.Add(this.WorkstationEntityUsingClientID);
			toReturn.Add(this.AddressBookEntryEntityUsingClientID);
			toReturn.Add(this.CardEntityUsingClientID);
			toReturn.Add(this.ConfigurationDefinitionEntityUsingOwnerClientID);
			toReturn.Add(this.ConfigurationOverwriteEntityUsingClientID);
			toReturn.Add(this.ContractEntityUsingClientID);
			toReturn.Add(this.ContractEntityUsingVanpoolClientID);
			toReturn.Add(this.CryptoCryptogramArchiveEntityUsingClientID);
			toReturn.Add(this.CryptoQArchiveEntityUsingClientID);
			toReturn.Add(this.FareChangeCauseEntityUsingClientID);
			toReturn.Add(this.FareEvasionBehaviourEntityUsingClientID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingClientID);
			toReturn.Add(this.FareEvasionInspectionEntityUsingClientID);
			toReturn.Add(this.FilterValueSetEntityUsingClientID);
			toReturn.Add(this.FormEntityUsingClientID);
			toReturn.Add(this.IdentificationTypeEntityUsingClientID);
			toReturn.Add(this.InvoiceEntityUsingClientID);
			toReturn.Add(this.InvoicingEntityUsingClientID);
			toReturn.Add(this.JobEntityUsingClientID);
			toReturn.Add(this.ParameterArchiveEntityUsingClientID);
			toReturn.Add(this.PostingKeyEntityUsingClientID);
			toReturn.Add(this.ProductEntityUsingClientID);
			toReturn.Add(this.ReportJobEntityUsingClientID);
			toReturn.Add(this.ReportJobResultEntityUsingClientID);
			toReturn.Add(this.ReportToClientEntityUsingClientID);
			toReturn.Add(this.RevenueSettlementEntityUsingClientID);
			toReturn.Add(this.SaleEntityUsingClientID);
			toReturn.Add(this.SchoolYearEntityUsingClientID);
			toReturn.Add(this.StorageLocationEntityUsingClientID);
			toReturn.Add(this.TransactionJournalEntityUsingClientID);
			toReturn.Add(this.TransportCompanyEntityUsingClientID);
			toReturn.Add(this.AccountEntryNumberEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and AccountEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - AccountEntry.ClientID
		/// </summary>
		public virtual IEntityRelation AccountEntryEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountEntries" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, AccountEntryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ApportionmentResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ApportionmentResult.AcquirerID
		/// </summary>
		public virtual IEntityRelation ApportionmentResultEntityUsingAcquirerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApportionmentResults" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.AcquirerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ApportionmentResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ApportionmentResult.FromClientID
		/// </summary>
		public virtual IEntityRelation ApportionmentResultEntityUsingFromClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApportionmentResults1" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.FromClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ApportionmentResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ApportionmentResult.ToClientID
		/// </summary>
		public virtual IEntityRelation ApportionmentResultEntityUsingToClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApportionmentResults2" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.ToClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CalendarEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Calendar.OwnerClientId
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingOwnerClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Calendar" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, CalendarFields.OwnerClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ClearingResult.CardIssuerID
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingCardIssuerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResultsByCardIssuer" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.CardIssuerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ClearingResult.ClientID
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResults" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ClearingResult.FromClient
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingFromClient
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResultsByClientFrom" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.FromClient);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ClearingResult.ToClient
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingToClient
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResultsByClientTo" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.ToClient);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ClientAdaptedLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ClientAdaptedLayoutObject.ClientID
		/// </summary>
		public virtual IEntityRelation ClientAdaptedLayoutObjectEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientAdaptedLayoutObjects" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClientAdaptedLayoutObjectFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientAdaptedLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DebtorEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Debtor.ClientID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Debtors" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, DebtorFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and DepotEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Depot.ClientID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Depots" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, DepotFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareMatrixEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareMatrix.OwnerClientID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixes" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareMatrixFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareTableEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareTable.OwnerCLientID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingOwnerCLientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTables" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareTableFields.OwnerCLientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and LayoutEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Layout.OwnerClientID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Layouts" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, LayoutFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ResponsibilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Responsibility.ClientID
		/// </summary>
		public virtual IEntityRelation ResponsibilityEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WhoIsResponsible" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ResponsibilityFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ResponsibilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ResponsibilityEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Responsibility.ResponsibleClientID
		/// </summary>
		public virtual IEntityRelation ResponsibilityEntityUsingResponsibleClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ResponsibleFor" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ResponsibilityFields.ResponsibleClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ResponsibilityEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Tariff.ClientID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientTariffs" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TariffFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Tariff.OwnerClientID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OwnerClientTariffs" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TariffFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Ticket.OwnerClientID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TicketCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - TicketCategory.ClientID
		/// </summary>
		public virtual IEntityRelation TicketCategoryEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketCategories" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketCategoryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TicketVendingClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - TicketVendingClient.Clientid
		/// </summary>
		public virtual IEntityRelation TicketVendingClientEntityUsingClientid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketVendingClient" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketVendingClientFields.Clientid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketVendingClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Transaction.CardIssuerID
		/// </summary>
		public virtual IEntityRelation TransactionEntityUsingCardIssuerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardIssuerCardTransactions" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TransactionFields.CardIssuerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and UnitEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Unit.ClientID
		/// </summary>
		public virtual IEntityRelation UnitEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UmUnits" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, UnitFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and UserListEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - UserList.ClientID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Users" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, UserListFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and VarioSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - VarioSettlement.ClientID
		/// </summary>
		public virtual IEntityRelation VarioSettlementEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Settlements" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, VarioSettlementFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and VdvKeySetEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - VdvKeySet.ClientID
		/// </summary>
		public virtual IEntityRelation VdvKeySetEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvKeySet" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, VdvKeySetFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvKeySetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and WorkstationEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Workstation.ClientID
		/// </summary>
		public virtual IEntityRelation WorkstationEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Workstations" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, WorkstationFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkstationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and AddressBookEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - AddressBookEntry.ClientID
		/// </summary>
		public virtual IEntityRelation AddressBookEntryEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AddressBookEntries" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, AddressBookEntryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Card.ClientID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Cards" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, CardFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ConfigurationDefinitionEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ConfigurationDefinition.OwnerClientID
		/// </summary>
		public virtual IEntityRelation ConfigurationDefinitionEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ConfigurationDefinitions" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ConfigurationDefinitionFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ConfigurationOverwriteEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ConfigurationOverwrite.ClientID
		/// </summary>
		public virtual IEntityRelation ConfigurationOverwriteEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ConfigurationOverwrites" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ConfigurationOverwriteFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationOverwriteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ContractEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Contract.ClientID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Contracts" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ContractFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ContractEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Contract.VanpoolClientID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingVanpoolClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VanpoolContracts" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ContractFields.VanpoolClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CryptoCryptogramArchiveEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - CryptoCryptogramArchive.ClientID
		/// </summary>
		public virtual IEntityRelation CryptoCryptogramArchiveEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoCryptogramArchives" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, CryptoCryptogramArchiveFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCryptogramArchiveEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and CryptoQArchiveEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - CryptoQArchive.ClientID
		/// </summary>
		public virtual IEntityRelation CryptoQArchiveEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoQArchives" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, CryptoQArchiveFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoQArchiveEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareChangeCauseEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareChangeCause.ClientID
		/// </summary>
		public virtual IEntityRelation FareChangeCauseEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareChangeCauses" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareChangeCauseFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareChangeCauseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareEvasionBehaviourEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareEvasionBehaviour.ClientID
		/// </summary>
		public virtual IEntityRelation FareEvasionBehaviourEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionBehaviours" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareEvasionBehaviourFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionBehaviourEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareEvasionIncident.ClientID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareEvasionIncidentFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FareEvasionInspectionEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FareEvasionInspection.ClientID
		/// </summary>
		public virtual IEntityRelation FareEvasionInspectionEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionInspections" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareEvasionInspectionFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionInspectionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FilterValueSetEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - FilterValueSet.ClientID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValueSets" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FilterValueSetFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and FormEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Form.ClientID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Forms" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, FormFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and IdentificationTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - IdentificationType.ClientID
		/// </summary>
		public virtual IEntityRelation IdentificationTypeEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "IdentificationTypes" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, IdentificationTypeFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IdentificationTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Invoice.ClientID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, InvoiceFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and InvoicingEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Invoicing.ClientID
		/// </summary>
		public virtual IEntityRelation InvoicingEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoicings" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, InvoicingFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Job.ClientID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, JobFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ParameterArchiveEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ParameterArchive.ClientID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterArchives" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ParameterArchiveFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and PostingKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - PostingKey.ClientID
		/// </summary>
		public virtual IEntityRelation PostingKeyEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PostingKeys" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, PostingKeyFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Product.ClientID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ProductFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ReportJobEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ReportJob.ClientID
		/// </summary>
		public virtual IEntityRelation ReportJobEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobs" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportJobFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ReportJobResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ReportJobResult.ClientID
		/// </summary>
		public virtual IEntityRelation ReportJobResultEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobResults" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportJobResultFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and ReportToClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - ReportToClient.ClientID
		/// </summary>
		public virtual IEntityRelation ReportToClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportToClients" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportToClientFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and RevenueSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - RevenueSettlement.ClientID
		/// </summary>
		public virtual IEntityRelation RevenueSettlementEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueSettlements" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, RevenueSettlementFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - Sale.ClientID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, SaleFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and SchoolYearEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - SchoolYear.ClientID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SchoolYears" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, SchoolYearFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and StorageLocationEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - StorageLocation.ClientID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StorageLocations" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, StorageLocationFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - TransactionJournal.ClientID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TransactionJournalFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and TransportCompanyEntity over the 1:n relation they have, using the relation between the fields:
		/// Client.ClientID - TransportCompany.ClientID
		/// </summary>
		public virtual IEntityRelation TransportCompanyEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransportCompanies" , true);
				relation.AddEntityFieldPair(ClientFields.ClientID, TransportCompanyFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransportCompanyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ClientEntity and AccountEntryNumberEntity over the 1:1 relation they have, using the relation between the fields:
		/// Client.ClientID - AccountEntryNumber.TrafficCompanyID
		/// </summary>
		public virtual IEntityRelation AccountEntryNumberEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "AccountEntryNumber", false);



				relation.AddEntityFieldPair(AccountEntryNumberFields.TrafficCompanyID, ClientFields.ClientID);

				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryNumberEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClientRelations
	{
		internal static readonly IEntityRelation AccountEntryEntityUsingClientIDStatic = new ClientRelations().AccountEntryEntityUsingClientID;
		internal static readonly IEntityRelation ApportionmentResultEntityUsingAcquirerIDStatic = new ClientRelations().ApportionmentResultEntityUsingAcquirerID;
		internal static readonly IEntityRelation ApportionmentResultEntityUsingFromClientIDStatic = new ClientRelations().ApportionmentResultEntityUsingFromClientID;
		internal static readonly IEntityRelation ApportionmentResultEntityUsingToClientIDStatic = new ClientRelations().ApportionmentResultEntityUsingToClientID;
		internal static readonly IEntityRelation CalendarEntityUsingOwnerClientIdStatic = new ClientRelations().CalendarEntityUsingOwnerClientId;
		internal static readonly IEntityRelation ClearingResultEntityUsingCardIssuerIDStatic = new ClientRelations().ClearingResultEntityUsingCardIssuerID;
		internal static readonly IEntityRelation ClearingResultEntityUsingClientIDStatic = new ClientRelations().ClearingResultEntityUsingClientID;
		internal static readonly IEntityRelation ClearingResultEntityUsingFromClientStatic = new ClientRelations().ClearingResultEntityUsingFromClient;
		internal static readonly IEntityRelation ClearingResultEntityUsingToClientStatic = new ClientRelations().ClearingResultEntityUsingToClient;
		internal static readonly IEntityRelation ClientAdaptedLayoutObjectEntityUsingClientIDStatic = new ClientRelations().ClientAdaptedLayoutObjectEntityUsingClientID;
		internal static readonly IEntityRelation DebtorEntityUsingClientIDStatic = new ClientRelations().DebtorEntityUsingClientID;
		internal static readonly IEntityRelation DepotEntityUsingClientIDStatic = new ClientRelations().DepotEntityUsingClientID;
		internal static readonly IEntityRelation FareMatrixEntityUsingOwnerClientIDStatic = new ClientRelations().FareMatrixEntityUsingOwnerClientID;
		internal static readonly IEntityRelation FareTableEntityUsingOwnerCLientIDStatic = new ClientRelations().FareTableEntityUsingOwnerCLientID;
		internal static readonly IEntityRelation LayoutEntityUsingOwnerClientIDStatic = new ClientRelations().LayoutEntityUsingOwnerClientID;
		internal static readonly IEntityRelation ResponsibilityEntityUsingClientIDStatic = new ClientRelations().ResponsibilityEntityUsingClientID;
		internal static readonly IEntityRelation ResponsibilityEntityUsingResponsibleClientIDStatic = new ClientRelations().ResponsibilityEntityUsingResponsibleClientID;
		internal static readonly IEntityRelation TariffEntityUsingClientIDStatic = new ClientRelations().TariffEntityUsingClientID;
		internal static readonly IEntityRelation TariffEntityUsingOwnerClientIDStatic = new ClientRelations().TariffEntityUsingOwnerClientID;
		internal static readonly IEntityRelation TicketEntityUsingOwnerClientIDStatic = new ClientRelations().TicketEntityUsingOwnerClientID;
		internal static readonly IEntityRelation TicketCategoryEntityUsingClientIDStatic = new ClientRelations().TicketCategoryEntityUsingClientID;
		internal static readonly IEntityRelation TicketVendingClientEntityUsingClientidStatic = new ClientRelations().TicketVendingClientEntityUsingClientid;
		internal static readonly IEntityRelation TransactionEntityUsingCardIssuerIDStatic = new ClientRelations().TransactionEntityUsingCardIssuerID;
		internal static readonly IEntityRelation UnitEntityUsingClientIDStatic = new ClientRelations().UnitEntityUsingClientID;
		internal static readonly IEntityRelation UserListEntityUsingClientIDStatic = new ClientRelations().UserListEntityUsingClientID;
		internal static readonly IEntityRelation VarioSettlementEntityUsingClientIDStatic = new ClientRelations().VarioSettlementEntityUsingClientID;
		internal static readonly IEntityRelation VdvKeySetEntityUsingClientIDStatic = new ClientRelations().VdvKeySetEntityUsingClientID;
		internal static readonly IEntityRelation WorkstationEntityUsingClientIDStatic = new ClientRelations().WorkstationEntityUsingClientID;
		internal static readonly IEntityRelation AddressBookEntryEntityUsingClientIDStatic = new ClientRelations().AddressBookEntryEntityUsingClientID;
		internal static readonly IEntityRelation CardEntityUsingClientIDStatic = new ClientRelations().CardEntityUsingClientID;
		internal static readonly IEntityRelation ConfigurationDefinitionEntityUsingOwnerClientIDStatic = new ClientRelations().ConfigurationDefinitionEntityUsingOwnerClientID;
		internal static readonly IEntityRelation ConfigurationOverwriteEntityUsingClientIDStatic = new ClientRelations().ConfigurationOverwriteEntityUsingClientID;
		internal static readonly IEntityRelation ContractEntityUsingClientIDStatic = new ClientRelations().ContractEntityUsingClientID;
		internal static readonly IEntityRelation ContractEntityUsingVanpoolClientIDStatic = new ClientRelations().ContractEntityUsingVanpoolClientID;
		internal static readonly IEntityRelation CryptoCryptogramArchiveEntityUsingClientIDStatic = new ClientRelations().CryptoCryptogramArchiveEntityUsingClientID;
		internal static readonly IEntityRelation CryptoQArchiveEntityUsingClientIDStatic = new ClientRelations().CryptoQArchiveEntityUsingClientID;
		internal static readonly IEntityRelation FareChangeCauseEntityUsingClientIDStatic = new ClientRelations().FareChangeCauseEntityUsingClientID;
		internal static readonly IEntityRelation FareEvasionBehaviourEntityUsingClientIDStatic = new ClientRelations().FareEvasionBehaviourEntityUsingClientID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingClientIDStatic = new ClientRelations().FareEvasionIncidentEntityUsingClientID;
		internal static readonly IEntityRelation FareEvasionInspectionEntityUsingClientIDStatic = new ClientRelations().FareEvasionInspectionEntityUsingClientID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingClientIDStatic = new ClientRelations().FilterValueSetEntityUsingClientID;
		internal static readonly IEntityRelation FormEntityUsingClientIDStatic = new ClientRelations().FormEntityUsingClientID;
		internal static readonly IEntityRelation IdentificationTypeEntityUsingClientIDStatic = new ClientRelations().IdentificationTypeEntityUsingClientID;
		internal static readonly IEntityRelation InvoiceEntityUsingClientIDStatic = new ClientRelations().InvoiceEntityUsingClientID;
		internal static readonly IEntityRelation InvoicingEntityUsingClientIDStatic = new ClientRelations().InvoicingEntityUsingClientID;
		internal static readonly IEntityRelation JobEntityUsingClientIDStatic = new ClientRelations().JobEntityUsingClientID;
		internal static readonly IEntityRelation ParameterArchiveEntityUsingClientIDStatic = new ClientRelations().ParameterArchiveEntityUsingClientID;
		internal static readonly IEntityRelation PostingKeyEntityUsingClientIDStatic = new ClientRelations().PostingKeyEntityUsingClientID;
		internal static readonly IEntityRelation ProductEntityUsingClientIDStatic = new ClientRelations().ProductEntityUsingClientID;
		internal static readonly IEntityRelation ReportJobEntityUsingClientIDStatic = new ClientRelations().ReportJobEntityUsingClientID;
		internal static readonly IEntityRelation ReportJobResultEntityUsingClientIDStatic = new ClientRelations().ReportJobResultEntityUsingClientID;
		internal static readonly IEntityRelation ReportToClientEntityUsingClientIDStatic = new ClientRelations().ReportToClientEntityUsingClientID;
		internal static readonly IEntityRelation RevenueSettlementEntityUsingClientIDStatic = new ClientRelations().RevenueSettlementEntityUsingClientID;
		internal static readonly IEntityRelation SaleEntityUsingClientIDStatic = new ClientRelations().SaleEntityUsingClientID;
		internal static readonly IEntityRelation SchoolYearEntityUsingClientIDStatic = new ClientRelations().SchoolYearEntityUsingClientID;
		internal static readonly IEntityRelation StorageLocationEntityUsingClientIDStatic = new ClientRelations().StorageLocationEntityUsingClientID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingClientIDStatic = new ClientRelations().TransactionJournalEntityUsingClientID;
		internal static readonly IEntityRelation TransportCompanyEntityUsingClientIDStatic = new ClientRelations().TransportCompanyEntityUsingClientID;
		internal static readonly IEntityRelation AccountEntryNumberEntityUsingClientIDStatic = new ClientRelations().AccountEntryNumberEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticClientRelations()
		{
		}
	}
}
