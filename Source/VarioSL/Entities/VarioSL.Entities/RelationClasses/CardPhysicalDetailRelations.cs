﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardPhysicalDetail. </summary>
	public partial class CardPhysicalDetailRelations
	{
		/// <summary>CTor</summary>
		public CardPhysicalDetailRelations()
		{
		}

		/// <summary>Gets all relations of the CardPhysicalDetailEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardTicketEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.CardEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.JobCardImportEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.CardChipTypeEntityUsingChipTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardPhysicalDetailEntity and CardTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// CardPhysicalDetail.CardPhysicalDetailID - CardTicket.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation CardTicketEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardTicket" , true);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, CardTicketFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardPhysicalDetailEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// CardPhysicalDetail.CardPhysicalDetailID - Card.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Cards" , true);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, CardFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardPhysicalDetailEntity and JobCardImportEntity over the 1:n relation they have, using the relation between the fields:
		/// CardPhysicalDetail.CardPhysicalDetailID - JobCardImport.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation JobCardImportEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "JobCardImports" , true);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, JobCardImportFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CardPhysicalDetailEntity and CardChipTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// CardPhysicalDetail.ChipTypeID - CardChipType.CardChipTypeID
		/// </summary>
		public virtual IEntityRelation CardChipTypeEntityUsingChipTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardChipType", false);
				relation.AddEntityFieldPair(CardChipTypeFields.CardChipTypeID, CardPhysicalDetailFields.ChipTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardChipTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardPhysicalDetailRelations
	{
		internal static readonly IEntityRelation CardTicketEntityUsingCardPhysicalDetailIDStatic = new CardPhysicalDetailRelations().CardTicketEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation CardEntityUsingCardPhysicalDetailIDStatic = new CardPhysicalDetailRelations().CardEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation JobCardImportEntityUsingCardPhysicalDetailIDStatic = new CardPhysicalDetailRelations().JobCardImportEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation CardChipTypeEntityUsingChipTypeIDStatic = new CardPhysicalDetailRelations().CardChipTypeEntityUsingChipTypeID;

		/// <summary>CTor</summary>
		static StaticCardPhysicalDetailRelations()
		{
		}
	}
}
