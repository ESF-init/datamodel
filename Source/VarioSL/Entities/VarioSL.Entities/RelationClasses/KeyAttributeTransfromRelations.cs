﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: KeyAttributeTransfrom. </summary>
	public partial class KeyAttributeTransfromRelations
	{
		/// <summary>CTor</summary>
		public KeyAttributeTransfromRelations()
		{
		}

		/// <summary>Gets all relations of the KeyAttributeTransfromEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeEntityUsingAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingAttributeValueFromID);
			toReturn.Add(this.AttributeValueEntityUsingAttributeValueToID);
			toReturn.Add(this.PredefinedKeyEntityUsingKeyID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between KeyAttributeTransfromEntity and AttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// KeyAttributeTransfrom.AttributeID - Attribute.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Attribute", false);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, KeyAttributeTransfromFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between KeyAttributeTransfromEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// KeyAttributeTransfrom.AttributeValueFromID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeValueFromID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueFrom", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, KeyAttributeTransfromFields.AttributeValueFromID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between KeyAttributeTransfromEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// KeyAttributeTransfrom.AttributeValueToID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeValueToID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueTo", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, KeyAttributeTransfromFields.AttributeValueToID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between KeyAttributeTransfromEntity and PredefinedKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// KeyAttributeTransfrom.KeyID - PredefinedKey.KeyID
		/// </summary>
		public virtual IEntityRelation PredefinedKeyEntityUsingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PredefinedKey", false);
				relation.AddEntityFieldPair(PredefinedKeyFields.KeyID, KeyAttributeTransfromFields.KeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between KeyAttributeTransfromEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// KeyAttributeTransfrom.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, KeyAttributeTransfromFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticKeyAttributeTransfromRelations
	{
		internal static readonly IEntityRelation AttributeEntityUsingAttributeIDStatic = new KeyAttributeTransfromRelations().AttributeEntityUsingAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeValueFromIDStatic = new KeyAttributeTransfromRelations().AttributeValueEntityUsingAttributeValueFromID;
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeValueToIDStatic = new KeyAttributeTransfromRelations().AttributeValueEntityUsingAttributeValueToID;
		internal static readonly IEntityRelation PredefinedKeyEntityUsingKeyIDStatic = new KeyAttributeTransfromRelations().PredefinedKeyEntityUsingKeyID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new KeyAttributeTransfromRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticKeyAttributeTransfromRelations()
		{
		}
	}
}
