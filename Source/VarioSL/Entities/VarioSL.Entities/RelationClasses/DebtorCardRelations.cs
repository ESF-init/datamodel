﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DebtorCard. </summary>
	public partial class DebtorCardRelations
	{
		/// <summary>CTor</summary>
		public DebtorCardRelations()
		{
		}

		/// <summary>Gets all relations of the DebtorCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DebtorCardHistoryEntityUsingDebtorCardID);
			toReturn.Add(this.TypeOfCardEntityUsingTypeOfCardID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DebtorCardEntity and DebtorCardHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// DebtorCard.DebtorCardID - DebtorCardHistory.DebtorCardID
		/// </summary>
		public virtual IEntityRelation DebtorCardHistoryEntityUsingDebtorCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DebtorCardHistory" , true);
				relation.AddEntityFieldPair(DebtorCardFields.DebtorCardID, DebtorCardHistoryFields.DebtorCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorCardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorCardHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between DebtorCardEntity and TypeOfCardEntity over the m:1 relation they have, using the relation between the fields:
		/// DebtorCard.TypeOfCardID - TypeOfCard.TypeOfCardID
		/// </summary>
		public virtual IEntityRelation TypeOfCardEntityUsingTypeOfCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TypeOfCard", false);
				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, DebtorCardFields.TypeOfCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorCardEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDebtorCardRelations
	{
		internal static readonly IEntityRelation DebtorCardHistoryEntityUsingDebtorCardIDStatic = new DebtorCardRelations().DebtorCardHistoryEntityUsingDebtorCardID;
		internal static readonly IEntityRelation TypeOfCardEntityUsingTypeOfCardIDStatic = new DebtorCardRelations().TypeOfCardEntityUsingTypeOfCardID;

		/// <summary>CTor</summary>
		static StaticDebtorCardRelations()
		{
		}
	}
}
