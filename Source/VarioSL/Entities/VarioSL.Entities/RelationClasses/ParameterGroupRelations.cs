﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterGroup. </summary>
	public partial class ParameterGroupRelations
	{
		/// <summary>CTor</summary>
		public ParameterGroupRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterGroupToParameterEntityUsingParameterGroupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ParameterGroupEntity and ParameterGroupToParameterEntity over the 1:n relation they have, using the relation between the fields:
		/// ParameterGroup.ParameterGroupID - ParameterGroupToParameter.ParameterGroupID
		/// </summary>
		public virtual IEntityRelation ParameterGroupToParameterEntityUsingParameterGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterGroupToParameters" , true);
				relation.AddEntityFieldPair(ParameterGroupFields.ParameterGroupID, ParameterGroupToParameterFields.ParameterGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupToParameterEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterGroupRelations
	{
		internal static readonly IEntityRelation ParameterGroupToParameterEntityUsingParameterGroupIDStatic = new ParameterGroupRelations().ParameterGroupToParameterEntityUsingParameterGroupID;

		/// <summary>CTor</summary>
		static StaticParameterGroupRelations()
		{
		}
	}
}
