﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AutoloadSetting. </summary>
	public partial class AutoloadSettingRelations
	{
		/// <summary>CTor</summary>
		public AutoloadSettingRelations()
		{
		}

		/// <summary>Gets all relations of the AutoloadSettingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutoloadToPaymentOptionEntityUsingAutoloadSettingID);
			toReturn.Add(this.EmailEventResendLockEntityUsingAutoloadSettingID);
			toReturn.Add(this.PaymentOptionEntityUsingAlternativePaymentOptionId);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AutoloadSettingEntity and AutoloadToPaymentOptionEntity over the 1:n relation they have, using the relation between the fields:
		/// AutoloadSetting.AutoloadSettingID - AutoloadToPaymentOption.AutoloadSettingID
		/// </summary>
		public virtual IEntityRelation AutoloadToPaymentOptionEntityUsingAutoloadSettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AutoloadToPaymentOptions" , true);
				relation.AddEntityFieldPair(AutoloadSettingFields.AutoloadSettingID, AutoloadToPaymentOptionFields.AutoloadSettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AutoloadSettingEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// AutoloadSetting.AutoloadSettingID - EmailEventResendLock.AutoloadSettingID
		/// </summary>
		public virtual IEntityRelation EmailEventResendLockEntityUsingAutoloadSettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(AutoloadSettingFields.AutoloadSettingID, EmailEventResendLockFields.AutoloadSettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AutoloadSettingEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadSetting.AlternativePaymentOptionId - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingAlternativePaymentOptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AltPaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadSettingFields.AlternativePaymentOptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AutoloadSettingEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadSetting.PaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadSettingFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AutoloadSettingEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadSetting.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, AutoloadSettingFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAutoloadSettingRelations
	{
		internal static readonly IEntityRelation AutoloadToPaymentOptionEntityUsingAutoloadSettingIDStatic = new AutoloadSettingRelations().AutoloadToPaymentOptionEntityUsingAutoloadSettingID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingAutoloadSettingIDStatic = new AutoloadSettingRelations().EmailEventResendLockEntityUsingAutoloadSettingID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingAlternativePaymentOptionIdStatic = new AutoloadSettingRelations().PaymentOptionEntityUsingAlternativePaymentOptionId;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new AutoloadSettingRelations().PaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new AutoloadSettingRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticAutoloadSettingRelations()
		{
		}
	}
}
