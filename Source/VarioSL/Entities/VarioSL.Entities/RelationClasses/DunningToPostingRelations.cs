﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningToPosting. </summary>
	public partial class DunningToPostingRelations
	{
		/// <summary>CTor</summary>
		public DunningToPostingRelations()
		{
		}

		/// <summary>Gets all relations of the DunningToPostingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningProcessEntityUsingDunningProcessID);
			toReturn.Add(this.PostingEntityUsingPostingID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DunningToPostingEntity and DunningProcessEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToPosting.DunningProcessID - DunningProcess.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningProcessEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DunningProcess", false);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToPostingFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToPostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningToPostingEntity and PostingEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToPosting.PostingID - Posting.PostingID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingPostingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Posting", false);
				relation.AddEntityFieldPair(PostingFields.PostingID, DunningToPostingFields.PostingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToPostingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningToPostingRelations
	{
		internal static readonly IEntityRelation DunningProcessEntityUsingDunningProcessIDStatic = new DunningToPostingRelations().DunningProcessEntityUsingDunningProcessID;
		internal static readonly IEntityRelation PostingEntityUsingPostingIDStatic = new DunningToPostingRelations().PostingEntityUsingPostingID;

		/// <summary>CTor</summary>
		static StaticDunningToPostingRelations()
		{
		}
	}
}
