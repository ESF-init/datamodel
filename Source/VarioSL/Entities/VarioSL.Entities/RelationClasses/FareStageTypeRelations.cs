﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareStageType. </summary>
	public partial class FareStageTypeRelations
	{
		/// <summary>CTor</summary>
		public FareStageTypeRelations()
		{
		}

		/// <summary>Gets all relations of the FareStageTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareStageEntityUsingFareStageTypeID);
			toReturn.Add(this.FareStageAliasEntityUsingFareStageTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareStageTypeEntity and FareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStageType.FarestageTypeID - FareStage.FareStageTypeID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareStages" , true);
				relation.AddEntityFieldPair(FareStageTypeFields.FarestageTypeID, FareStageFields.FareStageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageTypeEntity and FareStageAliasEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStageType.FarestageTypeID - FareStageAlias.FareStageTypeID
		/// </summary>
		public virtual IEntityRelation FareStageAliasEntityUsingFareStageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareStageAlia" , true);
				relation.AddEntityFieldPair(FareStageTypeFields.FarestageTypeID, FareStageAliasFields.FareStageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageAliasEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareStageTypeRelations
	{
		internal static readonly IEntityRelation FareStageEntityUsingFareStageTypeIDStatic = new FareStageTypeRelations().FareStageEntityUsingFareStageTypeID;
		internal static readonly IEntityRelation FareStageAliasEntityUsingFareStageTypeIDStatic = new FareStageTypeRelations().FareStageAliasEntityUsingFareStageTypeID;

		/// <summary>CTor</summary>
		static StaticFareStageTypeRelations()
		{
		}
	}
}
