﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BankStatementVerification. </summary>
	public partial class BankStatementVerificationRelations
	{
		/// <summary>CTor</summary>
		public BankStatementVerificationRelations()
		{
		}

		/// <summary>Gets all relations of the BankStatementVerificationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BankStatementEntityUsingBankStatementID);
			toReturn.Add(this.PaymentJournalEntityUsingPaymentJournalID);
			toReturn.Add(this.SaleEntityUsingSaleID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between BankStatementVerificationEntity and BankStatementEntity over the 1:1 relation they have, using the relation between the fields:
		/// BankStatementVerification.BankStatementID - BankStatement.BankStatementID
		/// </summary>
		public virtual IEntityRelation BankStatementEntityUsingBankStatementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "BankStatement", false);




				relation.AddEntityFieldPair(BankStatementFields.BankStatementID, BankStatementVerificationFields.BankStatementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankStatementVerificationEntity and PaymentJournalEntity over the 1:1 relation they have, using the relation between the fields:
		/// BankStatementVerification.PaymentJournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentJournal", false);




				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, BankStatementVerificationFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankStatementVerificationEntity and SaleEntity over the 1:1 relation they have, using the relation between the fields:
		/// BankStatementVerification.SaleID - Sale.SaleID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Sale", false);




				relation.AddEntityFieldPair(SaleFields.SaleID, BankStatementVerificationFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBankStatementVerificationRelations
	{
		internal static readonly IEntityRelation BankStatementEntityUsingBankStatementIDStatic = new BankStatementVerificationRelations().BankStatementEntityUsingBankStatementID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentJournalIDStatic = new BankStatementVerificationRelations().PaymentJournalEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation SaleEntityUsingSaleIDStatic = new BankStatementVerificationRelations().SaleEntityUsingSaleID;

		/// <summary>CTor</summary>
		static StaticBankStatementVerificationRelations()
		{
		}
	}
}
