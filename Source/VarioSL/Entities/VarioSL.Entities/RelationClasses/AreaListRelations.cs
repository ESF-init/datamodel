﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AreaList. </summary>
	public partial class AreaListRelations
	{
		/// <summary>CTor</summary>
		public AreaListRelations()
		{
		}

		/// <summary>Gets all relations of the AreaListEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListElementGroupEntityUsingAreaListElementGroupId);
			toReturn.Add(this.AreaTypeEntityUsingAreaListTypeID);
			toReturn.Add(this.AttributeValueEntityUsingTicketGroupAttributeID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AreaListEntity and AreaListElementGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaList.AreaListElementGroupId - AreaListElementGroup.AreaListeElementGroupId
		/// </summary>
		public virtual IEntityRelation AreaListElementGroupEntityUsingAreaListElementGroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AreaListElementGroup", false);
				relation.AddEntityFieldPair(AreaListElementGroupFields.AreaListeElementGroupId, AreaListFields.AreaListElementGroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListEntity and AreaTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaList.AreaListTypeID - AreaType.TypeID
		/// </summary>
		public virtual IEntityRelation AreaTypeEntityUsingAreaListTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AreaListType", false);
				relation.AddEntityFieldPair(AreaTypeFields.TypeID, AreaListFields.AreaListTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaList.TicketGroupAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingTicketGroupAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, AreaListFields.TicketGroupAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaList.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, AreaListFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAreaListRelations
	{
		internal static readonly IEntityRelation AreaListElementGroupEntityUsingAreaListElementGroupIdStatic = new AreaListRelations().AreaListElementGroupEntityUsingAreaListElementGroupId;
		internal static readonly IEntityRelation AreaTypeEntityUsingAreaListTypeIDStatic = new AreaListRelations().AreaTypeEntityUsingAreaListTypeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingTicketGroupAttributeIDStatic = new AreaListRelations().AttributeValueEntityUsingTicketGroupAttributeID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new AreaListRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticAreaListRelations()
		{
		}
	}
}
