﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceReaderKey. </summary>
	public partial class DeviceReaderKeyRelations
	{
		/// <summary>CTor</summary>
		public DeviceReaderKeyRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceReaderKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceReaderKeyToCertEntityUsingDeviceReaderKeyID);
			toReturn.Add(this.DeviceReaderEntityUsingDeviceReaderID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderKeyEntity and DeviceReaderKeyToCertEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReaderKey.DeviceReaderKeyID - DeviceReaderKeyToCert.DeviceReaderKeyID
		/// </summary>
		public virtual IEntityRelation DeviceReaderKeyToCertEntityUsingDeviceReaderKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderKeyToCerts" , true);
				relation.AddEntityFieldPair(DeviceReaderKeyFields.DeviceReaderKeyID, DeviceReaderKeyToCertFields.DeviceReaderKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyToCertEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between DeviceReaderKeyEntity and DeviceReaderEntity over the m:1 relation they have, using the relation between the fields:
		/// DeviceReaderKey.DeviceReaderID - DeviceReader.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceReader", false);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderKeyFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceReaderKeyRelations
	{
		internal static readonly IEntityRelation DeviceReaderKeyToCertEntityUsingDeviceReaderKeyIDStatic = new DeviceReaderKeyRelations().DeviceReaderKeyToCertEntityUsingDeviceReaderKeyID;
		internal static readonly IEntityRelation DeviceReaderEntityUsingDeviceReaderIDStatic = new DeviceReaderKeyRelations().DeviceReaderEntityUsingDeviceReaderID;

		/// <summary>CTor</summary>
		static StaticDeviceReaderKeyRelations()
		{
		}
	}
}
