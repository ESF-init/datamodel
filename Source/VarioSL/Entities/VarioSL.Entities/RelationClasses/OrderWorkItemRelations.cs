﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderWorkItem. </summary>
	public partial class OrderWorkItemRelations : WorkItemRelations
	{
		/// <summary>CTor</summary>
		public OrderWorkItemRelations()
		{
		}

		/// <summary>Gets all relations of the OrderWorkItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = base.GetAllRelations();
			toReturn.Add(this.OrderEntityUsingOrderID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderWorkItemEntity and WorkItemHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderWorkItem.WorkItemID - WorkItemHistory.WorkItemID
		/// </summary>
		public override IEntityRelation WorkItemHistoryEntityUsingWorkItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItemHistory" , true);
				relation.AddEntityFieldPair(OrderWorkItemFields.WorkItemID, WorkItemHistoryFields.WorkItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderWorkItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderWorkItemEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderWorkItem.Assignee - UserList.UserID
		/// </summary>
		public override IEntityRelation UserListEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, OrderWorkItemFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderWorkItemEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderWorkItem.OrderID - Order.OrderID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Order", false);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderWorkItemFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderWorkItemEntity and WorkItemSubjectEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderWorkItem.WorkItemSubjectID - WorkItemSubject.WorkItemSubjectID
		/// </summary>
		public override IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WorkItemSubject", false);
				relation.AddEntityFieldPair(WorkItemSubjectFields.WorkItemSubjectID, OrderWorkItemFields.WorkItemSubjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemSubjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderWorkItemRelations
	{
		internal static readonly IEntityRelation WorkItemHistoryEntityUsingWorkItemIDStatic = new OrderWorkItemRelations().WorkItemHistoryEntityUsingWorkItemID;
		internal static readonly IEntityRelation UserListEntityUsingAssigneeStatic = new OrderWorkItemRelations().UserListEntityUsingAssignee;
		internal static readonly IEntityRelation OrderEntityUsingOrderIDStatic = new OrderWorkItemRelations().OrderEntityUsingOrderID;
		internal static readonly IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectIDStatic = new OrderWorkItemRelations().WorkItemSubjectEntityUsingWorkItemSubjectID;

		/// <summary>CTor</summary>
		static StaticOrderWorkItemRelations()
		{
		}
	}
}
