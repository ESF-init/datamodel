﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettledRevenue. </summary>
	public partial class SettledRevenueRelations
	{
		/// <summary>CTor</summary>
		public SettledRevenueRelations()
		{
		}

		/// <summary>Gets all relations of the SettledRevenueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RevenueRecognitionEntityUsingRevenueRecognitionID);
			toReturn.Add(this.RevenueSettlementEntityUsingRevenueSettlementID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SettledRevenueEntity and RevenueRecognitionEntity over the m:1 relation they have, using the relation between the fields:
		/// SettledRevenue.RevenueRecognitionID - RevenueRecognition.RevenueRecognitionID
		/// </summary>
		public virtual IEntityRelation RevenueRecognitionEntityUsingRevenueRecognitionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueRecognition", false);
				relation.AddEntityFieldPair(RevenueRecognitionFields.RevenueRecognitionID, SettledRevenueFields.RevenueRecognitionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettledRevenueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SettledRevenueEntity and RevenueSettlementEntity over the m:1 relation they have, using the relation between the fields:
		/// SettledRevenue.RevenueSettlementID - RevenueSettlement.RevenueSettlementID
		/// </summary>
		public virtual IEntityRelation RevenueSettlementEntityUsingRevenueSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueSettlement", false);
				relation.AddEntityFieldPair(RevenueSettlementFields.RevenueSettlementID, SettledRevenueFields.RevenueSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettledRevenueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettledRevenueRelations
	{
		internal static readonly IEntityRelation RevenueRecognitionEntityUsingRevenueRecognitionIDStatic = new SettledRevenueRelations().RevenueRecognitionEntityUsingRevenueRecognitionID;
		internal static readonly IEntityRelation RevenueSettlementEntityUsingRevenueSettlementIDStatic = new SettledRevenueRelations().RevenueSettlementEntityUsingRevenueSettlementID;

		/// <summary>CTor</summary>
		static StaticSettledRevenueRelations()
		{
		}
	}
}
