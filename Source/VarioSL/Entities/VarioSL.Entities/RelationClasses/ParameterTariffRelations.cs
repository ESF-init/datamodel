﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterTariff. </summary>
	public partial class ParameterTariffRelations
	{
		/// <summary>CTor</summary>
		public ParameterTariffRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterTariffEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TariffParameterEntityUsingParameterID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterTariffEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterTariff.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterTariffFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterTariffEntity and TariffParameterEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterTariff.ParameterID - TariffParameter.ParameterID
		/// </summary>
		public virtual IEntityRelation TariffParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Parameter", false);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterTariffFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTariffEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterTariffRelations
	{
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ParameterTariffRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TariffParameterEntityUsingParameterIDStatic = new ParameterTariffRelations().TariffParameterEntityUsingParameterID;

		/// <summary>CTor</summary>
		static StaticParameterTariffRelations()
		{
		}
	}
}
