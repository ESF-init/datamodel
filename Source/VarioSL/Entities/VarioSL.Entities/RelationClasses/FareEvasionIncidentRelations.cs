﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareEvasionIncident. </summary>
	public partial class FareEvasionIncidentRelations
	{
		/// <summary>CTor</summary>
		public FareEvasionIncidentRelations()
		{
		}

		/// <summary>Gets all relations of the FareEvasionIncidentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BinaryDataEntityUsingBinaryDataID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DebtorEntityUsingDebtorID);
			toReturn.Add(this.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.FareEvasionReasonEntityUsingFareEvasionReasonID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.FareChangeCauseEntityUsingFareChangeCauseID);
			toReturn.Add(this.FareEvasionBehaviourEntityUsingFareEvasionBehaviourID);
			toReturn.Add(this.IdentificationTypeEntityUsingIdentificationTypeID);
			toReturn.Add(this.InspectionReportEntityUsingInspectionReportID);
			toReturn.Add(this.PersonEntityUsingGuardianID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.TransportCompanyEntityUsingTransportCompanyID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and BinaryDataEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.BinaryDataID - BinaryData.BinaryDataID
		/// </summary>
		public virtual IEntityRelation BinaryDataEntityUsingBinaryDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BinaryData", false);
				relation.AddEntityFieldPair(BinaryDataFields.BinaryDataID, FareEvasionIncidentFields.BinaryDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BinaryDataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareEvasionIncidentFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.DebtorID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Debtor", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, FareEvasionIncidentFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and FareEvasionCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.FareEvasionCategoryID - FareEvasionCategory.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionCategory", false);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, FareEvasionIncidentFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and FareEvasionReasonEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.FareEvasionReasonID - FareEvasionReason.FareEvasionReasonID
		/// </summary>
		public virtual IEntityRelation FareEvasionReasonEntityUsingFareEvasionReasonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionReason", false);
				relation.AddEntityFieldPair(FareEvasionReasonFields.FareEvasionReasonID, FareEvasionIncidentFields.FareEvasionReasonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, FareEvasionIncidentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and FareChangeCauseEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.FareChangeCauseID - FareChangeCause.FareChangeCauseID
		/// </summary>
		public virtual IEntityRelation FareChangeCauseEntityUsingFareChangeCauseID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareChangeCause", false);
				relation.AddEntityFieldPair(FareChangeCauseFields.FareChangeCauseID, FareEvasionIncidentFields.FareChangeCauseID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareChangeCauseEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and FareEvasionBehaviourEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.FareEvasionBehaviourID - FareEvasionBehaviour.FareEvasionBehaviourID
		/// </summary>
		public virtual IEntityRelation FareEvasionBehaviourEntityUsingFareEvasionBehaviourID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionBehaviour", false);
				relation.AddEntityFieldPair(FareEvasionBehaviourFields.FareEvasionBehaviourID, FareEvasionIncidentFields.FareEvasionBehaviourID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionBehaviourEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and IdentificationTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.IdentificationTypeID - IdentificationType.IdentificationTypeID
		/// </summary>
		public virtual IEntityRelation IdentificationTypeEntityUsingIdentificationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IdentificationType", false);
				relation.AddEntityFieldPair(IdentificationTypeFields.IdentificationTypeID, FareEvasionIncidentFields.IdentificationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IdentificationTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and InspectionReportEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.InspectionReportID - InspectionReport.InspectionReportID
		/// </summary>
		public virtual IEntityRelation InspectionReportEntityUsingInspectionReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InspectionReport", false);
				relation.AddEntityFieldPair(InspectionReportFields.InspectionReportID, FareEvasionIncidentFields.InspectionReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.GuardianID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingGuardianID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Guardian", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, FareEvasionIncidentFields.GuardianID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, FareEvasionIncidentFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionIncidentEntity and TransportCompanyEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionIncident.TransportCompanyID - TransportCompany.TransportCompanyID
		/// </summary>
		public virtual IEntityRelation TransportCompanyEntityUsingTransportCompanyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransportCompany", false);
				relation.AddEntityFieldPair(TransportCompanyFields.TransportCompanyID, FareEvasionIncidentFields.TransportCompanyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransportCompanyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareEvasionIncidentRelations
	{
		internal static readonly IEntityRelation BinaryDataEntityUsingBinaryDataIDStatic = new FareEvasionIncidentRelations().BinaryDataEntityUsingBinaryDataID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new FareEvasionIncidentRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DebtorEntityUsingDebtorIDStatic = new FareEvasionIncidentRelations().DebtorEntityUsingDebtorID;
		internal static readonly IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic = new FareEvasionIncidentRelations().FareEvasionCategoryEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation FareEvasionReasonEntityUsingFareEvasionReasonIDStatic = new FareEvasionIncidentRelations().FareEvasionReasonEntityUsingFareEvasionReasonID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new FareEvasionIncidentRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation FareChangeCauseEntityUsingFareChangeCauseIDStatic = new FareEvasionIncidentRelations().FareChangeCauseEntityUsingFareChangeCauseID;
		internal static readonly IEntityRelation FareEvasionBehaviourEntityUsingFareEvasionBehaviourIDStatic = new FareEvasionIncidentRelations().FareEvasionBehaviourEntityUsingFareEvasionBehaviourID;
		internal static readonly IEntityRelation IdentificationTypeEntityUsingIdentificationTypeIDStatic = new FareEvasionIncidentRelations().IdentificationTypeEntityUsingIdentificationTypeID;
		internal static readonly IEntityRelation InspectionReportEntityUsingInspectionReportIDStatic = new FareEvasionIncidentRelations().InspectionReportEntityUsingInspectionReportID;
		internal static readonly IEntityRelation PersonEntityUsingGuardianIDStatic = new FareEvasionIncidentRelations().PersonEntityUsingGuardianID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new FareEvasionIncidentRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation TransportCompanyEntityUsingTransportCompanyIDStatic = new FareEvasionIncidentRelations().TransportCompanyEntityUsingTransportCompanyID;

		/// <summary>CTor</summary>
		static StaticFareEvasionIncidentRelations()
		{
		}
	}
}
