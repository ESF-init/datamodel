﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EmailEventResendLock. </summary>
	public partial class EmailEventResendLockRelations
	{
		/// <summary>CTor</summary>
		public EmailEventResendLockRelations()
		{
		}

		/// <summary>Gets all relations of the EmailEventResendLockEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutoloadSettingEntityUsingAutoloadSettingID);
			toReturn.Add(this.BookingItemEntityUsingBookingItemID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EmailEventResendLockEntity and AutoloadSettingEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailEventResendLock.AutoloadSettingID - AutoloadSetting.AutoloadSettingID
		/// </summary>
		public virtual IEntityRelation AutoloadSettingEntityUsingAutoloadSettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AutoloadSetting", false);
				relation.AddEntityFieldPair(AutoloadSettingFields.AutoloadSettingID, EmailEventResendLockFields.AutoloadSettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailEventResendLockEntity and BookingItemEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailEventResendLock.BookingItemID - BookingItem.BookingItemID
		/// </summary>
		public virtual IEntityRelation BookingItemEntityUsingBookingItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BookingItem", false);
				relation.AddEntityFieldPair(BookingItemFields.BookingItemID, EmailEventResendLockFields.BookingItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailEventResendLockEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailEventResendLock.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, EmailEventResendLockFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailEventResendLockEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailEventResendLock.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, EmailEventResendLockFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailEventResendLockEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailEventResendLock.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, EmailEventResendLockFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEmailEventResendLockRelations
	{
		internal static readonly IEntityRelation AutoloadSettingEntityUsingAutoloadSettingIDStatic = new EmailEventResendLockRelations().AutoloadSettingEntityUsingAutoloadSettingID;
		internal static readonly IEntityRelation BookingItemEntityUsingBookingItemIDStatic = new EmailEventResendLockRelations().BookingItemEntityUsingBookingItemID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new EmailEventResendLockRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new EmailEventResendLockRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new EmailEventResendLockRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticEmailEventResendLockRelations()
		{
		}
	}
}
