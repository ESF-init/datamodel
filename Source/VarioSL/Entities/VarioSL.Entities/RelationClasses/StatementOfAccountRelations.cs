﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: StatementOfAccount. </summary>
	public partial class StatementOfAccountRelations
	{
		/// <summary>CTor</summary>
		public StatementOfAccountRelations()
		{
		}

		/// <summary>Gets all relations of the StatementOfAccountEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.InvoiceEntityUsingInvoiceID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between StatementOfAccountEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// StatementOfAccount.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, StatementOfAccountFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StatementOfAccountEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between StatementOfAccountEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// StatementOfAccount.InvoiceID - Invoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoice", false);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, StatementOfAccountFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StatementOfAccountEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStatementOfAccountRelations
	{
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new StatementOfAccountRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIDStatic = new StatementOfAccountRelations().InvoiceEntityUsingInvoiceID;

		/// <summary>CTor</summary>
		static StaticStatementOfAccountRelations()
		{
		}
	}
}
