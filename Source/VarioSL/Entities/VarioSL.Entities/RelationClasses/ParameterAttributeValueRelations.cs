﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterAttributeValue. </summary>
	public partial class ParameterAttributeValueRelations
	{
		/// <summary>CTor</summary>
		public ParameterAttributeValueRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterAttributeValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingAttributeValueID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TariffParameterEntityUsingParameterID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterAttributeValueEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterAttributeValue.AttributeValueID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, ParameterAttributeValueFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterAttributeValueEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterAttributeValue.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterAttributeValueFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterAttributeValueEntity and TariffParameterEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterAttributeValue.ParameterID - TariffParameter.ParameterID
		/// </summary>
		public virtual IEntityRelation TariffParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TmParameter", false);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterAttributeValueFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterAttributeValueRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeValueIDStatic = new ParameterAttributeValueRelations().AttributeValueEntityUsingAttributeValueID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ParameterAttributeValueRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TariffParameterEntityUsingParameterIDStatic = new ParameterAttributeValueRelations().TariffParameterEntityUsingParameterID;

		/// <summary>CTor</summary>
		static StaticParameterAttributeValueRelations()
		{
		}
	}
}
