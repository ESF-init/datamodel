﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: StorageLocation. </summary>
	public partial class StorageLocationRelations
	{
		/// <summary>CTor</summary>
		public StorageLocationRelations()
		{
		}

		/// <summary>Gets all relations of the StorageLocationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEntityUsingStorageLocationID);
			toReturn.Add(this.JobCardImportEntityUsingStorageLocationID);
			toReturn.Add(this.StockTransferEntityUsingNewStorageLocationID);
			toReturn.Add(this.StockTransferEntityUsingOldStorageLocationID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.AddressEntityUsingAddressID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// StorageLocation.StorageLocationID - Card.StorageLocationID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Cards" , true);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, CardFields.StorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and JobCardImportEntity over the 1:n relation they have, using the relation between the fields:
		/// StorageLocation.StorageLocationID - JobCardImport.StorageLocationID
		/// </summary>
		public virtual IEntityRelation JobCardImportEntityUsingStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "JobCardImports" , true);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, JobCardImportFields.StorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and StockTransferEntity over the 1:n relation they have, using the relation between the fields:
		/// StorageLocation.StorageLocationID - StockTransfer.NewStorageLocationID
		/// </summary>
		public virtual IEntityRelation StockTransferEntityUsingNewStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NewLocationStockTransfers" , true);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, StockTransferFields.NewStorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and StockTransferEntity over the 1:n relation they have, using the relation between the fields:
		/// StorageLocation.StorageLocationID - StockTransfer.OldStorageLocationID
		/// </summary>
		public virtual IEntityRelation StockTransferEntityUsingOldStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OldLocationStockTransfers" , true);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, StockTransferFields.OldStorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// StorageLocation.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, StorageLocationFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between StorageLocationEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// StorageLocation.AddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Address", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, StorageLocationFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStorageLocationRelations
	{
		internal static readonly IEntityRelation CardEntityUsingStorageLocationIDStatic = new StorageLocationRelations().CardEntityUsingStorageLocationID;
		internal static readonly IEntityRelation JobCardImportEntityUsingStorageLocationIDStatic = new StorageLocationRelations().JobCardImportEntityUsingStorageLocationID;
		internal static readonly IEntityRelation StockTransferEntityUsingNewStorageLocationIDStatic = new StorageLocationRelations().StockTransferEntityUsingNewStorageLocationID;
		internal static readonly IEntityRelation StockTransferEntityUsingOldStorageLocationIDStatic = new StorageLocationRelations().StockTransferEntityUsingOldStorageLocationID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new StorageLocationRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new StorageLocationRelations().AddressEntityUsingAddressID;

		/// <summary>CTor</summary>
		static StaticStorageLocationRelations()
		{
		}
	}
}
