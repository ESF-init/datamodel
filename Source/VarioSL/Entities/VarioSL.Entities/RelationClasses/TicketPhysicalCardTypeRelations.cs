﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketPhysicalCardType. </summary>
	public partial class TicketPhysicalCardTypeRelations
	{
		/// <summary>CTor</summary>
		public TicketPhysicalCardTypeRelations()
		{
		}

		/// <summary>Gets all relations of the TicketPhysicalCardTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardChipTypeEntityUsingPhysicalCardTypeID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketPhysicalCardTypeEntity and CardChipTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketPhysicalCardType.PhysicalCardTypeID - CardChipType.CardChipTypeID
		/// </summary>
		public virtual IEntityRelation CardChipTypeEntityUsingPhysicalCardTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardChipType", false);
				relation.AddEntityFieldPair(CardChipTypeFields.CardChipTypeID, TicketPhysicalCardTypeFields.PhysicalCardTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardChipTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPhysicalCardTypeEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketPhysicalCardTypeEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketPhysicalCardType.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketPhysicalCardTypeFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPhysicalCardTypeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketPhysicalCardTypeRelations
	{
		internal static readonly IEntityRelation CardChipTypeEntityUsingPhysicalCardTypeIDStatic = new TicketPhysicalCardTypeRelations().CardChipTypeEntityUsingPhysicalCardTypeID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketPhysicalCardTypeRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticTicketPhysicalCardTypeRelations()
		{
		}
	}
}
