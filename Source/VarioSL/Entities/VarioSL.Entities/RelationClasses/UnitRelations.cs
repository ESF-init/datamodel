﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Unit. </summary>
	public partial class UnitRelations
	{
		/// <summary>CTor</summary>
		public UnitRelations()
		{
		}

		/// <summary>Gets all relations of the UnitEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceEntityUsingUnitID);
			toReturn.Add(this.ParameterValueEntityUsingUnitID);
			toReturn.Add(this.PrinterToUnitEntityUsingUnitID);
			toReturn.Add(this.UnitCollectionToUnitEntityUsingUnitID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DepotEntityUsingDepotID);
			toReturn.Add(this.TypeOfUnitEntityUsingTypeOfUnitID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UnitEntity and DeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// Unit.UnitID - Device.UnitID
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Devices" , true);
				relation.AddEntityFieldPair(UnitFields.UnitID, DeviceFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UnitEntity and ParameterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Unit.UnitID - ParameterValue.UnitID
		/// </summary>
		public virtual IEntityRelation ParameterValueEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterValues" , true);
				relation.AddEntityFieldPair(UnitFields.UnitID, ParameterValueFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UnitEntity and PrinterToUnitEntity over the 1:n relation they have, using the relation between the fields:
		/// Unit.UnitID - PrinterToUnit.UnitID
		/// </summary>
		public virtual IEntityRelation PrinterToUnitEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PrinterToUnits" , true);
				relation.AddEntityFieldPair(UnitFields.UnitID, PrinterToUnitFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrinterToUnitEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UnitEntity and UnitCollectionToUnitEntity over the 1:n relation they have, using the relation between the fields:
		/// Unit.UnitID - UnitCollectionToUnit.UnitID
		/// </summary>
		public virtual IEntityRelation UnitCollectionToUnitEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UnitCollectionToUnits" , true);
				relation.AddEntityFieldPair(UnitFields.UnitID, UnitCollectionToUnitFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionToUnitEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UnitEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Unit.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, UnitFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UnitEntity and DepotEntity over the m:1 relation they have, using the relation between the fields:
		/// Unit.DepotID - Depot.DepotID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VarioDepot", false);
				relation.AddEntityFieldPair(DepotFields.DepotID, UnitFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UnitEntity and TypeOfUnitEntity over the m:1 relation they have, using the relation between the fields:
		/// Unit.TypeOfUnitID - TypeOfUnit.TypeOfUnitID
		/// </summary>
		public virtual IEntityRelation TypeOfUnitEntityUsingTypeOfUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TypeOfUnit", false);
				relation.AddEntityFieldPair(TypeOfUnitFields.TypeOfUnitID, UnitFields.TypeOfUnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfUnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUnitRelations
	{
		internal static readonly IEntityRelation DeviceEntityUsingUnitIDStatic = new UnitRelations().DeviceEntityUsingUnitID;
		internal static readonly IEntityRelation ParameterValueEntityUsingUnitIDStatic = new UnitRelations().ParameterValueEntityUsingUnitID;
		internal static readonly IEntityRelation PrinterToUnitEntityUsingUnitIDStatic = new UnitRelations().PrinterToUnitEntityUsingUnitID;
		internal static readonly IEntityRelation UnitCollectionToUnitEntityUsingUnitIDStatic = new UnitRelations().UnitCollectionToUnitEntityUsingUnitID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new UnitRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DepotEntityUsingDepotIDStatic = new UnitRelations().DepotEntityUsingDepotID;
		internal static readonly IEntityRelation TypeOfUnitEntityUsingTypeOfUnitIDStatic = new UnitRelations().TypeOfUnitEntityUsingTypeOfUnitID;

		/// <summary>CTor</summary>
		static StaticUnitRelations()
		{
		}
	}
}
