﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PrimalKey. </summary>
	public partial class PrimalKeyRelations
	{
		/// <summary>CTor</summary>
		public PrimalKeyRelations()
		{
		}

		/// <summary>Gets all relations of the PrimalKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PredefinedKeyEntityUsingKeyID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PrimalKeyEntity and PredefinedKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// PrimalKey.KeyID - PredefinedKey.KeyID
		/// </summary>
		public virtual IEntityRelation PredefinedKeyEntityUsingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PredefinedKey", false);
				relation.AddEntityFieldPair(PredefinedKeyFields.KeyID, PrimalKeyFields.KeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PrimalKeyEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// PrimalKey.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, PrimalKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PrimalKeyEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// PrimalKey.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, PrimalKeyFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPrimalKeyRelations
	{
		internal static readonly IEntityRelation PredefinedKeyEntityUsingKeyIDStatic = new PrimalKeyRelations().PredefinedKeyEntityUsingKeyID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new PrimalKeyRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new PrimalKeyRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticPrimalKeyRelations()
		{
		}
	}
}
