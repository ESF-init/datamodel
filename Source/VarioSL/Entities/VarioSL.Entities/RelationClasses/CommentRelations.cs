﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Comment. </summary>
	public partial class CommentRelations
	{
		/// <summary>CTor</summary>
		public CommentRelations()
		{
		}

		/// <summary>Gets all relations of the CommentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CommentTypeEntityUsingCommentType);
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CommentEntity and CommentTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Comment.CommentType - CommentType.EnumerationValue
		/// </summary>
		public virtual IEntityRelation CommentTypeEntityUsingCommentType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SlCommenttype", false);
				relation.AddEntityFieldPair(CommentTypeFields.EnumerationValue, CommentFields.CommentType);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CommentTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CommentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CommentEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Comment.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, CommentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CommentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCommentRelations
	{
		internal static readonly IEntityRelation CommentTypeEntityUsingCommentTypeStatic = new CommentRelations().CommentTypeEntityUsingCommentType;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new CommentRelations().ContractEntityUsingContractID;

		/// <summary>CTor</summary>
		static StaticCommentRelations()
		{
		}
	}
}
