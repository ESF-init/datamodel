﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketPaymentInterval. </summary>
	public partial class TicketPaymentIntervalRelations
	{
		/// <summary>CTor</summary>
		public TicketPaymentIntervalRelations()
		{
		}

		/// <summary>Gets all relations of the TicketPaymentIntervalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentIntervalEntityUsingPaymentIntervalID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketPaymentIntervalEntity and PaymentIntervalEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketPaymentInterval.PaymentIntervalID - PaymentInterval.PaymentIntervalID
		/// </summary>
		public virtual IEntityRelation PaymentIntervalEntityUsingPaymentIntervalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentInterval", false);
				relation.AddEntityFieldPair(PaymentIntervalFields.PaymentIntervalID, TicketPaymentIntervalFields.PaymentIntervalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntervalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPaymentIntervalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketPaymentIntervalEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketPaymentInterval.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketPaymentIntervalFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPaymentIntervalEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketPaymentIntervalRelations
	{
		internal static readonly IEntityRelation PaymentIntervalEntityUsingPaymentIntervalIDStatic = new TicketPaymentIntervalRelations().PaymentIntervalEntityUsingPaymentIntervalID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketPaymentIntervalRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticTicketPaymentIntervalRelations()
		{
		}
	}
}
