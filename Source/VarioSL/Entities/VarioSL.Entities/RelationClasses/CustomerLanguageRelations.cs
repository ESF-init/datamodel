﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomerLanguage. </summary>
	public partial class CustomerLanguageRelations
	{
		/// <summary>CTor</summary>
		public CustomerLanguageRelations()
		{
		}

		/// <summary>Gets all relations of the CustomerLanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomerAccountEntityUsingCustomerLanguageID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CustomerLanguageEntity and CustomerAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerLanguage.CustomerLanguageID - CustomerAccount.CustomerLanguageID
		/// </summary>
		public virtual IEntityRelation CustomerAccountEntityUsingCustomerLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccounts" , true);
				relation.AddEntityFieldPair(CustomerLanguageFields.CustomerLanguageID, CustomerAccountFields.CustomerLanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerLanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomerLanguageRelations
	{
		internal static readonly IEntityRelation CustomerAccountEntityUsingCustomerLanguageIDStatic = new CustomerLanguageRelations().CustomerAccountEntityUsingCustomerLanguageID;

		/// <summary>CTor</summary>
		static StaticCustomerLanguageRelations()
		{
		}
	}
}
