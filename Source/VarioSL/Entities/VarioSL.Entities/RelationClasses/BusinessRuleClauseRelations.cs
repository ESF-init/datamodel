﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRuleClause. </summary>
	public partial class BusinessRuleClauseRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleClauseRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleClauseEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleEntityUsingBusinessRuleID);
			toReturn.Add(this.BusinessRuleConditionEntityUsingBusinessRuleConditionID);
			toReturn.Add(this.BusinessRuleResultEntityUsingBusinessruleResultID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between BusinessRuleClauseEntity and BusinessRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleClause.BusinessRuleID - BusinessRule.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBusinessRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRule", false);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, BusinessRuleClauseFields.BusinessRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleClauseEntity and BusinessRuleConditionEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleClause.BusinessRuleConditionID - BusinessRuleCondition.BusinessRuleConditionID
		/// </summary>
		public virtual IEntityRelation BusinessRuleConditionEntityUsingBusinessRuleConditionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleCondition", false);
				relation.AddEntityFieldPair(BusinessRuleConditionFields.BusinessRuleConditionID, BusinessRuleClauseFields.BusinessRuleConditionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleClauseEntity and BusinessRuleResultEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleClause.BusinessruleResultID - BusinessRuleResult.BusinessRuleResultID
		/// </summary>
		public virtual IEntityRelation BusinessRuleResultEntityUsingBusinessruleResultID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleResult", false);
				relation.AddEntityFieldPair(BusinessRuleResultFields.BusinessRuleResultID, BusinessRuleClauseFields.BusinessruleResultID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleClauseRelations
	{
		internal static readonly IEntityRelation BusinessRuleEntityUsingBusinessRuleIDStatic = new BusinessRuleClauseRelations().BusinessRuleEntityUsingBusinessRuleID;
		internal static readonly IEntityRelation BusinessRuleConditionEntityUsingBusinessRuleConditionIDStatic = new BusinessRuleClauseRelations().BusinessRuleConditionEntityUsingBusinessRuleConditionID;
		internal static readonly IEntityRelation BusinessRuleResultEntityUsingBusinessruleResultIDStatic = new BusinessRuleClauseRelations().BusinessRuleResultEntityUsingBusinessruleResultID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleClauseRelations()
		{
		}
	}
}
