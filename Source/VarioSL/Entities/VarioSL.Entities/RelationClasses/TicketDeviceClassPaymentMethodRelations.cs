﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketDeviceClassPaymentMethod. </summary>
	public partial class TicketDeviceClassPaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public TicketDeviceClassPaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the TicketDeviceClassPaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.TicketDeviceClassEntityUsingTicketDeviceClassID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassPaymentMethodEntity and DevicePaymentMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClassPaymentMethod.DevicePaymentMethodID - DevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DevicePaymentMethod", false);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, TicketDeviceClassPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassPaymentMethodEntity and TicketDeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClassPaymentMethod.TicketDeviceClassID - TicketDeviceClass.TicketDeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingTicketDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketDeviceClasses", false);
				relation.AddEntityFieldPair(TicketDeviceClassFields.TicketDeviceClassID, TicketDeviceClassPaymentMethodFields.TicketDeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketDeviceClassPaymentMethodRelations
	{
		internal static readonly IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new TicketDeviceClassPaymentMethodRelations().DevicePaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingTicketDeviceClassIDStatic = new TicketDeviceClassPaymentMethodRelations().TicketDeviceClassEntityUsingTicketDeviceClassID;

		/// <summary>CTor</summary>
		static StaticTicketDeviceClassPaymentMethodRelations()
		{
		}
	}
}
