﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AreaListElementGroup. </summary>
	public partial class AreaListElementGroupRelations
	{
		/// <summary>CTor</summary>
		public AreaListElementGroupRelations()
		{
		}

		/// <summary>Gets all relations of the AreaListElementGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListEntityUsingAreaListElementGroupId);
			toReturn.Add(this.AreaListElementEntityUsingAreaListID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AreaListElementGroupEntity and AreaListEntity over the 1:n relation they have, using the relation between the fields:
		/// AreaListElementGroup.AreaListeElementGroupId - AreaList.AreaListElementGroupId
		/// </summary>
		public virtual IEntityRelation AreaListEntityUsingAreaListElementGroupId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaList" , true);
				relation.AddEntityFieldPair(AreaListElementGroupFields.AreaListeElementGroupId, AreaListFields.AreaListElementGroupId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AreaListElementGroupEntity and AreaListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// AreaListElementGroup.AreaListeElementGroupId - AreaListElement.AreaListID
		/// </summary>
		public virtual IEntityRelation AreaListElementEntityUsingAreaListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaListElement" , true);
				relation.AddEntityFieldPair(AreaListElementGroupFields.AreaListeElementGroupId, AreaListElementFields.AreaListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAreaListElementGroupRelations
	{
		internal static readonly IEntityRelation AreaListEntityUsingAreaListElementGroupIdStatic = new AreaListElementGroupRelations().AreaListEntityUsingAreaListElementGroupId;
		internal static readonly IEntityRelation AreaListElementEntityUsingAreaListIDStatic = new AreaListElementGroupRelations().AreaListElementEntityUsingAreaListID;

		/// <summary>CTor</summary>
		static StaticAreaListElementGroupRelations()
		{
		}
	}
}
