﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareMatrix. </summary>
	public partial class FareMatrixRelations
	{
		/// <summary>CTor</summary>
		public FareMatrixRelations()
		{
		}

		/// <summary>Gets all relations of the FareMatrixEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareMatrixEntityUsingMasterMatrixID);
			toReturn.Add(this.FareMatrixEntryEntityUsingFareMatrixID);
			toReturn.Add(this.TicketEntityUsingMatrixID);
			toReturn.Add(this.TicketDeviceClassEntityUsingMatrixID);
			toReturn.Add(this.ClientEntityUsingOwnerClientID);
			toReturn.Add(this.FareMatrixEntityUsingFareMatrixIDMasterMatrixID);
			toReturn.Add(this.FareStageListEntityUsingFareStageListID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and FareMatrixEntity over the 1:n relation they have, using the relation between the fields:
		/// FareMatrix.FareMatrixID - FareMatrix.MasterMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingMasterMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrices" , true);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, FareMatrixFields.MasterMatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareMatrix.FareMatrixID - FareMatrixEntry.FareMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingFareMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntry" , true);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, FareMatrixEntryFields.FareMatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// FareMatrix.FareMatrixID - Ticket.MatrixID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, TicketFields.MatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// FareMatrix.FareMatrixID - TicketDeviceClass.MatrixID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClass" , true);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, TicketDeviceClassFields.MatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrix.OwnerClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareMatrixFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and FareMatrixEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrix.MasterMatrixID - FareMatrix.FareMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingFareMatrixIDMasterMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareMatrix", false);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, FareMatrixFields.MasterMatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and FareStageListEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrix.FareStageListID - FareStageList.FareStageListID
		/// </summary>
		public virtual IEntityRelation FareStageListEntityUsingFareStageListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageList", false);
				relation.AddEntityFieldPair(FareStageListFields.FareStageListID, FareMatrixFields.FareStageListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrix.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareMatrixFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareMatrixRelations
	{
		internal static readonly IEntityRelation FareMatrixEntityUsingMasterMatrixIDStatic = new FareMatrixRelations().FareMatrixEntityUsingMasterMatrixID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingFareMatrixIDStatic = new FareMatrixRelations().FareMatrixEntryEntityUsingFareMatrixID;
		internal static readonly IEntityRelation TicketEntityUsingMatrixIDStatic = new FareMatrixRelations().TicketEntityUsingMatrixID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingMatrixIDStatic = new FareMatrixRelations().TicketDeviceClassEntityUsingMatrixID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIDStatic = new FareMatrixRelations().ClientEntityUsingOwnerClientID;
		internal static readonly IEntityRelation FareMatrixEntityUsingFareMatrixIDMasterMatrixIDStatic = new FareMatrixRelations().FareMatrixEntityUsingFareMatrixIDMasterMatrixID;
		internal static readonly IEntityRelation FareStageListEntityUsingFareStageListIDStatic = new FareMatrixRelations().FareStageListEntityUsingFareStageListID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new FareMatrixRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticFareMatrixRelations()
		{
		}
	}
}
