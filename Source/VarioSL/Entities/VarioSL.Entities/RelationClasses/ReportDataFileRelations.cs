﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportDataFile. </summary>
	public partial class ReportDataFileRelations
	{
		/// <summary>CTor</summary>
		public ReportDataFileRelations()
		{
		}

		/// <summary>Gets all relations of the ReportDataFileEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportEntityUsingDataFileID);
			toReturn.Add(this.ReportEntityUsingReportID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportDataFileEntity and ReportEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportDataFile.ReportDataFileID - Report.DataFileID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingDataFileID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Reports" , true);
				relation.AddEntityFieldPair(ReportDataFileFields.ReportDataFileID, ReportFields.DataFileID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportDataFileEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReportDataFileEntity and ReportEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportDataFile.ReportID - Report.ReportID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Report", false);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportDataFileFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportDataFileEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportDataFileRelations
	{
		internal static readonly IEntityRelation ReportEntityUsingDataFileIDStatic = new ReportDataFileRelations().ReportEntityUsingDataFileID;
		internal static readonly IEntityRelation ReportEntityUsingReportIDStatic = new ReportDataFileRelations().ReportEntityUsingReportID;

		/// <summary>CTor</summary>
		static StaticReportDataFileRelations()
		{
		}
	}
}
