﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyQuestion. </summary>
	public partial class SurveyQuestionRelations
	{
		/// <summary>CTor</summary>
		public SurveyQuestionRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyQuestionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyAnswerEntityUsingSurveyQuestionID);
			toReturn.Add(this.SurveyChoiceEntityUsingFollowUpQuestionID);
			toReturn.Add(this.SurveyChoiceEntityUsingSurveyQuestionID);
			toReturn.Add(this.SurveyEntityUsingSurveyID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyAnswerEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionID - SurveyAnswer.SurveyQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingSurveyQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswers" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyAnswerFields.SurveyQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionID - SurveyChoice.FollowUpQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyChoiceEntityUsingFollowUpQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FollowedUpChoices" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyChoiceFields.FollowUpQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyQuestionID - SurveyChoice.SurveyQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyChoiceEntityUsingSurveyQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyChoices" , true);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyChoiceFields.SurveyQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyQuestionEntity and SurveyEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyQuestion.SurveyID - Survey.SurveyID
		/// </summary>
		public virtual IEntityRelation SurveyEntityUsingSurveyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Survey", false);
				relation.AddEntityFieldPair(SurveyFields.SurveyID, SurveyQuestionFields.SurveyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyQuestionRelations
	{
		internal static readonly IEntityRelation SurveyAnswerEntityUsingSurveyQuestionIDStatic = new SurveyQuestionRelations().SurveyAnswerEntityUsingSurveyQuestionID;
		internal static readonly IEntityRelation SurveyChoiceEntityUsingFollowUpQuestionIDStatic = new SurveyQuestionRelations().SurveyChoiceEntityUsingFollowUpQuestionID;
		internal static readonly IEntityRelation SurveyChoiceEntityUsingSurveyQuestionIDStatic = new SurveyQuestionRelations().SurveyChoiceEntityUsingSurveyQuestionID;
		internal static readonly IEntityRelation SurveyEntityUsingSurveyIDStatic = new SurveyQuestionRelations().SurveyEntityUsingSurveyID;

		/// <summary>CTor</summary>
		static StaticSurveyQuestionRelations()
		{
		}
	}
}
