﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Tariff. </summary>
	public partial class TariffRelations
	{
		/// <summary>CTor</summary>
		public TariffRelations()
		{
		}

		/// <summary>Gets all relations of the TariffEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListEntityUsingTariffID);
			toReturn.Add(this.AreaListElementEntityUsingTariffID);
			toReturn.Add(this.AttributeEntityUsingTarifID);
			toReturn.Add(this.BusinessRuleEntityUsingTariffID);
			toReturn.Add(this.BusinessRuleConditionEntityUsingTariffID);
			toReturn.Add(this.BusinessRuleResultEntityUsingTariffID);
			toReturn.Add(this.CalendarEntityUsingTariffId);
			toReturn.Add(this.CardTicketEntityUsingTariffID);
			toReturn.Add(this.ChoiceEntityUsingTariffID);
			toReturn.Add(this.ClientAdaptedLayoutObjectEntityUsingTariffID);
			toReturn.Add(this.EticketEntityUsingTariffID);
			toReturn.Add(this.ExternalCardEntityUsingTariffId);
			toReturn.Add(this.ExternalEffortEntityUsingTariffID);
			toReturn.Add(this.ExternalPacketEntityUsingTariffID);
			toReturn.Add(this.ExternalPacketEffortEntityUsingTariffID);
			toReturn.Add(this.ExternalTypeEntityUsingTariffID);
			toReturn.Add(this.FareEvasionCategoryEntityUsingTariffID);
			toReturn.Add(this.FareEvasionReasonEntityUsingTariffID);
			toReturn.Add(this.FareMatrixEntityUsingTariffID);
			toReturn.Add(this.FareStageListEntityUsingTariffID);
			toReturn.Add(this.FareTableEntityUsingTariffID);
			toReturn.Add(this.GuiDefEntityUsingTariffID);
			toReturn.Add(this.KeyAttributeTransfromEntityUsingTariffID);
			toReturn.Add(this.LayoutEntityUsingTariffID);
			toReturn.Add(this.LineGroupEntityUsingTariffID);
			toReturn.Add(this.LogoEntityUsingTariffId);
			toReturn.Add(this.PageContentGroupEntityUsingTariffID);
			toReturn.Add(this.ParameterAttributeValueEntityUsingTariffID);
			toReturn.Add(this.ParameterFareStageEntityUsingTariffID);
			toReturn.Add(this.ParameterTariffEntityUsingTariffID);
			toReturn.Add(this.ParameterTicketEntityUsingTariffID);
			toReturn.Add(this.PredefinedKeyEntityUsingTariffID);
			toReturn.Add(this.PrimalKeyEntityUsingTariffID);
			toReturn.Add(this.PrintTextEntityUsingTariffId);
			toReturn.Add(this.RouteEntityUsingTariffID);
			toReturn.Add(this.RouteNameEntityUsingTariffId);
			toReturn.Add(this.RuleCappingEntityUsingTariffID);
			toReturn.Add(this.ServiceAllocationEntityUsingTariffId);
			toReturn.Add(this.ShortDistanceEntityUsingTariffID);
			toReturn.Add(this.SpecialReceiptEntityUsingTariffID);
			toReturn.Add(this.SystemTextEntityUsingTariffID);
			toReturn.Add(this.TariffEntityUsingBaseTariffID);
			toReturn.Add(this.TariffEntityUsingMatrixTariffID);
			toReturn.Add(this.TariffReleaseEntityUsingTariffID);
			toReturn.Add(this.TicketEntityUsingTarifID);
			toReturn.Add(this.TicketGroupEntityUsingTariffID);
			toReturn.Add(this.TranslationEntityUsingTariffID);
			toReturn.Add(this.UserKeyEntityUsingTariffID);
			toReturn.Add(this.VdvKeySetEntityUsingTariffID);
			toReturn.Add(this.VdvLayoutEntityUsingTariffID);
			toReturn.Add(this.VdvProductEntityUsingTariffID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ClientEntityUsingOwnerClientID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.NetEntityUsingNetID);
			toReturn.Add(this.TariffEntityUsingTarifIDBaseTariffID);
			toReturn.Add(this.TariffEntityUsingTarifIDMatrixTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and AreaListEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - AreaList.TariffID
		/// </summary>
		public virtual IEntityRelation AreaListEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaList" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, AreaListFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and AreaListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - AreaListElement.TariffID
		/// </summary>
		public virtual IEntityRelation AreaListElementEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaListElements" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, AreaListElementFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and AttributeEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Attribute.TarifID
		/// </summary>
		public virtual IEntityRelation AttributeEntityUsingTarifID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Attributes" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, AttributeFields.TarifID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and BusinessRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - BusinessRule.TariffID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRule" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and BusinessRuleConditionEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - BusinessRuleCondition.TariffID
		/// </summary>
		public virtual IEntityRelation BusinessRuleConditionEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleCondition" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleConditionFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and BusinessRuleResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - BusinessRuleResult.TariffID
		/// </summary>
		public virtual IEntityRelation BusinessRuleResultEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleResult" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleResultFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and CalendarEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Calendar.TariffId
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Calendars" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, CalendarFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and CardTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - CardTicket.TariffID
		/// </summary>
		public virtual IEntityRelation CardTicketEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardTickets" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, CardTicketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Choice.TariffID
		/// </summary>
		public virtual IEntityRelation ChoiceEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Choices" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ChoiceFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ClientAdaptedLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ClientAdaptedLayoutObject.TariffID
		/// </summary>
		public virtual IEntityRelation ClientAdaptedLayoutObjectEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClientAdaptedLayoutObjects" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ClientAdaptedLayoutObjectFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientAdaptedLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and EticketEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Eticket.TariffID
		/// </summary>
		public virtual IEntityRelation EticketEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Etickets" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, EticketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ExternalCardEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ExternalCard.TariffId
		/// </summary>
		public virtual IEntityRelation ExternalCardEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalCards" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalCardFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ExternalEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ExternalEffort.TariffID
		/// </summary>
		public virtual IEntityRelation ExternalEffortEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalEfforts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalEffortFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ExternalPacketEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ExternalPacket.TariffID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPackets" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalPacketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ExternalPacketEffort.TariffID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketefforts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalPacketEffortFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ExternalTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ExternalType.TariffID
		/// </summary>
		public virtual IEntityRelation ExternalTypeEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalTypes" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalTypeFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and FareEvasionCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - FareEvasionCategory.TariffID
		/// </summary>
		public virtual IEntityRelation FareEvasionCategoryEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionCategories" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareEvasionCategoryFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and FareEvasionReasonEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - FareEvasionReason.TariffID
		/// </summary>
		public virtual IEntityRelation FareEvasionReasonEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionReasons" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareEvasionReasonFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and FareMatrixEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - FareMatrix.TariffID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrices" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareMatrixFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and FareStageListEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - FareStageList.TariffID
		/// </summary>
		public virtual IEntityRelation FareStageListEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareStageLists" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareStageListFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and FareTableEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - FareTable.TariffID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTables" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareTableFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and GuiDefEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - GuiDef.TariffID
		/// </summary>
		public virtual IEntityRelation GuiDefEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GuiDefs" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, GuiDefFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and KeyAttributeTransfromEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - KeyAttributeTransfrom.TariffID
		/// </summary>
		public virtual IEntityRelation KeyAttributeTransfromEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "KeyAttributeTransforms" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, KeyAttributeTransfromFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and LayoutEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Layout.TariffID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Layouts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, LayoutFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and LineGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - LineGroup.TariffID
		/// </summary>
		public virtual IEntityRelation LineGroupEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LineGroups" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, LineGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and LogoEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Logo.TariffId
		/// </summary>
		public virtual IEntityRelation LogoEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Logos" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, LogoFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and PageContentGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - PageContentGroup.TariffID
		/// </summary>
		public virtual IEntityRelation PageContentGroupEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageContentGroups" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, PageContentGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ParameterAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ParameterAttributeValue.TariffID
		/// </summary>
		public virtual IEntityRelation ParameterAttributeValueEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterAttributeValues" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterAttributeValueFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ParameterFareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ParameterFareStage.TariffID
		/// </summary>
		public virtual IEntityRelation ParameterFareStageEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterFareStage" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterFareStageFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ParameterTariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ParameterTariff.TariffID
		/// </summary>
		public virtual IEntityRelation ParameterTariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterTariff" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterTariffFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ParameterTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ParameterTicket.TariffID
		/// </summary>
		public virtual IEntityRelation ParameterTicketEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterTickets" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterTicketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and PredefinedKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - PredefinedKey.TariffID
		/// </summary>
		public virtual IEntityRelation PredefinedKeyEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PredefinedKeys" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, PredefinedKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and PrimalKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - PrimalKey.TariffID
		/// </summary>
		public virtual IEntityRelation PrimalKeyEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PrimalKeys" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, PrimalKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and PrintTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - PrintText.TariffId
		/// </summary>
		public virtual IEntityRelation PrintTextEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PrintTexts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, PrintTextFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Route.TariffID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Routes" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, RouteFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and RouteNameEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - RouteName.TariffId
		/// </summary>
		public virtual IEntityRelation RouteNameEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RouteNames" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, RouteNameFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and RuleCappingEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - RuleCapping.TariffID
		/// </summary>
		public virtual IEntityRelation RuleCappingEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCappings" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, RuleCappingFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ServiceAllocationEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ServiceAllocation.TariffId
		/// </summary>
		public virtual IEntityRelation ServiceAllocationEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceAllocations" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ServiceAllocationFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceAllocationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ShortDistanceEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - ShortDistance.TariffID
		/// </summary>
		public virtual IEntityRelation ShortDistanceEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ShortDistances" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, ShortDistanceFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShortDistanceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and SpecialReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - SpecialReceipt.TariffID
		/// </summary>
		public virtual IEntityRelation SpecialReceiptEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SpecialReceipts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, SpecialReceiptFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SpecialReceiptEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and SystemTextEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - SystemText.TariffID
		/// </summary>
		public virtual IEntityRelation SystemTextEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SystemTexts" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, SystemTextFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemTextEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Tariff.BaseTariffID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingBaseTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildTariffs" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TariffFields.BaseTariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Tariff.MatrixTariffID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingMatrixTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MatrixParentTariff" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TariffFields.MatrixTariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TariffReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - TariffRelease.TariffID
		/// </summary>
		public virtual IEntityRelation TariffReleaseEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TariffRelease" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TariffReleaseFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Ticket.TarifID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTarifID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TicketFields.TarifID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TicketGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - TicketGroup.TariffID
		/// </summary>
		public virtual IEntityRelation TicketGroupEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketGroups" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TicketGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TranslationEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - Translation.TariffID
		/// </summary>
		public virtual IEntityRelation TranslationEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Translations" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, TranslationFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TranslationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and UserKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - UserKey.TariffID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserKeys" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, UserKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and VdvKeySetEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - VdvKeySet.TariffID
		/// </summary>
		public virtual IEntityRelation VdvKeySetEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvKeySet" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, VdvKeySetFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvKeySetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and VdvLayoutEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - VdvLayout.TariffID
		/// </summary>
		public virtual IEntityRelation VdvLayoutEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvLayout" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, VdvLayoutFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Tariff.TarifID - VdvProduct.TariffID
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProduct" , true);
				relation.AddEntityFieldPair(TariffFields.TarifID, VdvProductFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TariffFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TariffEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.OwnerClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OwnerClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TariffFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TariffEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TariffFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TariffEntity and NetEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.NetID - Net.NetID
		/// </summary>
		public virtual IEntityRelation NetEntityUsingNetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Net", false);
				relation.AddEntityFieldPair(NetFields.NetID, TariffFields.NetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.BaseTariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTarifIDBaseTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BaseTariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, TariffFields.BaseTariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TariffEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Tariff.MatrixTariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTarifIDMatrixTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MatrixTariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, TariffFields.MatrixTariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTariffRelations
	{
		internal static readonly IEntityRelation AreaListEntityUsingTariffIDStatic = new TariffRelations().AreaListEntityUsingTariffID;
		internal static readonly IEntityRelation AreaListElementEntityUsingTariffIDStatic = new TariffRelations().AreaListElementEntityUsingTariffID;
		internal static readonly IEntityRelation AttributeEntityUsingTarifIDStatic = new TariffRelations().AttributeEntityUsingTarifID;
		internal static readonly IEntityRelation BusinessRuleEntityUsingTariffIDStatic = new TariffRelations().BusinessRuleEntityUsingTariffID;
		internal static readonly IEntityRelation BusinessRuleConditionEntityUsingTariffIDStatic = new TariffRelations().BusinessRuleConditionEntityUsingTariffID;
		internal static readonly IEntityRelation BusinessRuleResultEntityUsingTariffIDStatic = new TariffRelations().BusinessRuleResultEntityUsingTariffID;
		internal static readonly IEntityRelation CalendarEntityUsingTariffIdStatic = new TariffRelations().CalendarEntityUsingTariffId;
		internal static readonly IEntityRelation CardTicketEntityUsingTariffIDStatic = new TariffRelations().CardTicketEntityUsingTariffID;
		internal static readonly IEntityRelation ChoiceEntityUsingTariffIDStatic = new TariffRelations().ChoiceEntityUsingTariffID;
		internal static readonly IEntityRelation ClientAdaptedLayoutObjectEntityUsingTariffIDStatic = new TariffRelations().ClientAdaptedLayoutObjectEntityUsingTariffID;
		internal static readonly IEntityRelation EticketEntityUsingTariffIDStatic = new TariffRelations().EticketEntityUsingTariffID;
		internal static readonly IEntityRelation ExternalCardEntityUsingTariffIdStatic = new TariffRelations().ExternalCardEntityUsingTariffId;
		internal static readonly IEntityRelation ExternalEffortEntityUsingTariffIDStatic = new TariffRelations().ExternalEffortEntityUsingTariffID;
		internal static readonly IEntityRelation ExternalPacketEntityUsingTariffIDStatic = new TariffRelations().ExternalPacketEntityUsingTariffID;
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingTariffIDStatic = new TariffRelations().ExternalPacketEffortEntityUsingTariffID;
		internal static readonly IEntityRelation ExternalTypeEntityUsingTariffIDStatic = new TariffRelations().ExternalTypeEntityUsingTariffID;
		internal static readonly IEntityRelation FareEvasionCategoryEntityUsingTariffIDStatic = new TariffRelations().FareEvasionCategoryEntityUsingTariffID;
		internal static readonly IEntityRelation FareEvasionReasonEntityUsingTariffIDStatic = new TariffRelations().FareEvasionReasonEntityUsingTariffID;
		internal static readonly IEntityRelation FareMatrixEntityUsingTariffIDStatic = new TariffRelations().FareMatrixEntityUsingTariffID;
		internal static readonly IEntityRelation FareStageListEntityUsingTariffIDStatic = new TariffRelations().FareStageListEntityUsingTariffID;
		internal static readonly IEntityRelation FareTableEntityUsingTariffIDStatic = new TariffRelations().FareTableEntityUsingTariffID;
		internal static readonly IEntityRelation GuiDefEntityUsingTariffIDStatic = new TariffRelations().GuiDefEntityUsingTariffID;
		internal static readonly IEntityRelation KeyAttributeTransfromEntityUsingTariffIDStatic = new TariffRelations().KeyAttributeTransfromEntityUsingTariffID;
		internal static readonly IEntityRelation LayoutEntityUsingTariffIDStatic = new TariffRelations().LayoutEntityUsingTariffID;
		internal static readonly IEntityRelation LineGroupEntityUsingTariffIDStatic = new TariffRelations().LineGroupEntityUsingTariffID;
		internal static readonly IEntityRelation LogoEntityUsingTariffIdStatic = new TariffRelations().LogoEntityUsingTariffId;
		internal static readonly IEntityRelation PageContentGroupEntityUsingTariffIDStatic = new TariffRelations().PageContentGroupEntityUsingTariffID;
		internal static readonly IEntityRelation ParameterAttributeValueEntityUsingTariffIDStatic = new TariffRelations().ParameterAttributeValueEntityUsingTariffID;
		internal static readonly IEntityRelation ParameterFareStageEntityUsingTariffIDStatic = new TariffRelations().ParameterFareStageEntityUsingTariffID;
		internal static readonly IEntityRelation ParameterTariffEntityUsingTariffIDStatic = new TariffRelations().ParameterTariffEntityUsingTariffID;
		internal static readonly IEntityRelation ParameterTicketEntityUsingTariffIDStatic = new TariffRelations().ParameterTicketEntityUsingTariffID;
		internal static readonly IEntityRelation PredefinedKeyEntityUsingTariffIDStatic = new TariffRelations().PredefinedKeyEntityUsingTariffID;
		internal static readonly IEntityRelation PrimalKeyEntityUsingTariffIDStatic = new TariffRelations().PrimalKeyEntityUsingTariffID;
		internal static readonly IEntityRelation PrintTextEntityUsingTariffIdStatic = new TariffRelations().PrintTextEntityUsingTariffId;
		internal static readonly IEntityRelation RouteEntityUsingTariffIDStatic = new TariffRelations().RouteEntityUsingTariffID;
		internal static readonly IEntityRelation RouteNameEntityUsingTariffIdStatic = new TariffRelations().RouteNameEntityUsingTariffId;
		internal static readonly IEntityRelation RuleCappingEntityUsingTariffIDStatic = new TariffRelations().RuleCappingEntityUsingTariffID;
		internal static readonly IEntityRelation ServiceAllocationEntityUsingTariffIdStatic = new TariffRelations().ServiceAllocationEntityUsingTariffId;
		internal static readonly IEntityRelation ShortDistanceEntityUsingTariffIDStatic = new TariffRelations().ShortDistanceEntityUsingTariffID;
		internal static readonly IEntityRelation SpecialReceiptEntityUsingTariffIDStatic = new TariffRelations().SpecialReceiptEntityUsingTariffID;
		internal static readonly IEntityRelation SystemTextEntityUsingTariffIDStatic = new TariffRelations().SystemTextEntityUsingTariffID;
		internal static readonly IEntityRelation TariffEntityUsingBaseTariffIDStatic = new TariffRelations().TariffEntityUsingBaseTariffID;
		internal static readonly IEntityRelation TariffEntityUsingMatrixTariffIDStatic = new TariffRelations().TariffEntityUsingMatrixTariffID;
		internal static readonly IEntityRelation TariffReleaseEntityUsingTariffIDStatic = new TariffRelations().TariffReleaseEntityUsingTariffID;
		internal static readonly IEntityRelation TicketEntityUsingTarifIDStatic = new TariffRelations().TicketEntityUsingTarifID;
		internal static readonly IEntityRelation TicketGroupEntityUsingTariffIDStatic = new TariffRelations().TicketGroupEntityUsingTariffID;
		internal static readonly IEntityRelation TranslationEntityUsingTariffIDStatic = new TariffRelations().TranslationEntityUsingTariffID;
		internal static readonly IEntityRelation UserKeyEntityUsingTariffIDStatic = new TariffRelations().UserKeyEntityUsingTariffID;
		internal static readonly IEntityRelation VdvKeySetEntityUsingTariffIDStatic = new TariffRelations().VdvKeySetEntityUsingTariffID;
		internal static readonly IEntityRelation VdvLayoutEntityUsingTariffIDStatic = new TariffRelations().VdvLayoutEntityUsingTariffID;
		internal static readonly IEntityRelation VdvProductEntityUsingTariffIDStatic = new TariffRelations().VdvProductEntityUsingTariffID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new TariffRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIDStatic = new TariffRelations().ClientEntityUsingOwnerClientID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new TariffRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation NetEntityUsingNetIDStatic = new TariffRelations().NetEntityUsingNetID;
		internal static readonly IEntityRelation TariffEntityUsingTarifIDBaseTariffIDStatic = new TariffRelations().TariffEntityUsingTarifIDBaseTariffID;
		internal static readonly IEntityRelation TariffEntityUsingTarifIDMatrixTariffIDStatic = new TariffRelations().TariffEntityUsingTarifIDMatrixTariffID;

		/// <summary>CTor</summary>
		static StaticTariffRelations()
		{
		}
	}
}
