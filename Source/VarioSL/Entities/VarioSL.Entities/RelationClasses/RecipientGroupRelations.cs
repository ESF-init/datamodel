﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RecipientGroup. </summary>
	public partial class RecipientGroupRelations
	{
		/// <summary>CTor</summary>
		public RecipientGroupRelations()
		{
		}

		/// <summary>Gets all relations of the RecipientGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.MessageEntityUsingRecipientGroupID);
			toReturn.Add(this.RecipientGroupMemberEntityUsingRecipientGroupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RecipientGroupEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// RecipientGroup.RecipientGroupID - Message.RecipientGroupID
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingRecipientGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Messages" , true);
				relation.AddEntityFieldPair(RecipientGroupFields.RecipientGroupID, MessageFields.RecipientGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RecipientGroupEntity and RecipientGroupMemberEntity over the 1:n relation they have, using the relation between the fields:
		/// RecipientGroup.RecipientGroupID - RecipientGroupMember.RecipientGroupID
		/// </summary>
		public virtual IEntityRelation RecipientGroupMemberEntityUsingRecipientGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RecipientGroupMembers" , true);
				relation.AddEntityFieldPair(RecipientGroupFields.RecipientGroupID, RecipientGroupMemberFields.RecipientGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupMemberEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRecipientGroupRelations
	{
		internal static readonly IEntityRelation MessageEntityUsingRecipientGroupIDStatic = new RecipientGroupRelations().MessageEntityUsingRecipientGroupID;
		internal static readonly IEntityRelation RecipientGroupMemberEntityUsingRecipientGroupIDStatic = new RecipientGroupRelations().RecipientGroupMemberEntityUsingRecipientGroupID;

		/// <summary>CTor</summary>
		static StaticRecipientGroupRelations()
		{
		}
	}
}
