﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Coordinate. </summary>
	public partial class CoordinateRelations
	{
		/// <summary>CTor</summary>
		public CoordinateRelations()
		{
		}

		/// <summary>Gets all relations of the CoordinateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.StopEntityUsingCoordinateID);
			toReturn.Add(this.AddressEntityUsingCoordinateID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CoordinateEntity and StopEntity over the 1:n relation they have, using the relation between the fields:
		/// Coordinate.CoordinateID - Stop.CoordinateID
		/// </summary>
		public virtual IEntityRelation StopEntityUsingCoordinateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Stops" , true);
				relation.AddEntityFieldPair(CoordinateFields.CoordinateID, StopFields.CoordinateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CoordinateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StopEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CoordinateEntity and AddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Coordinate.CoordinateID - Address.CoordinateID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingCoordinateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Addresses" , true);
				relation.AddEntityFieldPair(CoordinateFields.CoordinateID, AddressFields.CoordinateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CoordinateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCoordinateRelations
	{
		internal static readonly IEntityRelation StopEntityUsingCoordinateIDStatic = new CoordinateRelations().StopEntityUsingCoordinateID;
		internal static readonly IEntityRelation AddressEntityUsingCoordinateIDStatic = new CoordinateRelations().AddressEntityUsingCoordinateID;

		/// <summary>CTor</summary>
		static StaticCoordinateRelations()
		{
		}
	}
}
