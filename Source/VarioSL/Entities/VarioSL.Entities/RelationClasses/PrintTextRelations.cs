﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PrintText. </summary>
	public partial class PrintTextRelations
	{
		/// <summary>CTor</summary>
		public PrintTextRelations()
		{
		}

		/// <summary>Gets all relations of the PrintTextEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RouteEntityUsingPrintText1ID);
			toReturn.Add(this.RouteEntityUsingPrintText2ID);
			toReturn.Add(this.RouteEntityUsingPrintText3ID);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PrintTextEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// PrintText.PrintTextID - Route.PrintText1ID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingPrintText1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutesPrintText1" , true);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PrintTextEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// PrintText.PrintTextID - Route.PrintText2ID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingPrintText2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutesPrintText2" , true);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PrintTextEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// PrintText.PrintTextID - Route.PrintText3ID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingPrintText3ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoutesPrintText3" , true);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText3ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PrintTextEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// PrintText.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, PrintTextFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPrintTextRelations
	{
		internal static readonly IEntityRelation RouteEntityUsingPrintText1IDStatic = new PrintTextRelations().RouteEntityUsingPrintText1ID;
		internal static readonly IEntityRelation RouteEntityUsingPrintText2IDStatic = new PrintTextRelations().RouteEntityUsingPrintText2ID;
		internal static readonly IEntityRelation RouteEntityUsingPrintText3IDStatic = new PrintTextRelations().RouteEntityUsingPrintText3ID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new PrintTextRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticPrintTextRelations()
		{
		}
	}
}
