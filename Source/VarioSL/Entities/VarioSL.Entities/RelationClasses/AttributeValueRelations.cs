﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AttributeValue. </summary>
	public partial class AttributeValueRelations
	{
		/// <summary>CTor</summary>
		public AttributeValueRelations()
		{
		}

		/// <summary>Gets all relations of the AttributeValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListEntityUsingTicketGroupAttributeID);
			toReturn.Add(this.AttributeValueListEntityUsingAttributeValueID);
			toReturn.Add(this.BusinessRuleResultEntityUsingUserGroupAttributeValue);
			toReturn.Add(this.ChoiceEntityUsingDistanceAttributeID);
			toReturn.Add(this.FareMatrixEntryEntityUsingDirectionAttributeID);
			toReturn.Add(this.FareMatrixEntryEntityUsingDistanceAttributeID);
			toReturn.Add(this.FareMatrixEntryEntityUsingLineFilterAttributeID);
			toReturn.Add(this.FareMatrixEntryEntityUsingTariffAttributeID);
			toReturn.Add(this.FareMatrixEntryEntityUsingUseDaysAttributeID);
			toReturn.Add(this.FareStageListEntityUsingAttributeValueID);
			toReturn.Add(this.FareTableEntryEntityUsingAttributeValueID);
			toReturn.Add(this.KeyAttributeTransfromEntityUsingAttributeValueFromID);
			toReturn.Add(this.KeyAttributeTransfromEntityUsingAttributeValueToID);
			toReturn.Add(this.ParameterAttributeValueEntityUsingAttributeValueID);
			toReturn.Add(this.RouteEntityUsingTariffAttributeID);
			toReturn.Add(this.RouteEntityUsingTicketGroupAttributeID);
			toReturn.Add(this.VdvProductEntityUsingDistanceAttributeValueID);
			toReturn.Add(this.AttributeEntityUsingAttributeID);
			toReturn.Add(this.LogoEntityUsingLogo1ID);
			toReturn.Add(this.LogoEntityUsingLogo2ID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and AreaListEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - AreaList.TicketGroupAttributeID
		/// </summary>
		public virtual IEntityRelation AreaListEntityUsingTicketGroupAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaList" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, AreaListFields.TicketGroupAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and AttributeValueListEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - AttributeValueList.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueListEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValueList" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, AttributeValueListFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and BusinessRuleResultEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - BusinessRuleResult.UserGroupAttributeValue
		/// </summary>
		public virtual IEntityRelation BusinessRuleResultEntityUsingUserGroupAttributeValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleResultUserGroup" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, BusinessRuleResultFields.UserGroupAttributeValue);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and ChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - Choice.DistanceAttributeID
		/// </summary>
		public virtual IEntityRelation ChoiceEntityUsingDistanceAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Choices" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, ChoiceFields.DistanceAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareMatrixEntry.DirectionAttributeID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingDirectionAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DirectionAttributeValueFareMatrixEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.DirectionAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareMatrixEntry.DistanceAttributeID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingDistanceAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DistanceAttributeValueFareMatrixEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.DistanceAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareMatrixEntry.LineFilterAttributeID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingLineFilterAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LineFilterAttributeValueFareMatrixEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.LineFilterAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareMatrixEntry.TariffAttributeID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingTariffAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TariffAttributeValueFareMatrixEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.TariffAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareMatrixEntry.UseDaysAttributeID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingUseDaysAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UseDaysAttributeFareMatrixEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.UseDaysAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareStageListEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareStageList.AttributeValueID
		/// </summary>
		public virtual IEntityRelation FareStageListEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareStageLists" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareStageListFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and FareTableEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - FareTableEntry.AttributeValueID
		/// </summary>
		public virtual IEntityRelation FareTableEntryEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTableEntries" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareTableEntryFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and KeyAttributeTransfromEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - KeyAttributeTransfrom.AttributeValueFromID
		/// </summary>
		public virtual IEntityRelation KeyAttributeTransfromEntityUsingAttributeValueFromID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Key1AttributeTransforms" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, KeyAttributeTransfromFields.AttributeValueFromID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and KeyAttributeTransfromEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - KeyAttributeTransfrom.AttributeValueToID
		/// </summary>
		public virtual IEntityRelation KeyAttributeTransfromEntityUsingAttributeValueToID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Key2AttributeTransforms" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, KeyAttributeTransfromFields.AttributeValueToID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and ParameterAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - ParameterAttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation ParameterAttributeValueEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterAttributeValues" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, ParameterAttributeValueFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - Route.TariffAttributeID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingTariffAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TariffAttributeRoutes" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, RouteFields.TariffAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - Route.TicketGroupAttributeID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingTicketGroupAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketGroupAttributeRoutes" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, RouteFields.TicketGroupAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// AttributeValue.AttributeValueID - VdvProduct.DistanceAttributeValueID
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingDistanceAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProduct" , true);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, VdvProductFields.DistanceAttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and AttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeValue.AttributeID - Attribute.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Attribute", false);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, AttributeValueFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and LogoEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeValue.Logo1ID - Logo.LogoID
		/// </summary>
		public virtual IEntityRelation LogoEntityUsingLogo1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Logo1", false);
				relation.AddEntityFieldPair(LogoFields.LogoID, AttributeValueFields.Logo1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttributeValueEntity and LogoEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeValue.Logo2ID - Logo.LogoID
		/// </summary>
		public virtual IEntityRelation LogoEntityUsingLogo2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Logo2", false);
				relation.AddEntityFieldPair(LogoFields.LogoID, AttributeValueFields.Logo2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttributeValueRelations
	{
		internal static readonly IEntityRelation AreaListEntityUsingTicketGroupAttributeIDStatic = new AttributeValueRelations().AreaListEntityUsingTicketGroupAttributeID;
		internal static readonly IEntityRelation AttributeValueListEntityUsingAttributeValueIDStatic = new AttributeValueRelations().AttributeValueListEntityUsingAttributeValueID;
		internal static readonly IEntityRelation BusinessRuleResultEntityUsingUserGroupAttributeValueStatic = new AttributeValueRelations().BusinessRuleResultEntityUsingUserGroupAttributeValue;
		internal static readonly IEntityRelation ChoiceEntityUsingDistanceAttributeIDStatic = new AttributeValueRelations().ChoiceEntityUsingDistanceAttributeID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingDirectionAttributeIDStatic = new AttributeValueRelations().FareMatrixEntryEntityUsingDirectionAttributeID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingDistanceAttributeIDStatic = new AttributeValueRelations().FareMatrixEntryEntityUsingDistanceAttributeID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingLineFilterAttributeIDStatic = new AttributeValueRelations().FareMatrixEntryEntityUsingLineFilterAttributeID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingTariffAttributeIDStatic = new AttributeValueRelations().FareMatrixEntryEntityUsingTariffAttributeID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingUseDaysAttributeIDStatic = new AttributeValueRelations().FareMatrixEntryEntityUsingUseDaysAttributeID;
		internal static readonly IEntityRelation FareStageListEntityUsingAttributeValueIDStatic = new AttributeValueRelations().FareStageListEntityUsingAttributeValueID;
		internal static readonly IEntityRelation FareTableEntryEntityUsingAttributeValueIDStatic = new AttributeValueRelations().FareTableEntryEntityUsingAttributeValueID;
		internal static readonly IEntityRelation KeyAttributeTransfromEntityUsingAttributeValueFromIDStatic = new AttributeValueRelations().KeyAttributeTransfromEntityUsingAttributeValueFromID;
		internal static readonly IEntityRelation KeyAttributeTransfromEntityUsingAttributeValueToIDStatic = new AttributeValueRelations().KeyAttributeTransfromEntityUsingAttributeValueToID;
		internal static readonly IEntityRelation ParameterAttributeValueEntityUsingAttributeValueIDStatic = new AttributeValueRelations().ParameterAttributeValueEntityUsingAttributeValueID;
		internal static readonly IEntityRelation RouteEntityUsingTariffAttributeIDStatic = new AttributeValueRelations().RouteEntityUsingTariffAttributeID;
		internal static readonly IEntityRelation RouteEntityUsingTicketGroupAttributeIDStatic = new AttributeValueRelations().RouteEntityUsingTicketGroupAttributeID;
		internal static readonly IEntityRelation VdvProductEntityUsingDistanceAttributeValueIDStatic = new AttributeValueRelations().VdvProductEntityUsingDistanceAttributeValueID;
		internal static readonly IEntityRelation AttributeEntityUsingAttributeIDStatic = new AttributeValueRelations().AttributeEntityUsingAttributeID;
		internal static readonly IEntityRelation LogoEntityUsingLogo1IDStatic = new AttributeValueRelations().LogoEntityUsingLogo1ID;
		internal static readonly IEntityRelation LogoEntityUsingLogo2IDStatic = new AttributeValueRelations().LogoEntityUsingLogo2ID;

		/// <summary>CTor</summary>
		static StaticAttributeValueRelations()
		{
		}
	}
}
