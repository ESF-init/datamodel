﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageContentGroupToContent. </summary>
	public partial class PageContentGroupToContentRelations
	{
		/// <summary>CTor</summary>
		public PageContentGroupToContentRelations()
		{
		}

		/// <summary>Gets all relations of the PageContentGroupToContentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PageContentGroupEntityUsingPageContentGroupid);
			toReturn.Add(this.PageContentEntityUsingPageContentID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PageContentGroupToContentEntity and PageContentGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// PageContentGroupToContent.PageContentGroupid - PageContentGroup.PageContentGroupID
		/// </summary>
		public virtual IEntityRelation PageContentGroupEntityUsingPageContentGroupid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageContentGroup", false);
				relation.AddEntityFieldPair(PageContentGroupFields.PageContentGroupID, PageContentGroupToContentFields.PageContentGroupid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupToContentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PageContentGroupToContentEntity and PageContentEntity over the m:1 relation they have, using the relation between the fields:
		/// PageContentGroupToContent.PageContentID - PageContent.PageContentID
		/// </summary>
		public virtual IEntityRelation PageContentEntityUsingPageContentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageContent", false);
				relation.AddEntityFieldPair(PageContentFields.PageContentID, PageContentGroupToContentFields.PageContentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupToContentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageContentGroupToContentRelations
	{
		internal static readonly IEntityRelation PageContentGroupEntityUsingPageContentGroupidStatic = new PageContentGroupToContentRelations().PageContentGroupEntityUsingPageContentGroupid;
		internal static readonly IEntityRelation PageContentEntityUsingPageContentIDStatic = new PageContentGroupToContentRelations().PageContentEntityUsingPageContentID;

		/// <summary>CTor</summary>
		static StaticPageContentGroupToContentRelations()
		{
		}
	}
}
