﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EmailMessage. </summary>
	public partial class EmailMessageRelations
	{
		/// <summary>CTor</summary>
		public EmailMessageRelations()
		{
		}

		/// <summary>Gets all relations of the EmailMessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EmailEventEntityUsingEmailEventID);
			toReturn.Add(this.FormEntityUsingFormID);
			toReturn.Add(this.MessageTypeEntityUsingMessageType);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EmailMessageEntity and EmailEventEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailMessage.EmailEventID - EmailEvent.EmailEventID
		/// </summary>
		public virtual IEntityRelation EmailEventEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailEvent", false);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, EmailMessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailMessageEntity and FormEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailMessage.FormID - Form.FormID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Form", false);
				relation.AddEntityFieldPair(FormFields.FormID, EmailMessageFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EmailMessageEntity and MessageTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// EmailMessage.MessageType - MessageType.EnumerationValue
		/// </summary>
		public virtual IEntityRelation MessageTypeEntityUsingMessageType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailMessageType", false);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, EmailMessageFields.MessageType);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEmailMessageRelations
	{
		internal static readonly IEntityRelation EmailEventEntityUsingEmailEventIDStatic = new EmailMessageRelations().EmailEventEntityUsingEmailEventID;
		internal static readonly IEntityRelation FormEntityUsingFormIDStatic = new EmailMessageRelations().FormEntityUsingFormID;
		internal static readonly IEntityRelation MessageTypeEntityUsingMessageTypeStatic = new EmailMessageRelations().MessageTypeEntityUsingMessageType;

		/// <summary>CTor</summary>
		static StaticEmailMessageRelations()
		{
		}
	}
}
