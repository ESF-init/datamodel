﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EmailEvent. </summary>
	public partial class EmailEventRelations
	{
		/// <summary>CTor</summary>
		public EmailEventRelations()
		{
		}

		/// <summary>Gets all relations of the EmailEventEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccountMessageSettingEntityUsingEmailEventID);
			toReturn.Add(this.EmailMessageEntityUsingEmailEventID);
			toReturn.Add(this.EventSettingEntityUsingEmailEventID);
			toReturn.Add(this.MessageEntityUsingEmailEventID);
			toReturn.Add(this.NotificationMessageEntityUsingEmailEventID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EmailEventEntity and AccountMessageSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// EmailEvent.EmailEventID - AccountMessageSetting.EmailEventID
		/// </summary>
		public virtual IEntityRelation AccountMessageSettingEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountMessageSettings" , true);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, AccountMessageSettingFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountMessageSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EmailEventEntity and EmailMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// EmailEvent.EmailEventID - EmailMessage.EmailEventID
		/// </summary>
		public virtual IEntityRelation EmailMessageEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailMessages" , true);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, EmailMessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EmailEventEntity and EventSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// EmailEvent.EmailEventID - EventSetting.EmailEventID
		/// </summary>
		public virtual IEntityRelation EventSettingEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EventSettings" , true);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, EventSettingFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EventSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EmailEventEntity and MessageEntity over the 1:n relation they have, using the relation between the fields:
		/// EmailEvent.EmailEventID - Message.EmailEventID
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Messages" , true);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, MessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EmailEventEntity and NotificationMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// EmailEvent.EmailEventID - NotificationMessage.EmailEventID
		/// </summary>
		public virtual IEntityRelation NotificationMessageEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessages" , true);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, NotificationMessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEmailEventRelations
	{
		internal static readonly IEntityRelation AccountMessageSettingEntityUsingEmailEventIDStatic = new EmailEventRelations().AccountMessageSettingEntityUsingEmailEventID;
		internal static readonly IEntityRelation EmailMessageEntityUsingEmailEventIDStatic = new EmailEventRelations().EmailMessageEntityUsingEmailEventID;
		internal static readonly IEntityRelation EventSettingEntityUsingEmailEventIDStatic = new EmailEventRelations().EventSettingEntityUsingEmailEventID;
		internal static readonly IEntityRelation MessageEntityUsingEmailEventIDStatic = new EmailEventRelations().MessageEntityUsingEmailEventID;
		internal static readonly IEntityRelation NotificationMessageEntityUsingEmailEventIDStatic = new EmailEventRelations().NotificationMessageEntityUsingEmailEventID;

		/// <summary>CTor</summary>
		static StaticEmailEventRelations()
		{
		}
	}
}
