﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProtocolFunction. </summary>
	public partial class ProtocolFunctionRelations
	{
		/// <summary>CTor</summary>
		public ProtocolFunctionRelations()
		{
		}

		/// <summary>Gets all relations of the ProtocolFunctionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProtocolEntityUsingFunctionID);
			toReturn.Add(this.ProtocolFunctionGroupEntityUsingGroupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProtocolFunctionEntity and ProtocolEntity over the 1:n relation they have, using the relation between the fields:
		/// ProtocolFunction.ProtocolFunctionID - Protocol.FunctionID
		/// </summary>
		public virtual IEntityRelation ProtocolEntityUsingFunctionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Protocols" , true);
				relation.AddEntityFieldPair(ProtocolFunctionFields.ProtocolFunctionID, ProtocolFields.FunctionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolFunctionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProtocolFunctionEntity and ProtocolFunctionGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ProtocolFunction.GroupID - ProtocolFunctionGroup.ProtocolFunctionGroupID
		/// </summary>
		public virtual IEntityRelation ProtocolFunctionGroupEntityUsingGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProtocolFunctionGroup", false);
				relation.AddEntityFieldPair(ProtocolFunctionGroupFields.ProtocolFunctionGroupID, ProtocolFunctionFields.GroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolFunctionGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolFunctionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProtocolFunctionRelations
	{
		internal static readonly IEntityRelation ProtocolEntityUsingFunctionIDStatic = new ProtocolFunctionRelations().ProtocolEntityUsingFunctionID;
		internal static readonly IEntityRelation ProtocolFunctionGroupEntityUsingGroupIDStatic = new ProtocolFunctionRelations().ProtocolFunctionGroupEntityUsingGroupID;

		/// <summary>CTor</summary>
		static StaticProtocolFunctionRelations()
		{
		}
	}
}
