﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRuleTypeToVariable. </summary>
	public partial class BusinessRuleTypeToVariableRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleTypeToVariableRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleTypeToVariableEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.BusinessRuleVariableEntityUsingBusinessRuleVariableID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeToVariableEntity and BusinessRuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleTypeToVariable.BusinessRuleTypeID - BusinessRuleType.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleType", false);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleTypeToVariableFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeToVariableEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeToVariableEntity and BusinessRuleVariableEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleTypeToVariable.BusinessRuleVariableID - BusinessRuleVariable.BusinessRuleVariableID
		/// </summary>
		public virtual IEntityRelation BusinessRuleVariableEntityUsingBusinessRuleVariableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleVariable", false);
				relation.AddEntityFieldPair(BusinessRuleVariableFields.BusinessRuleVariableID, BusinessRuleTypeToVariableFields.BusinessRuleVariableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleVariableEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeToVariableEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleTypeToVariableRelations
	{
		internal static readonly IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleTypeToVariableRelations().BusinessRuleTypeEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation BusinessRuleVariableEntityUsingBusinessRuleVariableIDStatic = new BusinessRuleTypeToVariableRelations().BusinessRuleVariableEntityUsingBusinessRuleVariableID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleTypeToVariableRelations()
		{
		}
	}
}
