﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportCategory. </summary>
	public partial class ReportCategoryRelations
	{
		/// <summary>CTor</summary>
		public ReportCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the ReportCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportEntityUsingCategoryID);
			toReturn.Add(this.ReportCategoryEntityUsingMasterCategoryID);
			toReturn.Add(this.UserResourceEntityUsingResourceID);
			toReturn.Add(this.FilterListEntityUsingCategoryFilterListID);
			toReturn.Add(this.ReportCategoryEntityUsingReportCategoryIDMasterCategoryID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportCategoryEntity and ReportEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportCategory.ReportCategoryID - Report.CategoryID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Reports" , true);
				relation.AddEntityFieldPair(ReportCategoryFields.ReportCategoryID, ReportFields.CategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportCategoryEntity and ReportCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportCategory.ReportCategoryID - ReportCategory.MasterCategoryID
		/// </summary>
		public virtual IEntityRelation ReportCategoryEntityUsingMasterCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SubReportCategories" , true);
				relation.AddEntityFieldPair(ReportCategoryFields.ReportCategoryID, ReportCategoryFields.MasterCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReportCategoryEntity and UserResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportCategory.ResourceID - UserResource.ResourceID
		/// </summary>
		public virtual IEntityRelation UserResourceEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserResource", false);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, ReportCategoryFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportCategoryEntity and FilterListEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportCategory.CategoryFilterListID - FilterList.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterListEntityUsingCategoryFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterList", false);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, ReportCategoryFields.CategoryFilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportCategoryEntity and ReportCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportCategory.MasterCategoryID - ReportCategory.ReportCategoryID
		/// </summary>
		public virtual IEntityRelation ReportCategoryEntityUsingReportCategoryIDMasterCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MasterReportCategory", false);
				relation.AddEntityFieldPair(ReportCategoryFields.ReportCategoryID, ReportCategoryFields.MasterCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportCategoryRelations
	{
		internal static readonly IEntityRelation ReportEntityUsingCategoryIDStatic = new ReportCategoryRelations().ReportEntityUsingCategoryID;
		internal static readonly IEntityRelation ReportCategoryEntityUsingMasterCategoryIDStatic = new ReportCategoryRelations().ReportCategoryEntityUsingMasterCategoryID;
		internal static readonly IEntityRelation UserResourceEntityUsingResourceIDStatic = new ReportCategoryRelations().UserResourceEntityUsingResourceID;
		internal static readonly IEntityRelation FilterListEntityUsingCategoryFilterListIDStatic = new ReportCategoryRelations().FilterListEntityUsingCategoryFilterListID;
		internal static readonly IEntityRelation ReportCategoryEntityUsingReportCategoryIDMasterCategoryIDStatic = new ReportCategoryRelations().ReportCategoryEntityUsingReportCategoryIDMasterCategoryID;

		/// <summary>CTor</summary>
		static StaticReportCategoryRelations()
		{
		}
	}
}
