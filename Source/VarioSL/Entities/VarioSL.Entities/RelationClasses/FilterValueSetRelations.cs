﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FilterValueSet. </summary>
	public partial class FilterValueSetRelations
	{
		/// <summary>CTor</summary>
		public FilterValueSetRelations()
		{
		}

		/// <summary>Gets all relations of the FilterValueSetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FilterValueEntityUsingFilterValueSetID);
			toReturn.Add(this.ReportJobEntityUsingFilterValueSetID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.UserListEntityUsingOwnerID);
			toReturn.Add(this.UserListEntityUsingUserID);
			toReturn.Add(this.FilterListEntityUsingFilterListID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and FilterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterValueSet.FilterValueSetID - FilterValue.FilterValueSetID
		/// </summary>
		public virtual IEntityRelation FilterValueEntityUsingFilterValueSetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValues" , true);
				relation.AddEntityFieldPair(FilterValueSetFields.FilterValueSetID, FilterValueFields.FilterValueSetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and ReportJobEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterValueSet.FilterValueSetID - ReportJob.FilterValueSetID
		/// </summary>
		public virtual IEntityRelation ReportJobEntityUsingFilterValueSetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobs" , true);
				relation.AddEntityFieldPair(FilterValueSetFields.FilterValueSetID, ReportJobFields.FilterValueSetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValueSet.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FilterValueSetFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValueSet.OwnerID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingOwnerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, FilterValueSetFields.OwnerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValueSet.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList_", false);
				relation.AddEntityFieldPair(UserListFields.UserID, FilterValueSetFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterValueSetEntity and FilterListEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValueSet.FilterListID - FilterList.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterListEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterList", false);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, FilterValueSetFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFilterValueSetRelations
	{
		internal static readonly IEntityRelation FilterValueEntityUsingFilterValueSetIDStatic = new FilterValueSetRelations().FilterValueEntityUsingFilterValueSetID;
		internal static readonly IEntityRelation ReportJobEntityUsingFilterValueSetIDStatic = new FilterValueSetRelations().ReportJobEntityUsingFilterValueSetID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new FilterValueSetRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation UserListEntityUsingOwnerIDStatic = new FilterValueSetRelations().UserListEntityUsingOwnerID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new FilterValueSetRelations().UserListEntityUsingUserID;
		internal static readonly IEntityRelation FilterListEntityUsingFilterListIDStatic = new FilterValueSetRelations().FilterListEntityUsingFilterListID;

		/// <summary>CTor</summary>
		static StaticFilterValueSetRelations()
		{
		}
	}
}
