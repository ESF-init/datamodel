﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DevicePaymentMethod. </summary>
	public partial class DevicePaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public DevicePaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the DevicePaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClearingResultEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.TicketDevicePaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.ContractToPaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.SalesChannelToPaymentMethodEntityUsingDevicePaymentMethodID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DevicePaymentMethodEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// DevicePaymentMethod.DevicePaymentMethodID - ClearingResult.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResults" , true);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, ClearingResultFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DevicePaymentMethodEntity and TicketDeviceClassPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// DevicePaymentMethod.DevicePaymentMethodID - TicketDeviceClassPaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClassPaymentMethod" , true);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, TicketDeviceClassPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassPaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DevicePaymentMethodEntity and TicketDevicePaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// DevicePaymentMethod.DevicePaymentMethodID - TicketDevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation TicketDevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDevicePaymentMethods" , true);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, TicketDevicePaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDevicePaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DevicePaymentMethodEntity and ContractToPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// DevicePaymentMethod.DevicePaymentMethodID - ContractToPaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation ContractToPaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractToPaymentMethods" , true);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, ContractToPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToPaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DevicePaymentMethodEntity and SalesChannelToPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// DevicePaymentMethod.DevicePaymentMethodID - SalesChannelToPaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation SalesChannelToPaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SalesChannelToPaymentMethods" , true);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, SalesChannelToPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesChannelToPaymentMethodEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDevicePaymentMethodRelations
	{
		internal static readonly IEntityRelation ClearingResultEntityUsingDevicePaymentMethodIDStatic = new DevicePaymentMethodRelations().ClearingResultEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodIDStatic = new DevicePaymentMethodRelations().TicketDeviceClassPaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation TicketDevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new DevicePaymentMethodRelations().TicketDevicePaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation ContractToPaymentMethodEntityUsingDevicePaymentMethodIDStatic = new DevicePaymentMethodRelations().ContractToPaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation SalesChannelToPaymentMethodEntityUsingDevicePaymentMethodIDStatic = new DevicePaymentMethodRelations().SalesChannelToPaymentMethodEntityUsingDevicePaymentMethodID;

		/// <summary>CTor</summary>
		static StaticDevicePaymentMethodRelations()
		{
		}
	}
}
