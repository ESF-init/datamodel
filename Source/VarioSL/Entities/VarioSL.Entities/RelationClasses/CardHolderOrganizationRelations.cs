﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardHolderOrganization. </summary>
	public partial class CardHolderOrganizationRelations
	{
		/// <summary>CTor</summary>
		public CardHolderOrganizationRelations()
		{
		}

		/// <summary>Gets all relations of the CardHolderOrganizationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardHolderEntityUsingCardHolderID);
			toReturn.Add(this.OrganizationEntityUsingOrganizationID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between CardHolderOrganizationEntity and CardHolderEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolderOrganization.CardHolderID - CardHolder.PersonID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardHolder", false);




				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardHolderOrganizationFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderOrganizationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderOrganizationEntity and OrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolderOrganization.OrganizationID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Organization", false);



				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, CardHolderOrganizationFields.OrganizationID);

				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderOrganizationEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardHolderOrganizationRelations
	{
		internal static readonly IEntityRelation CardHolderEntityUsingCardHolderIDStatic = new CardHolderOrganizationRelations().CardHolderEntityUsingCardHolderID;
		internal static readonly IEntityRelation OrganizationEntityUsingOrganizationIDStatic = new CardHolderOrganizationRelations().OrganizationEntityUsingOrganizationID;

		/// <summary>CTor</summary>
		static StaticCardHolderOrganizationRelations()
		{
		}
	}
}
