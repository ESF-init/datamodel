﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvKeyInfo. </summary>
	public partial class VdvKeyInfoRelations
	{
		/// <summary>CTor</summary>
		public VdvKeyInfoRelations()
		{
		}

		/// <summary>Gets all relations of the VdvKeyInfoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SamModuleEntityUsingSamID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between VdvKeyInfoEntity and SamModuleEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvKeyInfo.SamID - SamModule.SamID
		/// </summary>
		public virtual IEntityRelation SamModuleEntityUsingSamID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SamModule", false);
				relation.AddEntityFieldPair(SamModuleFields.SamID, VdvKeyInfoFields.SamID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SamModuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvKeyInfoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvKeyInfoRelations
	{
		internal static readonly IEntityRelation SamModuleEntityUsingSamIDStatic = new VdvKeyInfoRelations().SamModuleEntityUsingSamID;

		/// <summary>CTor</summary>
		static StaticVdvKeyInfoRelations()
		{
		}
	}
}
