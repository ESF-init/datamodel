﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SecurityQuestion. </summary>
	public partial class SecurityQuestionRelations
	{
		/// <summary>CTor</summary>
		public SecurityQuestionRelations()
		{
		}

		/// <summary>Gets all relations of the SecurityQuestionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SecurityResponseEntityUsingSecurityQuestionID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SecurityQuestionEntity and SecurityResponseEntity over the 1:n relation they have, using the relation between the fields:
		/// SecurityQuestion.SecurityQuestionID - SecurityResponse.SecurityQuestionID
		/// </summary>
		public virtual IEntityRelation SecurityResponseEntityUsingSecurityQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SecurityResponses" , true);
				relation.AddEntityFieldPair(SecurityQuestionFields.SecurityQuestionID, SecurityResponseFields.SecurityQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SecurityQuestionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SecurityResponseEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSecurityQuestionRelations
	{
		internal static readonly IEntityRelation SecurityResponseEntityUsingSecurityQuestionIDStatic = new SecurityQuestionRelations().SecurityResponseEntityUsingSecurityQuestionID;

		/// <summary>CTor</summary>
		static StaticSecurityQuestionRelations()
		{
		}
	}
}
