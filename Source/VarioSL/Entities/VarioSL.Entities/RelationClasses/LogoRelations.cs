﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Logo. </summary>
	public partial class LogoRelations
	{
		/// <summary>CTor</summary>
		public LogoRelations()
		{
		}

		/// <summary>Gets all relations of the LogoEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingLogo1ID);
			toReturn.Add(this.AttributeValueEntityUsingLogo2ID);
			toReturn.Add(this.FixedBitmapLayoutObjectEntityUsingLogoID);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LogoEntity and AttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Logo.LogoID - AttributeValue.Logo1ID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingLogo1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValue" , true);
				relation.AddEntityFieldPair(LogoFields.LogoID, AttributeValueFields.Logo1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LogoEntity and AttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Logo.LogoID - AttributeValue.Logo2ID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingLogo2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValue_" , true);
				relation.AddEntityFieldPair(LogoFields.LogoID, AttributeValueFields.Logo2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LogoEntity and FixedBitmapLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Logo.LogoID - FixedBitmapLayoutObject.LogoID
		/// </summary>
		public virtual IEntityRelation FixedBitmapLayoutObjectEntityUsingLogoID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FixedBitmapLayoutObjects" , true);
				relation.AddEntityFieldPair(LogoFields.LogoID, FixedBitmapLayoutObjectFields.LogoID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FixedBitmapLayoutObjectEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LogoEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Logo.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, LogoFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLogoRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingLogo1IDStatic = new LogoRelations().AttributeValueEntityUsingLogo1ID;
		internal static readonly IEntityRelation AttributeValueEntityUsingLogo2IDStatic = new LogoRelations().AttributeValueEntityUsingLogo2ID;
		internal static readonly IEntityRelation FixedBitmapLayoutObjectEntityUsingLogoIDStatic = new LogoRelations().FixedBitmapLayoutObjectEntityUsingLogoID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new LogoRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticLogoRelations()
		{
		}
	}
}
