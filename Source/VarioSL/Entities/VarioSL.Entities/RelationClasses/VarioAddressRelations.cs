﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VarioAddress. </summary>
	public partial class VarioAddressRelations
	{
		/// <summary>CTor</summary>
		public VarioAddressRelations()
		{
		}

		/// <summary>Gets all relations of the VarioAddressEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserListEntityUsingAddressID);
			toReturn.Add(this.DebtorEntityUsingAddressID);
			toReturn.Add(this.DepotEntityUsingAddressID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VarioAddressEntity and UserListEntity over the 1:n relation they have, using the relation between the fields:
		/// VarioAddress.AddressID - UserList.AddressID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserLists" , true);
				relation.AddEntityFieldPair(VarioAddressFields.AddressID, UserListFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VarioAddressEntity and DebtorEntity over the 1:1 relation they have, using the relation between the fields:
		/// VarioAddress.AddressID - Debtor.AddressID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Debtor", true);


				relation.AddEntityFieldPair(VarioAddressFields.AddressID, DebtorFields.AddressID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VarioAddressEntity and DepotEntity over the 1:1 relation they have, using the relation between the fields:
		/// VarioAddress.AddressID - Depot.AddressID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Depot", true);


				relation.AddEntityFieldPair(VarioAddressFields.AddressID, DepotFields.AddressID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVarioAddressRelations
	{
		internal static readonly IEntityRelation UserListEntityUsingAddressIDStatic = new VarioAddressRelations().UserListEntityUsingAddressID;
		internal static readonly IEntityRelation DebtorEntityUsingAddressIDStatic = new VarioAddressRelations().DebtorEntityUsingAddressID;
		internal static readonly IEntityRelation DepotEntityUsingAddressIDStatic = new VarioAddressRelations().DepotEntityUsingAddressID;

		/// <summary>CTor</summary>
		static StaticVarioAddressRelations()
		{
		}
	}
}
