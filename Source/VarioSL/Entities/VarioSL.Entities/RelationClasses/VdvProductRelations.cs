﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvProduct. </summary>
	public partial class VdvProductRelations
	{
		/// <summary>CTor</summary>
		public VdvProductRelations()
		{
		}

		/// <summary>Gets all relations of the VdvProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingDistanceAttributeValueID);
			toReturn.Add(this.RulePeriodEntityUsingPeriodID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.VdvLayoutEntityUsingVdvLayoutID);
			toReturn.Add(this.VdvTypeEntityUsingIssueModeNm);
			toReturn.Add(this.VdvTypeEntityUsingIssueModeSe);
			toReturn.Add(this.VdvTypeEntityUsingPriorityMode);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.DistanceAttributeValueID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingDistanceAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DistanceAttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, VdvProductFields.DistanceAttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and RulePeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.PeriodID - RulePeriod.RulePeriodID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RulePeriod", false);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, VdvProductFields.PeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, VdvProductFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, VdvProductFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and VdvLayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.VdvLayoutID - VdvLayout.LayoutID
		/// </summary>
		public virtual IEntityRelation VdvLayoutEntityUsingVdvLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VdvLayout", false);
				relation.AddEntityFieldPair(VdvLayoutFields.LayoutID, VdvProductFields.VdvLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and VdvTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.IssueModeNm - VdvType.TypeID
		/// </summary>
		public virtual IEntityRelation VdvTypeEntityUsingIssueModeNm
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IssueModeNmType", false);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.IssueModeNm);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and VdvTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.IssueModeSe - VdvType.TypeID
		/// </summary>
		public virtual IEntityRelation VdvTypeEntityUsingIssueModeSe
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "IssueModeSeType", false);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.IssueModeSe);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvProductEntity and VdvTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvProduct.PriorityMode - VdvType.TypeID
		/// </summary>
		public virtual IEntityRelation VdvTypeEntityUsingPriorityMode
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriorityModeType", false);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.PriorityMode);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvProductRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingDistanceAttributeValueIDStatic = new VdvProductRelations().AttributeValueEntityUsingDistanceAttributeValueID;
		internal static readonly IEntityRelation RulePeriodEntityUsingPeriodIDStatic = new VdvProductRelations().RulePeriodEntityUsingPeriodID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new VdvProductRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new VdvProductRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation VdvLayoutEntityUsingVdvLayoutIDStatic = new VdvProductRelations().VdvLayoutEntityUsingVdvLayoutID;
		internal static readonly IEntityRelation VdvTypeEntityUsingIssueModeNmStatic = new VdvProductRelations().VdvTypeEntityUsingIssueModeNm;
		internal static readonly IEntityRelation VdvTypeEntityUsingIssueModeSeStatic = new VdvProductRelations().VdvTypeEntityUsingIssueModeSe;
		internal static readonly IEntityRelation VdvTypeEntityUsingPriorityModeStatic = new VdvProductRelations().VdvTypeEntityUsingPriorityMode;

		/// <summary>CTor</summary>
		static StaticVdvProductRelations()
		{
		}
	}
}
