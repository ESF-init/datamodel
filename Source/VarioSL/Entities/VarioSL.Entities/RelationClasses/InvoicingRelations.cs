﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Invoicing. </summary>
	public partial class InvoicingRelations
	{
		/// <summary>CTor</summary>
		public InvoicingRelations()
		{
		}

		/// <summary>Gets all relations of the InvoicingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.InvoiceEntityUsingInvoicingID);
			toReturn.Add(this.JobEntityUsingInvoicingID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.UserListEntityUsingUserID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InvoicingEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoicing.InvoicingID - Invoice.InvoicingID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoicingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(InvoicingFields.InvoicingID, InvoiceFields.InvoicingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InvoicingEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoicing.InvoicingID - Job.InvoicingID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingInvoicingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(InvoicingFields.InvoicingID, JobFields.InvoicingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between InvoicingEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoicing.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, InvoicingFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoicingEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoicing.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, InvoicingFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInvoicingRelations
	{
		internal static readonly IEntityRelation InvoiceEntityUsingInvoicingIDStatic = new InvoicingRelations().InvoiceEntityUsingInvoicingID;
		internal static readonly IEntityRelation JobEntityUsingInvoicingIDStatic = new InvoicingRelations().JobEntityUsingInvoicingID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new InvoicingRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new InvoicingRelations().UserListEntityUsingUserID;

		/// <summary>CTor</summary>
		static StaticInvoicingRelations()
		{
		}
	}
}
