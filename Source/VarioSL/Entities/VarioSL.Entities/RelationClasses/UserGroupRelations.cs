﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserGroup. </summary>
	public partial class UserGroupRelations
	{
		/// <summary>CTor</summary>
		public UserGroupRelations()
		{
		}

		/// <summary>Gets all relations of the UserGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserGroupRightEntityUsingUserGroupID);
			toReturn.Add(this.UserIsMemberEntityUsingUserGroupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UserGroupEntity and UserGroupRightEntity over the 1:n relation they have, using the relation between the fields:
		/// UserGroup.UserGroupID - UserGroupRight.UserGroupID
		/// </summary>
		public virtual IEntityRelation UserGroupRightEntityUsingUserGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserGroupRights" , true);
				relation.AddEntityFieldPair(UserGroupFields.UserGroupID, UserGroupRightFields.UserGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupRightEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserGroupEntity and UserIsMemberEntity over the 1:n relation they have, using the relation between the fields:
		/// UserGroup.UserGroupID - UserIsMember.UserGroupID
		/// </summary>
		public virtual IEntityRelation UserIsMemberEntityUsingUserGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserIsMembers" , true);
				relation.AddEntityFieldPair(UserGroupFields.UserGroupID, UserIsMemberFields.UserGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserIsMemberEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserGroupRelations
	{
		internal static readonly IEntityRelation UserGroupRightEntityUsingUserGroupIDStatic = new UserGroupRelations().UserGroupRightEntityUsingUserGroupID;
		internal static readonly IEntityRelation UserIsMemberEntityUsingUserGroupIDStatic = new UserGroupRelations().UserIsMemberEntityUsingUserGroupID;

		/// <summary>CTor</summary>
		static StaticUserGroupRelations()
		{
		}
	}
}
