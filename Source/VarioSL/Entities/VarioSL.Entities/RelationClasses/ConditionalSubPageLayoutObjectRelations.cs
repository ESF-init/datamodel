﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ConditionalSubPageLayoutObject. </summary>
	public partial class ConditionalSubPageLayoutObjectRelations
	{
		/// <summary>CTor</summary>
		public ConditionalSubPageLayoutObjectRelations()
		{
		}

		/// <summary>Gets all relations of the ConditionalSubPageLayoutObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LayoutEntityUsingLayoutID);
			toReturn.Add(this.LayoutEntityUsingPageID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ConditionalSubPageLayoutObjectEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// ConditionalSubPageLayoutObject.LayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Layout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ConditionalSubPageLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConditionalSubPageLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ConditionalSubPageLayoutObjectEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// ConditionalSubPageLayoutObject.PageID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingPageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Page", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ConditionalSubPageLayoutObjectFields.PageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConditionalSubPageLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticConditionalSubPageLayoutObjectRelations
	{
		internal static readonly IEntityRelation LayoutEntityUsingLayoutIDStatic = new ConditionalSubPageLayoutObjectRelations().LayoutEntityUsingLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingPageIDStatic = new ConditionalSubPageLayoutObjectRelations().LayoutEntityUsingPageID;

		/// <summary>CTor</summary>
		static StaticConditionalSubPageLayoutObjectRelations()
		{
		}
	}
}
