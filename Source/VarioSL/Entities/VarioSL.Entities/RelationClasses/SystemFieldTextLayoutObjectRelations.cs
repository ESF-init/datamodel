﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SystemFieldTextLayoutObject. </summary>
	public partial class SystemFieldTextLayoutObjectRelations
	{
		/// <summary>CTor</summary>
		public SystemFieldTextLayoutObjectRelations()
		{
		}

		/// <summary>Gets all relations of the SystemFieldTextLayoutObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LayoutEntityUsingLayoutID);
			toReturn.Add(this.SystemFieldEntityUsingSystemFieldID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SystemFieldTextLayoutObjectEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// SystemFieldTextLayoutObject.LayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Layout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, SystemFieldTextLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldTextLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SystemFieldTextLayoutObjectEntity and SystemFieldEntity over the m:1 relation they have, using the relation between the fields:
		/// SystemFieldTextLayoutObject.SystemFieldID - SystemField.SystemFieldID
		/// </summary>
		public virtual IEntityRelation SystemFieldEntityUsingSystemFieldID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SystemField", false);
				relation.AddEntityFieldPair(SystemFieldFields.SystemFieldID, SystemFieldTextLayoutObjectFields.SystemFieldID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldTextLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSystemFieldTextLayoutObjectRelations
	{
		internal static readonly IEntityRelation LayoutEntityUsingLayoutIDStatic = new SystemFieldTextLayoutObjectRelations().LayoutEntityUsingLayoutID;
		internal static readonly IEntityRelation SystemFieldEntityUsingSystemFieldIDStatic = new SystemFieldTextLayoutObjectRelations().SystemFieldEntityUsingSystemFieldID;

		/// <summary>CTor</summary>
		static StaticSystemFieldTextLayoutObjectRelations()
		{
		}
	}
}
