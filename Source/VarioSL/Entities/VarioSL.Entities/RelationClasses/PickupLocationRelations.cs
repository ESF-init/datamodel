﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PickupLocation. </summary>
	public partial class PickupLocationRelations
	{
		/// <summary>CTor</summary>
		public PickupLocationRelations()
		{
		}

		/// <summary>Gets all relations of the PickupLocationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PickupLocationToTicketEntityUsingPickupLocationID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PickupLocationEntity and PickupLocationToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// PickupLocation.PickupLocationID - PickupLocationToTicket.PickupLocationID
		/// </summary>
		public virtual IEntityRelation PickupLocationToTicketEntityUsingPickupLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PickupLocationToTickets" , true);
				relation.AddEntityFieldPair(PickupLocationFields.PickupLocationID, PickupLocationToTicketFields.PickupLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PickupLocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PickupLocationToTicketEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPickupLocationRelations
	{
		internal static readonly IEntityRelation PickupLocationToTicketEntityUsingPickupLocationIDStatic = new PickupLocationRelations().PickupLocationToTicketEntityUsingPickupLocationID;

		/// <summary>CTor</summary>
		static StaticPickupLocationRelations()
		{
		}
	}
}
