﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SamModule. </summary>
	public partial class SamModuleRelations
	{
		/// <summary>CTor</summary>
		public SamModuleRelations()
		{
		}

		/// <summary>Gets all relations of the SamModuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvKeyInfoEntityUsingSamID);
			toReturn.Add(this.VdvLoadKeyMessageEntityUsingSamID);
			toReturn.Add(this.VdvSamStatusEntityUsingSamID);
			toReturn.Add(this.DeviceEntityUsingDeviceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SamModuleEntity and VdvKeyInfoEntity over the 1:n relation they have, using the relation between the fields:
		/// SamModule.SamID - VdvKeyInfo.SamID
		/// </summary>
		public virtual IEntityRelation VdvKeyInfoEntityUsingSamID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvKeyInfos" , true);
				relation.AddEntityFieldPair(SamModuleFields.SamID, VdvKeyInfoFields.SamID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SamModuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvKeyInfoEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SamModuleEntity and VdvLoadKeyMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// SamModule.SamID - VdvLoadKeyMessage.SamID
		/// </summary>
		public virtual IEntityRelation VdvLoadKeyMessageEntityUsingSamID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvLoadKeyMessages" , true);
				relation.AddEntityFieldPair(SamModuleFields.SamID, VdvLoadKeyMessageFields.SamID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SamModuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLoadKeyMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SamModuleEntity and VdvSamStatusEntity over the 1:n relation they have, using the relation between the fields:
		/// SamModule.SamID - VdvSamStatus.SamID
		/// </summary>
		public virtual IEntityRelation VdvSamStatusEntityUsingSamID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvSamStatuses" , true);
				relation.AddEntityFieldPair(SamModuleFields.SamID, VdvSamStatusFields.SamID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SamModuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvSamStatusEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SamModuleEntity and DeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// SamModule.DeviceID - Device.DeviceID
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Device", false);
				relation.AddEntityFieldPair(DeviceFields.DeviceID, SamModuleFields.DeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SamModuleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSamModuleRelations
	{
		internal static readonly IEntityRelation VdvKeyInfoEntityUsingSamIDStatic = new SamModuleRelations().VdvKeyInfoEntityUsingSamID;
		internal static readonly IEntityRelation VdvLoadKeyMessageEntityUsingSamIDStatic = new SamModuleRelations().VdvLoadKeyMessageEntityUsingSamID;
		internal static readonly IEntityRelation VdvSamStatusEntityUsingSamIDStatic = new SamModuleRelations().VdvSamStatusEntityUsingSamID;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceIDStatic = new SamModuleRelations().DeviceEntityUsingDeviceID;

		/// <summary>CTor</summary>
		static StaticSamModuleRelations()
		{
		}
	}
}
