﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TransactionJournal. </summary>
	public partial class TransactionJournalRelations
	{
		/// <summary>CTor</summary>
		public TransactionJournalRelations()
		{
		}

		/// <summary>Gets all relations of the TransactionJournalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionEntityUsingTransactionJournalID);
			toReturn.Add(this.CappingJournalEntityUsingTransactionJournalID);
			toReturn.Add(this.PaymentJournalEntityUsingTransactionJournalID);
			toReturn.Add(this.RevenueRecognitionEntityUsingTransactionJournalID);
			toReturn.Add(this.RevenueSettlementEntityUsingTransactionJournalID);
			toReturn.Add(this.RuleViolationToTransactionEntityUsingTransactionJournalID);
			toReturn.Add(this.TransactionJournalEntityUsingCancellationReference);
			toReturn.Add(this.TransactionJournalCorrectionEntityUsingTransactionJournalID);
			toReturn.Add(this.TransactionToInspectionEntityUsingTransactionJournalID);
			toReturn.Add(this.SalesRevenueEntityUsingTransactionJournalID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DeviceClassEntityUsingSalesChannelID);
			toReturn.Add(this.TicketEntityUsingAppliedPassTicketID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.TicketEntityUsingTaxTicketID);
			toReturn.Add(this.CardEntityUsingTransitAccountID);
			toReturn.Add(this.CardEntityUsingTransferredfromcardid);
			toReturn.Add(this.CardEntityUsingTokenCardID);
			toReturn.Add(this.CardHolderEntityUsingCardHolderID);
			toReturn.Add(this.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
			toReturn.Add(this.ProductEntityUsingAppliedPassProductID);
			toReturn.Add(this.ProductEntityUsingProductID);
			toReturn.Add(this.SaleEntityUsingSaleID);
			toReturn.Add(this.TransactionJournalEntityUsingTransactionJournalIDCancellationReference);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and ApportionEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - Apportion.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation ApportionEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Apportions" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, ApportionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CappingJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - CappingJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation CappingJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CappingJournals" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, CappingJournalFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CappingJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - PaymentJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, PaymentJournalFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and RevenueRecognitionEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - RevenueRecognition.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation RevenueRecognitionEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueRecognitions" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RevenueRecognitionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and RevenueSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - RevenueSettlement.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation RevenueSettlementEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueSettlements" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RevenueSettlementFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and RuleViolationToTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - RuleViolationToTransaction.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation RuleViolationToTransactionEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolationToTransactions" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RuleViolationToTransactionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - TransactionJournal.CancellationReference
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingCancellationReference
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, TransactionJournalFields.CancellationReference);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TransactionJournalCorrectionEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - TransactionJournalCorrection.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalCorrectionEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournalCorrections" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, TransactionJournalCorrectionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalCorrectionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TransactionToInspectionEntity over the 1:n relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - TransactionToInspection.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionToInspectionEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionToInspections" , true);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, TransactionToInspectionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionToInspectionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and SalesRevenueEntity over the 1:1 relation they have, using the relation between the fields:
		/// TransactionJournal.TransactionJournalID - SalesRevenue.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation SalesRevenueEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "SalesRevenue", true);


				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, SalesRevenueFields.TransactionJournalID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesRevenueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TransactionJournalFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.SalesChannelID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingSalesChannelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TransactionJournalFields.SalesChannelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.AppliedPassTicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingAppliedPassTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AppliedPassTicket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.AppliedPassTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.TaxTicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTaxTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket_", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.TaxTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.TransitAccountID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingTransitAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.TransitAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.Transferredfromcardid - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingTransferredfromcardid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card_", false);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.Transferredfromcardid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.TokenCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingTokenCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TokenCard", false);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.TokenCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CardHolderEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.CardHolderID - CardHolder.PersonID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardHolder", false);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, TransactionJournalFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and CreditCardAuthorizationEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.CreditCardAuthorizationID - CreditCardAuthorization.CreditCardAuthorizationID
		/// </summary>
		public virtual IEntityRelation CreditCardAuthorizationEntityUsingCreditCardAuthorizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CreditCardAuthorization", false);
				relation.AddEntityFieldPair(CreditCardAuthorizationFields.CreditCardAuthorizationID, TransactionJournalFields.CreditCardAuthorizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.AppliedPassProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingAppliedPassProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AppliedPassProduct", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, TransactionJournalFields.AppliedPassProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, TransactionJournalFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and SaleEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.SaleID - Sale.SaleID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Sale", false);
				relation.AddEntityFieldPair(SaleFields.SaleID, TransactionJournalFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionJournalEntity and TransactionJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// TransactionJournal.CancellationReference - TransactionJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransactionJournalIDCancellationReference
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CancellationReferenceTransaction", false);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, TransactionJournalFields.CancellationReference);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTransactionJournalRelations
	{
		internal static readonly IEntityRelation ApportionEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().ApportionEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation CappingJournalEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().CappingJournalEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().PaymentJournalEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation RevenueRecognitionEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().RevenueRecognitionEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation RevenueSettlementEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().RevenueSettlementEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation RuleViolationToTransactionEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().RuleViolationToTransactionEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingCancellationReferenceStatic = new TransactionJournalRelations().TransactionJournalEntityUsingCancellationReference;
		internal static readonly IEntityRelation TransactionJournalCorrectionEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().TransactionJournalCorrectionEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation TransactionToInspectionEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().TransactionToInspectionEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation SalesRevenueEntityUsingTransactionJournalIDStatic = new TransactionJournalRelations().SalesRevenueEntityUsingTransactionJournalID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new TransactionJournalRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DeviceClassEntityUsingSalesChannelIDStatic = new TransactionJournalRelations().DeviceClassEntityUsingSalesChannelID;
		internal static readonly IEntityRelation TicketEntityUsingAppliedPassTicketIDStatic = new TransactionJournalRelations().TicketEntityUsingAppliedPassTicketID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TransactionJournalRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketEntityUsingTaxTicketIDStatic = new TransactionJournalRelations().TicketEntityUsingTaxTicketID;
		internal static readonly IEntityRelation CardEntityUsingTransitAccountIDStatic = new TransactionJournalRelations().CardEntityUsingTransitAccountID;
		internal static readonly IEntityRelation CardEntityUsingTransferredfromcardidStatic = new TransactionJournalRelations().CardEntityUsingTransferredfromcardid;
		internal static readonly IEntityRelation CardEntityUsingTokenCardIDStatic = new TransactionJournalRelations().CardEntityUsingTokenCardID;
		internal static readonly IEntityRelation CardHolderEntityUsingCardHolderIDStatic = new TransactionJournalRelations().CardHolderEntityUsingCardHolderID;
		internal static readonly IEntityRelation CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic = new TransactionJournalRelations().CreditCardAuthorizationEntityUsingCreditCardAuthorizationID;
		internal static readonly IEntityRelation ProductEntityUsingAppliedPassProductIDStatic = new TransactionJournalRelations().ProductEntityUsingAppliedPassProductID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new TransactionJournalRelations().ProductEntityUsingProductID;
		internal static readonly IEntityRelation SaleEntityUsingSaleIDStatic = new TransactionJournalRelations().SaleEntityUsingSaleID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransactionJournalIDCancellationReferenceStatic = new TransactionJournalRelations().TransactionJournalEntityUsingTransactionJournalIDCancellationReference;

		/// <summary>CTor</summary>
		static StaticTransactionJournalRelations()
		{
		}
	}
}
