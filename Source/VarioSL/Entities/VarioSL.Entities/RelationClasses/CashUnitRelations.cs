﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CashUnit. </summary>
	public partial class CashUnitRelations
	{
		/// <summary>CTor</summary>
		public CashUnitRelations()
		{
		}

		/// <summary>Gets all relations of the CashUnitEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceDataEntityUsingCashUnitID);
			toReturn.Add(this.ComponentFillingEntityUsingCashUnitID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CashUnitEntity and CashServiceDataEntity over the 1:n relation they have, using the relation between the fields:
		/// CashUnit.CashUnitID - CashServiceData.CashUnitID
		/// </summary>
		public virtual IEntityRelation CashServiceDataEntityUsingCashUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AmCashservicedatas" , true);
				relation.AddEntityFieldPair(CashUnitFields.CashUnitID, CashServiceDataFields.CashUnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashUnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceDataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CashUnitEntity and ComponentFillingEntity over the 1:n relation they have, using the relation between the fields:
		/// CashUnit.CashUnitID - ComponentFilling.CashUnitID
		/// </summary>
		public virtual IEntityRelation ComponentFillingEntityUsingCashUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentFillings" , true);
				relation.AddEntityFieldPair(CashUnitFields.CashUnitID, ComponentFillingFields.CashUnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashUnitEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCashUnitRelations
	{
		internal static readonly IEntityRelation CashServiceDataEntityUsingCashUnitIDStatic = new CashUnitRelations().CashServiceDataEntityUsingCashUnitID;
		internal static readonly IEntityRelation ComponentFillingEntityUsingCashUnitIDStatic = new CashUnitRelations().ComponentFillingEntityUsingCashUnitID;

		/// <summary>CTor</summary>
		static StaticCashUnitRelations()
		{
		}
	}
}
