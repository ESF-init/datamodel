﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Line. </summary>
	public partial class LineRelations
	{
		/// <summary>CTor</summary>
		public LineRelations()
		{
		}

		/// <summary>Gets all relations of the LineEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalPacketEffortEntityUsingLineID);
			toReturn.Add(this.TicketServicesPermittedEntityUsingLineID);
			toReturn.Add(this.NetEntityUsingNetID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LineEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// Line.LineID - ExternalPacketEffort.LineID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingLineID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketEfforts" , true);
				relation.AddEntityFieldPair(LineFields.LineID, ExternalPacketEffortFields.LineID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LineEntity and TicketServicesPermittedEntity over the 1:n relation they have, using the relation between the fields:
		/// Line.LineID - TicketServicesPermitted.LineID
		/// </summary>
		public virtual IEntityRelation TicketServicesPermittedEntityUsingLineID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketServicesPermitted" , true);
				relation.AddEntityFieldPair(LineFields.LineID, TicketServicesPermittedFields.LineID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketServicesPermittedEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LineEntity and NetEntity over the m:1 relation they have, using the relation between the fields:
		/// Line.NetID - Net.NetID
		/// </summary>
		public virtual IEntityRelation NetEntityUsingNetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Net", false);
				relation.AddEntityFieldPair(NetFields.NetID, LineFields.NetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLineRelations
	{
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingLineIDStatic = new LineRelations().ExternalPacketEffortEntityUsingLineID;
		internal static readonly IEntityRelation TicketServicesPermittedEntityUsingLineIDStatic = new LineRelations().TicketServicesPermittedEntityUsingLineID;
		internal static readonly IEntityRelation NetEntityUsingNetIDStatic = new LineRelations().NetEntityUsingNetID;

		/// <summary>CTor</summary>
		static StaticLineRelations()
		{
		}
	}
}
