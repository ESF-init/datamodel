﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderDetail. </summary>
	public partial class OrderDetailRelations
	{
		/// <summary>CTor</summary>
		public OrderDetailRelations()
		{
		}

		/// <summary>Gets all relations of the OrderDetailEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.JobEntityUsingOrderDetailID);
			toReturn.Add(this.OrderDetailEntityUsingRequiredOrderDetailID);
			toReturn.Add(this.OrderDetailToCardHolderEntityUsingOrderDetailID);
			toReturn.Add(this.CardEntityUsingReplacementCardID);
			toReturn.Add(this.OrderEntityUsingOrderID);
			toReturn.Add(this.OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailID);
			toReturn.Add(this.PaymentEntityUsingPaymentID);
			toReturn.Add(this.ProductEntityUsingProductID);
			toReturn.Add(this.SchoolYearEntityUsingSchoolyearID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderDetail.OrderDetailID - Job.OrderDetailID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, JobFields.OrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and OrderDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderDetail.OrderDetailID - OrderDetail.RequiredOrderDetailID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingRequiredOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SubsequentOrderDetails" , true);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, OrderDetailFields.RequiredOrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and OrderDetailToCardHolderEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderDetail.OrderDetailID - OrderDetailToCardHolder.OrderDetailID
		/// </summary>
		public virtual IEntityRelation OrderDetailToCardHolderEntityUsingOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetailToCardHolder" , true);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, OrderDetailToCardHolderFields.OrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardHolderEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.ReplacementCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingReplacementCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, OrderDetailFields.ReplacementCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and OrderEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.OrderID - Order.OrderID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Order", false);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderDetailFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and OrderDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.RequiredOrderDetailID - OrderDetail.OrderDetailID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RequiredOrderDetail", false);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, OrderDetailFields.RequiredOrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and PaymentEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.PaymentID - Payment.PaymentID
		/// </summary>
		public virtual IEntityRelation PaymentEntityUsingPaymentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Payment", false);
				relation.AddEntityFieldPair(PaymentFields.PaymentID, OrderDetailFields.PaymentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, OrderDetailFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailEntity and SchoolYearEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetail.SchoolyearID - SchoolYear.SchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingSchoolyearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SchoolYear", false);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, OrderDetailFields.SchoolyearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderDetailRelations
	{
		internal static readonly IEntityRelation JobEntityUsingOrderDetailIDStatic = new OrderDetailRelations().JobEntityUsingOrderDetailID;
		internal static readonly IEntityRelation OrderDetailEntityUsingRequiredOrderDetailIDStatic = new OrderDetailRelations().OrderDetailEntityUsingRequiredOrderDetailID;
		internal static readonly IEntityRelation OrderDetailToCardHolderEntityUsingOrderDetailIDStatic = new OrderDetailRelations().OrderDetailToCardHolderEntityUsingOrderDetailID;
		internal static readonly IEntityRelation CardEntityUsingReplacementCardIDStatic = new OrderDetailRelations().CardEntityUsingReplacementCardID;
		internal static readonly IEntityRelation OrderEntityUsingOrderIDStatic = new OrderDetailRelations().OrderEntityUsingOrderID;
		internal static readonly IEntityRelation OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailIDStatic = new OrderDetailRelations().OrderDetailEntityUsingOrderDetailIDRequiredOrderDetailID;
		internal static readonly IEntityRelation PaymentEntityUsingPaymentIDStatic = new OrderDetailRelations().PaymentEntityUsingPaymentID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new OrderDetailRelations().ProductEntityUsingProductID;
		internal static readonly IEntityRelation SchoolYearEntityUsingSchoolyearIDStatic = new OrderDetailRelations().SchoolYearEntityUsingSchoolyearID;

		/// <summary>CTor</summary>
		static StaticOrderDetailRelations()
		{
		}
	}
}
