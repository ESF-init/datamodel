﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Form. </summary>
	public partial class FormRelations
	{
		/// <summary>CTor</summary>
		public FormRelations()
		{
		}

		/// <summary>Gets all relations of the FormEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardTicketEntityUsingFormID);
			toReturn.Add(this.LayoutEntityUsingFormid);
			toReturn.Add(this.DunningLevelConfigurationEntityUsingFormID);
			toReturn.Add(this.EmailMessageEntityUsingFormID);
			toReturn.Add(this.NotificationMessageEntityUsingFormID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FormEntity and CardTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Form.FormID - CardTicket.FormID
		/// </summary>
		public virtual IEntityRelation CardTicketEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardTicket" , true);
				relation.AddEntityFieldPair(FormFields.FormID, CardTicketFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FormEntity and LayoutEntity over the 1:n relation they have, using the relation between the fields:
		/// Form.FormID - Layout.Formid
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingFormid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Layouts" , true);
				relation.AddEntityFieldPair(FormFields.FormID, LayoutFields.Formid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FormEntity and DunningLevelConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// Form.FormID - DunningLevelConfiguration.FormID
		/// </summary>
		public virtual IEntityRelation DunningLevelConfigurationEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningLevels" , true);
				relation.AddEntityFieldPair(FormFields.FormID, DunningLevelConfigurationFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FormEntity and EmailMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Form.FormID - EmailMessage.FormID
		/// </summary>
		public virtual IEntityRelation EmailMessageEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailMessages" , true);
				relation.AddEntityFieldPair(FormFields.FormID, EmailMessageFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FormEntity and NotificationMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Form.FormID - NotificationMessage.FormID
		/// </summary>
		public virtual IEntityRelation NotificationMessageEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessages" , true);
				relation.AddEntityFieldPair(FormFields.FormID, NotificationMessageFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FormEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Form.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FormFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFormRelations
	{
		internal static readonly IEntityRelation CardTicketEntityUsingFormIDStatic = new FormRelations().CardTicketEntityUsingFormID;
		internal static readonly IEntityRelation LayoutEntityUsingFormidStatic = new FormRelations().LayoutEntityUsingFormid;
		internal static readonly IEntityRelation DunningLevelConfigurationEntityUsingFormIDStatic = new FormRelations().DunningLevelConfigurationEntityUsingFormID;
		internal static readonly IEntityRelation EmailMessageEntityUsingFormIDStatic = new FormRelations().EmailMessageEntityUsingFormID;
		internal static readonly IEntityRelation NotificationMessageEntityUsingFormIDStatic = new FormRelations().NotificationMessageEntityUsingFormID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new FormRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticFormRelations()
		{
		}
	}
}
