﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Protocol. </summary>
	public partial class ProtocolRelations
	{
		/// <summary>CTor</summary>
		public ProtocolRelations()
		{
		}

		/// <summary>Gets all relations of the ProtocolEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProtocolMessageEntityUsingProtocolID);
			toReturn.Add(this.ProtocolFunctionEntityUsingFunctionID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProtocolEntity and ProtocolMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// Protocol.ProtocolID - ProtocolMessage.ProtocolID
		/// </summary>
		public virtual IEntityRelation ProtocolMessageEntityUsingProtocolID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProtMessages" , true);
				relation.AddEntityFieldPair(ProtocolFields.ProtocolID, ProtocolMessageFields.ProtocolID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolMessageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ProtocolEntity and ProtocolFunctionEntity over the m:1 relation they have, using the relation between the fields:
		/// Protocol.FunctionID - ProtocolFunction.ProtocolFunctionID
		/// </summary>
		public virtual IEntityRelation ProtocolFunctionEntityUsingFunctionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProtocolFunction", false);
				relation.AddEntityFieldPair(ProtocolFunctionFields.ProtocolFunctionID, ProtocolFields.FunctionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolFunctionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProtocolRelations
	{
		internal static readonly IEntityRelation ProtocolMessageEntityUsingProtocolIDStatic = new ProtocolRelations().ProtocolMessageEntityUsingProtocolID;
		internal static readonly IEntityRelation ProtocolFunctionEntityUsingFunctionIDStatic = new ProtocolRelations().ProtocolFunctionEntityUsingFunctionID;

		/// <summary>CTor</summary>
		static StaticProtocolRelations()
		{
		}
	}
}
