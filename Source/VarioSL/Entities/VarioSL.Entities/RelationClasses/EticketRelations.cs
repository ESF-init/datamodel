﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Eticket. </summary>
	public partial class EticketRelations
	{
		/// <summary>CTor</summary>
		public EticketRelations()
		{
		}

		/// <summary>Gets all relations of the EticketEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketEntityUsingEtickID);
			toReturn.Add(this.TicketDeviceClassEntityUsingEtickID);
			toReturn.Add(this.LayoutEntityUsingEtickLayoutID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EticketEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Eticket.EtickID - Ticket.EtickID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingEtickID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(EticketFields.EtickID, TicketFields.EtickID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EticketEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Eticket.EtickID - TicketDeviceClass.EtickID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingEtickID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClass" , true);
				relation.AddEntityFieldPair(EticketFields.EtickID, TicketDeviceClassFields.EtickID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between EticketEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// Eticket.EtickLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingEtickLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Layout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, EticketFields.EtickLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EticketEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Eticket.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, EticketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEticketRelations
	{
		internal static readonly IEntityRelation TicketEntityUsingEtickIDStatic = new EticketRelations().TicketEntityUsingEtickID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingEtickIDStatic = new EticketRelations().TicketDeviceClassEntityUsingEtickID;
		internal static readonly IEntityRelation LayoutEntityUsingEtickLayoutIDStatic = new EticketRelations().LayoutEntityUsingEtickLayoutID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new EticketRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticEticketRelations()
		{
		}
	}
}
