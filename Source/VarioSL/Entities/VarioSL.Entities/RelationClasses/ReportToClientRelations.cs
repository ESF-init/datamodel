﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportToClient. </summary>
	public partial class ReportToClientRelations
	{
		/// <summary>CTor</summary>
		public ReportToClientRelations()
		{
		}

		/// <summary>Gets all relations of the ReportToClientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ConfigurationEntityUsingConfigurationID);
			toReturn.Add(this.ConfigurationDefinitionEntityUsingConfigurationID);
			toReturn.Add(this.ReportEntityUsingReportID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ReportToClientEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportToClient.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportToClientFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportToClientEntity and ConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportToClient.ConfigurationID - Configuration.ConfigurationID
		/// </summary>
		public virtual IEntityRelation ConfigurationEntityUsingConfigurationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Configuration", false);
				relation.AddEntityFieldPair(ConfigurationFields.ConfigurationID, ReportToClientFields.ConfigurationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportToClientEntity and ConfigurationDefinitionEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportToClient.ConfigurationID - ConfigurationDefinition.ConfigurationDefinitionID
		/// </summary>
		public virtual IEntityRelation ConfigurationDefinitionEntityUsingConfigurationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ConfigurationDefinition", false);
				relation.AddEntityFieldPair(ConfigurationDefinitionFields.ConfigurationDefinitionID, ReportToClientFields.ConfigurationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportToClientEntity and ReportEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportToClient.ReportID - Report.ReportID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Report", false);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportToClientFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportToClientRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ReportToClientRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ConfigurationEntityUsingConfigurationIDStatic = new ReportToClientRelations().ConfigurationEntityUsingConfigurationID;
		internal static readonly IEntityRelation ConfigurationDefinitionEntityUsingConfigurationIDStatic = new ReportToClientRelations().ConfigurationDefinitionEntityUsingConfigurationID;
		internal static readonly IEntityRelation ReportEntityUsingReportIDStatic = new ReportToClientRelations().ReportEntityUsingReportID;

		/// <summary>CTor</summary>
		static StaticReportToClientRelations()
		{
		}
	}
}
