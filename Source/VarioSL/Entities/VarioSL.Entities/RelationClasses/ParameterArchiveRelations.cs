﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterArchive. </summary>
	public partial class ParameterArchiveRelations
	{
		/// <summary>CTor</summary>
		public ParameterArchiveRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterArchiveEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterArchiveReleaseEntityUsingParameterArchiveID);
			toReturn.Add(this.ParameterValueEntityUsingParameterArchiveID);
			toReturn.Add(this.UnitCollectionEntityUsingParameterArchiveID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveEntity and ParameterArchiveReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// ParameterArchive.ParameterArchiveID - ParameterArchiveRelease.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveReleaseEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterArchiveReleases" , true);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, ParameterArchiveReleaseFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveEntity and ParameterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// ParameterArchive.ParameterArchiveID - ParameterValue.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation ParameterValueEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterValues" , true);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, ParameterValueFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveEntity and UnitCollectionEntity over the 1:n relation they have, using the relation between the fields:
		/// ParameterArchive.ParameterArchiveID - UnitCollection.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation UnitCollectionEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UnitCollections" , true);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, UnitCollectionFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterArchive.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ParameterArchiveFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterArchiveRelations
	{
		internal static readonly IEntityRelation ParameterArchiveReleaseEntityUsingParameterArchiveIDStatic = new ParameterArchiveRelations().ParameterArchiveReleaseEntityUsingParameterArchiveID;
		internal static readonly IEntityRelation ParameterValueEntityUsingParameterArchiveIDStatic = new ParameterArchiveRelations().ParameterValueEntityUsingParameterArchiveID;
		internal static readonly IEntityRelation UnitCollectionEntityUsingParameterArchiveIDStatic = new ParameterArchiveRelations().UnitCollectionEntityUsingParameterArchiveID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ParameterArchiveRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticParameterArchiveRelations()
		{
		}
	}
}
