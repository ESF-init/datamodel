﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrganizationParticipant. </summary>
	public partial class OrganizationParticipantRelations
	{
		/// <summary>CTor</summary>
		public OrganizationParticipantRelations()
		{
		}

		/// <summary>Gets all relations of the OrganizationParticipantEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.ParticipantGroupEntityUsingParticipantGroupID);
			toReturn.Add(this.PhotographEntityUsingPhotographID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OrganizationParticipantEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// OrganizationParticipant.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, OrganizationParticipantFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrganizationParticipantEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// OrganizationParticipant.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, OrganizationParticipantFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrganizationParticipantEntity and ParticipantGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// OrganizationParticipant.ParticipantGroupID - ParticipantGroup.ParticipantGroupID
		/// </summary>
		public virtual IEntityRelation ParticipantGroupEntityUsingParticipantGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParticipantGroup", false);
				relation.AddEntityFieldPair(ParticipantGroupFields.ParticipantGroupID, OrganizationParticipantFields.ParticipantGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParticipantGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrganizationParticipantEntity and PhotographEntity over the m:1 relation they have, using the relation between the fields:
		/// OrganizationParticipant.PhotographID - Photograph.PhotographID
		/// </summary>
		public virtual IEntityRelation PhotographEntityUsingPhotographID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Photograph", false);
				relation.AddEntityFieldPair(PhotographFields.PhotographID, OrganizationParticipantFields.PhotographID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrganizationParticipantRelations
	{
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new OrganizationParticipantRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new OrganizationParticipantRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation ParticipantGroupEntityUsingParticipantGroupIDStatic = new OrganizationParticipantRelations().ParticipantGroupEntityUsingParticipantGroupID;
		internal static readonly IEntityRelation PhotographEntityUsingPhotographIDStatic = new OrganizationParticipantRelations().PhotographEntityUsingPhotographID;

		/// <summary>CTor</summary>
		static StaticOrganizationParticipantRelations()
		{
		}
	}
}
