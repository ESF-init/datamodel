﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RuleViolation. </summary>
	public partial class RuleViolationRelations
	{
		/// <summary>CTor</summary>
		public RuleViolationRelations()
		{
		}

		/// <summary>Gets all relations of the RuleViolationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RuleViolationToTransactionEntityUsingRuleViolationID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RuleViolationEntity and RuleViolationToTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleViolation.RuleViolationID - RuleViolationToTransaction.RuleViolationID
		/// </summary>
		public virtual IEntityRelation RuleViolationToTransactionEntityUsingRuleViolationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolationToTransactions" , true);
				relation.AddEntityFieldPair(RuleViolationFields.RuleViolationID, RuleViolationToTransactionFields.RuleViolationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RuleViolationEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolation.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, RuleViolationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleViolationEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolation.PaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, RuleViolationFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleViolationEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolation.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, RuleViolationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRuleViolationRelations
	{
		internal static readonly IEntityRelation RuleViolationToTransactionEntityUsingRuleViolationIDStatic = new RuleViolationRelations().RuleViolationToTransactionEntityUsingRuleViolationID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new RuleViolationRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new RuleViolationRelations().PaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new RuleViolationRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticRuleViolationRelations()
		{
		}
	}
}
