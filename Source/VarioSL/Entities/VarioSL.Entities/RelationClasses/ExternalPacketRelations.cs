﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalPacket. </summary>
	public partial class ExternalPacketRelations
	{
		/// <summary>CTor</summary>
		public ExternalPacketRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalPacketEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalCardEntityUsingPacketID);
			toReturn.Add(this.ExternalPacketEffortEntityUsingPacketID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEntity and ExternalCardEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalPacket.PacketID - ExternalCard.PacketID
		/// </summary>
		public virtual IEntityRelation ExternalCardEntityUsingPacketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalCards" , true);
				relation.AddEntityFieldPair(ExternalPacketFields.PacketID, ExternalCardFields.PacketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalPacket.PacketID - ExternalPacketEffort.PacketID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingPacketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketEfforts" , true);
				relation.AddEntityFieldPair(ExternalPacketFields.PacketID, ExternalPacketEffortFields.PacketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacket.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalPacketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalPacketRelations
	{
		internal static readonly IEntityRelation ExternalCardEntityUsingPacketIDStatic = new ExternalPacketRelations().ExternalCardEntityUsingPacketID;
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingPacketIDStatic = new ExternalPacketRelations().ExternalPacketEffortEntityUsingPacketID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ExternalPacketRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticExternalPacketRelations()
		{
		}
	}
}
