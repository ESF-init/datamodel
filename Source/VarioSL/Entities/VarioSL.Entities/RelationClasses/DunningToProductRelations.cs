﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningToProduct. </summary>
	public partial class DunningToProductRelations
	{
		/// <summary>CTor</summary>
		public DunningToProductRelations()
		{
		}

		/// <summary>Gets all relations of the DunningToProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningProcessEntityUsingDunningProcessID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DunningToProductEntity and DunningProcessEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToProduct.DunningProcessID - DunningProcess.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningProcessEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DunningProcess", false);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToProductFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningToProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToProduct.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, DunningToProductFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningToProductRelations
	{
		internal static readonly IEntityRelation DunningProcessEntityUsingDunningProcessIDStatic = new DunningToProductRelations().DunningProcessEntityUsingDunningProcessID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new DunningToProductRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticDunningToProductRelations()
		{
		}
	}
}
