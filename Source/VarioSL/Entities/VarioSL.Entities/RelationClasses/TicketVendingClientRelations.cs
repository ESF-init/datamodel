﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketVendingClient. </summary>
	public partial class TicketVendingClientRelations
	{
		/// <summary>CTor</summary>
		public TicketVendingClientRelations()
		{
		}

		/// <summary>Gets all relations of the TicketVendingClientEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientid);
			toReturn.Add(this.TicketEntityUsingTicketid);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketVendingClientEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketVendingClient.Clientid - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketVendingClientFields.Clientid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketVendingClientEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketVendingClientEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketVendingClient.Ticketid - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketVendingClientFields.Ticketid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketVendingClientEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketVendingClientRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientidStatic = new TicketVendingClientRelations().ClientEntityUsingClientid;
		internal static readonly IEntityRelation TicketEntityUsingTicketidStatic = new TicketVendingClientRelations().TicketEntityUsingTicketid;

		/// <summary>CTor</summary>
		static StaticTicketVendingClientRelations()
		{
		}
	}
}
