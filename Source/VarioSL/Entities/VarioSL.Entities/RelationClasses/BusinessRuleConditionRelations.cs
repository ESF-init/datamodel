﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRuleCondition. </summary>
	public partial class BusinessRuleConditionRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleConditionRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleConditionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleClauseEntityUsingBusinessRuleConditionID);
			toReturn.Add(this.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleConditionEntity and BusinessRuleClauseEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleCondition.BusinessRuleConditionID - BusinessRuleClause.BusinessRuleConditionID
		/// </summary>
		public virtual IEntityRelation BusinessRuleClauseEntityUsingBusinessRuleConditionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleClause" , true);
				relation.AddEntityFieldPair(BusinessRuleConditionFields.BusinessRuleConditionID, BusinessRuleClauseFields.BusinessRuleConditionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between BusinessRuleConditionEntity and BusinessRuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleCondition.BusinessRuleTypeID - BusinessRuleType.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleType", false);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleConditionFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleConditionEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleCondition.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleConditionFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleConditionRelations
	{
		internal static readonly IEntityRelation BusinessRuleClauseEntityUsingBusinessRuleConditionIDStatic = new BusinessRuleConditionRelations().BusinessRuleClauseEntityUsingBusinessRuleConditionID;
		internal static readonly IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleConditionRelations().BusinessRuleTypeEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new BusinessRuleConditionRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleConditionRelations()
		{
		}
	}
}
