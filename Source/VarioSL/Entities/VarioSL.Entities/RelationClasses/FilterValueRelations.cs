﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FilterValue. </summary>
	public partial class FilterValueRelations
	{
		/// <summary>CTor</summary>
		public FilterValueRelations()
		{
		}

		/// <summary>Gets all relations of the FilterValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FilterListElementEntityUsingFilterListElementID);
			toReturn.Add(this.FilterValueSetEntityUsingFilterValueSetID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FilterValueEntity and FilterListElementEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValue.FilterListElementID - FilterListElement.FilterListElementID
		/// </summary>
		public virtual IEntityRelation FilterListElementEntityUsingFilterListElementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterListElement", false);
				relation.AddEntityFieldPair(FilterListElementFields.FilterListElementID, FilterValueFields.FilterListElementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterValueEntity and FilterValueSetEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterValue.FilterValueSetID - FilterValueSet.FilterValueSetID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingFilterValueSetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterValueSet", false);
				relation.AddEntityFieldPair(FilterValueSetFields.FilterValueSetID, FilterValueFields.FilterValueSetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFilterValueRelations
	{
		internal static readonly IEntityRelation FilterListElementEntityUsingFilterListElementIDStatic = new FilterValueRelations().FilterListElementEntityUsingFilterListElementID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingFilterValueSetIDStatic = new FilterValueRelations().FilterValueSetEntityUsingFilterValueSetID;

		/// <summary>CTor</summary>
		static StaticFilterValueRelations()
		{
		}
	}
}
