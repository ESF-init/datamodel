﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Calendar. </summary>
	public partial class CalendarRelations
	{
		/// <summary>CTor</summary>
		public CalendarRelations()
		{
		}

		/// <summary>Gets all relations of the CalendarEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CalendarEntryEntityUsingCalendarID);
			toReturn.Add(this.RuleCappingEntityUsingCalendarID);
			toReturn.Add(this.RulePeriodEntityUsingCalendarID);
			toReturn.Add(this.RulePeriodEntityUsingExtCalendarID);
			toReturn.Add(this.TicketEntityUsingCalendarID);
			toReturn.Add(this.TicketDeviceClassEntityUsingCalendarID);
			toReturn.Add(this.ClientEntityUsingOwnerClientId);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and CalendarEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - CalendarEntry.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntryEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CalendarEntries" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, CalendarEntryFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and RuleCappingEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - RuleCapping.CalendarID
		/// </summary>
		public virtual IEntityRelation RuleCappingEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCapping" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RuleCappingFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - RulePeriod.CalendarID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriod" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RulePeriodFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - RulePeriod.ExtCalendarID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingExtCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriodByExtensionCalendar" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RulePeriodFields.ExtCalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - Ticket.CalendarID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, TicketFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Calendar.CalendarID - TicketDeviceClass.CalendarID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClass" , true);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, TicketDeviceClassFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Calendar.OwnerClientId - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, CalendarFields.OwnerClientId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CalendarEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Calendar.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, CalendarFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCalendarRelations
	{
		internal static readonly IEntityRelation CalendarEntryEntityUsingCalendarIDStatic = new CalendarRelations().CalendarEntryEntityUsingCalendarID;
		internal static readonly IEntityRelation RuleCappingEntityUsingCalendarIDStatic = new CalendarRelations().RuleCappingEntityUsingCalendarID;
		internal static readonly IEntityRelation RulePeriodEntityUsingCalendarIDStatic = new CalendarRelations().RulePeriodEntityUsingCalendarID;
		internal static readonly IEntityRelation RulePeriodEntityUsingExtCalendarIDStatic = new CalendarRelations().RulePeriodEntityUsingExtCalendarID;
		internal static readonly IEntityRelation TicketEntityUsingCalendarIDStatic = new CalendarRelations().TicketEntityUsingCalendarID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingCalendarIDStatic = new CalendarRelations().TicketDeviceClassEntityUsingCalendarID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIdStatic = new CalendarRelations().ClientEntityUsingOwnerClientId;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new CalendarRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticCalendarRelations()
		{
		}
	}
}
