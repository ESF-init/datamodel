﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserGroupRight. </summary>
	public partial class UserGroupRightRelations
	{
		/// <summary>CTor</summary>
		public UserGroupRightRelations()
		{
		}

		/// <summary>Gets all relations of the UserGroupRightEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserGroupEntityUsingUserGroupID);
			toReturn.Add(this.UserResourceEntityUsingResourceID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UserGroupRightEntity and UserGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// UserGroupRight.UserGroupID - UserGroup.UserGroupID
		/// </summary>
		public virtual IEntityRelation UserGroupEntityUsingUserGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserGroup", false);
				relation.AddEntityFieldPair(UserGroupFields.UserGroupID, UserGroupRightFields.UserGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupRightEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserGroupRightEntity and UserResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// UserGroupRight.ResourceID - UserResource.ResourceID
		/// </summary>
		public virtual IEntityRelation UserResourceEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserResource", false);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, UserGroupRightFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupRightEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserGroupRightRelations
	{
		internal static readonly IEntityRelation UserGroupEntityUsingUserGroupIDStatic = new UserGroupRightRelations().UserGroupEntityUsingUserGroupID;
		internal static readonly IEntityRelation UserResourceEntityUsingResourceIDStatic = new UserGroupRightRelations().UserResourceEntityUsingResourceID;

		/// <summary>CTor</summary>
		static StaticUserGroupRightRelations()
		{
		}
	}
}
