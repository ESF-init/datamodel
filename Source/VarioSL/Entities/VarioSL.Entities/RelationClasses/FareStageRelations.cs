﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareStage. </summary>
	public partial class FareStageRelations
	{
		/// <summary>CTor</summary>
		public FareStageRelations()
		{
		}

		/// <summary>Gets all relations of the FareStageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListElementEntityUsingFareStageID);
			toReturn.Add(this.BusinessRuleResultEntityUsingDestinationFarestageID);
			toReturn.Add(this.ExternalPacketEffortEntityUsingFareStageID);
			toReturn.Add(this.FareMatrixEntryEntityUsingBoardingID);
			toReturn.Add(this.FareMatrixEntryEntityUsingDestinationID);
			toReturn.Add(this.FareMatrixEntryEntityUsingViaID);
			toReturn.Add(this.FareStageEntityUsingParentID);
			toReturn.Add(this.FareStageAliasEntityUsingFareStageID);
			toReturn.Add(this.ParameterFareStageEntityUsingFareStageID);
			toReturn.Add(this.FareStageEntityUsingFareStageIDParentID);
			toReturn.Add(this.FareStageHierarchieLevelEntityUsingHierarchieLevel);
			toReturn.Add(this.FareStageListEntityUsingFareStageListID);
			toReturn.Add(this.FareStageTypeEntityUsingFareStageTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and AreaListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - AreaListElement.FareStageID
		/// </summary>
		public virtual IEntityRelation AreaListElementEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaListElement" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, AreaListElementFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and BusinessRuleResultEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - BusinessRuleResult.DestinationFarestageID
		/// </summary>
		public virtual IEntityRelation BusinessRuleResultEntityUsingDestinationFarestageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleResult" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, BusinessRuleResultFields.DestinationFarestageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - ExternalPacketEffort.FareStageID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketEfforts" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, ExternalPacketEffortFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - FareMatrixEntry.BoardingID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingBoardingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntryBoarding" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.BoardingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - FareMatrixEntry.DestinationID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingDestinationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntryDestination" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.DestinationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - FareMatrixEntry.ViaID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingViaID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntryVia" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.ViaID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - FareStage.ParentID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingParentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildFareStages" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareStageFields.ParentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageAliasEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - FareStageAlias.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageAliasEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareStageAlias" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareStageAliasFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageAliasEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and ParameterFareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStage.FareStageID - ParameterFareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation ParameterFareStageEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterFareStage" , true);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, ParameterFareStageFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStage.ParentID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageIDParentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentFareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareStageFields.ParentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageHierarchieLevelEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStage.HierarchieLevel - FareStageHierarchieLevel.LevelID
		/// </summary>
		public virtual IEntityRelation FareStageHierarchieLevelEntityUsingHierarchieLevel
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageHierarchieLevel", false);
				relation.AddEntityFieldPair(FareStageHierarchieLevelFields.LevelID, FareStageFields.HierarchieLevel);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageHierarchieLevelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageListEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStage.FareStageListID - FareStageList.FareStageListID
		/// </summary>
		public virtual IEntityRelation FareStageListEntityUsingFareStageListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageList", false);
				relation.AddEntityFieldPair(FareStageListFields.FareStageListID, FareStageFields.FareStageListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareStageEntity and FareStageTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStage.FareStageTypeID - FareStageType.FarestageTypeID
		/// </summary>
		public virtual IEntityRelation FareStageTypeEntityUsingFareStageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageType", false);
				relation.AddEntityFieldPair(FareStageTypeFields.FarestageTypeID, FareStageFields.FareStageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareStageRelations
	{
		internal static readonly IEntityRelation AreaListElementEntityUsingFareStageIDStatic = new FareStageRelations().AreaListElementEntityUsingFareStageID;
		internal static readonly IEntityRelation BusinessRuleResultEntityUsingDestinationFarestageIDStatic = new FareStageRelations().BusinessRuleResultEntityUsingDestinationFarestageID;
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingFareStageIDStatic = new FareStageRelations().ExternalPacketEffortEntityUsingFareStageID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingBoardingIDStatic = new FareStageRelations().FareMatrixEntryEntityUsingBoardingID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingDestinationIDStatic = new FareStageRelations().FareMatrixEntryEntityUsingDestinationID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingViaIDStatic = new FareStageRelations().FareMatrixEntryEntityUsingViaID;
		internal static readonly IEntityRelation FareStageEntityUsingParentIDStatic = new FareStageRelations().FareStageEntityUsingParentID;
		internal static readonly IEntityRelation FareStageAliasEntityUsingFareStageIDStatic = new FareStageRelations().FareStageAliasEntityUsingFareStageID;
		internal static readonly IEntityRelation ParameterFareStageEntityUsingFareStageIDStatic = new FareStageRelations().ParameterFareStageEntityUsingFareStageID;
		internal static readonly IEntityRelation FareStageEntityUsingFareStageIDParentIDStatic = new FareStageRelations().FareStageEntityUsingFareStageIDParentID;
		internal static readonly IEntityRelation FareStageHierarchieLevelEntityUsingHierarchieLevelStatic = new FareStageRelations().FareStageHierarchieLevelEntityUsingHierarchieLevel;
		internal static readonly IEntityRelation FareStageListEntityUsingFareStageListIDStatic = new FareStageRelations().FareStageListEntityUsingFareStageListID;
		internal static readonly IEntityRelation FareStageTypeEntityUsingFareStageTypeIDStatic = new FareStageRelations().FareStageTypeEntityUsingFareStageTypeID;

		/// <summary>CTor</summary>
		static StaticFareStageRelations()
		{
		}
	}
}
