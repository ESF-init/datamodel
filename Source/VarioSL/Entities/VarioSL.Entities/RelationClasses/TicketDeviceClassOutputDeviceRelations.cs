﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketDeviceClassOutputDevice. </summary>
	public partial class TicketDeviceClassOutputDeviceRelations
	{
		/// <summary>CTor</summary>
		public TicketDeviceClassOutputDeviceRelations()
		{
		}

		/// <summary>Gets all relations of the TicketDeviceClassOutputDeviceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OutputDeviceEntityUsingOutputDeviceID);
			toReturn.Add(this.TicketDeviceClassEntityUsingTicketDeviceClassID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassOutputDeviceEntity and OutputDeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClassOutputDevice.OutputDeviceID - OutputDevice.OutputDeviceID
		/// </summary>
		public virtual IEntityRelation OutputDeviceEntityUsingOutputDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutputDevice", false);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, TicketDeviceClassOutputDeviceFields.OutputDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassOutputDeviceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassOutputDeviceEntity and TicketDeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClassOutputDevice.TicketDeviceClassID - TicketDeviceClass.TicketDeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingTicketDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketDeviceClass", false);
				relation.AddEntityFieldPair(TicketDeviceClassFields.TicketDeviceClassID, TicketDeviceClassOutputDeviceFields.TicketDeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassOutputDeviceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketDeviceClassOutputDeviceRelations
	{
		internal static readonly IEntityRelation OutputDeviceEntityUsingOutputDeviceIDStatic = new TicketDeviceClassOutputDeviceRelations().OutputDeviceEntityUsingOutputDeviceID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingTicketDeviceClassIDStatic = new TicketDeviceClassOutputDeviceRelations().TicketDeviceClassEntityUsingTicketDeviceClassID;

		/// <summary>CTor</summary>
		static StaticTicketDeviceClassOutputDeviceRelations()
		{
		}
	}
}
