﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementRunValueToVarioSettlement. </summary>
	public partial class SettlementRunValueToVarioSettlementRelations
	{
		/// <summary>CTor</summary>
		public SettlementRunValueToVarioSettlementRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementRunValueToVarioSettlementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VarioSettlementEntityUsingVarioSettlementID);
			toReturn.Add(this.SettlementRunValueEntityUsingSettlementRunValueID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SettlementRunValueToVarioSettlementEntity and VarioSettlementEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementRunValueToVarioSettlement.VarioSettlementID - VarioSettlement.ID
		/// </summary>
		public virtual IEntityRelation VarioSettlementEntityUsingVarioSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VarioSettlement", false);
				relation.AddEntityFieldPair(VarioSettlementFields.ID, SettlementRunValueToVarioSettlementFields.VarioSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueToVarioSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SettlementRunValueToVarioSettlementEntity and SettlementRunValueEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementRunValueToVarioSettlement.SettlementRunValueID - SettlementRunValue.SettlementRunValueID
		/// </summary>
		public virtual IEntityRelation SettlementRunValueEntityUsingSettlementRunValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SettlementRunValue", false);
				relation.AddEntityFieldPair(SettlementRunValueFields.SettlementRunValueID, SettlementRunValueToVarioSettlementFields.SettlementRunValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueToVarioSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementRunValueToVarioSettlementRelations
	{
		internal static readonly IEntityRelation VarioSettlementEntityUsingVarioSettlementIDStatic = new SettlementRunValueToVarioSettlementRelations().VarioSettlementEntityUsingVarioSettlementID;
		internal static readonly IEntityRelation SettlementRunValueEntityUsingSettlementRunValueIDStatic = new SettlementRunValueToVarioSettlementRelations().SettlementRunValueEntityUsingSettlementRunValueID;

		/// <summary>CTor</summary>
		static StaticSettlementRunValueToVarioSettlementRelations()
		{
		}
	}
}
