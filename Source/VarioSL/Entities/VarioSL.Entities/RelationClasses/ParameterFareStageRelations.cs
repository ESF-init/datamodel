﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterFareStage. </summary>
	public partial class ParameterFareStageRelations
	{
		/// <summary>CTor</summary>
		public ParameterFareStageRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterFareStageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareStageEntityUsingFareStageID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TariffParameterEntityUsingParameterID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterFareStageEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterFareStage.FareStageID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, ParameterFareStageFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterFareStageEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterFareStage.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ParameterFareStageFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterFareStageEntity and TariffParameterEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterFareStage.ParameterID - TariffParameter.ParameterID
		/// </summary>
		public virtual IEntityRelation TariffParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Parameters", false);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterFareStageFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterFareStageRelations
	{
		internal static readonly IEntityRelation FareStageEntityUsingFareStageIDStatic = new ParameterFareStageRelations().FareStageEntityUsingFareStageID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ParameterFareStageRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TariffParameterEntityUsingParameterIDStatic = new ParameterFareStageRelations().TariffParameterEntityUsingParameterID;

		/// <summary>CTor</summary>
		static StaticParameterFareStageRelations()
		{
		}
	}
}
