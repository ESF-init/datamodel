﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OutputDevice. </summary>
	public partial class OutputDeviceRelations
	{
		/// <summary>CTor</summary>
		public OutputDeviceRelations()
		{
		}

		/// <summary>Gets all relations of the OutputDeviceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LayoutEntityUsingOutDeviceID);
			toReturn.Add(this.TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID);
			toReturn.Add(this.TicketOutputdeviceEntityUsingOutputDeviceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OutputDeviceEntity and LayoutEntity over the 1:n relation they have, using the relation between the fields:
		/// OutputDevice.OutputDeviceID - Layout.OutDeviceID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingOutDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Layouts" , true);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, LayoutFields.OutDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutputDeviceEntity and TicketDeviceClassOutputDeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// OutputDevice.OutputDeviceID - TicketDeviceClassOutputDevice.OutputDeviceID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClassOutputDevice" , true);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, TicketDeviceClassOutputDeviceFields.OutputDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassOutputDeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OutputDeviceEntity and TicketOutputdeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// OutputDevice.OutputDeviceID - TicketOutputdevice.OutputDeviceID
		/// </summary>
		public virtual IEntityRelation TicketOutputdeviceEntityUsingOutputDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketOutputDevices" , true);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, TicketOutputdeviceFields.OutputDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOutputdeviceEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOutputDeviceRelations
	{
		internal static readonly IEntityRelation LayoutEntityUsingOutDeviceIDStatic = new OutputDeviceRelations().LayoutEntityUsingOutDeviceID;
		internal static readonly IEntityRelation TicketDeviceClassOutputDeviceEntityUsingOutputDeviceIDStatic = new OutputDeviceRelations().TicketDeviceClassOutputDeviceEntityUsingOutputDeviceID;
		internal static readonly IEntityRelation TicketOutputdeviceEntityUsingOutputDeviceIDStatic = new OutputDeviceRelations().TicketOutputdeviceEntityUsingOutputDeviceID;

		/// <summary>CTor</summary>
		static StaticOutputDeviceRelations()
		{
		}
	}
}
