﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: JobCardImport. </summary>
	public partial class JobCardImportRelations
	{
		/// <summary>CTor</summary>
		public JobCardImportRelations()
		{
		}

		/// <summary>Gets all relations of the JobCardImportEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.JobEntityUsingJobID);
			toReturn.Add(this.StorageLocationEntityUsingStorageLocationID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between JobCardImportEntity and CardPhysicalDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// JobCardImport.CardPhysicalDetailID - CardPhysicalDetail.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardPhysicalDetail", false);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, JobCardImportFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobCardImportEntity and JobEntity over the m:1 relation they have, using the relation between the fields:
		/// JobCardImport.JobID - Job.JobID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingJobID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Job", false);
				relation.AddEntityFieldPair(JobFields.JobID, JobCardImportFields.JobID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobCardImportEntity and StorageLocationEntity over the m:1 relation they have, using the relation between the fields:
		/// JobCardImport.StorageLocationID - StorageLocation.StorageLocationID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StorageLocation", false);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, JobCardImportFields.StorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticJobCardImportRelations
	{
		internal static readonly IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic = new JobCardImportRelations().CardPhysicalDetailEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation JobEntityUsingJobIDStatic = new JobCardImportRelations().JobEntityUsingJobID;
		internal static readonly IEntityRelation StorageLocationEntityUsingStorageLocationIDStatic = new JobCardImportRelations().StorageLocationEntityUsingStorageLocationID;

		/// <summary>CTor</summary>
		static StaticJobCardImportRelations()
		{
		}
	}
}
