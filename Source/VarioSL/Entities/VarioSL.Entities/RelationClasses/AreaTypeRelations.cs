﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AreaType. </summary>
	public partial class AreaTypeRelations
	{
		/// <summary>CTor</summary>
		public AreaTypeRelations()
		{
		}

		/// <summary>Gets all relations of the AreaTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListEntityUsingAreaListTypeID);
			toReturn.Add(this.AreaListElementEntityUsingAreaInstanceTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AreaTypeEntity and AreaListEntity over the 1:n relation they have, using the relation between the fields:
		/// AreaType.TypeID - AreaList.AreaListTypeID
		/// </summary>
		public virtual IEntityRelation AreaListEntityUsingAreaListTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaListType" , true);
				relation.AddEntityFieldPair(AreaTypeFields.TypeID, AreaListFields.AreaListTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AreaTypeEntity and AreaListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// AreaType.TypeID - AreaListElement.AreaInstanceTypeID
		/// </summary>
		public virtual IEntityRelation AreaListElementEntityUsingAreaInstanceTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AreaListElement" , true);
				relation.AddEntityFieldPair(AreaTypeFields.TypeID, AreaListElementFields.AreaInstanceTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAreaTypeRelations
	{
		internal static readonly IEntityRelation AreaListEntityUsingAreaListTypeIDStatic = new AreaTypeRelations().AreaListEntityUsingAreaListTypeID;
		internal static readonly IEntityRelation AreaListElementEntityUsingAreaInstanceTypeIDStatic = new AreaTypeRelations().AreaListElementEntityUsingAreaInstanceTypeID;

		/// <summary>CTor</summary>
		static StaticAreaTypeRelations()
		{
		}
	}
}
