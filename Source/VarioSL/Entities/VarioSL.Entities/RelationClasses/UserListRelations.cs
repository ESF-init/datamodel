﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserList. </summary>
	public partial class UserListRelations
	{
		/// <summary>CTor</summary>
		public UserListRelations()
		{
		}

		/// <summary>Gets all relations of the UserListEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccountEntryEntityUsingUserID);
			toReturn.Add(this.UserIsMemberEntityUsingUserID);
			toReturn.Add(this.UserToWorkstationEntityUsingUserID);
			toReturn.Add(this.VarioSettlementEntityUsingUserID);
			toReturn.Add(this.ContractTerminationEntityUsingEmployeeID);
			toReturn.Add(this.FilterValueSetEntityUsingOwnerID);
			toReturn.Add(this.FilterValueSetEntityUsingUserID);
			toReturn.Add(this.InvoiceEntityUsingUserID);
			toReturn.Add(this.InvoicingEntityUsingUserID);
			toReturn.Add(this.OrderEntityUsingAssignee);
			toReturn.Add(this.PostingEntityUsingUserID);
			toReturn.Add(this.ProductTerminationEntityUsingEmployeeID);
			toReturn.Add(this.ReportJobEntityUsingUserID);
			toReturn.Add(this.WorkItemEntityUsingAssignee);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DebtorEntityUsingDebtorID);
			toReturn.Add(this.DepotEntityUsingDepotID);
			toReturn.Add(this.LanguageEntityUsingLanguageID);
			toReturn.Add(this.UserLanguageEntityUsingLanguageID);
			toReturn.Add(this.VarioAddressEntityUsingAddressID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and AccountEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - AccountEntry.UserID
		/// </summary>
		public virtual IEntityRelation AccountEntryEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountEntries" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, AccountEntryFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and UserIsMemberEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - UserIsMember.UserID
		/// </summary>
		public virtual IEntityRelation UserIsMemberEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserIsMembers" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, UserIsMemberFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserIsMemberEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and UserToWorkstationEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - UserToWorkstation.UserID
		/// </summary>
		public virtual IEntityRelation UserToWorkstationEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UsersToWorkstations" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, UserToWorkstationFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserToWorkstationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and VarioSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - VarioSettlement.UserID
		/// </summary>
		public virtual IEntityRelation VarioSettlementEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Settlements" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, VarioSettlementFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and ContractTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - ContractTermination.EmployeeID
		/// </summary>
		public virtual IEntityRelation ContractTerminationEntityUsingEmployeeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractTerminations" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, ContractTerminationFields.EmployeeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and FilterValueSetEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - FilterValueSet.OwnerID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingOwnerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValueSets" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, FilterValueSetFields.OwnerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and FilterValueSetEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - FilterValueSet.UserID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValueSets_" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, FilterValueSetFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - Invoice.UserID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, InvoiceFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and InvoicingEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - Invoicing.UserID
		/// </summary>
		public virtual IEntityRelation InvoicingEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoicings" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, InvoicingFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - Order.Assignee
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Orders" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, OrderFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - Posting.UserID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, PostingFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and ProductTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - ProductTermination.EmployeeID
		/// </summary>
		public virtual IEntityRelation ProductTerminationEntityUsingEmployeeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTerminations" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, ProductTerminationFields.EmployeeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and ReportJobEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - ReportJob.UserID
		/// </summary>
		public virtual IEntityRelation ReportJobEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobs" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, ReportJobFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserListEntity and WorkItemEntity over the 1:n relation they have, using the relation between the fields:
		/// UserList.UserID - WorkItem.Assignee
		/// </summary>
		public virtual IEntityRelation WorkItemEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItems" , true);
				relation.AddEntityFieldPair(UserListFields.UserID, WorkItemFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UserListEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, UserListFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserListEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.DebtorID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Debtor", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, UserListFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserListEntity and DepotEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.DepotID - Depot.DepotID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VarioDepot", false);
				relation.AddEntityFieldPair(DepotFields.DepotID, UserListFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserListEntity and LanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.LanguageID - Language.LanguageID
		/// </summary>
		public virtual IEntityRelation LanguageEntityUsingLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Language", false);
				relation.AddEntityFieldPair(LanguageFields.LanguageID, UserListFields.LanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserListEntity and UserLanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.LanguageID - UserLanguage.LanguageID
		/// </summary>
		public virtual IEntityRelation UserLanguageEntityUsingLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserLanguage", false);
				relation.AddEntityFieldPair(UserLanguageFields.LanguageID, UserListFields.LanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserLanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserListEntity and VarioAddressEntity over the m:1 relation they have, using the relation between the fields:
		/// UserList.AddressID - VarioAddress.AddressID
		/// </summary>
		public virtual IEntityRelation VarioAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VarioAddress", false);
				relation.AddEntityFieldPair(VarioAddressFields.AddressID, UserListFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserListRelations
	{
		internal static readonly IEntityRelation AccountEntryEntityUsingUserIDStatic = new UserListRelations().AccountEntryEntityUsingUserID;
		internal static readonly IEntityRelation UserIsMemberEntityUsingUserIDStatic = new UserListRelations().UserIsMemberEntityUsingUserID;
		internal static readonly IEntityRelation UserToWorkstationEntityUsingUserIDStatic = new UserListRelations().UserToWorkstationEntityUsingUserID;
		internal static readonly IEntityRelation VarioSettlementEntityUsingUserIDStatic = new UserListRelations().VarioSettlementEntityUsingUserID;
		internal static readonly IEntityRelation ContractTerminationEntityUsingEmployeeIDStatic = new UserListRelations().ContractTerminationEntityUsingEmployeeID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingOwnerIDStatic = new UserListRelations().FilterValueSetEntityUsingOwnerID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingUserIDStatic = new UserListRelations().FilterValueSetEntityUsingUserID;
		internal static readonly IEntityRelation InvoiceEntityUsingUserIDStatic = new UserListRelations().InvoiceEntityUsingUserID;
		internal static readonly IEntityRelation InvoicingEntityUsingUserIDStatic = new UserListRelations().InvoicingEntityUsingUserID;
		internal static readonly IEntityRelation OrderEntityUsingAssigneeStatic = new UserListRelations().OrderEntityUsingAssignee;
		internal static readonly IEntityRelation PostingEntityUsingUserIDStatic = new UserListRelations().PostingEntityUsingUserID;
		internal static readonly IEntityRelation ProductTerminationEntityUsingEmployeeIDStatic = new UserListRelations().ProductTerminationEntityUsingEmployeeID;
		internal static readonly IEntityRelation ReportJobEntityUsingUserIDStatic = new UserListRelations().ReportJobEntityUsingUserID;
		internal static readonly IEntityRelation WorkItemEntityUsingAssigneeStatic = new UserListRelations().WorkItemEntityUsingAssignee;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new UserListRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DebtorEntityUsingDebtorIDStatic = new UserListRelations().DebtorEntityUsingDebtorID;
		internal static readonly IEntityRelation DepotEntityUsingDepotIDStatic = new UserListRelations().DepotEntityUsingDepotID;
		internal static readonly IEntityRelation LanguageEntityUsingLanguageIDStatic = new UserListRelations().LanguageEntityUsingLanguageID;
		internal static readonly IEntityRelation UserLanguageEntityUsingLanguageIDStatic = new UserListRelations().UserLanguageEntityUsingLanguageID;
		internal static readonly IEntityRelation VarioAddressEntityUsingAddressIDStatic = new UserListRelations().VarioAddressEntityUsingAddressID;

		/// <summary>CTor</summary>
		static StaticUserListRelations()
		{
		}
	}
}
