﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Voucher. </summary>
	public partial class VoucherRelations
	{
		/// <summary>CTor</summary>
		public VoucherRelations()
		{
		}

		/// <summary>Gets all relations of the VoucherEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProductEntityUsingVoucherID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.CardEntityUsingRedeemedForCardID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VoucherEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Voucher.VoucherID - Product.VoucherID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingVoucherID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(VoucherFields.VoucherID, ProductFields.VoucherID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VoucherEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// Voucher.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, VoucherFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VoucherEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// Voucher.RedeemedForCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingRedeemedForCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, VoucherFields.RedeemedForCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVoucherRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingVoucherIDStatic = new VoucherRelations().ProductEntityUsingVoucherID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new VoucherRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation CardEntityUsingRedeemedForCardIDStatic = new VoucherRelations().CardEntityUsingRedeemedForCardID;

		/// <summary>CTor</summary>
		static StaticVoucherRelations()
		{
		}
	}
}
