﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AutoloadToPaymentOption. </summary>
	public partial class AutoloadToPaymentOptionRelations
	{
		/// <summary>CTor</summary>
		public AutoloadToPaymentOptionRelations()
		{
		}

		/// <summary>Gets all relations of the AutoloadToPaymentOptionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutoloadSettingEntityUsingAutoloadSettingID);
			toReturn.Add(this.PaymentOptionEntityUsingBackupPaymentOptionID);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AutoloadToPaymentOptionEntity and AutoloadSettingEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadToPaymentOption.AutoloadSettingID - AutoloadSetting.AutoloadSettingID
		/// </summary>
		public virtual IEntityRelation AutoloadSettingEntityUsingAutoloadSettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AutoloadSetting", false);
				relation.AddEntityFieldPair(AutoloadSettingFields.AutoloadSettingID, AutoloadToPaymentOptionFields.AutoloadSettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AutoloadToPaymentOptionEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadToPaymentOption.BackupPaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingBackupPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BackupPaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadToPaymentOptionFields.BackupPaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AutoloadToPaymentOptionEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// AutoloadToPaymentOption.PaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadToPaymentOptionFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAutoloadToPaymentOptionRelations
	{
		internal static readonly IEntityRelation AutoloadSettingEntityUsingAutoloadSettingIDStatic = new AutoloadToPaymentOptionRelations().AutoloadSettingEntityUsingAutoloadSettingID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingBackupPaymentOptionIDStatic = new AutoloadToPaymentOptionRelations().PaymentOptionEntityUsingBackupPaymentOptionID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new AutoloadToPaymentOptionRelations().PaymentOptionEntityUsingPaymentOptionID;

		/// <summary>CTor</summary>
		static StaticAutoloadToPaymentOptionRelations()
		{
		}
	}
}
