﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvTag. </summary>
	public partial class VdvTagRelations
	{
		/// <summary>CTor</summary>
		public VdvTagRelations()
		{
		}

		/// <summary>Gets all relations of the VdvTagEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvLayoutObjectEntityUsingVdvTagID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VdvTagEntity and VdvLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvTag.TagID - VdvLayoutObject.VdvTagID
		/// </summary>
		public virtual IEntityRelation VdvLayoutObjectEntityUsingVdvTagID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvLayoutObjects" , true);
				relation.AddEntityFieldPair(VdvTagFields.TagID, VdvLayoutObjectFields.VdvTagID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTagEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvTagRelations
	{
		internal static readonly IEntityRelation VdvLayoutObjectEntityUsingVdvTagIDStatic = new VdvTagRelations().VdvLayoutObjectEntityUsingVdvTagID;

		/// <summary>CTor</summary>
		static StaticVdvTagRelations()
		{
		}
	}
}
