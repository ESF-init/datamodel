﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SalesChannelToPaymentMethod. </summary>
	public partial class SalesChannelToPaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public SalesChannelToPaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the SalesChannelToPaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SalesChannelToPaymentMethodEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// SalesChannelToPaymentMethod.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, SalesChannelToPaymentMethodFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesChannelToPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SalesChannelToPaymentMethodEntity and DevicePaymentMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// SalesChannelToPaymentMethod.DevicePaymentMethodID - DevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DevicePaymentMethod", false);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, SalesChannelToPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesChannelToPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSalesChannelToPaymentMethodRelations
	{
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new SalesChannelToPaymentMethodRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new SalesChannelToPaymentMethodRelations().DevicePaymentMethodEntityUsingDevicePaymentMethodID;

		/// <summary>CTor</summary>
		static StaticSalesChannelToPaymentMethodRelations()
		{
		}
	}
}
