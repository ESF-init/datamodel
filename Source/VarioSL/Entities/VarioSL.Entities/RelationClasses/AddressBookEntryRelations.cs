﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AddressBookEntry. </summary>
	public partial class AddressBookEntryRelations
	{
		/// <summary>CTor</summary>
		public AddressBookEntryRelations()
		{
		}

		/// <summary>Gets all relations of the AddressBookEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.AddressEntityUsingAddressID);
			toReturn.Add(this.BankConnectionDataEntityUsingBankConnectionDataID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AddressBookEntryEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// AddressBookEntry.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, AddressBookEntryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AddressBookEntryEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// AddressBookEntry.AddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Address", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, AddressBookEntryFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AddressBookEntryEntity and BankConnectionDataEntity over the m:1 relation they have, using the relation between the fields:
		/// AddressBookEntry.BankConnectionDataID - BankConnectionData.BankConnectionDataID
		/// </summary>
		public virtual IEntityRelation BankConnectionDataEntityUsingBankConnectionDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BankConnectionData", false);
				relation.AddEntityFieldPair(BankConnectionDataFields.BankConnectionDataID, AddressBookEntryFields.BankConnectionDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAddressBookEntryRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new AddressBookEntryRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new AddressBookEntryRelations().AddressEntityUsingAddressID;
		internal static readonly IEntityRelation BankConnectionDataEntityUsingBankConnectionDataIDStatic = new AddressBookEntryRelations().BankConnectionDataEntityUsingBankConnectionDataID;

		/// <summary>CTor</summary>
		static StaticAddressBookEntryRelations()
		{
		}
	}
}
