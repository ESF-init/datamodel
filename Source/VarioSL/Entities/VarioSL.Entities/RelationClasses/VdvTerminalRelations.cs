﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvTerminal. </summary>
	public partial class VdvTerminalRelations
	{
		/// <summary>CTor</summary>
		public VdvTerminalRelations()
		{
		}

		/// <summary>Gets all relations of the VdvTerminalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvSamStatusEntityUsingTerminalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VdvTerminalEntity and VdvSamStatusEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvTerminal.ID - VdvSamStatus.TerminalID
		/// </summary>
		public virtual IEntityRelation VdvSamStatusEntityUsingTerminalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvSamStatuses" , true);
				relation.AddEntityFieldPair(VdvTerminalFields.ID, VdvSamStatusFields.TerminalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvSamStatusEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvTerminalRelations
	{
		internal static readonly IEntityRelation VdvSamStatusEntityUsingTerminalIDStatic = new VdvTerminalRelations().VdvSamStatusEntityUsingTerminalID;

		/// <summary>CTor</summary>
		static StaticVdvTerminalRelations()
		{
		}
	}
}
