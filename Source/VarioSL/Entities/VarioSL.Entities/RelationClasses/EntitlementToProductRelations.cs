﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntitlementToProduct. </summary>
	public partial class EntitlementToProductRelations
	{
		/// <summary>CTor</summary>
		public EntitlementToProductRelations()
		{
		}

		/// <summary>Gets all relations of the EntitlementToProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntitlementEntityUsingEntitlementID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EntitlementToProductEntity and EntitlementEntity over the m:1 relation they have, using the relation between the fields:
		/// EntitlementToProduct.EntitlementID - Entitlement.EntitlementID
		/// </summary>
		public virtual IEntityRelation EntitlementEntityUsingEntitlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Entitlement", false);
				relation.AddEntityFieldPair(EntitlementFields.EntitlementID, EntitlementToProductFields.EntitlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementToProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntitlementToProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// EntitlementToProduct.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, EntitlementToProductFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementToProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntitlementToProductRelations
	{
		internal static readonly IEntityRelation EntitlementEntityUsingEntitlementIDStatic = new EntitlementToProductRelations().EntitlementEntityUsingEntitlementID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new EntitlementToProductRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticEntitlementToProductRelations()
		{
		}
	}
}
