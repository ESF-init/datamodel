﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketAssignment. </summary>
	public partial class TicketAssignmentRelations
	{
		/// <summary>CTor</summary>
		public TicketAssignmentRelations()
		{
		}

		/// <summary>Gets all relations of the TicketAssignmentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RuleTypeEntityUsingPeriodicTypeID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.EntitlementEntityUsingEntitlementID);
			toReturn.Add(this.OrganizationEntityUsingInstitutionID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketAssignmentEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketAssignment.PeriodicTypeID - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingPeriodicTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleType", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, TicketAssignmentFields.PeriodicTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketAssignmentEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketAssignment.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, TicketAssignmentFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketAssignmentEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketAssignment.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, TicketAssignmentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketAssignmentEntity and EntitlementEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketAssignment.EntitlementID - Entitlement.EntitlementID
		/// </summary>
		public virtual IEntityRelation EntitlementEntityUsingEntitlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Entitlement", false);
				relation.AddEntityFieldPair(EntitlementFields.EntitlementID, TicketAssignmentFields.EntitlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketAssignmentEntity and OrganizationEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketAssignment.InstitutionID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingInstitutionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Organization", false);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, TicketAssignmentFields.InstitutionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketAssignmentRelations
	{
		internal static readonly IEntityRelation RuleTypeEntityUsingPeriodicTypeIDStatic = new TicketAssignmentRelations().RuleTypeEntityUsingPeriodicTypeID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new TicketAssignmentRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new TicketAssignmentRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation EntitlementEntityUsingEntitlementIDStatic = new TicketAssignmentRelations().EntitlementEntityUsingEntitlementID;
		internal static readonly IEntityRelation OrganizationEntityUsingInstitutionIDStatic = new TicketAssignmentRelations().OrganizationEntityUsingInstitutionID;

		/// <summary>CTor</summary>
		static StaticTicketAssignmentRelations()
		{
		}
	}
}
