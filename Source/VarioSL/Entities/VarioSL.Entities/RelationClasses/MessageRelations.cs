﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Message. </summary>
	public partial class MessageRelations
	{
		/// <summary>CTor</summary>
		public MessageRelations()
		{
		}

		/// <summary>Gets all relations of the MessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.JobEntityUsingMessageID);
			toReturn.Add(this.EmailEventEntityUsingEmailEventID);
			toReturn.Add(this.RecipientGroupEntityUsingRecipientGroupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessageEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// Message.MessageID - Job.MessageID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingMessageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(MessageFields.MessageID, JobFields.MessageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MessageEntity and EmailEventEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.EmailEventID - EmailEvent.EmailEventID
		/// </summary>
		public virtual IEntityRelation EmailEventEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailEvent", false);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, MessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between MessageEntity and RecipientGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Message.RecipientGroupID - RecipientGroup.RecipientGroupID
		/// </summary>
		public virtual IEntityRelation RecipientGroupEntityUsingRecipientGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RecipientGroup", false);
				relation.AddEntityFieldPair(RecipientGroupFields.RecipientGroupID, MessageFields.RecipientGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageRelations
	{
		internal static readonly IEntityRelation JobEntityUsingMessageIDStatic = new MessageRelations().JobEntityUsingMessageID;
		internal static readonly IEntityRelation EmailEventEntityUsingEmailEventIDStatic = new MessageRelations().EmailEventEntityUsingEmailEventID;
		internal static readonly IEntityRelation RecipientGroupEntityUsingRecipientGroupIDStatic = new MessageRelations().RecipientGroupEntityUsingRecipientGroupID;

		/// <summary>CTor</summary>
		static StaticMessageRelations()
		{
		}
	}
}
