﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CalendarEntry. </summary>
	public partial class CalendarEntryRelations
	{
		/// <summary>CTor</summary>
		public CalendarEntryRelations()
		{
		}

		/// <summary>Gets all relations of the CalendarEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CalendarEntityUsingCalendarID);
			toReturn.Add(this.DayTypeEntityUsingDayTypeEntryID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CalendarEntryEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// CalendarEntry.CalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Calendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, CalendarEntryFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CalendarEntryEntity and DayTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// CalendarEntry.DayTypeEntryID - DayType.DayTypeID
		/// </summary>
		public virtual IEntityRelation DayTypeEntityUsingDayTypeEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DayType", false);
				relation.AddEntityFieldPair(DayTypeFields.DayTypeID, CalendarEntryFields.DayTypeEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DayTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCalendarEntryRelations
	{
		internal static readonly IEntityRelation CalendarEntityUsingCalendarIDStatic = new CalendarEntryRelations().CalendarEntityUsingCalendarID;
		internal static readonly IEntityRelation DayTypeEntityUsingDayTypeEntryIDStatic = new CalendarEntryRelations().DayTypeEntityUsingDayTypeEntryID;

		/// <summary>CTor</summary>
		static StaticCalendarEntryRelations()
		{
		}
	}
}
