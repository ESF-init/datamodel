﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalEffort. </summary>
	public partial class ExternalEffortRelations
	{
		/// <summary>CTor</summary>
		public ExternalEffortRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalEffortEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalCardEntityUsingEffortID);
			toReturn.Add(this.ExternalPacketEffortEntityUsingEffortID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ExternalEffortEntity and ExternalCardEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalEffort.EffortID - ExternalCard.EffortID
		/// </summary>
		public virtual IEntityRelation ExternalCardEntityUsingEffortID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalCards" , true);
				relation.AddEntityFieldPair(ExternalEffortFields.EffortID, ExternalCardFields.EffortID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ExternalEffortEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// ExternalEffort.EffortID - ExternalPacketEffort.EffortID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingEffortID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketEfforts" , true);
				relation.AddEntityFieldPair(ExternalEffortFields.EffortID, ExternalPacketEffortFields.EffortID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ExternalEffortEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalEffort.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalEffortFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalEffortRelations
	{
		internal static readonly IEntityRelation ExternalCardEntityUsingEffortIDStatic = new ExternalEffortRelations().ExternalCardEntityUsingEffortID;
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingEffortIDStatic = new ExternalEffortRelations().ExternalPacketEffortEntityUsingEffortID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ExternalEffortRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticExternalEffortRelations()
		{
		}
	}
}
