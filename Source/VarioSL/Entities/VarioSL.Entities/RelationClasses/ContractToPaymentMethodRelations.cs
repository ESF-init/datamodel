﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractToPaymentMethod. </summary>
	public partial class ContractToPaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public ContractToPaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the ContractToPaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ContractToPaymentMethodEntity and DevicePaymentMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractToPaymentMethod.DevicePaymentMethodID - DevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DevicePaymentMethod", false);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, ContractToPaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractToPaymentMethodEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractToPaymentMethod.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractToPaymentMethodFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToPaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractToPaymentMethodRelations
	{
		internal static readonly IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new ContractToPaymentMethodRelations().DevicePaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ContractToPaymentMethodRelations().ContractEntityUsingContractID;

		/// <summary>CTor</summary>
		static StaticContractToPaymentMethodRelations()
		{
		}
	}
}
