﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Tenant. </summary>
	public partial class TenantRelations
	{
		/// <summary>CTor</summary>
		public TenantRelations()
		{
		}

		/// <summary>Gets all relations of the TenantEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TenantHistoryEntityUsingTenantID);
			toReturn.Add(this.TenantPersonEntityUsingTenantID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TenantEntity and TenantHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Tenant.TenantID - TenantHistory.TenantID
		/// </summary>
		public virtual IEntityRelation TenantHistoryEntityUsingTenantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TenantHistories" , true);
				relation.AddEntityFieldPair(TenantFields.TenantID, TenantHistoryFields.TenantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TenantEntity and TenantPersonEntity over the 1:n relation they have, using the relation between the fields:
		/// Tenant.TenantID - TenantPerson.TenantID
		/// </summary>
		public virtual IEntityRelation TenantPersonEntityUsingTenantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TenantPeople" , true);
				relation.AddEntityFieldPair(TenantFields.TenantID, TenantPersonFields.TenantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantPersonEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTenantRelations
	{
		internal static readonly IEntityRelation TenantHistoryEntityUsingTenantIDStatic = new TenantRelations().TenantHistoryEntityUsingTenantID;
		internal static readonly IEntityRelation TenantPersonEntityUsingTenantIDStatic = new TenantRelations().TenantPersonEntityUsingTenantID;

		/// <summary>CTor</summary>
		static StaticTenantRelations()
		{
		}
	}
}
