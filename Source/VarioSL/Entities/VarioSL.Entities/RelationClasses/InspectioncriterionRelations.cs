﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InspectionCriterion. </summary>
	public partial class InspectionCriterionRelations
	{
		/// <summary>CTor</summary>
		public InspectionCriterionRelations()
		{
		}

		/// <summary>Gets all relations of the InspectionCriterionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.InspectionReportEntityUsingInspectionReportID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between InspectionCriterionEntity and InspectionReportEntity over the m:1 relation they have, using the relation between the fields:
		/// InspectionCriterion.InspectionReportID - InspectionReport.InspectionReportID
		/// </summary>
		public virtual IEntityRelation InspectionReportEntityUsingInspectionReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InspectionReport", false);
				relation.AddEntityFieldPair(InspectionReportFields.InspectionReportID, InspectionCriterionFields.InspectionReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionCriterionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInspectionCriterionRelations
	{
		internal static readonly IEntityRelation InspectionReportEntityUsingInspectionReportIDStatic = new InspectionCriterionRelations().InspectionReportEntityUsingInspectionReportID;

		/// <summary>CTor</summary>
		static StaticInspectionCriterionRelations()
		{
		}
	}
}
