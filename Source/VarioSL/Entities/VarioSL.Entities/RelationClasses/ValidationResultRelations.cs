﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ValidationResult. </summary>
	public partial class ValidationResultRelations
	{
		/// <summary>CTor</summary>
		public ValidationResultRelations()
		{
		}

		/// <summary>Gets all relations of the ValidationResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ValidationResultEntityUsingOriginalValidationResultID);
			toReturn.Add(this.ValidationValueEntityUsingValidationResultID);
			toReturn.Add(this.ValidationResultEntityUsingValidationResultIDOriginalValidationResultID);
			toReturn.Add(this.ValidationRuleEntityUsingValidationRuleID);
			toReturn.Add(this.ValidationRunRuleEntityUsingValidationRunRuleID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ValidationResultEntity and ValidationResultEntity over the 1:n relation they have, using the relation between the fields:
		/// ValidationResult.ValidationResultID - ValidationResult.OriginalValidationResultID
		/// </summary>
		public virtual IEntityRelation ValidationResultEntityUsingOriginalValidationResultID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ValidationResults" , true);
				relation.AddEntityFieldPair(ValidationResultFields.ValidationResultID, ValidationResultFields.OriginalValidationResultID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ValidationResultEntity and ValidationValueEntity over the 1:n relation they have, using the relation between the fields:
		/// ValidationResult.ValidationResultID - ValidationValue.ValidationResultID
		/// </summary>
		public virtual IEntityRelation ValidationValueEntityUsingValidationResultID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ValidationValues" , true);
				relation.AddEntityFieldPair(ValidationResultFields.ValidationResultID, ValidationValueFields.ValidationResultID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ValidationResultEntity and ValidationResultEntity over the m:1 relation they have, using the relation between the fields:
		/// ValidationResult.OriginalValidationResultID - ValidationResult.ValidationResultID
		/// </summary>
		public virtual IEntityRelation ValidationResultEntityUsingValidationResultIDOriginalValidationResultID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ValidationResult", false);
				relation.AddEntityFieldPair(ValidationResultFields.ValidationResultID, ValidationResultFields.OriginalValidationResultID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ValidationResultEntity and ValidationRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// ValidationResult.ValidationRuleID - ValidationRule.ValidationRuleID
		/// </summary>
		public virtual IEntityRelation ValidationRuleEntityUsingValidationRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ValidationRule", false);
				relation.AddEntityFieldPair(ValidationRuleFields.ValidationRuleID, ValidationResultFields.ValidationRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ValidationResultEntity and ValidationRunRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// ValidationResult.ValidationRunRuleID - ValidationRunRule.ValidationRunRuleID
		/// </summary>
		public virtual IEntityRelation ValidationRunRuleEntityUsingValidationRunRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ValidationRunRule", false);
				relation.AddEntityFieldPair(ValidationRunRuleFields.ValidationRunRuleID, ValidationResultFields.ValidationRunRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticValidationResultRelations
	{
		internal static readonly IEntityRelation ValidationResultEntityUsingOriginalValidationResultIDStatic = new ValidationResultRelations().ValidationResultEntityUsingOriginalValidationResultID;
		internal static readonly IEntityRelation ValidationValueEntityUsingValidationResultIDStatic = new ValidationResultRelations().ValidationValueEntityUsingValidationResultID;
		internal static readonly IEntityRelation ValidationResultEntityUsingValidationResultIDOriginalValidationResultIDStatic = new ValidationResultRelations().ValidationResultEntityUsingValidationResultIDOriginalValidationResultID;
		internal static readonly IEntityRelation ValidationRuleEntityUsingValidationRuleIDStatic = new ValidationResultRelations().ValidationRuleEntityUsingValidationRuleID;
		internal static readonly IEntityRelation ValidationRunRuleEntityUsingValidationRunRuleIDStatic = new ValidationResultRelations().ValidationRunRuleEntityUsingValidationRunRuleID;

		/// <summary>CTor</summary>
		static StaticValidationResultRelations()
		{
		}
	}
}
