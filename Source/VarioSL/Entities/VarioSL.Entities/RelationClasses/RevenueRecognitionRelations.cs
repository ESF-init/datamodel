﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RevenueRecognition. </summary>
	public partial class RevenueRecognitionRelations
	{
		/// <summary>CTor</summary>
		public RevenueRecognitionRelations()
		{
		}

		/// <summary>Gets all relations of the RevenueRecognitionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionEntityUsingRevenueRecognitionID);
			toReturn.Add(this.SettledRevenueEntityUsingRevenueRecognitionID);
			toReturn.Add(this.CloseoutPeriodEntityUsingCloseoutPeriodID);
			toReturn.Add(this.RevenueSettlementEntityUsingRevenueSettlementID);
			toReturn.Add(this.TransactionJournalEntityUsingTransactionJournalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RevenueRecognitionEntity and ApportionEntity over the 1:n relation they have, using the relation between the fields:
		/// RevenueRecognition.RevenueRecognitionID - Apportion.RevenueRecognitionID
		/// </summary>
		public virtual IEntityRelation ApportionEntityUsingRevenueRecognitionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Apportions" , true);
				relation.AddEntityFieldPair(RevenueRecognitionFields.RevenueRecognitionID, ApportionFields.RevenueRecognitionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevenueRecognitionEntity and SettledRevenueEntity over the 1:n relation they have, using the relation between the fields:
		/// RevenueRecognition.RevenueRecognitionID - SettledRevenue.RevenueRecognitionID
		/// </summary>
		public virtual IEntityRelation SettledRevenueEntityUsingRevenueRecognitionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettledRevenues" , true);
				relation.AddEntityFieldPair(RevenueRecognitionFields.RevenueRecognitionID, SettledRevenueFields.RevenueRecognitionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettledRevenueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RevenueRecognitionEntity and CloseoutPeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueRecognition.CloseoutPeriodID - CloseoutPeriod.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CloseoutPeriod", false);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, RevenueRecognitionFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevenueRecognitionEntity and RevenueSettlementEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueRecognition.RevenueSettlementID - RevenueSettlement.RevenueSettlementID
		/// </summary>
		public virtual IEntityRelation RevenueSettlementEntityUsingRevenueSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueSettlement", false);
				relation.AddEntityFieldPair(RevenueSettlementFields.RevenueSettlementID, RevenueRecognitionFields.RevenueSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevenueRecognitionEntity and TransactionJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueRecognition.TransactionJournalID - TransactionJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransactionJournal", false);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RevenueRecognitionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRevenueRecognitionRelations
	{
		internal static readonly IEntityRelation ApportionEntityUsingRevenueRecognitionIDStatic = new RevenueRecognitionRelations().ApportionEntityUsingRevenueRecognitionID;
		internal static readonly IEntityRelation SettledRevenueEntityUsingRevenueRecognitionIDStatic = new RevenueRecognitionRelations().SettledRevenueEntityUsingRevenueRecognitionID;
		internal static readonly IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodIDStatic = new RevenueRecognitionRelations().CloseoutPeriodEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation RevenueSettlementEntityUsingRevenueSettlementIDStatic = new RevenueRecognitionRelations().RevenueSettlementEntityUsingRevenueSettlementID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransactionJournalIDStatic = new RevenueRecognitionRelations().TransactionJournalEntityUsingTransactionJournalID;

		/// <summary>CTor</summary>
		static StaticRevenueRecognitionRelations()
		{
		}
	}
}
