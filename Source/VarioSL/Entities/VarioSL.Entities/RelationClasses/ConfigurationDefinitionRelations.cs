﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ConfigurationDefinition. </summary>
	public partial class ConfigurationDefinitionRelations
	{
		/// <summary>CTor</summary>
		public ConfigurationDefinitionRelations()
		{
		}

		/// <summary>Gets all relations of the ConfigurationDefinitionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ConfigurationOverwriteEntityUsingConfigurationDefinitionID);
			toReturn.Add(this.FilterElementEntityUsingValueListDataSourceID);
			toReturn.Add(this.ReportToClientEntityUsingConfigurationID);
			toReturn.Add(this.ClientEntityUsingOwnerClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ConfigurationDefinitionEntity and ConfigurationOverwriteEntity over the 1:n relation they have, using the relation between the fields:
		/// ConfigurationDefinition.ConfigurationDefinitionID - ConfigurationOverwrite.ConfigurationDefinitionID
		/// </summary>
		public virtual IEntityRelation ConfigurationOverwriteEntityUsingConfigurationDefinitionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ConfigurationOverwrites" , true);
				relation.AddEntityFieldPair(ConfigurationDefinitionFields.ConfigurationDefinitionID, ConfigurationOverwriteFields.ConfigurationDefinitionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationOverwriteEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ConfigurationDefinitionEntity and FilterElementEntity over the 1:n relation they have, using the relation between the fields:
		/// ConfigurationDefinition.ConfigurationDefinitionID - FilterElement.ValueListDataSourceID
		/// </summary>
		public virtual IEntityRelation FilterElementEntityUsingValueListDataSourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterElements" , true);
				relation.AddEntityFieldPair(ConfigurationDefinitionFields.ConfigurationDefinitionID, FilterElementFields.ValueListDataSourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ConfigurationDefinitionEntity and ReportToClientEntity over the 1:n relation they have, using the relation between the fields:
		/// ConfigurationDefinition.ConfigurationDefinitionID - ReportToClient.ConfigurationID
		/// </summary>
		public virtual IEntityRelation ReportToClientEntityUsingConfigurationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportToClients" , true);
				relation.AddEntityFieldPair(ConfigurationDefinitionFields.ConfigurationDefinitionID, ReportToClientFields.ConfigurationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ConfigurationDefinitionEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ConfigurationDefinition.OwnerClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ConfigurationDefinitionFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticConfigurationDefinitionRelations
	{
		internal static readonly IEntityRelation ConfigurationOverwriteEntityUsingConfigurationDefinitionIDStatic = new ConfigurationDefinitionRelations().ConfigurationOverwriteEntityUsingConfigurationDefinitionID;
		internal static readonly IEntityRelation FilterElementEntityUsingValueListDataSourceIDStatic = new ConfigurationDefinitionRelations().FilterElementEntityUsingValueListDataSourceID;
		internal static readonly IEntityRelation ReportToClientEntityUsingConfigurationIDStatic = new ConfigurationDefinitionRelations().ReportToClientEntityUsingConfigurationID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIDStatic = new ConfigurationDefinitionRelations().ClientEntityUsingOwnerClientID;

		/// <summary>CTor</summary>
		static StaticConfigurationDefinitionRelations()
		{
		}
	}
}
