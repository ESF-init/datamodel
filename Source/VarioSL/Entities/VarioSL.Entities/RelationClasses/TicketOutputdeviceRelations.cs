﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketOutputdevice. </summary>
	public partial class TicketOutputdeviceRelations
	{
		/// <summary>CTor</summary>
		public TicketOutputdeviceRelations()
		{
		}

		/// <summary>Gets all relations of the TicketOutputdeviceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OutputDeviceEntityUsingOutputDeviceID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketOutputdeviceEntity and OutputDeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketOutputdevice.OutputDeviceID - OutputDevice.OutputDeviceID
		/// </summary>
		public virtual IEntityRelation OutputDeviceEntityUsingOutputDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutputDevice", false);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, TicketOutputdeviceFields.OutputDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOutputdeviceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketOutputdeviceEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketOutputdevice.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketOutputdeviceFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOutputdeviceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketOutputdeviceRelations
	{
		internal static readonly IEntityRelation OutputDeviceEntityUsingOutputDeviceIDStatic = new TicketOutputdeviceRelations().OutputDeviceEntityUsingOutputDeviceID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketOutputdeviceRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticTicketOutputdeviceRelations()
		{
		}
	}
}
