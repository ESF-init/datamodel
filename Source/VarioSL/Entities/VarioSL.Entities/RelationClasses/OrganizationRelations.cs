﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Organization. </summary>
	public partial class OrganizationRelations
	{
		/// <summary>CTor</summary>
		public OrganizationRelations()
		{
		}

		/// <summary>Gets all relations of the OrganizationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketOrganizationEntityUsingOrganizationID);
			toReturn.Add(this.ContractEntityUsingOrganizationID);
			toReturn.Add(this.OrganizationEntityUsingFrameOrganizationID);
			toReturn.Add(this.OrganizationAddressEntityUsingOrganizationID);
			toReturn.Add(this.ProductEntityUsingOrganizationID);
			toReturn.Add(this.ProductSubsidyEntityUsingOrganizationID);
			toReturn.Add(this.TicketAssignmentEntityUsingInstitutionID);
			toReturn.Add(this.CardHolderOrganizationEntityUsingOrganizationID);
			toReturn.Add(this.PersonEntityUsingContactPersonID);
			toReturn.Add(this.SubsidyLimitEntityUsingOrganizationID);
			toReturn.Add(this.OrganizationEntityUsingOrganizationIDFrameOrganizationID);
			toReturn.Add(this.OrganizationTypeEntityUsingOrganizationTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and TicketOrganizationEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - TicketOrganization.OrganizationID
		/// </summary>
		public virtual IEntityRelation TicketOrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketOrganizations" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, TicketOrganizationFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and ContractEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - Contract.OrganizationID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Contracts" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, ContractFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and OrganizationEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - Organization.FrameOrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingFrameOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Organizations" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, OrganizationFields.FrameOrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and OrganizationAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - OrganizationAddress.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationAddressEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrganizationAddresses" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, OrganizationAddressFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - Product.OrganizationID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, ProductFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and ProductSubsidyEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - ProductSubsidy.OrganizationID
		/// </summary>
		public virtual IEntityRelation ProductSubsidyEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductSubsidies" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, ProductSubsidyFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductSubsidyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and TicketAssignmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Organization.OrganizationID - TicketAssignment.InstitutionID
		/// </summary>
		public virtual IEntityRelation TicketAssignmentEntityUsingInstitutionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketAssignments" , true);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, TicketAssignmentFields.InstitutionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and CardHolderOrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Organization.OrganizationID - CardHolderOrganization.OrganizationID
		/// </summary>
		public virtual IEntityRelation CardHolderOrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardHolderOrganization", true);

				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, CardHolderOrganizationFields.OrganizationID);



				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderOrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and PersonEntity over the 1:1 relation they have, using the relation between the fields:
		/// Organization.ContactPersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingContactPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ContactPerson", false);




				relation.AddEntityFieldPair(PersonFields.PersonID, OrganizationFields.ContactPersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and SubsidyLimitEntity over the 1:1 relation they have, using the relation between the fields:
		/// Organization.OrganizationID - SubsidyLimit.OrganizationID
		/// </summary>
		public virtual IEntityRelation SubsidyLimitEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "SubsidyLimit", true);


				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, SubsidyLimitFields.OrganizationID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SubsidyLimitEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and OrganizationEntity over the m:1 relation they have, using the relation between the fields:
		/// Organization.FrameOrganizationID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingOrganizationIDFrameOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Organization", false);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, OrganizationFields.FrameOrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrganizationEntity and OrganizationTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Organization.OrganizationTypeID - OrganizationType.OrganizationTypeID
		/// </summary>
		public virtual IEntityRelation OrganizationTypeEntityUsingOrganizationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrganizationType", false);
				relation.AddEntityFieldPair(OrganizationTypeFields.OrganizationTypeID, OrganizationFields.OrganizationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrganizationRelations
	{
		internal static readonly IEntityRelation TicketOrganizationEntityUsingOrganizationIDStatic = new OrganizationRelations().TicketOrganizationEntityUsingOrganizationID;
		internal static readonly IEntityRelation ContractEntityUsingOrganizationIDStatic = new OrganizationRelations().ContractEntityUsingOrganizationID;
		internal static readonly IEntityRelation OrganizationEntityUsingFrameOrganizationIDStatic = new OrganizationRelations().OrganizationEntityUsingFrameOrganizationID;
		internal static readonly IEntityRelation OrganizationAddressEntityUsingOrganizationIDStatic = new OrganizationRelations().OrganizationAddressEntityUsingOrganizationID;
		internal static readonly IEntityRelation ProductEntityUsingOrganizationIDStatic = new OrganizationRelations().ProductEntityUsingOrganizationID;
		internal static readonly IEntityRelation ProductSubsidyEntityUsingOrganizationIDStatic = new OrganizationRelations().ProductSubsidyEntityUsingOrganizationID;
		internal static readonly IEntityRelation TicketAssignmentEntityUsingInstitutionIDStatic = new OrganizationRelations().TicketAssignmentEntityUsingInstitutionID;
		internal static readonly IEntityRelation CardHolderOrganizationEntityUsingOrganizationIDStatic = new OrganizationRelations().CardHolderOrganizationEntityUsingOrganizationID;
		internal static readonly IEntityRelation PersonEntityUsingContactPersonIDStatic = new OrganizationRelations().PersonEntityUsingContactPersonID;
		internal static readonly IEntityRelation SubsidyLimitEntityUsingOrganizationIDStatic = new OrganizationRelations().SubsidyLimitEntityUsingOrganizationID;
		internal static readonly IEntityRelation OrganizationEntityUsingOrganizationIDFrameOrganizationIDStatic = new OrganizationRelations().OrganizationEntityUsingOrganizationIDFrameOrganizationID;
		internal static readonly IEntityRelation OrganizationTypeEntityUsingOrganizationTypeIDStatic = new OrganizationRelations().OrganizationTypeEntityUsingOrganizationTypeID;

		/// <summary>CTor</summary>
		static StaticOrganizationRelations()
		{
		}
	}
}
