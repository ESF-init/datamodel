﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ValidationRule. </summary>
	public partial class ValidationRuleRelations
	{
		/// <summary>CTor</summary>
		public ValidationRuleRelations()
		{
		}

		/// <summary>Gets all relations of the ValidationRuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ValidationResultEntityUsingValidationRuleID);
			toReturn.Add(this.ValidationRunRuleEntityUsingValidationRuleID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ValidationRuleEntity and ValidationResultEntity over the 1:n relation they have, using the relation between the fields:
		/// ValidationRule.ValidationRuleID - ValidationResult.ValidationRuleID
		/// </summary>
		public virtual IEntityRelation ValidationResultEntityUsingValidationRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ValidationResults" , true);
				relation.AddEntityFieldPair(ValidationRuleFields.ValidationRuleID, ValidationResultFields.ValidationRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ValidationRuleEntity and ValidationRunRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// ValidationRule.ValidationRuleID - ValidationRunRule.ValidationRuleID
		/// </summary>
		public virtual IEntityRelation ValidationRunRuleEntityUsingValidationRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ValidationRunRules" , true);
				relation.AddEntityFieldPair(ValidationRuleFields.ValidationRuleID, ValidationRunRuleFields.ValidationRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunRuleEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticValidationRuleRelations
	{
		internal static readonly IEntityRelation ValidationResultEntityUsingValidationRuleIDStatic = new ValidationRuleRelations().ValidationResultEntityUsingValidationRuleID;
		internal static readonly IEntityRelation ValidationRunRuleEntityUsingValidationRuleIDStatic = new ValidationRuleRelations().ValidationRunRuleEntityUsingValidationRuleID;

		/// <summary>CTor</summary>
		static StaticValidationRuleRelations()
		{
		}
	}
}
