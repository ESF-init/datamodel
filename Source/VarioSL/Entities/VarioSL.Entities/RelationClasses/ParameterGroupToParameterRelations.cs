﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterGroupToParameter. </summary>
	public partial class ParameterGroupToParameterRelations
	{
		/// <summary>CTor</summary>
		public ParameterGroupToParameterRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterGroupToParameterEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterEntityUsingParameterID);
			toReturn.Add(this.ParameterGroupEntityUsingParameterGroupID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterGroupToParameterEntity and ParameterEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterGroupToParameter.ParameterID - Parameter.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Parameter", false);
				relation.AddEntityFieldPair(ParameterFields.ParameterID, ParameterGroupToParameterFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupToParameterEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterGroupToParameterEntity and ParameterGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterGroupToParameter.ParameterGroupID - ParameterGroup.ParameterGroupID
		/// </summary>
		public virtual IEntityRelation ParameterGroupEntityUsingParameterGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParameterGroup", false);
				relation.AddEntityFieldPair(ParameterGroupFields.ParameterGroupID, ParameterGroupToParameterFields.ParameterGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupToParameterEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterGroupToParameterRelations
	{
		internal static readonly IEntityRelation ParameterEntityUsingParameterIDStatic = new ParameterGroupToParameterRelations().ParameterEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterGroupEntityUsingParameterGroupIDStatic = new ParameterGroupToParameterRelations().ParameterGroupEntityUsingParameterGroupID;

		/// <summary>CTor</summary>
		static StaticParameterGroupToParameterRelations()
		{
		}
	}
}
