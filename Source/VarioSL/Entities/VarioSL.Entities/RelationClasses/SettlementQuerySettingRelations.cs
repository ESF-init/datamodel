﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementQuerySetting. </summary>
	public partial class SettlementQuerySettingRelations
	{
		/// <summary>CTor</summary>
		public SettlementQuerySettingRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementQuerySettingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID);
			toReturn.Add(this.SettlementQueryValueEntityUsingSettlementQuerySettingID);
			toReturn.Add(this.SettlementCalendarEntityUsingSettlementcalendarid);
			toReturn.Add(this.SettlementSetupEntityUsingSettlementSetupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SettlementQuerySettingEntity and SettlementQuerySettingToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementQuerySetting.SettlementQuerySettingID - SettlementQuerySettingToTicket.SettlementQuerySettingID
		/// </summary>
		public virtual IEntityRelation SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementQuerySettingToTickets" , true);
				relation.AddEntityFieldPair(SettlementQuerySettingFields.SettlementQuerySettingID, SettlementQuerySettingToTicketFields.SettlementQuerySettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingToTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SettlementQuerySettingEntity and SettlementQueryValueEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementQuerySetting.SettlementQuerySettingID - SettlementQueryValue.SettlementQuerySettingID
		/// </summary>
		public virtual IEntityRelation SettlementQueryValueEntityUsingSettlementQuerySettingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementQueryValues" , true);
				relation.AddEntityFieldPair(SettlementQuerySettingFields.SettlementQuerySettingID, SettlementQueryValueFields.SettlementQuerySettingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQueryValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SettlementQuerySettingEntity and SettlementCalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementQuerySetting.Settlementcalendarid - SettlementCalendar.SettlementCalendarID
		/// </summary>
		public virtual IEntityRelation SettlementCalendarEntityUsingSettlementcalendarid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SettlementCalendar", false);
				relation.AddEntityFieldPair(SettlementCalendarFields.SettlementCalendarID, SettlementQuerySettingFields.Settlementcalendarid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementCalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SettlementQuerySettingEntity and SettlementSetupEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementQuerySetting.SettlementSetupID - SettlementSetup.SettlementSetupID
		/// </summary>
		public virtual IEntityRelation SettlementSetupEntityUsingSettlementSetupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SettlementSetup", false);
				relation.AddEntityFieldPair(SettlementSetupFields.SettlementSetupID, SettlementQuerySettingFields.SettlementSetupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementQuerySettingRelations
	{
		internal static readonly IEntityRelation SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingIDStatic = new SettlementQuerySettingRelations().SettlementQuerySettingToTicketEntityUsingSettlementQuerySettingID;
		internal static readonly IEntityRelation SettlementQueryValueEntityUsingSettlementQuerySettingIDStatic = new SettlementQuerySettingRelations().SettlementQueryValueEntityUsingSettlementQuerySettingID;
		internal static readonly IEntityRelation SettlementCalendarEntityUsingSettlementcalendaridStatic = new SettlementQuerySettingRelations().SettlementCalendarEntityUsingSettlementcalendarid;
		internal static readonly IEntityRelation SettlementSetupEntityUsingSettlementSetupIDStatic = new SettlementQuerySettingRelations().SettlementSetupEntityUsingSettlementSetupID;

		/// <summary>CTor</summary>
		static StaticSettlementQuerySettingRelations()
		{
		}
	}
}
