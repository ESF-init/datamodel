﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Certificate. </summary>
	public partial class CertificateRelations
	{
		/// <summary>CTor</summary>
		public CertificateRelations()
		{
		}

		/// <summary>Gets all relations of the CertificateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceReaderKeyToCertEntityUsingCertificateID);
			toReturn.Add(this.DeviceReaderToCertEntityUsingCertificateID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CertificateEntity and DeviceReaderKeyToCertEntity over the 1:n relation they have, using the relation between the fields:
		/// Certificate.CertificateID - DeviceReaderKeyToCert.CertificateID
		/// </summary>
		public virtual IEntityRelation DeviceReaderKeyToCertEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderKeyToCerts" , true);
				relation.AddEntityFieldPair(CertificateFields.CertificateID, DeviceReaderKeyToCertFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyToCertEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CertificateEntity and DeviceReaderToCertEntity over the 1:n relation they have, using the relation between the fields:
		/// Certificate.CertificateID - DeviceReaderToCert.CertificateID
		/// </summary>
		public virtual IEntityRelation DeviceReaderToCertEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderToCerts" , true);
				relation.AddEntityFieldPair(CertificateFields.CertificateID, DeviceReaderToCertFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderToCertEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCertificateRelations
	{
		internal static readonly IEntityRelation DeviceReaderKeyToCertEntityUsingCertificateIDStatic = new CertificateRelations().DeviceReaderKeyToCertEntityUsingCertificateID;
		internal static readonly IEntityRelation DeviceReaderToCertEntityUsingCertificateIDStatic = new CertificateRelations().DeviceReaderToCertEntityUsingCertificateID;

		/// <summary>CTor</summary>
		static StaticCertificateRelations()
		{
		}
	}
}
