﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Card. </summary>
	public partial class CardRelations
	{
		/// <summary>CTor</summary>
		public CardRelations()
		{
		}

		/// <summary>Gets all relations of the CardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionmentResultEntityUsingCardID);
			toReturn.Add(this.BadCardEntityUsingCardID);
			toReturn.Add(this.BookingEntityUsingCardId);
			toReturn.Add(this.CardEntityUsingReplacedCardID);
			toReturn.Add(this.CardEventEntityUsingCardID);
			toReturn.Add(this.CardLinkEntityUsingSourceCardID);
			toReturn.Add(this.CardStockTransferEntityUsingCardID);
			toReturn.Add(this.CardToContractEntityUsingCardID);
			toReturn.Add(this.CardToRuleViolationEntityUsingCardID);
			toReturn.Add(this.CardWorkItemEntityUsingCardID);
			toReturn.Add(this.CreditCardAuthorizationEntityUsingCardID);
			toReturn.Add(this.DormancyNotificationEntityUsingCardID);
			toReturn.Add(this.EmailEventResendLockEntityUsingCardID);
			toReturn.Add(this.JobEntityUsingCardID);
			toReturn.Add(this.OrderDetailEntityUsingReplacementCardID);
			toReturn.Add(this.OrganizationParticipantEntityUsingCardID);
			toReturn.Add(this.PaymentJournalEntityUsingCardID);
			toReturn.Add(this.PaymentOptionEntityUsingCardID);
			toReturn.Add(this.ProductEntityUsingCardID);
			toReturn.Add(this.RuleViolationEntityUsingCardID);
			toReturn.Add(this.TicketAssignmentEntityUsingCardID);
			toReturn.Add(this.TransactionJournalEntityUsingTransitAccountID);
			toReturn.Add(this.TransactionJournalEntityUsingTransferredfromcardid);
			toReturn.Add(this.TransactionJournalEntityUsingTokenCardID);
			toReturn.Add(this.VirtualCardEntityUsingCardID);
			toReturn.Add(this.VoucherEntityUsingRedeemedForCardID);
			toReturn.Add(this.WhitelistEntityUsingCardID);
			toReturn.Add(this.WhitelistJournalEntityUsingCardID);
			toReturn.Add(this.CardLinkEntityUsingTargetCardID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.CardEntityUsingCardIDReplacedCardID);
			toReturn.Add(this.CardHolderEntityUsingCardHolderID);
			toReturn.Add(this.CardHolderEntityUsingParticipantID);
			toReturn.Add(this.CardKeyEntityUsingCardKeyID);
			toReturn.Add(this.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.StorageLocationEntityUsingStorageLocationID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardEntity and ApportionmentResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - ApportionmentResult.CardID
		/// </summary>
		public virtual IEntityRelation ApportionmentResultEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApportionmentResults" , true);
				relation.AddEntityFieldPair(CardFields.CardID, ApportionmentResultFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and BadCardEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - BadCard.CardID
		/// </summary>
		public virtual IEntityRelation BadCardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BadCards" , true);
				relation.AddEntityFieldPair(CardFields.CardID, BadCardFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BadCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and BookingEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Booking.CardId
		/// </summary>
		public virtual IEntityRelation BookingEntityUsingCardId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Bookings" , true);
				relation.AddEntityFieldPair(CardFields.CardID, BookingFields.CardId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Card.ReplacedCardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingReplacedCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReplacementCard" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardFields.ReplacedCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardEventEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardEvent.CardID
		/// </summary>
		public virtual IEntityRelation CardEventEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardEvents" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardEventFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEventEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardLinkEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardLink.SourceCardID
		/// </summary>
		public virtual IEntityRelation CardLinkEntityUsingSourceCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardLinksSource" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardLinkFields.SourceCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardLinkEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardStockTransferEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardStockTransfer.CardID
		/// </summary>
		public virtual IEntityRelation CardStockTransferEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardStockTransfers" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardStockTransferFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardStockTransferEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardToContractEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardToContract.CardID
		/// </summary>
		public virtual IEntityRelation CardToContractEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardsToContracts" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardToContractFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardToContractEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardToRuleViolationEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardToRuleViolation.CardID
		/// </summary>
		public virtual IEntityRelation CardToRuleViolationEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardToRuleViolations" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardToRuleViolationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardToRuleViolationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardWorkItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CardWorkItem.CardID
		/// </summary>
		public virtual IEntityRelation CardWorkItemEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItems" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CardWorkItemFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardWorkItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CreditCardAuthorizationEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - CreditCardAuthorization.CardID
		/// </summary>
		public virtual IEntityRelation CreditCardAuthorizationEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CreditCardAuthorizations" , true);
				relation.AddEntityFieldPair(CardFields.CardID, CreditCardAuthorizationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and DormancyNotificationEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - DormancyNotification.CardID
		/// </summary>
		public virtual IEntityRelation DormancyNotificationEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DormancyNotifications" , true);
				relation.AddEntityFieldPair(CardFields.CardID, DormancyNotificationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DormancyNotificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - EmailEventResendLock.CardID
		/// </summary>
		public virtual IEntityRelation EmailEventResendLockEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(CardFields.CardID, EmailEventResendLockFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Job.CardID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(CardFields.CardID, JobFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and OrderDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - OrderDetail.ReplacementCardID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingReplacementCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetails" , true);
				relation.AddEntityFieldPair(CardFields.CardID, OrderDetailFields.ReplacementCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and OrganizationParticipantEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - OrganizationParticipant.CardID
		/// </summary>
		public virtual IEntityRelation OrganizationParticipantEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrganizationParticipants" , true);
				relation.AddEntityFieldPair(CardFields.CardID, OrganizationParticipantFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - PaymentJournal.CardID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(CardFields.CardID, PaymentJournalFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and PaymentOptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - PaymentOption.CardID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentOptions" , true);
				relation.AddEntityFieldPair(CardFields.CardID, PaymentOptionFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Product.CardID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(CardFields.CardID, ProductFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and RuleViolationEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - RuleViolation.CardID
		/// </summary>
		public virtual IEntityRelation RuleViolationEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolations" , true);
				relation.AddEntityFieldPair(CardFields.CardID, RuleViolationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and TicketAssignmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - TicketAssignment.CardID
		/// </summary>
		public virtual IEntityRelation TicketAssignmentEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketAssignments" , true);
				relation.AddEntityFieldPair(CardFields.CardID, TicketAssignmentFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - TransactionJournal.TransitAccountID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransitAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.TransitAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - TransactionJournal.Transferredfromcardid
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransferredfromcardid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals_" , true);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.Transferredfromcardid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - TransactionJournal.TokenCardID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTokenCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TokenTransactionJournals" , true);
				relation.AddEntityFieldPair(CardFields.CardID, TransactionJournalFields.TokenCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and VirtualCardEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - VirtualCard.CardID
		/// </summary>
		public virtual IEntityRelation VirtualCardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VirtualCards" , true);
				relation.AddEntityFieldPair(CardFields.CardID, VirtualCardFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VirtualCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and VoucherEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Voucher.RedeemedForCardID
		/// </summary>
		public virtual IEntityRelation VoucherEntityUsingRedeemedForCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Vouchers" , true);
				relation.AddEntityFieldPair(CardFields.CardID, VoucherFields.RedeemedForCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and WhitelistEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - Whitelist.CardID
		/// </summary>
		public virtual IEntityRelation WhitelistEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Whitelists" , true);
				relation.AddEntityFieldPair(CardFields.CardID, WhitelistFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WhitelistEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and WhitelistJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Card.CardID - WhitelistJournal.CardID
		/// </summary>
		public virtual IEntityRelation WhitelistJournalEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WhitelistJournals" , true);
				relation.AddEntityFieldPair(CardFields.CardID, WhitelistJournalFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WhitelistJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardLinkEntity over the 1:1 relation they have, using the relation between the fields:
		/// Card.CardID - CardLink.TargetCardID
		/// </summary>
		public virtual IEntityRelation CardLinkEntityUsingTargetCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardLinkTarget", true);

				relation.AddEntityFieldPair(CardFields.CardID, CardLinkFields.TargetCardID);



				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardLinkEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, CardFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.ReplacedCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardIDReplacedCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReplacedCard", false);
				relation.AddEntityFieldPair(CardFields.CardID, CardFields.ReplacedCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardHolderEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.CardHolderID - CardHolder.PersonID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardHolder", false);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardHolderEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.ParticipantID - CardHolder.PersonID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingParticipantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Participant", false);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardFields.ParticipantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.CardKeyID - CardKey.CardKeyID
		/// </summary>
		public virtual IEntityRelation CardKeyEntityUsingCardKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardKey", false);
				relation.AddEntityFieldPair(CardKeyFields.CardKeyID, CardFields.CardKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and CardPhysicalDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.CardPhysicalDetailID - CardPhysicalDetail.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardPhysicalDetail", false);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, CardFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardEntity and StorageLocationEntity over the m:1 relation they have, using the relation between the fields:
		/// Card.StorageLocationID - StorageLocation.StorageLocationID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StorageLocation", false);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, CardFields.StorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardRelations
	{
		internal static readonly IEntityRelation ApportionmentResultEntityUsingCardIDStatic = new CardRelations().ApportionmentResultEntityUsingCardID;
		internal static readonly IEntityRelation BadCardEntityUsingCardIDStatic = new CardRelations().BadCardEntityUsingCardID;
		internal static readonly IEntityRelation BookingEntityUsingCardIdStatic = new CardRelations().BookingEntityUsingCardId;
		internal static readonly IEntityRelation CardEntityUsingReplacedCardIDStatic = new CardRelations().CardEntityUsingReplacedCardID;
		internal static readonly IEntityRelation CardEventEntityUsingCardIDStatic = new CardRelations().CardEventEntityUsingCardID;
		internal static readonly IEntityRelation CardLinkEntityUsingSourceCardIDStatic = new CardRelations().CardLinkEntityUsingSourceCardID;
		internal static readonly IEntityRelation CardStockTransferEntityUsingCardIDStatic = new CardRelations().CardStockTransferEntityUsingCardID;
		internal static readonly IEntityRelation CardToContractEntityUsingCardIDStatic = new CardRelations().CardToContractEntityUsingCardID;
		internal static readonly IEntityRelation CardToRuleViolationEntityUsingCardIDStatic = new CardRelations().CardToRuleViolationEntityUsingCardID;
		internal static readonly IEntityRelation CardWorkItemEntityUsingCardIDStatic = new CardRelations().CardWorkItemEntityUsingCardID;
		internal static readonly IEntityRelation CreditCardAuthorizationEntityUsingCardIDStatic = new CardRelations().CreditCardAuthorizationEntityUsingCardID;
		internal static readonly IEntityRelation DormancyNotificationEntityUsingCardIDStatic = new CardRelations().DormancyNotificationEntityUsingCardID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingCardIDStatic = new CardRelations().EmailEventResendLockEntityUsingCardID;
		internal static readonly IEntityRelation JobEntityUsingCardIDStatic = new CardRelations().JobEntityUsingCardID;
		internal static readonly IEntityRelation OrderDetailEntityUsingReplacementCardIDStatic = new CardRelations().OrderDetailEntityUsingReplacementCardID;
		internal static readonly IEntityRelation OrganizationParticipantEntityUsingCardIDStatic = new CardRelations().OrganizationParticipantEntityUsingCardID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingCardIDStatic = new CardRelations().PaymentJournalEntityUsingCardID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingCardIDStatic = new CardRelations().PaymentOptionEntityUsingCardID;
		internal static readonly IEntityRelation ProductEntityUsingCardIDStatic = new CardRelations().ProductEntityUsingCardID;
		internal static readonly IEntityRelation RuleViolationEntityUsingCardIDStatic = new CardRelations().RuleViolationEntityUsingCardID;
		internal static readonly IEntityRelation TicketAssignmentEntityUsingCardIDStatic = new CardRelations().TicketAssignmentEntityUsingCardID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransitAccountIDStatic = new CardRelations().TransactionJournalEntityUsingTransitAccountID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransferredfromcardidStatic = new CardRelations().TransactionJournalEntityUsingTransferredfromcardid;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTokenCardIDStatic = new CardRelations().TransactionJournalEntityUsingTokenCardID;
		internal static readonly IEntityRelation VirtualCardEntityUsingCardIDStatic = new CardRelations().VirtualCardEntityUsingCardID;
		internal static readonly IEntityRelation VoucherEntityUsingRedeemedForCardIDStatic = new CardRelations().VoucherEntityUsingRedeemedForCardID;
		internal static readonly IEntityRelation WhitelistEntityUsingCardIDStatic = new CardRelations().WhitelistEntityUsingCardID;
		internal static readonly IEntityRelation WhitelistJournalEntityUsingCardIDStatic = new CardRelations().WhitelistJournalEntityUsingCardID;
		internal static readonly IEntityRelation CardLinkEntityUsingTargetCardIDStatic = new CardRelations().CardLinkEntityUsingTargetCardID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new CardRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation CardEntityUsingCardIDReplacedCardIDStatic = new CardRelations().CardEntityUsingCardIDReplacedCardID;
		internal static readonly IEntityRelation CardHolderEntityUsingCardHolderIDStatic = new CardRelations().CardHolderEntityUsingCardHolderID;
		internal static readonly IEntityRelation CardHolderEntityUsingParticipantIDStatic = new CardRelations().CardHolderEntityUsingParticipantID;
		internal static readonly IEntityRelation CardKeyEntityUsingCardKeyIDStatic = new CardRelations().CardKeyEntityUsingCardKeyID;
		internal static readonly IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic = new CardRelations().CardPhysicalDetailEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation StorageLocationEntityUsingStorageLocationIDStatic = new CardRelations().StorageLocationEntityUsingStorageLocationID;

		/// <summary>CTor</summary>
		static StaticCardRelations()
		{
		}
	}
}
