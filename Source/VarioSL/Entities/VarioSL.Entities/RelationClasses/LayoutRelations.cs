﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Layout. </summary>
	public partial class LayoutRelations
	{
		/// <summary>CTor</summary>
		public LayoutRelations()
		{
		}

		/// <summary>Gets all relations of the LayoutEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeBitmapLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.AttributeTextLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.ConditionalSubPageLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.ConditionalSubPageLayoutObjectEntityUsingPageID);
			toReturn.Add(this.EticketEntityUsingEtickLayoutID);
			toReturn.Add(this.FixedBitmapLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.FixedTextLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.ListLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.ListLayoutObjectEntityUsingPageID);
			toReturn.Add(this.SpecialReceiptEntityUsingLayoutID);
			toReturn.Add(this.SystemFieldBarcodeLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.SystemFieldTextLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.TicketEntityUsingCancellationLayoutID);
			toReturn.Add(this.TicketEntityUsingGroupLayoutID);
			toReturn.Add(this.TicketEntityUsingPrintLayoutID);
			toReturn.Add(this.TicketEntityUsingReceiptLayoutID);
			toReturn.Add(this.TicketDeviceClassEntityUsingCancellationLayoutID);
			toReturn.Add(this.TicketDeviceClassEntityUsingGroupLayoutID);
			toReturn.Add(this.TicketDeviceClassEntityUsingPrintLayoutID);
			toReturn.Add(this.TicketDeviceClassEntityUsingReceiptLayoutID);
			toReturn.Add(this.ClientEntityUsingOwnerClientID);
			toReturn.Add(this.OutputDeviceEntityUsingOutDeviceID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.FormEntityUsingFormid);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and AttributeBitmapLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - AttributeBitmapLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation AttributeBitmapLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeBitmapLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, AttributeBitmapLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeBitmapLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and AttributeTextLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - AttributeTextLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation AttributeTextLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeTextLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, AttributeTextLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeTextLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and ConditionalSubPageLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - ConditionalSubPageLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation ConditionalSubPageLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CondintionalSubpageLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ConditionalSubPageLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConditionalSubPageLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and ConditionalSubPageLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - ConditionalSubPageLayoutObject.PageID
		/// </summary>
		public virtual IEntityRelation ConditionalSubPageLayoutObjectEntityUsingPageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContintionalSubPageLayoutObjectsPage" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ConditionalSubPageLayoutObjectFields.PageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConditionalSubPageLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and EticketEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - Eticket.EtickLayoutID
		/// </summary>
		public virtual IEntityRelation EticketEntityUsingEtickLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Etickets" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, EticketFields.EtickLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and FixedBitmapLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - FixedBitmapLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation FixedBitmapLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FixedBitmapLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, FixedBitmapLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FixedBitmapLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and FixedTextLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - FixedTextLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation FixedTextLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FixedTextLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, FixedTextLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FixedTextLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and ListLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - ListLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation ListLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ListLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ListLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ListLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and ListLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - ListLayoutObject.PageID
		/// </summary>
		public virtual IEntityRelation ListLayoutObjectEntityUsingPageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ListLayoutObjectsPage" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, ListLayoutObjectFields.PageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ListLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and SpecialReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - SpecialReceipt.LayoutID
		/// </summary>
		public virtual IEntityRelation SpecialReceiptEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SpecialReceipts" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, SpecialReceiptFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SpecialReceiptEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and SystemFieldBarcodeLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - SystemFieldBarcodeLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation SystemFieldBarcodeLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SystemFieldBarcodeLayoutObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, SystemFieldBarcodeLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldBarcodeLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and SystemFieldTextLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - SystemFieldTextLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation SystemFieldTextLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SystemFieldTextLayoutoObjects" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, SystemFieldTextLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldTextLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - Ticket.CancellationLayoutID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingCancellationLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsCancellationLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.CancellationLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - Ticket.GroupLayoutID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingGroupLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsGroupLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.GroupLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - Ticket.PrintLayoutID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingPrintLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsPrintLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.PrintLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - Ticket.ReceiptLayoutID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingReceiptLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsReceiptLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.ReceiptLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - TicketDeviceClass.CancellationLayoutID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingCancellationLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesCancellationLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.CancellationLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - TicketDeviceClass.GroupLayoutID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingGroupLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesGroupLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.GroupLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - TicketDeviceClass.PrintLayoutID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingPrintLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesPrintLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.PrintLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Layout.LayoutID - TicketDeviceClass.ReceiptLayoutID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingReceiptLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesReceiptLayout" , true);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.ReceiptLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Layout.OwnerClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, LayoutFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and OutputDeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// Layout.OutDeviceID - OutputDevice.OutputDeviceID
		/// </summary>
		public virtual IEntityRelation OutputDeviceEntityUsingOutDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OutputDevice", false);
				relation.AddEntityFieldPair(OutputDeviceFields.OutputDeviceID, LayoutFields.OutDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OutputDeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Layout.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, LayoutFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LayoutEntity and FormEntity over the m:1 relation they have, using the relation between the fields:
		/// Layout.Formid - Form.FormID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingFormid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Form", false);
				relation.AddEntityFieldPair(FormFields.FormID, LayoutFields.Formid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLayoutRelations
	{
		internal static readonly IEntityRelation AttributeBitmapLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().AttributeBitmapLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation AttributeTextLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().AttributeTextLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation ConditionalSubPageLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().ConditionalSubPageLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation ConditionalSubPageLayoutObjectEntityUsingPageIDStatic = new LayoutRelations().ConditionalSubPageLayoutObjectEntityUsingPageID;
		internal static readonly IEntityRelation EticketEntityUsingEtickLayoutIDStatic = new LayoutRelations().EticketEntityUsingEtickLayoutID;
		internal static readonly IEntityRelation FixedBitmapLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().FixedBitmapLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation FixedTextLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().FixedTextLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation ListLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().ListLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation ListLayoutObjectEntityUsingPageIDStatic = new LayoutRelations().ListLayoutObjectEntityUsingPageID;
		internal static readonly IEntityRelation SpecialReceiptEntityUsingLayoutIDStatic = new LayoutRelations().SpecialReceiptEntityUsingLayoutID;
		internal static readonly IEntityRelation SystemFieldBarcodeLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().SystemFieldBarcodeLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation SystemFieldTextLayoutObjectEntityUsingLayoutIDStatic = new LayoutRelations().SystemFieldTextLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation TicketEntityUsingCancellationLayoutIDStatic = new LayoutRelations().TicketEntityUsingCancellationLayoutID;
		internal static readonly IEntityRelation TicketEntityUsingGroupLayoutIDStatic = new LayoutRelations().TicketEntityUsingGroupLayoutID;
		internal static readonly IEntityRelation TicketEntityUsingPrintLayoutIDStatic = new LayoutRelations().TicketEntityUsingPrintLayoutID;
		internal static readonly IEntityRelation TicketEntityUsingReceiptLayoutIDStatic = new LayoutRelations().TicketEntityUsingReceiptLayoutID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingCancellationLayoutIDStatic = new LayoutRelations().TicketDeviceClassEntityUsingCancellationLayoutID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingGroupLayoutIDStatic = new LayoutRelations().TicketDeviceClassEntityUsingGroupLayoutID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingPrintLayoutIDStatic = new LayoutRelations().TicketDeviceClassEntityUsingPrintLayoutID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingReceiptLayoutIDStatic = new LayoutRelations().TicketDeviceClassEntityUsingReceiptLayoutID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIDStatic = new LayoutRelations().ClientEntityUsingOwnerClientID;
		internal static readonly IEntityRelation OutputDeviceEntityUsingOutDeviceIDStatic = new LayoutRelations().OutputDeviceEntityUsingOutDeviceID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new LayoutRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation FormEntityUsingFormidStatic = new LayoutRelations().FormEntityUsingFormid;

		/// <summary>CTor</summary>
		static StaticLayoutRelations()
		{
		}
	}
}
