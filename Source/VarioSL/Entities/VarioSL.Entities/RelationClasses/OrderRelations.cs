﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Order. </summary>
	public partial class OrderRelations
	{
		/// <summary>CTor</summary>
		public OrderRelations()
		{
		}

		/// <summary>Gets all relations of the OrderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderDetailEntityUsingOrderID);
			toReturn.Add(this.OrderHistoryEntityUsingOrderID);
			toReturn.Add(this.OrderProcessingCleanupEntityUsingOrderID);
			toReturn.Add(this.OrderWorkItemEntityUsingOrderID);
			toReturn.Add(this.SaleEntityUsingOrderID);
			toReturn.Add(this.UserListEntityUsingAssignee);
			toReturn.Add(this.AddressEntityUsingInvoiceAddressID);
			toReturn.Add(this.AddressEntityUsingShippingAddressID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.SchoolYearEntityUsingSchoolYearID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderID - OrderDetail.OrderID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetails" , true);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderDetailFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderID - OrderHistory.OrderID
		/// </summary>
		public virtual IEntityRelation OrderHistoryEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderHistories" , true);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderHistoryFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderProcessingCleanupEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderID - OrderProcessingCleanup.OrderID
		/// </summary>
		public virtual IEntityRelation OrderProcessingCleanupEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderProcessingCleanups" , true);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderProcessingCleanupFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderProcessingCleanupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and OrderWorkItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Order.OrderID - OrderWorkItem.OrderID
		/// </summary>
		public virtual IEntityRelation OrderWorkItemEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItems" , true);
				relation.AddEntityFieldPair(OrderFields.OrderID, OrderWorkItemFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderWorkItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and SaleEntity over the 1:1 relation they have, using the relation between the fields:
		/// Order.OrderID - Sale.OrderID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Sale", true);


				relation.AddEntityFieldPair(OrderFields.OrderID, SaleFields.OrderID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between OrderEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.Assignee - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AssignedUser", false);
				relation.AddEntityFieldPair(UserListFields.UserID, OrderFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.InvoiceAddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingInvoiceAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InvoiceAddress", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, OrderFields.InvoiceAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.ShippingAddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingShippingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ShippingAddress", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, OrderFields.ShippingAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, OrderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.PaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, OrderFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderEntity and SchoolYearEntity over the m:1 relation they have, using the relation between the fields:
		/// Order.SchoolYearID - SchoolYear.SchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SchoolYear", false);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, OrderFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderRelations
	{
		internal static readonly IEntityRelation OrderDetailEntityUsingOrderIDStatic = new OrderRelations().OrderDetailEntityUsingOrderID;
		internal static readonly IEntityRelation OrderHistoryEntityUsingOrderIDStatic = new OrderRelations().OrderHistoryEntityUsingOrderID;
		internal static readonly IEntityRelation OrderProcessingCleanupEntityUsingOrderIDStatic = new OrderRelations().OrderProcessingCleanupEntityUsingOrderID;
		internal static readonly IEntityRelation OrderWorkItemEntityUsingOrderIDStatic = new OrderRelations().OrderWorkItemEntityUsingOrderID;
		internal static readonly IEntityRelation SaleEntityUsingOrderIDStatic = new OrderRelations().SaleEntityUsingOrderID;
		internal static readonly IEntityRelation UserListEntityUsingAssigneeStatic = new OrderRelations().UserListEntityUsingAssignee;
		internal static readonly IEntityRelation AddressEntityUsingInvoiceAddressIDStatic = new OrderRelations().AddressEntityUsingInvoiceAddressID;
		internal static readonly IEntityRelation AddressEntityUsingShippingAddressIDStatic = new OrderRelations().AddressEntityUsingShippingAddressID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new OrderRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new OrderRelations().PaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation SchoolYearEntityUsingSchoolYearIDStatic = new OrderRelations().SchoolYearEntityUsingSchoolYearID;

		/// <summary>CTor</summary>
		static StaticOrderRelations()
		{
		}
	}
}
