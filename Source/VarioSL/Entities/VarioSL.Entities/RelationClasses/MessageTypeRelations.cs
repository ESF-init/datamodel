﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MessageType. </summary>
	public partial class MessageTypeRelations
	{
		/// <summary>CTor</summary>
		public MessageTypeRelations()
		{
		}

		/// <summary>Gets all relations of the MessageTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomerAccountNotificationEntityUsingMessageTypeID);
			toReturn.Add(this.EmailMessageEntityUsingMessageType);
			toReturn.Add(this.EventSettingEntityUsingMessageTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MessageTypeEntity and CustomerAccountNotificationEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageType.EnumerationValue - CustomerAccountNotification.MessageTypeID
		/// </summary>
		public virtual IEntityRelation CustomerAccountNotificationEntityUsingMessageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccountNotifications" , true);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, CustomerAccountNotificationFields.MessageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessageTypeEntity and EmailMessageEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageType.EnumerationValue - EmailMessage.MessageType
		/// </summary>
		public virtual IEntityRelation EmailMessageEntityUsingMessageType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailMessages" , true);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, EmailMessageFields.MessageType);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailMessageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MessageTypeEntity and EventSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// MessageType.EnumerationValue - EventSetting.MessageTypeID
		/// </summary>
		public virtual IEntityRelation EventSettingEntityUsingMessageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EventSettings" , true);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, EventSettingFields.MessageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EventSettingEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMessageTypeRelations
	{
		internal static readonly IEntityRelation CustomerAccountNotificationEntityUsingMessageTypeIDStatic = new MessageTypeRelations().CustomerAccountNotificationEntityUsingMessageTypeID;
		internal static readonly IEntityRelation EmailMessageEntityUsingMessageTypeStatic = new MessageTypeRelations().EmailMessageEntityUsingMessageType;
		internal static readonly IEntityRelation EventSettingEntityUsingMessageTypeIDStatic = new MessageTypeRelations().EventSettingEntityUsingMessageTypeID;

		/// <summary>CTor</summary>
		static StaticMessageTypeRelations()
		{
		}
	}
}
