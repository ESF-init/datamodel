﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomAttribute. </summary>
	public partial class CustomAttributeRelations
	{
		/// <summary>CTor</summary>
		public CustomAttributeRelations()
		{
		}

		/// <summary>Gets all relations of the CustomAttributeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeToMobilityProviderEntityUsingAttributeID);
			toReturn.Add(this.CustomAttributeValueEntityUsingCustomAttributeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CustomAttributeEntity and AttributeToMobilityProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomAttribute.CustomAttributeID - AttributeToMobilityProvider.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeToMobilityProviderEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeToMobilityProviders" , true);
				relation.AddEntityFieldPair(CustomAttributeFields.CustomAttributeID, AttributeToMobilityProviderFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeToMobilityProviderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomAttributeEntity and CustomAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomAttribute.CustomAttributeID - CustomAttributeValue.CustomAttributeID
		/// </summary>
		public virtual IEntityRelation CustomAttributeValueEntityUsingCustomAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomAttributeValues" , true);
				relation.AddEntityFieldPair(CustomAttributeFields.CustomAttributeID, CustomAttributeValueFields.CustomAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomAttributeRelations
	{
		internal static readonly IEntityRelation AttributeToMobilityProviderEntityUsingAttributeIDStatic = new CustomAttributeRelations().AttributeToMobilityProviderEntityUsingAttributeID;
		internal static readonly IEntityRelation CustomAttributeValueEntityUsingCustomAttributeIDStatic = new CustomAttributeRelations().CustomAttributeValueEntityUsingCustomAttributeID;

		/// <summary>CTor</summary>
		static StaticCustomAttributeRelations()
		{
		}
	}
}
