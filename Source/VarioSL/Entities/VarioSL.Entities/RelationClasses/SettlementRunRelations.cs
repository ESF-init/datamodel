﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementRun. </summary>
	public partial class SettlementRunRelations
	{
		/// <summary>CTor</summary>
		public SettlementRunRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementRunEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SettlementRunValueEntityUsingSettlementRunID);
			toReturn.Add(this.SettlementSetupEntityUsingSettlementSetupID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SettlementRunEntity and SettlementRunValueEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementRun.SettlementRunID - SettlementRunValue.SettlementRunID
		/// </summary>
		public virtual IEntityRelation SettlementRunValueEntityUsingSettlementRunID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementRunValues" , true);
				relation.AddEntityFieldPair(SettlementRunFields.SettlementRunID, SettlementRunValueFields.SettlementRunID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SettlementRunEntity and SettlementSetupEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementRun.SettlementSetupID - SettlementSetup.SettlementSetupID
		/// </summary>
		public virtual IEntityRelation SettlementSetupEntityUsingSettlementSetupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SettlementSetup", false);
				relation.AddEntityFieldPair(SettlementSetupFields.SettlementSetupID, SettlementRunFields.SettlementSetupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementRunRelations
	{
		internal static readonly IEntityRelation SettlementRunValueEntityUsingSettlementRunIDStatic = new SettlementRunRelations().SettlementRunValueEntityUsingSettlementRunID;
		internal static readonly IEntityRelation SettlementSetupEntityUsingSettlementSetupIDStatic = new SettlementRunRelations().SettlementSetupEntityUsingSettlementSetupID;

		/// <summary>CTor</summary>
		static StaticSettlementRunRelations()
		{
		}
	}
}
