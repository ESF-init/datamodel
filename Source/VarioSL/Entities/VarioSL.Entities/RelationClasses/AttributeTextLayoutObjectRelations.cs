﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AttributeTextLayoutObject. </summary>
	public partial class AttributeTextLayoutObjectRelations
	{
		/// <summary>CTor</summary>
		public AttributeTextLayoutObjectRelations()
		{
		}

		/// <summary>Gets all relations of the AttributeTextLayoutObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeEntityUsingAttributeID);
			toReturn.Add(this.LayoutEntityUsingLayoutID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AttributeTextLayoutObjectEntity and AttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeTextLayoutObject.AttributeID - Attribute.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Attribute", false);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, AttributeTextLayoutObjectFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeTextLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttributeTextLayoutObjectEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeTextLayoutObject.LayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Layout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, AttributeTextLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeTextLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttributeTextLayoutObjectRelations
	{
		internal static readonly IEntityRelation AttributeEntityUsingAttributeIDStatic = new AttributeTextLayoutObjectRelations().AttributeEntityUsingAttributeID;
		internal static readonly IEntityRelation LayoutEntityUsingLayoutIDStatic = new AttributeTextLayoutObjectRelations().LayoutEntityUsingLayoutID;

		/// <summary>CTor</summary>
		static StaticAttributeTextLayoutObjectRelations()
		{
		}
	}
}
