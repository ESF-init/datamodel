﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRuleResult. </summary>
	public partial class BusinessRuleResultRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleResultRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleClauseEntityUsingBusinessruleResultID);
			toReturn.Add(this.AttributeValueEntityUsingUserGroupAttributeValue);
			toReturn.Add(this.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.FareStageEntityUsingDestinationFarestageID);
			toReturn.Add(this.KeySelectionModeEntityUsingKeySelectionModeID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and BusinessRuleClauseEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleResult.BusinessRuleResultID - BusinessRuleClause.BusinessruleResultID
		/// </summary>
		public virtual IEntityRelation BusinessRuleClauseEntityUsingBusinessruleResultID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleClause" , true);
				relation.AddEntityFieldPair(BusinessRuleResultFields.BusinessRuleResultID, BusinessRuleClauseFields.BusinessruleResultID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleResult.UserGroupAttributeValue - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingUserGroupAttributeValue
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserGroup", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, BusinessRuleResultFields.UserGroupAttributeValue);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and BusinessRuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleResult.BusinessRuleTypeID - BusinessRuleType.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleType", false);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleResultFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleResult.DestinationFarestageID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingDestinationFarestageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, BusinessRuleResultFields.DestinationFarestageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and KeySelectionModeEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleResult.KeySelectionModeID - KeySelectionMode.KeySelectionModeID
		/// </summary>
		public virtual IEntityRelation KeySelectionModeEntityUsingKeySelectionModeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "KeySelectionMode", false);
				relation.AddEntityFieldPair(KeySelectionModeFields.KeySelectionModeID, BusinessRuleResultFields.KeySelectionModeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeySelectionModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleResultEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRuleResult.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleResultFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleResultRelations
	{
		internal static readonly IEntityRelation BusinessRuleClauseEntityUsingBusinessruleResultIDStatic = new BusinessRuleResultRelations().BusinessRuleClauseEntityUsingBusinessruleResultID;
		internal static readonly IEntityRelation AttributeValueEntityUsingUserGroupAttributeValueStatic = new BusinessRuleResultRelations().AttributeValueEntityUsingUserGroupAttributeValue;
		internal static readonly IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleResultRelations().BusinessRuleTypeEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation FareStageEntityUsingDestinationFarestageIDStatic = new BusinessRuleResultRelations().FareStageEntityUsingDestinationFarestageID;
		internal static readonly IEntityRelation KeySelectionModeEntityUsingKeySelectionModeIDStatic = new BusinessRuleResultRelations().KeySelectionModeEntityUsingKeySelectionModeID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new BusinessRuleResultRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleResultRelations()
		{
		}
	}
}
