﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NotificationMessageOwner. </summary>
	public partial class NotificationMessageOwnerRelations
	{
		/// <summary>CTor</summary>
		public NotificationMessageOwnerRelations()
		{
		}

		/// <summary>Gets all relations of the NotificationMessageOwnerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.NotificationMessageEntityUsingNotificationMessageID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between NotificationMessageOwnerEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// NotificationMessageOwner.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, NotificationMessageOwnerFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageOwnerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NotificationMessageOwnerEntity and NotificationMessageEntity over the m:1 relation they have, using the relation between the fields:
		/// NotificationMessageOwner.NotificationMessageID - NotificationMessage.NotificationMessageID
		/// </summary>
		public virtual IEntityRelation NotificationMessageEntityUsingNotificationMessageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NotificationMessage", false);
				relation.AddEntityFieldPair(NotificationMessageFields.NotificationMessageID, NotificationMessageOwnerFields.NotificationMessageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageOwnerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNotificationMessageOwnerRelations
	{
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new NotificationMessageOwnerRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation NotificationMessageEntityUsingNotificationMessageIDStatic = new NotificationMessageOwnerRelations().NotificationMessageEntityUsingNotificationMessageID;

		/// <summary>CTor</summary>
		static StaticNotificationMessageOwnerRelations()
		{
		}
	}
}
