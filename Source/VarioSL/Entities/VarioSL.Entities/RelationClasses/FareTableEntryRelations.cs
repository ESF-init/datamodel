﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareTableEntry. </summary>
	public partial class FareTableEntryRelations
	{
		/// <summary>CTor</summary>
		public FareTableEntryRelations()
		{
		}

		/// <summary>Gets all relations of the FareTableEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareTableEntryEntityUsingMasterFareTableEntryID);
			toReturn.Add(this.RuleCappingToFtEntryEntityUsingFareTableEntryID);
			toReturn.Add(this.AttributeValueEntityUsingAttributeValueID);
			toReturn.Add(this.FareTableEntityUsingFareTableID);
			toReturn.Add(this.FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareTableEntryEntity and FareTableEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTableEntry.FareTableEntryID - FareTableEntry.MasterFareTableEntryID
		/// </summary>
		public virtual IEntityRelation FareTableEntryEntityUsingMasterFareTableEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTableEntries" , true);
				relation.AddEntityFieldPair(FareTableEntryFields.FareTableEntryID, FareTableEntryFields.MasterFareTableEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareTableEntryEntity and RuleCappingToFtEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTableEntry.FareTableEntryID - RuleCappingToFtEntry.FareTableEntryID
		/// </summary>
		public virtual IEntityRelation RuleCappingToFtEntryEntityUsingFareTableEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCappingToFtEntry" , true);
				relation.AddEntityFieldPair(FareTableEntryFields.FareTableEntryID, RuleCappingToFtEntryFields.FareTableEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingToFtEntryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareTableEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTableEntry.AttributeValueID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareTableEntryFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareTableEntryEntity and FareTableEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTableEntry.FareTableID - FareTable.FareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareTable", false);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, FareTableEntryFields.FareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareTableEntryEntity and FareTableEntryEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTableEntry.MasterFareTableEntryID - FareTableEntry.FareTableEntryID
		/// </summary>
		public virtual IEntityRelation FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareTableEntry", false);
				relation.AddEntityFieldPair(FareTableEntryFields.FareTableEntryID, FareTableEntryFields.MasterFareTableEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareTableEntryRelations
	{
		internal static readonly IEntityRelation FareTableEntryEntityUsingMasterFareTableEntryIDStatic = new FareTableEntryRelations().FareTableEntryEntityUsingMasterFareTableEntryID;
		internal static readonly IEntityRelation RuleCappingToFtEntryEntityUsingFareTableEntryIDStatic = new FareTableEntryRelations().RuleCappingToFtEntryEntityUsingFareTableEntryID;
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeValueIDStatic = new FareTableEntryRelations().AttributeValueEntityUsingAttributeValueID;
		internal static readonly IEntityRelation FareTableEntityUsingFareTableIDStatic = new FareTableEntryRelations().FareTableEntityUsingFareTableID;
		internal static readonly IEntityRelation FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryIDStatic = new FareTableEntryRelations().FareTableEntryEntityUsingFareTableEntryIDMasterFareTableEntryID;

		/// <summary>CTor</summary>
		static StaticFareTableEntryRelations()
		{
		}
	}
}
