﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BankConnectionData. </summary>
	public partial class BankConnectionDataRelations
	{
		/// <summary>CTor</summary>
		public BankConnectionDataRelations()
		{
		}

		/// <summary>Gets all relations of the BankConnectionDataEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CreditScreeningEntityUsingBankconnectionDataID);
			toReturn.Add(this.AddressBookEntryEntityUsingBankConnectionDataID);
			toReturn.Add(this.PaymentOptionEntityUsingBankConnectionDataID);
			toReturn.Add(this.PersonEntityUsingAccountOwnerID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BankConnectionDataEntity and CreditScreeningEntity over the 1:n relation they have, using the relation between the fields:
		/// BankConnectionData.BankConnectionDataID - CreditScreening.BankconnectionDataID
		/// </summary>
		public virtual IEntityRelation CreditScreeningEntityUsingBankconnectionDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CreditScreenings" , true);
				relation.AddEntityFieldPair(BankConnectionDataFields.BankConnectionDataID, CreditScreeningFields.BankconnectionDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditScreeningEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankConnectionDataEntity and AddressBookEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// BankConnectionData.BankConnectionDataID - AddressBookEntry.BankConnectionDataID
		/// </summary>
		public virtual IEntityRelation AddressBookEntryEntityUsingBankConnectionDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AddressBookEntries" , true);
				relation.AddEntityFieldPair(BankConnectionDataFields.BankConnectionDataID, AddressBookEntryFields.BankConnectionDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankConnectionDataEntity and PaymentOptionEntity over the 1:1 relation they have, using the relation between the fields:
		/// BankConnectionData.BankConnectionDataID - PaymentOption.BankConnectionDataID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingBankConnectionDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentOption", true);


				relation.AddEntityFieldPair(BankConnectionDataFields.BankConnectionDataID, PaymentOptionFields.BankConnectionDataID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankConnectionDataEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// BankConnectionData.AccountOwnerID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingAccountOwnerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AccountOwner", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, BankConnectionDataFields.AccountOwnerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBankConnectionDataRelations
	{
		internal static readonly IEntityRelation CreditScreeningEntityUsingBankconnectionDataIDStatic = new BankConnectionDataRelations().CreditScreeningEntityUsingBankconnectionDataID;
		internal static readonly IEntityRelation AddressBookEntryEntityUsingBankConnectionDataIDStatic = new BankConnectionDataRelations().AddressBookEntryEntityUsingBankConnectionDataID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingBankConnectionDataIDStatic = new BankConnectionDataRelations().PaymentOptionEntityUsingBankConnectionDataID;
		internal static readonly IEntityRelation PersonEntityUsingAccountOwnerIDStatic = new BankConnectionDataRelations().PersonEntityUsingAccountOwnerID;

		/// <summary>CTor</summary>
		static StaticBankConnectionDataRelations()
		{
		}
	}
}
