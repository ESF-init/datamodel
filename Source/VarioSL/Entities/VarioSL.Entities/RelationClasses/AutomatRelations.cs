﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Automat. </summary>
	public partial class AutomatRelations
	{
		/// <summary>CTor</summary>
		public AutomatRelations()
		{
		}

		/// <summary>Gets all relations of the AutomatEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceEntityUsingAutomatID);
			toReturn.Add(this.ComponentEntityUsingAutomatID);
			toReturn.Add(this.ComponentClearingEntityUsingAutomatID);
			toReturn.Add(this.ComponentFillingEntityUsingAutomatID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AutomatEntity and CashServiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Automat.AutomatID - CashService.AutomatID
		/// </summary>
		public virtual IEntityRelation CashServiceEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AmCashservices" , true);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, CashServiceFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AutomatEntity and ComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// Automat.AutomatID - Component.AutomatID
		/// </summary>
		public virtual IEntityRelation ComponentEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Components" , true);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, ComponentFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AutomatEntity and ComponentClearingEntity over the 1:n relation they have, using the relation between the fields:
		/// Automat.AutomatID - ComponentClearing.AutomatID
		/// </summary>
		public virtual IEntityRelation ComponentClearingEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentClearings" , true);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, ComponentClearingFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentClearingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AutomatEntity and ComponentFillingEntity over the 1:n relation they have, using the relation between the fields:
		/// Automat.AutomatID - ComponentFilling.AutomatID
		/// </summary>
		public virtual IEntityRelation ComponentFillingEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentFillings" , true);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, ComponentFillingFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAutomatRelations
	{
		internal static readonly IEntityRelation CashServiceEntityUsingAutomatIDStatic = new AutomatRelations().CashServiceEntityUsingAutomatID;
		internal static readonly IEntityRelation ComponentEntityUsingAutomatIDStatic = new AutomatRelations().ComponentEntityUsingAutomatID;
		internal static readonly IEntityRelation ComponentClearingEntityUsingAutomatIDStatic = new AutomatRelations().ComponentClearingEntityUsingAutomatID;
		internal static readonly IEntityRelation ComponentFillingEntityUsingAutomatIDStatic = new AutomatRelations().ComponentFillingEntityUsingAutomatID;

		/// <summary>CTor</summary>
		static StaticAutomatRelations()
		{
		}
	}
}
