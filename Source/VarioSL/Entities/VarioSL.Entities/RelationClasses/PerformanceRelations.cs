﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Performance. </summary>
	public partial class PerformanceRelations
	{
		/// <summary>CTor</summary>
		public PerformanceRelations()
		{
		}

		/// <summary>Gets all relations of the PerformanceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AdditionalDataEntityUsingPerformanceID);
			toReturn.Add(this.PerformanceIndicatorEntityUsingIndicatorID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between PerformanceEntity and AdditionalDataEntity over the 1:1 relation they have, using the relation between the fields:
		/// Performance.PerformanceID - AdditionalData.PerformanceID
		/// </summary>
		public virtual IEntityRelation AdditionalDataEntityUsingPerformanceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "AdditionalData", true);


				relation.AddEntityFieldPair(PerformanceFields.PerformanceID, AdditionalDataFields.PerformanceID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdditionalDataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PerformanceEntity and PerformanceIndicatorEntity over the m:1 relation they have, using the relation between the fields:
		/// Performance.IndicatorID - PerformanceIndicator.IndicatorID
		/// </summary>
		public virtual IEntityRelation PerformanceIndicatorEntityUsingIndicatorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PerformanceIndicator", false);
				relation.AddEntityFieldPair(PerformanceIndicatorFields.IndicatorID, PerformanceFields.IndicatorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceIndicatorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPerformanceRelations
	{
		internal static readonly IEntityRelation AdditionalDataEntityUsingPerformanceIDStatic = new PerformanceRelations().AdditionalDataEntityUsingPerformanceID;
		internal static readonly IEntityRelation PerformanceIndicatorEntityUsingIndicatorIDStatic = new PerformanceRelations().PerformanceIndicatorEntityUsingIndicatorID;

		/// <summary>CTor</summary>
		static StaticPerformanceRelations()
		{
		}
	}
}
