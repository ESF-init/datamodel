﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Panel. </summary>
	public partial class PanelRelations
	{
		/// <summary>CTor</summary>
		public PanelRelations()
		{
		}

		/// <summary>Gets all relations of the PanelEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GuiDefEntityUsingPanelID);
			toReturn.Add(this.PredefinedKeyEntityUsingPanelID);
			toReturn.Add(this.UserKeyEntityUsingDestinationPanelID);
			toReturn.Add(this.UserKeyEntityUsingPanelID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PanelEntity and GuiDefEntity over the 1:n relation they have, using the relation between the fields:
		/// Panel.PanelID - GuiDef.PanelID
		/// </summary>
		public virtual IEntityRelation GuiDefEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GuiDefs" , true);
				relation.AddEntityFieldPair(PanelFields.PanelID, GuiDefFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PanelEntity and PredefinedKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Panel.PanelID - PredefinedKey.PanelID
		/// </summary>
		public virtual IEntityRelation PredefinedKeyEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PredefinedKeys" , true);
				relation.AddEntityFieldPair(PanelFields.PanelID, PredefinedKeyFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PanelEntity and UserKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Panel.PanelID - UserKey.DestinationPanelID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingDestinationPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OriginKeys" , true);
				relation.AddEntityFieldPair(PanelFields.PanelID, UserKeyFields.DestinationPanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PanelEntity and UserKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Panel.PanelID - UserKey.PanelID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserKeys" , true);
				relation.AddEntityFieldPair(PanelFields.PanelID, UserKeyFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PanelEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Panel.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, PanelFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPanelRelations
	{
		internal static readonly IEntityRelation GuiDefEntityUsingPanelIDStatic = new PanelRelations().GuiDefEntityUsingPanelID;
		internal static readonly IEntityRelation PredefinedKeyEntityUsingPanelIDStatic = new PanelRelations().PredefinedKeyEntityUsingPanelID;
		internal static readonly IEntityRelation UserKeyEntityUsingDestinationPanelIDStatic = new PanelRelations().UserKeyEntityUsingDestinationPanelID;
		internal static readonly IEntityRelation UserKeyEntityUsingPanelIDStatic = new PanelRelations().UserKeyEntityUsingPanelID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new PanelRelations().DeviceClassEntityUsingDeviceClassID;

		/// <summary>CTor</summary>
		static StaticPanelRelations()
		{
		}
	}
}
