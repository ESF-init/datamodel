﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Ticket. </summary>
	public partial class TicketRelations
	{
		/// <summary>CTor</summary>
		public TicketRelations()
		{
		}

		/// <summary>Gets all relations of the TicketEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionmentResultEntityUsingTicketID);
			toReturn.Add(this.AttributeValueListEntityUsingTicketID);
			toReturn.Add(this.CardTicketToTicketEntityUsingTicketID);
			toReturn.Add(this.ChoiceEntityUsingTicketID);
			toReturn.Add(this.ParameterTicketEntityUsingTicketID);
			toReturn.Add(this.PrimalKeyEntityUsingTicketID);
			toReturn.Add(this.RuleCappingToTicketEntityUsingTicketID);
			toReturn.Add(this.TicketDayTypeEntityUsingTicketID);
			toReturn.Add(this.TicketDeviceClassEntityUsingTicketID);
			toReturn.Add(this.TicketDevicePaymentMethodEntityUsingTicketID);
			toReturn.Add(this.TicketOrganizationEntityUsingTicketID);
			toReturn.Add(this.TicketOutputdeviceEntityUsingTicketID);
			toReturn.Add(this.TicketPaymentIntervalEntityUsingTicketID);
			toReturn.Add(this.TicketPhysicalCardTypeEntityUsingTicketID);
			toReturn.Add(this.TicketServicesPermittedEntityUsingTicketID);
			toReturn.Add(this.TicketToGroupEntityUsingTicketID);
			toReturn.Add(this.TicketVendingClientEntityUsingTicketid);
			toReturn.Add(this.VdvKeySetEntityUsingTicketID);
			toReturn.Add(this.VdvProductEntityUsingTicketID);
			toReturn.Add(this.ProductEntityUsingTaxTicketID);
			toReturn.Add(this.ProductEntityUsingTicketID);
			toReturn.Add(this.SettlementQuerySettingToTicketEntityUsingTicketID);
			toReturn.Add(this.TicketSerialNumberEntityUsingTicketID);
			toReturn.Add(this.TransactionJournalEntityUsingAppliedPassTicketID);
			toReturn.Add(this.TransactionJournalEntityUsingTicketID);
			toReturn.Add(this.TransactionJournalEntityUsingTaxTicketID);
			toReturn.Add(this.VoucherEntityUsingTicketID);
			toReturn.Add(this.CardTicketEntityUsingTicketID);
			toReturn.Add(this.BusinessRuleEntityUsingBrType10ID);
			toReturn.Add(this.BusinessRuleEntityUsingBrType2ID);
			toReturn.Add(this.BusinessRuleEntityUsingBrType1ID);
			toReturn.Add(this.BusinessRuleEntityUsingBrType9ID);
			toReturn.Add(this.CalendarEntityUsingCalendarID);
			toReturn.Add(this.ClientEntityUsingOwnerClientID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.EticketEntityUsingEtickID);
			toReturn.Add(this.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.FareMatrixEntityUsingMatrixID);
			toReturn.Add(this.FareTableEntityUsingFareTableID);
			toReturn.Add(this.FareTableEntityUsingFareTable2ID);
			toReturn.Add(this.LayoutEntityUsingCancellationLayoutID);
			toReturn.Add(this.LayoutEntityUsingGroupLayoutID);
			toReturn.Add(this.LayoutEntityUsingPrintLayoutID);
			toReturn.Add(this.LayoutEntityUsingReceiptLayoutID);
			toReturn.Add(this.LineGroupEntityUsingLineGroupID);
			toReturn.Add(this.PageContentGroupEntityUsingPageContentGroupID);
			toReturn.Add(this.PriceTypeEntityUsingPriceTypeID);
			toReturn.Add(this.RulePeriodEntityUsingRulePeriodID);
			toReturn.Add(this.TariffEntityUsingTarifID);
			toReturn.Add(this.TemporalTypeEntityUsingTemporalTypeID);
			toReturn.Add(this.TicketCancellationTypeEntityUsingTicketCancellationTypeID);
			toReturn.Add(this.TicketCategoryEntityUsingCategoryID);
			toReturn.Add(this.TicketSelectionModeEntityUsingSelectionMode);
			toReturn.Add(this.TicketTypeEntityUsingTicketTypeID);
			toReturn.Add(this.UserKeyEntityUsingKey1);
			toReturn.Add(this.UserKeyEntityUsingKey2);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ApportionmentResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - ApportionmentResult.TicketID
		/// </summary>
		public virtual IEntityRelation ApportionmentResultEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ApportionmentResults" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, ApportionmentResultFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and AttributeValueListEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - AttributeValueList.TicketID
		/// </summary>
		public virtual IEntityRelation AttributeValueListEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValueList" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, AttributeValueListFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and CardTicketToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - CardTicketToTicket.TicketID
		/// </summary>
		public virtual IEntityRelation CardTicketToTicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardTicketToTicket" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, CardTicketToTicketFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketToTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - Choice.TicketID
		/// </summary>
		public virtual IEntityRelation ChoiceEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Choices" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, ChoiceFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ParameterTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - ParameterTicket.TicketID
		/// </summary>
		public virtual IEntityRelation ParameterTicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterTicket" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, ParameterTicketFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and PrimalKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - PrimalKey.TicketID
		/// </summary>
		public virtual IEntityRelation PrimalKeyEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PrimalKeys" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, PrimalKeyFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and RuleCappingToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - RuleCappingToTicket.TicketID
		/// </summary>
		public virtual IEntityRelation RuleCappingToTicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCappingToTicket" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, RuleCappingToTicketFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingToTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketDayTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketDayType.TicketID
		/// </summary>
		public virtual IEntityRelation TicketDayTypeEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDayTypes" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketDayTypeFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDayTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketDeviceClass.TicketID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClasses" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketDeviceClassFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketDevicePaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketDevicePaymentMethod.TicketID
		/// </summary>
		public virtual IEntityRelation TicketDevicePaymentMethodEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDevicePaymentMethods" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketDevicePaymentMethodFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDevicePaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketOrganizationEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketOrganization.TicketID
		/// </summary>
		public virtual IEntityRelation TicketOrganizationEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketOrganizations" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketOrganizationFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketOutputdeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketOutputdevice.TicketID
		/// </summary>
		public virtual IEntityRelation TicketOutputdeviceEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketOutputDevices" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketOutputdeviceFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketOutputdeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketPaymentIntervalEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketPaymentInterval.TicketID
		/// </summary>
		public virtual IEntityRelation TicketPaymentIntervalEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketPaymentIntervals" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketPaymentIntervalFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPaymentIntervalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketPhysicalCardTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketPhysicalCardType.TicketID
		/// </summary>
		public virtual IEntityRelation TicketPhysicalCardTypeEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketPhysicalCardTypes" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketPhysicalCardTypeFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPhysicalCardTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketServicesPermittedEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketServicesPermitted.TicketID
		/// </summary>
		public virtual IEntityRelation TicketServicesPermittedEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketServicesPermitteds" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketServicesPermittedFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketServicesPermittedEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketToGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketToGroup.TicketID
		/// </summary>
		public virtual IEntityRelation TicketToGroupEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketToGroups" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketToGroupFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketToGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketVendingClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketVendingClient.Ticketid
		/// </summary>
		public virtual IEntityRelation TicketVendingClientEntityUsingTicketid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketVendingClient" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketVendingClientFields.Ticketid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketVendingClientEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and VdvKeySetEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - VdvKeySet.TicketID
		/// </summary>
		public virtual IEntityRelation VdvKeySetEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvKeySet" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, VdvKeySetFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvKeySetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - VdvProduct.TicketID
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProduct" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, VdvProductFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - Product.TaxTicketID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingTaxTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TaxProducts" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, ProductFields.TaxTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - Product.TicketID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, ProductFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and SettlementQuerySettingToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - SettlementQuerySettingToTicket.TicketID
		/// </summary>
		public virtual IEntityRelation SettlementQuerySettingToTicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementQuerySettingToTickets" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, SettlementQuerySettingToTicketFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingToTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketSerialNumberEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TicketSerialNumber.TicketID
		/// </summary>
		public virtual IEntityRelation TicketSerialNumberEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketSerialNumbers" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketSerialNumberFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TransactionJournal.AppliedPassTicketID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingAppliedPassTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournalsForAppliedPassTicket" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.AppliedPassTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TransactionJournal.TicketID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - TransactionJournal.TaxTicketID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTaxTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals_" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, TransactionJournalFields.TaxTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and VoucherEntity over the 1:n relation they have, using the relation between the fields:
		/// Ticket.TicketID - Voucher.TicketID
		/// </summary>
		public virtual IEntityRelation VoucherEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Vouchers" , true);
				relation.AddEntityFieldPair(TicketFields.TicketID, VoucherFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and CardTicketEntity over the 1:1 relation they have, using the relation between the fields:
		/// Ticket.TicketID - CardTicket.TicketID
		/// </summary>
		public virtual IEntityRelation CardTicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardTicketAddition", true);


				relation.AddEntityFieldPair(TicketFields.TicketID, CardTicketFields.TicketID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketEntity and BusinessRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.BrType10ID - BusinessRule.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBrType10ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleInspection", false);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType10ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and BusinessRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.BrType2ID - BusinessRule.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBrType2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleTestBoarding", false);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and BusinessRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.BrType1ID - BusinessRule.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBrType1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRule", false);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and BusinessRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.BrType9ID - BusinessRule.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBrType9ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleTicketAssignment", false);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType9ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.CalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Calendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, TicketFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.OwnerClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketFields.OwnerClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TicketFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and EticketEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.EtickID - Eticket.EtickID
		/// </summary>
		public virtual IEntityRelation EticketEntityUsingEtickID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Eticket", false);
				relation.AddEntityFieldPair(EticketFields.EtickID, TicketFields.EtickID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and FareEvasionCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.FareEvasionCategoryID - FareEvasionCategory.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionCategory", false);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, TicketFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and FareMatrixEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.MatrixID - FareMatrix.FareMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareMatrix", false);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, TicketFields.MatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and FareTableEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.FareTableID - FareTable.FareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareTable1", false);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, TicketFields.FareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and FareTableEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.FareTable2ID - FareTable.FareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingFareTable2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareTable2", false);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, TicketFields.FareTable2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.CancellationLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingCancellationLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CancellationLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.CancellationLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.GroupLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingGroupLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GroupLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.GroupLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.PrintLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingPrintLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PrintLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.PrintLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.ReceiptLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingReceiptLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiptLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketFields.ReceiptLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and LineGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.LineGroupID - LineGroup.LineGroupID
		/// </summary>
		public virtual IEntityRelation LineGroupEntityUsingLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LineGroup", false);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, TicketFields.LineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and PageContentGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.PageContentGroupID - PageContentGroup.PageContentGroupID
		/// </summary>
		public virtual IEntityRelation PageContentGroupEntityUsingPageContentGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PageContentGroup", false);
				relation.AddEntityFieldPair(PageContentGroupFields.PageContentGroupID, TicketFields.PageContentGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and PriceTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.PriceTypeID - PriceType.PriceTypeID
		/// </summary>
		public virtual IEntityRelation PriceTypeEntityUsingPriceTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PriceType", false);
				relation.AddEntityFieldPair(PriceTypeFields.PriceTypeID, TicketFields.PriceTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PriceTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and RulePeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.RulePeriodID - RulePeriod.RulePeriodID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RulePeriod", false);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, TicketFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.TarifID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTarifID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, TicketFields.TarifID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TemporalTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.TemporalTypeID - TemporalType.TemporalTypeID
		/// </summary>
		public virtual IEntityRelation TemporalTypeEntityUsingTemporalTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TemporalType", false);
				relation.AddEntityFieldPair(TemporalTypeFields.TemporalTypeID, TicketFields.TemporalTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TemporalTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketCancellationTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.TicketCancellationTypeID - TicketCancellationType.TicketCancellationTypeID
		/// </summary>
		public virtual IEntityRelation TicketCancellationTypeEntityUsingTicketCancellationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketCancellationType", false);
				relation.AddEntityFieldPair(TicketCancellationTypeFields.TicketCancellationTypeID, TicketFields.TicketCancellationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketCancellationTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.CategoryID - TicketCategory.TicketCategoryID
		/// </summary>
		public virtual IEntityRelation TicketCategoryEntityUsingCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketCategory", false);
				relation.AddEntityFieldPair(TicketCategoryFields.TicketCategoryID, TicketFields.CategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketSelectionModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.SelectionMode - TicketSelectionMode.SelectionModeNumber
		/// </summary>
		public virtual IEntityRelation TicketSelectionModeEntityUsingSelectionMode
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketSelectionMode", false);
				relation.AddEntityFieldPair(TicketSelectionModeFields.SelectionModeNumber, TicketFields.SelectionMode);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSelectionModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and TicketTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.TicketTypeID - TicketType.TicketTypeID
		/// </summary>
		public virtual IEntityRelation TicketTypeEntityUsingTicketTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketType", false);
				relation.AddEntityFieldPair(TicketTypeFields.TicketTypeID, TicketFields.TicketTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and UserKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.Key1 - UserKey.KeyID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingKey1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserKey1", false);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketFields.Key1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketEntity and UserKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// Ticket.Key2 - UserKey.KeyID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingKey2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserKey2", false);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketFields.Key2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketRelations
	{
		internal static readonly IEntityRelation ApportionmentResultEntityUsingTicketIDStatic = new TicketRelations().ApportionmentResultEntityUsingTicketID;
		internal static readonly IEntityRelation AttributeValueListEntityUsingTicketIDStatic = new TicketRelations().AttributeValueListEntityUsingTicketID;
		internal static readonly IEntityRelation CardTicketToTicketEntityUsingTicketIDStatic = new TicketRelations().CardTicketToTicketEntityUsingTicketID;
		internal static readonly IEntityRelation ChoiceEntityUsingTicketIDStatic = new TicketRelations().ChoiceEntityUsingTicketID;
		internal static readonly IEntityRelation ParameterTicketEntityUsingTicketIDStatic = new TicketRelations().ParameterTicketEntityUsingTicketID;
		internal static readonly IEntityRelation PrimalKeyEntityUsingTicketIDStatic = new TicketRelations().PrimalKeyEntityUsingTicketID;
		internal static readonly IEntityRelation RuleCappingToTicketEntityUsingTicketIDStatic = new TicketRelations().RuleCappingToTicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketDayTypeEntityUsingTicketIDStatic = new TicketRelations().TicketDayTypeEntityUsingTicketID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingTicketIDStatic = new TicketRelations().TicketDeviceClassEntityUsingTicketID;
		internal static readonly IEntityRelation TicketDevicePaymentMethodEntityUsingTicketIDStatic = new TicketRelations().TicketDevicePaymentMethodEntityUsingTicketID;
		internal static readonly IEntityRelation TicketOrganizationEntityUsingTicketIDStatic = new TicketRelations().TicketOrganizationEntityUsingTicketID;
		internal static readonly IEntityRelation TicketOutputdeviceEntityUsingTicketIDStatic = new TicketRelations().TicketOutputdeviceEntityUsingTicketID;
		internal static readonly IEntityRelation TicketPaymentIntervalEntityUsingTicketIDStatic = new TicketRelations().TicketPaymentIntervalEntityUsingTicketID;
		internal static readonly IEntityRelation TicketPhysicalCardTypeEntityUsingTicketIDStatic = new TicketRelations().TicketPhysicalCardTypeEntityUsingTicketID;
		internal static readonly IEntityRelation TicketServicesPermittedEntityUsingTicketIDStatic = new TicketRelations().TicketServicesPermittedEntityUsingTicketID;
		internal static readonly IEntityRelation TicketToGroupEntityUsingTicketIDStatic = new TicketRelations().TicketToGroupEntityUsingTicketID;
		internal static readonly IEntityRelation TicketVendingClientEntityUsingTicketidStatic = new TicketRelations().TicketVendingClientEntityUsingTicketid;
		internal static readonly IEntityRelation VdvKeySetEntityUsingTicketIDStatic = new TicketRelations().VdvKeySetEntityUsingTicketID;
		internal static readonly IEntityRelation VdvProductEntityUsingTicketIDStatic = new TicketRelations().VdvProductEntityUsingTicketID;
		internal static readonly IEntityRelation ProductEntityUsingTaxTicketIDStatic = new TicketRelations().ProductEntityUsingTaxTicketID;
		internal static readonly IEntityRelation ProductEntityUsingTicketIDStatic = new TicketRelations().ProductEntityUsingTicketID;
		internal static readonly IEntityRelation SettlementQuerySettingToTicketEntityUsingTicketIDStatic = new TicketRelations().SettlementQuerySettingToTicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketSerialNumberEntityUsingTicketIDStatic = new TicketRelations().TicketSerialNumberEntityUsingTicketID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingAppliedPassTicketIDStatic = new TicketRelations().TransactionJournalEntityUsingAppliedPassTicketID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTicketIDStatic = new TicketRelations().TransactionJournalEntityUsingTicketID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTaxTicketIDStatic = new TicketRelations().TransactionJournalEntityUsingTaxTicketID;
		internal static readonly IEntityRelation VoucherEntityUsingTicketIDStatic = new TicketRelations().VoucherEntityUsingTicketID;
		internal static readonly IEntityRelation CardTicketEntityUsingTicketIDStatic = new TicketRelations().CardTicketEntityUsingTicketID;
		internal static readonly IEntityRelation BusinessRuleEntityUsingBrType10IDStatic = new TicketRelations().BusinessRuleEntityUsingBrType10ID;
		internal static readonly IEntityRelation BusinessRuleEntityUsingBrType2IDStatic = new TicketRelations().BusinessRuleEntityUsingBrType2ID;
		internal static readonly IEntityRelation BusinessRuleEntityUsingBrType1IDStatic = new TicketRelations().BusinessRuleEntityUsingBrType1ID;
		internal static readonly IEntityRelation BusinessRuleEntityUsingBrType9IDStatic = new TicketRelations().BusinessRuleEntityUsingBrType9ID;
		internal static readonly IEntityRelation CalendarEntityUsingCalendarIDStatic = new TicketRelations().CalendarEntityUsingCalendarID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerClientIDStatic = new TicketRelations().ClientEntityUsingOwnerClientID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new TicketRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation EticketEntityUsingEtickIDStatic = new TicketRelations().EticketEntityUsingEtickID;
		internal static readonly IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic = new TicketRelations().FareEvasionCategoryEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation FareMatrixEntityUsingMatrixIDStatic = new TicketRelations().FareMatrixEntityUsingMatrixID;
		internal static readonly IEntityRelation FareTableEntityUsingFareTableIDStatic = new TicketRelations().FareTableEntityUsingFareTableID;
		internal static readonly IEntityRelation FareTableEntityUsingFareTable2IDStatic = new TicketRelations().FareTableEntityUsingFareTable2ID;
		internal static readonly IEntityRelation LayoutEntityUsingCancellationLayoutIDStatic = new TicketRelations().LayoutEntityUsingCancellationLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingGroupLayoutIDStatic = new TicketRelations().LayoutEntityUsingGroupLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingPrintLayoutIDStatic = new TicketRelations().LayoutEntityUsingPrintLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingReceiptLayoutIDStatic = new TicketRelations().LayoutEntityUsingReceiptLayoutID;
		internal static readonly IEntityRelation LineGroupEntityUsingLineGroupIDStatic = new TicketRelations().LineGroupEntityUsingLineGroupID;
		internal static readonly IEntityRelation PageContentGroupEntityUsingPageContentGroupIDStatic = new TicketRelations().PageContentGroupEntityUsingPageContentGroupID;
		internal static readonly IEntityRelation PriceTypeEntityUsingPriceTypeIDStatic = new TicketRelations().PriceTypeEntityUsingPriceTypeID;
		internal static readonly IEntityRelation RulePeriodEntityUsingRulePeriodIDStatic = new TicketRelations().RulePeriodEntityUsingRulePeriodID;
		internal static readonly IEntityRelation TariffEntityUsingTarifIDStatic = new TicketRelations().TariffEntityUsingTarifID;
		internal static readonly IEntityRelation TemporalTypeEntityUsingTemporalTypeIDStatic = new TicketRelations().TemporalTypeEntityUsingTemporalTypeID;
		internal static readonly IEntityRelation TicketCancellationTypeEntityUsingTicketCancellationTypeIDStatic = new TicketRelations().TicketCancellationTypeEntityUsingTicketCancellationTypeID;
		internal static readonly IEntityRelation TicketCategoryEntityUsingCategoryIDStatic = new TicketRelations().TicketCategoryEntityUsingCategoryID;
		internal static readonly IEntityRelation TicketSelectionModeEntityUsingSelectionModeStatic = new TicketRelations().TicketSelectionModeEntityUsingSelectionMode;
		internal static readonly IEntityRelation TicketTypeEntityUsingTicketTypeIDStatic = new TicketRelations().TicketTypeEntityUsingTicketTypeID;
		internal static readonly IEntityRelation UserKeyEntityUsingKey1Static = new TicketRelations().UserKeyEntityUsingKey1;
		internal static readonly IEntityRelation UserKeyEntityUsingKey2Static = new TicketRelations().UserKeyEntityUsingKey2;

		/// <summary>CTor</summary>
		static StaticTicketRelations()
		{
		}
	}
}
