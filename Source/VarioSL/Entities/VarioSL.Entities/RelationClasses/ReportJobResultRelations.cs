﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportJobResult. </summary>
	public partial class ReportJobResultRelations
	{
		/// <summary>CTor</summary>
		public ReportJobResultRelations()
		{
		}

		/// <summary>Gets all relations of the ReportJobResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ReportEntityUsingReportID);
			toReturn.Add(this.ReportJobEntityUsingReportJobID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ReportJobResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJobResult.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportJobResultFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportJobResultEntity and ReportEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJobResult.ReportID - Report.ReportID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Report", false);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportJobResultFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportJobResultEntity and ReportJobEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJobResult.ReportJobID - ReportJob.ReportJobID
		/// </summary>
		public virtual IEntityRelation ReportJobEntityUsingReportJobID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReportJob", false);
				relation.AddEntityFieldPair(ReportJobFields.ReportJobID, ReportJobResultFields.ReportJobID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportJobResultRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ReportJobResultRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ReportEntityUsingReportIDStatic = new ReportJobResultRelations().ReportEntityUsingReportID;
		internal static readonly IEntityRelation ReportJobEntityUsingReportJobIDStatic = new ReportJobResultRelations().ReportJobEntityUsingReportJobID;

		/// <summary>CTor</summary>
		static StaticReportJobResultRelations()
		{
		}
	}
}
