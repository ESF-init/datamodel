﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvLayoutObject. </summary>
	public partial class VdvLayoutObjectRelations
	{
		/// <summary>CTor</summary>
		public VdvLayoutObjectRelations()
		{
		}

		/// <summary>Gets all relations of the VdvLayoutObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvLayoutObjectEntityUsingParentLayoutObjectID);
			toReturn.Add(this.VdvLayoutEntityUsingLayoutID);
			toReturn.Add(this.VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectID);
			toReturn.Add(this.VdvTagEntityUsingVdvTagID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VdvLayoutObjectEntity and VdvLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvLayoutObject.LayoutObjectID - VdvLayoutObject.ParentLayoutObjectID
		/// </summary>
		public virtual IEntityRelation VdvLayoutObjectEntityUsingParentLayoutObjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildrenVdvLayoutObjects" , true);
				relation.AddEntityFieldPair(VdvLayoutObjectFields.LayoutObjectID, VdvLayoutObjectFields.ParentLayoutObjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VdvLayoutObjectEntity and VdvLayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvLayoutObject.LayoutID - VdvLayout.LayoutID
		/// </summary>
		public virtual IEntityRelation VdvLayoutEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VdvLayout", false);
				relation.AddEntityFieldPair(VdvLayoutFields.LayoutID, VdvLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvLayoutObjectEntity and VdvLayoutObjectEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvLayoutObject.ParentLayoutObjectID - VdvLayoutObject.LayoutObjectID
		/// </summary>
		public virtual IEntityRelation VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentVdvLayoutObject", false);
				relation.AddEntityFieldPair(VdvLayoutObjectFields.LayoutObjectID, VdvLayoutObjectFields.ParentLayoutObjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VdvLayoutObjectEntity and VdvTagEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvLayoutObject.VdvTagID - VdvTag.TagID
		/// </summary>
		public virtual IEntityRelation VdvTagEntityUsingVdvTagID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VdvTag", false);
				relation.AddEntityFieldPair(VdvTagFields.TagID, VdvLayoutObjectFields.VdvTagID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTagEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvLayoutObjectRelations
	{
		internal static readonly IEntityRelation VdvLayoutObjectEntityUsingParentLayoutObjectIDStatic = new VdvLayoutObjectRelations().VdvLayoutObjectEntityUsingParentLayoutObjectID;
		internal static readonly IEntityRelation VdvLayoutEntityUsingLayoutIDStatic = new VdvLayoutObjectRelations().VdvLayoutEntityUsingLayoutID;
		internal static readonly IEntityRelation VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectIDStatic = new VdvLayoutObjectRelations().VdvLayoutObjectEntityUsingLayoutObjectIDParentLayoutObjectID;
		internal static readonly IEntityRelation VdvTagEntityUsingVdvTagIDStatic = new VdvLayoutObjectRelations().VdvTagEntityUsingVdvTagID;

		/// <summary>CTor</summary>
		static StaticVdvLayoutObjectRelations()
		{
		}
	}
}
