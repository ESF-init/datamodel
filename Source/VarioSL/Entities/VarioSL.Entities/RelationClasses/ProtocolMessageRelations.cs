﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProtocolMessage. </summary>
	public partial class ProtocolMessageRelations
	{
		/// <summary>CTor</summary>
		public ProtocolMessageRelations()
		{
		}

		/// <summary>Gets all relations of the ProtocolMessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProtocolEntityUsingProtocolID);
			toReturn.Add(this.ProtocolActionEntityUsingActionID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ProtocolMessageEntity and ProtocolEntity over the m:1 relation they have, using the relation between the fields:
		/// ProtocolMessage.ProtocolID - Protocol.ProtocolID
		/// </summary>
		public virtual IEntityRelation ProtocolEntityUsingProtocolID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Protocol", false);
				relation.AddEntityFieldPair(ProtocolFields.ProtocolID, ProtocolMessageFields.ProtocolID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProtocolMessageEntity and ProtocolActionEntity over the m:1 relation they have, using the relation between the fields:
		/// ProtocolMessage.ActionID - ProtocolAction.ProtocolActionID
		/// </summary>
		public virtual IEntityRelation ProtocolActionEntityUsingActionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProtocolAction", false);
				relation.AddEntityFieldPair(ProtocolActionFields.ProtocolActionID, ProtocolMessageFields.ActionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolActionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProtocolMessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProtocolMessageRelations
	{
		internal static readonly IEntityRelation ProtocolEntityUsingProtocolIDStatic = new ProtocolMessageRelations().ProtocolEntityUsingProtocolID;
		internal static readonly IEntityRelation ProtocolActionEntityUsingActionIDStatic = new ProtocolMessageRelations().ProtocolActionEntityUsingActionID;

		/// <summary>CTor</summary>
		static StaticProtocolMessageRelations()
		{
		}
	}
}
