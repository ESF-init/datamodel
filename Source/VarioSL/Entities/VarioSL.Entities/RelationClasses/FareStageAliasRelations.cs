﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareStageAlias. </summary>
	public partial class FareStageAliasRelations
	{
		/// <summary>CTor</summary>
		public FareStageAliasRelations()
		{
		}

		/// <summary>Gets all relations of the FareStageAliasEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareStageEntityUsingFareStageID);
			toReturn.Add(this.FareStageTypeEntityUsingFareStageTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FareStageAliasEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStageAlias.FareStageID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareStageAliasFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageAliasEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareStageAliasEntity and FareStageTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStageAlias.FareStageTypeID - FareStageType.FarestageTypeID
		/// </summary>
		public virtual IEntityRelation FareStageTypeEntityUsingFareStageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageType", false);
				relation.AddEntityFieldPair(FareStageTypeFields.FarestageTypeID, FareStageAliasFields.FareStageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageAliasEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareStageAliasRelations
	{
		internal static readonly IEntityRelation FareStageEntityUsingFareStageIDStatic = new FareStageAliasRelations().FareStageEntityUsingFareStageID;
		internal static readonly IEntityRelation FareStageTypeEntityUsingFareStageTypeIDStatic = new FareStageAliasRelations().FareStageTypeEntityUsingFareStageTypeID;

		/// <summary>CTor</summary>
		static StaticFareStageAliasRelations()
		{
		}
	}
}
