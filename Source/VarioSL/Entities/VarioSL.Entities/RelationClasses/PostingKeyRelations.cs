﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PostingKey. </summary>
	public partial class PostingKeyRelations
	{
		/// <summary>CTor</summary>
		public PostingKeyRelations()
		{
		}

		/// <summary>Gets all relations of the PostingKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningLevelPostingKeyEntityUsingPostingKeyID);
			toReturn.Add(this.PostingEntityUsingPostingKeyID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PostingKeyEntity and DunningLevelPostingKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// PostingKey.PostingKeyID - DunningLevelPostingKey.PostingKeyID
		/// </summary>
		public virtual IEntityRelation DunningLevelPostingKeyEntityUsingPostingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningLevelPostingKeys" , true);
				relation.AddEntityFieldPair(PostingKeyFields.PostingKeyID, DunningLevelPostingKeyFields.PostingKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelPostingKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PostingKeyEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// PostingKey.PostingKeyID - Posting.PostingKeyID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingPostingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(PostingKeyFields.PostingKeyID, PostingFields.PostingKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PostingKeyEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// PostingKey.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, PostingKeyFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPostingKeyRelations
	{
		internal static readonly IEntityRelation DunningLevelPostingKeyEntityUsingPostingKeyIDStatic = new PostingKeyRelations().DunningLevelPostingKeyEntityUsingPostingKeyID;
		internal static readonly IEntityRelation PostingEntityUsingPostingKeyIDStatic = new PostingKeyRelations().PostingEntityUsingPostingKeyID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new PostingKeyRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticPostingKeyRelations()
		{
		}
	}
}
