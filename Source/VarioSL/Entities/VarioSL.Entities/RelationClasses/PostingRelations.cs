﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Posting. </summary>
	public partial class PostingRelations
	{
		/// <summary>CTor</summary>
		public PostingRelations()
		{
		}

		/// <summary>Gets all relations of the PostingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningToPostingEntityUsingPostingID);
			toReturn.Add(this.UserListEntityUsingUserID);
			toReturn.Add(this.VarioSettlementEntityUsingAccountSettlementId);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.InvoiceEntityUsingInvoiceID);
			toReturn.Add(this.InvoiceEntryEntityUsingInvoiceEntryID);
			toReturn.Add(this.PostingKeyEntityUsingPostingKeyID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PostingEntity and DunningToPostingEntity over the 1:n relation they have, using the relation between the fields:
		/// Posting.PostingID - DunningToPosting.PostingID
		/// </summary>
		public virtual IEntityRelation DunningToPostingEntityUsingPostingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToPostings" , true);
				relation.AddEntityFieldPair(PostingFields.PostingID, DunningToPostingFields.PostingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToPostingEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PostingEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, PostingFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and VarioSettlementEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.AccountSettlementId - VarioSettlement.ID
		/// </summary>
		public virtual IEntityRelation VarioSettlementEntityUsingAccountSettlementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VarioSettlement", false);
				relation.AddEntityFieldPair(VarioSettlementFields.ID, PostingFields.AccountSettlementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, PostingFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.InvoiceID - Invoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoice", false);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, PostingFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and InvoiceEntryEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.InvoiceEntryID - InvoiceEntry.InvoiceEntryID
		/// </summary>
		public virtual IEntityRelation InvoiceEntryEntityUsingInvoiceEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "InvoiceEntry", false);
				relation.AddEntityFieldPair(InvoiceEntryFields.InvoiceEntryID, PostingFields.InvoiceEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and PostingKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.PostingKeyID - PostingKey.PostingKeyID
		/// </summary>
		public virtual IEntityRelation PostingKeyEntityUsingPostingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PostingKey", false);
				relation.AddEntityFieldPair(PostingKeyFields.PostingKeyID, PostingFields.PostingKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostingEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Posting.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, PostingFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPostingRelations
	{
		internal static readonly IEntityRelation DunningToPostingEntityUsingPostingIDStatic = new PostingRelations().DunningToPostingEntityUsingPostingID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new PostingRelations().UserListEntityUsingUserID;
		internal static readonly IEntityRelation VarioSettlementEntityUsingAccountSettlementIdStatic = new PostingRelations().VarioSettlementEntityUsingAccountSettlementId;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new PostingRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIDStatic = new PostingRelations().InvoiceEntityUsingInvoiceID;
		internal static readonly IEntityRelation InvoiceEntryEntityUsingInvoiceEntryIDStatic = new PostingRelations().InvoiceEntryEntityUsingInvoiceEntryID;
		internal static readonly IEntityRelation PostingKeyEntityUsingPostingKeyIDStatic = new PostingRelations().PostingKeyEntityUsingPostingKeyID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new PostingRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticPostingRelations()
		{
		}
	}
}
