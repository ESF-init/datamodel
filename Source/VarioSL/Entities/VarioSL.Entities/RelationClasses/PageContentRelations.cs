﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageContent. </summary>
	public partial class PageContentRelations
	{
		/// <summary>CTor</summary>
		public PageContentRelations()
		{
		}

		/// <summary>Gets all relations of the PageContentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PageContentGroupToContentEntityUsingPageContentID);
			toReturn.Add(this.ContentItemEntityUsingPageContentID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageContentEntity and PageContentGroupToContentEntity over the 1:n relation they have, using the relation between the fields:
		/// PageContent.PageContentID - PageContentGroupToContent.PageContentID
		/// </summary>
		public virtual IEntityRelation PageContentGroupToContentEntityUsingPageContentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageContentGroupToContent" , true);
				relation.AddEntityFieldPair(PageContentFields.PageContentID, PageContentGroupToContentFields.PageContentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupToContentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageContentEntity and ContentItemEntity over the 1:n relation they have, using the relation between the fields:
		/// PageContent.PageContentID - ContentItem.PageContentID
		/// </summary>
		public virtual IEntityRelation ContentItemEntityUsingPageContentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContentItems" , true);
				relation.AddEntityFieldPair(PageContentFields.PageContentID, ContentItemFields.PageContentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContentItemEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageContentRelations
	{
		internal static readonly IEntityRelation PageContentGroupToContentEntityUsingPageContentIDStatic = new PageContentRelations().PageContentGroupToContentEntityUsingPageContentID;
		internal static readonly IEntityRelation ContentItemEntityUsingPageContentIDStatic = new PageContentRelations().ContentItemEntityUsingPageContentID;

		/// <summary>CTor</summary>
		static StaticPageContentRelations()
		{
		}
	}
}
