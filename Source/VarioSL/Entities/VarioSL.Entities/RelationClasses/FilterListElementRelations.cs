﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FilterListElement. </summary>
	public partial class FilterListElementRelations
	{
		/// <summary>CTor</summary>
		public FilterListElementRelations()
		{
		}

		/// <summary>Gets all relations of the FilterListElementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FilterValueEntityUsingFilterListElementID);
			toReturn.Add(this.FilterCategoryEntityUsingFilterCategoryID);
			toReturn.Add(this.FilterElementEntityUsingFilterElementID);
			toReturn.Add(this.FilterListEntityUsingFilterListID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FilterListElementEntity and FilterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterListElement.FilterListElementID - FilterValue.FilterListElementID
		/// </summary>
		public virtual IEntityRelation FilterValueEntityUsingFilterListElementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValues" , true);
				relation.AddEntityFieldPair(FilterListElementFields.FilterListElementID, FilterValueFields.FilterListElementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FilterListElementEntity and FilterCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterListElement.FilterCategoryID - FilterCategory.FilterCategoryID
		/// </summary>
		public virtual IEntityRelation FilterCategoryEntityUsingFilterCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterCategory", false);
				relation.AddEntityFieldPair(FilterCategoryFields.FilterCategoryID, FilterListElementFields.FilterCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterListElementEntity and FilterElementEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterListElement.FilterElementID - FilterElement.FilterElementID
		/// </summary>
		public virtual IEntityRelation FilterElementEntityUsingFilterElementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterElement", false);
				relation.AddEntityFieldPair(FilterElementFields.FilterElementID, FilterListElementFields.FilterElementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterListElementEntity and FilterListEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterListElement.FilterListID - FilterList.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterListEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterList", false);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, FilterListElementFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFilterListElementRelations
	{
		internal static readonly IEntityRelation FilterValueEntityUsingFilterListElementIDStatic = new FilterListElementRelations().FilterValueEntityUsingFilterListElementID;
		internal static readonly IEntityRelation FilterCategoryEntityUsingFilterCategoryIDStatic = new FilterListElementRelations().FilterCategoryEntityUsingFilterCategoryID;
		internal static readonly IEntityRelation FilterElementEntityUsingFilterElementIDStatic = new FilterListElementRelations().FilterElementEntityUsingFilterElementID;
		internal static readonly IEntityRelation FilterListEntityUsingFilterListIDStatic = new FilterListElementRelations().FilterListEntityUsingFilterListID;

		/// <summary>CTor</summary>
		static StaticFilterListElementRelations()
		{
		}
	}
}
