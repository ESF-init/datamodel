﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareEvasionReasonToCategory. </summary>
	public partial class FareEvasionReasonToCategoryRelations
	{
		/// <summary>CTor</summary>
		public FareEvasionReasonToCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the FareEvasionReasonToCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareEvasionCategoryEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.FareEvasionReasonEntityUsingFareEvasionReasonID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FareEvasionReasonToCategoryEntity and FareEvasionCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionReasonToCategory.FareEvasionCategoryID - FareEvasionCategory.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionCategory", false);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, FareEvasionReasonToCategoryFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonToCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareEvasionReasonToCategoryEntity and FareEvasionReasonEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionReasonToCategory.FareEvasionReasonID - FareEvasionReason.FareEvasionReasonID
		/// </summary>
		public virtual IEntityRelation FareEvasionReasonEntityUsingFareEvasionReasonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionReason", false);
				relation.AddEntityFieldPair(FareEvasionReasonFields.FareEvasionReasonID, FareEvasionReasonToCategoryFields.FareEvasionReasonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonToCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareEvasionReasonToCategoryRelations
	{
		internal static readonly IEntityRelation FareEvasionCategoryEntityUsingFareEvasionCategoryIDStatic = new FareEvasionReasonToCategoryRelations().FareEvasionCategoryEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation FareEvasionReasonEntityUsingFareEvasionReasonIDStatic = new FareEvasionReasonToCategoryRelations().FareEvasionReasonEntityUsingFareEvasionReasonID;

		/// <summary>CTor</summary>
		static StaticFareEvasionReasonToCategoryRelations()
		{
		}
	}
}
