﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceReaderCryptoResource. </summary>
	public partial class DeviceReaderCryptoResourceRelations
	{
		/// <summary>CTor</summary>
		public DeviceReaderCryptoResourceRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceReaderCryptoResourceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CryptoResourceDataEntityUsingCryptoResourceDataID);
			toReturn.Add(this.DeviceReaderEntityUsingDeviceReaderID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeviceReaderCryptoResourceEntity and CryptoResourceDataEntity over the m:1 relation they have, using the relation between the fields:
		/// DeviceReaderCryptoResource.CryptoResourceDataID - CryptoResourceData.CryptoResourceDataID
		/// </summary>
		public virtual IEntityRelation CryptoResourceDataEntityUsingCryptoResourceDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoResourceData", false);
				relation.AddEntityFieldPair(CryptoResourceDataFields.CryptoResourceDataID, DeviceReaderCryptoResourceFields.CryptoResourceDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceDataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderCryptoResourceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeviceReaderCryptoResourceEntity and DeviceReaderEntity over the m:1 relation they have, using the relation between the fields:
		/// DeviceReaderCryptoResource.DeviceReaderID - DeviceReader.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceReader", false);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderCryptoResourceFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderCryptoResourceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceReaderCryptoResourceRelations
	{
		internal static readonly IEntityRelation CryptoResourceDataEntityUsingCryptoResourceDataIDStatic = new DeviceReaderCryptoResourceRelations().CryptoResourceDataEntityUsingCryptoResourceDataID;
		internal static readonly IEntityRelation DeviceReaderEntityUsingDeviceReaderIDStatic = new DeviceReaderCryptoResourceRelations().DeviceReaderEntityUsingDeviceReaderID;

		/// <summary>CTor</summary>
		static StaticDeviceReaderCryptoResourceRelations()
		{
		}
	}
}
