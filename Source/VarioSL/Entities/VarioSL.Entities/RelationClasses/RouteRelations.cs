﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Route. </summary>
	public partial class RouteRelations
	{
		/// <summary>CTor</summary>
		public RouteRelations()
		{
		}

		/// <summary>Gets all relations of the RouteEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingTariffAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingTicketGroupAttributeID);
			toReturn.Add(this.PrintTextEntityUsingPrintText1ID);
			toReturn.Add(this.PrintTextEntityUsingPrintText2ID);
			toReturn.Add(this.PrintTextEntityUsingPrintText3ID);
			toReturn.Add(this.RouteNameEntityUsingRouteNameID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RouteEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.TariffAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingTariffAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TariffAttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, RouteFields.TariffAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.TicketGroupAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingTicketGroupAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketGroupAttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, RouteFields.TicketGroupAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and PrintTextEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.PrintText1ID - PrintText.PrintTextID
		/// </summary>
		public virtual IEntityRelation PrintTextEntityUsingPrintText1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PrintText1", false);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and PrintTextEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.PrintText2ID - PrintText.PrintTextID
		/// </summary>
		public virtual IEntityRelation PrintTextEntityUsingPrintText2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PrintText2", false);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and PrintTextEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.PrintText3ID - PrintText.PrintTextID
		/// </summary>
		public virtual IEntityRelation PrintTextEntityUsingPrintText3ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PrintText3", false);
				relation.AddEntityFieldPair(PrintTextFields.PrintTextID, RouteFields.PrintText3ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrintTextEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and RouteNameEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.RouteNameID - RouteName.RouteNameid
		/// </summary>
		public virtual IEntityRelation RouteNameEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteName", false);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, RouteFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RouteEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Route.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, RouteFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRouteRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingTariffAttributeIDStatic = new RouteRelations().AttributeValueEntityUsingTariffAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingTicketGroupAttributeIDStatic = new RouteRelations().AttributeValueEntityUsingTicketGroupAttributeID;
		internal static readonly IEntityRelation PrintTextEntityUsingPrintText1IDStatic = new RouteRelations().PrintTextEntityUsingPrintText1ID;
		internal static readonly IEntityRelation PrintTextEntityUsingPrintText2IDStatic = new RouteRelations().PrintTextEntityUsingPrintText2ID;
		internal static readonly IEntityRelation PrintTextEntityUsingPrintText3IDStatic = new RouteRelations().PrintTextEntityUsingPrintText3ID;
		internal static readonly IEntityRelation RouteNameEntityUsingRouteNameIDStatic = new RouteRelations().RouteNameEntityUsingRouteNameID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new RouteRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticRouteRelations()
		{
		}
	}
}
