﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRule. </summary>
	public partial class BusinessRuleRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleClauseEntityUsingBusinessRuleID);
			toReturn.Add(this.TicketEntityUsingBrType10ID);
			toReturn.Add(this.TicketEntityUsingBrType2ID);
			toReturn.Add(this.TicketEntityUsingBrType1ID);
			toReturn.Add(this.TicketEntityUsingBrType9ID);
			toReturn.Add(this.BusinessRuleTypeEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and BusinessRuleClauseEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleID - BusinessRuleClause.BusinessRuleID
		/// </summary>
		public virtual IEntityRelation BusinessRuleClauseEntityUsingBusinessRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleClause" , true);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, BusinessRuleClauseFields.BusinessRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleClauseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleID - Ticket.BrType10ID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingBrType10ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsInspection" , true);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType10ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleID - Ticket.BrType2ID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingBrType2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsTestBoarding" , true);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleID - Ticket.BrType1ID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingBrType1ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsTestSale" , true);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType1ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleID - Ticket.BrType9ID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingBrType9ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsTicketAssignment" , true);
				relation.AddEntityFieldPair(BusinessRuleFields.BusinessRuleID, TicketFields.BrType9ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and BusinessRuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRule.BusinessRuleTypeID - BusinessRuleType.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BusinessRuleType", false);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BusinessRuleEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// BusinessRule.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, BusinessRuleFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleRelations
	{
		internal static readonly IEntityRelation BusinessRuleClauseEntityUsingBusinessRuleIDStatic = new BusinessRuleRelations().BusinessRuleClauseEntityUsingBusinessRuleID;
		internal static readonly IEntityRelation TicketEntityUsingBrType10IDStatic = new BusinessRuleRelations().TicketEntityUsingBrType10ID;
		internal static readonly IEntityRelation TicketEntityUsingBrType2IDStatic = new BusinessRuleRelations().TicketEntityUsingBrType2ID;
		internal static readonly IEntityRelation TicketEntityUsingBrType1IDStatic = new BusinessRuleRelations().TicketEntityUsingBrType1ID;
		internal static readonly IEntityRelation TicketEntityUsingBrType9IDStatic = new BusinessRuleRelations().TicketEntityUsingBrType9ID;
		internal static readonly IEntityRelation BusinessRuleTypeEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleRelations().BusinessRuleTypeEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new BusinessRuleRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleRelations()
		{
		}
	}
}
