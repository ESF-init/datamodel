﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Net. </summary>
	public partial class NetRelations
	{
		/// <summary>CTor</summary>
		public NetRelations()
		{
		}

		/// <summary>Gets all relations of the NetEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LineEntityUsingNetID);
			toReturn.Add(this.StopEntityUsingNetId);
			toReturn.Add(this.TariffEntityUsingNetID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between NetEntity and LineEntity over the 1:n relation they have, using the relation between the fields:
		/// Net.NetID - Line.NetID
		/// </summary>
		public virtual IEntityRelation LineEntityUsingNetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Lines" , true);
				relation.AddEntityFieldPair(NetFields.NetID, LineFields.NetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NetEntity and StopEntity over the 1:n relation they have, using the relation between the fields:
		/// Net.NetID - Stop.NetId
		/// </summary>
		public virtual IEntityRelation StopEntityUsingNetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Stops" , true);
				relation.AddEntityFieldPair(NetFields.NetID, StopFields.NetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StopEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NetEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// Net.NetID - Tariff.NetID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingNetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tariffs" , true);
				relation.AddEntityFieldPair(NetFields.NetID, TariffFields.NetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNetRelations
	{
		internal static readonly IEntityRelation LineEntityUsingNetIDStatic = new NetRelations().LineEntityUsingNetID;
		internal static readonly IEntityRelation StopEntityUsingNetIdStatic = new NetRelations().StopEntityUsingNetId;
		internal static readonly IEntityRelation TariffEntityUsingNetIDStatic = new NetRelations().TariffEntityUsingNetID;

		/// <summary>CTor</summary>
		static StaticNetRelations()
		{
		}
	}
}
