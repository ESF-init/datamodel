﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Job. </summary>
	public partial class JobRelations
	{
		/// <summary>CTor</summary>
		public JobRelations()
		{
		}

		/// <summary>Gets all relations of the JobEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.JobCardImportEntityUsingJobID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.InvoicingEntityUsingInvoicingID);
			toReturn.Add(this.JobBulkImportEntityUsingBulkImportID);
			toReturn.Add(this.MessageEntityUsingMessageID);
			toReturn.Add(this.OrderDetailEntityUsingOrderDetailID);
			toReturn.Add(this.PrinterEntityUsingPrinterID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between JobEntity and JobCardImportEntity over the 1:n relation they have, using the relation between the fields:
		/// Job.JobID - JobCardImport.JobID
		/// </summary>
		public virtual IEntityRelation JobCardImportEntityUsingJobID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "JobCardImports" , true);
				relation.AddEntityFieldPair(JobFields.JobID, JobCardImportFields.JobID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobCardImportEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between JobEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, JobFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, JobFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, JobFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and InvoicingEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.InvoicingID - Invoicing.InvoicingID
		/// </summary>
		public virtual IEntityRelation InvoicingEntityUsingInvoicingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoicing", false);
				relation.AddEntityFieldPair(InvoicingFields.InvoicingID, JobFields.InvoicingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and JobBulkImportEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.BulkImportID - JobBulkImport.BulkImportID
		/// </summary>
		public virtual IEntityRelation JobBulkImportEntityUsingBulkImportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "JobBulkImport", false);
				relation.AddEntityFieldPair(JobBulkImportFields.BulkImportID, JobFields.BulkImportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobBulkImportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and MessageEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.MessageID - Message.MessageID
		/// </summary>
		public virtual IEntityRelation MessageEntityUsingMessageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Message", false);
				relation.AddEntityFieldPair(MessageFields.MessageID, JobFields.MessageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and OrderDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.OrderDetailID - OrderDetail.OrderDetailID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderDetail", false);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, JobFields.OrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between JobEntity and PrinterEntity over the m:1 relation they have, using the relation between the fields:
		/// Job.PrinterID - Printer.PrinterID
		/// </summary>
		public virtual IEntityRelation PrinterEntityUsingPrinterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Printer", false);
				relation.AddEntityFieldPair(PrinterFields.PrinterID, JobFields.PrinterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrinterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticJobRelations
	{
		internal static readonly IEntityRelation JobCardImportEntityUsingJobIDStatic = new JobRelations().JobCardImportEntityUsingJobID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new JobRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new JobRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new JobRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation InvoicingEntityUsingInvoicingIDStatic = new JobRelations().InvoicingEntityUsingInvoicingID;
		internal static readonly IEntityRelation JobBulkImportEntityUsingBulkImportIDStatic = new JobRelations().JobBulkImportEntityUsingBulkImportID;
		internal static readonly IEntityRelation MessageEntityUsingMessageIDStatic = new JobRelations().MessageEntityUsingMessageID;
		internal static readonly IEntityRelation OrderDetailEntityUsingOrderDetailIDStatic = new JobRelations().OrderDetailEntityUsingOrderDetailID;
		internal static readonly IEntityRelation PrinterEntityUsingPrinterIDStatic = new JobRelations().PrinterEntityUsingPrinterID;

		/// <summary>CTor</summary>
		static StaticJobRelations()
		{
		}
	}
}
