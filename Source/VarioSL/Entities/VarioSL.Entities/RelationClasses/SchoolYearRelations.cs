﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SchoolYear. </summary>
	public partial class SchoolYearRelations
	{
		/// <summary>CTor</summary>
		public SchoolYearRelations()
		{
		}

		/// <summary>Gets all relations of the SchoolYearEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardHolderEntityUsingSchoolYearID);
			toReturn.Add(this.InvoiceEntityUsingSchoolYearID);
			toReturn.Add(this.OrderEntityUsingSchoolYearID);
			toReturn.Add(this.OrderDetailEntityUsingSchoolyearID);
			toReturn.Add(this.SchoolYearEntityUsingSchoolYearID);
			toReturn.Add(this.SchoolYearEntityUsingPreviousSchoolYearID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and CardHolderEntity over the 1:n relation they have, using the relation between the fields:
		/// SchoolYear.SchoolYearID - CardHolder.SchoolYearID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardHolder" , true);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, CardHolderFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// SchoolYear.SchoolYearID - Invoice.SchoolYearID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, InvoiceFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// SchoolYear.SchoolYearID - Order.SchoolYearID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Orders" , true);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, OrderFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and OrderDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// SchoolYear.SchoolYearID - OrderDetail.SchoolyearID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingSchoolyearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetails" , true);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, OrderDetailFields.SchoolyearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and SchoolYearEntity over the 1:1 relation they have, using the relation between the fields:
		/// SchoolYear.PreviousSchoolYearID - SchoolYear.SchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "FollowingSchoolYear", false);




				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, SchoolYearFields.PreviousSchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and SchoolYearEntity over the 1:1 relation they have, using the relation between the fields:
		/// SchoolYear.SchoolYearID - SchoolYear.PreviousSchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingPreviousSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PreviousSchoolYear", true);


				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, SchoolYearFields.PreviousSchoolYearID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SchoolYearEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// SchoolYear.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, SchoolYearFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSchoolYearRelations
	{
		internal static readonly IEntityRelation CardHolderEntityUsingSchoolYearIDStatic = new SchoolYearRelations().CardHolderEntityUsingSchoolYearID;
		internal static readonly IEntityRelation InvoiceEntityUsingSchoolYearIDStatic = new SchoolYearRelations().InvoiceEntityUsingSchoolYearID;
		internal static readonly IEntityRelation OrderEntityUsingSchoolYearIDStatic = new SchoolYearRelations().OrderEntityUsingSchoolYearID;
		internal static readonly IEntityRelation OrderDetailEntityUsingSchoolyearIDStatic = new SchoolYearRelations().OrderDetailEntityUsingSchoolyearID;
		internal static readonly IEntityRelation SchoolYearEntityUsingSchoolYearIDStatic = new SchoolYearRelations().SchoolYearEntityUsingSchoolYearID;
		internal static readonly IEntityRelation SchoolYearEntityUsingPreviousSchoolYearIDStatic = new SchoolYearRelations().SchoolYearEntityUsingPreviousSchoolYearID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new SchoolYearRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticSchoolYearRelations()
		{
		}
	}
}
