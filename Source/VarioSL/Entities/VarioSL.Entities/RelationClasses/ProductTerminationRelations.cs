﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductTermination. </summary>
	public partial class ProductTerminationRelations
	{
		/// <summary>CTor</summary>
		public ProductTerminationRelations()
		{
		}

		/// <summary>Gets all relations of the ProductTerminationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserListEntityUsingEmployeeID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.InvoiceEntityUsingInvoiceID);
			toReturn.Add(this.ProductEntityUsingProductID);
			toReturn.Add(this.ProductTerminationTypeEntityUsingProductTerminationTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ProductTerminationEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductTermination.EmployeeID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingEmployeeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, ProductTerminationFields.EmployeeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductTerminationEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductTermination.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ProductTerminationFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductTerminationEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductTermination.InvoiceID - Invoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoice", false);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, ProductTerminationFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductTerminationEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductTermination.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, ProductTerminationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductTerminationEntity and ProductTerminationTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductTermination.ProductTerminationTypeID - ProductTerminationType.ProductTerminationTypeID
		/// </summary>
		public virtual IEntityRelation ProductTerminationTypeEntityUsingProductTerminationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ProductTerminationType", false);
				relation.AddEntityFieldPair(ProductTerminationTypeFields.ProductTerminationTypeID, ProductTerminationFields.ProductTerminationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductTerminationRelations
	{
		internal static readonly IEntityRelation UserListEntityUsingEmployeeIDStatic = new ProductTerminationRelations().UserListEntityUsingEmployeeID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ProductTerminationRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIDStatic = new ProductTerminationRelations().InvoiceEntityUsingInvoiceID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new ProductTerminationRelations().ProductEntityUsingProductID;
		internal static readonly IEntityRelation ProductTerminationTypeEntityUsingProductTerminationTypeIDStatic = new ProductTerminationRelations().ProductTerminationTypeEntityUsingProductTerminationTypeID;

		/// <summary>CTor</summary>
		static StaticProductTerminationRelations()
		{
		}
	}
}
