﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketCategory. </summary>
	public partial class TicketCategoryRelations
	{
		/// <summary>CTor</summary>
		public TicketCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the TicketCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketEntityUsingCategoryID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TicketCategoryEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketCategory.TicketCategoryID - Ticket.CategoryID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(TicketCategoryFields.TicketCategoryID, TicketFields.CategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TicketCategoryEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketCategory.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TicketCategoryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketCategoryRelations
	{
		internal static readonly IEntityRelation TicketEntityUsingCategoryIDStatic = new TicketCategoryRelations().TicketEntityUsingCategoryID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new TicketCategoryRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticTicketCategoryRelations()
		{
		}
	}
}
