﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CashServiceData. </summary>
	public partial class CashServiceDataRelations
	{
		/// <summary>CTor</summary>
		public CashServiceDataRelations()
		{
		}

		/// <summary>Gets all relations of the CashServiceDataEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceEntityUsingCashServiceID);
			toReturn.Add(this.CashUnitEntityUsingCashUnitID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CashServiceDataEntity and CashServiceEntity over the m:1 relation they have, using the relation between the fields:
		/// CashServiceData.CashServiceID - CashService.CashServiceID
		/// </summary>
		public virtual IEntityRelation CashServiceEntityUsingCashServiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CashService", false);
				relation.AddEntityFieldPair(CashServiceFields.CashServiceID, CashServiceDataFields.CashServiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceDataEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CashServiceDataEntity and CashUnitEntity over the m:1 relation they have, using the relation between the fields:
		/// CashServiceData.CashUnitID - CashUnit.CashUnitID
		/// </summary>
		public virtual IEntityRelation CashUnitEntityUsingCashUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CashUnit", false);
				relation.AddEntityFieldPair(CashUnitFields.CashUnitID, CashServiceDataFields.CashUnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashUnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceDataEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCashServiceDataRelations
	{
		internal static readonly IEntityRelation CashServiceEntityUsingCashServiceIDStatic = new CashServiceDataRelations().CashServiceEntityUsingCashServiceID;
		internal static readonly IEntityRelation CashUnitEntityUsingCashUnitIDStatic = new CashServiceDataRelations().CashUnitEntityUsingCashUnitID;

		/// <summary>CTor</summary>
		static StaticCashServiceDataRelations()
		{
		}
	}
}
