﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterValue. </summary>
	public partial class ParameterValueRelations
	{
		/// <summary>CTor</summary>
		public ParameterValueRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.UnitEntityUsingUnitID);
			toReturn.Add(this.ParameterEntityUsingParameterID);
			toReturn.Add(this.ParameterArchiveEntityUsingParameterArchiveID);
			toReturn.Add(this.UnitCollectionEntityUsingUnitCollectionID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterValueEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterValue.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterValueFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterValueEntity and UnitEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterValue.UnitID - Unit.UnitID
		/// </summary>
		public virtual IEntityRelation UnitEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Unit", false);
				relation.AddEntityFieldPair(UnitFields.UnitID, ParameterValueFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterValueEntity and ParameterEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterValue.ParameterID - Parameter.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Parameter", false);
				relation.AddEntityFieldPair(ParameterFields.ParameterID, ParameterValueFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterValueEntity and ParameterArchiveEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterValue.ParameterArchiveID - ParameterArchive.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParameterArchive", false);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, ParameterValueFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterValueEntity and UnitCollectionEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterValue.UnitCollectionID - UnitCollection.UnitCollectionID
		/// </summary>
		public virtual IEntityRelation UnitCollectionEntityUsingUnitCollectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UnitCollection", false);
				relation.AddEntityFieldPair(UnitCollectionFields.UnitCollectionID, ParameterValueFields.UnitCollectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterValueRelations
	{
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new ParameterValueRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation UnitEntityUsingUnitIDStatic = new ParameterValueRelations().UnitEntityUsingUnitID;
		internal static readonly IEntityRelation ParameterEntityUsingParameterIDStatic = new ParameterValueRelations().ParameterEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterArchiveEntityUsingParameterArchiveIDStatic = new ParameterValueRelations().ParameterArchiveEntityUsingParameterArchiveID;
		internal static readonly IEntityRelation UnitCollectionEntityUsingUnitCollectionIDStatic = new ParameterValueRelations().UnitCollectionEntityUsingUnitCollectionID;

		/// <summary>CTor</summary>
		static StaticParameterValueRelations()
		{
		}
	}
}
