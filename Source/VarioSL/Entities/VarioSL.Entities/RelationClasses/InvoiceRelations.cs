﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Invoice. </summary>
	public partial class InvoiceRelations
	{
		/// <summary>CTor</summary>
		public InvoiceRelations()
		{
		}

		/// <summary>Gets all relations of the InvoiceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningToInvoiceEntityUsingInvoiceID);
			toReturn.Add(this.InvoiceEntryEntityUsingInvoiceID);
			toReturn.Add(this.PostingEntityUsingInvoiceID);
			toReturn.Add(this.ProductTerminationEntityUsingInvoiceID);
			toReturn.Add(this.StatementOfAccountEntityUsingInvoiceID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.UserListEntityUsingUserID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.InvoicingEntityUsingInvoicingID);
			toReturn.Add(this.PaymentOptionEntityUsingUsedPaymentOptionID);
			toReturn.Add(this.SchoolYearEntityUsingSchoolYearID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and DunningToInvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoice.InvoiceID - DunningToInvoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation DunningToInvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToInvoices" , true);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, DunningToInvoiceFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and InvoiceEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoice.InvoiceID - InvoiceEntry.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntryEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "InvoiceEntries" , true);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, InvoiceEntryFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoice.InvoiceID - Posting.InvoiceID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, PostingFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and ProductTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoice.InvoiceID - ProductTermination.InvoiceID
		/// </summary>
		public virtual IEntityRelation ProductTerminationEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTerminations" , true);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, ProductTerminationFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and StatementOfAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Invoice.InvoiceID - StatementOfAccount.InvoiceID
		/// </summary>
		public virtual IEntityRelation StatementOfAccountEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SlStatementofaccounts" , true);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, StatementOfAccountFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StatementOfAccountEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, InvoiceFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, InvoiceFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, InvoiceFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and InvoicingEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.InvoicingID - Invoicing.InvoicingID
		/// </summary>
		public virtual IEntityRelation InvoicingEntityUsingInvoicingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoicing", false);
				relation.AddEntityFieldPair(InvoicingFields.InvoicingID, InvoiceFields.InvoicingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoicingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.UsedPaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingUsedPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, InvoiceFields.UsedPaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between InvoiceEntity and SchoolYearEntity over the m:1 relation they have, using the relation between the fields:
		/// Invoice.SchoolYearID - SchoolYear.SchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SchoolYear", false);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, InvoiceFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInvoiceRelations
	{
		internal static readonly IEntityRelation DunningToInvoiceEntityUsingInvoiceIDStatic = new InvoiceRelations().DunningToInvoiceEntityUsingInvoiceID;
		internal static readonly IEntityRelation InvoiceEntryEntityUsingInvoiceIDStatic = new InvoiceRelations().InvoiceEntryEntityUsingInvoiceID;
		internal static readonly IEntityRelation PostingEntityUsingInvoiceIDStatic = new InvoiceRelations().PostingEntityUsingInvoiceID;
		internal static readonly IEntityRelation ProductTerminationEntityUsingInvoiceIDStatic = new InvoiceRelations().ProductTerminationEntityUsingInvoiceID;
		internal static readonly IEntityRelation StatementOfAccountEntityUsingInvoiceIDStatic = new InvoiceRelations().StatementOfAccountEntityUsingInvoiceID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new InvoiceRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new InvoiceRelations().UserListEntityUsingUserID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new InvoiceRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation InvoicingEntityUsingInvoicingIDStatic = new InvoiceRelations().InvoicingEntityUsingInvoicingID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingUsedPaymentOptionIDStatic = new InvoiceRelations().PaymentOptionEntityUsingUsedPaymentOptionID;
		internal static readonly IEntityRelation SchoolYearEntityUsingSchoolYearIDStatic = new InvoiceRelations().SchoolYearEntityUsingSchoolYearID;

		/// <summary>CTor</summary>
		static StaticInvoiceRelations()
		{
		}
	}
}
