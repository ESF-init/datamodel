﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardHolder. </summary>
	public partial class CardHolderRelations : PersonRelations
	{
		/// <summary>CTor</summary>
		public CardHolderRelations()
		{
		}

		/// <summary>Gets all relations of the CardHolderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = base.GetAllRelations();
			toReturn.Add(this.CardEntityUsingCardHolderID);
			toReturn.Add(this.CardEntityUsingParticipantID);
			toReturn.Add(this.OrderDetailToCardHolderEntityUsingCardHolderID);
			toReturn.Add(this.TransactionJournalEntityUsingCardHolderID);
			toReturn.Add(this.AddressEntityUsingAlternativeAdressID);
			toReturn.Add(this.CardHolderOrganizationEntityUsingCardHolderID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.SchoolYearEntityUsingSchoolYearID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and BankConnectionDataEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - BankConnectionData.AccountOwnerID
		/// </summary>
		public override IEntityRelation BankConnectionDataEntityUsingAccountOwnerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BankConnectionDatas" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, BankConnectionDataFields.AccountOwnerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Card.CardHolderID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Cards" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and CardEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Card.ParticipantID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingParticipantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParticipantCards" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardFields.ParticipantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and CustomAttributeValueToPersonEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - CustomAttributeValueToPerson.PersonID
		/// </summary>
		public override IEntityRelation CustomAttributeValueToPersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomAttributeValueToPeople" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CustomAttributeValueToPersonFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueToPersonEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and CustomerAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - CustomerAccount.PersonID
		/// </summary>
		public override IEntityRelation CustomerAccountEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccounts" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, CustomerAccountFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - EmailEventResendLock.PersonID
		/// </summary>
		public override IEntityRelation EmailEventResendLockEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, EmailEventResendLockFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and EntitlementEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Entitlement.PersonID
		/// </summary>
		public override IEntityRelation EntitlementEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Entitlements" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, EntitlementFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - FareEvasionIncident.GuardianID
		/// </summary>
		public override IEntityRelation FareEvasionIncidentEntityUsingGuardianID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidentsAsGuardian" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, FareEvasionIncidentFields.GuardianID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - FareEvasionIncident.PersonID
		/// </summary>
		public override IEntityRelation FareEvasionIncidentEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidentsAsPerson" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, FareEvasionIncidentFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and OrderDetailToCardHolderEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - OrderDetailToCardHolder.CardHolderID
		/// </summary>
		public virtual IEntityRelation OrderDetailToCardHolderEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetailToCardHolder" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, OrderDetailToCardHolderFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardHolderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - PaymentJournal.PersonID
		/// </summary>
		public override IEntityRelation PaymentJournalEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, PaymentJournalFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and PersonAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - PersonAddress.PersonID
		/// </summary>
		public override IEntityRelation PersonAddressEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PersonAddresses" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, PersonAddressFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and PhotographEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Photograph.PersonID
		/// </summary>
		public override IEntityRelation PhotographEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Photographs" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, PhotographFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and RecipientGroupMemberEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - RecipientGroupMember.PersonID
		/// </summary>
		public override IEntityRelation RecipientGroupMemberEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RecipientGroupMembers" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, RecipientGroupMemberFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupMemberEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Sale.PersonID
		/// </summary>
		public override IEntityRelation SaleEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, SaleFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and SurveyResponseEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - SurveyResponse.PersonID
		/// </summary>
		public override IEntityRelation SurveyResponseEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResponses" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, SurveyResponseFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// CardHolder.PersonID - TransactionJournal.CardHolderID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, TransactionJournalFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and AddressEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolder.AddressID - Address.AddressID
		/// </summary>
		public override IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Address", false);




				relation.AddEntityFieldPair(AddressFields.AddressID, CardHolderFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and AddressEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolder.AlternativeAdressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAlternativeAdressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "AlternativeAddress", false);




				relation.AddEntityFieldPair(AddressFields.AddressID, CardHolderFields.AlternativeAdressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and CardHolderOrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolder.PersonID - CardHolderOrganization.CardHolderID
		/// </summary>
		public virtual IEntityRelation CardHolderOrganizationEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardHolderOrganization", true);


				relation.AddEntityFieldPair(CardHolderFields.PersonID, CardHolderOrganizationFields.CardHolderID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderOrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and OrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardHolder.PersonID - Organization.ContactPersonID
		/// </summary>
		public override IEntityRelation OrganizationEntityUsingContactPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ContactPersonOnOrganization", true);


				relation.AddEntityFieldPair(CardHolderFields.PersonID, OrganizationFields.ContactPersonID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// CardHolder.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, CardHolderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardHolderEntity and SchoolYearEntity over the m:1 relation they have, using the relation between the fields:
		/// CardHolder.SchoolYearID - SchoolYear.SchoolYearID
		/// </summary>
		public virtual IEntityRelation SchoolYearEntityUsingSchoolYearID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SchoolYear", false);
				relation.AddEntityFieldPair(SchoolYearFields.SchoolYearID, CardHolderFields.SchoolYearID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SchoolYearEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardHolderRelations
	{
		internal static readonly IEntityRelation BankConnectionDataEntityUsingAccountOwnerIDStatic = new CardHolderRelations().BankConnectionDataEntityUsingAccountOwnerID;
		internal static readonly IEntityRelation CardEntityUsingCardHolderIDStatic = new CardHolderRelations().CardEntityUsingCardHolderID;
		internal static readonly IEntityRelation CardEntityUsingParticipantIDStatic = new CardHolderRelations().CardEntityUsingParticipantID;
		internal static readonly IEntityRelation CustomAttributeValueToPersonEntityUsingPersonIDStatic = new CardHolderRelations().CustomAttributeValueToPersonEntityUsingPersonID;
		internal static readonly IEntityRelation CustomerAccountEntityUsingPersonIDStatic = new CardHolderRelations().CustomerAccountEntityUsingPersonID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingPersonIDStatic = new CardHolderRelations().EmailEventResendLockEntityUsingPersonID;
		internal static readonly IEntityRelation EntitlementEntityUsingPersonIDStatic = new CardHolderRelations().EntitlementEntityUsingPersonID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingGuardianIDStatic = new CardHolderRelations().FareEvasionIncidentEntityUsingGuardianID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingPersonIDStatic = new CardHolderRelations().FareEvasionIncidentEntityUsingPersonID;
		internal static readonly IEntityRelation OrderDetailToCardHolderEntityUsingCardHolderIDStatic = new CardHolderRelations().OrderDetailToCardHolderEntityUsingCardHolderID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPersonIDStatic = new CardHolderRelations().PaymentJournalEntityUsingPersonID;
		internal static readonly IEntityRelation PersonAddressEntityUsingPersonIDStatic = new CardHolderRelations().PersonAddressEntityUsingPersonID;
		internal static readonly IEntityRelation PhotographEntityUsingPersonIDStatic = new CardHolderRelations().PhotographEntityUsingPersonID;
		internal static readonly IEntityRelation RecipientGroupMemberEntityUsingPersonIDStatic = new CardHolderRelations().RecipientGroupMemberEntityUsingPersonID;
		internal static readonly IEntityRelation SaleEntityUsingPersonIDStatic = new CardHolderRelations().SaleEntityUsingPersonID;
		internal static readonly IEntityRelation SurveyResponseEntityUsingPersonIDStatic = new CardHolderRelations().SurveyResponseEntityUsingPersonID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingCardHolderIDStatic = new CardHolderRelations().TransactionJournalEntityUsingCardHolderID;
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new CardHolderRelations().AddressEntityUsingAddressID;
		internal static readonly IEntityRelation AddressEntityUsingAlternativeAdressIDStatic = new CardHolderRelations().AddressEntityUsingAlternativeAdressID;
		internal static readonly IEntityRelation CardHolderOrganizationEntityUsingCardHolderIDStatic = new CardHolderRelations().CardHolderOrganizationEntityUsingCardHolderID;
		internal static readonly IEntityRelation OrganizationEntityUsingContactPersonIDStatic = new CardHolderRelations().OrganizationEntityUsingContactPersonID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new CardHolderRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation SchoolYearEntityUsingSchoolYearIDStatic = new CardHolderRelations().SchoolYearEntityUsingSchoolYearID;

		/// <summary>CTor</summary>
		static StaticCardHolderRelations()
		{
		}
	}
}
