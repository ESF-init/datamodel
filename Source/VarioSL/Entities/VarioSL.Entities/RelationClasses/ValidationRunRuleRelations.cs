﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ValidationRunRule. </summary>
	public partial class ValidationRunRuleRelations
	{
		/// <summary>CTor</summary>
		public ValidationRunRuleRelations()
		{
		}

		/// <summary>Gets all relations of the ValidationRunRuleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ValidationResultEntityUsingValidationRunRuleID);
			toReturn.Add(this.ValidationRuleEntityUsingValidationRuleID);
			toReturn.Add(this.ValidationRunEntityUsingValidationRunID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ValidationRunRuleEntity and ValidationResultEntity over the 1:n relation they have, using the relation between the fields:
		/// ValidationRunRule.ValidationRunRuleID - ValidationResult.ValidationRunRuleID
		/// </summary>
		public virtual IEntityRelation ValidationResultEntityUsingValidationRunRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ValidationResults" , true);
				relation.AddEntityFieldPair(ValidationRunRuleFields.ValidationRunRuleID, ValidationResultFields.ValidationRunRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunRuleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationResultEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ValidationRunRuleEntity and ValidationRuleEntity over the m:1 relation they have, using the relation between the fields:
		/// ValidationRunRule.ValidationRuleID - ValidationRule.ValidationRuleID
		/// </summary>
		public virtual IEntityRelation ValidationRuleEntityUsingValidationRuleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ValidationRule", false);
				relation.AddEntityFieldPair(ValidationRuleFields.ValidationRuleID, ValidationRunRuleFields.ValidationRuleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRuleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunRuleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ValidationRunRuleEntity and ValidationRunEntity over the m:1 relation they have, using the relation between the fields:
		/// ValidationRunRule.ValidationRunID - ValidationRun.ValidationRunID
		/// </summary>
		public virtual IEntityRelation ValidationRunEntityUsingValidationRunID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ValidationRun", false);
				relation.AddEntityFieldPair(ValidationRunFields.ValidationRunID, ValidationRunRuleFields.ValidationRunID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ValidationRunRuleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticValidationRunRuleRelations
	{
		internal static readonly IEntityRelation ValidationResultEntityUsingValidationRunRuleIDStatic = new ValidationRunRuleRelations().ValidationResultEntityUsingValidationRunRuleID;
		internal static readonly IEntityRelation ValidationRuleEntityUsingValidationRuleIDStatic = new ValidationRunRuleRelations().ValidationRuleEntityUsingValidationRuleID;
		internal static readonly IEntityRelation ValidationRunEntityUsingValidationRunIDStatic = new ValidationRunRuleRelations().ValidationRunEntityUsingValidationRunID;

		/// <summary>CTor</summary>
		static StaticValidationRunRuleRelations()
		{
		}
	}
}
