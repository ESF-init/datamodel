﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AdditionalData. </summary>
	public partial class AdditionalDataRelations
	{
		/// <summary>CTor</summary>
		public AdditionalDataRelations()
		{
		}

		/// <summary>Gets all relations of the AdditionalDataEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PerformanceEntityUsingPerformanceID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between AdditionalDataEntity and PerformanceEntity over the 1:1 relation they have, using the relation between the fields:
		/// AdditionalData.PerformanceID - Performance.PerformanceID
		/// </summary>
		public virtual IEntityRelation PerformanceEntityUsingPerformanceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Performance", false);




				relation.AddEntityFieldPair(PerformanceFields.PerformanceID, AdditionalDataFields.PerformanceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AdditionalDataEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAdditionalDataRelations
	{
		internal static readonly IEntityRelation PerformanceEntityUsingPerformanceIDStatic = new AdditionalDataRelations().PerformanceEntityUsingPerformanceID;

		/// <summary>CTor</summary>
		static StaticAdditionalDataRelations()
		{
		}
	}
}
