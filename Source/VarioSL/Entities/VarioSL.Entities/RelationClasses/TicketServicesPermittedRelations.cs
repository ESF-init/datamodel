﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketServicesPermitted. </summary>
	public partial class TicketServicesPermittedRelations
	{
		/// <summary>CTor</summary>
		public TicketServicesPermittedRelations()
		{
		}

		/// <summary>Gets all relations of the TicketServicesPermittedEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LineEntityUsingLineID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketServicesPermittedEntity and LineEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketServicesPermitted.LineID - Line.LineID
		/// </summary>
		public virtual IEntityRelation LineEntityUsingLineID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Line", false);
				relation.AddEntityFieldPair(LineFields.LineID, TicketServicesPermittedFields.LineID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketServicesPermittedEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketServicesPermittedEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketServicesPermitted.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketServicesPermittedFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketServicesPermittedEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketServicesPermittedRelations
	{
		internal static readonly IEntityRelation LineEntityUsingLineIDStatic = new TicketServicesPermittedRelations().LineEntityUsingLineID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketServicesPermittedRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticTicketServicesPermittedRelations()
		{
		}
	}
}
