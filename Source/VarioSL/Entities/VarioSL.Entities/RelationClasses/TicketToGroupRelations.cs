﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketToGroup. </summary>
	public partial class TicketToGroupRelations
	{
		/// <summary>CTor</summary>
		public TicketToGroupRelations()
		{
		}

		/// <summary>Gets all relations of the TicketToGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.TicketGroupEntityUsingTicketGroupID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketToGroupEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketToGroup.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketToGroupFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketToGroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketToGroupEntity and TicketGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketToGroup.TicketGroupID - TicketGroup.TicketGroupID
		/// </summary>
		public virtual IEntityRelation TicketGroupEntityUsingTicketGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketGroup", false);
				relation.AddEntityFieldPair(TicketGroupFields.TicketGroupID, TicketToGroupFields.TicketGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketToGroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketToGroupRelations
	{
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketToGroupRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketGroupEntityUsingTicketGroupIDStatic = new TicketToGroupRelations().TicketGroupEntityUsingTicketGroupID;

		/// <summary>CTor</summary>
		static StaticTicketToGroupRelations()
		{
		}
	}
}
