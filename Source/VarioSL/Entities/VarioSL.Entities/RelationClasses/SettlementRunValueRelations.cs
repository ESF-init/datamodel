﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementRunValue. </summary>
	public partial class SettlementRunValueRelations
	{
		/// <summary>CTor</summary>
		public SettlementRunValueRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementRunValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SettlementQueryValueEntityUsingSettlementRunValueID);
			toReturn.Add(this.SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID);
			toReturn.Add(this.SettlementRunEntityUsingSettlementRunID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SettlementRunValueEntity and SettlementQueryValueEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementRunValue.SettlementRunValueID - SettlementQueryValue.SettlementRunValueID
		/// </summary>
		public virtual IEntityRelation SettlementQueryValueEntityUsingSettlementRunValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementQueryValues" , true);
				relation.AddEntityFieldPair(SettlementRunValueFields.SettlementRunValueID, SettlementQueryValueFields.SettlementRunValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQueryValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SettlementRunValueEntity and SettlementRunValueToVarioSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementRunValue.SettlementRunValueID - SettlementRunValueToVarioSettlement.SettlementRunValueID
		/// </summary>
		public virtual IEntityRelation SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementRunValueToVarioSettlements" , true);
				relation.AddEntityFieldPair(SettlementRunValueFields.SettlementRunValueID, SettlementRunValueToVarioSettlementFields.SettlementRunValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueToVarioSettlementEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SettlementRunValueEntity and SettlementRunEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementRunValue.SettlementRunID - SettlementRun.SettlementRunID
		/// </summary>
		public virtual IEntityRelation SettlementRunEntityUsingSettlementRunID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SettlementRun", false);
				relation.AddEntityFieldPair(SettlementRunFields.SettlementRunID, SettlementRunValueFields.SettlementRunID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementRunValueRelations
	{
		internal static readonly IEntityRelation SettlementQueryValueEntityUsingSettlementRunValueIDStatic = new SettlementRunValueRelations().SettlementQueryValueEntityUsingSettlementRunValueID;
		internal static readonly IEntityRelation SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueIDStatic = new SettlementRunValueRelations().SettlementRunValueToVarioSettlementEntityUsingSettlementRunValueID;
		internal static readonly IEntityRelation SettlementRunEntityUsingSettlementRunIDStatic = new SettlementRunValueRelations().SettlementRunEntityUsingSettlementRunID;

		/// <summary>CTor</summary>
		static StaticSettlementRunValueRelations()
		{
		}
	}
}
