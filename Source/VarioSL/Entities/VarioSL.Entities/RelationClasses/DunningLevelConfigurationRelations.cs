﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningLevelConfiguration. </summary>
	public partial class DunningLevelConfigurationRelations
	{
		/// <summary>CTor</summary>
		public DunningLevelConfigurationRelations()
		{
		}

		/// <summary>Gets all relations of the DunningLevelConfigurationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningLevelConfigurationEntityUsingNextLevelID);
			toReturn.Add(this.DunningLevelPostingKeyEntityUsingDunningLevelID);
			toReturn.Add(this.DunningToInvoiceEntityUsingDunningLevelID);
			toReturn.Add(this.DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelID);
			toReturn.Add(this.FormEntityUsingFormID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DunningLevelConfigurationEntity and DunningLevelConfigurationEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningLevelConfiguration.DunningLevelID - DunningLevelConfiguration.NextLevelID
		/// </summary>
		public virtual IEntityRelation DunningLevelConfigurationEntityUsingNextLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PreviousDunningLevel" , true);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningLevelConfigurationFields.NextLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DunningLevelConfigurationEntity and DunningLevelPostingKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningLevelConfiguration.DunningLevelID - DunningLevelPostingKey.DunningLevelID
		/// </summary>
		public virtual IEntityRelation DunningLevelPostingKeyEntityUsingDunningLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningLevelPostingKeys" , true);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningLevelPostingKeyFields.DunningLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelPostingKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DunningLevelConfigurationEntity and DunningToInvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningLevelConfiguration.DunningLevelID - DunningToInvoice.DunningLevelID
		/// </summary>
		public virtual IEntityRelation DunningToInvoiceEntityUsingDunningLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToInvoices" , true);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningToInvoiceFields.DunningLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between DunningLevelConfigurationEntity and DunningLevelConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningLevelConfiguration.NextLevelID - DunningLevelConfiguration.DunningLevelID
		/// </summary>
		public virtual IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NextDunningLevel", false);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningLevelConfigurationFields.NextLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningLevelConfigurationEntity and FormEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningLevelConfiguration.FormID - Form.FormID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Form", false);
				relation.AddEntityFieldPair(FormFields.FormID, DunningLevelConfigurationFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningLevelConfigurationRelations
	{
		internal static readonly IEntityRelation DunningLevelConfigurationEntityUsingNextLevelIDStatic = new DunningLevelConfigurationRelations().DunningLevelConfigurationEntityUsingNextLevelID;
		internal static readonly IEntityRelation DunningLevelPostingKeyEntityUsingDunningLevelIDStatic = new DunningLevelConfigurationRelations().DunningLevelPostingKeyEntityUsingDunningLevelID;
		internal static readonly IEntityRelation DunningToInvoiceEntityUsingDunningLevelIDStatic = new DunningLevelConfigurationRelations().DunningToInvoiceEntityUsingDunningLevelID;
		internal static readonly IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelIDStatic = new DunningLevelConfigurationRelations().DunningLevelConfigurationEntityUsingDunningLevelIDNextLevelID;
		internal static readonly IEntityRelation FormEntityUsingFormIDStatic = new DunningLevelConfigurationRelations().FormEntityUsingFormID;

		/// <summary>CTor</summary>
		static StaticDunningLevelConfigurationRelations()
		{
		}
	}
}
