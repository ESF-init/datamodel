﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ClearingResult. </summary>
	public partial class ClearingResultRelations
	{
		/// <summary>CTor</summary>
		public ClearingResultRelations()
		{
		}

		/// <summary>Gets all relations of the ClearingResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingCardIssuerID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ClientEntityUsingFromClient);
			toReturn.Add(this.ClientEntityUsingToClient);
			toReturn.Add(this.DebtorEntityUsingDebtorID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.RevenueTransactionTypeEntityUsingTransactionTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.CardIssuerID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingCardIssuerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardIssuer", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.CardIssuerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.FromClient - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingFromClient
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientFrom", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.FromClient);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.ToClient - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingToClient
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ClientTo", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ClearingResultFields.ToClient);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.DebtorID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Debtor", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ClearingResultFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ClearingResultFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and DevicePaymentMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.DevicePaymentMethodID - DevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DevicePaymentMethod", false);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, ClearingResultFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ClearingResultEntity and RevenueTransactionTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// ClearingResult.TransactionTypeID - RevenueTransactionType.TypeID
		/// </summary>
		public virtual IEntityRelation RevenueTransactionTypeEntityUsingTransactionTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueTransactionType", false);
				relation.AddEntityFieldPair(RevenueTransactionTypeFields.TypeID, ClearingResultFields.TransactionTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueTransactionTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticClearingResultRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingCardIssuerIDStatic = new ClearingResultRelations().ClientEntityUsingCardIssuerID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ClearingResultRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ClientEntityUsingFromClientStatic = new ClearingResultRelations().ClientEntityUsingFromClient;
		internal static readonly IEntityRelation ClientEntityUsingToClientStatic = new ClearingResultRelations().ClientEntityUsingToClient;
		internal static readonly IEntityRelation DebtorEntityUsingDebtorIDStatic = new ClearingResultRelations().DebtorEntityUsingDebtorID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new ClearingResultRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new ClearingResultRelations().DevicePaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation RevenueTransactionTypeEntityUsingTransactionTypeIDStatic = new ClearingResultRelations().RevenueTransactionTypeEntityUsingTransactionTypeID;

		/// <summary>CTor</summary>
		static StaticClearingResultRelations()
		{
		}
	}
}
