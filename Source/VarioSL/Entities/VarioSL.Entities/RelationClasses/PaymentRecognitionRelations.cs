﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentRecognition. </summary>
	public partial class PaymentRecognitionRelations
	{
		/// <summary>CTor</summary>
		public PaymentRecognitionRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentRecognitionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentJournalEntityUsingPaymentJournalID);
			toReturn.Add(this.CloseoutPeriodEntityUsingCloseoutPeriodID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between PaymentRecognitionEntity and PaymentJournalEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentRecognition.PaymentJournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentJournal", false);




				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentRecognitionFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentRecognitionEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentRecognitionEntity and CloseoutPeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentRecognition.CloseoutPeriodID - CloseoutPeriod.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CloseoutPeriod", false);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, PaymentRecognitionFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentRecognitionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentRecognitionRelations
	{
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentJournalIDStatic = new PaymentRecognitionRelations().PaymentJournalEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodIDStatic = new PaymentRecognitionRelations().CloseoutPeriodEntityUsingCloseoutPeriodID;

		/// <summary>CTor</summary>
		static StaticPaymentRecognitionRelations()
		{
		}
	}
}
