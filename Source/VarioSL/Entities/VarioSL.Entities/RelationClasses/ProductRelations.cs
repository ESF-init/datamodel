﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Product. </summary>
	public partial class ProductRelations
	{
		/// <summary>CTor</summary>
		public ProductRelations()
		{
		}

		/// <summary>Gets all relations of the ProductEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutoloadSettingEntityUsingProductID);
			toReturn.Add(this.DunningToProductEntityUsingProductID);
			toReturn.Add(this.EmailEventResendLockEntityUsingProductID);
			toReturn.Add(this.EntitlementToProductEntityUsingProductID);
			toReturn.Add(this.OrderDetailEntityUsingProductID);
			toReturn.Add(this.PassExpiryNotificationEntityUsingProductID);
			toReturn.Add(this.PostingEntityUsingProductID);
			toReturn.Add(this.ProductEntityUsingReferencedProductID);
			toReturn.Add(this.ProductTerminationEntityUsingProductID);
			toReturn.Add(this.RuleViolationEntityUsingProductID);
			toReturn.Add(this.TicketSerialNumberEntityUsingProductID);
			toReturn.Add(this.TransactionJournalEntityUsingAppliedPassProductID);
			toReturn.Add(this.TransactionJournalEntityUsingProductID);
			toReturn.Add(this.ProductRelationEntityUsingProductID);
			toReturn.Add(this.SubsidyTransactionEntityUsingProductID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.PointOfSaleEntityUsingPointOfSaleID);
			toReturn.Add(this.TicketEntityUsingTaxTicketID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.OrganizationEntityUsingOrganizationID);
			toReturn.Add(this.ProductEntityUsingProductIDReferencedProductID);
			toReturn.Add(this.RefundEntityUsingRefundID);
			toReturn.Add(this.VoucherEntityUsingVoucherID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and AutoloadSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - AutoloadSetting.ProductID
		/// </summary>
		public virtual IEntityRelation AutoloadSettingEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AutoloadSettings" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, AutoloadSettingFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and DunningToProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - DunningToProduct.ProductID
		/// </summary>
		public virtual IEntityRelation DunningToProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToProducts" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, DunningToProductFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - EmailEventResendLock.ProductID
		/// </summary>
		public virtual IEntityRelation EmailEventResendLockEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, EmailEventResendLockFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and EntitlementToProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - EntitlementToProduct.ProductID
		/// </summary>
		public virtual IEntityRelation EntitlementToProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntitlementToProducts" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, EntitlementToProductFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementToProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrderDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - OrderDetail.ProductID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetails" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, OrderDetailFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PassExpiryNotificationEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - PassExpiryNotification.ProductID
		/// </summary>
		public virtual IEntityRelation PassExpiryNotificationEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PassExpiryNotifications" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, PassExpiryNotificationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PassExpiryNotificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - Posting.ProductID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, PostingFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - Product.ReferencedProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingReferencedProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Product" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, ProductFields.ReferencedProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - ProductTermination.ProductID
		/// </summary>
		public virtual IEntityRelation ProductTerminationEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTerminations" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, ProductTerminationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and RuleViolationEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - RuleViolation.ProductID
		/// </summary>
		public virtual IEntityRelation RuleViolationEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolations" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, RuleViolationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TicketSerialNumberEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - TicketSerialNumber.ProductID
		/// </summary>
		public virtual IEntityRelation TicketSerialNumberEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketSerialNumbers" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, TicketSerialNumberFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - TransactionJournal.AppliedPassProductID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingAppliedPassProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournalsForAppledPass" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, TransactionJournalFields.AppliedPassProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Product.ProductID - TransactionJournal.ProductID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(ProductFields.ProductID, TransactionJournalFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductRelationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Product.ProductID - ProductRelation.ProductID
		/// </summary>
		public virtual IEntityRelation ProductRelationEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ProductRelation", true);


				relation.AddEntityFieldPair(ProductFields.ProductID, ProductRelationFields.ProductID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and SubsidyTransactionEntity over the 1:1 relation they have, using the relation between the fields:
		/// Product.ProductID - SubsidyTransaction.ProductID
		/// </summary>
		public virtual IEntityRelation SubsidyTransactionEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "SubsidyTransaction", true);


				relation.AddEntityFieldPair(ProductFields.ProductID, SubsidyTransactionFields.ProductID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SubsidyTransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ProductFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and PointOfSaleEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.PointOfSaleID - PointOfSale.PointOfSaleID
		/// </summary>
		public virtual IEntityRelation PointOfSaleEntityUsingPointOfSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PointOfSale", false);
				relation.AddEntityFieldPair(PointOfSaleFields.PointOfSaleID, ProductFields.PointOfSaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PointOfSaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.TaxTicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTaxTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TaxTicket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, ProductFields.TaxTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, ProductFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, ProductFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ProductFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and OrganizationEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.OrganizationID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Organization", false);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, ProductFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.ReferencedProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductIDReferencedProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReferencedProduct", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, ProductFields.ReferencedProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and RefundEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.RefundID - Refund.RefundID
		/// </summary>
		public virtual IEntityRelation RefundEntityUsingRefundID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Refund", false);
				relation.AddEntityFieldPair(RefundFields.RefundID, ProductFields.RefundID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RefundEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductEntity and VoucherEntity over the m:1 relation they have, using the relation between the fields:
		/// Product.VoucherID - Voucher.VoucherID
		/// </summary>
		public virtual IEntityRelation VoucherEntityUsingVoucherID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Voucher", false);
				relation.AddEntityFieldPair(VoucherFields.VoucherID, ProductFields.VoucherID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VoucherEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductRelations
	{
		internal static readonly IEntityRelation AutoloadSettingEntityUsingProductIDStatic = new ProductRelations().AutoloadSettingEntityUsingProductID;
		internal static readonly IEntityRelation DunningToProductEntityUsingProductIDStatic = new ProductRelations().DunningToProductEntityUsingProductID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingProductIDStatic = new ProductRelations().EmailEventResendLockEntityUsingProductID;
		internal static readonly IEntityRelation EntitlementToProductEntityUsingProductIDStatic = new ProductRelations().EntitlementToProductEntityUsingProductID;
		internal static readonly IEntityRelation OrderDetailEntityUsingProductIDStatic = new ProductRelations().OrderDetailEntityUsingProductID;
		internal static readonly IEntityRelation PassExpiryNotificationEntityUsingProductIDStatic = new ProductRelations().PassExpiryNotificationEntityUsingProductID;
		internal static readonly IEntityRelation PostingEntityUsingProductIDStatic = new ProductRelations().PostingEntityUsingProductID;
		internal static readonly IEntityRelation ProductEntityUsingReferencedProductIDStatic = new ProductRelations().ProductEntityUsingReferencedProductID;
		internal static readonly IEntityRelation ProductTerminationEntityUsingProductIDStatic = new ProductRelations().ProductTerminationEntityUsingProductID;
		internal static readonly IEntityRelation RuleViolationEntityUsingProductIDStatic = new ProductRelations().RuleViolationEntityUsingProductID;
		internal static readonly IEntityRelation TicketSerialNumberEntityUsingProductIDStatic = new ProductRelations().TicketSerialNumberEntityUsingProductID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingAppliedPassProductIDStatic = new ProductRelations().TransactionJournalEntityUsingAppliedPassProductID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingProductIDStatic = new ProductRelations().TransactionJournalEntityUsingProductID;
		internal static readonly IEntityRelation ProductRelationEntityUsingProductIDStatic = new ProductRelations().ProductRelationEntityUsingProductID;
		internal static readonly IEntityRelation SubsidyTransactionEntityUsingProductIDStatic = new ProductRelations().SubsidyTransactionEntityUsingProductID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ProductRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation PointOfSaleEntityUsingPointOfSaleIDStatic = new ProductRelations().PointOfSaleEntityUsingPointOfSaleID;
		internal static readonly IEntityRelation TicketEntityUsingTaxTicketIDStatic = new ProductRelations().TicketEntityUsingTaxTicketID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new ProductRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new ProductRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ProductRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation OrganizationEntityUsingOrganizationIDStatic = new ProductRelations().OrganizationEntityUsingOrganizationID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDReferencedProductIDStatic = new ProductRelations().ProductEntityUsingProductIDReferencedProductID;
		internal static readonly IEntityRelation RefundEntityUsingRefundIDStatic = new ProductRelations().RefundEntityUsingRefundID;
		internal static readonly IEntityRelation VoucherEntityUsingVoucherIDStatic = new ProductRelations().VoucherEntityUsingVoucherID;

		/// <summary>CTor</summary>
		static StaticProductRelations()
		{
		}
	}
}
