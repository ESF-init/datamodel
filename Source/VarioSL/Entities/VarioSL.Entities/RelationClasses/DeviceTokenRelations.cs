﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceToken. </summary>
	public partial class DeviceTokenRelations
	{
		/// <summary>CTor</summary>
		public DeviceTokenRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceTokenEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomerAccountNotificationEntityUsingDeviceTokenID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeviceTokenEntity and CustomerAccountNotificationEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceToken.DeviceTokenID - CustomerAccountNotification.DeviceTokenID
		/// </summary>
		public virtual IEntityRelation CustomerAccountNotificationEntityUsingDeviceTokenID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccountNotifications" , true);
				relation.AddEntityFieldPair(DeviceTokenFields.DeviceTokenID, CustomerAccountNotificationFields.DeviceTokenID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceTokenRelations
	{
		internal static readonly IEntityRelation CustomerAccountNotificationEntityUsingDeviceTokenIDStatic = new DeviceTokenRelations().CustomerAccountNotificationEntityUsingDeviceTokenID;

		/// <summary>CTor</summary>
		static StaticDeviceTokenRelations()
		{
		}
	}
}
