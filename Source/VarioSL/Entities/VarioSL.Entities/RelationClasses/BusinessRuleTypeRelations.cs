﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BusinessRuleType. </summary>
	public partial class BusinessRuleTypeRelations
	{
		/// <summary>CTor</summary>
		public BusinessRuleTypeRelations()
		{
		}

		/// <summary>Gets all relations of the BusinessRuleTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BusinessRuleEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.BusinessRuleConditionEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.BusinessRuleResultEntityUsingBusinessRuleTypeID);
			toReturn.Add(this.BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeEntity and BusinessRuleEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleType.BusinessRuleTypeID - BusinessRule.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRules" , true);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeEntity and BusinessRuleConditionEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleType.BusinessRuleTypeID - BusinessRuleCondition.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleConditionEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleCondition" , true);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleConditionFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleConditionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeEntity and BusinessRuleResultEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleType.BusinessRuleTypeID - BusinessRuleResult.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleResultEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleResult" , true);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleResultFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BusinessRuleTypeEntity and BusinessRuleTypeToVariableEntity over the 1:n relation they have, using the relation between the fields:
		/// BusinessRuleType.BusinessRuleTypeID - BusinessRuleTypeToVariable.BusinessRuleTypeID
		/// </summary>
		public virtual IEntityRelation BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BusinessRuleTypeToVariables" , true);
				relation.AddEntityFieldPair(BusinessRuleTypeFields.BusinessRuleTypeID, BusinessRuleTypeToVariableFields.BusinessRuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BusinessRuleTypeToVariableEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBusinessRuleTypeRelations
	{
		internal static readonly IEntityRelation BusinessRuleEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleTypeRelations().BusinessRuleEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation BusinessRuleConditionEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleTypeRelations().BusinessRuleConditionEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation BusinessRuleResultEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleTypeRelations().BusinessRuleResultEntityUsingBusinessRuleTypeID;
		internal static readonly IEntityRelation BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeIDStatic = new BusinessRuleTypeRelations().BusinessRuleTypeToVariableEntityUsingBusinessRuleTypeID;

		/// <summary>CTor</summary>
		static StaticBusinessRuleTypeRelations()
		{
		}
	}
}
