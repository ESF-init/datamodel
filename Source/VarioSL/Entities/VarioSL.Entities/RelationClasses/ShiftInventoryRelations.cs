﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ShiftInventory. </summary>
	public partial class ShiftInventoryRelations
	{
		/// <summary>CTor</summary>
		public ShiftInventoryRelations()
		{
		}

		/// <summary>Gets all relations of the ShiftInventoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AuditRegisterEntityUsingShiftID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ShiftInventoryEntity and AuditRegisterEntity over the 1:n relation they have, using the relation between the fields:
		/// ShiftInventory.HashValue - AuditRegister.ShiftID
		/// </summary>
		public virtual IEntityRelation AuditRegisterEntityUsingShiftID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AuditRegisters" , true);
				relation.AddEntityFieldPair(ShiftInventoryFields.HashValue, AuditRegisterFields.ShiftID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftInventoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AuditRegisterEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticShiftInventoryRelations
	{
		internal static readonly IEntityRelation AuditRegisterEntityUsingShiftIDStatic = new ShiftInventoryRelations().AuditRegisterEntityUsingShiftID;

		/// <summary>CTor</summary>
		static StaticShiftInventoryRelations()
		{
		}
	}
}
