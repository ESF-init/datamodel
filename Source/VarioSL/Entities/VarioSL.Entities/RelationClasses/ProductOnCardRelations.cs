﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductOnCard. </summary>
	public partial class ProductOnCardRelations
	{
		/// <summary>CTor</summary>
		public ProductOnCardRelations()
		{
		}

		/// <summary>Gets all relations of the ProductOnCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardActionRequestEntityUsingCardActionRequestID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between ProductOnCardEntity and CardActionRequestEntity over the 1:1 relation they have, using the relation between the fields:
		/// ProductOnCard.CardActionRequestID - CardActionRequest.CardActionRequestID
		/// </summary>
		public virtual IEntityRelation CardActionRequestEntityUsingCardActionRequestID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardActionRequest", false);




				relation.AddEntityFieldPair(CardActionRequestFields.CardActionRequestID, ProductOnCardFields.CardActionRequestID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardActionRequestEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductOnCardEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductOnCardRelations
	{
		internal static readonly IEntityRelation CardActionRequestEntityUsingCardActionRequestIDStatic = new ProductOnCardRelations().CardActionRequestEntityUsingCardActionRequestID;

		/// <summary>CTor</summary>
		static StaticProductOnCardRelations()
		{
		}
	}
}
