﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningLevelPostingKey. </summary>
	public partial class DunningLevelPostingKeyRelations
	{
		/// <summary>CTor</summary>
		public DunningLevelPostingKeyRelations()
		{
		}

		/// <summary>Gets all relations of the DunningLevelPostingKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningLevelConfigurationEntityUsingDunningLevelID);
			toReturn.Add(this.PostingKeyEntityUsingPostingKeyID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DunningLevelPostingKeyEntity and DunningLevelConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningLevelPostingKey.DunningLevelID - DunningLevelConfiguration.DunningLevelID
		/// </summary>
		public virtual IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DunningLevel", false);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningLevelPostingKeyFields.DunningLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelPostingKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningLevelPostingKeyEntity and PostingKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningLevelPostingKey.PostingKeyID - PostingKey.PostingKeyID
		/// </summary>
		public virtual IEntityRelation PostingKeyEntityUsingPostingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PostingKey", false);
				relation.AddEntityFieldPair(PostingKeyFields.PostingKeyID, DunningLevelPostingKeyFields.PostingKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelPostingKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningLevelPostingKeyRelations
	{
		internal static readonly IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelIDStatic = new DunningLevelPostingKeyRelations().DunningLevelConfigurationEntityUsingDunningLevelID;
		internal static readonly IEntityRelation PostingKeyEntityUsingPostingKeyIDStatic = new DunningLevelPostingKeyRelations().PostingKeyEntityUsingPostingKeyID;

		/// <summary>CTor</summary>
		static StaticDunningLevelPostingKeyRelations()
		{
		}
	}
}
