﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EventSetting. </summary>
	public partial class EventSettingRelations
	{
		/// <summary>CTor</summary>
		public EventSettingRelations()
		{
		}

		/// <summary>Gets all relations of the EventSettingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EmailEventEntityUsingEmailEventID);
			toReturn.Add(this.MessageTypeEntityUsingMessageTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between EventSettingEntity and EmailEventEntity over the m:1 relation they have, using the relation between the fields:
		/// EventSetting.EmailEventID - EmailEvent.EmailEventID
		/// </summary>
		public virtual IEntityRelation EmailEventEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailEvent", false);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, EventSettingFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EventSettingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EventSettingEntity and MessageTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// EventSetting.MessageTypeID - MessageType.EnumerationValue
		/// </summary>
		public virtual IEntityRelation MessageTypeEntityUsingMessageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageType", false);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, EventSettingFields.MessageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EventSettingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEventSettingRelations
	{
		internal static readonly IEntityRelation EmailEventEntityUsingEmailEventIDStatic = new EventSettingRelations().EmailEventEntityUsingEmailEventID;
		internal static readonly IEntityRelation MessageTypeEntityUsingMessageTypeIDStatic = new EventSettingRelations().MessageTypeEntityUsingMessageTypeID;

		/// <summary>CTor</summary>
		static StaticEventSettingRelations()
		{
		}
	}
}
