﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CryptoCertificate. </summary>
	public partial class CryptoCertificateRelations
	{
		/// <summary>CTor</summary>
		public CryptoCertificateRelations()
		{
		}

		/// <summary>Gets all relations of the CryptoCertificateEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CryptoCertificateEntityUsingParentID);
			toReturn.Add(this.CryptoOperatorActivationkeyEntityUsingCertificateID);
			toReturn.Add(this.CryptoSamSignCertificateEntityUsingCertificateID);
			toReturn.Add(this.CryptoUicCertificateEntityUsingCertificateID);
			toReturn.Add(this.CryptoCertificateEntityUsingCertificateIDParentID);
			toReturn.Add(this.CryptoQArchiveEntityUsingQArchiveID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoCertificateEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoCertificate.CertificateID - CryptoCertificate.ParentID
		/// </summary>
		public virtual IEntityRelation CryptoCertificateEntityUsingParentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ChildCertificates" , true);
				relation.AddEntityFieldPair(CryptoCertificateFields.CertificateID, CryptoCertificateFields.ParentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoOperatorActivationkeyEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoCertificate.CertificateID - CryptoOperatorActivationkey.CertificateID
		/// </summary>
		public virtual IEntityRelation CryptoOperatorActivationkeyEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoOperatorActivationkeys" , true);
				relation.AddEntityFieldPair(CryptoCertificateFields.CertificateID, CryptoOperatorActivationkeyFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoOperatorActivationkeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoSamSignCertificateEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoCertificate.CertificateID - CryptoSamSignCertificate.CertificateID
		/// </summary>
		public virtual IEntityRelation CryptoSamSignCertificateEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoSamSignCertificates" , true);
				relation.AddEntityFieldPair(CryptoCertificateFields.CertificateID, CryptoSamSignCertificateFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoSamSignCertificateEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoUicCertificateEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoCertificate.CertificateID - CryptoUicCertificate.CertificateID
		/// </summary>
		public virtual IEntityRelation CryptoUicCertificateEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoUicCertificates" , true);
				relation.AddEntityFieldPair(CryptoCertificateFields.CertificateID, CryptoUicCertificateFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoUicCertificateEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoCertificateEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoCertificate.ParentID - CryptoCertificate.CertificateID
		/// </summary>
		public virtual IEntityRelation CryptoCertificateEntityUsingCertificateIDParentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParentCertificate", false);
				relation.AddEntityFieldPair(CryptoCertificateFields.CertificateID, CryptoCertificateFields.ParentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CryptoCertificateEntity and CryptoQArchiveEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoCertificate.QArchiveID - CryptoQArchive.QArchiveID
		/// </summary>
		public virtual IEntityRelation CryptoQArchiveEntityUsingQArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoQArchive", false);
				relation.AddEntityFieldPair(CryptoQArchiveFields.QArchiveID, CryptoCertificateFields.QArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoQArchiveEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCertificateEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCryptoCertificateRelations
	{
		internal static readonly IEntityRelation CryptoCertificateEntityUsingParentIDStatic = new CryptoCertificateRelations().CryptoCertificateEntityUsingParentID;
		internal static readonly IEntityRelation CryptoOperatorActivationkeyEntityUsingCertificateIDStatic = new CryptoCertificateRelations().CryptoOperatorActivationkeyEntityUsingCertificateID;
		internal static readonly IEntityRelation CryptoSamSignCertificateEntityUsingCertificateIDStatic = new CryptoCertificateRelations().CryptoSamSignCertificateEntityUsingCertificateID;
		internal static readonly IEntityRelation CryptoUicCertificateEntityUsingCertificateIDStatic = new CryptoCertificateRelations().CryptoUicCertificateEntityUsingCertificateID;
		internal static readonly IEntityRelation CryptoCertificateEntityUsingCertificateIDParentIDStatic = new CryptoCertificateRelations().CryptoCertificateEntityUsingCertificateIDParentID;
		internal static readonly IEntityRelation CryptoQArchiveEntityUsingQArchiveIDStatic = new CryptoCertificateRelations().CryptoQArchiveEntityUsingQArchiveID;

		/// <summary>CTor</summary>
		static StaticCryptoCertificateRelations()
		{
		}
	}
}
