﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RouteName. </summary>
	public partial class RouteNameRelations
	{
		/// <summary>CTor</summary>
		public RouteNameRelations()
		{
		}

		/// <summary>Gets all relations of the RouteNameEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ChoiceEntityUsingRouteNameID);
			toReturn.Add(this.FareMatrixEntryEntityUsingRouteNameID);
			toReturn.Add(this.RouteEntityUsingRouteNameID);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RouteNameEntity and ChoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// RouteName.RouteNameid - Choice.RouteNameID
		/// </summary>
		public virtual IEntityRelation ChoiceEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Choices" , true);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, ChoiceFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteNameEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// RouteName.RouteNameid - FareMatrixEntry.RouteNameID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntry" , true);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, FareMatrixEntryFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RouteNameEntity and RouteEntity over the 1:n relation they have, using the relation between the fields:
		/// RouteName.RouteNameid - Route.RouteNameID
		/// </summary>
		public virtual IEntityRelation RouteEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Routes" , true);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, RouteFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RouteNameEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// RouteName.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, RouteNameFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRouteNameRelations
	{
		internal static readonly IEntityRelation ChoiceEntityUsingRouteNameIDStatic = new RouteNameRelations().ChoiceEntityUsingRouteNameID;
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingRouteNameIDStatic = new RouteNameRelations().FareMatrixEntryEntityUsingRouteNameID;
		internal static readonly IEntityRelation RouteEntityUsingRouteNameIDStatic = new RouteNameRelations().RouteEntityUsingRouteNameID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new RouteNameRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticRouteNameRelations()
		{
		}
	}
}
