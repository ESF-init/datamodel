﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractAddress. </summary>
	public partial class ContractAddressRelations
	{
		/// <summary>CTor</summary>
		public ContractAddressRelations()
		{
		}

		/// <summary>Gets all relations of the ContractAddressEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AddressEntityUsingAddressID);
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ContractAddressEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractAddress.AddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Address", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, ContractAddressFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractAddressEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractAddressEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractAddress.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractAddressFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractAddressEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractAddressRelations
	{
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new ContractAddressRelations().AddressEntityUsingAddressID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ContractAddressRelations().ContractEntityUsingContractID;

		/// <summary>CTor</summary>
		static StaticContractAddressRelations()
		{
		}
	}
}
