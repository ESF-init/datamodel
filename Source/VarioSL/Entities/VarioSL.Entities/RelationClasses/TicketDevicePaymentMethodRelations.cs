﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketDevicePaymentMethod. </summary>
	public partial class TicketDevicePaymentMethodRelations
	{
		/// <summary>CTor</summary>
		public TicketDevicePaymentMethodRelations()
		{
		}

		/// <summary>Gets all relations of the TicketDevicePaymentMethodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DevicePaymentMethodEntityUsingDevicePaymentMethodID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketDevicePaymentMethodEntity and DevicePaymentMethodEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDevicePaymentMethod.DevicePaymentMethodID - DevicePaymentMethod.DevicePaymentMethodID
		/// </summary>
		public virtual IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DevicePaymentMethod", false);
				relation.AddEntityFieldPair(DevicePaymentMethodFields.DevicePaymentMethodID, TicketDevicePaymentMethodFields.DevicePaymentMethodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DevicePaymentMethodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDevicePaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDevicePaymentMethodEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDevicePaymentMethod.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketDevicePaymentMethodFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDevicePaymentMethodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketDevicePaymentMethodRelations
	{
		internal static readonly IEntityRelation DevicePaymentMethodEntityUsingDevicePaymentMethodIDStatic = new TicketDevicePaymentMethodRelations().DevicePaymentMethodEntityUsingDevicePaymentMethodID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketDevicePaymentMethodRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticTicketDevicePaymentMethodRelations()
		{
		}
	}
}
