﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Person. </summary>
	public partial class PersonRelations : IRelationFactory
	{
		/// <summary>CTor</summary>
		public PersonRelations()
		{
		}

		/// <summary>Gets all relations of the PersonEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BankConnectionDataEntityUsingAccountOwnerID);
			toReturn.Add(this.CustomAttributeValueToPersonEntityUsingPersonID);
			toReturn.Add(this.CustomerAccountEntityUsingPersonID);
			toReturn.Add(this.EmailEventResendLockEntityUsingPersonID);
			toReturn.Add(this.EntitlementEntityUsingPersonID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingGuardianID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingPersonID);
			toReturn.Add(this.PaymentJournalEntityUsingPersonID);
			toReturn.Add(this.PersonAddressEntityUsingPersonID);
			toReturn.Add(this.PhotographEntityUsingPersonID);
			toReturn.Add(this.RecipientGroupMemberEntityUsingPersonID);
			toReturn.Add(this.SaleEntityUsingPersonID);
			toReturn.Add(this.SurveyResponseEntityUsingPersonID);
			toReturn.Add(this.AddressEntityUsingAddressID);
			toReturn.Add(this.OrganizationEntityUsingContactPersonID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and BankConnectionDataEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - BankConnectionData.AccountOwnerID
		/// </summary>
		public virtual IEntityRelation BankConnectionDataEntityUsingAccountOwnerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BankConnectionDatas" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, BankConnectionDataFields.AccountOwnerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and CustomAttributeValueToPersonEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - CustomAttributeValueToPerson.PersonID
		/// </summary>
		public virtual IEntityRelation CustomAttributeValueToPersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomAttributeValueToPeople" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, CustomAttributeValueToPersonFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueToPersonEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and CustomerAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - CustomerAccount.PersonID
		/// </summary>
		public virtual IEntityRelation CustomerAccountEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccounts" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, CustomerAccountFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - EmailEventResendLock.PersonID
		/// </summary>
		public virtual IEntityRelation EmailEventResendLockEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, EmailEventResendLockFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and EntitlementEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - Entitlement.PersonID
		/// </summary>
		public virtual IEntityRelation EntitlementEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Entitlements" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, EntitlementFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - FareEvasionIncident.GuardianID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingGuardianID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidentsAsGuardian" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, FareEvasionIncidentFields.GuardianID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - FareEvasionIncident.PersonID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidentsAsPerson" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, FareEvasionIncidentFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - PaymentJournal.PersonID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, PaymentJournalFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and PersonAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - PersonAddress.PersonID
		/// </summary>
		public virtual IEntityRelation PersonAddressEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PersonAddresses" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, PersonAddressFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and PhotographEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - Photograph.PersonID
		/// </summary>
		public virtual IEntityRelation PhotographEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Photographs" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, PhotographFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and RecipientGroupMemberEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - RecipientGroupMember.PersonID
		/// </summary>
		public virtual IEntityRelation RecipientGroupMemberEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RecipientGroupMembers" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, RecipientGroupMemberFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupMemberEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - Sale.PersonID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, SaleFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and SurveyResponseEntity over the 1:n relation they have, using the relation between the fields:
		/// Person.PersonID - SurveyResponse.PersonID
		/// </summary>
		public virtual IEntityRelation SurveyResponseEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyResponses" , true);
				relation.AddEntityFieldPair(PersonFields.PersonID, SurveyResponseFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and AddressEntity over the 1:1 relation they have, using the relation between the fields:
		/// Person.AddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Address", false);




				relation.AddEntityFieldPair(AddressFields.AddressID, PersonFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PersonEntity and OrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Person.PersonID - Organization.ContactPersonID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingContactPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ContactPersonOnOrganization", true);


				relation.AddEntityFieldPair(PersonFields.PersonID, OrganizationFields.ContactPersonID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPersonRelations
	{
		internal static readonly IEntityRelation BankConnectionDataEntityUsingAccountOwnerIDStatic = new PersonRelations().BankConnectionDataEntityUsingAccountOwnerID;
		internal static readonly IEntityRelation CustomAttributeValueToPersonEntityUsingPersonIDStatic = new PersonRelations().CustomAttributeValueToPersonEntityUsingPersonID;
		internal static readonly IEntityRelation CustomerAccountEntityUsingPersonIDStatic = new PersonRelations().CustomerAccountEntityUsingPersonID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingPersonIDStatic = new PersonRelations().EmailEventResendLockEntityUsingPersonID;
		internal static readonly IEntityRelation EntitlementEntityUsingPersonIDStatic = new PersonRelations().EntitlementEntityUsingPersonID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingGuardianIDStatic = new PersonRelations().FareEvasionIncidentEntityUsingGuardianID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingPersonIDStatic = new PersonRelations().FareEvasionIncidentEntityUsingPersonID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPersonIDStatic = new PersonRelations().PaymentJournalEntityUsingPersonID;
		internal static readonly IEntityRelation PersonAddressEntityUsingPersonIDStatic = new PersonRelations().PersonAddressEntityUsingPersonID;
		internal static readonly IEntityRelation PhotographEntityUsingPersonIDStatic = new PersonRelations().PhotographEntityUsingPersonID;
		internal static readonly IEntityRelation RecipientGroupMemberEntityUsingPersonIDStatic = new PersonRelations().RecipientGroupMemberEntityUsingPersonID;
		internal static readonly IEntityRelation SaleEntityUsingPersonIDStatic = new PersonRelations().SaleEntityUsingPersonID;
		internal static readonly IEntityRelation SurveyResponseEntityUsingPersonIDStatic = new PersonRelations().SurveyResponseEntityUsingPersonID;
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new PersonRelations().AddressEntityUsingAddressID;
		internal static readonly IEntityRelation OrganizationEntityUsingContactPersonIDStatic = new PersonRelations().OrganizationEntityUsingContactPersonID;

		/// <summary>CTor</summary>
		static StaticPersonRelations()
		{
		}
	}
}
