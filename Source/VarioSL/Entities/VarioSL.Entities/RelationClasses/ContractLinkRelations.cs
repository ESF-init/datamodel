﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractLink. </summary>
	public partial class ContractLinkRelations
	{
		/// <summary>CTor</summary>
		public ContractLinkRelations()
		{
		}

		/// <summary>Gets all relations of the ContractLinkEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractEntityUsingSourceContractID);
			toReturn.Add(this.ContractEntityUsingTargetContractID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ContractLinkEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractLink.SourceContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingSourceContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SourceContract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractLinkFields.SourceContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractLinkEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractLinkEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractLink.TargetContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingTargetContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TargetContract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractLinkFields.TargetContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractLinkEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractLinkRelations
	{
		internal static readonly IEntityRelation ContractEntityUsingSourceContractIDStatic = new ContractLinkRelations().ContractEntityUsingSourceContractID;
		internal static readonly IEntityRelation ContractEntityUsingTargetContractIDStatic = new ContractLinkRelations().ContractEntityUsingTargetContractID;

		/// <summary>CTor</summary>
		static StaticContractLinkRelations()
		{
		}
	}
}
