﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CashServiceBalance. </summary>
	public partial class CashServiceBalanceRelations
	{
		/// <summary>CTor</summary>
		public CashServiceBalanceRelations()
		{
		}

		/// <summary>Gets all relations of the CashServiceBalanceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceEntityUsingCashServiceID);
			toReturn.Add(this.ComponentTypeEntityUsingComponentTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CashServiceBalanceEntity and CashServiceEntity over the m:1 relation they have, using the relation between the fields:
		/// CashServiceBalance.CashServiceID - CashService.CashServiceID
		/// </summary>
		public virtual IEntityRelation CashServiceEntityUsingCashServiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CashService", false);
				relation.AddEntityFieldPair(CashServiceFields.CashServiceID, CashServiceBalanceFields.CashServiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceBalanceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CashServiceBalanceEntity and ComponentTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// CashServiceBalance.ComponentTypeID - ComponentType.ComponentTypeID
		/// </summary>
		public virtual IEntityRelation ComponentTypeEntityUsingComponentTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ComponentType", false);
				relation.AddEntityFieldPair(ComponentTypeFields.ComponentTypeID, CashServiceBalanceFields.ComponentTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceBalanceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCashServiceBalanceRelations
	{
		internal static readonly IEntityRelation CashServiceEntityUsingCashServiceIDStatic = new CashServiceBalanceRelations().CashServiceEntityUsingCashServiceID;
		internal static readonly IEntityRelation ComponentTypeEntityUsingComponentTypeIDStatic = new CashServiceBalanceRelations().ComponentTypeEntityUsingComponentTypeID;

		/// <summary>CTor</summary>
		static StaticCashServiceBalanceRelations()
		{
		}
	}
}
