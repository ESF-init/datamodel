﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceReaderKeyToCert. </summary>
	public partial class DeviceReaderKeyToCertRelations
	{
		/// <summary>CTor</summary>
		public DeviceReaderKeyToCertRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceReaderKeyToCertEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CertificateEntityUsingCertificateID);
			toReturn.Add(this.DeviceReaderKeyEntityUsingDeviceReaderKeyID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DeviceReaderKeyToCertEntity and CertificateEntity over the m:1 relation they have, using the relation between the fields:
		/// DeviceReaderKeyToCert.CertificateID - Certificate.CertificateID
		/// </summary>
		public virtual IEntityRelation CertificateEntityUsingCertificateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Certificate", false);
				relation.AddEntityFieldPair(CertificateFields.CertificateID, DeviceReaderKeyToCertFields.CertificateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CertificateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyToCertEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DeviceReaderKeyToCertEntity and DeviceReaderKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// DeviceReaderKeyToCert.DeviceReaderKeyID - DeviceReaderKey.DeviceReaderKeyID
		/// </summary>
		public virtual IEntityRelation DeviceReaderKeyEntityUsingDeviceReaderKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceReaderKey", false);
				relation.AddEntityFieldPair(DeviceReaderKeyFields.DeviceReaderKeyID, DeviceReaderKeyToCertFields.DeviceReaderKeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyToCertEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceReaderKeyToCertRelations
	{
		internal static readonly IEntityRelation CertificateEntityUsingCertificateIDStatic = new DeviceReaderKeyToCertRelations().CertificateEntityUsingCertificateID;
		internal static readonly IEntityRelation DeviceReaderKeyEntityUsingDeviceReaderKeyIDStatic = new DeviceReaderKeyToCertRelations().DeviceReaderKeyEntityUsingDeviceReaderKeyID;

		/// <summary>CTor</summary>
		static StaticDeviceReaderKeyToCertRelations()
		{
		}
	}
}
