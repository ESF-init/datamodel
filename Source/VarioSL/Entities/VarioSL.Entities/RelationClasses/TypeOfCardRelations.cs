﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TypeOfCard. </summary>
	public partial class TypeOfCardRelations
	{
		/// <summary>CTor</summary>
		public TypeOfCardRelations()
		{
		}

		/// <summary>Gets all relations of the TypeOfCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DebtorCardEntityUsingTypeOfCardID);
			toReturn.Add(this.ExternalPacketEffortEntityUsingCardTypeId);
			toReturn.Add(this.DefaultPinEntityUsingTypeOfCardID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TypeOfCardEntity and DebtorCardEntity over the 1:n relation they have, using the relation between the fields:
		/// TypeOfCard.TypeOfCardID - DebtorCard.TypeOfCardID
		/// </summary>
		public virtual IEntityRelation DebtorCardEntityUsingTypeOfCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DebtorCards" , true);
				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, DebtorCardFields.TypeOfCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TypeOfCardEntity and ExternalPacketEffortEntity over the 1:n relation they have, using the relation between the fields:
		/// TypeOfCard.TypeOfCardID - ExternalPacketEffort.CardTypeId
		/// </summary>
		public virtual IEntityRelation ExternalPacketEffortEntityUsingCardTypeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ExternalPacketefforts" , true);
				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, ExternalPacketEffortFields.CardTypeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TypeOfCardEntity and DefaultPinEntity over the 1:1 relation they have, using the relation between the fields:
		/// TypeOfCard.TypeOfCardID - DefaultPin.TypeOfCardID
		/// </summary>
		public virtual IEntityRelation DefaultPinEntityUsingTypeOfCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "DefaultPin", true);


				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, DefaultPinFields.TypeOfCardID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DefaultPinEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTypeOfCardRelations
	{
		internal static readonly IEntityRelation DebtorCardEntityUsingTypeOfCardIDStatic = new TypeOfCardRelations().DebtorCardEntityUsingTypeOfCardID;
		internal static readonly IEntityRelation ExternalPacketEffortEntityUsingCardTypeIdStatic = new TypeOfCardRelations().ExternalPacketEffortEntityUsingCardTypeId;
		internal static readonly IEntityRelation DefaultPinEntityUsingTypeOfCardIDStatic = new TypeOfCardRelations().DefaultPinEntityUsingTypeOfCardID;

		/// <summary>CTor</summary>
		static StaticTypeOfCardRelations()
		{
		}
	}
}
