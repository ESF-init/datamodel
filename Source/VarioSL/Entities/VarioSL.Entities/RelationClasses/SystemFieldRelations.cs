﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SystemField. </summary>
	public partial class SystemFieldRelations
	{
		/// <summary>CTor</summary>
		public SystemFieldRelations()
		{
		}

		/// <summary>Gets all relations of the SystemFieldEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SystemFieldBarcodeLayoutObjectEntityUsingSystemFieldID);
			toReturn.Add(this.SystemFieldTextLayoutObjectEntityUsingSystemFieldID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SystemFieldEntity and SystemFieldBarcodeLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// SystemField.SystemFieldID - SystemFieldBarcodeLayoutObject.SystemFieldID
		/// </summary>
		public virtual IEntityRelation SystemFieldBarcodeLayoutObjectEntityUsingSystemFieldID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SystemFieldBarcodeLayoutObjects" , true);
				relation.AddEntityFieldPair(SystemFieldFields.SystemFieldID, SystemFieldBarcodeLayoutObjectFields.SystemFieldID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldBarcodeLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SystemFieldEntity and SystemFieldTextLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// SystemField.SystemFieldID - SystemFieldTextLayoutObject.SystemFieldID
		/// </summary>
		public virtual IEntityRelation SystemFieldTextLayoutObjectEntityUsingSystemFieldID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SystemFieldTextLayoutObjects" , true);
				relation.AddEntityFieldPair(SystemFieldFields.SystemFieldID, SystemFieldTextLayoutObjectFields.SystemFieldID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SystemFieldTextLayoutObjectEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSystemFieldRelations
	{
		internal static readonly IEntityRelation SystemFieldBarcodeLayoutObjectEntityUsingSystemFieldIDStatic = new SystemFieldRelations().SystemFieldBarcodeLayoutObjectEntityUsingSystemFieldID;
		internal static readonly IEntityRelation SystemFieldTextLayoutObjectEntityUsingSystemFieldIDStatic = new SystemFieldRelations().SystemFieldTextLayoutObjectEntityUsingSystemFieldID;

		/// <summary>CTor</summary>
		static StaticSystemFieldRelations()
		{
		}
	}
}
