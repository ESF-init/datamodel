﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentReconciliation. </summary>
	public partial class PaymentReconciliationRelations
	{
		/// <summary>CTor</summary>
		public PaymentReconciliationRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentReconciliationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentJournalEntityUsingPaymentJournalID);
			toReturn.Add(this.CloseoutPeriodEntityUsingCloseoutPeriodID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between PaymentReconciliationEntity and PaymentJournalEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentReconciliation.PaymentJournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentJournal", false);




				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentReconciliationFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentReconciliationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentReconciliationEntity and CloseoutPeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentReconciliation.CloseoutPeriodID - CloseoutPeriod.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CloseoutPeriod", false);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, PaymentReconciliationFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentReconciliationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentReconciliationRelations
	{
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentJournalIDStatic = new PaymentReconciliationRelations().PaymentJournalEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodIDStatic = new PaymentReconciliationRelations().CloseoutPeriodEntityUsingCloseoutPeriodID;

		/// <summary>CTor</summary>
		static StaticPaymentReconciliationRelations()
		{
		}
	}
}
