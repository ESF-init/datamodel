﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardWorkItem. </summary>
	public partial class CardWorkItemRelations : WorkItemRelations
	{
		/// <summary>CTor</summary>
		public CardWorkItemRelations()
		{
		}

		/// <summary>Gets all relations of the CardWorkItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = base.GetAllRelations();
			toReturn.Add(this.CardEntityUsingCardID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardWorkItemEntity and WorkItemHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// CardWorkItem.WorkItemID - WorkItemHistory.WorkItemID
		/// </summary>
		public override IEntityRelation WorkItemHistoryEntityUsingWorkItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItemHistory" , true);
				relation.AddEntityFieldPair(CardWorkItemFields.WorkItemID, WorkItemHistoryFields.WorkItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardWorkItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CardWorkItemEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// CardWorkItem.Assignee - UserList.UserID
		/// </summary>
		public override IEntityRelation UserListEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, CardWorkItemFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardWorkItemEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// CardWorkItem.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, CardWorkItemFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardWorkItemEntity and WorkItemSubjectEntity over the m:1 relation they have, using the relation between the fields:
		/// CardWorkItem.WorkItemSubjectID - WorkItemSubject.WorkItemSubjectID
		/// </summary>
		public override IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WorkItemSubject", false);
				relation.AddEntityFieldPair(WorkItemSubjectFields.WorkItemSubjectID, CardWorkItemFields.WorkItemSubjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemSubjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardWorkItemRelations
	{
		internal static readonly IEntityRelation WorkItemHistoryEntityUsingWorkItemIDStatic = new CardWorkItemRelations().WorkItemHistoryEntityUsingWorkItemID;
		internal static readonly IEntityRelation UserListEntityUsingAssigneeStatic = new CardWorkItemRelations().UserListEntityUsingAssignee;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new CardWorkItemRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectIDStatic = new CardWorkItemRelations().WorkItemSubjectEntityUsingWorkItemSubjectID;

		/// <summary>CTor</summary>
		static StaticCardWorkItemRelations()
		{
		}
	}
}
