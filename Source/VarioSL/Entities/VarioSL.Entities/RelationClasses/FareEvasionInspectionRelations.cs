﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareEvasionInspection. </summary>
	public partial class FareEvasionInspectionRelations
	{
		/// <summary>CTor</summary>
		public FareEvasionInspectionRelations()
		{
		}

		/// <summary>Gets all relations of the FareEvasionInspectionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.InspectionReportEntityUsingFareEvasionInspectionID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareEvasionInspectionEntity and InspectionReportEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionInspection.FareEvasionInspectionID - InspectionReport.FareEvasionInspectionID
		/// </summary>
		public virtual IEntityRelation InspectionReportEntityUsingFareEvasionInspectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "InspectionReports" , true);
				relation.AddEntityFieldPair(FareEvasionInspectionFields.FareEvasionInspectionID, InspectionReportFields.FareEvasionInspectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionInspectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareEvasionInspectionEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionInspection.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareEvasionInspectionFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionInspectionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareEvasionInspectionRelations
	{
		internal static readonly IEntityRelation InspectionReportEntityUsingFareEvasionInspectionIDStatic = new FareEvasionInspectionRelations().InspectionReportEntityUsingFareEvasionInspectionID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new FareEvasionInspectionRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticFareEvasionInspectionRelations()
		{
		}
	}
}
