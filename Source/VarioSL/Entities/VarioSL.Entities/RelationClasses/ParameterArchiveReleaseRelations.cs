﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParameterArchiveRelease. </summary>
	public partial class ParameterArchiveReleaseRelations
	{
		/// <summary>CTor</summary>
		public ParameterArchiveReleaseRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterArchiveReleaseEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.ParameterArchiveEntityUsingParameterArchiveID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveReleaseEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterArchiveRelease.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterArchiveReleaseFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveReleaseEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ParameterArchiveReleaseEntity and ParameterArchiveEntity over the m:1 relation they have, using the relation between the fields:
		/// ParameterArchiveRelease.ParameterArchiveID - ParameterArchive.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParameterArchive", false);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, ParameterArchiveReleaseFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveReleaseEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterArchiveReleaseRelations
	{
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new ParameterArchiveReleaseRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation ParameterArchiveEntityUsingParameterArchiveIDStatic = new ParameterArchiveReleaseRelations().ParameterArchiveEntityUsingParameterArchiveID;

		/// <summary>CTor</summary>
		static StaticParameterArchiveReleaseRelations()
		{
		}
	}
}
