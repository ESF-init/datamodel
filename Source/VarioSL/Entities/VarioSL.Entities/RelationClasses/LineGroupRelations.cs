﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LineGroup. </summary>
	public partial class LineGroupRelations
	{
		/// <summary>CTor</summary>
		public LineGroupRelations()
		{
		}

		/// <summary>Gets all relations of the LineGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LineGroupToLineGroupEntityUsingLineGroupID);
			toReturn.Add(this.LineGroupToLineGroupEntityUsingMasterLineGroupID);
			toReturn.Add(this.LineToLineGroupEntityUsingLineGroupID);
			toReturn.Add(this.TicketEntityUsingLineGroupID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LineGroupEntity and LineGroupToLineGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// LineGroup.LineGroupID - LineGroupToLineGroup.LineGroupID
		/// </summary>
		public virtual IEntityRelation LineGroupToLineGroupEntityUsingLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MasterLineGroupId" , true);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, LineGroupToLineGroupFields.LineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupToLineGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LineGroupEntity and LineGroupToLineGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// LineGroup.LineGroupID - LineGroupToLineGroup.MasterLineGroupID
		/// </summary>
		public virtual IEntityRelation LineGroupToLineGroupEntityUsingMasterLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SubLineGroupIds" , true);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, LineGroupToLineGroupFields.MasterLineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupToLineGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LineGroupEntity and LineToLineGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// LineGroup.LineGroupID - LineToLineGroup.LineGroupID
		/// </summary>
		public virtual IEntityRelation LineToLineGroupEntityUsingLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LineToLineGroup" , true);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, LineToLineGroupFields.LineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineToLineGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LineGroupEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// LineGroup.LineGroupID - Ticket.LineGroupID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Ticket" , true);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, TicketFields.LineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LineGroupEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// LineGroup.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, LineGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLineGroupRelations
	{
		internal static readonly IEntityRelation LineGroupToLineGroupEntityUsingLineGroupIDStatic = new LineGroupRelations().LineGroupToLineGroupEntityUsingLineGroupID;
		internal static readonly IEntityRelation LineGroupToLineGroupEntityUsingMasterLineGroupIDStatic = new LineGroupRelations().LineGroupToLineGroupEntityUsingMasterLineGroupID;
		internal static readonly IEntityRelation LineToLineGroupEntityUsingLineGroupIDStatic = new LineGroupRelations().LineToLineGroupEntityUsingLineGroupID;
		internal static readonly IEntityRelation TicketEntityUsingLineGroupIDStatic = new LineGroupRelations().TicketEntityUsingLineGroupID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new LineGroupRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticLineGroupRelations()
		{
		}
	}
}
