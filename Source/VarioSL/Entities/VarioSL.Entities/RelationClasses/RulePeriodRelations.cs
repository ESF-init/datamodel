﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RulePeriod. </summary>
	public partial class RulePeriodRelations
	{
		/// <summary>CTor</summary>
		public RulePeriodRelations()
		{
		}

		/// <summary>Gets all relations of the RulePeriodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RuleCappingEntityUsingRulePeriodID);
			toReturn.Add(this.TicketEntityUsingRulePeriodID);
			toReturn.Add(this.TicketDeviceClassEntityUsingRulePeriodID);
			toReturn.Add(this.VdvProductEntityUsingPeriodID);
			toReturn.Add(this.CalendarEntityUsingCalendarID);
			toReturn.Add(this.CalendarEntityUsingExtCalendarID);
			toReturn.Add(this.RuleTypeEntityUsingPeriodUnit);
			toReturn.Add(this.RuleTypeEntityUsingValidTimeUnit);
			toReturn.Add(this.RuleTypeEntityUsingValidationType);
			toReturn.Add(this.RuleTypeEntityUsingExtTimeUnit);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and RuleCappingEntity over the 1:n relation they have, using the relation between the fields:
		/// RulePeriod.RulePeriodID - RuleCapping.RulePeriodID
		/// </summary>
		public virtual IEntityRelation RuleCappingEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCapping" , true);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, RuleCappingFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// RulePeriod.RulePeriodID - Ticket.RulePeriodID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, TicketFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// RulePeriod.RulePeriodID - TicketDeviceClass.RulePeriodID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClass" , true);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, TicketDeviceClassFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// RulePeriod.RulePeriodID - VdvProduct.PeriodID
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProduct" , true);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, VdvProductFields.PeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.CalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Calendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RulePeriodFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.ExtCalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingExtCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExtensionCalendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RulePeriodFields.ExtCalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.PeriodUnit - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingPeriodUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleTypePeriodUnit", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.PeriodUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.ValidTimeUnit - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingValidTimeUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleTypesValidTimeUnit", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ValidTimeUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.ValidationType - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingValidationType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleTypeValidationType", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ValidationType);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RulePeriodEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// RulePeriod.ExtTimeUnit - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingExtTimeUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleTypeExtTimeUnit", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ExtTimeUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRulePeriodRelations
	{
		internal static readonly IEntityRelation RuleCappingEntityUsingRulePeriodIDStatic = new RulePeriodRelations().RuleCappingEntityUsingRulePeriodID;
		internal static readonly IEntityRelation TicketEntityUsingRulePeriodIDStatic = new RulePeriodRelations().TicketEntityUsingRulePeriodID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingRulePeriodIDStatic = new RulePeriodRelations().TicketDeviceClassEntityUsingRulePeriodID;
		internal static readonly IEntityRelation VdvProductEntityUsingPeriodIDStatic = new RulePeriodRelations().VdvProductEntityUsingPeriodID;
		internal static readonly IEntityRelation CalendarEntityUsingCalendarIDStatic = new RulePeriodRelations().CalendarEntityUsingCalendarID;
		internal static readonly IEntityRelation CalendarEntityUsingExtCalendarIDStatic = new RulePeriodRelations().CalendarEntityUsingExtCalendarID;
		internal static readonly IEntityRelation RuleTypeEntityUsingPeriodUnitStatic = new RulePeriodRelations().RuleTypeEntityUsingPeriodUnit;
		internal static readonly IEntityRelation RuleTypeEntityUsingValidTimeUnitStatic = new RulePeriodRelations().RuleTypeEntityUsingValidTimeUnit;
		internal static readonly IEntityRelation RuleTypeEntityUsingValidationTypeStatic = new RulePeriodRelations().RuleTypeEntityUsingValidationType;
		internal static readonly IEntityRelation RuleTypeEntityUsingExtTimeUnitStatic = new RulePeriodRelations().RuleTypeEntityUsingExtTimeUnit;

		/// <summary>CTor</summary>
		static StaticRulePeriodRelations()
		{
		}
	}
}
