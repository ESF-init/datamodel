﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserKey. </summary>
	public partial class UserKeyRelations
	{
		/// <summary>CTor</summary>
		public UserKeyRelations()
		{
		}

		/// <summary>Gets all relations of the UserKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketEntityUsingKey1);
			toReturn.Add(this.TicketEntityUsingKey2);
			toReturn.Add(this.TicketDeviceClassEntityUsingKey1);
			toReturn.Add(this.TicketDeviceClassEntityUsingKey2);
			toReturn.Add(this.PanelEntityUsingDestinationPanelID);
			toReturn.Add(this.PanelEntityUsingPanelID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// UserKey.KeyID - Ticket.Key1
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingKey1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsKey1" , true);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketFields.Key1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// UserKey.KeyID - Ticket.Key2
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingKey2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsKey2" , true);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketFields.Key2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// UserKey.KeyID - TicketDeviceClass.Key1
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingKey1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesKey1" , true);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketDeviceClassFields.Key1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// UserKey.KeyID - TicketDeviceClass.Key2
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingKey2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceclassesKey2" , true);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketDeviceClassFields.Key2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and PanelEntity over the m:1 relation they have, using the relation between the fields:
		/// UserKey.DestinationPanelID - Panel.PanelID
		/// </summary>
		public virtual IEntityRelation PanelEntityUsingDestinationPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DestinationPanel", false);
				relation.AddEntityFieldPair(PanelFields.PanelID, UserKeyFields.DestinationPanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and PanelEntity over the m:1 relation they have, using the relation between the fields:
		/// UserKey.PanelID - Panel.PanelID
		/// </summary>
		public virtual IEntityRelation PanelEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Panel", false);
				relation.AddEntityFieldPair(PanelFields.PanelID, UserKeyFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserKeyEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// UserKey.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, UserKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserKeyRelations
	{
		internal static readonly IEntityRelation TicketEntityUsingKey1Static = new UserKeyRelations().TicketEntityUsingKey1;
		internal static readonly IEntityRelation TicketEntityUsingKey2Static = new UserKeyRelations().TicketEntityUsingKey2;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingKey1Static = new UserKeyRelations().TicketDeviceClassEntityUsingKey1;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingKey2Static = new UserKeyRelations().TicketDeviceClassEntityUsingKey2;
		internal static readonly IEntityRelation PanelEntityUsingDestinationPanelIDStatic = new UserKeyRelations().PanelEntityUsingDestinationPanelID;
		internal static readonly IEntityRelation PanelEntityUsingPanelIDStatic = new UserKeyRelations().PanelEntityUsingPanelID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new UserKeyRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticUserKeyRelations()
		{
		}
	}
}
