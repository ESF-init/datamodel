﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UnitCollectionToUnit. </summary>
	public partial class UnitCollectionToUnitRelations
	{
		/// <summary>CTor</summary>
		public UnitCollectionToUnitRelations()
		{
		}

		/// <summary>Gets all relations of the UnitCollectionToUnitEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UnitEntityUsingUnitID);
			toReturn.Add(this.UnitCollectionEntityUsingUnitCollectionID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UnitCollectionToUnitEntity and UnitEntity over the m:1 relation they have, using the relation between the fields:
		/// UnitCollectionToUnit.UnitID - Unit.UnitID
		/// </summary>
		public virtual IEntityRelation UnitEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Unit", false);
				relation.AddEntityFieldPair(UnitFields.UnitID, UnitCollectionToUnitFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionToUnitEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UnitCollectionToUnitEntity and UnitCollectionEntity over the m:1 relation they have, using the relation between the fields:
		/// UnitCollectionToUnit.UnitCollectionID - UnitCollection.UnitCollectionID
		/// </summary>
		public virtual IEntityRelation UnitCollectionEntityUsingUnitCollectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UnitCollection", false);
				relation.AddEntityFieldPair(UnitCollectionFields.UnitCollectionID, UnitCollectionToUnitFields.UnitCollectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionToUnitEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUnitCollectionToUnitRelations
	{
		internal static readonly IEntityRelation UnitEntityUsingUnitIDStatic = new UnitCollectionToUnitRelations().UnitEntityUsingUnitID;
		internal static readonly IEntityRelation UnitCollectionEntityUsingUnitCollectionIDStatic = new UnitCollectionToUnitRelations().UnitCollectionEntityUsingUnitCollectionID;

		/// <summary>CTor</summary>
		static StaticUnitCollectionToUnitRelations()
		{
		}
	}
}
