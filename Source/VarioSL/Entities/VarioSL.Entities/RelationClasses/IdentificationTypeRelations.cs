﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: IdentificationType. </summary>
	public partial class IdentificationTypeRelations
	{
		/// <summary>CTor</summary>
		public IdentificationTypeRelations()
		{
		}

		/// <summary>Gets all relations of the IdentificationTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareEvasionIncidentEntityUsingIdentificationTypeID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between IdentificationTypeEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// IdentificationType.IdentificationTypeID - FareEvasionIncident.IdentificationTypeID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingIdentificationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(IdentificationTypeFields.IdentificationTypeID, FareEvasionIncidentFields.IdentificationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IdentificationTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between IdentificationTypeEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// IdentificationType.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, IdentificationTypeFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("IdentificationTypeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticIdentificationTypeRelations
	{
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingIdentificationTypeIDStatic = new IdentificationTypeRelations().FareEvasionIncidentEntityUsingIdentificationTypeID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new IdentificationTypeRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticIdentificationTypeRelations()
		{
		}
	}
}
