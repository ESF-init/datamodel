﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AttributeToMobilityProvider. </summary>
	public partial class AttributeToMobilityProviderRelations
	{
		/// <summary>CTor</summary>
		public AttributeToMobilityProviderRelations()
		{
		}

		/// <summary>Gets all relations of the AttributeToMobilityProviderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomAttributeEntityUsingAttributeID);
			toReturn.Add(this.MobilityProviderEntityUsingMobilityProviderID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AttributeToMobilityProviderEntity and CustomAttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeToMobilityProvider.AttributeID - CustomAttribute.CustomAttributeID
		/// </summary>
		public virtual IEntityRelation CustomAttributeEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomAttribute", false);
				relation.AddEntityFieldPair(CustomAttributeFields.CustomAttributeID, AttributeToMobilityProviderFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeToMobilityProviderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AttributeToMobilityProviderEntity and MobilityProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// AttributeToMobilityProvider.MobilityProviderID - MobilityProvider.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation MobilityProviderEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MobilityProvider", false);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, AttributeToMobilityProviderFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeToMobilityProviderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttributeToMobilityProviderRelations
	{
		internal static readonly IEntityRelation CustomAttributeEntityUsingAttributeIDStatic = new AttributeToMobilityProviderRelations().CustomAttributeEntityUsingAttributeID;
		internal static readonly IEntityRelation MobilityProviderEntityUsingMobilityProviderIDStatic = new AttributeToMobilityProviderRelations().MobilityProviderEntityUsingMobilityProviderID;

		/// <summary>CTor</summary>
		static StaticAttributeToMobilityProviderRelations()
		{
		}
	}
}
