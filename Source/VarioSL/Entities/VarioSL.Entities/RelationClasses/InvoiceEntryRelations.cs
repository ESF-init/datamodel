﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InvoiceEntry. </summary>
	public partial class InvoiceEntryRelations
	{
		/// <summary>CTor</summary>
		public InvoiceEntryRelations()
		{
		}

		/// <summary>Gets all relations of the InvoiceEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PostingEntityUsingInvoiceEntryID);
			toReturn.Add(this.InvoiceEntityUsingInvoiceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InvoiceEntryEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// InvoiceEntry.InvoiceEntryID - Posting.InvoiceEntryID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingInvoiceEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(InvoiceEntryFields.InvoiceEntryID, PostingFields.InvoiceEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between InvoiceEntryEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// InvoiceEntry.InvoiceID - Invoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoice", false);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, InvoiceEntryFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInvoiceEntryRelations
	{
		internal static readonly IEntityRelation PostingEntityUsingInvoiceEntryIDStatic = new InvoiceEntryRelations().PostingEntityUsingInvoiceEntryID;
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIDStatic = new InvoiceEntryRelations().InvoiceEntityUsingInvoiceID;

		/// <summary>CTor</summary>
		static StaticInvoiceEntryRelations()
		{
		}
	}
}
