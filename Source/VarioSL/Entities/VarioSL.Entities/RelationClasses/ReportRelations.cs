﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Report. </summary>
	public partial class ReportRelations
	{
		/// <summary>CTor</summary>
		public ReportRelations()
		{
		}

		/// <summary>Gets all relations of the ReportEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportDataFileEntityUsingReportID);
			toReturn.Add(this.ReportJobEntityUsingReportID);
			toReturn.Add(this.ReportJobResultEntityUsingReportID);
			toReturn.Add(this.ReportToClientEntityUsingReportID);
			toReturn.Add(this.UserResourceEntityUsingResourceID);
			toReturn.Add(this.FilterListEntityUsingFilterListID);
			toReturn.Add(this.ReportCategoryEntityUsingCategoryID);
			toReturn.Add(this.ReportDataFileEntityUsingDataFileID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportDataFileEntity over the 1:n relation they have, using the relation between the fields:
		/// Report.ReportID - ReportDataFile.ReportID
		/// </summary>
		public virtual IEntityRelation ReportDataFileEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportDataFiles" , true);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportDataFileFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportDataFileEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportJobEntity over the 1:n relation they have, using the relation between the fields:
		/// Report.ReportID - ReportJob.ReportID
		/// </summary>
		public virtual IEntityRelation ReportJobEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobs" , true);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportJobFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportJobResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Report.ReportID - ReportJobResult.ReportID
		/// </summary>
		public virtual IEntityRelation ReportJobResultEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobResults" , true);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportJobResultFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportToClientEntity over the 1:n relation they have, using the relation between the fields:
		/// Report.ReportID - ReportToClient.ReportID
		/// </summary>
		public virtual IEntityRelation ReportToClientEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportToClients" , true);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportToClientFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportToClientEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReportEntity and UserResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// Report.ResourceID - UserResource.ResourceID
		/// </summary>
		public virtual IEntityRelation UserResourceEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserResource", false);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, ReportFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportEntity and FilterListEntity over the m:1 relation they have, using the relation between the fields:
		/// Report.FilterListID - FilterList.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterListEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterList", false);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, ReportFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportCategoryEntity over the m:1 relation they have, using the relation between the fields:
		/// Report.CategoryID - ReportCategory.ReportCategoryID
		/// </summary>
		public virtual IEntityRelation ReportCategoryEntityUsingCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReportCategory", false);
				relation.AddEntityFieldPair(ReportCategoryFields.ReportCategoryID, ReportFields.CategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportEntity and ReportDataFileEntity over the m:1 relation they have, using the relation between the fields:
		/// Report.DataFileID - ReportDataFile.ReportDataFileID
		/// </summary>
		public virtual IEntityRelation ReportDataFileEntityUsingDataFileID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DefaultReportDataFile", false);
				relation.AddEntityFieldPair(ReportDataFileFields.ReportDataFileID, ReportFields.DataFileID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportDataFileEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportRelations
	{
		internal static readonly IEntityRelation ReportDataFileEntityUsingReportIDStatic = new ReportRelations().ReportDataFileEntityUsingReportID;
		internal static readonly IEntityRelation ReportJobEntityUsingReportIDStatic = new ReportRelations().ReportJobEntityUsingReportID;
		internal static readonly IEntityRelation ReportJobResultEntityUsingReportIDStatic = new ReportRelations().ReportJobResultEntityUsingReportID;
		internal static readonly IEntityRelation ReportToClientEntityUsingReportIDStatic = new ReportRelations().ReportToClientEntityUsingReportID;
		internal static readonly IEntityRelation UserResourceEntityUsingResourceIDStatic = new ReportRelations().UserResourceEntityUsingResourceID;
		internal static readonly IEntityRelation FilterListEntityUsingFilterListIDStatic = new ReportRelations().FilterListEntityUsingFilterListID;
		internal static readonly IEntityRelation ReportCategoryEntityUsingCategoryIDStatic = new ReportRelations().ReportCategoryEntityUsingCategoryID;
		internal static readonly IEntityRelation ReportDataFileEntityUsingDataFileIDStatic = new ReportRelations().ReportDataFileEntityUsingDataFileID;

		/// <summary>CTor</summary>
		static StaticReportRelations()
		{
		}
	}
}
