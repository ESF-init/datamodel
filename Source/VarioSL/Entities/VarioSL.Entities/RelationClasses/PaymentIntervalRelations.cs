﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentInterval. </summary>
	public partial class PaymentIntervalRelations
	{
		/// <summary>CTor</summary>
		public PaymentIntervalRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentIntervalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketPaymentIntervalEntityUsingPaymentIntervalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentIntervalEntity and TicketPaymentIntervalEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentInterval.PaymentIntervalID - TicketPaymentInterval.PaymentIntervalID
		/// </summary>
		public virtual IEntityRelation TicketPaymentIntervalEntityUsingPaymentIntervalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketPaymentIntervals" , true);
				relation.AddEntityFieldPair(PaymentIntervalFields.PaymentIntervalID, TicketPaymentIntervalFields.PaymentIntervalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentIntervalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPaymentIntervalEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentIntervalRelations
	{
		internal static readonly IEntityRelation TicketPaymentIntervalEntityUsingPaymentIntervalIDStatic = new PaymentIntervalRelations().TicketPaymentIntervalEntityUsingPaymentIntervalID;

		/// <summary>CTor</summary>
		static StaticPaymentIntervalRelations()
		{
		}
	}
}
