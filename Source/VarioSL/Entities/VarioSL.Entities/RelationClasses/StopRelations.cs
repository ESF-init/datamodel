﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Stop. </summary>
	public partial class StopRelations
	{
		/// <summary>CTor</summary>
		public StopRelations()
		{
		}

		/// <summary>Gets all relations of the StopEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NetEntityUsingNetId);
			toReturn.Add(this.CoordinateEntityUsingCoordinateID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between StopEntity and NetEntity over the m:1 relation they have, using the relation between the fields:
		/// Stop.NetId - Net.NetID
		/// </summary>
		public virtual IEntityRelation NetEntityUsingNetId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Net", false);
				relation.AddEntityFieldPair(NetFields.NetID, StopFields.NetId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StopEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between StopEntity and CoordinateEntity over the m:1 relation they have, using the relation between the fields:
		/// Stop.CoordinateID - Coordinate.CoordinateID
		/// </summary>
		public virtual IEntityRelation CoordinateEntityUsingCoordinateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Coordinate", false);
				relation.AddEntityFieldPair(CoordinateFields.CoordinateID, StopFields.CoordinateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CoordinateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StopEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStopRelations
	{
		internal static readonly IEntityRelation NetEntityUsingNetIdStatic = new StopRelations().NetEntityUsingNetId;
		internal static readonly IEntityRelation CoordinateEntityUsingCoordinateIDStatic = new StopRelations().CoordinateEntityUsingCoordinateID;

		/// <summary>CTor</summary>
		static StaticStopRelations()
		{
		}
	}
}
