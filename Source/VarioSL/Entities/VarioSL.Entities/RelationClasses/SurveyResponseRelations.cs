﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyResponse. </summary>
	public partial class SurveyResponseRelations
	{
		/// <summary>CTor</summary>
		public SurveyResponseRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyResponseEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyAnswerEntityUsingSurveyResponseID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.SurveyEntityUsingSurveyID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyResponseEntity and SurveyAnswerEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyResponse.SurveyResponseID - SurveyAnswer.SurveyResponseID
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingSurveyResponseID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswers" , true);
				relation.AddEntityFieldPair(SurveyResponseFields.SurveyResponseID, SurveyAnswerFields.SurveyResponseID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyResponseEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResponse.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, SurveyResponseFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyResponseEntity and SurveyEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyResponse.SurveyID - Survey.SurveyID
		/// </summary>
		public virtual IEntityRelation SurveyEntityUsingSurveyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Survey", false);
				relation.AddEntityFieldPair(SurveyFields.SurveyID, SurveyResponseFields.SurveyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyResponseRelations
	{
		internal static readonly IEntityRelation SurveyAnswerEntityUsingSurveyResponseIDStatic = new SurveyResponseRelations().SurveyAnswerEntityUsingSurveyResponseID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new SurveyResponseRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation SurveyEntityUsingSurveyIDStatic = new SurveyResponseRelations().SurveyEntityUsingSurveyID;

		/// <summary>CTor</summary>
		static StaticSurveyResponseRelations()
		{
		}
	}
}
