﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareTable. </summary>
	public partial class FareTableRelations
	{
		/// <summary>CTor</summary>
		public FareTableRelations()
		{
		}

		/// <summary>Gets all relations of the FareTableEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareTableEntityUsingMasterFareTableID);
			toReturn.Add(this.FareTableEntryEntityUsingFareTableID);
			toReturn.Add(this.TicketEntityUsingFareTableID);
			toReturn.Add(this.TicketEntityUsingFareTable2ID);
			toReturn.Add(this.ClientEntityUsingOwnerCLientID);
			toReturn.Add(this.FareTableEntityUsingFareTableIDMasterFareTableID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and FareTableEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTable.FareTableID - FareTable.MasterFareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingMasterFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTables" , true);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, FareTableFields.MasterFareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and FareTableEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTable.FareTableID - FareTableEntry.FareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntryEntityUsingFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareTableEntries" , true);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, FareTableEntryFields.FareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTable.FareTableID - Ticket.FareTableID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsFareTable1" , true);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, TicketFields.FareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// FareTable.FareTableID - Ticket.FareTable2ID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingFareTable2ID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketsFareTable2" , true);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, TicketFields.FareTable2ID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTable.OwnerCLientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingOwnerCLientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, FareTableFields.OwnerCLientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and FareTableEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTable.MasterFareTableID - FareTable.FareTableID
		/// </summary>
		public virtual IEntityRelation FareTableEntityUsingFareTableIDMasterFareTableID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Faretable", false);
				relation.AddEntityFieldPair(FareTableFields.FareTableID, FareTableFields.MasterFareTableID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareTableEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// FareTable.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareTableFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareTableEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareTableRelations
	{
		internal static readonly IEntityRelation FareTableEntityUsingMasterFareTableIDStatic = new FareTableRelations().FareTableEntityUsingMasterFareTableID;
		internal static readonly IEntityRelation FareTableEntryEntityUsingFareTableIDStatic = new FareTableRelations().FareTableEntryEntityUsingFareTableID;
		internal static readonly IEntityRelation TicketEntityUsingFareTableIDStatic = new FareTableRelations().TicketEntityUsingFareTableID;
		internal static readonly IEntityRelation TicketEntityUsingFareTable2IDStatic = new FareTableRelations().TicketEntityUsingFareTable2ID;
		internal static readonly IEntityRelation ClientEntityUsingOwnerCLientIDStatic = new FareTableRelations().ClientEntityUsingOwnerCLientID;
		internal static readonly IEntityRelation FareTableEntityUsingFareTableIDMasterFareTableIDStatic = new FareTableRelations().FareTableEntityUsingFareTableIDMasterFareTableID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new FareTableRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticFareTableRelations()
		{
		}
	}
}
