﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentOption. </summary>
	public partial class PaymentOptionRelations
	{
		/// <summary>CTor</summary>
		public PaymentOptionRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentOptionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutoloadSettingEntityUsingAlternativePaymentOptionId);
			toReturn.Add(this.AutoloadSettingEntityUsingPaymentOptionID);
			toReturn.Add(this.AutoloadToPaymentOptionEntityUsingBackupPaymentOptionID);
			toReturn.Add(this.AutoloadToPaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.InvoiceEntityUsingUsedPaymentOptionID);
			toReturn.Add(this.OrderEntityUsingPaymentOptionID);
			toReturn.Add(this.PaymentJournalEntityUsingPaymentOptionID);
			toReturn.Add(this.RuleViolationEntityUsingPaymentOptionID);
			toReturn.Add(this.BankConnectionDataEntityUsingBankConnectionDataID);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.PaymentOptionEntityUsingProspectivePaymentOptionID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and AutoloadSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - AutoloadSetting.AlternativePaymentOptionId
		/// </summary>
		public virtual IEntityRelation AutoloadSettingEntityUsingAlternativePaymentOptionId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AutoloadSettingsAlternative" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadSettingFields.AlternativePaymentOptionId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and AutoloadSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - AutoloadSetting.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation AutoloadSettingEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AutoloadSettings" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadSettingFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and AutoloadToPaymentOptionEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - AutoloadToPaymentOption.BackupPaymentOptionID
		/// </summary>
		public virtual IEntityRelation AutoloadToPaymentOptionEntityUsingBackupPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BackupAutoloadToPaymentOptions" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadToPaymentOptionFields.BackupPaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and AutoloadToPaymentOptionEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - AutoloadToPaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation AutoloadToPaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AutoloadToPaymentOptions" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, AutoloadToPaymentOptionFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutoloadToPaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - Invoice.UsedPaymentOptionID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingUsedPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, InvoiceFields.UsedPaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - Order.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Orders" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, OrderFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - PaymentJournal.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, PaymentJournalFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and RuleViolationEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - RuleViolation.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation RuleViolationEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolations" , true);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, RuleViolationFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and BankConnectionDataEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentOption.BankConnectionDataID - BankConnectionData.BankConnectionDataID
		/// </summary>
		public virtual IEntityRelation BankConnectionDataEntityUsingBankConnectionDataID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "BankConnectionData", false);




				relation.AddEntityFieldPair(BankConnectionDataFields.BankConnectionDataID, PaymentOptionFields.BankConnectionDataID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankConnectionDataEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and PaymentOptionEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentOption.ProspectivePaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PreviousPaymentOption", false);




				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, PaymentOptionFields.ProspectivePaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and PaymentOptionEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentOption.PaymentOptionID - PaymentOption.ProspectivePaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingProspectivePaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ProspectivePaymentOption", true);


				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, PaymentOptionFields.ProspectivePaymentOptionID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentOption.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, PaymentOptionFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentOptionEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentOption.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, PaymentOptionFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentOptionRelations
	{
		internal static readonly IEntityRelation AutoloadSettingEntityUsingAlternativePaymentOptionIdStatic = new PaymentOptionRelations().AutoloadSettingEntityUsingAlternativePaymentOptionId;
		internal static readonly IEntityRelation AutoloadSettingEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().AutoloadSettingEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation AutoloadToPaymentOptionEntityUsingBackupPaymentOptionIDStatic = new PaymentOptionRelations().AutoloadToPaymentOptionEntityUsingBackupPaymentOptionID;
		internal static readonly IEntityRelation AutoloadToPaymentOptionEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().AutoloadToPaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation InvoiceEntityUsingUsedPaymentOptionIDStatic = new PaymentOptionRelations().InvoiceEntityUsingUsedPaymentOptionID;
		internal static readonly IEntityRelation OrderEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().OrderEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().PaymentJournalEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation RuleViolationEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().RuleViolationEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation BankConnectionDataEntityUsingBankConnectionDataIDStatic = new PaymentOptionRelations().BankConnectionDataEntityUsingBankConnectionDataID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new PaymentOptionRelations().PaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingProspectivePaymentOptionIDStatic = new PaymentOptionRelations().PaymentOptionEntityUsingProspectivePaymentOptionID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new PaymentOptionRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new PaymentOptionRelations().ContractEntityUsingContractID;

		/// <summary>CTor</summary>
		static StaticPaymentOptionRelations()
		{
		}
	}
}
