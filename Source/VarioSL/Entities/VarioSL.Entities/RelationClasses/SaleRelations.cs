﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Sale. </summary>
	public partial class SaleRelations
	{
		/// <summary>CTor</summary>
		public SaleRelations()
		{
		}

		/// <summary>Gets all relations of the SaleEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentJournalEntityUsingSaleID);
			toReturn.Add(this.SaleEntityUsingRefundReferenceID);
			toReturn.Add(this.SaleEntityUsingCancellationReferenceID);
			toReturn.Add(this.TransactionJournalEntityUsingSaleID);
			toReturn.Add(this.BankStatementVerificationEntityUsingSaleID);
			toReturn.Add(this.OrderEntityUsingOrderID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DeviceClassEntityUsingSalesChannelID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.ContractEntityUsingSecondaryContractID);
			toReturn.Add(this.CreditCardAuthorizationEntityUsingCreditCardAuthorizationID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.SaleEntityUsingSaleIDRefundReferenceID);
			toReturn.Add(this.SaleEntityUsingSaleIDCancellationReferenceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Sale.SaleID - PaymentJournal.SaleID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(SaleFields.SaleID, PaymentJournalFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Sale.SaleID - Sale.RefundReferenceID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingRefundReferenceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Refunds" , true);
				relation.AddEntityFieldPair(SaleFields.SaleID, SaleFields.RefundReferenceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Sale.SaleID - Sale.CancellationReferenceID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingCancellationReferenceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(SaleFields.SaleID, SaleFields.CancellationReferenceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Sale.SaleID - TransactionJournal.SaleID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(SaleFields.SaleID, TransactionJournalFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and BankStatementVerificationEntity over the 1:1 relation they have, using the relation between the fields:
		/// Sale.SaleID - BankStatementVerification.SaleID
		/// </summary>
		public virtual IEntityRelation BankStatementVerificationEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "BankStatementVerification", true);


				relation.AddEntityFieldPair(SaleFields.SaleID, BankStatementVerificationFields.SaleID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and OrderEntity over the 1:1 relation they have, using the relation between the fields:
		/// Sale.OrderID - Order.OrderID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingOrderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Order", false);




				relation.AddEntityFieldPair(OrderFields.OrderID, SaleFields.OrderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SaleEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, SaleFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.SalesChannelID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingSalesChannelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, SaleFields.SalesChannelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, SaleFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.SecondaryContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingSecondaryContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SecondaryContract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, SaleFields.SecondaryContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and CreditCardAuthorizationEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.CreditCardAuthorizationID - CreditCardAuthorization.CreditCardAuthorizationID
		/// </summary>
		public virtual IEntityRelation CreditCardAuthorizationEntityUsingCreditCardAuthorizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CreditCardAuthorization", false);
				relation.AddEntityFieldPair(CreditCardAuthorizationFields.CreditCardAuthorizationID, SaleFields.CreditCardAuthorizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, SaleFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and SaleEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.RefundReferenceID - Sale.SaleID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSaleIDRefundReferenceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RefundedSale", false);
				relation.AddEntityFieldPair(SaleFields.SaleID, SaleFields.RefundReferenceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleEntity and SaleEntity over the m:1 relation they have, using the relation between the fields:
		/// Sale.CancellationReferenceID - Sale.SaleID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSaleIDCancellationReferenceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Sale", false);
				relation.AddEntityFieldPair(SaleFields.SaleID, SaleFields.CancellationReferenceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSaleRelations
	{
		internal static readonly IEntityRelation PaymentJournalEntityUsingSaleIDStatic = new SaleRelations().PaymentJournalEntityUsingSaleID;
		internal static readonly IEntityRelation SaleEntityUsingRefundReferenceIDStatic = new SaleRelations().SaleEntityUsingRefundReferenceID;
		internal static readonly IEntityRelation SaleEntityUsingCancellationReferenceIDStatic = new SaleRelations().SaleEntityUsingCancellationReferenceID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingSaleIDStatic = new SaleRelations().TransactionJournalEntityUsingSaleID;
		internal static readonly IEntityRelation BankStatementVerificationEntityUsingSaleIDStatic = new SaleRelations().BankStatementVerificationEntityUsingSaleID;
		internal static readonly IEntityRelation OrderEntityUsingOrderIDStatic = new SaleRelations().OrderEntityUsingOrderID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new SaleRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DeviceClassEntityUsingSalesChannelIDStatic = new SaleRelations().DeviceClassEntityUsingSalesChannelID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new SaleRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation ContractEntityUsingSecondaryContractIDStatic = new SaleRelations().ContractEntityUsingSecondaryContractID;
		internal static readonly IEntityRelation CreditCardAuthorizationEntityUsingCreditCardAuthorizationIDStatic = new SaleRelations().CreditCardAuthorizationEntityUsingCreditCardAuthorizationID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new SaleRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation SaleEntityUsingSaleIDRefundReferenceIDStatic = new SaleRelations().SaleEntityUsingSaleIDRefundReferenceID;
		internal static readonly IEntityRelation SaleEntityUsingSaleIDCancellationReferenceIDStatic = new SaleRelations().SaleEntityUsingSaleIDCancellationReferenceID;

		/// <summary>CTor</summary>
		static StaticSaleRelations()
		{
		}
	}
}
