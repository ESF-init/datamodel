﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ApportionmentResult. </summary>
	public partial class ApportionmentResultRelations
	{
		/// <summary>CTor</summary>
		public ApportionmentResultRelations()
		{
		}

		/// <summary>Gets all relations of the ApportionmentResultEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionmentEntityUsingApportionmentID);
			toReturn.Add(this.ClientEntityUsingAcquirerID);
			toReturn.Add(this.ClientEntityUsingFromClientID);
			toReturn.Add(this.ClientEntityUsingToClientID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.CardEntityUsingCardID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and ApportionmentEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.ApportionmentID - Apportionment.ApportionmentID
		/// </summary>
		public virtual IEntityRelation ApportionmentEntityUsingApportionmentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Apportionment", false);
				relation.AddEntityFieldPair(ApportionmentFields.ApportionmentID, ApportionmentResultFields.ApportionmentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.AcquirerID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingAcquirerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AcquirerClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.AcquirerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.FromClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingFromClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FromClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.FromClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.ToClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingToClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ToClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ApportionmentResultFields.ToClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, ApportionmentResultFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ApportionmentResultEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// ApportionmentResult.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, ApportionmentResultFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionmentResultEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticApportionmentResultRelations
	{
		internal static readonly IEntityRelation ApportionmentEntityUsingApportionmentIDStatic = new ApportionmentResultRelations().ApportionmentEntityUsingApportionmentID;
		internal static readonly IEntityRelation ClientEntityUsingAcquirerIDStatic = new ApportionmentResultRelations().ClientEntityUsingAcquirerID;
		internal static readonly IEntityRelation ClientEntityUsingFromClientIDStatic = new ApportionmentResultRelations().ClientEntityUsingFromClientID;
		internal static readonly IEntityRelation ClientEntityUsingToClientIDStatic = new ApportionmentResultRelations().ClientEntityUsingToClientID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new ApportionmentResultRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new ApportionmentResultRelations().CardEntityUsingCardID;

		/// <summary>CTor</summary>
		static StaticApportionmentResultRelations()
		{
		}
	}
}
