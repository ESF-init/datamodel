﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvLayout. </summary>
	public partial class VdvLayoutRelations
	{
		/// <summary>CTor</summary>
		public VdvLayoutRelations()
		{
		}

		/// <summary>Gets all relations of the VdvLayoutEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvLayoutObjectEntityUsingLayoutID);
			toReturn.Add(this.VdvProductEntityUsingVdvLayoutID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VdvLayoutEntity and VdvLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvLayout.LayoutID - VdvLayoutObject.LayoutID
		/// </summary>
		public virtual IEntityRelation VdvLayoutObjectEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvLayoutObjects" , true);
				relation.AddEntityFieldPair(VdvLayoutFields.LayoutID, VdvLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VdvLayoutEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvLayout.LayoutID - VdvProduct.VdvLayoutID
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingVdvLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProducts" , true);
				relation.AddEntityFieldPair(VdvLayoutFields.LayoutID, VdvProductFields.VdvLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VdvLayoutEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// VdvLayout.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, VdvLayoutFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvLayoutEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvLayoutRelations
	{
		internal static readonly IEntityRelation VdvLayoutObjectEntityUsingLayoutIDStatic = new VdvLayoutRelations().VdvLayoutObjectEntityUsingLayoutID;
		internal static readonly IEntityRelation VdvProductEntityUsingVdvLayoutIDStatic = new VdvLayoutRelations().VdvProductEntityUsingVdvLayoutID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new VdvLayoutRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticVdvLayoutRelations()
		{
		}
	}
}
