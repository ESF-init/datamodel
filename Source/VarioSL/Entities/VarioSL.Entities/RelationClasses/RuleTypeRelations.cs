﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RuleType. </summary>
	public partial class RuleTypeRelations
	{
		/// <summary>CTor</summary>
		public RuleTypeRelations()
		{
		}

		/// <summary>Gets all relations of the RuleTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RuleCappingEntityUsingRuleTypeID);
			toReturn.Add(this.RulePeriodEntityUsingPeriodUnit);
			toReturn.Add(this.RulePeriodEntityUsingValidTimeUnit);
			toReturn.Add(this.RulePeriodEntityUsingValidationType);
			toReturn.Add(this.RulePeriodEntityUsingExtTimeUnit);
			toReturn.Add(this.TicketAssignmentEntityUsingPeriodicTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and RuleCappingEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - RuleCapping.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleCappingEntityUsingRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TmRuleCappings" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RuleCappingFields.RuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - RulePeriod.PeriodUnit
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingPeriodUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriodPeriodUnit" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.PeriodUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - RulePeriod.ValidTimeUnit
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingValidTimeUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriodValidTimeUnit" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ValidTimeUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - RulePeriod.ValidationType
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingValidationType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriodsPeriodType" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ValidationType);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and RulePeriodEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - RulePeriod.ExtTimeUnit
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingExtTimeUnit
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RulePeriodExtTimeUnit" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RulePeriodFields.ExtTimeUnit);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RuleTypeEntity and TicketAssignmentEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleType.RuleTypeID - TicketAssignment.PeriodicTypeID
		/// </summary>
		public virtual IEntityRelation TicketAssignmentEntityUsingPeriodicTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketAssignments" , true);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, TicketAssignmentFields.PeriodicTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRuleTypeRelations
	{
		internal static readonly IEntityRelation RuleCappingEntityUsingRuleTypeIDStatic = new RuleTypeRelations().RuleCappingEntityUsingRuleTypeID;
		internal static readonly IEntityRelation RulePeriodEntityUsingPeriodUnitStatic = new RuleTypeRelations().RulePeriodEntityUsingPeriodUnit;
		internal static readonly IEntityRelation RulePeriodEntityUsingValidTimeUnitStatic = new RuleTypeRelations().RulePeriodEntityUsingValidTimeUnit;
		internal static readonly IEntityRelation RulePeriodEntityUsingValidationTypeStatic = new RuleTypeRelations().RulePeriodEntityUsingValidationType;
		internal static readonly IEntityRelation RulePeriodEntityUsingExtTimeUnitStatic = new RuleTypeRelations().RulePeriodEntityUsingExtTimeUnit;
		internal static readonly IEntityRelation TicketAssignmentEntityUsingPeriodicTypeIDStatic = new RuleTypeRelations().TicketAssignmentEntityUsingPeriodicTypeID;

		/// <summary>CTor</summary>
		static StaticRuleTypeRelations()
		{
		}
	}
}
