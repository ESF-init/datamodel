﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Attribute. </summary>
	public partial class AttributeRelations
	{
		/// <summary>CTor</summary>
		public AttributeRelations()
		{
		}

		/// <summary>Gets all relations of the AttributeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeBitmapLayoutObjectEntityUsingAttributeID);
			toReturn.Add(this.AttributeTextLayoutObjectEntityUsingAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingAttributeID);
			toReturn.Add(this.KeyAttributeTransfromEntityUsingAttributeID);
			toReturn.Add(this.TariffEntityUsingTarifID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AttributeEntity and AttributeBitmapLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Attribute.AttributeID - AttributeBitmapLayoutObject.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeBitmapLayoutObjectEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeBitmapLayoutObjects" , true);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, AttributeBitmapLayoutObjectFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeBitmapLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeEntity and AttributeTextLayoutObjectEntity over the 1:n relation they have, using the relation between the fields:
		/// Attribute.AttributeID - AttributeTextLayoutObject.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeTextLayoutObjectEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeTextLayoutObjects" , true);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, AttributeTextLayoutObjectFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeTextLayoutObjectEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeEntity and AttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Attribute.AttributeID - AttributeValue.AttributeID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeValues" , true);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, AttributeValueFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AttributeEntity and KeyAttributeTransfromEntity over the 1:n relation they have, using the relation between the fields:
		/// Attribute.AttributeID - KeyAttributeTransfrom.AttributeID
		/// </summary>
		public virtual IEntityRelation KeyAttributeTransfromEntityUsingAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "KeyAttributeTransforms" , true);
				relation.AddEntityFieldPair(AttributeFields.AttributeID, KeyAttributeTransfromFields.AttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AttributeEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Attribute.TarifID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTarifID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, AttributeFields.TarifID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAttributeRelations
	{
		internal static readonly IEntityRelation AttributeBitmapLayoutObjectEntityUsingAttributeIDStatic = new AttributeRelations().AttributeBitmapLayoutObjectEntityUsingAttributeID;
		internal static readonly IEntityRelation AttributeTextLayoutObjectEntityUsingAttributeIDStatic = new AttributeRelations().AttributeTextLayoutObjectEntityUsingAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeIDStatic = new AttributeRelations().AttributeValueEntityUsingAttributeID;
		internal static readonly IEntityRelation KeyAttributeTransfromEntityUsingAttributeIDStatic = new AttributeRelations().KeyAttributeTransfromEntityUsingAttributeID;
		internal static readonly IEntityRelation TariffEntityUsingTarifIDStatic = new AttributeRelations().TariffEntityUsingTarifID;

		/// <summary>CTor</summary>
		static StaticAttributeRelations()
		{
		}
	}
}
