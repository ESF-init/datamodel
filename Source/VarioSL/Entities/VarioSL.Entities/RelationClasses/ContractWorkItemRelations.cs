﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractWorkItem. </summary>
	public partial class ContractWorkItemRelations : WorkItemRelations
	{
		/// <summary>CTor</summary>
		public ContractWorkItemRelations()
		{
		}

		/// <summary>Gets all relations of the ContractWorkItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public override List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = base.GetAllRelations();
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ContractWorkItemEntity and WorkItemHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// ContractWorkItem.WorkItemID - WorkItemHistory.WorkItemID
		/// </summary>
		public override IEntityRelation WorkItemHistoryEntityUsingWorkItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItemHistory" , true);
				relation.AddEntityFieldPair(ContractWorkItemFields.WorkItemID, WorkItemHistoryFields.WorkItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractWorkItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ContractWorkItemEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractWorkItem.Assignee - UserList.UserID
		/// </summary>
		public override IEntityRelation UserListEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, ContractWorkItemFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractWorkItemEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractWorkItem.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractWorkItemFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractWorkItemEntity and WorkItemSubjectEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractWorkItem.WorkItemSubjectID - WorkItemSubject.WorkItemSubjectID
		/// </summary>
		public override IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WorkItemSubject", false);
				relation.AddEntityFieldPair(WorkItemSubjectFields.WorkItemSubjectID, ContractWorkItemFields.WorkItemSubjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemSubjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractWorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public override IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractWorkItemRelations
	{
		internal static readonly IEntityRelation WorkItemHistoryEntityUsingWorkItemIDStatic = new ContractWorkItemRelations().WorkItemHistoryEntityUsingWorkItemID;
		internal static readonly IEntityRelation UserListEntityUsingAssigneeStatic = new ContractWorkItemRelations().UserListEntityUsingAssignee;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ContractWorkItemRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectIDStatic = new ContractWorkItemRelations().WorkItemSubjectEntityUsingWorkItemSubjectID;

		/// <summary>CTor</summary>
		static StaticContractWorkItemRelations()
		{
		}
	}
}
