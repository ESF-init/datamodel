﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TariffParameter. </summary>
	public partial class TariffParameterRelations
	{
		/// <summary>CTor</summary>
		public TariffParameterRelations()
		{
		}

		/// <summary>Gets all relations of the TariffParameterEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterAttributeValueEntityUsingParameterID);
			toReturn.Add(this.ParameterFareStageEntityUsingParameterID);
			toReturn.Add(this.ParameterTariffEntityUsingParameterID);
			toReturn.Add(this.ParameterTicketEntityUsingParameterID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TariffParameterEntity and ParameterAttributeValueEntity over the 1:n relation they have, using the relation between the fields:
		/// TariffParameter.ParameterID - ParameterAttributeValue.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterAttributeValueEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterAttributeValues" , true);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterAttributeValueFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterAttributeValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffParameterEntity and ParameterFareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// TariffParameter.ParameterID - ParameterFareStage.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterFareStageEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterFareStage" , true);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterFareStageFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterFareStageEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffParameterEntity and ParameterTariffEntity over the 1:n relation they have, using the relation between the fields:
		/// TariffParameter.ParameterID - ParameterTariff.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterTariffEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterTariff" , true);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterTariffFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TariffParameterEntity and ParameterTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// TariffParameter.ParameterID - ParameterTicket.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterTicketEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterTicket" , true);
				relation.AddEntityFieldPair(TariffParameterFields.ParameterID, ParameterTicketFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterTicketEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTariffParameterRelations
	{
		internal static readonly IEntityRelation ParameterAttributeValueEntityUsingParameterIDStatic = new TariffParameterRelations().ParameterAttributeValueEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterFareStageEntityUsingParameterIDStatic = new TariffParameterRelations().ParameterFareStageEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterTariffEntityUsingParameterIDStatic = new TariffParameterRelations().ParameterTariffEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterTicketEntityUsingParameterIDStatic = new TariffParameterRelations().ParameterTicketEntityUsingParameterID;

		/// <summary>CTor</summary>
		static StaticTariffParameterRelations()
		{
		}
	}
}
