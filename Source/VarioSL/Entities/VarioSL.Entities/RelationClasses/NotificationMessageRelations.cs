﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: NotificationMessage. </summary>
	public partial class NotificationMessageRelations
	{
		/// <summary>CTor</summary>
		public NotificationMessageRelations()
		{
		}

		/// <summary>Gets all relations of the NotificationMessageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.NotificationMessageOwnerEntityUsingNotificationMessageID);
			toReturn.Add(this.NotificationMessageToCustomerEntityUsingNotificationMessageID);
			toReturn.Add(this.EmailEventEntityUsingEmailEventID);
			toReturn.Add(this.FormEntityUsingFormID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between NotificationMessageEntity and NotificationMessageOwnerEntity over the 1:n relation they have, using the relation between the fields:
		/// NotificationMessage.NotificationMessageID - NotificationMessageOwner.NotificationMessageID
		/// </summary>
		public virtual IEntityRelation NotificationMessageOwnerEntityUsingNotificationMessageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessageOwners" , true);
				relation.AddEntityFieldPair(NotificationMessageFields.NotificationMessageID, NotificationMessageOwnerFields.NotificationMessageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageOwnerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between NotificationMessageEntity and NotificationMessageToCustomerEntity over the 1:n relation they have, using the relation between the fields:
		/// NotificationMessage.NotificationMessageID - NotificationMessageToCustomer.NotificationMessageID
		/// </summary>
		public virtual IEntityRelation NotificationMessageToCustomerEntityUsingNotificationMessageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessageToCustomers" , true);
				relation.AddEntityFieldPair(NotificationMessageFields.NotificationMessageID, NotificationMessageToCustomerFields.NotificationMessageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageToCustomerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between NotificationMessageEntity and EmailEventEntity over the m:1 relation they have, using the relation between the fields:
		/// NotificationMessage.EmailEventID - EmailEvent.EmailEventID
		/// </summary>
		public virtual IEntityRelation EmailEventEntityUsingEmailEventID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EmailEvent", false);
				relation.AddEntityFieldPair(EmailEventFields.EmailEventID, NotificationMessageFields.EmailEventID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between NotificationMessageEntity and FormEntity over the m:1 relation they have, using the relation between the fields:
		/// NotificationMessage.FormID - Form.FormID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Form", false);
				relation.AddEntityFieldPair(FormFields.FormID, NotificationMessageFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticNotificationMessageRelations
	{
		internal static readonly IEntityRelation NotificationMessageOwnerEntityUsingNotificationMessageIDStatic = new NotificationMessageRelations().NotificationMessageOwnerEntityUsingNotificationMessageID;
		internal static readonly IEntityRelation NotificationMessageToCustomerEntityUsingNotificationMessageIDStatic = new NotificationMessageRelations().NotificationMessageToCustomerEntityUsingNotificationMessageID;
		internal static readonly IEntityRelation EmailEventEntityUsingEmailEventIDStatic = new NotificationMessageRelations().EmailEventEntityUsingEmailEventID;
		internal static readonly IEntityRelation FormEntityUsingFormIDStatic = new NotificationMessageRelations().FormEntityUsingFormID;

		/// <summary>CTor</summary>
		static StaticNotificationMessageRelations()
		{
		}
	}
}
