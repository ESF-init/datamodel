﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RecipientGroupMember. </summary>
	public partial class RecipientGroupMemberRelations
	{
		/// <summary>CTor</summary>
		public RecipientGroupMemberRelations()
		{
		}

		/// <summary>Gets all relations of the RecipientGroupMemberEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.RecipientGroupEntityUsingRecipientGroupID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RecipientGroupMemberEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// RecipientGroupMember.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, RecipientGroupMemberFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupMemberEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RecipientGroupMemberEntity and RecipientGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// RecipientGroupMember.RecipientGroupID - RecipientGroup.RecipientGroupID
		/// </summary>
		public virtual IEntityRelation RecipientGroupEntityUsingRecipientGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RecipientGroup", false);
				relation.AddEntityFieldPair(RecipientGroupFields.RecipientGroupID, RecipientGroupMemberFields.RecipientGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RecipientGroupMemberEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRecipientGroupMemberRelations
	{
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new RecipientGroupMemberRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation RecipientGroupEntityUsingRecipientGroupIDStatic = new RecipientGroupMemberRelations().RecipientGroupEntityUsingRecipientGroupID;

		/// <summary>CTor</summary>
		static StaticRecipientGroupMemberRelations()
		{
		}
	}
}
