﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UnitCollection. </summary>
	public partial class UnitCollectionRelations
	{
		/// <summary>CTor</summary>
		public UnitCollectionRelations()
		{
		}

		/// <summary>Gets all relations of the UnitCollectionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterValueEntityUsingUnitCollectionID);
			toReturn.Add(this.UnitCollectionToUnitEntityUsingUnitCollectionID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.ParameterArchiveEntityUsingParameterArchiveID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UnitCollectionEntity and ParameterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// UnitCollection.UnitCollectionID - ParameterValue.UnitCollectionID
		/// </summary>
		public virtual IEntityRelation ParameterValueEntityUsingUnitCollectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterValues" , true);
				relation.AddEntityFieldPair(UnitCollectionFields.UnitCollectionID, ParameterValueFields.UnitCollectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UnitCollectionEntity and UnitCollectionToUnitEntity over the 1:n relation they have, using the relation between the fields:
		/// UnitCollection.UnitCollectionID - UnitCollectionToUnit.UnitCollectionID
		/// </summary>
		public virtual IEntityRelation UnitCollectionToUnitEntityUsingUnitCollectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UnitCollectionToUnits" , true);
				relation.AddEntityFieldPair(UnitCollectionFields.UnitCollectionID, UnitCollectionToUnitFields.UnitCollectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionToUnitEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between UnitCollectionEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// UnitCollection.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, UnitCollectionFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UnitCollectionEntity and ParameterArchiveEntity over the m:1 relation they have, using the relation between the fields:
		/// UnitCollection.ParameterArchiveID - ParameterArchive.ParameterArchiveID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveEntityUsingParameterArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParameterArchive", false);
				relation.AddEntityFieldPair(ParameterArchiveFields.ParameterArchiveID, UnitCollectionFields.ParameterArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUnitCollectionRelations
	{
		internal static readonly IEntityRelation ParameterValueEntityUsingUnitCollectionIDStatic = new UnitCollectionRelations().ParameterValueEntityUsingUnitCollectionID;
		internal static readonly IEntityRelation UnitCollectionToUnitEntityUsingUnitCollectionIDStatic = new UnitCollectionRelations().UnitCollectionToUnitEntityUsingUnitCollectionID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new UnitCollectionRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation ParameterArchiveEntityUsingParameterArchiveIDStatic = new UnitCollectionRelations().ParameterArchiveEntityUsingParameterArchiveID;

		/// <summary>CTor</summary>
		static StaticUnitCollectionRelations()
		{
		}
	}
}
