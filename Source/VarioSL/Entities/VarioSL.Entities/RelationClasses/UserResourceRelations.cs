﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserResource. </summary>
	public partial class UserResourceRelations
	{
		/// <summary>CTor</summary>
		public UserResourceRelations()
		{
		}

		/// <summary>Gets all relations of the UserResourceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserGroupRightEntityUsingResourceID);
			toReturn.Add(this.ClaimEntityUsingResourceID);
			toReturn.Add(this.ReportEntityUsingResourceID);
			toReturn.Add(this.ReportCategoryEntityUsingResourceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between UserResourceEntity and UserGroupRightEntity over the 1:n relation they have, using the relation between the fields:
		/// UserResource.ResourceID - UserGroupRight.ResourceID
		/// </summary>
		public virtual IEntityRelation UserGroupRightEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserGroupRights" , true);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, UserGroupRightFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupRightEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserResourceEntity and ClaimEntity over the 1:n relation they have, using the relation between the fields:
		/// UserResource.ResourceID - Claim.ResourceID
		/// </summary>
		public virtual IEntityRelation ClaimEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Claims" , true);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, ClaimFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClaimEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserResourceEntity and ReportEntity over the 1:n relation they have, using the relation between the fields:
		/// UserResource.ResourceID - Report.ResourceID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Reports" , true);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, ReportFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between UserResourceEntity and ReportCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// UserResource.ResourceID - ReportCategory.ResourceID
		/// </summary>
		public virtual IEntityRelation ReportCategoryEntityUsingResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportCategories" , true);
				relation.AddEntityFieldPair(UserResourceFields.ResourceID, ReportCategoryFields.ResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserResourceRelations
	{
		internal static readonly IEntityRelation UserGroupRightEntityUsingResourceIDStatic = new UserResourceRelations().UserGroupRightEntityUsingResourceID;
		internal static readonly IEntityRelation ClaimEntityUsingResourceIDStatic = new UserResourceRelations().ClaimEntityUsingResourceID;
		internal static readonly IEntityRelation ReportEntityUsingResourceIDStatic = new UserResourceRelations().ReportEntityUsingResourceID;
		internal static readonly IEntityRelation ReportCategoryEntityUsingResourceIDStatic = new UserResourceRelations().ReportCategoryEntityUsingResourceID;

		/// <summary>CTor</summary>
		static StaticUserResourceRelations()
		{
		}
	}
}
