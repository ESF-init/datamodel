﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ServiceAllocation. </summary>
	public partial class ServiceAllocationRelations
	{
		/// <summary>CTor</summary>
		public ServiceAllocationRelations()
		{
		}

		/// <summary>Gets all relations of the ServiceAllocationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareMatrixEntryEntityUsingServiceAllocationID);
			toReturn.Add(this.ServicePercentageEntityUsingServiceAllocationID);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ServiceAllocationEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceAllocation.ServiceAllocationID - FareMatrixEntry.ServiceAllocationID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingServiceAllocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntry" , true);
				relation.AddEntityFieldPair(ServiceAllocationFields.ServiceAllocationID, FareMatrixEntryFields.ServiceAllocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceAllocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ServiceAllocationEntity and ServicePercentageEntity over the 1:n relation they have, using the relation between the fields:
		/// ServiceAllocation.ServiceAllocationID - ServicePercentage.ServiceAllocationID
		/// </summary>
		public virtual IEntityRelation ServicePercentageEntityUsingServiceAllocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServicePercentages" , true);
				relation.AddEntityFieldPair(ServiceAllocationFields.ServiceAllocationID, ServicePercentageFields.ServiceAllocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceAllocationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServicePercentageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ServiceAllocationEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ServiceAllocation.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ServiceAllocationFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceAllocationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticServiceAllocationRelations
	{
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingServiceAllocationIDStatic = new ServiceAllocationRelations().FareMatrixEntryEntityUsingServiceAllocationID;
		internal static readonly IEntityRelation ServicePercentageEntityUsingServiceAllocationIDStatic = new ServiceAllocationRelations().ServicePercentageEntityUsingServiceAllocationID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new ServiceAllocationRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticServiceAllocationRelations()
		{
		}
	}
}
