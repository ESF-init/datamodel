﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareStageList. </summary>
	public partial class FareStageListRelations
	{
		/// <summary>CTor</summary>
		public FareStageListRelations()
		{
		}

		/// <summary>Gets all relations of the FareStageListEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareMatrixEntityUsingFareStageListID);
			toReturn.Add(this.FareStageEntityUsingFareStageListID);
			toReturn.Add(this.AttributeValueEntityUsingAttributeValueID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareStageListEntity and FareMatrixEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStageList.FareStageListID - FareMatrix.FareStageListID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingFareStageListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Farematrices" , true);
				relation.AddEntityFieldPair(FareStageListFields.FareStageListID, FareMatrixFields.FareStageListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareStageListEntity and FareStageEntity over the 1:n relation they have, using the relation between the fields:
		/// FareStageList.FareStageListID - FareStage.FareStageListID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Farestages" , true);
				relation.AddEntityFieldPair(FareStageListFields.FareStageListID, FareStageFields.FareStageListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareStageListEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStageList.AttributeValueID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Attributevalue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareStageListFields.AttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareStageListEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// FareStageList.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tarif", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareStageListFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageListEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareStageListRelations
	{
		internal static readonly IEntityRelation FareMatrixEntityUsingFareStageListIDStatic = new FareStageListRelations().FareMatrixEntityUsingFareStageListID;
		internal static readonly IEntityRelation FareStageEntityUsingFareStageListIDStatic = new FareStageListRelations().FareStageEntityUsingFareStageListID;
		internal static readonly IEntityRelation AttributeValueEntityUsingAttributeValueIDStatic = new FareStageListRelations().AttributeValueEntityUsingAttributeValueID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new FareStageListRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticFareStageListRelations()
		{
		}
	}
}
