﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareEvasionReason. </summary>
	public partial class FareEvasionReasonRelations
	{
		/// <summary>CTor</summary>
		public FareEvasionReasonRelations()
		{
		}

		/// <summary>Gets all relations of the FareEvasionReasonEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareEvasionReasonToCategoryEntityUsingFareEvasionReasonID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingFareEvasionReasonID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareEvasionReasonEntity and FareEvasionReasonToCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionReason.FareEvasionReasonID - FareEvasionReasonToCategory.FareEvasionReasonID
		/// </summary>
		public virtual IEntityRelation FareEvasionReasonToCategoryEntityUsingFareEvasionReasonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionReasonToCategories" , true);
				relation.AddEntityFieldPair(FareEvasionReasonFields.FareEvasionReasonID, FareEvasionReasonToCategoryFields.FareEvasionReasonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonToCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareEvasionReasonEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionReason.FareEvasionReasonID - FareEvasionIncident.FareEvasionReasonID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingFareEvasionReasonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(FareEvasionReasonFields.FareEvasionReasonID, FareEvasionIncidentFields.FareEvasionReasonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareEvasionReasonEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionReason.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareEvasionReasonFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareEvasionReasonRelations
	{
		internal static readonly IEntityRelation FareEvasionReasonToCategoryEntityUsingFareEvasionReasonIDStatic = new FareEvasionReasonRelations().FareEvasionReasonToCategoryEntityUsingFareEvasionReasonID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingFareEvasionReasonIDStatic = new FareEvasionReasonRelations().FareEvasionIncidentEntityUsingFareEvasionReasonID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new FareEvasionReasonRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticFareEvasionReasonRelations()
		{
		}
	}
}
