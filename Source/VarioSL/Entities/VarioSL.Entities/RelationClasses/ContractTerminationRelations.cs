﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractTermination. </summary>
	public partial class ContractTerminationRelations
	{
		/// <summary>CTor</summary>
		public ContractTerminationRelations()
		{
		}

		/// <summary>Gets all relations of the ContractTerminationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserListEntityUsingEmployeeID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.ContractTerminationTypeEntityUsingContractTerminationTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ContractTerminationEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractTermination.EmployeeID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingEmployeeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, ContractTerminationFields.EmployeeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractTerminationEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractTermination.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractTerminationFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractTerminationEntity and ContractTerminationTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractTermination.ContractTerminationTypeID - ContractTerminationType.ContractTerminationTypeID
		/// </summary>
		public virtual IEntityRelation ContractTerminationTypeEntityUsingContractTerminationTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ContractTerminationType", false);
				relation.AddEntityFieldPair(ContractTerminationTypeFields.ContractTerminationTypeID, ContractTerminationFields.ContractTerminationTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractTerminationRelations
	{
		internal static readonly IEntityRelation UserListEntityUsingEmployeeIDStatic = new ContractTerminationRelations().UserListEntityUsingEmployeeID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ContractTerminationRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation ContractTerminationTypeEntityUsingContractTerminationTypeIDStatic = new ContractTerminationRelations().ContractTerminationTypeEntityUsingContractTerminationTypeID;

		/// <summary>CTor</summary>
		static StaticContractTerminationRelations()
		{
		}
	}
}
