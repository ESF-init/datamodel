﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareMatrixEntry. </summary>
	public partial class FareMatrixEntryRelations
	{
		/// <summary>CTor</summary>
		public FareMatrixEntryRelations()
		{
		}

		/// <summary>Gets all relations of the FareMatrixEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingDirectionAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingDistanceAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingLineFilterAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingTariffAttributeID);
			toReturn.Add(this.AttributeValueEntityUsingUseDaysAttributeID);
			toReturn.Add(this.FareMatrixEntityUsingFareMatrixID);
			toReturn.Add(this.FareStageEntityUsingBoardingID);
			toReturn.Add(this.FareStageEntityUsingDestinationID);
			toReturn.Add(this.FareStageEntityUsingViaID);
			toReturn.Add(this.RouteNameEntityUsingRouteNameID);
			toReturn.Add(this.ServiceAllocationEntityUsingServiceAllocationID);
			toReturn.Add(this.TicketGroupEntityUsingTicketGroupID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.DirectionAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingDirectionAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueDirection", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.DirectionAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.DistanceAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingDistanceAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueDistance", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.DistanceAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.LineFilterAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingLineFilterAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueLineFilter", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.LineFilterAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.TariffAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingTariffAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueTariff", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.TariffAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.UseDaysAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingUseDaysAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValueUseDays", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, FareMatrixEntryFields.UseDaysAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and FareMatrixEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.FareMatrixID - FareMatrix.FareMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingFareMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareMatrix", false);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, FareMatrixEntryFields.FareMatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.BoardingID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingBoardingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageBoarding", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.BoardingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.DestinationID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingDestinationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageDestination", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.DestinationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.ViaID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingViaID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStageVia", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, FareMatrixEntryFields.ViaID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and RouteNameEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.RouteNameID - RouteName.RouteNameid
		/// </summary>
		public virtual IEntityRelation RouteNameEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteName", false);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, FareMatrixEntryFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and ServiceAllocationEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.ServiceAllocationID - ServiceAllocation.ServiceAllocationID
		/// </summary>
		public virtual IEntityRelation ServiceAllocationEntityUsingServiceAllocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ServiceAllocation", false);
				relation.AddEntityFieldPair(ServiceAllocationFields.ServiceAllocationID, FareMatrixEntryFields.ServiceAllocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceAllocationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FareMatrixEntryEntity and TicketGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// FareMatrixEntry.TicketGroupID - TicketGroup.TicketGroupID
		/// </summary>
		public virtual IEntityRelation TicketGroupEntityUsingTicketGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketGroup", false);
				relation.AddEntityFieldPair(TicketGroupFields.TicketGroupID, FareMatrixEntryFields.TicketGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareMatrixEntryRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingDirectionAttributeIDStatic = new FareMatrixEntryRelations().AttributeValueEntityUsingDirectionAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingDistanceAttributeIDStatic = new FareMatrixEntryRelations().AttributeValueEntityUsingDistanceAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingLineFilterAttributeIDStatic = new FareMatrixEntryRelations().AttributeValueEntityUsingLineFilterAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingTariffAttributeIDStatic = new FareMatrixEntryRelations().AttributeValueEntityUsingTariffAttributeID;
		internal static readonly IEntityRelation AttributeValueEntityUsingUseDaysAttributeIDStatic = new FareMatrixEntryRelations().AttributeValueEntityUsingUseDaysAttributeID;
		internal static readonly IEntityRelation FareMatrixEntityUsingFareMatrixIDStatic = new FareMatrixEntryRelations().FareMatrixEntityUsingFareMatrixID;
		internal static readonly IEntityRelation FareStageEntityUsingBoardingIDStatic = new FareMatrixEntryRelations().FareStageEntityUsingBoardingID;
		internal static readonly IEntityRelation FareStageEntityUsingDestinationIDStatic = new FareMatrixEntryRelations().FareStageEntityUsingDestinationID;
		internal static readonly IEntityRelation FareStageEntityUsingViaIDStatic = new FareMatrixEntryRelations().FareStageEntityUsingViaID;
		internal static readonly IEntityRelation RouteNameEntityUsingRouteNameIDStatic = new FareMatrixEntryRelations().RouteNameEntityUsingRouteNameID;
		internal static readonly IEntityRelation ServiceAllocationEntityUsingServiceAllocationIDStatic = new FareMatrixEntryRelations().ServiceAllocationEntityUsingServiceAllocationID;
		internal static readonly IEntityRelation TicketGroupEntityUsingTicketGroupIDStatic = new FareMatrixEntryRelations().TicketGroupEntityUsingTicketGroupID;

		/// <summary>CTor</summary>
		static StaticFareMatrixEntryRelations()
		{
		}
	}
}
