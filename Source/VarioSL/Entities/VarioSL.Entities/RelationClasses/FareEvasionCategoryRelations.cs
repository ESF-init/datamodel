﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FareEvasionCategory. </summary>
	public partial class FareEvasionCategoryRelations
	{
		/// <summary>CTor</summary>
		public FareEvasionCategoryRelations()
		{
		}

		/// <summary>Gets all relations of the FareEvasionCategoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.TicketEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingFareEvasionCategoryID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FareEvasionCategoryEntity and FareEvasionReasonToCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionCategory.FareEvasionCategoryID - FareEvasionReasonToCategory.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionReasonToCategories" , true);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, FareEvasionReasonToCategoryFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionReasonToCategoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareEvasionCategoryEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionCategory.FareEvasionCategoryID - Ticket.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, TicketFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FareEvasionCategoryEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// FareEvasionCategory.FareEvasionCategoryID - FareEvasionIncident.FareEvasionCategoryID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingFareEvasionCategoryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(FareEvasionCategoryFields.FareEvasionCategoryID, FareEvasionIncidentFields.FareEvasionCategoryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FareEvasionCategoryEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// FareEvasionCategory.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, FareEvasionCategoryFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionCategoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFareEvasionCategoryRelations
	{
		internal static readonly IEntityRelation FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryIDStatic = new FareEvasionCategoryRelations().FareEvasionReasonToCategoryEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation TicketEntityUsingFareEvasionCategoryIDStatic = new FareEvasionCategoryRelations().TicketEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingFareEvasionCategoryIDStatic = new FareEvasionCategoryRelations().FareEvasionIncidentEntityUsingFareEvasionCategoryID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new FareEvasionCategoryRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticFareEvasionCategoryRelations()
		{
		}
	}
}
