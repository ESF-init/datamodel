﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PageContentGroup. </summary>
	public partial class PageContentGroupRelations
	{
		/// <summary>CTor</summary>
		public PageContentGroupRelations()
		{
		}

		/// <summary>Gets all relations of the PageContentGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PageContentGroupToContentEntityUsingPageContentGroupid);
			toReturn.Add(this.TicketEntityUsingPageContentGroupID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PageContentGroupEntity and PageContentGroupToContentEntity over the 1:n relation they have, using the relation between the fields:
		/// PageContentGroup.PageContentGroupID - PageContentGroupToContent.PageContentGroupid
		/// </summary>
		public virtual IEntityRelation PageContentGroupToContentEntityUsingPageContentGroupid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PageContentGroupToContents" , true);
				relation.AddEntityFieldPair(PageContentGroupFields.PageContentGroupID, PageContentGroupToContentFields.PageContentGroupid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupToContentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PageContentGroupEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// PageContentGroup.PageContentGroupID - Ticket.PageContentGroupID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingPageContentGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(PageContentGroupFields.PageContentGroupID, TicketFields.PageContentGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PageContentGroupEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// PageContentGroup.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, PageContentGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PageContentGroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPageContentGroupRelations
	{
		internal static readonly IEntityRelation PageContentGroupToContentEntityUsingPageContentGroupidStatic = new PageContentGroupRelations().PageContentGroupToContentEntityUsingPageContentGroupid;
		internal static readonly IEntityRelation TicketEntityUsingPageContentGroupIDStatic = new PageContentGroupRelations().TicketEntityUsingPageContentGroupID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new PageContentGroupRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticPageContentGroupRelations()
		{
		}
	}
}
