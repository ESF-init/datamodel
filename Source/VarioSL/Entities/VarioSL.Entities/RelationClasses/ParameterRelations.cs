﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Parameter. </summary>
	public partial class ParameterRelations
	{
		/// <summary>CTor</summary>
		public ParameterRelations()
		{
		}

		/// <summary>Gets all relations of the ParameterEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParameterGroupToParameterEntityUsingParameterID);
			toReturn.Add(this.ParameterValueEntityUsingParameterID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ParameterEntity and ParameterGroupToParameterEntity over the 1:n relation they have, using the relation between the fields:
		/// Parameter.ParameterID - ParameterGroupToParameter.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterGroupToParameterEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterGroupToParameters" , true);
				relation.AddEntityFieldPair(ParameterFields.ParameterID, ParameterGroupToParameterFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterGroupToParameterEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ParameterEntity and ParameterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// Parameter.ParameterID - ParameterValue.ParameterID
		/// </summary>
		public virtual IEntityRelation ParameterValueEntityUsingParameterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterValues" , true);
				relation.AddEntityFieldPair(ParameterFields.ParameterID, ParameterValueFields.ParameterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ParameterEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// Parameter.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParameterRelations
	{
		internal static readonly IEntityRelation ParameterGroupToParameterEntityUsingParameterIDStatic = new ParameterRelations().ParameterGroupToParameterEntityUsingParameterID;
		internal static readonly IEntityRelation ParameterValueEntityUsingParameterIDStatic = new ParameterRelations().ParameterValueEntityUsingParameterID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new ParameterRelations().DeviceClassEntityUsingDeviceClassID;

		/// <summary>CTor</summary>
		static StaticParameterRelations()
		{
		}
	}
}
