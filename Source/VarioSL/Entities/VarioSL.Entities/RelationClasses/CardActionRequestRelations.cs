﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardActionRequest. </summary>
	public partial class CardActionRequestRelations
	{
		/// <summary>CTor</summary>
		public CardActionRequestRelations()
		{
		}

		/// <summary>Gets all relations of the CardActionRequestEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardActionAttributeEntityUsingCardActionRequestID);
			toReturn.Add(this.ProductOnCardEntityUsingCardActionRequestID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardActionRequestEntity and CardActionAttributeEntity over the 1:n relation they have, using the relation between the fields:
		/// CardActionRequest.CardActionRequestID - CardActionAttribute.CardActionRequestID
		/// </summary>
		public virtual IEntityRelation CardActionAttributeEntityUsingCardActionRequestID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardActionAttributes" , true);
				relation.AddEntityFieldPair(CardActionRequestFields.CardActionRequestID, CardActionAttributeFields.CardActionRequestID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardActionRequestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardActionAttributeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardActionRequestEntity and ProductOnCardEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardActionRequest.CardActionRequestID - ProductOnCard.CardActionRequestID
		/// </summary>
		public virtual IEntityRelation ProductOnCardEntityUsingCardActionRequestID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ProductOnCard", true);


				relation.AddEntityFieldPair(CardActionRequestFields.CardActionRequestID, ProductOnCardFields.CardActionRequestID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardActionRequestEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductOnCardEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardActionRequestRelations
	{
		internal static readonly IEntityRelation CardActionAttributeEntityUsingCardActionRequestIDStatic = new CardActionRequestRelations().CardActionAttributeEntityUsingCardActionRequestID;
		internal static readonly IEntityRelation ProductOnCardEntityUsingCardActionRequestIDStatic = new CardActionRequestRelations().ProductOnCardEntityUsingCardActionRequestID;

		/// <summary>CTor</summary>
		static StaticCardActionRequestRelations()
		{
		}
	}
}
