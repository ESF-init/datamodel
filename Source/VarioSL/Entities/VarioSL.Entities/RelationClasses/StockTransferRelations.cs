﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: StockTransfer. </summary>
	public partial class StockTransferRelations
	{
		/// <summary>CTor</summary>
		public StockTransferRelations()
		{
		}

		/// <summary>Gets all relations of the StockTransferEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardStockTransferEntityUsingStockTransferID);
			toReturn.Add(this.StorageLocationEntityUsingNewStorageLocationID);
			toReturn.Add(this.StorageLocationEntityUsingOldStorageLocationID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between StockTransferEntity and CardStockTransferEntity over the 1:n relation they have, using the relation between the fields:
		/// StockTransfer.StockTransferID - CardStockTransfer.StockTransferID
		/// </summary>
		public virtual IEntityRelation CardStockTransferEntityUsingStockTransferID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardStockTransfers" , true);
				relation.AddEntityFieldPair(StockTransferFields.StockTransferID, CardStockTransferFields.StockTransferID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardStockTransferEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between StockTransferEntity and StorageLocationEntity over the m:1 relation they have, using the relation between the fields:
		/// StockTransfer.NewStorageLocationID - StorageLocation.StorageLocationID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingNewStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NewStorageLocation", false);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, StockTransferFields.NewStorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between StockTransferEntity and StorageLocationEntity over the m:1 relation they have, using the relation between the fields:
		/// StockTransfer.OldStorageLocationID - StorageLocation.StorageLocationID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingOldStorageLocationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OldStorageLocation", false);
				relation.AddEntityFieldPair(StorageLocationFields.StorageLocationID, StockTransferFields.OldStorageLocationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticStockTransferRelations
	{
		internal static readonly IEntityRelation CardStockTransferEntityUsingStockTransferIDStatic = new StockTransferRelations().CardStockTransferEntityUsingStockTransferID;
		internal static readonly IEntityRelation StorageLocationEntityUsingNewStorageLocationIDStatic = new StockTransferRelations().StorageLocationEntityUsingNewStorageLocationID;
		internal static readonly IEntityRelation StorageLocationEntityUsingOldStorageLocationIDStatic = new StockTransferRelations().StorageLocationEntityUsingOldStorageLocationID;

		/// <summary>CTor</summary>
		static StaticStockTransferRelations()
		{
		}
	}
}
