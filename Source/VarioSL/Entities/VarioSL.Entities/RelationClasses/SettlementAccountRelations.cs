﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementAccount. </summary>
	public partial class SettlementAccountRelations
	{
		/// <summary>CTor</summary>
		public SettlementAccountRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementAccountEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SettlementSetupEntityUsingFromAccountID);
			toReturn.Add(this.SettlementSetupEntityUsingToAccountID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SettlementAccountEntity and SettlementSetupEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementAccount.SettlementAccountID - SettlementSetup.FromAccountID
		/// </summary>
		public virtual IEntityRelation SettlementSetupEntityUsingFromAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FromSettlementSetups" , true);
				relation.AddEntityFieldPair(SettlementAccountFields.SettlementAccountID, SettlementSetupFields.FromAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SettlementAccountEntity and SettlementSetupEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementAccount.SettlementAccountID - SettlementSetup.ToAccountID
		/// </summary>
		public virtual IEntityRelation SettlementSetupEntityUsingToAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ToSettlementSetups" , true);
				relation.AddEntityFieldPair(SettlementAccountFields.SettlementAccountID, SettlementSetupFields.ToAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementAccountRelations
	{
		internal static readonly IEntityRelation SettlementSetupEntityUsingFromAccountIDStatic = new SettlementAccountRelations().SettlementSetupEntityUsingFromAccountID;
		internal static readonly IEntityRelation SettlementSetupEntityUsingToAccountIDStatic = new SettlementAccountRelations().SettlementSetupEntityUsingToAccountID;

		/// <summary>CTor</summary>
		static StaticSettlementAccountRelations()
		{
		}
	}
}
