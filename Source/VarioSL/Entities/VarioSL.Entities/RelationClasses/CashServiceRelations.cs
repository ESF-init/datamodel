﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CashService. </summary>
	public partial class CashServiceRelations
	{
		/// <summary>CTor</summary>
		public CashServiceRelations()
		{
		}

		/// <summary>Gets all relations of the CashServiceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceBalanceEntityUsingCashServiceID);
			toReturn.Add(this.CashServiceDataEntityUsingCashServiceID);
			toReturn.Add(this.AutomatEntityUsingAutomatID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CashServiceEntity and CashServiceBalanceEntity over the 1:n relation they have, using the relation between the fields:
		/// CashService.CashServiceID - CashServiceBalance.CashServiceID
		/// </summary>
		public virtual IEntityRelation CashServiceBalanceEntityUsingCashServiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CashServiceBalance" , true);
				relation.AddEntityFieldPair(CashServiceFields.CashServiceID, CashServiceBalanceFields.CashServiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceBalanceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CashServiceEntity and CashServiceDataEntity over the 1:n relation they have, using the relation between the fields:
		/// CashService.CashServiceID - CashServiceData.CashServiceID
		/// </summary>
		public virtual IEntityRelation CashServiceDataEntityUsingCashServiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AmCashservicedatas" , true);
				relation.AddEntityFieldPair(CashServiceFields.CashServiceID, CashServiceDataFields.CashServiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceDataEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CashServiceEntity and AutomatEntity over the m:1 relation they have, using the relation between the fields:
		/// CashService.AutomatID - Automat.AutomatID
		/// </summary>
		public virtual IEntityRelation AutomatEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Automat", false);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, CashServiceFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCashServiceRelations
	{
		internal static readonly IEntityRelation CashServiceBalanceEntityUsingCashServiceIDStatic = new CashServiceRelations().CashServiceBalanceEntityUsingCashServiceID;
		internal static readonly IEntityRelation CashServiceDataEntityUsingCashServiceIDStatic = new CashServiceRelations().CashServiceDataEntityUsingCashServiceID;
		internal static readonly IEntityRelation AutomatEntityUsingAutomatIDStatic = new CashServiceRelations().AutomatEntityUsingAutomatID;

		/// <summary>CTor</summary>
		static StaticCashServiceRelations()
		{
		}
	}
}
