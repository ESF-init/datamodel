﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserToWorkstation. </summary>
	public partial class UserToWorkstationRelations
	{
		/// <summary>CTor</summary>
		public UserToWorkstationRelations()
		{
		}

		/// <summary>Gets all relations of the UserToWorkstationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserListEntityUsingUserID);
			toReturn.Add(this.WorkstationEntityUsingWorkstationID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UserToWorkstationEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// UserToWorkstation.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "User", false);
				relation.AddEntityFieldPair(UserListFields.UserID, UserToWorkstationFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserToWorkstationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserToWorkstationEntity and WorkstationEntity over the m:1 relation they have, using the relation between the fields:
		/// UserToWorkstation.WorkstationID - Workstation.WorkstationID
		/// </summary>
		public virtual IEntityRelation WorkstationEntityUsingWorkstationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Workstation", false);
				relation.AddEntityFieldPair(WorkstationFields.WorkstationID, UserToWorkstationFields.WorkstationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkstationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserToWorkstationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserToWorkstationRelations
	{
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new UserToWorkstationRelations().UserListEntityUsingUserID;
		internal static readonly IEntityRelation WorkstationEntityUsingWorkstationIDStatic = new UserToWorkstationRelations().WorkstationEntityUsingWorkstationID;

		/// <summary>CTor</summary>
		static StaticUserToWorkstationRelations()
		{
		}
	}
}
