﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CreditCardAuthorization. </summary>
	public partial class CreditCardAuthorizationRelations
	{
		/// <summary>CTor</summary>
		public CreditCardAuthorizationRelations()
		{
		}

		/// <summary>Gets all relations of the CreditCardAuthorizationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CreditCardAuthorizationLogEntityUsingCreditCardAuthorizationID);
			toReturn.Add(this.SaleEntityUsingCreditCardAuthorizationID);
			toReturn.Add(this.TransactionJournalEntityUsingCreditCardAuthorizationID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.CreditCardTerminalEntityUsingCreditCardTerminalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CreditCardAuthorizationEntity and CreditCardAuthorizationLogEntity over the 1:n relation they have, using the relation between the fields:
		/// CreditCardAuthorization.CreditCardAuthorizationID - CreditCardAuthorizationLog.CreditCardAuthorizationID
		/// </summary>
		public virtual IEntityRelation CreditCardAuthorizationLogEntityUsingCreditCardAuthorizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CreditCardAuthorizationLogs" , true);
				relation.AddEntityFieldPair(CreditCardAuthorizationFields.CreditCardAuthorizationID, CreditCardAuthorizationLogFields.CreditCardAuthorizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationLogEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CreditCardAuthorizationEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// CreditCardAuthorization.CreditCardAuthorizationID - Sale.CreditCardAuthorizationID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingCreditCardAuthorizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(CreditCardAuthorizationFields.CreditCardAuthorizationID, SaleFields.CreditCardAuthorizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CreditCardAuthorizationEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// CreditCardAuthorization.CreditCardAuthorizationID - TransactionJournal.CreditCardAuthorizationID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingCreditCardAuthorizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(CreditCardAuthorizationFields.CreditCardAuthorizationID, TransactionJournalFields.CreditCardAuthorizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CreditCardAuthorizationEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// CreditCardAuthorization.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, CreditCardAuthorizationFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CreditCardAuthorizationEntity and CreditCardTerminalEntity over the m:1 relation they have, using the relation between the fields:
		/// CreditCardAuthorization.CreditCardTerminalID - CreditCardTerminal.CreditCardTerminalID
		/// </summary>
		public virtual IEntityRelation CreditCardTerminalEntityUsingCreditCardTerminalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CreditCardTerminal", false);
				relation.AddEntityFieldPair(CreditCardTerminalFields.CreditCardTerminalID, CreditCardAuthorizationFields.CreditCardTerminalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardTerminalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCreditCardAuthorizationRelations
	{
		internal static readonly IEntityRelation CreditCardAuthorizationLogEntityUsingCreditCardAuthorizationIDStatic = new CreditCardAuthorizationRelations().CreditCardAuthorizationLogEntityUsingCreditCardAuthorizationID;
		internal static readonly IEntityRelation SaleEntityUsingCreditCardAuthorizationIDStatic = new CreditCardAuthorizationRelations().SaleEntityUsingCreditCardAuthorizationID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingCreditCardAuthorizationIDStatic = new CreditCardAuthorizationRelations().TransactionJournalEntityUsingCreditCardAuthorizationID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new CreditCardAuthorizationRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation CreditCardTerminalEntityUsingCreditCardTerminalIDStatic = new CreditCardAuthorizationRelations().CreditCardTerminalEntityUsingCreditCardTerminalID;

		/// <summary>CTor</summary>
		static StaticCreditCardAuthorizationRelations()
		{
		}
	}
}
