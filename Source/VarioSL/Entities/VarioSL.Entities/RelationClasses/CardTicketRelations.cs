﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardTicket. </summary>
	public partial class CardTicketRelations
	{
		/// <summary>CTor</summary>
		public CardTicketRelations()
		{
		}

		/// <summary>Gets all relations of the CardTicketEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardTicketToTicketEntityUsingCardTicketID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.CardPhysicalDetailEntityUsingCardPhysicalDetailID);
			toReturn.Add(this.FormEntityUsingFormID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardTicketEntity and CardTicketToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// CardTicket.CardTicketID - CardTicketToTicket.CardTicketID
		/// </summary>
		public virtual IEntityRelation CardTicketToTicketEntityUsingCardTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardTicketToTicket" , true);
				relation.AddEntityFieldPair(CardTicketFields.CardTicketID, CardTicketToTicketFields.CardTicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketToTicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardTicketEntity and TicketEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardTicket.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "AssignedCardTicket", false);




				relation.AddEntityFieldPair(TicketFields.TicketID, CardTicketFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardTicketEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// CardTicket.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, CardTicketFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardTicketEntity and CardPhysicalDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// CardTicket.CardPhysicalDetailID - CardPhysicalDetail.CardPhysicalDetailID
		/// </summary>
		public virtual IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardPhysicalDetail", false);
				relation.AddEntityFieldPair(CardPhysicalDetailFields.CardPhysicalDetailID, CardTicketFields.CardPhysicalDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardTicketEntity and FormEntity over the m:1 relation they have, using the relation between the fields:
		/// CardTicket.FormID - Form.FormID
		/// </summary>
		public virtual IEntityRelation FormEntityUsingFormID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Form", false);
				relation.AddEntityFieldPair(FormFields.FormID, CardTicketFields.FormID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FormEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardTicketEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardTicketRelations
	{
		internal static readonly IEntityRelation CardTicketToTicketEntityUsingCardTicketIDStatic = new CardTicketRelations().CardTicketToTicketEntityUsingCardTicketID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new CardTicketRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new CardTicketRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation CardPhysicalDetailEntityUsingCardPhysicalDetailIDStatic = new CardTicketRelations().CardPhysicalDetailEntityUsingCardPhysicalDetailID;
		internal static readonly IEntityRelation FormEntityUsingFormIDStatic = new CardTicketRelations().FormEntityUsingFormID;

		/// <summary>CTor</summary>
		static StaticCardTicketRelations()
		{
		}
	}
}
