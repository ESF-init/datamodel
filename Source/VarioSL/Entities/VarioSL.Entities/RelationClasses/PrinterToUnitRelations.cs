﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PrinterToUnit. </summary>
	public partial class PrinterToUnitRelations
	{
		/// <summary>CTor</summary>
		public PrinterToUnitRelations()
		{
		}

		/// <summary>Gets all relations of the PrinterToUnitEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UnitEntityUsingUnitID);
			toReturn.Add(this.PrinterEntityUsingPrinterID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PrinterToUnitEntity and UnitEntity over the m:1 relation they have, using the relation between the fields:
		/// PrinterToUnit.UnitID - Unit.UnitID
		/// </summary>
		public virtual IEntityRelation UnitEntityUsingUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Unit", false);
				relation.AddEntityFieldPair(UnitFields.UnitID, PrinterToUnitFields.UnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrinterToUnitEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PrinterToUnitEntity and PrinterEntity over the m:1 relation they have, using the relation between the fields:
		/// PrinterToUnit.PrinterID - Printer.PrinterID
		/// </summary>
		public virtual IEntityRelation PrinterEntityUsingPrinterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Printer", false);
				relation.AddEntityFieldPair(PrinterFields.PrinterID, PrinterToUnitFields.PrinterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrinterEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrinterToUnitEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPrinterToUnitRelations
	{
		internal static readonly IEntityRelation UnitEntityUsingUnitIDStatic = new PrinterToUnitRelations().UnitEntityUsingUnitID;
		internal static readonly IEntityRelation PrinterEntityUsingPrinterIDStatic = new PrinterToUnitRelations().PrinterEntityUsingPrinterID;

		/// <summary>CTor</summary>
		static StaticPrinterToUnitRelations()
		{
		}
	}
}
