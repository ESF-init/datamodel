﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AccountEntry. </summary>
	public partial class AccountEntryRelations
	{
		/// <summary>CTor</summary>
		public AccountEntryRelations()
		{
		}

		/// <summary>Gets all relations of the AccountEntryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DebtorEntityUsingContractID);
			toReturn.Add(this.UserListEntityUsingUserID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AccountEntryEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// AccountEntry.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, AccountEntryFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AccountEntryEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// AccountEntry.ContractID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Debtor", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, AccountEntryFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AccountEntryEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// AccountEntry.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "User", false);
				relation.AddEntityFieldPair(UserListFields.UserID, AccountEntryFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAccountEntryRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new AccountEntryRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DebtorEntityUsingContractIDStatic = new AccountEntryRelations().DebtorEntityUsingContractID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new AccountEntryRelations().UserListEntityUsingUserID;

		/// <summary>CTor</summary>
		static StaticAccountEntryRelations()
		{
		}
	}
}
