﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardChipType. </summary>
	public partial class CardChipTypeRelations
	{
		/// <summary>CTor</summary>
		public CardChipTypeRelations()
		{
		}

		/// <summary>Gets all relations of the CardChipTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID);
			toReturn.Add(this.CardPhysicalDetailEntityUsingChipTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CardChipTypeEntity and TicketPhysicalCardTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// CardChipType.CardChipTypeID - TicketPhysicalCardType.PhysicalCardTypeID
		/// </summary>
		public virtual IEntityRelation TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketCardChipTypes" , true);
				relation.AddEntityFieldPair(CardChipTypeFields.CardChipTypeID, TicketPhysicalCardTypeFields.PhysicalCardTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardChipTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketPhysicalCardTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardChipTypeEntity and CardPhysicalDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// CardChipType.CardChipTypeID - CardPhysicalDetail.ChipTypeID
		/// </summary>
		public virtual IEntityRelation CardPhysicalDetailEntityUsingChipTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardPhysicalDetails" , true);
				relation.AddEntityFieldPair(CardChipTypeFields.CardChipTypeID, CardPhysicalDetailFields.ChipTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardChipTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardPhysicalDetailEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardChipTypeRelations
	{
		internal static readonly IEntityRelation TicketPhysicalCardTypeEntityUsingPhysicalCardTypeIDStatic = new CardChipTypeRelations().TicketPhysicalCardTypeEntityUsingPhysicalCardTypeID;
		internal static readonly IEntityRelation CardPhysicalDetailEntityUsingChipTypeIDStatic = new CardChipTypeRelations().CardPhysicalDetailEntityUsingChipTypeID;

		/// <summary>CTor</summary>
		static StaticCardChipTypeRelations()
		{
		}
	}
}
