﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VirtualCard. </summary>
	public partial class VirtualCardRelations
	{
		/// <summary>CTor</summary>
		public VirtualCardRelations()
		{
		}

		/// <summary>Gets all relations of the VirtualCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.NotificationDeviceEntityUsingNotificationDeviceID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between VirtualCardEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// VirtualCard.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, VirtualCardFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VirtualCardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VirtualCardEntity and NotificationDeviceEntity over the m:1 relation they have, using the relation between the fields:
		/// VirtualCard.NotificationDeviceID - NotificationDevice.NotificationDeviceID
		/// </summary>
		public virtual IEntityRelation NotificationDeviceEntityUsingNotificationDeviceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "NotificationDevice", false);
				relation.AddEntityFieldPair(NotificationDeviceFields.NotificationDeviceID, VirtualCardFields.NotificationDeviceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationDeviceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VirtualCardEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVirtualCardRelations
	{
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new VirtualCardRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation NotificationDeviceEntityUsingNotificationDeviceIDStatic = new VirtualCardRelations().NotificationDeviceEntityUsingNotificationDeviceID;

		/// <summary>CTor</summary>
		static StaticVirtualCardRelations()
		{
		}
	}
}
