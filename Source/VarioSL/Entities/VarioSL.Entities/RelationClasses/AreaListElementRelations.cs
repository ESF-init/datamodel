﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AreaListElement. </summary>
	public partial class AreaListElementRelations
	{
		/// <summary>CTor</summary>
		public AreaListElementRelations()
		{
		}

		/// <summary>Gets all relations of the AreaListElementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AreaListElementGroupEntityUsingAreaListID);
			toReturn.Add(this.AreaTypeEntityUsingAreaInstanceTypeID);
			toReturn.Add(this.FareStageEntityUsingFareStageID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AreaListElementEntity and AreaListElementGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaListElement.AreaListID - AreaListElementGroup.AreaListeElementGroupId
		/// </summary>
		public virtual IEntityRelation AreaListElementGroupEntityUsingAreaListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AreaListElementGroup", false);
				relation.AddEntityFieldPair(AreaListElementGroupFields.AreaListeElementGroupId, AreaListElementFields.AreaListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListElementEntity and AreaTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaListElement.AreaInstanceTypeID - AreaType.TypeID
		/// </summary>
		public virtual IEntityRelation AreaTypeEntityUsingAreaInstanceTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AreaTypeElement", false);
				relation.AddEntityFieldPair(AreaTypeFields.TypeID, AreaListElementFields.AreaInstanceTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListElementEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaListElement.FareStageID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, AreaListElementFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between AreaListElementEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// AreaListElement.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, AreaListElementFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AreaListElementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAreaListElementRelations
	{
		internal static readonly IEntityRelation AreaListElementGroupEntityUsingAreaListIDStatic = new AreaListElementRelations().AreaListElementGroupEntityUsingAreaListID;
		internal static readonly IEntityRelation AreaTypeEntityUsingAreaInstanceTypeIDStatic = new AreaListElementRelations().AreaTypeEntityUsingAreaInstanceTypeID;
		internal static readonly IEntityRelation FareStageEntityUsingFareStageIDStatic = new AreaListElementRelations().FareStageEntityUsingFareStageID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new AreaListElementRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticAreaListElementRelations()
		{
		}
	}
}
