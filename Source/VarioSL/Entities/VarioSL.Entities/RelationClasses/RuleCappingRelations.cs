﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RuleCapping. </summary>
	public partial class RuleCappingRelations
	{
		/// <summary>CTor</summary>
		public RuleCappingRelations()
		{
		}

		/// <summary>Gets all relations of the RuleCappingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RuleCappingToTicketEntityUsingRuleCappingID);
			toReturn.Add(this.CalendarEntityUsingCalendarID);
			toReturn.Add(this.RulePeriodEntityUsingRulePeriodID);
			toReturn.Add(this.RuleTypeEntityUsingRuleTypeID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RuleCappingEntity and RuleCappingToTicketEntity over the 1:n relation they have, using the relation between the fields:
		/// RuleCapping.RuleCappingID - RuleCappingToTicket.RuleCappingID
		/// </summary>
		public virtual IEntityRelation RuleCappingToTicketEntityUsingRuleCappingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleCappingToTicket" , true);
				relation.AddEntityFieldPair(RuleCappingFields.RuleCappingID, RuleCappingToTicketFields.RuleCappingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingToTicketEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RuleCappingEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleCapping.CalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Calendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, RuleCappingFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleCappingEntity and RulePeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleCapping.RulePeriodID - RulePeriod.RulePeriodID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RulePeriod", false);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, RuleCappingFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleCappingEntity and RuleTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleCapping.RuleTypeID - RuleType.RuleTypeID
		/// </summary>
		public virtual IEntityRelation RuleTypeEntityUsingRuleTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TmRuleType", false);
				relation.AddEntityFieldPair(RuleTypeFields.RuleTypeID, RuleCappingFields.RuleTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleCappingEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleCapping.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, RuleCappingFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleCappingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRuleCappingRelations
	{
		internal static readonly IEntityRelation RuleCappingToTicketEntityUsingRuleCappingIDStatic = new RuleCappingRelations().RuleCappingToTicketEntityUsingRuleCappingID;
		internal static readonly IEntityRelation CalendarEntityUsingCalendarIDStatic = new RuleCappingRelations().CalendarEntityUsingCalendarID;
		internal static readonly IEntityRelation RulePeriodEntityUsingRulePeriodIDStatic = new RuleCappingRelations().RulePeriodEntityUsingRulePeriodID;
		internal static readonly IEntityRelation RuleTypeEntityUsingRuleTypeIDStatic = new RuleCappingRelations().RuleTypeEntityUsingRuleTypeID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new RuleCappingRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticRuleCappingRelations()
		{
		}
	}
}
