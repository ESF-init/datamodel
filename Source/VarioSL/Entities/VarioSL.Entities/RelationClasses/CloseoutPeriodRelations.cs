﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CloseoutPeriod. </summary>
	public partial class CloseoutPeriodRelations
	{
		/// <summary>CTor</summary>
		public CloseoutPeriodRelations()
		{
		}

		/// <summary>Gets all relations of the CloseoutPeriodEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ApportionEntityUsingCloseoutPeriodID);
			toReturn.Add(this.PaymentRecognitionEntityUsingCloseoutPeriodID);
			toReturn.Add(this.PaymentReconciliationEntityUsingCloseoutPeriodID);
			toReturn.Add(this.RevenueRecognitionEntityUsingCloseoutPeriodID);
			toReturn.Add(this.RevenueSettlementEntityUsingCloseoutPeriodID);
			toReturn.Add(this.SalesRevenueEntityUsingCloseoutPeriodID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and ApportionEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - Apportion.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation ApportionEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Apportions" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, ApportionFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ApportionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and PaymentRecognitionEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - PaymentRecognition.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation PaymentRecognitionEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentRecognitions" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, PaymentRecognitionFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentRecognitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and PaymentReconciliationEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - PaymentReconciliation.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation PaymentReconciliationEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentReconciliations" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, PaymentReconciliationFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentReconciliationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and RevenueRecognitionEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - RevenueRecognition.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation RevenueRecognitionEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueRecognitions" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, RevenueRecognitionFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and RevenueSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - RevenueSettlement.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation RevenueSettlementEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueSettlements" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, RevenueSettlementFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CloseoutPeriodEntity and SalesRevenueEntity over the 1:n relation they have, using the relation between the fields:
		/// CloseoutPeriod.CloseoutPeriodID - SalesRevenue.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation SalesRevenueEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SalesRevenues" , true);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, SalesRevenueFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesRevenueEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCloseoutPeriodRelations
	{
		internal static readonly IEntityRelation ApportionEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().ApportionEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation PaymentRecognitionEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().PaymentRecognitionEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation PaymentReconciliationEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().PaymentReconciliationEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation RevenueRecognitionEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().RevenueRecognitionEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation RevenueSettlementEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().RevenueSettlementEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation SalesRevenueEntityUsingCloseoutPeriodIDStatic = new CloseoutPeriodRelations().SalesRevenueEntityUsingCloseoutPeriodID;

		/// <summary>CTor</summary>
		static StaticCloseoutPeriodRelations()
		{
		}
	}
}
