﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ReportJob. </summary>
	public partial class ReportJobRelations
	{
		/// <summary>CTor</summary>
		public ReportJobRelations()
		{
		}

		/// <summary>Gets all relations of the ReportJobEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ReportJobResultEntityUsingReportJobID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.UserListEntityUsingUserID);
			toReturn.Add(this.FilterValueSetEntityUsingFilterValueSetID);
			toReturn.Add(this.ReportEntityUsingReportID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ReportJobEntity and ReportJobResultEntity over the 1:n relation they have, using the relation between the fields:
		/// ReportJob.ReportJobID - ReportJobResult.ReportJobID
		/// </summary>
		public virtual IEntityRelation ReportJobResultEntityUsingReportJobID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportJobResults" , true);
				relation.AddEntityFieldPair(ReportJobFields.ReportJobID, ReportJobResultFields.ReportJobID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobResultEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ReportJobEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJob.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ReportJobFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportJobEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJob.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, ReportJobFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportJobEntity and FilterValueSetEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJob.FilterValueSetID - FilterValueSet.FilterValueSetID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingFilterValueSetID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterValueSet", false);
				relation.AddEntityFieldPair(FilterValueSetFields.FilterValueSetID, ReportJobFields.FilterValueSetID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ReportJobEntity and ReportEntity over the m:1 relation they have, using the relation between the fields:
		/// ReportJob.ReportID - Report.ReportID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Report", false);
				relation.AddEntityFieldPair(ReportFields.ReportID, ReportJobFields.ReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportJobEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticReportJobRelations
	{
		internal static readonly IEntityRelation ReportJobResultEntityUsingReportJobIDStatic = new ReportJobRelations().ReportJobResultEntityUsingReportJobID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ReportJobRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new ReportJobRelations().UserListEntityUsingUserID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingFilterValueSetIDStatic = new ReportJobRelations().FilterValueSetEntityUsingFilterValueSetID;
		internal static readonly IEntityRelation ReportEntityUsingReportIDStatic = new ReportJobRelations().ReportEntityUsingReportID;

		/// <summary>CTor</summary>
		static StaticReportJobRelations()
		{
		}
	}
}
