﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FilterList. </summary>
	public partial class FilterListRelations
	{
		/// <summary>CTor</summary>
		public FilterListRelations()
		{
		}

		/// <summary>Gets all relations of the FilterListEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FilterListElementEntityUsingFilterListID);
			toReturn.Add(this.FilterValueSetEntityUsingFilterListID);
			toReturn.Add(this.ReportEntityUsingFilterListID);
			toReturn.Add(this.ReportCategoryEntityUsingCategoryFilterListID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FilterListEntity and FilterListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterList.FilterListID - FilterListElement.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterListElementEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterListElements" , true);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, FilterListElementFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FilterListEntity and FilterValueSetEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterList.FilterListID - FilterValueSet.FilterListID
		/// </summary>
		public virtual IEntityRelation FilterValueSetEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterValueSets" , true);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, FilterValueSetFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterValueSetEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FilterListEntity and ReportEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterList.FilterListID - Report.FilterListID
		/// </summary>
		public virtual IEntityRelation ReportEntityUsingFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Reports" , true);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, ReportFields.FilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between FilterListEntity and ReportCategoryEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterList.FilterListID - ReportCategory.CategoryFilterListID
		/// </summary>
		public virtual IEntityRelation ReportCategoryEntityUsingCategoryFilterListID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ReportCategories" , true);
				relation.AddEntityFieldPair(FilterListFields.FilterListID, ReportCategoryFields.CategoryFilterListID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ReportCategoryEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFilterListRelations
	{
		internal static readonly IEntityRelation FilterListElementEntityUsingFilterListIDStatic = new FilterListRelations().FilterListElementEntityUsingFilterListID;
		internal static readonly IEntityRelation FilterValueSetEntityUsingFilterListIDStatic = new FilterListRelations().FilterValueSetEntityUsingFilterListID;
		internal static readonly IEntityRelation ReportEntityUsingFilterListIDStatic = new FilterListRelations().ReportEntityUsingFilterListID;
		internal static readonly IEntityRelation ReportCategoryEntityUsingCategoryFilterListIDStatic = new FilterListRelations().ReportCategoryEntityUsingCategoryFilterListID;

		/// <summary>CTor</summary>
		static StaticFilterListRelations()
		{
		}
	}
}
