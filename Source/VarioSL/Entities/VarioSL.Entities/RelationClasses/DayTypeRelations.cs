﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DayType. </summary>
	public partial class DayTypeRelations
	{
		/// <summary>CTor</summary>
		public DayTypeRelations()
		{
		}

		/// <summary>Gets all relations of the DayTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CalendarEntryEntityUsingDayTypeEntryID);
			toReturn.Add(this.TicketDayTypeEntityUsingDayTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DayTypeEntity and CalendarEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// DayType.DayTypeID - CalendarEntry.DayTypeEntryID
		/// </summary>
		public virtual IEntityRelation CalendarEntryEntityUsingDayTypeEntryID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CalendarEntries" , true);
				relation.AddEntityFieldPair(DayTypeFields.DayTypeID, CalendarEntryFields.DayTypeEntryID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DayTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DayTypeEntity and TicketDayTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// DayType.DayTypeID - TicketDayType.DayTypeID
		/// </summary>
		public virtual IEntityRelation TicketDayTypeEntityUsingDayTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDayTypes" , true);
				relation.AddEntityFieldPair(DayTypeFields.DayTypeID, TicketDayTypeFields.DayTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DayTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDayTypeEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDayTypeRelations
	{
		internal static readonly IEntityRelation CalendarEntryEntityUsingDayTypeEntryIDStatic = new DayTypeRelations().CalendarEntryEntityUsingDayTypeEntryID;
		internal static readonly IEntityRelation TicketDayTypeEntityUsingDayTypeIDStatic = new DayTypeRelations().TicketDayTypeEntityUsingDayTypeID;

		/// <summary>CTor</summary>
		static StaticDayTypeRelations()
		{
		}
	}
}
