﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomerAccount. </summary>
	public partial class CustomerAccountRelations
	{
		/// <summary>CTor</summary>
		public CustomerAccountRelations()
		{
		}

		/// <summary>Gets all relations of the CustomerAccountEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccountMessageSettingEntityUsingCustomerAccountID);
			toReturn.Add(this.CustomerAccountNotificationEntityUsingCustomerAccountID);
			toReturn.Add(this.CustomerAccountToVerificationAttemptTypeEntityUsingCustomerAccountID);
			toReturn.Add(this.NotificationMessageToCustomerEntityUsingCustomerAccountID);
			toReturn.Add(this.SecurityResponseEntityUsingCustomerAccountID);
			toReturn.Add(this.VerificationAttemptEntityUsingCustomerAccountID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.CustomerAccountRoleEntityUsingCustomerAccountRoleID);
			toReturn.Add(this.CustomerLanguageEntityUsingCustomerLanguageID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and AccountMessageSettingEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - AccountMessageSetting.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation AccountMessageSettingEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountMessageSettings" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, AccountMessageSettingFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountMessageSettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and CustomerAccountNotificationEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - CustomerAccountNotification.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation CustomerAccountNotificationEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccountNotifications" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, CustomerAccountNotificationFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and CustomerAccountToVerificationAttemptTypeEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - CustomerAccountToVerificationAttemptType.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation CustomerAccountToVerificationAttemptTypeEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccountToVerificationAttemptTypes" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, CustomerAccountToVerificationAttemptTypeFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountToVerificationAttemptTypeEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and NotificationMessageToCustomerEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - NotificationMessageToCustomer.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation NotificationMessageToCustomerEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessageToCustomers" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, NotificationMessageToCustomerFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageToCustomerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and SecurityResponseEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - SecurityResponse.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation SecurityResponseEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SecurityResponses" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, SecurityResponseFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SecurityResponseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and VerificationAttemptEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountID - VerificationAttempt.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation VerificationAttemptEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VerificationAttempts" , true);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, VerificationAttemptFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VerificationAttemptEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccount.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, CustomerAccountFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and CustomerAccountRoleEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerAccountRoleID - CustomerAccountRole.CustomerAccountRoleID
		/// </summary>
		public virtual IEntityRelation CustomerAccountRoleEntityUsingCustomerAccountRoleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerAccountRole", false);
				relation.AddEntityFieldPair(CustomerAccountRoleFields.CustomerAccountRoleID, CustomerAccountFields.CustomerAccountRoleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountRoleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and CustomerLanguageEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccount.CustomerLanguageID - CustomerLanguage.CustomerLanguageID
		/// </summary>
		public virtual IEntityRelation CustomerLanguageEntityUsingCustomerLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerLanguage", false);
				relation.AddEntityFieldPair(CustomerLanguageFields.CustomerLanguageID, CustomerAccountFields.CustomerLanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerLanguageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomerAccountEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccount.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, CustomerAccountFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomerAccountRelations
	{
		internal static readonly IEntityRelation AccountMessageSettingEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().AccountMessageSettingEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation CustomerAccountNotificationEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().CustomerAccountNotificationEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation CustomerAccountToVerificationAttemptTypeEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().CustomerAccountToVerificationAttemptTypeEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation NotificationMessageToCustomerEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().NotificationMessageToCustomerEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation SecurityResponseEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().SecurityResponseEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation VerificationAttemptEntityUsingCustomerAccountIDStatic = new CustomerAccountRelations().VerificationAttemptEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new CustomerAccountRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation CustomerAccountRoleEntityUsingCustomerAccountRoleIDStatic = new CustomerAccountRelations().CustomerAccountRoleEntityUsingCustomerAccountRoleID;
		internal static readonly IEntityRelation CustomerLanguageEntityUsingCustomerLanguageIDStatic = new CustomerAccountRelations().CustomerLanguageEntityUsingCustomerLanguageID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new CustomerAccountRelations().PersonEntityUsingPersonID;

		/// <summary>CTor</summary>
		static StaticCustomerAccountRelations()
		{
		}
	}
}
