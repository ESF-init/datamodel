﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ProductRelation. </summary>
	public partial class ProductRelationRelations
	{
		/// <summary>CTor</summary>
		public ProductRelationRelations()
		{
		}

		/// <summary>Gets all relations of the ProductRelationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ProductEntityUsingProductID);
			toReturn.Add(this.AddressEntityUsingFromAddressID);
			toReturn.Add(this.AddressEntityUsingToAddressID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between ProductRelationEntity and ProductEntity over the 1:1 relation they have, using the relation between the fields:
		/// ProductRelation.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Product", false);




				relation.AddEntityFieldPair(ProductFields.ProductID, ProductRelationFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ProductRelationEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductRelation.FromAddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingFromAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FromAddress", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, ProductRelationFields.FromAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ProductRelationEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// ProductRelation.ToAddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingToAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ToAddress", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, ProductRelationFields.ToAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticProductRelationRelations
	{
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new ProductRelationRelations().ProductEntityUsingProductID;
		internal static readonly IEntityRelation AddressEntityUsingFromAddressIDStatic = new ProductRelationRelations().AddressEntityUsingFromAddressID;
		internal static readonly IEntityRelation AddressEntityUsingToAddressIDStatic = new ProductRelationRelations().AddressEntityUsingToAddressID;

		/// <summary>CTor</summary>
		static StaticProductRelationRelations()
		{
		}
	}
}
