﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SubsidyLimit. </summary>
	public partial class SubsidyLimitRelations
	{
		/// <summary>CTor</summary>
		public SubsidyLimitRelations()
		{
		}

		/// <summary>Gets all relations of the SubsidyLimitEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrganizationEntityUsingOrganizationID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between SubsidyLimitEntity and OrganizationEntity over the 1:1 relation they have, using the relation between the fields:
		/// SubsidyLimit.OrganizationID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Organization", false);




				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, SubsidyLimitFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SubsidyLimitEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSubsidyLimitRelations
	{
		internal static readonly IEntityRelation OrganizationEntityUsingOrganizationIDStatic = new SubsidyLimitRelations().OrganizationEntityUsingOrganizationID;

		/// <summary>CTor</summary>
		static StaticSubsidyLimitRelations()
		{
		}
	}
}
