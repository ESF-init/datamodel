﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderDetailToCard. </summary>
	public partial class OrderDetailToCardRelations
	{
		/// <summary>CTor</summary>
		public OrderDetailToCardRelations()
		{
		}

		/// <summary>Gets all relations of the OrderDetailToCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrderDetailToCardEntityUsingReplacedOrderDetailToCardID);
			toReturn.Add(this.OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between OrderDetailToCardEntity and OrderDetailToCardEntity over the 1:n relation they have, using the relation between the fields:
		/// OrderDetailToCard.OrderDetailToCardID - OrderDetailToCard.ReplacedOrderDetailToCardID
		/// </summary>
		public virtual IEntityRelation OrderDetailToCardEntityUsingReplacedOrderDetailToCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrderDetailToCards" , true);
				relation.AddEntityFieldPair(OrderDetailToCardFields.OrderDetailToCardID, OrderDetailToCardFields.ReplacedOrderDetailToCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between OrderDetailToCardEntity and OrderDetailToCardEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetailToCard.ReplacedOrderDetailToCardID - OrderDetailToCard.OrderDetailToCardID
		/// </summary>
		public virtual IEntityRelation OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderDetailToCard", false);
				relation.AddEntityFieldPair(OrderDetailToCardFields.OrderDetailToCardID, OrderDetailToCardFields.ReplacedOrderDetailToCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderDetailToCardRelations
	{
		internal static readonly IEntityRelation OrderDetailToCardEntityUsingReplacedOrderDetailToCardIDStatic = new OrderDetailToCardRelations().OrderDetailToCardEntityUsingReplacedOrderDetailToCardID;
		internal static readonly IEntityRelation OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardIDStatic = new OrderDetailToCardRelations().OrderDetailToCardEntityUsingOrderDetailToCardIDReplacedOrderDetailToCardID;

		/// <summary>CTor</summary>
		static StaticOrderDetailToCardRelations()
		{
		}
	}
}
