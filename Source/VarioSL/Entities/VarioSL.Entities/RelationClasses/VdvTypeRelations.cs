﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VdvType. </summary>
	public partial class VdvTypeRelations
	{
		/// <summary>CTor</summary>
		public VdvTypeRelations()
		{
		}

		/// <summary>Gets all relations of the VdvTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.VdvProductEntityUsingIssueModeNm);
			toReturn.Add(this.VdvProductEntityUsingIssueModeSe);
			toReturn.Add(this.VdvProductEntityUsingPriorityMode);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VdvTypeEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvType.TypeID - VdvProduct.IssueModeNm
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingIssueModeNm
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProductNmType" , true);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.IssueModeNm);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VdvTypeEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvType.TypeID - VdvProduct.IssueModeSe
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingIssueModeSe
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProductSeType" , true);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.IssueModeSe);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VdvTypeEntity and VdvProductEntity over the 1:n relation they have, using the relation between the fields:
		/// VdvType.TypeID - VdvProduct.PriorityMode
		/// </summary>
		public virtual IEntityRelation VdvProductEntityUsingPriorityMode
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "VdvProductsPriority" , true);
				relation.AddEntityFieldPair(VdvTypeFields.TypeID, VdvProductFields.PriorityMode);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VdvProductEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVdvTypeRelations
	{
		internal static readonly IEntityRelation VdvProductEntityUsingIssueModeNmStatic = new VdvTypeRelations().VdvProductEntityUsingIssueModeNm;
		internal static readonly IEntityRelation VdvProductEntityUsingIssueModeSeStatic = new VdvTypeRelations().VdvProductEntityUsingIssueModeSe;
		internal static readonly IEntityRelation VdvProductEntityUsingPriorityModeStatic = new VdvTypeRelations().VdvProductEntityUsingPriorityMode;

		/// <summary>CTor</summary>
		static StaticVdvTypeRelations()
		{
		}
	}
}
