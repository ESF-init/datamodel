﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AuditRegister. </summary>
	public partial class AuditRegisterRelations
	{
		/// <summary>CTor</summary>
		public AuditRegisterRelations()
		{
		}

		/// <summary>Gets all relations of the AuditRegisterEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AuditRegisterValueEntityUsingAuditRegisterID);
			toReturn.Add(this.ShiftInventoryEntityUsingShiftID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AuditRegisterEntity and AuditRegisterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// AuditRegister.AuditRegisterID - AuditRegisterValue.AuditRegisterID
		/// </summary>
		public virtual IEntityRelation AuditRegisterValueEntityUsingAuditRegisterID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AuditRegisterValues" , true);
				relation.AddEntityFieldPair(AuditRegisterFields.AuditRegisterID, AuditRegisterValueFields.AuditRegisterID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AuditRegisterEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AuditRegisterValueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between AuditRegisterEntity and ShiftInventoryEntity over the m:1 relation they have, using the relation between the fields:
		/// AuditRegister.ShiftID - ShiftInventory.HashValue
		/// </summary>
		public virtual IEntityRelation ShiftInventoryEntityUsingShiftID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ShiftInventory", false);
				relation.AddEntityFieldPair(ShiftInventoryFields.HashValue, AuditRegisterFields.ShiftID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftInventoryEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AuditRegisterEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAuditRegisterRelations
	{
		internal static readonly IEntityRelation AuditRegisterValueEntityUsingAuditRegisterIDStatic = new AuditRegisterRelations().AuditRegisterValueEntityUsingAuditRegisterID;
		internal static readonly IEntityRelation ShiftInventoryEntityUsingShiftIDStatic = new AuditRegisterRelations().ShiftInventoryEntityUsingShiftID;

		/// <summary>CTor</summary>
		static StaticAuditRegisterRelations()
		{
		}
	}
}
