﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Booking. </summary>
	public partial class BookingRelations
	{
		/// <summary>CTor</summary>
		public BookingRelations()
		{
		}

		/// <summary>Gets all relations of the BookingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BookingItemEntityUsingBookingID);
			toReturn.Add(this.CardEntityUsingCardId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BookingEntity and BookingItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Booking.BookingID - BookingItem.BookingID
		/// </summary>
		public virtual IEntityRelation BookingItemEntityUsingBookingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BookingItems" , true);
				relation.AddEntityFieldPair(BookingFields.BookingID, BookingItemFields.BookingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between BookingEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// Booking.CardId - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, BookingFields.CardId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBookingRelations
	{
		internal static readonly IEntityRelation BookingItemEntityUsingBookingIDStatic = new BookingRelations().BookingItemEntityUsingBookingID;
		internal static readonly IEntityRelation CardEntityUsingCardIdStatic = new BookingRelations().CardEntityUsingCardId;

		/// <summary>CTor</summary>
		static StaticBookingRelations()
		{
		}
	}
}
