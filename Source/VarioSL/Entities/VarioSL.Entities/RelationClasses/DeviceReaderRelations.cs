﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceReader. </summary>
	public partial class DeviceReaderRelations
	{
		/// <summary>CTor</summary>
		public DeviceReaderRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceReaderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceCommunicationHistoryEntityUsingDeviceReaderID);
			toReturn.Add(this.DeviceReaderCryptoResourceEntityUsingDeviceReaderID);
			toReturn.Add(this.DeviceReaderJobEntityUsingTargetReaderID);
			toReturn.Add(this.DeviceReaderJobEntityUsingWorkerReaderID);
			toReturn.Add(this.DeviceReaderKeyEntityUsingDeviceReaderID);
			toReturn.Add(this.DeviceReaderToCertEntityUsingDeviceReaderID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceCommunicationHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceCommunicationHistory.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceCommunicationHistoryEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceCommunicationHistories" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceCommunicationHistoryFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceCommunicationHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceReaderCryptoResourceEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceReaderCryptoResource.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderCryptoResourceEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderCryptoResources" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderCryptoResourceFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderCryptoResourceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceReaderJobEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceReaderJob.TargetReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderJobEntityUsingTargetReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TargetDeviceReaderJobs" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderJobFields.TargetReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderJobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceReaderJobEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceReaderJob.WorkerReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderJobEntityUsingWorkerReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkerDeviceReaderJobs" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderJobFields.WorkerReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderJobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceReaderKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceReaderKey.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderKeyEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderKeys" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderKeyFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderKeyEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceReaderEntity and DeviceReaderToCertEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceReader.DeviceReaderID - DeviceReaderToCert.DeviceReaderID
		/// </summary>
		public virtual IEntityRelation DeviceReaderToCertEntityUsingDeviceReaderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DeviceReaderToCerts" , true);
				relation.AddEntityFieldPair(DeviceReaderFields.DeviceReaderID, DeviceReaderToCertFields.DeviceReaderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceReaderToCertEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceReaderRelations
	{
		internal static readonly IEntityRelation DeviceCommunicationHistoryEntityUsingDeviceReaderIDStatic = new DeviceReaderRelations().DeviceCommunicationHistoryEntityUsingDeviceReaderID;
		internal static readonly IEntityRelation DeviceReaderCryptoResourceEntityUsingDeviceReaderIDStatic = new DeviceReaderRelations().DeviceReaderCryptoResourceEntityUsingDeviceReaderID;
		internal static readonly IEntityRelation DeviceReaderJobEntityUsingTargetReaderIDStatic = new DeviceReaderRelations().DeviceReaderJobEntityUsingTargetReaderID;
		internal static readonly IEntityRelation DeviceReaderJobEntityUsingWorkerReaderIDStatic = new DeviceReaderRelations().DeviceReaderJobEntityUsingWorkerReaderID;
		internal static readonly IEntityRelation DeviceReaderKeyEntityUsingDeviceReaderIDStatic = new DeviceReaderRelations().DeviceReaderKeyEntityUsingDeviceReaderID;
		internal static readonly IEntityRelation DeviceReaderToCertEntityUsingDeviceReaderIDStatic = new DeviceReaderRelations().DeviceReaderToCertEntityUsingDeviceReaderID;

		/// <summary>CTor</summary>
		static StaticDeviceReaderRelations()
		{
		}
	}
}
