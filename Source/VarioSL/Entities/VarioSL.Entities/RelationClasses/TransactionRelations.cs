﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Transaction. </summary>
	public partial class TransactionRelations
	{
		/// <summary>CTor</summary>
		public TransactionRelations()
		{
		}

		/// <summary>Gets all relations of the TransactionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PaymentDetailEntityUsingTransactionID);
			toReturn.Add(this.RuleViolationToTransactionEntityUsingTransactionID);
			toReturn.Add(this.ExternalDataEntityUsingTransactionID);
			toReturn.Add(this.TransactionExtensionEntityUsingTransactionID);
			toReturn.Add(this.ClientEntityUsingCardIssuerID);
			toReturn.Add(this.RevenueTransactionTypeEntityUsingTypeID);
			toReturn.Add(this.ShiftEntityUsingShiftID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and PaymentDetailEntity over the 1:n relation they have, using the relation between the fields:
		/// Transaction.TransactionID - PaymentDetail.TransactionID
		/// </summary>
		public virtual IEntityRelation PaymentDetailEntityUsingTransactionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentDetails" , true);
				relation.AddEntityFieldPair(TransactionFields.TransactionID, PaymentDetailFields.TransactionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentDetailEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and RuleViolationToTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// Transaction.TransactionID - RuleViolationToTransaction.TransactionID
		/// </summary>
		public virtual IEntityRelation RuleViolationToTransactionEntityUsingTransactionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolationToTransactions" , true);
				relation.AddEntityFieldPair(TransactionFields.TransactionID, RuleViolationToTransactionFields.TransactionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and ExternalDataEntity over the 1:1 relation they have, using the relation between the fields:
		/// Transaction.TransactionID - ExternalData.TransactionID
		/// </summary>
		public virtual IEntityRelation ExternalDataEntityUsingTransactionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "ExternalData", true);


				relation.AddEntityFieldPair(TransactionFields.TransactionID, ExternalDataFields.TransactionID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalDataEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and TransactionExtensionEntity over the 1:1 relation they have, using the relation between the fields:
		/// Transaction.TransactionID - TransactionExtension.TransactionID
		/// </summary>
		public virtual IEntityRelation TransactionExtensionEntityUsingTransactionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TransactionExtension", true);

				relation.AddEntityFieldPair(TransactionFields.TransactionID, TransactionExtensionFields.TransactionID);



				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionExtensionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Transaction.CardIssuerID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingCardIssuerID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardIssuer", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, TransactionFields.CardIssuerID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and RevenueTransactionTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Transaction.TypeID - RevenueTransactionType.TypeID
		/// </summary>
		public virtual IEntityRelation RevenueTransactionTypeEntityUsingTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransactionType", false);
				relation.AddEntityFieldPair(RevenueTransactionTypeFields.TypeID, TransactionFields.TypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueTransactionTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TransactionEntity and ShiftEntity over the m:1 relation they have, using the relation between the fields:
		/// Transaction.ShiftID - Shift.ShiftID
		/// </summary>
		public virtual IEntityRelation ShiftEntityUsingShiftID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Shift", false);
				relation.AddEntityFieldPair(ShiftFields.ShiftID, TransactionFields.ShiftID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTransactionRelations
	{
		internal static readonly IEntityRelation PaymentDetailEntityUsingTransactionIDStatic = new TransactionRelations().PaymentDetailEntityUsingTransactionID;
		internal static readonly IEntityRelation RuleViolationToTransactionEntityUsingTransactionIDStatic = new TransactionRelations().RuleViolationToTransactionEntityUsingTransactionID;
		internal static readonly IEntityRelation ExternalDataEntityUsingTransactionIDStatic = new TransactionRelations().ExternalDataEntityUsingTransactionID;
		internal static readonly IEntityRelation TransactionExtensionEntityUsingTransactionIDStatic = new TransactionRelations().TransactionExtensionEntityUsingTransactionID;
		internal static readonly IEntityRelation ClientEntityUsingCardIssuerIDStatic = new TransactionRelations().ClientEntityUsingCardIssuerID;
		internal static readonly IEntityRelation RevenueTransactionTypeEntityUsingTypeIDStatic = new TransactionRelations().RevenueTransactionTypeEntityUsingTypeID;
		internal static readonly IEntityRelation ShiftEntityUsingShiftIDStatic = new TransactionRelations().ShiftEntityUsingShiftID;

		/// <summary>CTor</summary>
		static StaticTransactionRelations()
		{
		}
	}
}
