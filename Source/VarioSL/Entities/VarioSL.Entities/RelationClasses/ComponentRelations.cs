﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Component. </summary>
	public partial class ComponentRelations
	{
		/// <summary>CTor</summary>
		public ComponentRelations()
		{
		}

		/// <summary>Gets all relations of the ComponentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ComponentClearingEntityUsingComponentID);
			toReturn.Add(this.ComponentFillingEntityUsingComponentID);
			toReturn.Add(this.PaymentJournalToComponentEntityUsingComponentID);
			toReturn.Add(this.SaleToComponentEntityUsingComponentID);
			toReturn.Add(this.AutomatEntityUsingAutomatID);
			toReturn.Add(this.ComponentStateEntityUsingStatusID);
			toReturn.Add(this.ComponentTypeEntityUsingComponentTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and ComponentClearingEntity over the 1:n relation they have, using the relation between the fields:
		/// Component.ComponentID - ComponentClearing.ComponentID
		/// </summary>
		public virtual IEntityRelation ComponentClearingEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentClearings" , true);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, ComponentClearingFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentClearingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and ComponentFillingEntity over the 1:n relation they have, using the relation between the fields:
		/// Component.ComponentID - ComponentFilling.ComponentID
		/// </summary>
		public virtual IEntityRelation ComponentFillingEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentFillings" , true);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, ComponentFillingFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and PaymentJournalToComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// Component.ComponentID - PaymentJournalToComponent.ComponentID
		/// </summary>
		public virtual IEntityRelation PaymentJournalToComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournalToComponent" , true);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, PaymentJournalToComponentFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalToComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and SaleToComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// Component.ComponentID - SaleToComponent.ComponentID
		/// </summary>
		public virtual IEntityRelation SaleToComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SaleToComponents" , true);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, SaleToComponentFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleToComponentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and AutomatEntity over the m:1 relation they have, using the relation between the fields:
		/// Component.AutomatID - Automat.AutomatID
		/// </summary>
		public virtual IEntityRelation AutomatEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Automat", false);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, ComponentFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and ComponentStateEntity over the m:1 relation they have, using the relation between the fields:
		/// Component.StatusID - ComponentState.ComponentStateID
		/// </summary>
		public virtual IEntityRelation ComponentStateEntityUsingStatusID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ComponentState", false);
				relation.AddEntityFieldPair(ComponentStateFields.ComponentStateID, ComponentFields.StatusID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentStateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentEntity and ComponentTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Component.ComponentTypeID - ComponentType.ComponentTypeID
		/// </summary>
		public virtual IEntityRelation ComponentTypeEntityUsingComponentTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ComponentType", false);
				relation.AddEntityFieldPair(ComponentTypeFields.ComponentTypeID, ComponentFields.ComponentTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticComponentRelations
	{
		internal static readonly IEntityRelation ComponentClearingEntityUsingComponentIDStatic = new ComponentRelations().ComponentClearingEntityUsingComponentID;
		internal static readonly IEntityRelation ComponentFillingEntityUsingComponentIDStatic = new ComponentRelations().ComponentFillingEntityUsingComponentID;
		internal static readonly IEntityRelation PaymentJournalToComponentEntityUsingComponentIDStatic = new ComponentRelations().PaymentJournalToComponentEntityUsingComponentID;
		internal static readonly IEntityRelation SaleToComponentEntityUsingComponentIDStatic = new ComponentRelations().SaleToComponentEntityUsingComponentID;
		internal static readonly IEntityRelation AutomatEntityUsingAutomatIDStatic = new ComponentRelations().AutomatEntityUsingAutomatID;
		internal static readonly IEntityRelation ComponentStateEntityUsingStatusIDStatic = new ComponentRelations().ComponentStateEntityUsingStatusID;
		internal static readonly IEntityRelation ComponentTypeEntityUsingComponentTypeIDStatic = new ComponentRelations().ComponentTypeEntityUsingComponentTypeID;

		/// <summary>CTor</summary>
		static StaticComponentRelations()
		{
		}
	}
}
