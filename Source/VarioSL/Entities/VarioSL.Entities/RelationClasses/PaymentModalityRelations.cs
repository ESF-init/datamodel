﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentModality. </summary>
	public partial class PaymentModalityRelations
	{
		/// <summary>CTor</summary>
		public PaymentModalityRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentModalityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractEntityUsingPaymentModalityID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between PaymentModalityEntity and ContractEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentModality.PaymentModalityID - Contract.PaymentModalityID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingPaymentModalityID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Contract", true);


				relation.AddEntityFieldPair(PaymentModalityFields.PaymentModalityID, ContractFields.PaymentModalityID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentModalityEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentModalityRelations
	{
		internal static readonly IEntityRelation ContractEntityUsingPaymentModalityIDStatic = new PaymentModalityRelations().ContractEntityUsingPaymentModalityID;

		/// <summary>CTor</summary>
		static StaticPaymentModalityRelations()
		{
		}
	}
}
