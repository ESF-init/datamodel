﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: MobilityProvider. </summary>
	public partial class MobilityProviderRelations
	{
		/// <summary>CTor</summary>
		public MobilityProviderRelations()
		{
		}

		/// <summary>Gets all relations of the MobilityProviderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeToMobilityProviderEntityUsingMobilityProviderID);
			toReturn.Add(this.BookingItemEntityUsingMobilityProviderID);
			toReturn.Add(this.ContractToProviderEntityUsingMobilityProviderID);
			toReturn.Add(this.AddressEntityUsingAddressID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between MobilityProviderEntity and AttributeToMobilityProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// MobilityProvider.MobilityProviderID - AttributeToMobilityProvider.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation AttributeToMobilityProviderEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AttributeToMobilityProviders" , true);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, AttributeToMobilityProviderFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeToMobilityProviderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MobilityProviderEntity and BookingItemEntity over the 1:n relation they have, using the relation between the fields:
		/// MobilityProvider.MobilityProviderID - BookingItem.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation BookingItemEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BookingItems" , true);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, BookingItemFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between MobilityProviderEntity and ContractToProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// MobilityProvider.MobilityProviderID - ContractToProvider.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation ContractToProviderEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractToProviders" , true);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, ContractToProviderFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToProviderEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between MobilityProviderEntity and AddressEntity over the m:1 relation they have, using the relation between the fields:
		/// MobilityProvider.AddressID - Address.AddressID
		/// </summary>
		public virtual IEntityRelation AddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Address", false);
				relation.AddEntityFieldPair(AddressFields.AddressID, MobilityProviderFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticMobilityProviderRelations
	{
		internal static readonly IEntityRelation AttributeToMobilityProviderEntityUsingMobilityProviderIDStatic = new MobilityProviderRelations().AttributeToMobilityProviderEntityUsingMobilityProviderID;
		internal static readonly IEntityRelation BookingItemEntityUsingMobilityProviderIDStatic = new MobilityProviderRelations().BookingItemEntityUsingMobilityProviderID;
		internal static readonly IEntityRelation ContractToProviderEntityUsingMobilityProviderIDStatic = new MobilityProviderRelations().ContractToProviderEntityUsingMobilityProviderID;
		internal static readonly IEntityRelation AddressEntityUsingAddressIDStatic = new MobilityProviderRelations().AddressEntityUsingAddressID;

		/// <summary>CTor</summary>
		static StaticMobilityProviderRelations()
		{
		}
	}
}
