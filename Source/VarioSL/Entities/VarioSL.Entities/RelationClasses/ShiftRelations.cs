﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Shift. </summary>
	public partial class ShiftRelations
	{
		/// <summary>CTor</summary>
		public ShiftRelations()
		{
		}

		/// <summary>Gets all relations of the ShiftEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TransactionEntityUsingShiftID);
			toReturn.Add(this.ShiftStateEntityUsingShiftStateID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ShiftEntity and TransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// Shift.ShiftID - Transaction.ShiftID
		/// </summary>
		public virtual IEntityRelation TransactionEntityUsingShiftID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Transactions" , true);
				relation.AddEntityFieldPair(ShiftFields.ShiftID, TransactionFields.ShiftID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between ShiftEntity and ShiftStateEntity over the m:1 relation they have, using the relation between the fields:
		/// Shift.ShiftStateID - ShiftState.ShiftStateID
		/// </summary>
		public virtual IEntityRelation ShiftStateEntityUsingShiftStateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ShiftState", false);
				relation.AddEntityFieldPair(ShiftStateFields.ShiftStateID, ShiftFields.ShiftStateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftStateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ShiftEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticShiftRelations
	{
		internal static readonly IEntityRelation TransactionEntityUsingShiftIDStatic = new ShiftRelations().TransactionEntityUsingShiftID;
		internal static readonly IEntityRelation ShiftStateEntityUsingShiftStateIDStatic = new ShiftRelations().ShiftStateEntityUsingShiftStateID;

		/// <summary>CTor</summary>
		static StaticShiftRelations()
		{
		}
	}
}
