﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BookingItem. </summary>
	public partial class BookingItemRelations
	{
		/// <summary>CTor</summary>
		public BookingItemRelations()
		{
		}

		/// <summary>Gets all relations of the BookingItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BookingItemEntityUsingCancellationID);
			toReturn.Add(this.BookingItemPaymentEntityUsingBookingItemID);
			toReturn.Add(this.EmailEventResendLockEntityUsingBookingItemID);
			toReturn.Add(this.BookingEntityUsingBookingID);
			toReturn.Add(this.BookingItemEntityUsingBookingItemIDCancellationID);
			toReturn.Add(this.MobilityProviderEntityUsingMobilityProviderID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and BookingItemEntity over the 1:n relation they have, using the relation between the fields:
		/// BookingItem.BookingItemID - BookingItem.CancellationID
		/// </summary>
		public virtual IEntityRelation BookingItemEntityUsingCancellationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BookingItems" , true);
				relation.AddEntityFieldPair(BookingItemFields.BookingItemID, BookingItemFields.CancellationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and BookingItemPaymentEntity over the 1:n relation they have, using the relation between the fields:
		/// BookingItem.BookingItemID - BookingItemPayment.BookingItemID
		/// </summary>
		public virtual IEntityRelation BookingItemPaymentEntityUsingBookingItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BookingItemPayments" , true);
				relation.AddEntityFieldPair(BookingItemFields.BookingItemID, BookingItemPaymentFields.BookingItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemPaymentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and EmailEventResendLockEntity over the 1:n relation they have, using the relation between the fields:
		/// BookingItem.BookingItemID - EmailEventResendLock.BookingItemID
		/// </summary>
		public virtual IEntityRelation EmailEventResendLockEntityUsingBookingItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EmailEventResendLocks" , true);
				relation.AddEntityFieldPair(BookingItemFields.BookingItemID, EmailEventResendLockFields.BookingItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EmailEventResendLockEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and BookingEntity over the m:1 relation they have, using the relation between the fields:
		/// BookingItem.BookingID - Booking.BookingID
		/// </summary>
		public virtual IEntityRelation BookingEntityUsingBookingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Booking", false);
				relation.AddEntityFieldPair(BookingFields.BookingID, BookingItemFields.BookingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and BookingItemEntity over the m:1 relation they have, using the relation between the fields:
		/// BookingItem.CancellationID - BookingItem.BookingItemID
		/// </summary>
		public virtual IEntityRelation BookingItemEntityUsingBookingItemIDCancellationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "BookingItem", false);
				relation.AddEntityFieldPair(BookingItemFields.BookingItemID, BookingItemFields.CancellationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between BookingItemEntity and MobilityProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// BookingItem.MobilityProviderID - MobilityProvider.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation MobilityProviderEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MobilityProvider", false);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, BookingItemFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BookingItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBookingItemRelations
	{
		internal static readonly IEntityRelation BookingItemEntityUsingCancellationIDStatic = new BookingItemRelations().BookingItemEntityUsingCancellationID;
		internal static readonly IEntityRelation BookingItemPaymentEntityUsingBookingItemIDStatic = new BookingItemRelations().BookingItemPaymentEntityUsingBookingItemID;
		internal static readonly IEntityRelation EmailEventResendLockEntityUsingBookingItemIDStatic = new BookingItemRelations().EmailEventResendLockEntityUsingBookingItemID;
		internal static readonly IEntityRelation BookingEntityUsingBookingIDStatic = new BookingItemRelations().BookingEntityUsingBookingID;
		internal static readonly IEntityRelation BookingItemEntityUsingBookingItemIDCancellationIDStatic = new BookingItemRelations().BookingItemEntityUsingBookingItemIDCancellationID;
		internal static readonly IEntityRelation MobilityProviderEntityUsingMobilityProviderIDStatic = new BookingItemRelations().MobilityProviderEntityUsingMobilityProviderID;

		/// <summary>CTor</summary>
		static StaticBookingItemRelations()
		{
		}
	}
}
