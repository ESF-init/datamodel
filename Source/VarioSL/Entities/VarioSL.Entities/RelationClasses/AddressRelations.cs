﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Address. </summary>
	public partial class AddressRelations
	{
		/// <summary>CTor</summary>
		public AddressRelations()
		{
		}

		/// <summary>Gets all relations of the AddressEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AddressBookEntryEntityUsingAddressID);
			toReturn.Add(this.ContractAddressEntityUsingAddressID);
			toReturn.Add(this.MobilityProviderEntityUsingAddressID);
			toReturn.Add(this.OrderEntityUsingInvoiceAddressID);
			toReturn.Add(this.OrderEntityUsingShippingAddressID);
			toReturn.Add(this.OrganizationAddressEntityUsingAddressID);
			toReturn.Add(this.PersonAddressEntityUsingAddressID);
			toReturn.Add(this.ProductRelationEntityUsingFromAddressID);
			toReturn.Add(this.ProductRelationEntityUsingToAddressID);
			toReturn.Add(this.StorageLocationEntityUsingAddressID);
			toReturn.Add(this.CardHolderEntityUsingAlternativeAdressID);
			toReturn.Add(this.PersonEntityUsingAddressID);
			toReturn.Add(this.CoordinateEntityUsingCoordinateID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and AddressBookEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - AddressBookEntry.AddressID
		/// </summary>
		public virtual IEntityRelation AddressBookEntryEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AddressBookEntries" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, AddressBookEntryFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressBookEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and ContractAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - ContractAddress.AddressID
		/// </summary>
		public virtual IEntityRelation ContractAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractAddresses" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, ContractAddressFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and MobilityProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - MobilityProvider.AddressID
		/// </summary>
		public virtual IEntityRelation MobilityProviderEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "MobilityProviders" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, MobilityProviderFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - Order.InvoiceAddressID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingInvoiceAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "InvoiceAddressOrders" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, OrderFields.InvoiceAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - Order.ShippingAddressID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingShippingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ShippingAddressOrders" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, OrderFields.ShippingAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and OrganizationAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - OrganizationAddress.AddressID
		/// </summary>
		public virtual IEntityRelation OrganizationAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrganizationAddresses" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, OrganizationAddressFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and PersonAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - PersonAddress.AddressID
		/// </summary>
		public virtual IEntityRelation PersonAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PersonAddresses" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, PersonAddressFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and ProductRelationEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - ProductRelation.FromAddressID
		/// </summary>
		public virtual IEntityRelation ProductRelationEntityUsingFromAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductRelationsFrom" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, ProductRelationFields.FromAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and ProductRelationEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - ProductRelation.ToAddressID
		/// </summary>
		public virtual IEntityRelation ProductRelationEntityUsingToAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductRelationsTo" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, ProductRelationFields.ToAddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductRelationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and StorageLocationEntity over the 1:n relation they have, using the relation between the fields:
		/// Address.AddressID - StorageLocation.AddressID
		/// </summary>
		public virtual IEntityRelation StorageLocationEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "StorageLocations" , true);
				relation.AddEntityFieldPair(AddressFields.AddressID, StorageLocationFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StorageLocationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and CardHolderEntity over the 1:1 relation they have, using the relation between the fields:
		/// Address.AddressID - CardHolder.AlternativeAdressID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingAlternativeAdressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "CardHolder", true);


				relation.AddEntityFieldPair(AddressFields.AddressID, CardHolderFields.AlternativeAdressID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and PersonEntity over the 1:1 relation they have, using the relation between the fields:
		/// Address.AddressID - Person.AddressID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Person", true);


				relation.AddEntityFieldPair(AddressFields.AddressID, PersonFields.AddressID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between AddressEntity and CoordinateEntity over the m:1 relation they have, using the relation between the fields:
		/// Address.CoordinateID - Coordinate.CoordinateID
		/// </summary>
		public virtual IEntityRelation CoordinateEntityUsingCoordinateID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Coordinate", false);
				relation.AddEntityFieldPair(CoordinateFields.CoordinateID, AddressFields.CoordinateID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CoordinateEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AddressEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAddressRelations
	{
		internal static readonly IEntityRelation AddressBookEntryEntityUsingAddressIDStatic = new AddressRelations().AddressBookEntryEntityUsingAddressID;
		internal static readonly IEntityRelation ContractAddressEntityUsingAddressIDStatic = new AddressRelations().ContractAddressEntityUsingAddressID;
		internal static readonly IEntityRelation MobilityProviderEntityUsingAddressIDStatic = new AddressRelations().MobilityProviderEntityUsingAddressID;
		internal static readonly IEntityRelation OrderEntityUsingInvoiceAddressIDStatic = new AddressRelations().OrderEntityUsingInvoiceAddressID;
		internal static readonly IEntityRelation OrderEntityUsingShippingAddressIDStatic = new AddressRelations().OrderEntityUsingShippingAddressID;
		internal static readonly IEntityRelation OrganizationAddressEntityUsingAddressIDStatic = new AddressRelations().OrganizationAddressEntityUsingAddressID;
		internal static readonly IEntityRelation PersonAddressEntityUsingAddressIDStatic = new AddressRelations().PersonAddressEntityUsingAddressID;
		internal static readonly IEntityRelation ProductRelationEntityUsingFromAddressIDStatic = new AddressRelations().ProductRelationEntityUsingFromAddressID;
		internal static readonly IEntityRelation ProductRelationEntityUsingToAddressIDStatic = new AddressRelations().ProductRelationEntityUsingToAddressID;
		internal static readonly IEntityRelation StorageLocationEntityUsingAddressIDStatic = new AddressRelations().StorageLocationEntityUsingAddressID;
		internal static readonly IEntityRelation CardHolderEntityUsingAlternativeAdressIDStatic = new AddressRelations().CardHolderEntityUsingAlternativeAdressID;
		internal static readonly IEntityRelation PersonEntityUsingAddressIDStatic = new AddressRelations().PersonEntityUsingAddressID;
		internal static readonly IEntityRelation CoordinateEntityUsingCoordinateIDStatic = new AddressRelations().CoordinateEntityUsingCoordinateID;

		/// <summary>CTor</summary>
		static StaticAddressRelations()
		{
		}
	}
}
