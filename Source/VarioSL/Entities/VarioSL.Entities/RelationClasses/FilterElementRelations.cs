﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FilterElement. </summary>
	public partial class FilterElementRelations
	{
		/// <summary>CTor</summary>
		public FilterElementRelations()
		{
		}

		/// <summary>Gets all relations of the FilterElementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FilterListElementEntityUsingFilterElementID);
			toReturn.Add(this.ConfigurationEntityUsingValueListDataSourceID);
			toReturn.Add(this.ConfigurationDefinitionEntityUsingValueListDataSourceID);
			toReturn.Add(this.FilterTypeEntityUsingFilterTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between FilterElementEntity and FilterListElementEntity over the 1:n relation they have, using the relation between the fields:
		/// FilterElement.FilterElementID - FilterListElement.FilterElementID
		/// </summary>
		public virtual IEntityRelation FilterListElementEntityUsingFilterElementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FilterListElements" , true);
				relation.AddEntityFieldPair(FilterElementFields.FilterElementID, FilterListElementFields.FilterElementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterListElementEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between FilterElementEntity and ConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterElement.ValueListDataSourceID - Configuration.ConfigurationID
		/// </summary>
		public virtual IEntityRelation ConfigurationEntityUsingValueListDataSourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Configuration", false);
				relation.AddEntityFieldPair(ConfigurationFields.ConfigurationID, FilterElementFields.ValueListDataSourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterElementEntity and ConfigurationDefinitionEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterElement.ValueListDataSourceID - ConfigurationDefinition.ConfigurationDefinitionID
		/// </summary>
		public virtual IEntityRelation ConfigurationDefinitionEntityUsingValueListDataSourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ConfigurationDefinition", false);
				relation.AddEntityFieldPair(ConfigurationDefinitionFields.ConfigurationDefinitionID, FilterElementFields.ValueListDataSourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ConfigurationDefinitionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FilterElementEntity and FilterTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// FilterElement.FilterTypeID - FilterType.FilterTypeID
		/// </summary>
		public virtual IEntityRelation FilterTypeEntityUsingFilterTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FilterType", false);
				relation.AddEntityFieldPair(FilterTypeFields.FilterTypeID, FilterElementFields.FilterTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FilterElementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFilterElementRelations
	{
		internal static readonly IEntityRelation FilterListElementEntityUsingFilterElementIDStatic = new FilterElementRelations().FilterListElementEntityUsingFilterElementID;
		internal static readonly IEntityRelation ConfigurationEntityUsingValueListDataSourceIDStatic = new FilterElementRelations().ConfigurationEntityUsingValueListDataSourceID;
		internal static readonly IEntityRelation ConfigurationDefinitionEntityUsingValueListDataSourceIDStatic = new FilterElementRelations().ConfigurationDefinitionEntityUsingValueListDataSourceID;
		internal static readonly IEntityRelation FilterTypeEntityUsingFilterTypeIDStatic = new FilterElementRelations().FilterTypeEntityUsingFilterTypeID;

		/// <summary>CTor</summary>
		static StaticFilterElementRelations()
		{
		}
	}
}
