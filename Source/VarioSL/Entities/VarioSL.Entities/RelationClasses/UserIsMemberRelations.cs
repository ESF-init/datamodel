﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: UserIsMember. </summary>
	public partial class UserIsMemberRelations
	{
		/// <summary>CTor</summary>
		public UserIsMemberRelations()
		{
		}

		/// <summary>Gets all relations of the UserIsMemberEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserGroupEntityUsingUserGroupID);
			toReturn.Add(this.UserListEntityUsingUserID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between UserIsMemberEntity and UserGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// UserIsMember.UserGroupID - UserGroup.UserGroupID
		/// </summary>
		public virtual IEntityRelation UserGroupEntityUsingUserGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserGroup", false);
				relation.AddEntityFieldPair(UserGroupFields.UserGroupID, UserIsMemberFields.UserGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserIsMemberEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between UserIsMemberEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// UserIsMember.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, UserIsMemberFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserIsMemberEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticUserIsMemberRelations
	{
		internal static readonly IEntityRelation UserGroupEntityUsingUserGroupIDStatic = new UserIsMemberRelations().UserGroupEntityUsingUserGroupID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new UserIsMemberRelations().UserListEntityUsingUserID;

		/// <summary>CTor</summary>
		static StaticUserIsMemberRelations()
		{
		}
	}
}
