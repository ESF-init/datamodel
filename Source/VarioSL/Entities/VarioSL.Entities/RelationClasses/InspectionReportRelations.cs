﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: InspectionReport. </summary>
	public partial class InspectionReportRelations
	{
		/// <summary>CTor</summary>
		public InspectionReportRelations()
		{
		}

		/// <summary>Gets all relations of the InspectionReportEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareEvasionIncidentEntityUsingInspectionReportID);
			toReturn.Add(this.InspectionCriterionEntityUsingInspectionReportID);
			toReturn.Add(this.TransactionToInspectionEntityUsingInspectionReportID);
			toReturn.Add(this.FareEvasionInspectionEntityUsingFareEvasionInspectionID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between InspectionReportEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// InspectionReport.InspectionReportID - FareEvasionIncident.InspectionReportID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingInspectionReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(InspectionReportFields.InspectionReportID, FareEvasionIncidentFields.InspectionReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InspectionReportEntity and InspectionCriterionEntity over the 1:n relation they have, using the relation between the fields:
		/// InspectionReport.InspectionReportID - InspectionCriterion.InspectionReportID
		/// </summary>
		public virtual IEntityRelation InspectionCriterionEntityUsingInspectionReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SlInspectioncriterions" , true);
				relation.AddEntityFieldPair(InspectionReportFields.InspectionReportID, InspectionCriterionFields.InspectionReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionCriterionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between InspectionReportEntity and TransactionToInspectionEntity over the 1:n relation they have, using the relation between the fields:
		/// InspectionReport.InspectionReportID - TransactionToInspection.InspectionReportID
		/// </summary>
		public virtual IEntityRelation TransactionToInspectionEntityUsingInspectionReportID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionToInspections" , true);
				relation.AddEntityFieldPair(InspectionReportFields.InspectionReportID, TransactionToInspectionFields.InspectionReportID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionToInspectionEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between InspectionReportEntity and FareEvasionInspectionEntity over the m:1 relation they have, using the relation between the fields:
		/// InspectionReport.FareEvasionInspectionID - FareEvasionInspection.FareEvasionInspectionID
		/// </summary>
		public virtual IEntityRelation FareEvasionInspectionEntityUsingFareEvasionInspectionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareEvasionInspection", false);
				relation.AddEntityFieldPair(FareEvasionInspectionFields.FareEvasionInspectionID, InspectionReportFields.FareEvasionInspectionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionInspectionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InspectionReportEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticInspectionReportRelations
	{
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingInspectionReportIDStatic = new InspectionReportRelations().FareEvasionIncidentEntityUsingInspectionReportID;
		internal static readonly IEntityRelation InspectionCriterionEntityUsingInspectionReportIDStatic = new InspectionReportRelations().InspectionCriterionEntityUsingInspectionReportID;
		internal static readonly IEntityRelation TransactionToInspectionEntityUsingInspectionReportIDStatic = new InspectionReportRelations().TransactionToInspectionEntityUsingInspectionReportID;
		internal static readonly IEntityRelation FareEvasionInspectionEntityUsingFareEvasionInspectionIDStatic = new InspectionReportRelations().FareEvasionInspectionEntityUsingFareEvasionInspectionID;

		/// <summary>CTor</summary>
		static StaticInspectionReportRelations()
		{
		}
	}
}
