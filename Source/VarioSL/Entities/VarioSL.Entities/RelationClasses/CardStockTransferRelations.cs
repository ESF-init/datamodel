﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardStockTransfer. </summary>
	public partial class CardStockTransferRelations
	{
		/// <summary>CTor</summary>
		public CardStockTransferRelations()
		{
		}

		/// <summary>Gets all relations of the CardStockTransferEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.StockTransferEntityUsingStockTransferID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CardStockTransferEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// CardStockTransfer.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, CardStockTransferFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardStockTransferEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CardStockTransferEntity and StockTransferEntity over the m:1 relation they have, using the relation between the fields:
		/// CardStockTransfer.StockTransferID - StockTransfer.StockTransferID
		/// </summary>
		public virtual IEntityRelation StockTransferEntityUsingStockTransferID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "StockTransfer", false);
				relation.AddEntityFieldPair(StockTransferFields.StockTransferID, CardStockTransferFields.StockTransferID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StockTransferEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardStockTransferEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardStockTransferRelations
	{
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new CardStockTransferRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation StockTransferEntityUsingStockTransferIDStatic = new CardStockTransferRelations().StockTransferEntityUsingStockTransferID;

		/// <summary>CTor</summary>
		static StaticCardStockTransferRelations()
		{
		}
	}
}
