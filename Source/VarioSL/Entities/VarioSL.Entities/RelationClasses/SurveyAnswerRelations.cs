﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyAnswer. </summary>
	public partial class SurveyAnswerRelations
	{
		/// <summary>CTor</summary>
		public SurveyAnswerRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyAnswerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyChoiceEntityUsingSurveyChoiceID);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionID);
			toReturn.Add(this.SurveyResponseEntityUsingSurveyResponseID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyChoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyChoiceID - SurveyChoice.SurveyChoiceID
		/// </summary>
		public virtual IEntityRelation SurveyChoiceEntityUsingSurveyChoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyChoice", false);
				relation.AddEntityFieldPair(SurveyChoiceFields.SurveyChoiceID, SurveyAnswerFields.SurveyChoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyQuestionID - SurveyQuestion.SurveyQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyQuestion", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyAnswerFields.SurveyQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyAnswerEntity and SurveyResponseEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyAnswer.SurveyResponseID - SurveyResponse.SurveyResponseID
		/// </summary>
		public virtual IEntityRelation SurveyResponseEntityUsingSurveyResponseID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyResponse", false);
				relation.AddEntityFieldPair(SurveyResponseFields.SurveyResponseID, SurveyAnswerFields.SurveyResponseID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyResponseEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyAnswerRelations
	{
		internal static readonly IEntityRelation SurveyChoiceEntityUsingSurveyChoiceIDStatic = new SurveyAnswerRelations().SurveyChoiceEntityUsingSurveyChoiceID;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIDStatic = new SurveyAnswerRelations().SurveyQuestionEntityUsingSurveyQuestionID;
		internal static readonly IEntityRelation SurveyResponseEntityUsingSurveyResponseIDStatic = new SurveyAnswerRelations().SurveyResponseEntityUsingSurveyResponseID;

		/// <summary>CTor</summary>
		static StaticSurveyAnswerRelations()
		{
		}
	}
}
