﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Workstation. </summary>
	public partial class WorkstationRelations
	{
		/// <summary>CTor</summary>
		public WorkstationRelations()
		{
		}

		/// <summary>Gets all relations of the WorkstationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.UserToWorkstationEntityUsingWorkstationID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between WorkstationEntity and UserToWorkstationEntity over the 1:n relation they have, using the relation between the fields:
		/// Workstation.WorkstationID - UserToWorkstation.WorkstationID
		/// </summary>
		public virtual IEntityRelation UserToWorkstationEntityUsingWorkstationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UsersToWorkstations" , true);
				relation.AddEntityFieldPair(WorkstationFields.WorkstationID, UserToWorkstationFields.WorkstationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkstationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserToWorkstationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between WorkstationEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Workstation.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, WorkstationFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkstationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWorkstationRelations
	{
		internal static readonly IEntityRelation UserToWorkstationEntityUsingWorkstationIDStatic = new WorkstationRelations().UserToWorkstationEntityUsingWorkstationID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new WorkstationRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticWorkstationRelations()
		{
		}
	}
}
