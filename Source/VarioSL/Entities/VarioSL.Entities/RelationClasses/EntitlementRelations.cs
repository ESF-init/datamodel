﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Entitlement. </summary>
	public partial class EntitlementRelations
	{
		/// <summary>CTor</summary>
		public EntitlementRelations()
		{
		}

		/// <summary>Gets all relations of the EntitlementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntitlementToProductEntityUsingEntitlementID);
			toReturn.Add(this.TicketAssignmentEntityUsingEntitlementID);
			toReturn.Add(this.EntitlementTypeEntityUsingEntitlementTypeID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntitlementEntity and EntitlementToProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Entitlement.EntitlementID - EntitlementToProduct.EntitlementID
		/// </summary>
		public virtual IEntityRelation EntitlementToProductEntityUsingEntitlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "EntitlementToProducts" , true);
				relation.AddEntityFieldPair(EntitlementFields.EntitlementID, EntitlementToProductFields.EntitlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementToProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between EntitlementEntity and TicketAssignmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Entitlement.EntitlementID - TicketAssignment.EntitlementID
		/// </summary>
		public virtual IEntityRelation TicketAssignmentEntityUsingEntitlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketAssignments" , true);
				relation.AddEntityFieldPair(EntitlementFields.EntitlementID, TicketAssignmentFields.EntitlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between EntitlementEntity and EntitlementTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// Entitlement.EntitlementTypeID - EntitlementType.EntitlementTypeID
		/// </summary>
		public virtual IEntityRelation EntitlementTypeEntityUsingEntitlementTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntitlementType", false);
				relation.AddEntityFieldPair(EntitlementTypeFields.EntitlementTypeID, EntitlementFields.EntitlementTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between EntitlementEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// Entitlement.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, EntitlementFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntitlementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntitlementRelations
	{
		internal static readonly IEntityRelation EntitlementToProductEntityUsingEntitlementIDStatic = new EntitlementRelations().EntitlementToProductEntityUsingEntitlementID;
		internal static readonly IEntityRelation TicketAssignmentEntityUsingEntitlementIDStatic = new EntitlementRelations().TicketAssignmentEntityUsingEntitlementID;
		internal static readonly IEntityRelation EntitlementTypeEntityUsingEntitlementTypeIDStatic = new EntitlementRelations().EntitlementTypeEntityUsingEntitlementTypeID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new EntitlementRelations().PersonEntityUsingPersonID;

		/// <summary>CTor</summary>
		static StaticEntitlementRelations()
		{
		}
	}
}
