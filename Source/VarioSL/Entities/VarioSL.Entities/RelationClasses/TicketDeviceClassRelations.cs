﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketDeviceClass. </summary>
	public partial class TicketDeviceClassRelations
	{
		/// <summary>CTor</summary>
		public TicketDeviceClassRelations()
		{
		}

		/// <summary>Gets all relations of the TicketDeviceClassEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID);
			toReturn.Add(this.TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID);
			toReturn.Add(this.CalendarEntityUsingCalendarID);
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.EticketEntityUsingEtickID);
			toReturn.Add(this.FareMatrixEntityUsingMatrixID);
			toReturn.Add(this.LayoutEntityUsingCancellationLayoutID);
			toReturn.Add(this.LayoutEntityUsingGroupLayoutID);
			toReturn.Add(this.LayoutEntityUsingPrintLayoutID);
			toReturn.Add(this.LayoutEntityUsingReceiptLayoutID);
			toReturn.Add(this.RulePeriodEntityUsingRulePeriodID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.TicketSelectionModeEntityUsingSelectionMode);
			toReturn.Add(this.UserKeyEntityUsingKey1);
			toReturn.Add(this.UserKeyEntityUsingKey2);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and TicketDeviceClassOutputDeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketDeviceClass.TicketDeviceClassID - TicketDeviceClassOutputDevice.TicketDeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClassOutputDevice" , true);
				relation.AddEntityFieldPair(TicketDeviceClassFields.TicketDeviceClassID, TicketDeviceClassOutputDeviceFields.TicketDeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassOutputDeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and TicketDeviceClassPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketDeviceClass.TicketDeviceClassID - TicketDeviceClassPaymentMethod.TicketDeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClassPaymentMethods" , true);
				relation.AddEntityFieldPair(TicketDeviceClassFields.TicketDeviceClassID, TicketDeviceClassPaymentMethodFields.TicketDeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassPaymentMethodEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and CalendarEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.CalendarID - Calendar.CalendarID
		/// </summary>
		public virtual IEntityRelation CalendarEntityUsingCalendarID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Calendar", false);
				relation.AddEntityFieldPair(CalendarFields.CalendarID, TicketDeviceClassFields.CalendarID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CalendarEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TicketDeviceClassFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and EticketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.EtickID - Eticket.EtickID
		/// </summary>
		public virtual IEntityRelation EticketEntityUsingEtickID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Eticket", false);
				relation.AddEntityFieldPair(EticketFields.EtickID, TicketDeviceClassFields.EtickID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EticketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and FareMatrixEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.MatrixID - FareMatrix.FareMatrixID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntityUsingMatrixID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareMatrix", false);
				relation.AddEntityFieldPair(FareMatrixFields.FareMatrixID, TicketDeviceClassFields.MatrixID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.CancellationLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingCancellationLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CancellationLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.CancellationLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.GroupLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingGroupLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "GroupLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.GroupLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.PrintLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingPrintLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PrintLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.PrintLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.ReceiptLayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingReceiptLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ReceiptLayout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, TicketDeviceClassFields.ReceiptLayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and RulePeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.RulePeriodID - RulePeriod.RulePeriodID
		/// </summary>
		public virtual IEntityRelation RulePeriodEntityUsingRulePeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RulePeriod", false);
				relation.AddEntityFieldPair(RulePeriodFields.RulePeriodID, TicketDeviceClassFields.RulePeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RulePeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketDeviceClassFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and TicketSelectionModeEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.SelectionMode - TicketSelectionMode.SelectionModeNumber
		/// </summary>
		public virtual IEntityRelation TicketSelectionModeEntityUsingSelectionMode
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketSelectionMode", false);
				relation.AddEntityFieldPair(TicketSelectionModeFields.SelectionModeNumber, TicketDeviceClassFields.SelectionMode);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSelectionModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and UserKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.Key1 - UserKey.KeyID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingKey1
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserKey1", false);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketDeviceClassFields.Key1);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketDeviceClassEntity and UserKeyEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketDeviceClass.Key2 - UserKey.KeyID
		/// </summary>
		public virtual IEntityRelation UserKeyEntityUsingKey2
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserKey2", false);
				relation.AddEntityFieldPair(UserKeyFields.KeyID, TicketDeviceClassFields.Key2);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserKeyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketDeviceClassRelations
	{
		internal static readonly IEntityRelation TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassIDStatic = new TicketDeviceClassRelations().TicketDeviceClassOutputDeviceEntityUsingTicketDeviceClassID;
		internal static readonly IEntityRelation TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassIDStatic = new TicketDeviceClassRelations().TicketDeviceClassPaymentMethodEntityUsingTicketDeviceClassID;
		internal static readonly IEntityRelation CalendarEntityUsingCalendarIDStatic = new TicketDeviceClassRelations().CalendarEntityUsingCalendarID;
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new TicketDeviceClassRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation EticketEntityUsingEtickIDStatic = new TicketDeviceClassRelations().EticketEntityUsingEtickID;
		internal static readonly IEntityRelation FareMatrixEntityUsingMatrixIDStatic = new TicketDeviceClassRelations().FareMatrixEntityUsingMatrixID;
		internal static readonly IEntityRelation LayoutEntityUsingCancellationLayoutIDStatic = new TicketDeviceClassRelations().LayoutEntityUsingCancellationLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingGroupLayoutIDStatic = new TicketDeviceClassRelations().LayoutEntityUsingGroupLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingPrintLayoutIDStatic = new TicketDeviceClassRelations().LayoutEntityUsingPrintLayoutID;
		internal static readonly IEntityRelation LayoutEntityUsingReceiptLayoutIDStatic = new TicketDeviceClassRelations().LayoutEntityUsingReceiptLayoutID;
		internal static readonly IEntityRelation RulePeriodEntityUsingRulePeriodIDStatic = new TicketDeviceClassRelations().RulePeriodEntityUsingRulePeriodID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketDeviceClassRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketSelectionModeEntityUsingSelectionModeStatic = new TicketDeviceClassRelations().TicketSelectionModeEntityUsingSelectionMode;
		internal static readonly IEntityRelation UserKeyEntityUsingKey1Static = new TicketDeviceClassRelations().UserKeyEntityUsingKey1;
		internal static readonly IEntityRelation UserKeyEntityUsingKey2Static = new TicketDeviceClassRelations().UserKeyEntityUsingKey2;

		/// <summary>CTor</summary>
		static StaticTicketDeviceClassRelations()
		{
		}
	}
}
