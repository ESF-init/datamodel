﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: GuiDef. </summary>
	public partial class GuiDefRelations
	{
		/// <summary>CTor</summary>
		public GuiDefRelations()
		{
		}

		/// <summary>Gets all relations of the GuiDefEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.PanelEntityUsingPanelID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between GuiDefEntity and DeviceClassEntity over the m:1 relation they have, using the relation between the fields:
		/// GuiDef.DeviceClassID - DeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceClass", false);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, GuiDefFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GuiDefEntity and PanelEntity over the m:1 relation they have, using the relation between the fields:
		/// GuiDef.PanelID - Panel.PanelID
		/// </summary>
		public virtual IEntityRelation PanelEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Panel", false);
				relation.AddEntityFieldPair(PanelFields.PanelID, GuiDefFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GuiDefEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// GuiDef.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, GuiDefFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGuiDefRelations
	{
		internal static readonly IEntityRelation DeviceClassEntityUsingDeviceClassIDStatic = new GuiDefRelations().DeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation PanelEntityUsingPanelIDStatic = new GuiDefRelations().PanelEntityUsingPanelID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new GuiDefRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticGuiDefRelations()
		{
		}
	}
}
