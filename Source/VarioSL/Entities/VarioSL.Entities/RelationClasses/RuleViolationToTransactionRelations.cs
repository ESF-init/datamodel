﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RuleViolationToTransaction. </summary>
	public partial class RuleViolationToTransactionRelations
	{
		/// <summary>CTor</summary>
		public RuleViolationToTransactionRelations()
		{
		}

		/// <summary>Gets all relations of the RuleViolationToTransactionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TransactionEntityUsingTransactionID);
			toReturn.Add(this.PaymentJournalEntityUsingPaymentJournalID);
			toReturn.Add(this.RuleViolationEntityUsingRuleViolationID);
			toReturn.Add(this.TransactionJournalEntityUsingTransactionJournalID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RuleViolationToTransactionEntity and TransactionEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolationToTransaction.TransactionID - Transaction.TransactionID
		/// </summary>
		public virtual IEntityRelation TransactionEntityUsingTransactionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueTransaction", false);
				relation.AddEntityFieldPair(TransactionFields.TransactionID, RuleViolationToTransactionFields.TransactionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleViolationToTransactionEntity and PaymentJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolationToTransaction.PaymentJournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentJournal", false);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, RuleViolationToTransactionFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleViolationToTransactionEntity and RuleViolationEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolationToTransaction.RuleViolationID - RuleViolation.RuleViolationID
		/// </summary>
		public virtual IEntityRelation RuleViolationEntityUsingRuleViolationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RuleViolation", false);
				relation.AddEntityFieldPair(RuleViolationFields.RuleViolationID, RuleViolationToTransactionFields.RuleViolationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RuleViolationToTransactionEntity and TransactionJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// RuleViolationToTransaction.TransactionJournalID - TransactionJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransactionJournal", false);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RuleViolationToTransactionFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRuleViolationToTransactionRelations
	{
		internal static readonly IEntityRelation TransactionEntityUsingTransactionIDStatic = new RuleViolationToTransactionRelations().TransactionEntityUsingTransactionID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentJournalIDStatic = new RuleViolationToTransactionRelations().PaymentJournalEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation RuleViolationEntityUsingRuleViolationIDStatic = new RuleViolationToTransactionRelations().RuleViolationEntityUsingRuleViolationID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransactionJournalIDStatic = new RuleViolationToTransactionRelations().TransactionJournalEntityUsingTransactionJournalID;

		/// <summary>CTor</summary>
		static StaticRuleViolationToTransactionRelations()
		{
		}
	}
}
