﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LineGroupToLineGroup. </summary>
	public partial class LineGroupToLineGroupRelations
	{
		/// <summary>CTor</summary>
		public LineGroupToLineGroupRelations()
		{
		}

		/// <summary>Gets all relations of the LineGroupToLineGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LineGroupEntityUsingLineGroupID);
			toReturn.Add(this.LineGroupEntityUsingMasterLineGroupID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between LineGroupToLineGroupEntity and LineGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// LineGroupToLineGroup.LineGroupID - LineGroup.LineGroupID
		/// </summary>
		public virtual IEntityRelation LineGroupEntityUsingLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SubLineGroup", false);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, LineGroupToLineGroupFields.LineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupToLineGroupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LineGroupToLineGroupEntity and LineGroupEntity over the m:1 relation they have, using the relation between the fields:
		/// LineGroupToLineGroup.MasterLineGroupID - LineGroup.LineGroupID
		/// </summary>
		public virtual IEntityRelation LineGroupEntityUsingMasterLineGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MasterLineGroup", false);
				relation.AddEntityFieldPair(LineGroupFields.LineGroupID, LineGroupToLineGroupFields.MasterLineGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineGroupToLineGroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLineGroupToLineGroupRelations
	{
		internal static readonly IEntityRelation LineGroupEntityUsingLineGroupIDStatic = new LineGroupToLineGroupRelations().LineGroupEntityUsingLineGroupID;
		internal static readonly IEntityRelation LineGroupEntityUsingMasterLineGroupIDStatic = new LineGroupToLineGroupRelations().LineGroupEntityUsingMasterLineGroupID;

		/// <summary>CTor</summary>
		static StaticLineGroupToLineGroupRelations()
		{
		}
	}
}
