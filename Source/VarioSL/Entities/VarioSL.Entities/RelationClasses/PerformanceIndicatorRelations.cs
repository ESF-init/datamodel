﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PerformanceIndicator. </summary>
	public partial class PerformanceIndicatorRelations
	{
		/// <summary>CTor</summary>
		public PerformanceIndicatorRelations()
		{
		}

		/// <summary>Gets all relations of the PerformanceIndicatorEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PerformanceEntityUsingIndicatorID);
			toReturn.Add(this.PerformanceAggregationEntityUsingIndicatorID);
			toReturn.Add(this.PerformanceComponentEntityUsingComponentID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PerformanceIndicatorEntity and PerformanceEntity over the 1:n relation they have, using the relation between the fields:
		/// PerformanceIndicator.IndicatorID - Performance.IndicatorID
		/// </summary>
		public virtual IEntityRelation PerformanceEntityUsingIndicatorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Performances" , true);
				relation.AddEntityFieldPair(PerformanceIndicatorFields.IndicatorID, PerformanceFields.IndicatorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceIndicatorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PerformanceIndicatorEntity and PerformanceAggregationEntity over the 1:n relation they have, using the relation between the fields:
		/// PerformanceIndicator.IndicatorID - PerformanceAggregation.IndicatorID
		/// </summary>
		public virtual IEntityRelation PerformanceAggregationEntityUsingIndicatorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Aggregations" , true);
				relation.AddEntityFieldPair(PerformanceIndicatorFields.IndicatorID, PerformanceAggregationFields.IndicatorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceIndicatorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceAggregationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PerformanceIndicatorEntity and PerformanceComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// PerformanceIndicator.ComponentID - PerformanceComponent.ComponentID
		/// </summary>
		public virtual IEntityRelation PerformanceComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PerformanceComponent", false);
				relation.AddEntityFieldPair(PerformanceComponentFields.ComponentID, PerformanceIndicatorFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PerformanceIndicatorEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPerformanceIndicatorRelations
	{
		internal static readonly IEntityRelation PerformanceEntityUsingIndicatorIDStatic = new PerformanceIndicatorRelations().PerformanceEntityUsingIndicatorID;
		internal static readonly IEntityRelation PerformanceAggregationEntityUsingIndicatorIDStatic = new PerformanceIndicatorRelations().PerformanceAggregationEntityUsingIndicatorID;
		internal static readonly IEntityRelation PerformanceComponentEntityUsingComponentIDStatic = new PerformanceIndicatorRelations().PerformanceComponentEntityUsingComponentID;

		/// <summary>CTor</summary>
		static StaticPerformanceIndicatorRelations()
		{
		}
	}
}
