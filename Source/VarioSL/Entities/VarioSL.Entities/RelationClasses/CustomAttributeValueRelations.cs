﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomAttributeValue. </summary>
	public partial class CustomAttributeValueRelations
	{
		/// <summary>CTor</summary>
		public CustomAttributeValueRelations()
		{
		}

		/// <summary>Gets all relations of the CustomAttributeValueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomAttributeValueToPersonEntityUsingCustomAttributeValueID);
			toReturn.Add(this.CustomAttributeEntityUsingCustomAttributeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CustomAttributeValueEntity and CustomAttributeValueToPersonEntity over the 1:n relation they have, using the relation between the fields:
		/// CustomAttributeValue.CustomAttributeValueID - CustomAttributeValueToPerson.CustomAttributeValueID
		/// </summary>
		public virtual IEntityRelation CustomAttributeValueToPersonEntityUsingCustomAttributeValueID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomAttributeValueToPeople" , true);
				relation.AddEntityFieldPair(CustomAttributeValueFields.CustomAttributeValueID, CustomAttributeValueToPersonFields.CustomAttributeValueID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueToPersonEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CustomAttributeValueEntity and CustomAttributeEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomAttributeValue.CustomAttributeID - CustomAttribute.CustomAttributeID
		/// </summary>
		public virtual IEntityRelation CustomAttributeEntityUsingCustomAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomAttribute", false);
				relation.AddEntityFieldPair(CustomAttributeFields.CustomAttributeID, CustomAttributeValueFields.CustomAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomAttributeValueEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomAttributeValueRelations
	{
		internal static readonly IEntityRelation CustomAttributeValueToPersonEntityUsingCustomAttributeValueIDStatic = new CustomAttributeValueRelations().CustomAttributeValueToPersonEntityUsingCustomAttributeValueID;
		internal static readonly IEntityRelation CustomAttributeEntityUsingCustomAttributeIDStatic = new CustomAttributeValueRelations().CustomAttributeEntityUsingCustomAttributeID;

		/// <summary>CTor</summary>
		static StaticCustomAttributeValueRelations()
		{
		}
	}
}
