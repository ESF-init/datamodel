﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CryptoCryptogramArchive. </summary>
	public partial class CryptoCryptogramArchiveRelations
	{
		/// <summary>CTor</summary>
		public CryptoCryptogramArchiveRelations()
		{
		}

		/// <summary>Gets all relations of the CryptoCryptogramArchiveEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CryptoContentEntityUsingCryptogramArchiveID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CryptoCryptogramArchiveEntity and CryptoContentEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoCryptogramArchive.CryptogramArchiveID - CryptoContent.CryptogramArchiveID
		/// </summary>
		public virtual IEntityRelation CryptoContentEntityUsingCryptogramArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoContents" , true);
				relation.AddEntityFieldPair(CryptoCryptogramArchiveFields.CryptogramArchiveID, CryptoContentFields.CryptogramArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCryptogramArchiveEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CryptoCryptogramArchiveEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoCryptogramArchive.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, CryptoCryptogramArchiveFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCryptogramArchiveEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCryptoCryptogramArchiveRelations
	{
		internal static readonly IEntityRelation CryptoContentEntityUsingCryptogramArchiveIDStatic = new CryptoCryptogramArchiveRelations().CryptoContentEntityUsingCryptogramArchiveID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new CryptoCryptogramArchiveRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticCryptoCryptogramArchiveRelations()
		{
		}
	}
}
