﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Responsibility. </summary>
	public partial class ResponsibilityRelations
	{
		/// <summary>CTor</summary>
		public ResponsibilityRelations()
		{
		}

		/// <summary>Gets all relations of the ResponsibilityEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ClientEntityUsingResponsibleClientID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ResponsibilityEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Responsibility.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ResponsibilityFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ResponsibilityEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ResponsibilityEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Responsibility.ResponsibleClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingResponsibleClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ResponsibleClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ResponsibilityFields.ResponsibleClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ResponsibilityEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticResponsibilityRelations
	{
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ResponsibilityRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ClientEntityUsingResponsibleClientIDStatic = new ResponsibilityRelations().ClientEntityUsingResponsibleClientID;

		/// <summary>CTor</summary>
		static StaticResponsibilityRelations()
		{
		}
	}
}
