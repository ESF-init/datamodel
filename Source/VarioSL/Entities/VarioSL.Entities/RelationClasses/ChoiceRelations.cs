﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Choice. </summary>
	public partial class ChoiceRelations
	{
		/// <summary>CTor</summary>
		public ChoiceRelations()
		{
		}

		/// <summary>Gets all relations of the ChoiceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AttributeValueEntityUsingDistanceAttributeID);
			toReturn.Add(this.RouteNameEntityUsingRouteNameID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TicketEntityUsingTicketID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ChoiceEntity and AttributeValueEntity over the m:1 relation they have, using the relation between the fields:
		/// Choice.DistanceAttributeID - AttributeValue.AttributeValueID
		/// </summary>
		public virtual IEntityRelation AttributeValueEntityUsingDistanceAttributeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AttributeValue", false);
				relation.AddEntityFieldPair(AttributeValueFields.AttributeValueID, ChoiceFields.DistanceAttributeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AttributeValueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ChoiceEntity and RouteNameEntity over the m:1 relation they have, using the relation between the fields:
		/// Choice.RouteNameID - RouteName.RouteNameid
		/// </summary>
		public virtual IEntityRelation RouteNameEntityUsingRouteNameID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RouteName", false);
				relation.AddEntityFieldPair(RouteNameFields.RouteNameid, ChoiceFields.RouteNameID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RouteNameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ChoiceEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// Choice.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ChoiceFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ChoiceEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// Choice.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, ChoiceFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticChoiceRelations
	{
		internal static readonly IEntityRelation AttributeValueEntityUsingDistanceAttributeIDStatic = new ChoiceRelations().AttributeValueEntityUsingDistanceAttributeID;
		internal static readonly IEntityRelation RouteNameEntityUsingRouteNameIDStatic = new ChoiceRelations().RouteNameEntityUsingRouteNameID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ChoiceRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new ChoiceRelations().TicketEntityUsingTicketID;

		/// <summary>CTor</summary>
		static StaticChoiceRelations()
		{
		}
	}
}
