﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CryptoResource. </summary>
	public partial class CryptoResourceRelations
	{
		/// <summary>CTor</summary>
		public CryptoResourceRelations()
		{
		}

		/// <summary>Gets all relations of the CryptoResourceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CryptoContentEntityUsingCryptogramResourceID);
			toReturn.Add(this.CryptoContentEntityUsingSignCertificateResourceID);
			toReturn.Add(this.CryptoContentEntityUsingSubCertificateResourceID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CryptoResourceEntity and CryptoContentEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoResource.ResourceID - CryptoContent.CryptogramResourceID
		/// </summary>
		public virtual IEntityRelation CryptoContentEntityUsingCryptogramResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoContentCryptograms" , true);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.CryptogramResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CryptoResourceEntity and CryptoContentEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoResource.ResourceID - CryptoContent.SignCertificateResourceID
		/// </summary>
		public virtual IEntityRelation CryptoContentEntityUsingSignCertificateResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoContentSignCertificates" , true);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.SignCertificateResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CryptoResourceEntity and CryptoContentEntity over the 1:n relation they have, using the relation between the fields:
		/// CryptoResource.ResourceID - CryptoContent.SubCertificateResourceID
		/// </summary>
		public virtual IEntityRelation CryptoContentEntityUsingSubCertificateResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CryptoContentSubCertificates" , true);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.SubCertificateResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCryptoResourceRelations
	{
		internal static readonly IEntityRelation CryptoContentEntityUsingCryptogramResourceIDStatic = new CryptoResourceRelations().CryptoContentEntityUsingCryptogramResourceID;
		internal static readonly IEntityRelation CryptoContentEntityUsingSignCertificateResourceIDStatic = new CryptoResourceRelations().CryptoContentEntityUsingSignCertificateResourceID;
		internal static readonly IEntityRelation CryptoContentEntityUsingSubCertificateResourceIDStatic = new CryptoResourceRelations().CryptoContentEntityUsingSubCertificateResourceID;

		/// <summary>CTor</summary>
		static StaticCryptoResourceRelations()
		{
		}
	}
}
