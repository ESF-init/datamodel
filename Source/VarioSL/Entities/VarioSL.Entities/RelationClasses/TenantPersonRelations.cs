﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TenantPerson. </summary>
	public partial class TenantPersonRelations
	{
		/// <summary>CTor</summary>
		public TenantPersonRelations()
		{
		}

		/// <summary>Gets all relations of the TenantPersonEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TenantHistoryEntityUsingTenantPersonID);
			toReturn.Add(this.TenantEntityUsingTenantID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TenantPersonEntity and TenantHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// TenantPerson.TenantPersonID - TenantHistory.TenantPersonID
		/// </summary>
		public virtual IEntityRelation TenantHistoryEntityUsingTenantPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TenantHistories" , true);
				relation.AddEntityFieldPair(TenantPersonFields.TenantPersonID, TenantHistoryFields.TenantPersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantPersonEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TenantPersonEntity and TenantEntity over the m:1 relation they have, using the relation between the fields:
		/// TenantPerson.TenantID - Tenant.TenantID
		/// </summary>
		public virtual IEntityRelation TenantEntityUsingTenantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tenant", false);
				relation.AddEntityFieldPair(TenantFields.TenantID, TenantPersonFields.TenantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TenantPersonEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTenantPersonRelations
	{
		internal static readonly IEntityRelation TenantHistoryEntityUsingTenantPersonIDStatic = new TenantPersonRelations().TenantHistoryEntityUsingTenantPersonID;
		internal static readonly IEntityRelation TenantEntityUsingTenantIDStatic = new TenantPersonRelations().TenantEntityUsingTenantID;

		/// <summary>CTor</summary>
		static StaticTenantPersonRelations()
		{
		}
	}
}
