﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ParaTransAttribute. </summary>
	public partial class ParaTransAttributeRelations
	{
		/// <summary>CTor</summary>
		public ParaTransAttributeRelations()
		{
		}

		/// <summary>Gets all relations of the ParaTransAttributeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ParaTransAttributeTypeEntityUsingParaTransAttributeTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ParaTransAttributeEntity and ParaTransAttributeTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// ParaTransAttribute.ParaTransAttributeTypeID - ParaTransAttributeType.EnumerationValue
		/// </summary>
		public virtual IEntityRelation ParaTransAttributeTypeEntityUsingParaTransAttributeTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ParaTransAttributeType", false);
				relation.AddEntityFieldPair(ParaTransAttributeTypeFields.EnumerationValue, ParaTransAttributeFields.ParaTransAttributeTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParaTransAttributeTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParaTransAttributeEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticParaTransAttributeRelations
	{
		internal static readonly IEntityRelation ParaTransAttributeTypeEntityUsingParaTransAttributeTypeIDStatic = new ParaTransAttributeRelations().ParaTransAttributeTypeEntityUsingParaTransAttributeTypeID;

		/// <summary>CTor</summary>
		static StaticParaTransAttributeRelations()
		{
		}
	}
}
