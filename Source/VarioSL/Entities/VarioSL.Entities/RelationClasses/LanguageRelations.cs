﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Language. </summary>
	public partial class LanguageRelations
	{
		/// <summary>CTor</summary>
		public LanguageRelations()
		{
		}

		/// <summary>Gets all relations of the LanguageEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TranslationEntityUsingLanguageID);
			toReturn.Add(this.UserListEntityUsingLanguageID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and TranslationEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageID - Translation.LanguageID
		/// </summary>
		public virtual IEntityRelation TranslationEntityUsingLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Translations" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageID, TranslationFields.LanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TranslationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LanguageEntity and UserListEntity over the 1:n relation they have, using the relation between the fields:
		/// Language.LanguageID - UserList.LanguageID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingLanguageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Users" , true);
				relation.AddEntityFieldPair(LanguageFields.LanguageID, UserListFields.LanguageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LanguageEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLanguageRelations
	{
		internal static readonly IEntityRelation TranslationEntityUsingLanguageIDStatic = new LanguageRelations().TranslationEntityUsingLanguageID;
		internal static readonly IEntityRelation UserListEntityUsingLanguageIDStatic = new LanguageRelations().UserListEntityUsingLanguageID;

		/// <summary>CTor</summary>
		static StaticLanguageRelations()
		{
		}
	}
}
