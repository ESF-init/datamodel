﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: OrderDetailToCardHolder. </summary>
	public partial class OrderDetailToCardHolderRelations
	{
		/// <summary>CTor</summary>
		public OrderDetailToCardHolderRelations()
		{
		}

		/// <summary>Gets all relations of the OrderDetailToCardHolderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardHolderEntityUsingCardHolderID);
			toReturn.Add(this.OrderDetailEntityUsingOrderDetailID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between OrderDetailToCardHolderEntity and CardHolderEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetailToCardHolder.CardHolderID - CardHolder.PersonID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingCardHolderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CardHolder", false);
				relation.AddEntityFieldPair(CardHolderFields.PersonID, OrderDetailToCardHolderFields.CardHolderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardHolderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between OrderDetailToCardHolderEntity and OrderDetailEntity over the m:1 relation they have, using the relation between the fields:
		/// OrderDetailToCardHolder.OrderDetailID - OrderDetail.OrderDetailID
		/// </summary>
		public virtual IEntityRelation OrderDetailEntityUsingOrderDetailID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "OrderDetail", false);
				relation.AddEntityFieldPair(OrderDetailFields.OrderDetailID, OrderDetailToCardHolderFields.OrderDetailID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderDetailToCardHolderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticOrderDetailToCardHolderRelations
	{
		internal static readonly IEntityRelation CardHolderEntityUsingCardHolderIDStatic = new OrderDetailToCardHolderRelations().CardHolderEntityUsingCardHolderID;
		internal static readonly IEntityRelation OrderDetailEntityUsingOrderDetailIDStatic = new OrderDetailToCardHolderRelations().OrderDetailEntityUsingOrderDetailID;

		/// <summary>CTor</summary>
		static StaticOrderDetailToCardHolderRelations()
		{
		}
	}
}
