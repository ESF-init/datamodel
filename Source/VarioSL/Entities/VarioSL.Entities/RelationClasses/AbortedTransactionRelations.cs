﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: AbortedTransaction. </summary>
	public partial class AbortedTransactionRelations
	{
		/// <summary>CTor</summary>
		public AbortedTransactionRelations()
		{
		}

		/// <summary>Gets all relations of the AbortedTransactionEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RevenueTransactionTypeEntityUsingTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between AbortedTransactionEntity and RevenueTransactionTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// AbortedTransaction.TypeID - RevenueTransactionType.TypeID
		/// </summary>
		public virtual IEntityRelation RevenueTransactionTypeEntityUsingTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "RevenueTransactionType", false);
				relation.AddEntityFieldPair(RevenueTransactionTypeFields.TypeID, AbortedTransactionFields.TypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueTransactionTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AbortedTransactionEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticAbortedTransactionRelations
	{
		internal static readonly IEntityRelation RevenueTransactionTypeEntityUsingTypeIDStatic = new AbortedTransactionRelations().RevenueTransactionTypeEntityUsingTypeID;

		/// <summary>CTor</summary>
		static StaticAbortedTransactionRelations()
		{
		}
	}
}
