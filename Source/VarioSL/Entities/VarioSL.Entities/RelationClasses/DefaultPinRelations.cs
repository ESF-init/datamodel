﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DefaultPin. </summary>
	public partial class DefaultPinRelations
	{
		/// <summary>CTor</summary>
		public DefaultPinRelations()
		{
		}

		/// <summary>Gets all relations of the DefaultPinEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TypeOfCardEntityUsingTypeOfCardID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between DefaultPinEntity and TypeOfCardEntity over the 1:1 relation they have, using the relation between the fields:
		/// DefaultPin.TypeOfCardID - TypeOfCard.TypeOfCardID
		/// </summary>
		public virtual IEntityRelation TypeOfCardEntityUsingTypeOfCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TypeOfCard", false);




				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, DefaultPinFields.TypeOfCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DefaultPinEntity", true);
				return relation;
			}
		}

		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDefaultPinRelations
	{
		internal static readonly IEntityRelation TypeOfCardEntityUsingTypeOfCardIDStatic = new DefaultPinRelations().TypeOfCardEntityUsingTypeOfCardID;

		/// <summary>CTor</summary>
		static StaticDefaultPinRelations()
		{
		}
	}
}
