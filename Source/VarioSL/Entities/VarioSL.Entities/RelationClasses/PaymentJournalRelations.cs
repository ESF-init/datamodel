﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentJournal. </summary>
	public partial class PaymentJournalRelations
	{
		/// <summary>CTor</summary>
		public PaymentJournalRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentJournalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BankStatementEntityUsingPaymentJournalID);
			toReturn.Add(this.PaymentJournalToComponentEntityUsingPaymentjournalID);
			toReturn.Add(this.RuleViolationToTransactionEntityUsingPaymentJournalID);
			toReturn.Add(this.SaleToComponentEntityUsingSaleID);
			toReturn.Add(this.BankStatementVerificationEntityUsingPaymentJournalID);
			toReturn.Add(this.PaymentRecognitionEntityUsingPaymentJournalID);
			toReturn.Add(this.PaymentReconciliationEntityUsingPaymentJournalID);
			toReturn.Add(this.CardEntityUsingCardID);
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.PaymentOptionEntityUsingPaymentOptionID);
			toReturn.Add(this.PersonEntityUsingPersonID);
			toReturn.Add(this.SaleEntityUsingSaleID);
			toReturn.Add(this.TransactionJournalEntityUsingTransactionJournalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and BankStatementEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - BankStatement.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation BankStatementEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "BankStatements" , true);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, BankStatementFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and PaymentJournalToComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - PaymentJournalToComponent.PaymentjournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalToComponentEntityUsingPaymentjournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournalToComponent" , true);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentJournalToComponentFields.PaymentjournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalToComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and RuleViolationToTransactionEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - RuleViolationToTransaction.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation RuleViolationToTransactionEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RuleViolationToTransactions" , true);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, RuleViolationToTransactionFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RuleViolationToTransactionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and SaleToComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - SaleToComponent.SaleID
		/// </summary>
		public virtual IEntityRelation SaleToComponentEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SaleToComponents" , true);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, SaleToComponentFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleToComponentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and BankStatementVerificationEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - BankStatementVerification.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation BankStatementVerificationEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "BankStatementVerification", true);


				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, BankStatementVerificationFields.PaymentJournalID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and PaymentRecognitionEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - PaymentRecognition.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentRecognitionEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentRecognition", true);


				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentRecognitionFields.PaymentJournalID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentRecognitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and PaymentReconciliationEntity over the 1:1 relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentJournalID - PaymentReconciliation.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentReconciliationEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentReconciliation", true);


				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentReconciliationFields.PaymentJournalID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentReconciliationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.CardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Card", false);
				relation.AddEntityFieldPair(CardFields.CardID, PaymentJournalFields.CardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, PaymentJournalFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and PaymentOptionEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.PaymentOptionID - PaymentOption.PaymentOptionID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingPaymentOptionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentOption", false);
				relation.AddEntityFieldPair(PaymentOptionFields.PaymentOptionID, PaymentJournalFields.PaymentOptionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, PaymentJournalFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and SaleEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.SaleID - Sale.SaleID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Sale", false);
				relation.AddEntityFieldPair(SaleFields.SaleID, PaymentJournalFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalEntity and TransactionJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournal.TransactionJournalID - TransactionJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransactionJournal", false);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, PaymentJournalFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentJournalRelations
	{
		internal static readonly IEntityRelation BankStatementEntityUsingPaymentJournalIDStatic = new PaymentJournalRelations().BankStatementEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation PaymentJournalToComponentEntityUsingPaymentjournalIDStatic = new PaymentJournalRelations().PaymentJournalToComponentEntityUsingPaymentjournalID;
		internal static readonly IEntityRelation RuleViolationToTransactionEntityUsingPaymentJournalIDStatic = new PaymentJournalRelations().RuleViolationToTransactionEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation SaleToComponentEntityUsingSaleIDStatic = new PaymentJournalRelations().SaleToComponentEntityUsingSaleID;
		internal static readonly IEntityRelation BankStatementVerificationEntityUsingPaymentJournalIDStatic = new PaymentJournalRelations().BankStatementVerificationEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation PaymentRecognitionEntityUsingPaymentJournalIDStatic = new PaymentJournalRelations().PaymentRecognitionEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation PaymentReconciliationEntityUsingPaymentJournalIDStatic = new PaymentJournalRelations().PaymentReconciliationEntityUsingPaymentJournalID;
		internal static readonly IEntityRelation CardEntityUsingCardIDStatic = new PaymentJournalRelations().CardEntityUsingCardID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new PaymentJournalRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingPaymentOptionIDStatic = new PaymentJournalRelations().PaymentOptionEntityUsingPaymentOptionID;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new PaymentJournalRelations().PersonEntityUsingPersonID;
		internal static readonly IEntityRelation SaleEntityUsingSaleIDStatic = new PaymentJournalRelations().SaleEntityUsingSaleID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransactionJournalIDStatic = new PaymentJournalRelations().TransactionJournalEntityUsingTransactionJournalID;

		/// <summary>CTor</summary>
		static StaticPaymentJournalRelations()
		{
		}
	}
}
