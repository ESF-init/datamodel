﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ComponentType. </summary>
	public partial class ComponentTypeRelations
	{
		/// <summary>CTor</summary>
		public ComponentTypeRelations()
		{
		}

		/// <summary>Gets all relations of the ComponentTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CashServiceBalanceEntityUsingComponentTypeID);
			toReturn.Add(this.ComponentEntityUsingComponentTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ComponentTypeEntity and CashServiceBalanceEntity over the 1:n relation they have, using the relation between the fields:
		/// ComponentType.ComponentTypeID - CashServiceBalance.ComponentTypeID
		/// </summary>
		public virtual IEntityRelation CashServiceBalanceEntityUsingComponentTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CashServiceBalance" , true);
				relation.AddEntityFieldPair(ComponentTypeFields.ComponentTypeID, CashServiceBalanceFields.ComponentTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashServiceBalanceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ComponentTypeEntity and ComponentEntity over the 1:n relation they have, using the relation between the fields:
		/// ComponentType.ComponentTypeID - Component.ComponentTypeID
		/// </summary>
		public virtual IEntityRelation ComponentEntityUsingComponentTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Components" , true);
				relation.AddEntityFieldPair(ComponentTypeFields.ComponentTypeID, ComponentFields.ComponentTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticComponentTypeRelations
	{
		internal static readonly IEntityRelation CashServiceBalanceEntityUsingComponentTypeIDStatic = new ComponentTypeRelations().CashServiceBalanceEntityUsingComponentTypeID;
		internal static readonly IEntityRelation ComponentEntityUsingComponentTypeIDStatic = new ComponentTypeRelations().ComponentEntityUsingComponentTypeID;

		/// <summary>CTor</summary>
		static StaticComponentTypeRelations()
		{
		}
	}
}
