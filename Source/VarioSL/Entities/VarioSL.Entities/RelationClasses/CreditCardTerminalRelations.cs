﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CreditCardTerminal. </summary>
	public partial class CreditCardTerminalRelations
	{
		/// <summary>CTor</summary>
		public CreditCardTerminalRelations()
		{
		}

		/// <summary>Gets all relations of the CreditCardTerminalEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CreditCardAuthorizationEntityUsingCreditCardTerminalID);
			toReturn.Add(this.CreditCardMerchantEntityUsingCreditCardMerchantID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between CreditCardTerminalEntity and CreditCardAuthorizationEntity over the 1:n relation they have, using the relation between the fields:
		/// CreditCardTerminal.CreditCardTerminalID - CreditCardAuthorization.CreditCardTerminalID
		/// </summary>
		public virtual IEntityRelation CreditCardAuthorizationEntityUsingCreditCardTerminalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CreditCardAuthorizations" , true);
				relation.AddEntityFieldPair(CreditCardTerminalFields.CreditCardTerminalID, CreditCardAuthorizationFields.CreditCardTerminalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardTerminalEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardAuthorizationEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between CreditCardTerminalEntity and CreditCardMerchantEntity over the m:1 relation they have, using the relation between the fields:
		/// CreditCardTerminal.CreditCardMerchantID - CreditCardMerchant.CreditCardMerchantID
		/// </summary>
		public virtual IEntityRelation CreditCardMerchantEntityUsingCreditCardMerchantID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CreditCardMerchant", false);
				relation.AddEntityFieldPair(CreditCardMerchantFields.CreditCardMerchantID, CreditCardTerminalFields.CreditCardMerchantID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardMerchantEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CreditCardTerminalEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCreditCardTerminalRelations
	{
		internal static readonly IEntityRelation CreditCardAuthorizationEntityUsingCreditCardTerminalIDStatic = new CreditCardTerminalRelations().CreditCardAuthorizationEntityUsingCreditCardTerminalID;
		internal static readonly IEntityRelation CreditCardMerchantEntityUsingCreditCardMerchantIDStatic = new CreditCardTerminalRelations().CreditCardMerchantEntityUsingCreditCardMerchantID;

		/// <summary>CTor</summary>
		static StaticCreditCardTerminalRelations()
		{
		}
	}
}
