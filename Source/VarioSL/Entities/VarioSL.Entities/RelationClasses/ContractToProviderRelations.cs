﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ContractToProvider. </summary>
	public partial class ContractToProviderRelations
	{
		/// <summary>CTor</summary>
		public ContractToProviderRelations()
		{
		}

		/// <summary>Gets all relations of the ContractToProviderEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ContractEntityUsingContractID);
			toReturn.Add(this.MobilityProviderEntityUsingMobilityProviderID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ContractToProviderEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractToProvider.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractToProviderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToProviderEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractToProviderEntity and MobilityProviderEntity over the m:1 relation they have, using the relation between the fields:
		/// ContractToProvider.MobilityProviderID - MobilityProvider.MobilityProviderID
		/// </summary>
		public virtual IEntityRelation MobilityProviderEntityUsingMobilityProviderID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MobilityProvider", false);
				relation.AddEntityFieldPair(MobilityProviderFields.MobilityProviderID, ContractToProviderFields.MobilityProviderID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MobilityProviderEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToProviderEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractToProviderRelations
	{
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new ContractToProviderRelations().ContractEntityUsingContractID;
		internal static readonly IEntityRelation MobilityProviderEntityUsingMobilityProviderIDStatic = new ContractToProviderRelations().MobilityProviderEntityUsingMobilityProviderID;

		/// <summary>CTor</summary>
		static StaticContractToProviderRelations()
		{
		}
	}
}
