﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CustomerAccountNotification. </summary>
	public partial class CustomerAccountNotificationRelations
	{
		/// <summary>CTor</summary>
		public CustomerAccountNotificationRelations()
		{
		}

		/// <summary>Gets all relations of the CustomerAccountNotificationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CustomerAccountEntityUsingCustomerAccountID);
			toReturn.Add(this.DeviceTokenEntityUsingDeviceTokenID);
			toReturn.Add(this.MessageTypeEntityUsingMessageTypeID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CustomerAccountNotificationEntity and CustomerAccountEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccountNotification.CustomerAccountID - CustomerAccount.CustomerAccountID
		/// </summary>
		public virtual IEntityRelation CustomerAccountEntityUsingCustomerAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CustomerAccount", false);
				relation.AddEntityFieldPair(CustomerAccountFields.CustomerAccountID, CustomerAccountNotificationFields.CustomerAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomerAccountNotificationEntity and DeviceTokenEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccountNotification.DeviceTokenID - DeviceToken.DeviceTokenID
		/// </summary>
		public virtual IEntityRelation DeviceTokenEntityUsingDeviceTokenID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DeviceToken", false);
				relation.AddEntityFieldPair(DeviceTokenFields.DeviceTokenID, CustomerAccountNotificationFields.DeviceTokenID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceTokenEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CustomerAccountNotificationEntity and MessageTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// CustomerAccountNotification.MessageTypeID - MessageType.EnumerationValue
		/// </summary>
		public virtual IEntityRelation MessageTypeEntityUsingMessageTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MessageType", false);
				relation.AddEntityFieldPair(MessageTypeFields.EnumerationValue, CustomerAccountNotificationFields.MessageTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("MessageTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountNotificationEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCustomerAccountNotificationRelations
	{
		internal static readonly IEntityRelation CustomerAccountEntityUsingCustomerAccountIDStatic = new CustomerAccountNotificationRelations().CustomerAccountEntityUsingCustomerAccountID;
		internal static readonly IEntityRelation DeviceTokenEntityUsingDeviceTokenIDStatic = new CustomerAccountNotificationRelations().DeviceTokenEntityUsingDeviceTokenID;
		internal static readonly IEntityRelation MessageTypeEntityUsingMessageTypeIDStatic = new CustomerAccountNotificationRelations().MessageTypeEntityUsingMessageTypeID;

		/// <summary>CTor</summary>
		static StaticCustomerAccountNotificationRelations()
		{
		}
	}
}
