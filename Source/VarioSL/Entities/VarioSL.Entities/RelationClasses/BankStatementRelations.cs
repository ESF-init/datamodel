﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: BankStatement. </summary>
	public partial class BankStatementRelations
	{
		/// <summary>CTor</summary>
		public BankStatementRelations()
		{
		}

		/// <summary>Gets all relations of the BankStatementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.BankStatementVerificationEntityUsingBankStatementID);
			toReturn.Add(this.PaymentJournalEntityUsingPaymentJournalID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between BankStatementEntity and BankStatementVerificationEntity over the 1:1 relation they have, using the relation between the fields:
		/// BankStatement.BankStatementID - BankStatementVerification.BankStatementID
		/// </summary>
		public virtual IEntityRelation BankStatementVerificationEntityUsingBankStatementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "BankStatementVerification", true);


				relation.AddEntityFieldPair(BankStatementFields.BankStatementID, BankStatementVerificationFields.BankStatementID);


				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementVerificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between BankStatementEntity and PaymentJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// BankStatement.PaymentJournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentJournal", false);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, BankStatementFields.PaymentJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("BankStatementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticBankStatementRelations
	{
		internal static readonly IEntityRelation BankStatementVerificationEntityUsingBankStatementIDStatic = new BankStatementRelations().BankStatementVerificationEntityUsingBankStatementID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentJournalIDStatic = new BankStatementRelations().PaymentJournalEntityUsingPaymentJournalID;

		/// <summary>CTor</summary>
		static StaticBankStatementRelations()
		{
		}
	}
}
