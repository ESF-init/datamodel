﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SettlementSetup. </summary>
	public partial class SettlementSetupRelations
	{
		/// <summary>CTor</summary>
		public SettlementSetupRelations()
		{
		}

		/// <summary>Gets all relations of the SettlementSetupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SettlementQuerySettingEntityUsingSettlementSetupID);
			toReturn.Add(this.SettlementRunEntityUsingSettlementSetupID);
			toReturn.Add(this.SettlementAccountEntityUsingFromAccountID);
			toReturn.Add(this.SettlementAccountEntityUsingToAccountID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SettlementSetupEntity and SettlementQuerySettingEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementSetup.SettlementSetupID - SettlementQuerySetting.SettlementSetupID
		/// </summary>
		public virtual IEntityRelation SettlementQuerySettingEntityUsingSettlementSetupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementQuerySettings" , true);
				relation.AddEntityFieldPair(SettlementSetupFields.SettlementSetupID, SettlementQuerySettingFields.SettlementSetupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementQuerySettingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between SettlementSetupEntity and SettlementRunEntity over the 1:n relation they have, using the relation between the fields:
		/// SettlementSetup.SettlementSetupID - SettlementRun.SettlementSetupID
		/// </summary>
		public virtual IEntityRelation SettlementRunEntityUsingSettlementSetupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementRuns" , true);
				relation.AddEntityFieldPair(SettlementSetupFields.SettlementSetupID, SettlementRunFields.SettlementSetupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SettlementSetupEntity and SettlementAccountEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementSetup.FromAccountID - SettlementAccount.SettlementAccountID
		/// </summary>
		public virtual IEntityRelation SettlementAccountEntityUsingFromAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FromSettlementAccount", false);
				relation.AddEntityFieldPair(SettlementAccountFields.SettlementAccountID, SettlementSetupFields.FromAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementAccountEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SettlementSetupEntity and SettlementAccountEntity over the m:1 relation they have, using the relation between the fields:
		/// SettlementSetup.ToAccountID - SettlementAccount.SettlementAccountID
		/// </summary>
		public virtual IEntityRelation SettlementAccountEntityUsingToAccountID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ToSettlementAccount", false);
				relation.AddEntityFieldPair(SettlementAccountFields.SettlementAccountID, SettlementSetupFields.ToAccountID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementAccountEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementSetupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSettlementSetupRelations
	{
		internal static readonly IEntityRelation SettlementQuerySettingEntityUsingSettlementSetupIDStatic = new SettlementSetupRelations().SettlementQuerySettingEntityUsingSettlementSetupID;
		internal static readonly IEntityRelation SettlementRunEntityUsingSettlementSetupIDStatic = new SettlementSetupRelations().SettlementRunEntityUsingSettlementSetupID;
		internal static readonly IEntityRelation SettlementAccountEntityUsingFromAccountIDStatic = new SettlementSetupRelations().SettlementAccountEntityUsingFromAccountID;
		internal static readonly IEntityRelation SettlementAccountEntityUsingToAccountIDStatic = new SettlementSetupRelations().SettlementAccountEntityUsingToAccountID;

		/// <summary>CTor</summary>
		static StaticSettlementSetupRelations()
		{
		}
	}
}
