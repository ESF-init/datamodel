﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Debtor. </summary>
	public partial class DebtorRelations
	{
		/// <summary>CTor</summary>
		public DebtorRelations()
		{
		}

		/// <summary>Gets all relations of the DebtorEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AccountEntryEntityUsingContractID);
			toReturn.Add(this.ClearingResultEntityUsingDebtorID);
			toReturn.Add(this.ComponentClearingEntityUsingMaintenanceStaffID);
			toReturn.Add(this.ComponentClearingEntityUsingPersonnelClearingID);
			toReturn.Add(this.ComponentFillingEntityUsingMaintenanceStaffID);
			toReturn.Add(this.ComponentFillingEntityUsingPersonnelFillingID);
			toReturn.Add(this.QualificationEntityUsingDebtorID);
			toReturn.Add(this.UserListEntityUsingDebtorID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingDebtorID);
			toReturn.Add(this.VarioAddressEntityUsingAddressID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DepotEntityUsingDepotID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and AccountEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - AccountEntry.ContractID
		/// </summary>
		public virtual IEntityRelation AccountEntryEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "AccountEntries" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, AccountEntryFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - ClearingResult.DebtorID
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResults" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ClearingResultFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ComponentClearingEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - ComponentClearing.MaintenanceStaffID
		/// </summary>
		public virtual IEntityRelation ComponentClearingEntityUsingMaintenanceStaffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentClearingsByMaintenanceStaff" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentClearingFields.MaintenanceStaffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentClearingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ComponentClearingEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - ComponentClearing.PersonnelClearingID
		/// </summary>
		public virtual IEntityRelation ComponentClearingEntityUsingPersonnelClearingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentClearingsByPersonnelClearing" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentClearingFields.PersonnelClearingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentClearingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ComponentFillingEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - ComponentFilling.MaintenanceStaffID
		/// </summary>
		public virtual IEntityRelation ComponentFillingEntityUsingMaintenanceStaffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentFillingsByMaintenanceStaff" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentFillingFields.MaintenanceStaffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ComponentFillingEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - ComponentFilling.PersonnelFillingID
		/// </summary>
		public virtual IEntityRelation ComponentFillingEntityUsingPersonnelFillingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ComponentFillingsByPersonnelFilling" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentFillingFields.PersonnelFillingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and QualificationEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - Qualification.DebtorID
		/// </summary>
		public virtual IEntityRelation QualificationEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Qualifications" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, QualificationFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("QualificationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and UserListEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - UserList.DebtorID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Users" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, UserListFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// Debtor.DebtorID - FareEvasionIncident.DebtorID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingDebtorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, FareEvasionIncidentFields.DebtorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and VarioAddressEntity over the 1:1 relation they have, using the relation between the fields:
		/// Debtor.AddressID - VarioAddress.AddressID
		/// </summary>
		public virtual IEntityRelation VarioAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Address", false);




				relation.AddEntityFieldPair(VarioAddressFields.AddressID, DebtorFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Debtor.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, DebtorFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DebtorEntity and DepotEntity over the m:1 relation they have, using the relation between the fields:
		/// Debtor.DepotID - Depot.DepotID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Depot", false);
				relation.AddEntityFieldPair(DepotFields.DepotID, DebtorFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDebtorRelations
	{
		internal static readonly IEntityRelation AccountEntryEntityUsingContractIDStatic = new DebtorRelations().AccountEntryEntityUsingContractID;
		internal static readonly IEntityRelation ClearingResultEntityUsingDebtorIDStatic = new DebtorRelations().ClearingResultEntityUsingDebtorID;
		internal static readonly IEntityRelation ComponentClearingEntityUsingMaintenanceStaffIDStatic = new DebtorRelations().ComponentClearingEntityUsingMaintenanceStaffID;
		internal static readonly IEntityRelation ComponentClearingEntityUsingPersonnelClearingIDStatic = new DebtorRelations().ComponentClearingEntityUsingPersonnelClearingID;
		internal static readonly IEntityRelation ComponentFillingEntityUsingMaintenanceStaffIDStatic = new DebtorRelations().ComponentFillingEntityUsingMaintenanceStaffID;
		internal static readonly IEntityRelation ComponentFillingEntityUsingPersonnelFillingIDStatic = new DebtorRelations().ComponentFillingEntityUsingPersonnelFillingID;
		internal static readonly IEntityRelation QualificationEntityUsingDebtorIDStatic = new DebtorRelations().QualificationEntityUsingDebtorID;
		internal static readonly IEntityRelation UserListEntityUsingDebtorIDStatic = new DebtorRelations().UserListEntityUsingDebtorID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingDebtorIDStatic = new DebtorRelations().FareEvasionIncidentEntityUsingDebtorID;
		internal static readonly IEntityRelation VarioAddressEntityUsingAddressIDStatic = new DebtorRelations().VarioAddressEntityUsingAddressID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new DebtorRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DepotEntityUsingDepotIDStatic = new DebtorRelations().DepotEntityUsingDepotID;

		/// <summary>CTor</summary>
		static StaticDebtorRelations()
		{
		}
	}
}
