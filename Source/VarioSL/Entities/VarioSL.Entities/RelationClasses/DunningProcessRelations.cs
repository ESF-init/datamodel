﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningProcess. </summary>
	public partial class DunningProcessRelations
	{
		/// <summary>CTor</summary>
		public DunningProcessRelations()
		{
		}

		/// <summary>Gets all relations of the DunningProcessEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningToInvoiceEntityUsingDunningProcessID);
			toReturn.Add(this.DunningToPostingEntityUsingDunningProcessID);
			toReturn.Add(this.DunningToProductEntityUsingDunningProcessID);
			toReturn.Add(this.ContractEntityUsingContractID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DunningProcessEntity and DunningToInvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningProcess.DunningProcessID - DunningToInvoice.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningToInvoiceEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToInvoices" , true);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToInvoiceFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DunningProcessEntity and DunningToPostingEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningProcess.DunningProcessID - DunningToPosting.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningToPostingEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToPostings" , true);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToPostingFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToPostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DunningProcessEntity and DunningToProductEntity over the 1:n relation they have, using the relation between the fields:
		/// DunningProcess.DunningProcessID - DunningToProduct.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningToProductEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningToProducts" , true);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToProductFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToProductEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between DunningProcessEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningProcess.ContractID - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, DunningProcessFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningProcessRelations
	{
		internal static readonly IEntityRelation DunningToInvoiceEntityUsingDunningProcessIDStatic = new DunningProcessRelations().DunningToInvoiceEntityUsingDunningProcessID;
		internal static readonly IEntityRelation DunningToPostingEntityUsingDunningProcessIDStatic = new DunningProcessRelations().DunningToPostingEntityUsingDunningProcessID;
		internal static readonly IEntityRelation DunningToProductEntityUsingDunningProcessIDStatic = new DunningProcessRelations().DunningToProductEntityUsingDunningProcessID;
		internal static readonly IEntityRelation ContractEntityUsingContractIDStatic = new DunningProcessRelations().ContractEntityUsingContractID;

		/// <summary>CTor</summary>
		static StaticDunningProcessRelations()
		{
		}
	}
}
