﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Photograph. </summary>
	public partial class PhotographRelations
	{
		/// <summary>CTor</summary>
		public PhotographRelations()
		{
		}

		/// <summary>Gets all relations of the PhotographEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.OrganizationParticipantEntityUsingPhotographID);
			toReturn.Add(this.ContractEntityUsingContractid);
			toReturn.Add(this.PersonEntityUsingPersonID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PhotographEntity and OrganizationParticipantEntity over the 1:n relation they have, using the relation between the fields:
		/// Photograph.PhotographID - OrganizationParticipant.PhotographID
		/// </summary>
		public virtual IEntityRelation OrganizationParticipantEntityUsingPhotographID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrganizationParticipants" , true);
				relation.AddEntityFieldPair(PhotographFields.PhotographID, OrganizationParticipantFields.PhotographID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PhotographEntity and ContractEntity over the m:1 relation they have, using the relation between the fields:
		/// Photograph.Contractid - Contract.ContractID
		/// </summary>
		public virtual IEntityRelation ContractEntityUsingContractid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Contract", false);
				relation.AddEntityFieldPair(ContractFields.ContractID, PhotographFields.Contractid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PhotographEntity and PersonEntity over the m:1 relation they have, using the relation between the fields:
		/// Photograph.PersonID - Person.PersonID
		/// </summary>
		public virtual IEntityRelation PersonEntityUsingPersonID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Person", false);
				relation.AddEntityFieldPair(PersonFields.PersonID, PhotographFields.PersonID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PersonEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPhotographRelations
	{
		internal static readonly IEntityRelation OrganizationParticipantEntityUsingPhotographIDStatic = new PhotographRelations().OrganizationParticipantEntityUsingPhotographID;
		internal static readonly IEntityRelation ContractEntityUsingContractidStatic = new PhotographRelations().ContractEntityUsingContractid;
		internal static readonly IEntityRelation PersonEntityUsingPersonIDStatic = new PhotographRelations().PersonEntityUsingPersonID;

		/// <summary>CTor</summary>
		static StaticPhotographRelations()
		{
		}
	}
}
