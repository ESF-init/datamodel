﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketType. </summary>
	public partial class TicketTypeRelations
	{
		/// <summary>CTor</summary>
		public TicketTypeRelations()
		{
		}

		/// <summary>Gets all relations of the TicketTypeEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ServiceIdToCardEntityUsingTicketTypeID);
			toReturn.Add(this.TicketEntityUsingTicketTypeID);
			toReturn.Add(this.TicketSerialNumberEntityUsingTicketTypeID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TicketTypeEntity and ServiceIdToCardEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketType.TicketTypeID - ServiceIdToCard.TicketTypeID
		/// </summary>
		public virtual IEntityRelation ServiceIdToCardEntityUsingTicketTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ServiceIdToCards" , true);
				relation.AddEntityFieldPair(TicketTypeFields.TicketTypeID, ServiceIdToCardFields.TicketTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ServiceIdToCardEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketTypeEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketType.TicketTypeID - Ticket.TicketTypeID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(TicketTypeFields.TicketTypeID, TicketFields.TicketTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketTypeEntity and TicketSerialNumberEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketType.TicketTypeID - TicketSerialNumber.TicketTypeID
		/// </summary>
		public virtual IEntityRelation TicketSerialNumberEntityUsingTicketTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketSerialNumbers" , true);
				relation.AddEntityFieldPair(TicketTypeFields.TicketTypeID, TicketSerialNumberFields.TicketTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketTypeEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketTypeRelations
	{
		internal static readonly IEntityRelation ServiceIdToCardEntityUsingTicketTypeIDStatic = new TicketTypeRelations().ServiceIdToCardEntityUsingTicketTypeID;
		internal static readonly IEntityRelation TicketEntityUsingTicketTypeIDStatic = new TicketTypeRelations().TicketEntityUsingTicketTypeID;
		internal static readonly IEntityRelation TicketSerialNumberEntityUsingTicketTypeIDStatic = new TicketTypeRelations().TicketSerialNumberEntityUsingTicketTypeID;

		/// <summary>CTor</summary>
		static StaticTicketTypeRelations()
		{
		}
	}
}
