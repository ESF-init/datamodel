﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: FixedBitmapLayoutObject. </summary>
	public partial class FixedBitmapLayoutObjectRelations
	{
		/// <summary>CTor</summary>
		public FixedBitmapLayoutObjectRelations()
		{
		}

		/// <summary>Gets all relations of the FixedBitmapLayoutObjectEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LayoutEntityUsingLayoutID);
			toReturn.Add(this.LogoEntityUsingLogoID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between FixedBitmapLayoutObjectEntity and LayoutEntity over the m:1 relation they have, using the relation between the fields:
		/// FixedBitmapLayoutObject.LayoutID - Layout.LayoutID
		/// </summary>
		public virtual IEntityRelation LayoutEntityUsingLayoutID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Layout", false);
				relation.AddEntityFieldPair(LayoutFields.LayoutID, FixedBitmapLayoutObjectFields.LayoutID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LayoutEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FixedBitmapLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between FixedBitmapLayoutObjectEntity and LogoEntity over the m:1 relation they have, using the relation between the fields:
		/// FixedBitmapLayoutObject.LogoID - Logo.LogoID
		/// </summary>
		public virtual IEntityRelation LogoEntityUsingLogoID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Logo", false);
				relation.AddEntityFieldPair(LogoFields.LogoID, FixedBitmapLayoutObjectFields.LogoID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LogoEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FixedBitmapLayoutObjectEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticFixedBitmapLayoutObjectRelations
	{
		internal static readonly IEntityRelation LayoutEntityUsingLayoutIDStatic = new FixedBitmapLayoutObjectRelations().LayoutEntityUsingLayoutID;
		internal static readonly IEntityRelation LogoEntityUsingLogoIDStatic = new FixedBitmapLayoutObjectRelations().LogoEntityUsingLogoID;

		/// <summary>CTor</summary>
		static StaticFixedBitmapLayoutObjectRelations()
		{
		}
	}
}
