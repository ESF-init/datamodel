﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalPacketEffort. </summary>
	public partial class ExternalPacketEffortRelations
	{
		/// <summary>CTor</summary>
		public ExternalPacketEffortRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalPacketEffortEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalEffortEntityUsingEffortID);
			toReturn.Add(this.ExternalPacketEntityUsingPacketID);
			toReturn.Add(this.FareStageEntityUsingFareStageID);
			toReturn.Add(this.LineEntityUsingLineID);
			toReturn.Add(this.OperatorEntityUsingOperatorID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			toReturn.Add(this.TypeOfCardEntityUsingCardTypeId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and ExternalEffortEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.EffortID - ExternalEffort.EffortID
		/// </summary>
		public virtual IEntityRelation ExternalEffortEntityUsingEffortID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalEffort", false);
				relation.AddEntityFieldPair(ExternalEffortFields.EffortID, ExternalPacketEffortFields.EffortID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and ExternalPacketEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.PacketID - ExternalPacket.PacketID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEntityUsingPacketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalPacket", false);
				relation.AddEntityFieldPair(ExternalPacketFields.PacketID, ExternalPacketEffortFields.PacketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and FareStageEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.FareStageID - FareStage.FareStageID
		/// </summary>
		public virtual IEntityRelation FareStageEntityUsingFareStageID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FareStage", false);
				relation.AddEntityFieldPair(FareStageFields.FareStageID, ExternalPacketEffortFields.FareStageID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareStageEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and LineEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.LineID - Line.LineID
		/// </summary>
		public virtual IEntityRelation LineEntityUsingLineID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Line", false);
				relation.AddEntityFieldPair(LineFields.LineID, ExternalPacketEffortFields.LineID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LineEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and OperatorEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.OperatorID - Operator.OperatorID
		/// </summary>
		public virtual IEntityRelation OperatorEntityUsingOperatorID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Operator", false);
				relation.AddEntityFieldPair(OperatorFields.OperatorID, ExternalPacketEffortFields.OperatorID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OperatorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalPacketEffortFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalPacketEffortEntity and TypeOfCardEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalPacketEffort.CardTypeId - TypeOfCard.TypeOfCardID
		/// </summary>
		public virtual IEntityRelation TypeOfCardEntityUsingCardTypeId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TypeOfCard", false);
				relation.AddEntityFieldPair(TypeOfCardFields.TypeOfCardID, ExternalPacketEffortFields.CardTypeId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TypeOfCardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEffortEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalPacketEffortRelations
	{
		internal static readonly IEntityRelation ExternalEffortEntityUsingEffortIDStatic = new ExternalPacketEffortRelations().ExternalEffortEntityUsingEffortID;
		internal static readonly IEntityRelation ExternalPacketEntityUsingPacketIDStatic = new ExternalPacketEffortRelations().ExternalPacketEntityUsingPacketID;
		internal static readonly IEntityRelation FareStageEntityUsingFareStageIDStatic = new ExternalPacketEffortRelations().FareStageEntityUsingFareStageID;
		internal static readonly IEntityRelation LineEntityUsingLineIDStatic = new ExternalPacketEffortRelations().LineEntityUsingLineID;
		internal static readonly IEntityRelation OperatorEntityUsingOperatorIDStatic = new ExternalPacketEffortRelations().OperatorEntityUsingOperatorID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new ExternalPacketEffortRelations().TariffEntityUsingTariffID;
		internal static readonly IEntityRelation TypeOfCardEntityUsingCardTypeIdStatic = new ExternalPacketEffortRelations().TypeOfCardEntityUsingCardTypeId;

		/// <summary>CTor</summary>
		static StaticExternalPacketEffortRelations()
		{
		}
	}
}
