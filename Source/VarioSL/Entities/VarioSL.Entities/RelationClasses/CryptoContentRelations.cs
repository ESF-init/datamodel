﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CryptoContent. </summary>
	public partial class CryptoContentRelations
	{
		/// <summary>CTor</summary>
		public CryptoContentRelations()
		{
		}

		/// <summary>Gets all relations of the CryptoContentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CryptoCryptogramArchiveEntityUsingCryptogramArchiveID);
			toReturn.Add(this.CryptoResourceEntityUsingCryptogramResourceID);
			toReturn.Add(this.CryptoResourceEntityUsingSignCertificateResourceID);
			toReturn.Add(this.CryptoResourceEntityUsingSubCertificateResourceID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CryptoContentEntity and CryptoCryptogramArchiveEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoContent.CryptogramArchiveID - CryptoCryptogramArchive.CryptogramArchiveID
		/// </summary>
		public virtual IEntityRelation CryptoCryptogramArchiveEntityUsingCryptogramArchiveID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoCryptogramArchive", false);
				relation.AddEntityFieldPair(CryptoCryptogramArchiveFields.CryptogramArchiveID, CryptoContentFields.CryptogramArchiveID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoCryptogramArchiveEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CryptoContentEntity and CryptoResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoContent.CryptogramResourceID - CryptoResource.ResourceID
		/// </summary>
		public virtual IEntityRelation CryptoResourceEntityUsingCryptogramResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoResourceCryptogram", false);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.CryptogramResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CryptoContentEntity and CryptoResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoContent.SignCertificateResourceID - CryptoResource.ResourceID
		/// </summary>
		public virtual IEntityRelation CryptoResourceEntityUsingSignCertificateResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoResourceSignCertificate", false);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.SignCertificateResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between CryptoContentEntity and CryptoResourceEntity over the m:1 relation they have, using the relation between the fields:
		/// CryptoContent.SubCertificateResourceID - CryptoResource.ResourceID
		/// </summary>
		public virtual IEntityRelation CryptoResourceEntityUsingSubCertificateResourceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CryptoResourceSubCertificate", false);
				relation.AddEntityFieldPair(CryptoResourceFields.ResourceID, CryptoContentFields.SubCertificateResourceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoResourceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CryptoContentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCryptoContentRelations
	{
		internal static readonly IEntityRelation CryptoCryptogramArchiveEntityUsingCryptogramArchiveIDStatic = new CryptoContentRelations().CryptoCryptogramArchiveEntityUsingCryptogramArchiveID;
		internal static readonly IEntityRelation CryptoResourceEntityUsingCryptogramResourceIDStatic = new CryptoContentRelations().CryptoResourceEntityUsingCryptogramResourceID;
		internal static readonly IEntityRelation CryptoResourceEntityUsingSignCertificateResourceIDStatic = new CryptoContentRelations().CryptoResourceEntityUsingSignCertificateResourceID;
		internal static readonly IEntityRelation CryptoResourceEntityUsingSubCertificateResourceIDStatic = new CryptoContentRelations().CryptoResourceEntityUsingSubCertificateResourceID;

		/// <summary>CTor</summary>
		static StaticCryptoContentRelations()
		{
		}
	}
}
