﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Contract. </summary>
	public partial class ContractRelations
	{
		/// <summary>CTor</summary>
		public ContractRelations()
		{
		}

		/// <summary>Gets all relations of the ContractEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEventEntityUsingContractID);
			toReturn.Add(this.CardHolderEntityUsingContractID);
			toReturn.Add(this.CardToContractEntityUsingContractID);
			toReturn.Add(this.CommentEntityUsingContractID);
			toReturn.Add(this.ContractAddressEntityUsingContractID);
			toReturn.Add(this.ContractHistoryEntityUsingContractID);
			toReturn.Add(this.ContractLinkEntityUsingSourceContractID);
			toReturn.Add(this.ContractLinkEntityUsingTargetContractID);
			toReturn.Add(this.ContractTerminationEntityUsingContractID);
			toReturn.Add(this.ContractToPaymentMethodEntityUsingContractID);
			toReturn.Add(this.ContractToProviderEntityUsingContractID);
			toReturn.Add(this.ContractWorkItemEntityUsingContractID);
			toReturn.Add(this.CustomerAccountEntityUsingContractID);
			toReturn.Add(this.DunningProcessEntityUsingContractID);
			toReturn.Add(this.FareEvasionIncidentEntityUsingContractID);
			toReturn.Add(this.InvoiceEntityUsingContractID);
			toReturn.Add(this.JobEntityUsingContractID);
			toReturn.Add(this.JobBulkImportEntityUsingContractID);
			toReturn.Add(this.NotificationMessageOwnerEntityUsingContractID);
			toReturn.Add(this.OrderEntityUsingContractID);
			toReturn.Add(this.OrganizationParticipantEntityUsingContractID);
			toReturn.Add(this.ParticipantGroupEntityUsingContractID);
			toReturn.Add(this.PaymentJournalEntityUsingContractID);
			toReturn.Add(this.PaymentOptionEntityUsingContractID);
			toReturn.Add(this.PaymentProviderAccountEntityUsingContractID);
			toReturn.Add(this.PhotographEntityUsingContractid);
			toReturn.Add(this.PostingEntityUsingContractID);
			toReturn.Add(this.ProductEntityUsingContractID);
			toReturn.Add(this.ProductTerminationEntityUsingContractID);
			toReturn.Add(this.SaleEntityUsingContractID);
			toReturn.Add(this.SaleEntityUsingSecondaryContractID);
			toReturn.Add(this.StatementOfAccountEntityUsingContractID);
			toReturn.Add(this.TicketAssignmentEntityUsingContractID);
			toReturn.Add(this.PaymentModalityEntityUsingPaymentModalityID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.ClientEntityUsingVanpoolClientID);
			toReturn.Add(this.AccountingModeEntityUsingAccountingModeID);
			toReturn.Add(this.OrganizationEntityUsingOrganizationID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and CardEventEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - CardEvent.ContractID
		/// </summary>
		public virtual IEntityRelation CardEventEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardEvents" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, CardEventFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEventEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and CardHolderEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - CardHolder.ContractID
		/// </summary>
		public virtual IEntityRelation CardHolderEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardHolders" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, CardHolderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardHolderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and CardToContractEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - CardToContract.ContractID
		/// </summary>
		public virtual IEntityRelation CardToContractEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CardsToContracts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, CardToContractFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardToContractEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and CommentEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Comment.ContractID
		/// </summary>
		public virtual IEntityRelation CommentEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SlComments" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, CommentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CommentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractAddressEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractAddress.ContractID
		/// </summary>
		public virtual IEntityRelation ContractAddressEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractAddresses" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractAddressFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractAddressEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractHistory.ContractID
		/// </summary>
		public virtual IEntityRelation ContractHistoryEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractHistoryItems" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractHistoryFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractHistoryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractLinkEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractLink.SourceContractID
		/// </summary>
		public virtual IEntityRelation ContractLinkEntityUsingSourceContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TargetContracts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractLinkFields.SourceContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractLinkEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractLinkEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractLink.TargetContractID
		/// </summary>
		public virtual IEntityRelation ContractLinkEntityUsingTargetContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SourceContracts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractLinkFields.TargetContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractLinkEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractTermination.ContractID
		/// </summary>
		public virtual IEntityRelation ContractTerminationEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractTerminations" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractTerminationFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractToPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractToPaymentMethod.ContractID
		/// </summary>
		public virtual IEntityRelation ContractToPaymentMethodEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractToPaymentMethods" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractToPaymentMethodFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToPaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractToProviderEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractToProvider.ContractID
		/// </summary>
		public virtual IEntityRelation ContractToProviderEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ContractToProviders" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractToProviderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractToProviderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ContractWorkItemEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ContractWorkItem.ContractID
		/// </summary>
		public virtual IEntityRelation ContractWorkItemEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItems" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ContractWorkItemFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractWorkItemEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and CustomerAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - CustomerAccount.ContractID
		/// </summary>
		public virtual IEntityRelation CustomerAccountEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "CustomerAccounts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, CustomerAccountFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CustomerAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and DunningProcessEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - DunningProcess.ContractID
		/// </summary>
		public virtual IEntityRelation DunningProcessEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "DunningProcesses" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, DunningProcessFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and FareEvasionIncidentEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - FareEvasionIncident.ContractID
		/// </summary>
		public virtual IEntityRelation FareEvasionIncidentEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareEvasionIncidents" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, FareEvasionIncidentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareEvasionIncidentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and InvoiceEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Invoice.ContractID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Invoices" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, InvoiceFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and JobEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Job.ContractID
		/// </summary>
		public virtual IEntityRelation JobEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Jobs" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, JobFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and JobBulkImportEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - JobBulkImport.ContractID
		/// </summary>
		public virtual IEntityRelation JobBulkImportEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "JobBulkImports" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, JobBulkImportFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("JobBulkImportEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and NotificationMessageOwnerEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - NotificationMessageOwner.ContractID
		/// </summary>
		public virtual IEntityRelation NotificationMessageOwnerEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "NotificationMessageOwners" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, NotificationMessageOwnerFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("NotificationMessageOwnerEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and OrderEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Order.ContractID
		/// </summary>
		public virtual IEntityRelation OrderEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Orders" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, OrderFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrderEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and OrganizationParticipantEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - OrganizationParticipant.ContractID
		/// </summary>
		public virtual IEntityRelation OrganizationParticipantEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "OrganizationParticipants" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, OrganizationParticipantFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationParticipantEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ParticipantGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ParticipantGroup.ContractID
		/// </summary>
		public virtual IEntityRelation ParticipantGroupEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParticipantGroups" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ParticipantGroupFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParticipantGroupEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PaymentJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - PaymentJournal.ContractID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentJournals" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, PaymentJournalFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PaymentOptionEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - PaymentOption.ContractID
		/// </summary>
		public virtual IEntityRelation PaymentOptionEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentOptions" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, PaymentOptionFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentOptionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PaymentProviderAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - PaymentProviderAccount.ContractID
		/// </summary>
		public virtual IEntityRelation PaymentProviderAccountEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PaymentProviderAccounts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, PaymentProviderAccountFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentProviderAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PhotographEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Photograph.Contractid
		/// </summary>
		public virtual IEntityRelation PhotographEntityUsingContractid
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Photographs" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, PhotographFields.Contractid);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PhotographEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Posting.ContractID
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, PostingFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ProductEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Product.ContractID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Products" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ProductFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ProductTerminationEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - ProductTermination.ContractID
		/// </summary>
		public virtual IEntityRelation ProductTerminationEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ProductTerminations" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, ProductTerminationFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductTerminationEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Sale.ContractID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, SaleFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - Sale.SecondaryContractID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSecondaryContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SecondarySales" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, SaleFields.SecondaryContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and StatementOfAccountEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - StatementOfAccount.ContractID
		/// </summary>
		public virtual IEntityRelation StatementOfAccountEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SlStatementofaccounts" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, StatementOfAccountFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("StatementOfAccountEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and TicketAssignmentEntity over the 1:n relation they have, using the relation between the fields:
		/// Contract.ContractID - TicketAssignment.ContractID
		/// </summary>
		public virtual IEntityRelation TicketAssignmentEntityUsingContractID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketAssignments" , true);
				relation.AddEntityFieldPair(ContractFields.ContractID, TicketAssignmentFields.ContractID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketAssignmentEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and PaymentModalityEntity over the 1:1 relation they have, using the relation between the fields:
		/// Contract.PaymentModalityID - PaymentModality.PaymentModalityID
		/// </summary>
		public virtual IEntityRelation PaymentModalityEntityUsingPaymentModalityID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "PaymentModality", false);




				relation.AddEntityFieldPair(PaymentModalityFields.PaymentModalityID, ContractFields.PaymentModalityID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentModalityEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Contract.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ContractFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Contract.VanpoolClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingVanpoolClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "VanpoolClient", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, ContractFields.VanpoolClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractEntity and AccountingModeEntity over the m:1 relation they have, using the relation between the fields:
		/// Contract.AccountingModeID - AccountingMode.AccountingModeID
		/// </summary>
		public virtual IEntityRelation AccountingModeEntityUsingAccountingModeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "AccountingMode", false);
				relation.AddEntityFieldPair(AccountingModeFields.AccountingModeID, ContractFields.AccountingModeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AccountingModeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ContractEntity and OrganizationEntity over the m:1 relation they have, using the relation between the fields:
		/// Contract.OrganizationID - Organization.OrganizationID
		/// </summary>
		public virtual IEntityRelation OrganizationEntityUsingOrganizationID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Organization", false);
				relation.AddEntityFieldPair(OrganizationFields.OrganizationID, ContractFields.OrganizationID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("OrganizationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ContractEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticContractRelations
	{
		internal static readonly IEntityRelation CardEventEntityUsingContractIDStatic = new ContractRelations().CardEventEntityUsingContractID;
		internal static readonly IEntityRelation CardHolderEntityUsingContractIDStatic = new ContractRelations().CardHolderEntityUsingContractID;
		internal static readonly IEntityRelation CardToContractEntityUsingContractIDStatic = new ContractRelations().CardToContractEntityUsingContractID;
		internal static readonly IEntityRelation CommentEntityUsingContractIDStatic = new ContractRelations().CommentEntityUsingContractID;
		internal static readonly IEntityRelation ContractAddressEntityUsingContractIDStatic = new ContractRelations().ContractAddressEntityUsingContractID;
		internal static readonly IEntityRelation ContractHistoryEntityUsingContractIDStatic = new ContractRelations().ContractHistoryEntityUsingContractID;
		internal static readonly IEntityRelation ContractLinkEntityUsingSourceContractIDStatic = new ContractRelations().ContractLinkEntityUsingSourceContractID;
		internal static readonly IEntityRelation ContractLinkEntityUsingTargetContractIDStatic = new ContractRelations().ContractLinkEntityUsingTargetContractID;
		internal static readonly IEntityRelation ContractTerminationEntityUsingContractIDStatic = new ContractRelations().ContractTerminationEntityUsingContractID;
		internal static readonly IEntityRelation ContractToPaymentMethodEntityUsingContractIDStatic = new ContractRelations().ContractToPaymentMethodEntityUsingContractID;
		internal static readonly IEntityRelation ContractToProviderEntityUsingContractIDStatic = new ContractRelations().ContractToProviderEntityUsingContractID;
		internal static readonly IEntityRelation ContractWorkItemEntityUsingContractIDStatic = new ContractRelations().ContractWorkItemEntityUsingContractID;
		internal static readonly IEntityRelation CustomerAccountEntityUsingContractIDStatic = new ContractRelations().CustomerAccountEntityUsingContractID;
		internal static readonly IEntityRelation DunningProcessEntityUsingContractIDStatic = new ContractRelations().DunningProcessEntityUsingContractID;
		internal static readonly IEntityRelation FareEvasionIncidentEntityUsingContractIDStatic = new ContractRelations().FareEvasionIncidentEntityUsingContractID;
		internal static readonly IEntityRelation InvoiceEntityUsingContractIDStatic = new ContractRelations().InvoiceEntityUsingContractID;
		internal static readonly IEntityRelation JobEntityUsingContractIDStatic = new ContractRelations().JobEntityUsingContractID;
		internal static readonly IEntityRelation JobBulkImportEntityUsingContractIDStatic = new ContractRelations().JobBulkImportEntityUsingContractID;
		internal static readonly IEntityRelation NotificationMessageOwnerEntityUsingContractIDStatic = new ContractRelations().NotificationMessageOwnerEntityUsingContractID;
		internal static readonly IEntityRelation OrderEntityUsingContractIDStatic = new ContractRelations().OrderEntityUsingContractID;
		internal static readonly IEntityRelation OrganizationParticipantEntityUsingContractIDStatic = new ContractRelations().OrganizationParticipantEntityUsingContractID;
		internal static readonly IEntityRelation ParticipantGroupEntityUsingContractIDStatic = new ContractRelations().ParticipantGroupEntityUsingContractID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingContractIDStatic = new ContractRelations().PaymentJournalEntityUsingContractID;
		internal static readonly IEntityRelation PaymentOptionEntityUsingContractIDStatic = new ContractRelations().PaymentOptionEntityUsingContractID;
		internal static readonly IEntityRelation PaymentProviderAccountEntityUsingContractIDStatic = new ContractRelations().PaymentProviderAccountEntityUsingContractID;
		internal static readonly IEntityRelation PhotographEntityUsingContractidStatic = new ContractRelations().PhotographEntityUsingContractid;
		internal static readonly IEntityRelation PostingEntityUsingContractIDStatic = new ContractRelations().PostingEntityUsingContractID;
		internal static readonly IEntityRelation ProductEntityUsingContractIDStatic = new ContractRelations().ProductEntityUsingContractID;
		internal static readonly IEntityRelation ProductTerminationEntityUsingContractIDStatic = new ContractRelations().ProductTerminationEntityUsingContractID;
		internal static readonly IEntityRelation SaleEntityUsingContractIDStatic = new ContractRelations().SaleEntityUsingContractID;
		internal static readonly IEntityRelation SaleEntityUsingSecondaryContractIDStatic = new ContractRelations().SaleEntityUsingSecondaryContractID;
		internal static readonly IEntityRelation StatementOfAccountEntityUsingContractIDStatic = new ContractRelations().StatementOfAccountEntityUsingContractID;
		internal static readonly IEntityRelation TicketAssignmentEntityUsingContractIDStatic = new ContractRelations().TicketAssignmentEntityUsingContractID;
		internal static readonly IEntityRelation PaymentModalityEntityUsingPaymentModalityIDStatic = new ContractRelations().PaymentModalityEntityUsingPaymentModalityID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new ContractRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation ClientEntityUsingVanpoolClientIDStatic = new ContractRelations().ClientEntityUsingVanpoolClientID;
		internal static readonly IEntityRelation AccountingModeEntityUsingAccountingModeIDStatic = new ContractRelations().AccountingModeEntityUsingAccountingModeID;
		internal static readonly IEntityRelation OrganizationEntityUsingOrganizationIDStatic = new ContractRelations().OrganizationEntityUsingOrganizationID;

		/// <summary>CTor</summary>
		static StaticContractRelations()
		{
		}
	}
}
