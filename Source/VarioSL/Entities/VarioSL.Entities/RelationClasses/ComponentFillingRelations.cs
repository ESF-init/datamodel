﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ComponentFilling. </summary>
	public partial class ComponentFillingRelations
	{
		/// <summary>CTor</summary>
		public ComponentFillingRelations()
		{
		}

		/// <summary>Gets all relations of the ComponentFillingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.AutomatEntityUsingAutomatID);
			toReturn.Add(this.CashUnitEntityUsingCashUnitID);
			toReturn.Add(this.ComponentEntityUsingComponentID);
			toReturn.Add(this.DebtorEntityUsingMaintenanceStaffID);
			toReturn.Add(this.DebtorEntityUsingPersonnelFillingID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ComponentFillingEntity and AutomatEntity over the m:1 relation they have, using the relation between the fields:
		/// ComponentFilling.AutomatID - Automat.AutomatID
		/// </summary>
		public virtual IEntityRelation AutomatEntityUsingAutomatID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Automat", false);
				relation.AddEntityFieldPair(AutomatFields.AutomatID, ComponentFillingFields.AutomatID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("AutomatEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentFillingEntity and CashUnitEntity over the m:1 relation they have, using the relation between the fields:
		/// ComponentFilling.CashUnitID - CashUnit.CashUnitID
		/// </summary>
		public virtual IEntityRelation CashUnitEntityUsingCashUnitID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CashUnit", false);
				relation.AddEntityFieldPair(CashUnitFields.CashUnitID, ComponentFillingFields.CashUnitID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CashUnitEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentFillingEntity and ComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// ComponentFilling.ComponentID - Component.ComponentID
		/// </summary>
		public virtual IEntityRelation ComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Component", false);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, ComponentFillingFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentFillingEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// ComponentFilling.MaintenanceStaffID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingMaintenanceStaffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "MaintenanceStaff", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentFillingFields.MaintenanceStaffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ComponentFillingEntity and DebtorEntity over the m:1 relation they have, using the relation between the fields:
		/// ComponentFilling.PersonnelFillingID - Debtor.DebtorID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingPersonnelFillingID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PersonnelFilling", false);
				relation.AddEntityFieldPair(DebtorFields.DebtorID, ComponentFillingFields.PersonnelFillingID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentFillingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticComponentFillingRelations
	{
		internal static readonly IEntityRelation AutomatEntityUsingAutomatIDStatic = new ComponentFillingRelations().AutomatEntityUsingAutomatID;
		internal static readonly IEntityRelation CashUnitEntityUsingCashUnitIDStatic = new ComponentFillingRelations().CashUnitEntityUsingCashUnitID;
		internal static readonly IEntityRelation ComponentEntityUsingComponentIDStatic = new ComponentFillingRelations().ComponentEntityUsingComponentID;
		internal static readonly IEntityRelation DebtorEntityUsingMaintenanceStaffIDStatic = new ComponentFillingRelations().DebtorEntityUsingMaintenanceStaffID;
		internal static readonly IEntityRelation DebtorEntityUsingPersonnelFillingIDStatic = new ComponentFillingRelations().DebtorEntityUsingPersonnelFillingID;

		/// <summary>CTor</summary>
		static StaticComponentFillingRelations()
		{
		}
	}
}
