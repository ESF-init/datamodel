﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Depot. </summary>
	public partial class DepotRelations
	{
		/// <summary>CTor</summary>
		public DepotRelations()
		{
		}

		/// <summary>Gets all relations of the DepotEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DebtorEntityUsingDepotID);
			toReturn.Add(this.UnitEntityUsingDepotID);
			toReturn.Add(this.UserListEntityUsingDepotID);
			toReturn.Add(this.VarioSettlementEntityUsingDepotID);
			toReturn.Add(this.VarioAddressEntityUsingAddressID);
			toReturn.Add(this.ClientEntityUsingClientID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and DebtorEntity over the 1:n relation they have, using the relation between the fields:
		/// Depot.DepotID - Debtor.DepotID
		/// </summary>
		public virtual IEntityRelation DebtorEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Debtors" , true);
				relation.AddEntityFieldPair(DepotFields.DepotID, DebtorFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DebtorEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and UnitEntity over the 1:n relation they have, using the relation between the fields:
		/// Depot.DepotID - Unit.DepotID
		/// </summary>
		public virtual IEntityRelation UnitEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Units" , true);
				relation.AddEntityFieldPair(DepotFields.DepotID, UnitFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and UserListEntity over the 1:n relation they have, using the relation between the fields:
		/// Depot.DepotID - UserList.DepotID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UserLists" , true);
				relation.AddEntityFieldPair(DepotFields.DepotID, UserListFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and VarioSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// Depot.DepotID - VarioSettlement.DepotID
		/// </summary>
		public virtual IEntityRelation VarioSettlementEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Settlements" , true);
				relation.AddEntityFieldPair(DepotFields.DepotID, VarioSettlementFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and VarioAddressEntity over the 1:1 relation they have, using the relation between the fields:
		/// Depot.AddressID - VarioAddress.AddressID
		/// </summary>
		public virtual IEntityRelation VarioAddressEntityUsingAddressID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "Address", false);




				relation.AddEntityFieldPair(VarioAddressFields.AddressID, DepotFields.AddressID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioAddressEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DepotEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// Depot.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, DepotFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDepotRelations
	{
		internal static readonly IEntityRelation DebtorEntityUsingDepotIDStatic = new DepotRelations().DebtorEntityUsingDepotID;
		internal static readonly IEntityRelation UnitEntityUsingDepotIDStatic = new DepotRelations().UnitEntityUsingDepotID;
		internal static readonly IEntityRelation UserListEntityUsingDepotIDStatic = new DepotRelations().UserListEntityUsingDepotID;
		internal static readonly IEntityRelation VarioSettlementEntityUsingDepotIDStatic = new DepotRelations().VarioSettlementEntityUsingDepotID;
		internal static readonly IEntityRelation VarioAddressEntityUsingAddressIDStatic = new DepotRelations().VarioAddressEntityUsingAddressID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new DepotRelations().ClientEntityUsingClientID;

		/// <summary>CTor</summary>
		static StaticDepotRelations()
		{
		}
	}
}
