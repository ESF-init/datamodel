﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PredefinedKey. </summary>
	public partial class PredefinedKeyRelations
	{
		/// <summary>CTor</summary>
		public PredefinedKeyRelations()
		{
		}

		/// <summary>Gets all relations of the PredefinedKeyEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.KeyAttributeTransfromEntityUsingKeyID);
			toReturn.Add(this.PrimalKeyEntityUsingKeyID);
			toReturn.Add(this.PanelEntityUsingPanelID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between PredefinedKeyEntity and KeyAttributeTransfromEntity over the 1:n relation they have, using the relation between the fields:
		/// PredefinedKey.KeyID - KeyAttributeTransfrom.KeyID
		/// </summary>
		public virtual IEntityRelation KeyAttributeTransfromEntityUsingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "KeyAttributeTransform" , true);
				relation.AddEntityFieldPair(PredefinedKeyFields.KeyID, KeyAttributeTransfromFields.KeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("KeyAttributeTransfromEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between PredefinedKeyEntity and PrimalKeyEntity over the 1:n relation they have, using the relation between the fields:
		/// PredefinedKey.KeyID - PrimalKey.KeyID
		/// </summary>
		public virtual IEntityRelation PrimalKeyEntityUsingKeyID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PrimalKeys" , true);
				relation.AddEntityFieldPair(PredefinedKeyFields.KeyID, PrimalKeyFields.KeyID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PrimalKeyEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between PredefinedKeyEntity and PanelEntity over the m:1 relation they have, using the relation between the fields:
		/// PredefinedKey.PanelID - Panel.PanelID
		/// </summary>
		public virtual IEntityRelation PanelEntityUsingPanelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Panel", false);
				relation.AddEntityFieldPair(PanelFields.PanelID, PredefinedKeyFields.PanelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PredefinedKeyEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// PredefinedKey.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, PredefinedKeyFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PredefinedKeyEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPredefinedKeyRelations
	{
		internal static readonly IEntityRelation KeyAttributeTransfromEntityUsingKeyIDStatic = new PredefinedKeyRelations().KeyAttributeTransfromEntityUsingKeyID;
		internal static readonly IEntityRelation PrimalKeyEntityUsingKeyIDStatic = new PredefinedKeyRelations().PrimalKeyEntityUsingKeyID;
		internal static readonly IEntityRelation PanelEntityUsingPanelIDStatic = new PredefinedKeyRelations().PanelEntityUsingPanelID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new PredefinedKeyRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticPredefinedKeyRelations()
		{
		}
	}
}
