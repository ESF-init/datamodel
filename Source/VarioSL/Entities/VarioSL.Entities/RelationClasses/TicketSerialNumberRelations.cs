﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketSerialNumber. </summary>
	public partial class TicketSerialNumberRelations
	{
		/// <summary>CTor</summary>
		public TicketSerialNumberRelations()
		{
		}

		/// <summary>Gets all relations of the TicketSerialNumberEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.TicketEntityUsingTicketID);
			toReturn.Add(this.TicketTypeEntityUsingTicketTypeID);
			toReturn.Add(this.ProductEntityUsingProductID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between TicketSerialNumberEntity and TicketEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketSerialNumber.TicketID - Ticket.TicketID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingTicketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Ticket", false);
				relation.AddEntityFieldPair(TicketFields.TicketID, TicketSerialNumberFields.TicketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketSerialNumberEntity and TicketTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketSerialNumber.TicketTypeID - TicketType.TicketTypeID
		/// </summary>
		public virtual IEntityRelation TicketTypeEntityUsingTicketTypeID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TicketType", false);
				relation.AddEntityFieldPair(TicketTypeFields.TicketTypeID, TicketSerialNumberFields.TicketTypeID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between TicketSerialNumberEntity and ProductEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketSerialNumber.ProductID - Product.ProductID
		/// </summary>
		public virtual IEntityRelation ProductEntityUsingProductID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Product", false);
				relation.AddEntityFieldPair(ProductFields.ProductID, TicketSerialNumberFields.ProductID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ProductEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketSerialNumberEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketSerialNumberRelations
	{
		internal static readonly IEntityRelation TicketEntityUsingTicketIDStatic = new TicketSerialNumberRelations().TicketEntityUsingTicketID;
		internal static readonly IEntityRelation TicketTypeEntityUsingTicketTypeIDStatic = new TicketSerialNumberRelations().TicketTypeEntityUsingTicketTypeID;
		internal static readonly IEntityRelation ProductEntityUsingProductIDStatic = new TicketSerialNumberRelations().ProductEntityUsingProductID;

		/// <summary>CTor</summary>
		static StaticTicketSerialNumberRelations()
		{
		}
	}
}
