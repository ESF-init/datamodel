﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SurveyChoice. </summary>
	public partial class SurveyChoiceRelations
	{
		/// <summary>CTor</summary>
		public SurveyChoiceRelations()
		{
		}

		/// <summary>Gets all relations of the SurveyChoiceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.SurveyAnswerEntityUsingSurveyChoiceID);
			toReturn.Add(this.SurveyQuestionEntityUsingFollowUpQuestionID);
			toReturn.Add(this.SurveyQuestionEntityUsingSurveyQuestionID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between SurveyChoiceEntity and SurveyAnswerEntity over the 1:n relation they have, using the relation between the fields:
		/// SurveyChoice.SurveyChoiceID - SurveyAnswer.SurveyChoiceID
		/// </summary>
		public virtual IEntityRelation SurveyAnswerEntityUsingSurveyChoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SurveyAnswers" , true);
				relation.AddEntityFieldPair(SurveyChoiceFields.SurveyChoiceID, SurveyAnswerFields.SurveyChoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyAnswerEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between SurveyChoiceEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyChoice.FollowUpQuestionID - SurveyQuestion.SurveyQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingFollowUpQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "FollowUpQuestion", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyChoiceFields.FollowUpQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SurveyChoiceEntity and SurveyQuestionEntity over the m:1 relation they have, using the relation between the fields:
		/// SurveyChoice.SurveyQuestionID - SurveyQuestion.SurveyQuestionID
		/// </summary>
		public virtual IEntityRelation SurveyQuestionEntityUsingSurveyQuestionID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SurveyQuestion", false);
				relation.AddEntityFieldPair(SurveyQuestionFields.SurveyQuestionID, SurveyChoiceFields.SurveyQuestionID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyQuestionEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SurveyChoiceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSurveyChoiceRelations
	{
		internal static readonly IEntityRelation SurveyAnswerEntityUsingSurveyChoiceIDStatic = new SurveyChoiceRelations().SurveyAnswerEntityUsingSurveyChoiceID;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingFollowUpQuestionIDStatic = new SurveyChoiceRelations().SurveyQuestionEntityUsingFollowUpQuestionID;
		internal static readonly IEntityRelation SurveyQuestionEntityUsingSurveyQuestionIDStatic = new SurveyChoiceRelations().SurveyQuestionEntityUsingSurveyQuestionID;

		/// <summary>CTor</summary>
		static StaticSurveyChoiceRelations()
		{
		}
	}
}
