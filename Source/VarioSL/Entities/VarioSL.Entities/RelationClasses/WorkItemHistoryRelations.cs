﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: WorkItemHistory. </summary>
	public partial class WorkItemHistoryRelations
	{
		/// <summary>CTor</summary>
		public WorkItemHistoryRelations()
		{
		}

		/// <summary>Gets all relations of the WorkItemHistoryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.WorkItemEntityUsingWorkItemID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between WorkItemHistoryEntity and WorkItemEntity over the m:1 relation they have, using the relation between the fields:
		/// WorkItemHistory.WorkItemID - WorkItem.WorkItemID
		/// </summary>
		public virtual IEntityRelation WorkItemEntityUsingWorkItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WorkItem", false);
				relation.AddEntityFieldPair(WorkItemFields.WorkItemID, WorkItemHistoryFields.WorkItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemHistoryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWorkItemHistoryRelations
	{
		internal static readonly IEntityRelation WorkItemEntityUsingWorkItemIDStatic = new WorkItemHistoryRelations().WorkItemEntityUsingWorkItemID;

		/// <summary>CTor</summary>
		static StaticWorkItemHistoryRelations()
		{
		}
	}
}
