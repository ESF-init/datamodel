﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DunningToInvoice. </summary>
	public partial class DunningToInvoiceRelations
	{
		/// <summary>CTor</summary>
		public DunningToInvoiceRelations()
		{
		}

		/// <summary>Gets all relations of the DunningToInvoiceEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.DunningLevelConfigurationEntityUsingDunningLevelID);
			toReturn.Add(this.DunningProcessEntityUsingDunningProcessID);
			toReturn.Add(this.InvoiceEntityUsingInvoiceID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between DunningToInvoiceEntity and DunningLevelConfigurationEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToInvoice.DunningLevelID - DunningLevelConfiguration.DunningLevelID
		/// </summary>
		public virtual IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DunningLevelConfiguration", false);
				relation.AddEntityFieldPair(DunningLevelConfigurationFields.DunningLevelID, DunningToInvoiceFields.DunningLevelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningLevelConfigurationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningToInvoiceEntity and DunningProcessEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToInvoice.DunningProcessID - DunningProcess.DunningProcessID
		/// </summary>
		public virtual IEntityRelation DunningProcessEntityUsingDunningProcessID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "DunningProcess", false);
				relation.AddEntityFieldPair(DunningProcessFields.DunningProcessID, DunningToInvoiceFields.DunningProcessID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningProcessEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between DunningToInvoiceEntity and InvoiceEntity over the m:1 relation they have, using the relation between the fields:
		/// DunningToInvoice.InvoiceID - Invoice.InvoiceID
		/// </summary>
		public virtual IEntityRelation InvoiceEntityUsingInvoiceID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Invoice", false);
				relation.AddEntityFieldPair(InvoiceFields.InvoiceID, DunningToInvoiceFields.InvoiceID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("InvoiceEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DunningToInvoiceEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDunningToInvoiceRelations
	{
		internal static readonly IEntityRelation DunningLevelConfigurationEntityUsingDunningLevelIDStatic = new DunningToInvoiceRelations().DunningLevelConfigurationEntityUsingDunningLevelID;
		internal static readonly IEntityRelation DunningProcessEntityUsingDunningProcessIDStatic = new DunningToInvoiceRelations().DunningProcessEntityUsingDunningProcessID;
		internal static readonly IEntityRelation InvoiceEntityUsingInvoiceIDStatic = new DunningToInvoiceRelations().InvoiceEntityUsingInvoiceID;

		/// <summary>CTor</summary>
		static StaticDunningToInvoiceRelations()
		{
		}
	}
}
