﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PaymentJournalToComponent. </summary>
	public partial class PaymentJournalToComponentRelations
	{
		/// <summary>CTor</summary>
		public PaymentJournalToComponentRelations()
		{
		}

		/// <summary>Gets all relations of the PaymentJournalToComponentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ComponentEntityUsingComponentID);
			toReturn.Add(this.PaymentJournalEntityUsingPaymentjournalID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PaymentJournalToComponentEntity and ComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournalToComponent.ComponentID - Component.ComponentID
		/// </summary>
		public virtual IEntityRelation ComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Component", false);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, PaymentJournalToComponentFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalToComponentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PaymentJournalToComponentEntity and PaymentJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// PaymentJournalToComponent.PaymentjournalID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingPaymentjournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentJournal", false);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, PaymentJournalToComponentFields.PaymentjournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalToComponentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPaymentJournalToComponentRelations
	{
		internal static readonly IEntityRelation ComponentEntityUsingComponentIDStatic = new PaymentJournalToComponentRelations().ComponentEntityUsingComponentID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingPaymentjournalIDStatic = new PaymentJournalToComponentRelations().PaymentJournalEntityUsingPaymentjournalID;

		/// <summary>CTor</summary>
		static StaticPaymentJournalToComponentRelations()
		{
		}
	}
}
