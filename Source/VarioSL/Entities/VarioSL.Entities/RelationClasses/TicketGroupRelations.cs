﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: TicketGroup. </summary>
	public partial class TicketGroupRelations
	{
		/// <summary>CTor</summary>
		public TicketGroupRelations()
		{
		}

		/// <summary>Gets all relations of the TicketGroupEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.FareMatrixEntryEntityUsingTicketGroupID);
			toReturn.Add(this.TicketToGroupEntityUsingTicketGroupID);
			toReturn.Add(this.TariffEntityUsingTariffID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between TicketGroupEntity and FareMatrixEntryEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketGroup.TicketGroupID - FareMatrixEntry.TicketGroupID
		/// </summary>
		public virtual IEntityRelation FareMatrixEntryEntityUsingTicketGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "FareMatrixEntry" , true);
				relation.AddEntityFieldPair(TicketGroupFields.TicketGroupID, FareMatrixEntryFields.TicketGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("FareMatrixEntryEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between TicketGroupEntity and TicketToGroupEntity over the 1:n relation they have, using the relation between the fields:
		/// TicketGroup.TicketGroupID - TicketToGroup.TicketGroupID
		/// </summary>
		public virtual IEntityRelation TicketToGroupEntityUsingTicketGroupID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketToGroups" , true);
				relation.AddEntityFieldPair(TicketGroupFields.TicketGroupID, TicketToGroupFields.TicketGroupID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketToGroupEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between TicketGroupEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// TicketGroup.TariffID - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, TicketGroupFields.TariffID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketGroupEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticTicketGroupRelations
	{
		internal static readonly IEntityRelation FareMatrixEntryEntityUsingTicketGroupIDStatic = new TicketGroupRelations().FareMatrixEntryEntityUsingTicketGroupID;
		internal static readonly IEntityRelation TicketToGroupEntityUsingTicketGroupIDStatic = new TicketGroupRelations().TicketToGroupEntityUsingTicketGroupID;
		internal static readonly IEntityRelation TariffEntityUsingTariffIDStatic = new TicketGroupRelations().TariffEntityUsingTariffID;

		/// <summary>CTor</summary>
		static StaticTicketGroupRelations()
		{
		}
	}
}
