﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: WorkItem. </summary>
	public partial class WorkItemRelations : IRelationFactory
	{
		/// <summary>CTor</summary>
		public WorkItemRelations()
		{
		}

		/// <summary>Gets all relations of the WorkItemEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.WorkItemHistoryEntityUsingWorkItemID);
			toReturn.Add(this.UserListEntityUsingAssignee);
			toReturn.Add(this.WorkItemSubjectEntityUsingWorkItemSubjectID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between WorkItemEntity and WorkItemHistoryEntity over the 1:n relation they have, using the relation between the fields:
		/// WorkItem.WorkItemID - WorkItemHistory.WorkItemID
		/// </summary>
		public virtual IEntityRelation WorkItemHistoryEntityUsingWorkItemID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "WorkItemHistory" , true);
				relation.AddEntityFieldPair(WorkItemFields.WorkItemID, WorkItemHistoryFields.WorkItemID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemHistoryEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between WorkItemEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// WorkItem.Assignee - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingAssignee
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "UserList", false);
				relation.AddEntityFieldPair(UserListFields.UserID, WorkItemFields.Assignee);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between WorkItemEntity and WorkItemSubjectEntity over the m:1 relation they have, using the relation between the fields:
		/// WorkItem.WorkItemSubjectID - WorkItemSubject.WorkItemSubjectID
		/// </summary>
		public virtual IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "WorkItemSubject", false);
				relation.AddEntityFieldPair(WorkItemSubjectFields.WorkItemSubjectID, WorkItemFields.WorkItemSubjectID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemSubjectEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("WorkItemEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticWorkItemRelations
	{
		internal static readonly IEntityRelation WorkItemHistoryEntityUsingWorkItemIDStatic = new WorkItemRelations().WorkItemHistoryEntityUsingWorkItemID;
		internal static readonly IEntityRelation UserListEntityUsingAssigneeStatic = new WorkItemRelations().UserListEntityUsingAssignee;
		internal static readonly IEntityRelation WorkItemSubjectEntityUsingWorkItemSubjectIDStatic = new WorkItemRelations().WorkItemSubjectEntityUsingWorkItemSubjectID;

		/// <summary>CTor</summary>
		static StaticWorkItemRelations()
		{
		}
	}
}
