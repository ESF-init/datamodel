﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: VarioSettlement. </summary>
	public partial class VarioSettlementRelations
	{
		/// <summary>CTor</summary>
		public VarioSettlementRelations()
		{
		}

		/// <summary>Gets all relations of the VarioSettlementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PostingEntityUsingAccountSettlementId);
			toReturn.Add(this.SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.DepotEntityUsingDepotID);
			toReturn.Add(this.UserListEntityUsingUserID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between VarioSettlementEntity and PostingEntity over the 1:n relation they have, using the relation between the fields:
		/// VarioSettlement.ID - Posting.AccountSettlementId
		/// </summary>
		public virtual IEntityRelation PostingEntityUsingAccountSettlementId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Postings" , true);
				relation.AddEntityFieldPair(VarioSettlementFields.ID, PostingFields.AccountSettlementId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostingEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between VarioSettlementEntity and SettlementRunValueToVarioSettlementEntity over the 1:n relation they have, using the relation between the fields:
		/// VarioSettlement.ID - SettlementRunValueToVarioSettlement.VarioSettlementID
		/// </summary>
		public virtual IEntityRelation SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettlementRunValueToVarioSettlements" , true);
				relation.AddEntityFieldPair(VarioSettlementFields.ID, SettlementRunValueToVarioSettlementFields.VarioSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettlementRunValueToVarioSettlementEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between VarioSettlementEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// VarioSettlement.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, VarioSettlementFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VarioSettlementEntity and DepotEntity over the m:1 relation they have, using the relation between the fields:
		/// VarioSettlement.DepotID - Depot.DepotID
		/// </summary>
		public virtual IEntityRelation DepotEntityUsingDepotID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Depot", false);
				relation.AddEntityFieldPair(DepotFields.DepotID, VarioSettlementFields.DepotID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DepotEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between VarioSettlementEntity and UserListEntity over the m:1 relation they have, using the relation between the fields:
		/// VarioSettlement.UserID - UserList.UserID
		/// </summary>
		public virtual IEntityRelation UserListEntityUsingUserID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "User", false);
				relation.AddEntityFieldPair(UserListFields.UserID, VarioSettlementFields.UserID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UserListEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("VarioSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticVarioSettlementRelations
	{
		internal static readonly IEntityRelation PostingEntityUsingAccountSettlementIdStatic = new VarioSettlementRelations().PostingEntityUsingAccountSettlementId;
		internal static readonly IEntityRelation SettlementRunValueToVarioSettlementEntityUsingVarioSettlementIDStatic = new VarioSettlementRelations().SettlementRunValueToVarioSettlementEntityUsingVarioSettlementID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new VarioSettlementRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation DepotEntityUsingDepotIDStatic = new VarioSettlementRelations().DepotEntityUsingDepotID;
		internal static readonly IEntityRelation UserListEntityUsingUserIDStatic = new VarioSettlementRelations().UserListEntityUsingUserID;

		/// <summary>CTor</summary>
		static StaticVarioSettlementRelations()
		{
		}
	}
}
