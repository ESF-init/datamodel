﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: CardLink. </summary>
	public partial class CardLinkRelations
	{
		/// <summary>CTor</summary>
		public CardLinkRelations()
		{
		}

		/// <summary>Gets all relations of the CardLinkEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CardEntityUsingTargetCardID);
			toReturn.Add(this.CardEntityUsingSourceCardID);
			return toReturn;
		}

		#region Class Property Declarations


		/// <summary>Returns a new IEntityRelation object, between CardLinkEntity and CardEntity over the 1:1 relation they have, using the relation between the fields:
		/// CardLink.TargetCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingTargetCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToOne, "TargetCard", false);



				relation.AddEntityFieldPair(CardFields.CardID, CardLinkFields.TargetCardID);

				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardLinkEntity", true);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between CardLinkEntity and CardEntity over the m:1 relation they have, using the relation between the fields:
		/// CardLink.SourceCardID - Card.CardID
		/// </summary>
		public virtual IEntityRelation CardEntityUsingSourceCardID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "SourceCard", false);
				relation.AddEntityFieldPair(CardFields.CardID, CardLinkFields.SourceCardID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CardLinkEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCardLinkRelations
	{
		internal static readonly IEntityRelation CardEntityUsingTargetCardIDStatic = new CardLinkRelations().CardEntityUsingTargetCardID;
		internal static readonly IEntityRelation CardEntityUsingSourceCardIDStatic = new CardLinkRelations().CardEntityUsingSourceCardID;

		/// <summary>CTor</summary>
		static StaticCardLinkRelations()
		{
		}
	}
}
