﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RevenueSettlement. </summary>
	public partial class RevenueSettlementRelations
	{
		/// <summary>CTor</summary>
		public RevenueSettlementRelations()
		{
		}

		/// <summary>Gets all relations of the RevenueSettlementEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RevenueRecognitionEntityUsingRevenueSettlementID);
			toReturn.Add(this.SettledRevenueEntityUsingRevenueSettlementID);
			toReturn.Add(this.ClientEntityUsingClientID);
			toReturn.Add(this.CloseoutPeriodEntityUsingCloseoutPeriodID);
			toReturn.Add(this.TransactionJournalEntityUsingTransactionJournalID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between RevenueSettlementEntity and RevenueRecognitionEntity over the 1:n relation they have, using the relation between the fields:
		/// RevenueSettlement.RevenueSettlementID - RevenueRecognition.RevenueSettlementID
		/// </summary>
		public virtual IEntityRelation RevenueRecognitionEntityUsingRevenueSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RevenueRecognition" , true);
				relation.AddEntityFieldPair(RevenueSettlementFields.RevenueSettlementID, RevenueRecognitionFields.RevenueSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueRecognitionEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between RevenueSettlementEntity and SettledRevenueEntity over the 1:n relation they have, using the relation between the fields:
		/// RevenueSettlement.RevenueSettlementID - SettledRevenue.RevenueSettlementID
		/// </summary>
		public virtual IEntityRelation SettledRevenueEntityUsingRevenueSettlementID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SettledRevenues" , true);
				relation.AddEntityFieldPair(RevenueSettlementFields.RevenueSettlementID, SettledRevenueFields.RevenueSettlementID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SettledRevenueEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between RevenueSettlementEntity and ClientEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueSettlement.ClientID - Client.ClientID
		/// </summary>
		public virtual IEntityRelation ClientEntityUsingClientID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Client", false);
				relation.AddEntityFieldPair(ClientFields.ClientID, RevenueSettlementFields.ClientID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClientEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevenueSettlementEntity and CloseoutPeriodEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueSettlement.CloseoutPeriodID - CloseoutPeriod.CloseoutPeriodID
		/// </summary>
		public virtual IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "CloseoutPeriod", false);
				relation.AddEntityFieldPair(CloseoutPeriodFields.CloseoutPeriodID, RevenueSettlementFields.CloseoutPeriodID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CloseoutPeriodEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RevenueSettlementEntity and TransactionJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// RevenueSettlement.TransactionJournalID - TransactionJournal.TransactionJournalID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingTransactionJournalID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "TransactionJournal", false);
				relation.AddEntityFieldPair(TransactionJournalFields.TransactionJournalID, RevenueSettlementFields.TransactionJournalID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RevenueSettlementEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRevenueSettlementRelations
	{
		internal static readonly IEntityRelation RevenueRecognitionEntityUsingRevenueSettlementIDStatic = new RevenueSettlementRelations().RevenueRecognitionEntityUsingRevenueSettlementID;
		internal static readonly IEntityRelation SettledRevenueEntityUsingRevenueSettlementIDStatic = new RevenueSettlementRelations().SettledRevenueEntityUsingRevenueSettlementID;
		internal static readonly IEntityRelation ClientEntityUsingClientIDStatic = new RevenueSettlementRelations().ClientEntityUsingClientID;
		internal static readonly IEntityRelation CloseoutPeriodEntityUsingCloseoutPeriodIDStatic = new RevenueSettlementRelations().CloseoutPeriodEntityUsingCloseoutPeriodID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingTransactionJournalIDStatic = new RevenueSettlementRelations().TransactionJournalEntityUsingTransactionJournalID;

		/// <summary>CTor</summary>
		static StaticRevenueSettlementRelations()
		{
		}
	}
}
