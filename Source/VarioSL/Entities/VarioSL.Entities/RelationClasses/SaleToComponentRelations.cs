﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: SaleToComponent. </summary>
	public partial class SaleToComponentRelations
	{
		/// <summary>CTor</summary>
		public SaleToComponentRelations()
		{
		}

		/// <summary>Gets all relations of the SaleToComponentEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ComponentEntityUsingComponentID);
			toReturn.Add(this.PaymentJournalEntityUsingSaleID);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between SaleToComponentEntity and ComponentEntity over the m:1 relation they have, using the relation between the fields:
		/// SaleToComponent.ComponentID - Component.ComponentID
		/// </summary>
		public virtual IEntityRelation ComponentEntityUsingComponentID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Component", false);
				relation.AddEntityFieldPair(ComponentFields.ComponentID, SaleToComponentFields.ComponentID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ComponentEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleToComponentEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between SaleToComponentEntity and PaymentJournalEntity over the m:1 relation they have, using the relation between the fields:
		/// SaleToComponent.SaleID - PaymentJournal.PaymentJournalID
		/// </summary>
		public virtual IEntityRelation PaymentJournalEntityUsingSaleID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "PaymentJournal", false);
				relation.AddEntityFieldPair(PaymentJournalFields.PaymentJournalID, SaleToComponentFields.SaleID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PaymentJournalEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleToComponentEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticSaleToComponentRelations
	{
		internal static readonly IEntityRelation ComponentEntityUsingComponentIDStatic = new SaleToComponentRelations().ComponentEntityUsingComponentID;
		internal static readonly IEntityRelation PaymentJournalEntityUsingSaleIDStatic = new SaleToComponentRelations().PaymentJournalEntityUsingSaleID;

		/// <summary>CTor</summary>
		static StaticSaleToComponentRelations()
		{
		}
	}
}
