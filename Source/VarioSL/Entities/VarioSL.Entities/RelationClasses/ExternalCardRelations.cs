﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: ExternalCard. </summary>
	public partial class ExternalCardRelations
	{
		/// <summary>CTor</summary>
		public ExternalCardRelations()
		{
		}

		/// <summary>Gets all relations of the ExternalCardEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ExternalEffortEntityUsingEffortID);
			toReturn.Add(this.ExternalPacketEntityUsingPacketID);
			toReturn.Add(this.ExternalTypeEntityUsingType);
			toReturn.Add(this.TariffEntityUsingTariffId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between ExternalCardEntity and ExternalEffortEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalCard.EffortID - ExternalEffort.EffortID
		/// </summary>
		public virtual IEntityRelation ExternalEffortEntityUsingEffortID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalEffort", false);
				relation.AddEntityFieldPair(ExternalEffortFields.EffortID, ExternalCardFields.EffortID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalEffortEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalCardEntity and ExternalPacketEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalCard.PacketID - ExternalPacket.PacketID
		/// </summary>
		public virtual IEntityRelation ExternalPacketEntityUsingPacketID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalPacket", false);
				relation.AddEntityFieldPair(ExternalPacketFields.PacketID, ExternalCardFields.PacketID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalPacketEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalCardEntity and ExternalTypeEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalCard.Type - ExternalType.TypeID
		/// </summary>
		public virtual IEntityRelation ExternalTypeEntityUsingType
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "ExternalType", false);
				relation.AddEntityFieldPair(ExternalTypeFields.TypeID, ExternalCardFields.Type);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalTypeEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between ExternalCardEntity and TariffEntity over the m:1 relation they have, using the relation between the fields:
		/// ExternalCard.TariffId - Tariff.TarifID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingTariffId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Tariff", false);
				relation.AddEntityFieldPair(TariffFields.TarifID, ExternalCardFields.TariffId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ExternalCardEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticExternalCardRelations
	{
		internal static readonly IEntityRelation ExternalEffortEntityUsingEffortIDStatic = new ExternalCardRelations().ExternalEffortEntityUsingEffortID;
		internal static readonly IEntityRelation ExternalPacketEntityUsingPacketIDStatic = new ExternalCardRelations().ExternalPacketEntityUsingPacketID;
		internal static readonly IEntityRelation ExternalTypeEntityUsingTypeStatic = new ExternalCardRelations().ExternalTypeEntityUsingType;
		internal static readonly IEntityRelation TariffEntityUsingTariffIdStatic = new ExternalCardRelations().TariffEntityUsingTariffId;

		/// <summary>CTor</summary>
		static StaticExternalCardRelations()
		{
		}
	}
}
