﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using VarioSL.Entities;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: DeviceClass. </summary>
	public partial class DeviceClassRelations
	{
		/// <summary>CTor</summary>
		public DeviceClassRelations()
		{
		}

		/// <summary>Gets all relations of the DeviceClassEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.ClearingResultEntityUsingDeviceClassID);
			toReturn.Add(this.DeviceEntityUsingDeviceClassID);
			toReturn.Add(this.GuiDefEntityUsingDeviceClassID);
			toReturn.Add(this.PanelEntityUsingDeviceClassID);
			toReturn.Add(this.SpecialReceiptEntityUsingDeviceClassID);
			toReturn.Add(this.TariffEntityUsingDeviceClassID);
			toReturn.Add(this.TariffReleaseEntityUsingDeviceClassID);
			toReturn.Add(this.TicketEntityUsingDeviceClassID);
			toReturn.Add(this.TicketDeviceClassEntityUsingDeviceClassID);
			toReturn.Add(this.ParameterEntityUsingDeviceClassID);
			toReturn.Add(this.ParameterArchiveReleaseEntityUsingDeviceClassID);
			toReturn.Add(this.ParameterValueEntityUsingDeviceClassID);
			toReturn.Add(this.SaleEntityUsingSalesChannelID);
			toReturn.Add(this.SalesChannelToPaymentMethodEntityUsingDeviceClassID);
			toReturn.Add(this.TransactionJournalEntityUsingSalesChannelID);
			toReturn.Add(this.UnitCollectionEntityUsingDeviceClassID);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and ClearingResultEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - ClearingResult.DeviceClassID
		/// </summary>
		public virtual IEntityRelation ClearingResultEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ClearingResults" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ClearingResultFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ClearingResultEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and DeviceEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Device.DeviceClassID
		/// </summary>
		public virtual IEntityRelation DeviceEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UmDevices" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, DeviceFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and GuiDefEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - GuiDef.DeviceClassID
		/// </summary>
		public virtual IEntityRelation GuiDefEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "GuiDefs" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, GuiDefFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GuiDefEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and PanelEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Panel.DeviceClassID
		/// </summary>
		public virtual IEntityRelation PanelEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Panels" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, PanelFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PanelEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and SpecialReceiptEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - SpecialReceipt.DeviceClassID
		/// </summary>
		public virtual IEntityRelation SpecialReceiptEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SpecialReceipts" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, SpecialReceiptFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SpecialReceiptEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and TariffEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Tariff.DeviceClassID
		/// </summary>
		public virtual IEntityRelation TariffEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tariffs" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TariffFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and TariffReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - TariffRelease.DeviceClassID
		/// </summary>
		public virtual IEntityRelation TariffReleaseEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TariffRelease" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TariffReleaseFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TariffReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and TicketEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Ticket.DeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Tickets" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TicketFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and TicketDeviceClassEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - TicketDeviceClass.DeviceClassID
		/// </summary>
		public virtual IEntityRelation TicketDeviceClassEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TicketDeviceClasses" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TicketDeviceClassFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TicketDeviceClassEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and ParameterEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Parameter.DeviceClassID
		/// </summary>
		public virtual IEntityRelation ParameterEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Parameters" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and ParameterArchiveReleaseEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - ParameterArchiveRelease.DeviceClassID
		/// </summary>
		public virtual IEntityRelation ParameterArchiveReleaseEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterArchiveReleases" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterArchiveReleaseFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterArchiveReleaseEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and ParameterValueEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - ParameterValue.DeviceClassID
		/// </summary>
		public virtual IEntityRelation ParameterValueEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "ParameterValues" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, ParameterValueFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("ParameterValueEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and SaleEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - Sale.SalesChannelID
		/// </summary>
		public virtual IEntityRelation SaleEntityUsingSalesChannelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Sales" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, SaleFields.SalesChannelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SaleEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and SalesChannelToPaymentMethodEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - SalesChannelToPaymentMethod.DeviceClassID
		/// </summary>
		public virtual IEntityRelation SalesChannelToPaymentMethodEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "SalesChannelToPaymentMethods" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, SalesChannelToPaymentMethodFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("SalesChannelToPaymentMethodEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and TransactionJournalEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - TransactionJournal.SalesChannelID
		/// </summary>
		public virtual IEntityRelation TransactionJournalEntityUsingSalesChannelID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "TransactionJournals" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, TransactionJournalFields.SalesChannelID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("TransactionJournalEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between DeviceClassEntity and UnitCollectionEntity over the 1:n relation they have, using the relation between the fields:
		/// DeviceClass.DeviceClassID - UnitCollection.DeviceClassID
		/// </summary>
		public virtual IEntityRelation UnitCollectionEntityUsingDeviceClassID
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "UnitCollections" , true);
				relation.AddEntityFieldPair(DeviceClassFields.DeviceClassID, UnitCollectionFields.DeviceClassID);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("DeviceClassEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("UnitCollectionEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticDeviceClassRelations
	{
		internal static readonly IEntityRelation ClearingResultEntityUsingDeviceClassIDStatic = new DeviceClassRelations().ClearingResultEntityUsingDeviceClassID;
		internal static readonly IEntityRelation DeviceEntityUsingDeviceClassIDStatic = new DeviceClassRelations().DeviceEntityUsingDeviceClassID;
		internal static readonly IEntityRelation GuiDefEntityUsingDeviceClassIDStatic = new DeviceClassRelations().GuiDefEntityUsingDeviceClassID;
		internal static readonly IEntityRelation PanelEntityUsingDeviceClassIDStatic = new DeviceClassRelations().PanelEntityUsingDeviceClassID;
		internal static readonly IEntityRelation SpecialReceiptEntityUsingDeviceClassIDStatic = new DeviceClassRelations().SpecialReceiptEntityUsingDeviceClassID;
		internal static readonly IEntityRelation TariffEntityUsingDeviceClassIDStatic = new DeviceClassRelations().TariffEntityUsingDeviceClassID;
		internal static readonly IEntityRelation TariffReleaseEntityUsingDeviceClassIDStatic = new DeviceClassRelations().TariffReleaseEntityUsingDeviceClassID;
		internal static readonly IEntityRelation TicketEntityUsingDeviceClassIDStatic = new DeviceClassRelations().TicketEntityUsingDeviceClassID;
		internal static readonly IEntityRelation TicketDeviceClassEntityUsingDeviceClassIDStatic = new DeviceClassRelations().TicketDeviceClassEntityUsingDeviceClassID;
		internal static readonly IEntityRelation ParameterEntityUsingDeviceClassIDStatic = new DeviceClassRelations().ParameterEntityUsingDeviceClassID;
		internal static readonly IEntityRelation ParameterArchiveReleaseEntityUsingDeviceClassIDStatic = new DeviceClassRelations().ParameterArchiveReleaseEntityUsingDeviceClassID;
		internal static readonly IEntityRelation ParameterValueEntityUsingDeviceClassIDStatic = new DeviceClassRelations().ParameterValueEntityUsingDeviceClassID;
		internal static readonly IEntityRelation SaleEntityUsingSalesChannelIDStatic = new DeviceClassRelations().SaleEntityUsingSalesChannelID;
		internal static readonly IEntityRelation SalesChannelToPaymentMethodEntityUsingDeviceClassIDStatic = new DeviceClassRelations().SalesChannelToPaymentMethodEntityUsingDeviceClassID;
		internal static readonly IEntityRelation TransactionJournalEntityUsingSalesChannelIDStatic = new DeviceClassRelations().TransactionJournalEntityUsingSalesChannelID;
		internal static readonly IEntityRelation UnitCollectionEntityUsingDeviceClassIDStatic = new DeviceClassRelations().UnitCollectionEntityUsingDeviceClassID;

		/// <summary>CTor</summary>
		static StaticDeviceClassRelations()
		{
		}
	}
}
