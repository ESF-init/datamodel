﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.RelationClasses;
using VarioSL.Entities.DaoClasses;

using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.FactoryClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase : EntityFactoryCore
	{
		private readonly VarioSL.Entities.EntityType _typeOfEntity;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		public EntityFactoryBase(string entityName, VarioSL.Entities.EntityType typeOfEntity) : base(entityName)
		{
			_typeOfEntity = typeOfEntity;
		}

		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((VarioSL.Entities.EntityType)entityTypeValue);
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields object for the entity to create. </summary>
		/// <returns>Empty IEntityFields object.</returns>
		public override IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(ForEntityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity)
		{
			return (IEntityFactory)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(ForEntityName, fieldValues, entityFieldStartIndexesPerEntity) ?? this;
		}
						
		/// <summary>Creates a new entity collection for the entity of this factory.</summary>
		/// <returns>ready to use new entity collection, typed.</returns>
		public override IEntityCollection CreateEntityCollection()
		{
			return GeneralEntityCollectionFactory.Create(_typeOfEntity);
		}
	}
	
	/// <summary>Factory to create new, empty AbortedTransactionEntity objects.</summary>
	[Serializable]
	public partial class AbortedTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AbortedTransactionEntityFactory() : base("AbortedTransactionEntity", VarioSL.Entities.EntityType.AbortedTransactionEntity) { }

		/// <summary>Creates a new, empty AbortedTransactionEntity object.</summary>
		/// <returns>A new, empty AbortedTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AbortedTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAbortedTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountBalanceEntity objects.</summary>
	[Serializable]
	public partial class AccountBalanceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountBalanceEntityFactory() : base("AccountBalanceEntity", VarioSL.Entities.EntityType.AccountBalanceEntity) { }

		/// <summary>Creates a new, empty AccountBalanceEntity object.</summary>
		/// <returns>A new, empty AccountBalanceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountBalanceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountBalance
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountEntryEntity objects.</summary>
	[Serializable]
	public partial class AccountEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountEntryEntityFactory() : base("AccountEntryEntity", VarioSL.Entities.EntityType.AccountEntryEntity) { }

		/// <summary>Creates a new, empty AccountEntryEntity object.</summary>
		/// <returns>A new, empty AccountEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountEntryNumberEntity objects.</summary>
	[Serializable]
	public partial class AccountEntryNumberEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountEntryNumberEntityFactory() : base("AccountEntryNumberEntity", VarioSL.Entities.EntityType.AccountEntryNumberEntity) { }

		/// <summary>Creates a new, empty AccountEntryNumberEntity object.</summary>
		/// <returns>A new, empty AccountEntryNumberEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountEntryNumberEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountEntryNumber
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountPostingKeyEntity objects.</summary>
	[Serializable]
	public partial class AccountPostingKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountPostingKeyEntityFactory() : base("AccountPostingKeyEntity", VarioSL.Entities.EntityType.AccountPostingKeyEntity) { }

		/// <summary>Creates a new, empty AccountPostingKeyEntity object.</summary>
		/// <returns>A new, empty AccountPostingKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountPostingKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountPostingKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApplicationEntity objects.</summary>
	[Serializable]
	public partial class ApplicationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApplicationEntityFactory() : base("ApplicationEntity", VarioSL.Entities.EntityType.ApplicationEntity) { }

		/// <summary>Creates a new, empty ApplicationEntity object.</summary>
		/// <returns>A new, empty ApplicationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApplicationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApplication
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApplicationVersionEntity objects.</summary>
	[Serializable]
	public partial class ApplicationVersionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApplicationVersionEntityFactory() : base("ApplicationVersionEntity", VarioSL.Entities.EntityType.ApplicationVersionEntity) { }

		/// <summary>Creates a new, empty ApplicationVersionEntity object.</summary>
		/// <returns>A new, empty ApplicationVersionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApplicationVersionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApplicationVersion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApportionmentEntity objects.</summary>
	[Serializable]
	public partial class ApportionmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApportionmentEntityFactory() : base("ApportionmentEntity", VarioSL.Entities.EntityType.ApportionmentEntity) { }

		/// <summary>Creates a new, empty ApportionmentEntity object.</summary>
		/// <returns>A new, empty ApportionmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApportionmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApportionment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApportionmentResultEntity objects.</summary>
	[Serializable]
	public partial class ApportionmentResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApportionmentResultEntityFactory() : base("ApportionmentResultEntity", VarioSL.Entities.EntityType.ApportionmentResultEntity) { }

		/// <summary>Creates a new, empty ApportionmentResultEntity object.</summary>
		/// <returns>A new, empty ApportionmentResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApportionmentResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApportionmentResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AreaListEntity objects.</summary>
	[Serializable]
	public partial class AreaListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AreaListEntityFactory() : base("AreaListEntity", VarioSL.Entities.EntityType.AreaListEntity) { }

		/// <summary>Creates a new, empty AreaListEntity object.</summary>
		/// <returns>A new, empty AreaListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AreaListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAreaList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AreaListElementEntity objects.</summary>
	[Serializable]
	public partial class AreaListElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AreaListElementEntityFactory() : base("AreaListElementEntity", VarioSL.Entities.EntityType.AreaListElementEntity) { }

		/// <summary>Creates a new, empty AreaListElementEntity object.</summary>
		/// <returns>A new, empty AreaListElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AreaListElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAreaListElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AreaListElementGroupEntity objects.</summary>
	[Serializable]
	public partial class AreaListElementGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AreaListElementGroupEntityFactory() : base("AreaListElementGroupEntity", VarioSL.Entities.EntityType.AreaListElementGroupEntity) { }

		/// <summary>Creates a new, empty AreaListElementGroupEntity object.</summary>
		/// <returns>A new, empty AreaListElementGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AreaListElementGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAreaListElementGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AreaTypeEntity objects.</summary>
	[Serializable]
	public partial class AreaTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AreaTypeEntityFactory() : base("AreaTypeEntity", VarioSL.Entities.EntityType.AreaTypeEntity) { }

		/// <summary>Creates a new, empty AreaTypeEntity object.</summary>
		/// <returns>A new, empty AreaTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AreaTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAreaType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeEntity objects.</summary>
	[Serializable]
	public partial class AttributeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeEntityFactory() : base("AttributeEntity", VarioSL.Entities.EntityType.AttributeEntity) { }

		/// <summary>Creates a new, empty AttributeEntity object.</summary>
		/// <returns>A new, empty AttributeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttribute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeBitmapLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class AttributeBitmapLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeBitmapLayoutObjectEntityFactory() : base("AttributeBitmapLayoutObjectEntity", VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity) { }

		/// <summary>Creates a new, empty AttributeBitmapLayoutObjectEntity object.</summary>
		/// <returns>A new, empty AttributeBitmapLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeBitmapLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeBitmapLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeTextLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class AttributeTextLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeTextLayoutObjectEntityFactory() : base("AttributeTextLayoutObjectEntity", VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity) { }

		/// <summary>Creates a new, empty AttributeTextLayoutObjectEntity object.</summary>
		/// <returns>A new, empty AttributeTextLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeTextLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeTextLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeValueEntity objects.</summary>
	[Serializable]
	public partial class AttributeValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeValueEntityFactory() : base("AttributeValueEntity", VarioSL.Entities.EntityType.AttributeValueEntity) { }

		/// <summary>Creates a new, empty AttributeValueEntity object.</summary>
		/// <returns>A new, empty AttributeValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeValueListEntity objects.</summary>
	[Serializable]
	public partial class AttributeValueListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeValueListEntityFactory() : base("AttributeValueListEntity", VarioSL.Entities.EntityType.AttributeValueListEntity) { }

		/// <summary>Creates a new, empty AttributeValueListEntity object.</summary>
		/// <returns>A new, empty AttributeValueListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeValueListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeValueList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AutomatEntity objects.</summary>
	[Serializable]
	public partial class AutomatEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AutomatEntityFactory() : base("AutomatEntity", VarioSL.Entities.EntityType.AutomatEntity) { }

		/// <summary>Creates a new, empty AutomatEntity object.</summary>
		/// <returns>A new, empty AutomatEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AutomatEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAutomat
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankEntity objects.</summary>
	[Serializable]
	public partial class BankEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankEntityFactory() : base("BankEntity", VarioSL.Entities.EntityType.BankEntity) { }

		/// <summary>Creates a new, empty BankEntity object.</summary>
		/// <returns>A new, empty BankEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBank
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BinaryDataEntity objects.</summary>
	[Serializable]
	public partial class BinaryDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BinaryDataEntityFactory() : base("BinaryDataEntity", VarioSL.Entities.EntityType.BinaryDataEntity) { }

		/// <summary>Creates a new, empty BinaryDataEntity object.</summary>
		/// <returns>A new, empty BinaryDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BinaryDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBinaryData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BlockingReasonEntity objects.</summary>
	[Serializable]
	public partial class BlockingReasonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BlockingReasonEntityFactory() : base("BlockingReasonEntity", VarioSL.Entities.EntityType.BlockingReasonEntity) { }

		/// <summary>Creates a new, empty BlockingReasonEntity object.</summary>
		/// <returns>A new, empty BlockingReasonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BlockingReasonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBlockingReason
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleEntityFactory() : base("BusinessRuleEntity", VarioSL.Entities.EntityType.BusinessRuleEntity) { }

		/// <summary>Creates a new, empty BusinessRuleEntity object.</summary>
		/// <returns>A new, empty BusinessRuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleClauseEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleClauseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleClauseEntityFactory() : base("BusinessRuleClauseEntity", VarioSL.Entities.EntityType.BusinessRuleClauseEntity) { }

		/// <summary>Creates a new, empty BusinessRuleClauseEntity object.</summary>
		/// <returns>A new, empty BusinessRuleClauseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleClauseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleClause
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleConditionEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleConditionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleConditionEntityFactory() : base("BusinessRuleConditionEntity", VarioSL.Entities.EntityType.BusinessRuleConditionEntity) { }

		/// <summary>Creates a new, empty BusinessRuleConditionEntity object.</summary>
		/// <returns>A new, empty BusinessRuleConditionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleConditionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleCondition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleResultEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleResultEntityFactory() : base("BusinessRuleResultEntity", VarioSL.Entities.EntityType.BusinessRuleResultEntity) { }

		/// <summary>Creates a new, empty BusinessRuleResultEntity object.</summary>
		/// <returns>A new, empty BusinessRuleResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleTypeEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleTypeEntityFactory() : base("BusinessRuleTypeEntity", VarioSL.Entities.EntityType.BusinessRuleTypeEntity) { }

		/// <summary>Creates a new, empty BusinessRuleTypeEntity object.</summary>
		/// <returns>A new, empty BusinessRuleTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleTypeToVariableEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleTypeToVariableEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleTypeToVariableEntityFactory() : base("BusinessRuleTypeToVariableEntity", VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity) { }

		/// <summary>Creates a new, empty BusinessRuleTypeToVariableEntity object.</summary>
		/// <returns>A new, empty BusinessRuleTypeToVariableEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleTypeToVariableEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleTypeToVariable
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinessRuleVariableEntity objects.</summary>
	[Serializable]
	public partial class BusinessRuleVariableEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinessRuleVariableEntityFactory() : base("BusinessRuleVariableEntity", VarioSL.Entities.EntityType.BusinessRuleVariableEntity) { }

		/// <summary>Creates a new, empty BusinessRuleVariableEntity object.</summary>
		/// <returns>A new, empty BusinessRuleVariableEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinessRuleVariableEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinessRuleVariable
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CalendarEntity objects.</summary>
	[Serializable]
	public partial class CalendarEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CalendarEntityFactory() : base("CalendarEntity", VarioSL.Entities.EntityType.CalendarEntity) { }

		/// <summary>Creates a new, empty CalendarEntity object.</summary>
		/// <returns>A new, empty CalendarEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CalendarEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCalendar
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CalendarEntryEntity objects.</summary>
	[Serializable]
	public partial class CalendarEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CalendarEntryEntityFactory() : base("CalendarEntryEntity", VarioSL.Entities.EntityType.CalendarEntryEntity) { }

		/// <summary>Creates a new, empty CalendarEntryEntity object.</summary>
		/// <returns>A new, empty CalendarEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CalendarEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCalendarEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardActionAttributeEntity objects.</summary>
	[Serializable]
	public partial class CardActionAttributeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardActionAttributeEntityFactory() : base("CardActionAttributeEntity", VarioSL.Entities.EntityType.CardActionAttributeEntity) { }

		/// <summary>Creates a new, empty CardActionAttributeEntity object.</summary>
		/// <returns>A new, empty CardActionAttributeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardActionAttributeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardActionAttribute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardActionRequestEntity objects.</summary>
	[Serializable]
	public partial class CardActionRequestEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardActionRequestEntityFactory() : base("CardActionRequestEntity", VarioSL.Entities.EntityType.CardActionRequestEntity) { }

		/// <summary>Creates a new, empty CardActionRequestEntity object.</summary>
		/// <returns>A new, empty CardActionRequestEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardActionRequestEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardActionRequest
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardActionRequestTypeEntity objects.</summary>
	[Serializable]
	public partial class CardActionRequestTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardActionRequestTypeEntityFactory() : base("CardActionRequestTypeEntity", VarioSL.Entities.EntityType.CardActionRequestTypeEntity) { }

		/// <summary>Creates a new, empty CardActionRequestTypeEntity object.</summary>
		/// <returns>A new, empty CardActionRequestTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardActionRequestTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardActionRequestType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardChipTypeEntity objects.</summary>
	[Serializable]
	public partial class CardChipTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardChipTypeEntityFactory() : base("CardChipTypeEntity", VarioSL.Entities.EntityType.CardChipTypeEntity) { }

		/// <summary>Creates a new, empty CardChipTypeEntity object.</summary>
		/// <returns>A new, empty CardChipTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardChipTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardChipType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardStateEntity objects.</summary>
	[Serializable]
	public partial class CardStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardStateEntityFactory() : base("CardStateEntity", VarioSL.Entities.EntityType.CardStateEntity) { }

		/// <summary>Creates a new, empty CardStateEntity object.</summary>
		/// <returns>A new, empty CardStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardTicketEntity objects.</summary>
	[Serializable]
	public partial class CardTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardTicketEntityFactory() : base("CardTicketEntity", VarioSL.Entities.EntityType.CardTicketEntity) { }

		/// <summary>Creates a new, empty CardTicketEntity object.</summary>
		/// <returns>A new, empty CardTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardTicketToTicketEntity objects.</summary>
	[Serializable]
	public partial class CardTicketToTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardTicketToTicketEntityFactory() : base("CardTicketToTicketEntity", VarioSL.Entities.EntityType.CardTicketToTicketEntity) { }

		/// <summary>Creates a new, empty CardTicketToTicketEntity object.</summary>
		/// <returns>A new, empty CardTicketToTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardTicketToTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardTicketToTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CashServiceEntity objects.</summary>
	[Serializable]
	public partial class CashServiceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CashServiceEntityFactory() : base("CashServiceEntity", VarioSL.Entities.EntityType.CashServiceEntity) { }

		/// <summary>Creates a new, empty CashServiceEntity object.</summary>
		/// <returns>A new, empty CashServiceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CashServiceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCashService
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CashServiceBalanceEntity objects.</summary>
	[Serializable]
	public partial class CashServiceBalanceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CashServiceBalanceEntityFactory() : base("CashServiceBalanceEntity", VarioSL.Entities.EntityType.CashServiceBalanceEntity) { }

		/// <summary>Creates a new, empty CashServiceBalanceEntity object.</summary>
		/// <returns>A new, empty CashServiceBalanceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CashServiceBalanceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCashServiceBalance
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CashServiceDataEntity objects.</summary>
	[Serializable]
	public partial class CashServiceDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CashServiceDataEntityFactory() : base("CashServiceDataEntity", VarioSL.Entities.EntityType.CashServiceDataEntity) { }

		/// <summary>Creates a new, empty CashServiceDataEntity object.</summary>
		/// <returns>A new, empty CashServiceDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CashServiceDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCashServiceData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CashUnitEntity objects.</summary>
	[Serializable]
	public partial class CashUnitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CashUnitEntityFactory() : base("CashUnitEntity", VarioSL.Entities.EntityType.CashUnitEntity) { }

		/// <summary>Creates a new, empty CashUnitEntity object.</summary>
		/// <returns>A new, empty CashUnitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CashUnitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCashUnit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ChoiceEntity objects.</summary>
	[Serializable]
	public partial class ChoiceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ChoiceEntityFactory() : base("ChoiceEntity", VarioSL.Entities.EntityType.ChoiceEntity) { }

		/// <summary>Creates a new, empty ChoiceEntity object.</summary>
		/// <returns>A new, empty ChoiceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ChoiceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewChoice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingEntity objects.</summary>
	[Serializable]
	public partial class ClearingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingEntityFactory() : base("ClearingEntity", VarioSL.Entities.EntityType.ClearingEntity) { }

		/// <summary>Creates a new, empty ClearingEntity object.</summary>
		/// <returns>A new, empty ClearingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearing
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingClassificationEntity objects.</summary>
	[Serializable]
	public partial class ClearingClassificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingClassificationEntityFactory() : base("ClearingClassificationEntity", VarioSL.Entities.EntityType.ClearingClassificationEntity) { }

		/// <summary>Creates a new, empty ClearingClassificationEntity object.</summary>
		/// <returns>A new, empty ClearingClassificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingClassificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingClassification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingDetailEntity objects.</summary>
	[Serializable]
	public partial class ClearingDetailEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingDetailEntityFactory() : base("ClearingDetailEntity", VarioSL.Entities.EntityType.ClearingDetailEntity) { }

		/// <summary>Creates a new, empty ClearingDetailEntity object.</summary>
		/// <returns>A new, empty ClearingDetailEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingDetailEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingDetail
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingResultEntity objects.</summary>
	[Serializable]
	public partial class ClearingResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingResultEntityFactory() : base("ClearingResultEntity", VarioSL.Entities.EntityType.ClearingResultEntity) { }

		/// <summary>Creates a new, empty ClearingResultEntity object.</summary>
		/// <returns>A new, empty ClearingResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingResultLevelEntity objects.</summary>
	[Serializable]
	public partial class ClearingResultLevelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingResultLevelEntityFactory() : base("ClearingResultLevelEntity", VarioSL.Entities.EntityType.ClearingResultLevelEntity) { }

		/// <summary>Creates a new, empty ClearingResultLevelEntity object.</summary>
		/// <returns>A new, empty ClearingResultLevelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingResultLevelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingResultLevel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingStateEntity objects.</summary>
	[Serializable]
	public partial class ClearingStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingStateEntityFactory() : base("ClearingStateEntity", VarioSL.Entities.EntityType.ClearingStateEntity) { }

		/// <summary>Creates a new, empty ClearingStateEntity object.</summary>
		/// <returns>A new, empty ClearingStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingSumEntity objects.</summary>
	[Serializable]
	public partial class ClearingSumEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingSumEntityFactory() : base("ClearingSumEntity", VarioSL.Entities.EntityType.ClearingSumEntity) { }

		/// <summary>Creates a new, empty ClearingSumEntity object.</summary>
		/// <returns>A new, empty ClearingSumEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingSumEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingSum
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClearingTransactionEntity objects.</summary>
	[Serializable]
	public partial class ClearingTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClearingTransactionEntityFactory() : base("ClearingTransactionEntity", VarioSL.Entities.EntityType.ClearingTransactionEntity) { }

		/// <summary>Creates a new, empty ClearingTransactionEntity object.</summary>
		/// <returns>A new, empty ClearingTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClearingTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClearingTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientEntity objects.</summary>
	[Serializable]
	public partial class ClientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientEntityFactory() : base("ClientEntity", VarioSL.Entities.EntityType.ClientEntity) { }

		/// <summary>Creates a new, empty ClientEntity object.</summary>
		/// <returns>A new, empty ClientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientAdaptedLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class ClientAdaptedLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientAdaptedLayoutObjectEntityFactory() : base("ClientAdaptedLayoutObjectEntity", VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity) { }

		/// <summary>Creates a new, empty ClientAdaptedLayoutObjectEntity object.</summary>
		/// <returns>A new, empty ClientAdaptedLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientAdaptedLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientAdaptedLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ComponentEntity objects.</summary>
	[Serializable]
	public partial class ComponentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ComponentEntityFactory() : base("ComponentEntity", VarioSL.Entities.EntityType.ComponentEntity) { }

		/// <summary>Creates a new, empty ComponentEntity object.</summary>
		/// <returns>A new, empty ComponentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ComponentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComponent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ComponentClearingEntity objects.</summary>
	[Serializable]
	public partial class ComponentClearingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ComponentClearingEntityFactory() : base("ComponentClearingEntity", VarioSL.Entities.EntityType.ComponentClearingEntity) { }

		/// <summary>Creates a new, empty ComponentClearingEntity object.</summary>
		/// <returns>A new, empty ComponentClearingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ComponentClearingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComponentClearing
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ComponentFillingEntity objects.</summary>
	[Serializable]
	public partial class ComponentFillingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ComponentFillingEntityFactory() : base("ComponentFillingEntity", VarioSL.Entities.EntityType.ComponentFillingEntity) { }

		/// <summary>Creates a new, empty ComponentFillingEntity object.</summary>
		/// <returns>A new, empty ComponentFillingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ComponentFillingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComponentFilling
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ComponentStateEntity objects.</summary>
	[Serializable]
	public partial class ComponentStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ComponentStateEntityFactory() : base("ComponentStateEntity", VarioSL.Entities.EntityType.ComponentStateEntity) { }

		/// <summary>Creates a new, empty ComponentStateEntity object.</summary>
		/// <returns>A new, empty ComponentStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ComponentStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComponentState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ComponentTypeEntity objects.</summary>
	[Serializable]
	public partial class ComponentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ComponentTypeEntityFactory() : base("ComponentTypeEntity", VarioSL.Entities.EntityType.ComponentTypeEntity) { }

		/// <summary>Creates a new, empty ComponentTypeEntity object.</summary>
		/// <returns>A new, empty ComponentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ComponentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComponentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConditionalSubPageLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class ConditionalSubPageLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConditionalSubPageLayoutObjectEntityFactory() : base("ConditionalSubPageLayoutObjectEntity", VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity) { }

		/// <summary>Creates a new, empty ConditionalSubPageLayoutObjectEntity object.</summary>
		/// <returns>A new, empty ConditionalSubPageLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConditionalSubPageLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConditionalSubPageLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditScreeningEntity objects.</summary>
	[Serializable]
	public partial class CreditScreeningEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditScreeningEntityFactory() : base("CreditScreeningEntity", VarioSL.Entities.EntityType.CreditScreeningEntity) { }

		/// <summary>Creates a new, empty CreditScreeningEntity object.</summary>
		/// <returns>A new, empty CreditScreeningEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditScreeningEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditScreening
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DayTypeEntity objects.</summary>
	[Serializable]
	public partial class DayTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DayTypeEntityFactory() : base("DayTypeEntity", VarioSL.Entities.EntityType.DayTypeEntity) { }

		/// <summary>Creates a new, empty DayTypeEntity object.</summary>
		/// <returns>A new, empty DayTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DayTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDayType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DebtorEntity objects.</summary>
	[Serializable]
	public partial class DebtorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DebtorEntityFactory() : base("DebtorEntity", VarioSL.Entities.EntityType.DebtorEntity) { }

		/// <summary>Creates a new, empty DebtorEntity object.</summary>
		/// <returns>A new, empty DebtorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DebtorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDebtor
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DebtorCardEntity objects.</summary>
	[Serializable]
	public partial class DebtorCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DebtorCardEntityFactory() : base("DebtorCardEntity", VarioSL.Entities.EntityType.DebtorCardEntity) { }

		/// <summary>Creates a new, empty DebtorCardEntity object.</summary>
		/// <returns>A new, empty DebtorCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DebtorCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDebtorCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DebtorCardHistoryEntity objects.</summary>
	[Serializable]
	public partial class DebtorCardHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DebtorCardHistoryEntityFactory() : base("DebtorCardHistoryEntity", VarioSL.Entities.EntityType.DebtorCardHistoryEntity) { }

		/// <summary>Creates a new, empty DebtorCardHistoryEntity object.</summary>
		/// <returns>A new, empty DebtorCardHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DebtorCardHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDebtorCardHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DefaultPinEntity objects.</summary>
	[Serializable]
	public partial class DefaultPinEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DefaultPinEntityFactory() : base("DefaultPinEntity", VarioSL.Entities.EntityType.DefaultPinEntity) { }

		/// <summary>Creates a new, empty DefaultPinEntity object.</summary>
		/// <returns>A new, empty DefaultPinEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DefaultPinEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDefaultPin
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DepotEntity objects.</summary>
	[Serializable]
	public partial class DepotEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DepotEntityFactory() : base("DepotEntity", VarioSL.Entities.EntityType.DepotEntity) { }

		/// <summary>Creates a new, empty DepotEntity object.</summary>
		/// <returns>A new, empty DepotEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DepotEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDepot
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceEntity objects.</summary>
	[Serializable]
	public partial class DeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceEntityFactory() : base("DeviceEntity", VarioSL.Entities.EntityType.DeviceEntity) { }

		/// <summary>Creates a new, empty DeviceEntity object.</summary>
		/// <returns>A new, empty DeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceBookingStateEntity objects.</summary>
	[Serializable]
	public partial class DeviceBookingStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceBookingStateEntityFactory() : base("DeviceBookingStateEntity", VarioSL.Entities.EntityType.DeviceBookingStateEntity) { }

		/// <summary>Creates a new, empty DeviceBookingStateEntity object.</summary>
		/// <returns>A new, empty DeviceBookingStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceBookingStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceBookingState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceClassEntity objects.</summary>
	[Serializable]
	public partial class DeviceClassEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceClassEntityFactory() : base("DeviceClassEntity", VarioSL.Entities.EntityType.DeviceClassEntity) { }

		/// <summary>Creates a new, empty DeviceClassEntity object.</summary>
		/// <returns>A new, empty DeviceClassEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceClassEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DevicePaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class DevicePaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DevicePaymentMethodEntityFactory() : base("DevicePaymentMethodEntity", VarioSL.Entities.EntityType.DevicePaymentMethodEntity) { }

		/// <summary>Creates a new, empty DevicePaymentMethodEntity object.</summary>
		/// <returns>A new, empty DevicePaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DevicePaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDevicePaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DirectionEntity objects.</summary>
	[Serializable]
	public partial class DirectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DirectionEntityFactory() : base("DirectionEntity", VarioSL.Entities.EntityType.DirectionEntity) { }

		/// <summary>Creates a new, empty DirectionEntity object.</summary>
		/// <returns>A new, empty DirectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DirectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDirection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningLevelEntity objects.</summary>
	[Serializable]
	public partial class DunningLevelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningLevelEntityFactory() : base("DunningLevelEntity", VarioSL.Entities.EntityType.DunningLevelEntity) { }

		/// <summary>Creates a new, empty DunningLevelEntity object.</summary>
		/// <returns>A new, empty DunningLevelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningLevelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningLevel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EticketEntity objects.</summary>
	[Serializable]
	public partial class EticketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EticketEntityFactory() : base("EticketEntity", VarioSL.Entities.EntityType.EticketEntity) { }

		/// <summary>Creates a new, empty EticketEntity object.</summary>
		/// <returns>A new, empty EticketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EticketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEticket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExportInfoEntity objects.</summary>
	[Serializable]
	public partial class ExportInfoEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExportInfoEntityFactory() : base("ExportInfoEntity", VarioSL.Entities.EntityType.ExportInfoEntity) { }

		/// <summary>Creates a new, empty ExportInfoEntity object.</summary>
		/// <returns>A new, empty ExportInfoEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExportInfoEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExportInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalCardEntity objects.</summary>
	[Serializable]
	public partial class ExternalCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalCardEntityFactory() : base("ExternalCardEntity", VarioSL.Entities.EntityType.ExternalCardEntity) { }

		/// <summary>Creates a new, empty ExternalCardEntity object.</summary>
		/// <returns>A new, empty ExternalCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalDataEntity objects.</summary>
	[Serializable]
	public partial class ExternalDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalDataEntityFactory() : base("ExternalDataEntity", VarioSL.Entities.EntityType.ExternalDataEntity) { }

		/// <summary>Creates a new, empty ExternalDataEntity object.</summary>
		/// <returns>A new, empty ExternalDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalEffortEntity objects.</summary>
	[Serializable]
	public partial class ExternalEffortEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalEffortEntityFactory() : base("ExternalEffortEntity", VarioSL.Entities.EntityType.ExternalEffortEntity) { }

		/// <summary>Creates a new, empty ExternalEffortEntity object.</summary>
		/// <returns>A new, empty ExternalEffortEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalEffortEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalEffort
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalPacketEntity objects.</summary>
	[Serializable]
	public partial class ExternalPacketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalPacketEntityFactory() : base("ExternalPacketEntity", VarioSL.Entities.EntityType.ExternalPacketEntity) { }

		/// <summary>Creates a new, empty ExternalPacketEntity object.</summary>
		/// <returns>A new, empty ExternalPacketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalPacketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalPacket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalPacketEffortEntity objects.</summary>
	[Serializable]
	public partial class ExternalPacketEffortEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalPacketEffortEntityFactory() : base("ExternalPacketEffortEntity", VarioSL.Entities.EntityType.ExternalPacketEffortEntity) { }

		/// <summary>Creates a new, empty ExternalPacketEffortEntity object.</summary>
		/// <returns>A new, empty ExternalPacketEffortEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalPacketEffortEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalPacketEffort
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalTypeEntity objects.</summary>
	[Serializable]
	public partial class ExternalTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalTypeEntityFactory() : base("ExternalTypeEntity", VarioSL.Entities.EntityType.ExternalTypeEntity) { }

		/// <summary>Creates a new, empty ExternalTypeEntity object.</summary>
		/// <returns>A new, empty ExternalTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionCategoryEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionCategoryEntityFactory() : base("FareEvasionCategoryEntity", VarioSL.Entities.EntityType.FareEvasionCategoryEntity) { }

		/// <summary>Creates a new, empty FareEvasionCategoryEntity object.</summary>
		/// <returns>A new, empty FareEvasionCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionReasonEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionReasonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionReasonEntityFactory() : base("FareEvasionReasonEntity", VarioSL.Entities.EntityType.FareEvasionReasonEntity) { }

		/// <summary>Creates a new, empty FareEvasionReasonEntity object.</summary>
		/// <returns>A new, empty FareEvasionReasonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionReasonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionReason
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionReasonToCategoryEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionReasonToCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionReasonToCategoryEntityFactory() : base("FareEvasionReasonToCategoryEntity", VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity) { }

		/// <summary>Creates a new, empty FareEvasionReasonToCategoryEntity object.</summary>
		/// <returns>A new, empty FareEvasionReasonToCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionReasonToCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionReasonToCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareMatrixEntity objects.</summary>
	[Serializable]
	public partial class FareMatrixEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareMatrixEntityFactory() : base("FareMatrixEntity", VarioSL.Entities.EntityType.FareMatrixEntity) { }

		/// <summary>Creates a new, empty FareMatrixEntity object.</summary>
		/// <returns>A new, empty FareMatrixEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareMatrixEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareMatrix
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareMatrixEntryEntity objects.</summary>
	[Serializable]
	public partial class FareMatrixEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareMatrixEntryEntityFactory() : base("FareMatrixEntryEntity", VarioSL.Entities.EntityType.FareMatrixEntryEntity) { }

		/// <summary>Creates a new, empty FareMatrixEntryEntity object.</summary>
		/// <returns>A new, empty FareMatrixEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareMatrixEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareMatrixEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareStageEntity objects.</summary>
	[Serializable]
	public partial class FareStageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareStageEntityFactory() : base("FareStageEntity", VarioSL.Entities.EntityType.FareStageEntity) { }

		/// <summary>Creates a new, empty FareStageEntity object.</summary>
		/// <returns>A new, empty FareStageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareStageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareStage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareStageAliasEntity objects.</summary>
	[Serializable]
	public partial class FareStageAliasEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareStageAliasEntityFactory() : base("FareStageAliasEntity", VarioSL.Entities.EntityType.FareStageAliasEntity) { }

		/// <summary>Creates a new, empty FareStageAliasEntity object.</summary>
		/// <returns>A new, empty FareStageAliasEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareStageAliasEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareStageAlias
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareStageHierarchieLevelEntity objects.</summary>
	[Serializable]
	public partial class FareStageHierarchieLevelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareStageHierarchieLevelEntityFactory() : base("FareStageHierarchieLevelEntity", VarioSL.Entities.EntityType.FareStageHierarchieLevelEntity) { }

		/// <summary>Creates a new, empty FareStageHierarchieLevelEntity object.</summary>
		/// <returns>A new, empty FareStageHierarchieLevelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareStageHierarchieLevelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareStageHierarchieLevel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareStageListEntity objects.</summary>
	[Serializable]
	public partial class FareStageListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareStageListEntityFactory() : base("FareStageListEntity", VarioSL.Entities.EntityType.FareStageListEntity) { }

		/// <summary>Creates a new, empty FareStageListEntity object.</summary>
		/// <returns>A new, empty FareStageListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareStageListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareStageList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareStageTypeEntity objects.</summary>
	[Serializable]
	public partial class FareStageTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareStageTypeEntityFactory() : base("FareStageTypeEntity", VarioSL.Entities.EntityType.FareStageTypeEntity) { }

		/// <summary>Creates a new, empty FareStageTypeEntity object.</summary>
		/// <returns>A new, empty FareStageTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareStageTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareStageType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareTableEntity objects.</summary>
	[Serializable]
	public partial class FareTableEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareTableEntityFactory() : base("FareTableEntity", VarioSL.Entities.EntityType.FareTableEntity) { }

		/// <summary>Creates a new, empty FareTableEntity object.</summary>
		/// <returns>A new, empty FareTableEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareTableEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareTable
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareTableEntryEntity objects.</summary>
	[Serializable]
	public partial class FareTableEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareTableEntryEntityFactory() : base("FareTableEntryEntity", VarioSL.Entities.EntityType.FareTableEntryEntity) { }

		/// <summary>Creates a new, empty FareTableEntryEntity object.</summary>
		/// <returns>A new, empty FareTableEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareTableEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareTableEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FixedBitmapLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class FixedBitmapLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FixedBitmapLayoutObjectEntityFactory() : base("FixedBitmapLayoutObjectEntity", VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity) { }

		/// <summary>Creates a new, empty FixedBitmapLayoutObjectEntity object.</summary>
		/// <returns>A new, empty FixedBitmapLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FixedBitmapLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFixedBitmapLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FixedTextLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class FixedTextLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FixedTextLayoutObjectEntityFactory() : base("FixedTextLayoutObjectEntity", VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity) { }

		/// <summary>Creates a new, empty FixedTextLayoutObjectEntity object.</summary>
		/// <returns>A new, empty FixedTextLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FixedTextLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFixedTextLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GuiDefEntity objects.</summary>
	[Serializable]
	public partial class GuiDefEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GuiDefEntityFactory() : base("GuiDefEntity", VarioSL.Entities.EntityType.GuiDefEntity) { }

		/// <summary>Creates a new, empty GuiDefEntity object.</summary>
		/// <returns>A new, empty GuiDefEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GuiDefEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGuiDef
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty KeyAttributeTransfromEntity objects.</summary>
	[Serializable]
	public partial class KeyAttributeTransfromEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public KeyAttributeTransfromEntityFactory() : base("KeyAttributeTransfromEntity", VarioSL.Entities.EntityType.KeyAttributeTransfromEntity) { }

		/// <summary>Creates a new, empty KeyAttributeTransfromEntity object.</summary>
		/// <returns>A new, empty KeyAttributeTransfromEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new KeyAttributeTransfromEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKeyAttributeTransfrom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty KeySelectionModeEntity objects.</summary>
	[Serializable]
	public partial class KeySelectionModeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public KeySelectionModeEntityFactory() : base("KeySelectionModeEntity", VarioSL.Entities.EntityType.KeySelectionModeEntity) { }

		/// <summary>Creates a new, empty KeySelectionModeEntity object.</summary>
		/// <returns>A new, empty KeySelectionModeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new KeySelectionModeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKeySelectionMode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty KVVSubscriptionEntity objects.</summary>
	[Serializable]
	public partial class KVVSubscriptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public KVVSubscriptionEntityFactory() : base("KVVSubscriptionEntity", VarioSL.Entities.EntityType.KVVSubscriptionEntity) { }

		/// <summary>Creates a new, empty KVVSubscriptionEntity object.</summary>
		/// <returns>A new, empty KVVSubscriptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new KVVSubscriptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewKVVSubscription
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LanguageEntity objects.</summary>
	[Serializable]
	public partial class LanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LanguageEntityFactory() : base("LanguageEntity", VarioSL.Entities.EntityType.LanguageEntity) { }

		/// <summary>Creates a new, empty LanguageEntity object.</summary>
		/// <returns>A new, empty LanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LayoutEntity objects.</summary>
	[Serializable]
	public partial class LayoutEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LayoutEntityFactory() : base("LayoutEntity", VarioSL.Entities.EntityType.LayoutEntity) { }

		/// <summary>Creates a new, empty LayoutEntity object.</summary>
		/// <returns>A new, empty LayoutEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LayoutEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLayout
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LineEntity objects.</summary>
	[Serializable]
	public partial class LineEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LineEntityFactory() : base("LineEntity", VarioSL.Entities.EntityType.LineEntity) { }

		/// <summary>Creates a new, empty LineEntity object.</summary>
		/// <returns>A new, empty LineEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LineEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLine
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LineGroupEntity objects.</summary>
	[Serializable]
	public partial class LineGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LineGroupEntityFactory() : base("LineGroupEntity", VarioSL.Entities.EntityType.LineGroupEntity) { }

		/// <summary>Creates a new, empty LineGroupEntity object.</summary>
		/// <returns>A new, empty LineGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LineGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLineGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LineGroupToLineGroupEntity objects.</summary>
	[Serializable]
	public partial class LineGroupToLineGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LineGroupToLineGroupEntityFactory() : base("LineGroupToLineGroupEntity", VarioSL.Entities.EntityType.LineGroupToLineGroupEntity) { }

		/// <summary>Creates a new, empty LineGroupToLineGroupEntity object.</summary>
		/// <returns>A new, empty LineGroupToLineGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LineGroupToLineGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLineGroupToLineGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LineToLineGroupEntity objects.</summary>
	[Serializable]
	public partial class LineToLineGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LineToLineGroupEntityFactory() : base("LineToLineGroupEntity", VarioSL.Entities.EntityType.LineToLineGroupEntity) { }

		/// <summary>Creates a new, empty LineToLineGroupEntity object.</summary>
		/// <returns>A new, empty LineToLineGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LineToLineGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLineToLineGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ListLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class ListLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ListLayoutObjectEntityFactory() : base("ListLayoutObjectEntity", VarioSL.Entities.EntityType.ListLayoutObjectEntity) { }

		/// <summary>Creates a new, empty ListLayoutObjectEntity object.</summary>
		/// <returns>A new, empty ListLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ListLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewListLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ListLayoutTypeEntity objects.</summary>
	[Serializable]
	public partial class ListLayoutTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ListLayoutTypeEntityFactory() : base("ListLayoutTypeEntity", VarioSL.Entities.EntityType.ListLayoutTypeEntity) { }

		/// <summary>Creates a new, empty ListLayoutTypeEntity object.</summary>
		/// <returns>A new, empty ListLayoutTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ListLayoutTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewListLayoutType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LogoEntity objects.</summary>
	[Serializable]
	public partial class LogoEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LogoEntityFactory() : base("LogoEntity", VarioSL.Entities.EntityType.LogoEntity) { }

		/// <summary>Creates a new, empty LogoEntity object.</summary>
		/// <returns>A new, empty LogoEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LogoEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLogo
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NetEntity objects.</summary>
	[Serializable]
	public partial class NetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NetEntityFactory() : base("NetEntity", VarioSL.Entities.EntityType.NetEntity) { }

		/// <summary>Creates a new, empty NetEntity object.</summary>
		/// <returns>A new, empty NetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNet
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NumberRangeEntity objects.</summary>
	[Serializable]
	public partial class NumberRangeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NumberRangeEntityFactory() : base("NumberRangeEntity", VarioSL.Entities.EntityType.NumberRangeEntity) { }

		/// <summary>Creates a new, empty NumberRangeEntity object.</summary>
		/// <returns>A new, empty NumberRangeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NumberRangeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNumberRange
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OperatorEntity objects.</summary>
	[Serializable]
	public partial class OperatorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OperatorEntityFactory() : base("OperatorEntity", VarioSL.Entities.EntityType.OperatorEntity) { }

		/// <summary>Creates a new, empty OperatorEntity object.</summary>
		/// <returns>A new, empty OperatorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OperatorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOperator
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OutputDeviceEntity objects.</summary>
	[Serializable]
	public partial class OutputDeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OutputDeviceEntityFactory() : base("OutputDeviceEntity", VarioSL.Entities.EntityType.OutputDeviceEntity) { }

		/// <summary>Creates a new, empty OutputDeviceEntity object.</summary>
		/// <returns>A new, empty OutputDeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OutputDeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOutputDevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageContentGroupEntity objects.</summary>
	[Serializable]
	public partial class PageContentGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageContentGroupEntityFactory() : base("PageContentGroupEntity", VarioSL.Entities.EntityType.PageContentGroupEntity) { }

		/// <summary>Creates a new, empty PageContentGroupEntity object.</summary>
		/// <returns>A new, empty PageContentGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageContentGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageContentGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageContentGroupToContentEntity objects.</summary>
	[Serializable]
	public partial class PageContentGroupToContentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageContentGroupToContentEntityFactory() : base("PageContentGroupToContentEntity", VarioSL.Entities.EntityType.PageContentGroupToContentEntity) { }

		/// <summary>Creates a new, empty PageContentGroupToContentEntity object.</summary>
		/// <returns>A new, empty PageContentGroupToContentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageContentGroupToContentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageContentGroupToContent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PanelEntity objects.</summary>
	[Serializable]
	public partial class PanelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PanelEntityFactory() : base("PanelEntity", VarioSL.Entities.EntityType.PanelEntity) { }

		/// <summary>Creates a new, empty PanelEntity object.</summary>
		/// <returns>A new, empty PanelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PanelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPanel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterAttributeValueEntity objects.</summary>
	[Serializable]
	public partial class ParameterAttributeValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterAttributeValueEntityFactory() : base("ParameterAttributeValueEntity", VarioSL.Entities.EntityType.ParameterAttributeValueEntity) { }

		/// <summary>Creates a new, empty ParameterAttributeValueEntity object.</summary>
		/// <returns>A new, empty ParameterAttributeValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterAttributeValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterAttributeValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterFareStageEntity objects.</summary>
	[Serializable]
	public partial class ParameterFareStageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterFareStageEntityFactory() : base("ParameterFareStageEntity", VarioSL.Entities.EntityType.ParameterFareStageEntity) { }

		/// <summary>Creates a new, empty ParameterFareStageEntity object.</summary>
		/// <returns>A new, empty ParameterFareStageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterFareStageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterFareStage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterTariffEntity objects.</summary>
	[Serializable]
	public partial class ParameterTariffEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterTariffEntityFactory() : base("ParameterTariffEntity", VarioSL.Entities.EntityType.ParameterTariffEntity) { }

		/// <summary>Creates a new, empty ParameterTariffEntity object.</summary>
		/// <returns>A new, empty ParameterTariffEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterTariffEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterTariff
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterTicketEntity objects.</summary>
	[Serializable]
	public partial class ParameterTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterTicketEntityFactory() : base("ParameterTicketEntity", VarioSL.Entities.EntityType.ParameterTicketEntity) { }

		/// <summary>Creates a new, empty ParameterTicketEntity object.</summary>
		/// <returns>A new, empty ParameterTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentDetailEntity objects.</summary>
	[Serializable]
	public partial class PaymentDetailEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentDetailEntityFactory() : base("PaymentDetailEntity", VarioSL.Entities.EntityType.PaymentDetailEntity) { }

		/// <summary>Creates a new, empty PaymentDetailEntity object.</summary>
		/// <returns>A new, empty PaymentDetailEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentDetailEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentDetail
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentIntervalEntity objects.</summary>
	[Serializable]
	public partial class PaymentIntervalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentIntervalEntityFactory() : base("PaymentIntervalEntity", VarioSL.Entities.EntityType.PaymentIntervalEntity) { }

		/// <summary>Creates a new, empty PaymentIntervalEntity object.</summary>
		/// <returns>A new, empty PaymentIntervalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentIntervalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentInterval
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PointOfSaleEntity objects.</summary>
	[Serializable]
	public partial class PointOfSaleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PointOfSaleEntityFactory() : base("PointOfSaleEntity", VarioSL.Entities.EntityType.PointOfSaleEntity) { }

		/// <summary>Creates a new, empty PointOfSaleEntity object.</summary>
		/// <returns>A new, empty PointOfSaleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PointOfSaleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPointOfSale
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PredefinedKeyEntity objects.</summary>
	[Serializable]
	public partial class PredefinedKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PredefinedKeyEntityFactory() : base("PredefinedKeyEntity", VarioSL.Entities.EntityType.PredefinedKeyEntity) { }

		/// <summary>Creates a new, empty PredefinedKeyEntity object.</summary>
		/// <returns>A new, empty PredefinedKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PredefinedKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPredefinedKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceTypeEntity objects.</summary>
	[Serializable]
	public partial class PriceTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceTypeEntityFactory() : base("PriceTypeEntity", VarioSL.Entities.EntityType.PriceTypeEntity) { }

		/// <summary>Creates a new, empty PriceTypeEntity object.</summary>
		/// <returns>A new, empty PriceTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrimalKeyEntity objects.</summary>
	[Serializable]
	public partial class PrimalKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrimalKeyEntityFactory() : base("PrimalKeyEntity", VarioSL.Entities.EntityType.PrimalKeyEntity) { }

		/// <summary>Creates a new, empty PrimalKeyEntity object.</summary>
		/// <returns>A new, empty PrimalKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrimalKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrimalKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrintTextEntity objects.</summary>
	[Serializable]
	public partial class PrintTextEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrintTextEntityFactory() : base("PrintTextEntity", VarioSL.Entities.EntityType.PrintTextEntity) { }

		/// <summary>Creates a new, empty PrintTextEntity object.</summary>
		/// <returns>A new, empty PrintTextEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrintTextEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrintText
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductOnCardEntity objects.</summary>
	[Serializable]
	public partial class ProductOnCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductOnCardEntityFactory() : base("ProductOnCardEntity", VarioSL.Entities.EntityType.ProductOnCardEntity) { }

		/// <summary>Creates a new, empty ProductOnCardEntity object.</summary>
		/// <returns>A new, empty ProductOnCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductOnCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductOnCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProtocolEntity objects.</summary>
	[Serializable]
	public partial class ProtocolEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProtocolEntityFactory() : base("ProtocolEntity", VarioSL.Entities.EntityType.ProtocolEntity) { }

		/// <summary>Creates a new, empty ProtocolEntity object.</summary>
		/// <returns>A new, empty ProtocolEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProtocolEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProtocol
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProtocolActionEntity objects.</summary>
	[Serializable]
	public partial class ProtocolActionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProtocolActionEntityFactory() : base("ProtocolActionEntity", VarioSL.Entities.EntityType.ProtocolActionEntity) { }

		/// <summary>Creates a new, empty ProtocolActionEntity object.</summary>
		/// <returns>A new, empty ProtocolActionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProtocolActionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProtocolAction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProtocolFunctionEntity objects.</summary>
	[Serializable]
	public partial class ProtocolFunctionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProtocolFunctionEntityFactory() : base("ProtocolFunctionEntity", VarioSL.Entities.EntityType.ProtocolFunctionEntity) { }

		/// <summary>Creates a new, empty ProtocolFunctionEntity object.</summary>
		/// <returns>A new, empty ProtocolFunctionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProtocolFunctionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProtocolFunction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProtocolFunctionGroupEntity objects.</summary>
	[Serializable]
	public partial class ProtocolFunctionGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProtocolFunctionGroupEntityFactory() : base("ProtocolFunctionGroupEntity", VarioSL.Entities.EntityType.ProtocolFunctionGroupEntity) { }

		/// <summary>Creates a new, empty ProtocolFunctionGroupEntity object.</summary>
		/// <returns>A new, empty ProtocolFunctionGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProtocolFunctionGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProtocolFunctionGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProtocolMessageEntity objects.</summary>
	[Serializable]
	public partial class ProtocolMessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProtocolMessageEntityFactory() : base("ProtocolMessageEntity", VarioSL.Entities.EntityType.ProtocolMessageEntity) { }

		/// <summary>Creates a new, empty ProtocolMessageEntity object.</summary>
		/// <returns>A new, empty ProtocolMessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProtocolMessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProtocolMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty QualificationEntity objects.</summary>
	[Serializable]
	public partial class QualificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public QualificationEntityFactory() : base("QualificationEntity", VarioSL.Entities.EntityType.QualificationEntity) { }

		/// <summary>Creates a new, empty QualificationEntity object.</summary>
		/// <returns>A new, empty QualificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new QualificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewQualification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ResponsibilityEntity objects.</summary>
	[Serializable]
	public partial class ResponsibilityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ResponsibilityEntityFactory() : base("ResponsibilityEntity", VarioSL.Entities.EntityType.ResponsibilityEntity) { }

		/// <summary>Creates a new, empty ResponsibilityEntity object.</summary>
		/// <returns>A new, empty ResponsibilityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ResponsibilityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewResponsibility
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RevenueTransactionTypeEntity objects.</summary>
	[Serializable]
	public partial class RevenueTransactionTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RevenueTransactionTypeEntityFactory() : base("RevenueTransactionTypeEntity", VarioSL.Entities.EntityType.RevenueTransactionTypeEntity) { }

		/// <summary>Creates a new, empty RevenueTransactionTypeEntity object.</summary>
		/// <returns>A new, empty RevenueTransactionTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RevenueTransactionTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueTransactionType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RmPaymentEntity objects.</summary>
	[Serializable]
	public partial class RmPaymentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RmPaymentEntityFactory() : base("RmPaymentEntity", VarioSL.Entities.EntityType.RmPaymentEntity) { }

		/// <summary>Creates a new, empty RmPaymentEntity object.</summary>
		/// <returns>A new, empty RmPaymentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RmPaymentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRmPayment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RouteEntity objects.</summary>
	[Serializable]
	public partial class RouteEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RouteEntityFactory() : base("RouteEntity", VarioSL.Entities.EntityType.RouteEntity) { }

		/// <summary>Creates a new, empty RouteEntity object.</summary>
		/// <returns>A new, empty RouteEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RouteEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RouteNameEntity objects.</summary>
	[Serializable]
	public partial class RouteNameEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RouteNameEntityFactory() : base("RouteNameEntity", VarioSL.Entities.EntityType.RouteNameEntity) { }

		/// <summary>Creates a new, empty RouteNameEntity object.</summary>
		/// <returns>A new, empty RouteNameEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RouteNameEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRouteName
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleCappingEntity objects.</summary>
	[Serializable]
	public partial class RuleCappingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleCappingEntityFactory() : base("RuleCappingEntity", VarioSL.Entities.EntityType.RuleCappingEntity) { }

		/// <summary>Creates a new, empty RuleCappingEntity object.</summary>
		/// <returns>A new, empty RuleCappingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleCappingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleCapping
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleCappingToFtEntryEntity objects.</summary>
	[Serializable]
	public partial class RuleCappingToFtEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleCappingToFtEntryEntityFactory() : base("RuleCappingToFtEntryEntity", VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity) { }

		/// <summary>Creates a new, empty RuleCappingToFtEntryEntity object.</summary>
		/// <returns>A new, empty RuleCappingToFtEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleCappingToFtEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleCappingToFtEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleCappingToTicketEntity objects.</summary>
	[Serializable]
	public partial class RuleCappingToTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleCappingToTicketEntityFactory() : base("RuleCappingToTicketEntity", VarioSL.Entities.EntityType.RuleCappingToTicketEntity) { }

		/// <summary>Creates a new, empty RuleCappingToTicketEntity object.</summary>
		/// <returns>A new, empty RuleCappingToTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleCappingToTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleCappingToTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RulePeriodEntity objects.</summary>
	[Serializable]
	public partial class RulePeriodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RulePeriodEntityFactory() : base("RulePeriodEntity", VarioSL.Entities.EntityType.RulePeriodEntity) { }

		/// <summary>Creates a new, empty RulePeriodEntity object.</summary>
		/// <returns>A new, empty RulePeriodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RulePeriodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRulePeriod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleTypeEntity objects.</summary>
	[Serializable]
	public partial class RuleTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleTypeEntityFactory() : base("RuleTypeEntity", VarioSL.Entities.EntityType.RuleTypeEntity) { }

		/// <summary>Creates a new, empty RuleTypeEntity object.</summary>
		/// <returns>A new, empty RuleTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SalutationEntity objects.</summary>
	[Serializable]
	public partial class SalutationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SalutationEntityFactory() : base("SalutationEntity", VarioSL.Entities.EntityType.SalutationEntity) { }

		/// <summary>Creates a new, empty SalutationEntity object.</summary>
		/// <returns>A new, empty SalutationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SalutationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSalutation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServiceAllocationEntity objects.</summary>
	[Serializable]
	public partial class ServiceAllocationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServiceAllocationEntityFactory() : base("ServiceAllocationEntity", VarioSL.Entities.EntityType.ServiceAllocationEntity) { }

		/// <summary>Creates a new, empty ServiceAllocationEntity object.</summary>
		/// <returns>A new, empty ServiceAllocationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServiceAllocationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceAllocation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServiceIdToCardEntity objects.</summary>
	[Serializable]
	public partial class ServiceIdToCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServiceIdToCardEntityFactory() : base("ServiceIdToCardEntity", VarioSL.Entities.EntityType.ServiceIdToCardEntity) { }

		/// <summary>Creates a new, empty ServiceIdToCardEntity object.</summary>
		/// <returns>A new, empty ServiceIdToCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServiceIdToCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceIdToCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServicePercentageEntity objects.</summary>
	[Serializable]
	public partial class ServicePercentageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServicePercentageEntityFactory() : base("ServicePercentageEntity", VarioSL.Entities.EntityType.ServicePercentageEntity) { }

		/// <summary>Creates a new, empty ServicePercentageEntity object.</summary>
		/// <returns>A new, empty ServicePercentageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServicePercentageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServicePercentage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ShiftEntity objects.</summary>
	[Serializable]
	public partial class ShiftEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ShiftEntityFactory() : base("ShiftEntity", VarioSL.Entities.EntityType.ShiftEntity) { }

		/// <summary>Creates a new, empty ShiftEntity object.</summary>
		/// <returns>A new, empty ShiftEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ShiftEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewShift
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ShiftInventoryEntity objects.</summary>
	[Serializable]
	public partial class ShiftInventoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ShiftInventoryEntityFactory() : base("ShiftInventoryEntity", VarioSL.Entities.EntityType.ShiftInventoryEntity) { }

		/// <summary>Creates a new, empty ShiftInventoryEntity object.</summary>
		/// <returns>A new, empty ShiftInventoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ShiftInventoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewShiftInventory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ShiftStateEntity objects.</summary>
	[Serializable]
	public partial class ShiftStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ShiftStateEntityFactory() : base("ShiftStateEntity", VarioSL.Entities.EntityType.ShiftStateEntity) { }

		/// <summary>Creates a new, empty ShiftStateEntity object.</summary>
		/// <returns>A new, empty ShiftStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ShiftStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewShiftState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ShortDistanceEntity objects.</summary>
	[Serializable]
	public partial class ShortDistanceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ShortDistanceEntityFactory() : base("ShortDistanceEntity", VarioSL.Entities.EntityType.ShortDistanceEntity) { }

		/// <summary>Creates a new, empty ShortDistanceEntity object.</summary>
		/// <returns>A new, empty ShortDistanceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ShortDistanceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewShortDistance
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SpecialReceiptEntity objects.</summary>
	[Serializable]
	public partial class SpecialReceiptEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SpecialReceiptEntityFactory() : base("SpecialReceiptEntity", VarioSL.Entities.EntityType.SpecialReceiptEntity) { }

		/// <summary>Creates a new, empty SpecialReceiptEntity object.</summary>
		/// <returns>A new, empty SpecialReceiptEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SpecialReceiptEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSpecialReceipt
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StopEntity objects.</summary>
	[Serializable]
	public partial class StopEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StopEntityFactory() : base("StopEntity", VarioSL.Entities.EntityType.StopEntity) { }

		/// <summary>Creates a new, empty StopEntity object.</summary>
		/// <returns>A new, empty StopEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StopEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStop
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SystemFieldEntity objects.</summary>
	[Serializable]
	public partial class SystemFieldEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SystemFieldEntityFactory() : base("SystemFieldEntity", VarioSL.Entities.EntityType.SystemFieldEntity) { }

		/// <summary>Creates a new, empty SystemFieldEntity object.</summary>
		/// <returns>A new, empty SystemFieldEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SystemFieldEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemField
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SystemFieldBarcodeLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class SystemFieldBarcodeLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SystemFieldBarcodeLayoutObjectEntityFactory() : base("SystemFieldBarcodeLayoutObjectEntity", VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity) { }

		/// <summary>Creates a new, empty SystemFieldBarcodeLayoutObjectEntity object.</summary>
		/// <returns>A new, empty SystemFieldBarcodeLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SystemFieldBarcodeLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemFieldBarcodeLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SystemFieldDynamicGraphicLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class SystemFieldDynamicGraphicLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SystemFieldDynamicGraphicLayoutObjectEntityFactory() : base("SystemFieldDynamicGraphicLayoutObjectEntity", VarioSL.Entities.EntityType.SystemFieldDynamicGraphicLayoutObjectEntity) { }

		/// <summary>Creates a new, empty SystemFieldDynamicGraphicLayoutObjectEntity object.</summary>
		/// <returns>A new, empty SystemFieldDynamicGraphicLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SystemFieldDynamicGraphicLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemFieldDynamicGraphicLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SystemFieldTextLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class SystemFieldTextLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SystemFieldTextLayoutObjectEntityFactory() : base("SystemFieldTextLayoutObjectEntity", VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity) { }

		/// <summary>Creates a new, empty SystemFieldTextLayoutObjectEntity object.</summary>
		/// <returns>A new, empty SystemFieldTextLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SystemFieldTextLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemFieldTextLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SystemTextEntity objects.</summary>
	[Serializable]
	public partial class SystemTextEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SystemTextEntityFactory() : base("SystemTextEntity", VarioSL.Entities.EntityType.SystemTextEntity) { }

		/// <summary>Creates a new, empty SystemTextEntity object.</summary>
		/// <returns>A new, empty SystemTextEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SystemTextEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSystemText
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TariffEntity objects.</summary>
	[Serializable]
	public partial class TariffEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TariffEntityFactory() : base("TariffEntity", VarioSL.Entities.EntityType.TariffEntity) { }

		/// <summary>Creates a new, empty TariffEntity object.</summary>
		/// <returns>A new, empty TariffEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TariffEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTariff
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TariffParameterEntity objects.</summary>
	[Serializable]
	public partial class TariffParameterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TariffParameterEntityFactory() : base("TariffParameterEntity", VarioSL.Entities.EntityType.TariffParameterEntity) { }

		/// <summary>Creates a new, empty TariffParameterEntity object.</summary>
		/// <returns>A new, empty TariffParameterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TariffParameterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTariffParameter
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TariffReleaseEntity objects.</summary>
	[Serializable]
	public partial class TariffReleaseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TariffReleaseEntityFactory() : base("TariffReleaseEntity", VarioSL.Entities.EntityType.TariffReleaseEntity) { }

		/// <summary>Creates a new, empty TariffReleaseEntity object.</summary>
		/// <returns>A new, empty TariffReleaseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TariffReleaseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTariffRelease
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TemporalTypeEntity objects.</summary>
	[Serializable]
	public partial class TemporalTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TemporalTypeEntityFactory() : base("TemporalTypeEntity", VarioSL.Entities.EntityType.TemporalTypeEntity) { }

		/// <summary>Creates a new, empty TemporalTypeEntity object.</summary>
		/// <returns>A new, empty TemporalTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TemporalTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTemporalType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketEntity objects.</summary>
	[Serializable]
	public partial class TicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketEntityFactory() : base("TicketEntity", VarioSL.Entities.EntityType.TicketEntity) { }

		/// <summary>Creates a new, empty TicketEntity object.</summary>
		/// <returns>A new, empty TicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketCancellationTypeEntity objects.</summary>
	[Serializable]
	public partial class TicketCancellationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketCancellationTypeEntityFactory() : base("TicketCancellationTypeEntity", VarioSL.Entities.EntityType.TicketCancellationTypeEntity) { }

		/// <summary>Creates a new, empty TicketCancellationTypeEntity object.</summary>
		/// <returns>A new, empty TicketCancellationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketCancellationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketCancellationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketCategoryEntity objects.</summary>
	[Serializable]
	public partial class TicketCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketCategoryEntityFactory() : base("TicketCategoryEntity", VarioSL.Entities.EntityType.TicketCategoryEntity) { }

		/// <summary>Creates a new, empty TicketCategoryEntity object.</summary>
		/// <returns>A new, empty TicketCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketDayTypeEntity objects.</summary>
	[Serializable]
	public partial class TicketDayTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketDayTypeEntityFactory() : base("TicketDayTypeEntity", VarioSL.Entities.EntityType.TicketDayTypeEntity) { }

		/// <summary>Creates a new, empty TicketDayTypeEntity object.</summary>
		/// <returns>A new, empty TicketDayTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketDayTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketDayType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketDeviceClassEntity objects.</summary>
	[Serializable]
	public partial class TicketDeviceClassEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketDeviceClassEntityFactory() : base("TicketDeviceClassEntity", VarioSL.Entities.EntityType.TicketDeviceClassEntity) { }

		/// <summary>Creates a new, empty TicketDeviceClassEntity object.</summary>
		/// <returns>A new, empty TicketDeviceClassEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketDeviceClassEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketDeviceClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketDeviceClassOutputDeviceEntity objects.</summary>
	[Serializable]
	public partial class TicketDeviceClassOutputDeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketDeviceClassOutputDeviceEntityFactory() : base("TicketDeviceClassOutputDeviceEntity", VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity) { }

		/// <summary>Creates a new, empty TicketDeviceClassOutputDeviceEntity object.</summary>
		/// <returns>A new, empty TicketDeviceClassOutputDeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketDeviceClassOutputDeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketDeviceClassOutputDevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketDeviceClassPaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class TicketDeviceClassPaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketDeviceClassPaymentMethodEntityFactory() : base("TicketDeviceClassPaymentMethodEntity", VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity) { }

		/// <summary>Creates a new, empty TicketDeviceClassPaymentMethodEntity object.</summary>
		/// <returns>A new, empty TicketDeviceClassPaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketDeviceClassPaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketDeviceClassPaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketDevicePaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class TicketDevicePaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketDevicePaymentMethodEntityFactory() : base("TicketDevicePaymentMethodEntity", VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity) { }

		/// <summary>Creates a new, empty TicketDevicePaymentMethodEntity object.</summary>
		/// <returns>A new, empty TicketDevicePaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketDevicePaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketDevicePaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketGroupEntity objects.</summary>
	[Serializable]
	public partial class TicketGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketGroupEntityFactory() : base("TicketGroupEntity", VarioSL.Entities.EntityType.TicketGroupEntity) { }

		/// <summary>Creates a new, empty TicketGroupEntity object.</summary>
		/// <returns>A new, empty TicketGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketOrganizationEntity objects.</summary>
	[Serializable]
	public partial class TicketOrganizationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketOrganizationEntityFactory() : base("TicketOrganizationEntity", VarioSL.Entities.EntityType.TicketOrganizationEntity) { }

		/// <summary>Creates a new, empty TicketOrganizationEntity object.</summary>
		/// <returns>A new, empty TicketOrganizationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketOrganizationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketOrganization
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketOutputdeviceEntity objects.</summary>
	[Serializable]
	public partial class TicketOutputdeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketOutputdeviceEntityFactory() : base("TicketOutputdeviceEntity", VarioSL.Entities.EntityType.TicketOutputdeviceEntity) { }

		/// <summary>Creates a new, empty TicketOutputdeviceEntity object.</summary>
		/// <returns>A new, empty TicketOutputdeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketOutputdeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketOutputdevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketPaymentIntervalEntity objects.</summary>
	[Serializable]
	public partial class TicketPaymentIntervalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketPaymentIntervalEntityFactory() : base("TicketPaymentIntervalEntity", VarioSL.Entities.EntityType.TicketPaymentIntervalEntity) { }

		/// <summary>Creates a new, empty TicketPaymentIntervalEntity object.</summary>
		/// <returns>A new, empty TicketPaymentIntervalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketPaymentIntervalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketPaymentInterval
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketPhysicalCardTypeEntity objects.</summary>
	[Serializable]
	public partial class TicketPhysicalCardTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketPhysicalCardTypeEntityFactory() : base("TicketPhysicalCardTypeEntity", VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity) { }

		/// <summary>Creates a new, empty TicketPhysicalCardTypeEntity object.</summary>
		/// <returns>A new, empty TicketPhysicalCardTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketPhysicalCardTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketPhysicalCardType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketSelectionModeEntity objects.</summary>
	[Serializable]
	public partial class TicketSelectionModeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketSelectionModeEntityFactory() : base("TicketSelectionModeEntity", VarioSL.Entities.EntityType.TicketSelectionModeEntity) { }

		/// <summary>Creates a new, empty TicketSelectionModeEntity object.</summary>
		/// <returns>A new, empty TicketSelectionModeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketSelectionModeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketSelectionMode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketServicesPermittedEntity objects.</summary>
	[Serializable]
	public partial class TicketServicesPermittedEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketServicesPermittedEntityFactory() : base("TicketServicesPermittedEntity", VarioSL.Entities.EntityType.TicketServicesPermittedEntity) { }

		/// <summary>Creates a new, empty TicketServicesPermittedEntity object.</summary>
		/// <returns>A new, empty TicketServicesPermittedEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketServicesPermittedEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketServicesPermitted
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketToGroupEntity objects.</summary>
	[Serializable]
	public partial class TicketToGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketToGroupEntityFactory() : base("TicketToGroupEntity", VarioSL.Entities.EntityType.TicketToGroupEntity) { }

		/// <summary>Creates a new, empty TicketToGroupEntity object.</summary>
		/// <returns>A new, empty TicketToGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketToGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketToGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketTypeEntity objects.</summary>
	[Serializable]
	public partial class TicketTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketTypeEntityFactory() : base("TicketTypeEntity", VarioSL.Entities.EntityType.TicketTypeEntity) { }

		/// <summary>Creates a new, empty TicketTypeEntity object.</summary>
		/// <returns>A new, empty TicketTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketVendingClientEntity objects.</summary>
	[Serializable]
	public partial class TicketVendingClientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketVendingClientEntityFactory() : base("TicketVendingClientEntity", VarioSL.Entities.EntityType.TicketVendingClientEntity) { }

		/// <summary>Creates a new, empty TicketVendingClientEntity object.</summary>
		/// <returns>A new, empty TicketVendingClientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketVendingClientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketVendingClient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionEntity objects.</summary>
	[Serializable]
	public partial class TransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionEntityFactory() : base("TransactionEntity", VarioSL.Entities.EntityType.TransactionEntity) { }

		/// <summary>Creates a new, empty TransactionEntity object.</summary>
		/// <returns>A new, empty TransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionAdditionalEntity objects.</summary>
	[Serializable]
	public partial class TransactionAdditionalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionAdditionalEntityFactory() : base("TransactionAdditionalEntity", VarioSL.Entities.EntityType.TransactionAdditionalEntity) { }

		/// <summary>Creates a new, empty TransactionAdditionalEntity object.</summary>
		/// <returns>A new, empty TransactionAdditionalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionAdditionalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionAdditional
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionBackupEntity objects.</summary>
	[Serializable]
	public partial class TransactionBackupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionBackupEntityFactory() : base("TransactionBackupEntity", VarioSL.Entities.EntityType.TransactionBackupEntity) { }

		/// <summary>Creates a new, empty TransactionBackupEntity object.</summary>
		/// <returns>A new, empty TransactionBackupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionBackupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionBackup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionExtensionEntity objects.</summary>
	[Serializable]
	public partial class TransactionExtensionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionExtensionEntityFactory() : base("TransactionExtensionEntity", VarioSL.Entities.EntityType.TransactionExtensionEntity) { }

		/// <summary>Creates a new, empty TransactionExtensionEntity object.</summary>
		/// <returns>A new, empty TransactionExtensionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionExtensionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionExtension
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TranslationEntity objects.</summary>
	[Serializable]
	public partial class TranslationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TranslationEntityFactory() : base("TranslationEntity", VarioSL.Entities.EntityType.TranslationEntity) { }

		/// <summary>Creates a new, empty TranslationEntity object.</summary>
		/// <returns>A new, empty TranslationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TranslationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTranslation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TypeOfCardEntity objects.</summary>
	[Serializable]
	public partial class TypeOfCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TypeOfCardEntityFactory() : base("TypeOfCardEntity", VarioSL.Entities.EntityType.TypeOfCardEntity) { }

		/// <summary>Creates a new, empty TypeOfCardEntity object.</summary>
		/// <returns>A new, empty TypeOfCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TypeOfCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTypeOfCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TypeOfUnitEntity objects.</summary>
	[Serializable]
	public partial class TypeOfUnitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TypeOfUnitEntityFactory() : base("TypeOfUnitEntity", VarioSL.Entities.EntityType.TypeOfUnitEntity) { }

		/// <summary>Creates a new, empty TypeOfUnitEntity object.</summary>
		/// <returns>A new, empty TypeOfUnitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TypeOfUnitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTypeOfUnit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UnitEntity objects.</summary>
	[Serializable]
	public partial class UnitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UnitEntityFactory() : base("UnitEntity", VarioSL.Entities.EntityType.UnitEntity) { }

		/// <summary>Creates a new, empty UnitEntity object.</summary>
		/// <returns>A new, empty UnitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UnitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUnit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserConfigEntity objects.</summary>
	[Serializable]
	public partial class UserConfigEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserConfigEntityFactory() : base("UserConfigEntity", VarioSL.Entities.EntityType.UserConfigEntity) { }

		/// <summary>Creates a new, empty UserConfigEntity object.</summary>
		/// <returns>A new, empty UserConfigEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserConfigEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserConfig
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserGroupEntity objects.</summary>
	[Serializable]
	public partial class UserGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserGroupEntityFactory() : base("UserGroupEntity", VarioSL.Entities.EntityType.UserGroupEntity) { }

		/// <summary>Creates a new, empty UserGroupEntity object.</summary>
		/// <returns>A new, empty UserGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserGroupRightEntity objects.</summary>
	[Serializable]
	public partial class UserGroupRightEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserGroupRightEntityFactory() : base("UserGroupRightEntity", VarioSL.Entities.EntityType.UserGroupRightEntity) { }

		/// <summary>Creates a new, empty UserGroupRightEntity object.</summary>
		/// <returns>A new, empty UserGroupRightEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserGroupRightEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserGroupRight
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserIsMemberEntity objects.</summary>
	[Serializable]
	public partial class UserIsMemberEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserIsMemberEntityFactory() : base("UserIsMemberEntity", VarioSL.Entities.EntityType.UserIsMemberEntity) { }

		/// <summary>Creates a new, empty UserIsMemberEntity object.</summary>
		/// <returns>A new, empty UserIsMemberEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserIsMemberEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserIsMember
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserKeyEntity objects.</summary>
	[Serializable]
	public partial class UserKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserKeyEntityFactory() : base("UserKeyEntity", VarioSL.Entities.EntityType.UserKeyEntity) { }

		/// <summary>Creates a new, empty UserKeyEntity object.</summary>
		/// <returns>A new, empty UserKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserLanguageEntity objects.</summary>
	[Serializable]
	public partial class UserLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserLanguageEntityFactory() : base("UserLanguageEntity", VarioSL.Entities.EntityType.UserLanguageEntity) { }

		/// <summary>Creates a new, empty UserLanguageEntity object.</summary>
		/// <returns>A new, empty UserLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserListEntity objects.</summary>
	[Serializable]
	public partial class UserListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserListEntityFactory() : base("UserListEntity", VarioSL.Entities.EntityType.UserListEntity) { }

		/// <summary>Creates a new, empty UserListEntity object.</summary>
		/// <returns>A new, empty UserListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserResourceEntity objects.</summary>
	[Serializable]
	public partial class UserResourceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserResourceEntityFactory() : base("UserResourceEntity", VarioSL.Entities.EntityType.UserResourceEntity) { }

		/// <summary>Creates a new, empty UserResourceEntity object.</summary>
		/// <returns>A new, empty UserResourceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserResourceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserResource
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserStatusEntity objects.</summary>
	[Serializable]
	public partial class UserStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserStatusEntityFactory() : base("UserStatusEntity", VarioSL.Entities.EntityType.UserStatusEntity) { }

		/// <summary>Creates a new, empty UserStatusEntity object.</summary>
		/// <returns>A new, empty UserStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserToWorkstationEntity objects.</summary>
	[Serializable]
	public partial class UserToWorkstationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserToWorkstationEntityFactory() : base("UserToWorkstationEntity", VarioSL.Entities.EntityType.UserToWorkstationEntity) { }

		/// <summary>Creates a new, empty UserToWorkstationEntity object.</summary>
		/// <returns>A new, empty UserToWorkstationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserToWorkstationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserToWorkstation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationTransactionEntity objects.</summary>
	[Serializable]
	public partial class ValidationTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationTransactionEntityFactory() : base("ValidationTransactionEntity", VarioSL.Entities.EntityType.ValidationTransactionEntity) { }

		/// <summary>Creates a new, empty ValidationTransactionEntity object.</summary>
		/// <returns>A new, empty ValidationTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VarioAddressEntity objects.</summary>
	[Serializable]
	public partial class VarioAddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VarioAddressEntityFactory() : base("VarioAddressEntity", VarioSL.Entities.EntityType.VarioAddressEntity) { }

		/// <summary>Creates a new, empty VarioAddressEntity object.</summary>
		/// <returns>A new, empty VarioAddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VarioAddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVarioAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VarioSettlementEntity objects.</summary>
	[Serializable]
	public partial class VarioSettlementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VarioSettlementEntityFactory() : base("VarioSettlementEntity", VarioSL.Entities.EntityType.VarioSettlementEntity) { }

		/// <summary>Creates a new, empty VarioSettlementEntity object.</summary>
		/// <returns>A new, empty VarioSettlementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VarioSettlementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVarioSettlement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VarioTypeOfSettlementEntity objects.</summary>
	[Serializable]
	public partial class VarioTypeOfSettlementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VarioTypeOfSettlementEntityFactory() : base("VarioTypeOfSettlementEntity", VarioSL.Entities.EntityType.VarioTypeOfSettlementEntity) { }

		/// <summary>Creates a new, empty VarioTypeOfSettlementEntity object.</summary>
		/// <returns>A new, empty VarioTypeOfSettlementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VarioTypeOfSettlementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVarioTypeOfSettlement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvKeyInfoEntity objects.</summary>
	[Serializable]
	public partial class VdvKeyInfoEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvKeyInfoEntityFactory() : base("VdvKeyInfoEntity", VarioSL.Entities.EntityType.VdvKeyInfoEntity) { }

		/// <summary>Creates a new, empty VdvKeyInfoEntity object.</summary>
		/// <returns>A new, empty VdvKeyInfoEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvKeyInfoEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvKeyInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvKeySetEntity objects.</summary>
	[Serializable]
	public partial class VdvKeySetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvKeySetEntityFactory() : base("VdvKeySetEntity", VarioSL.Entities.EntityType.VdvKeySetEntity) { }

		/// <summary>Creates a new, empty VdvKeySetEntity object.</summary>
		/// <returns>A new, empty VdvKeySetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvKeySetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvKeySet
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvLayoutEntity objects.</summary>
	[Serializable]
	public partial class VdvLayoutEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvLayoutEntityFactory() : base("VdvLayoutEntity", VarioSL.Entities.EntityType.VdvLayoutEntity) { }

		/// <summary>Creates a new, empty VdvLayoutEntity object.</summary>
		/// <returns>A new, empty VdvLayoutEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvLayoutEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvLayout
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvLayoutObjectEntity objects.</summary>
	[Serializable]
	public partial class VdvLayoutObjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvLayoutObjectEntityFactory() : base("VdvLayoutObjectEntity", VarioSL.Entities.EntityType.VdvLayoutObjectEntity) { }

		/// <summary>Creates a new, empty VdvLayoutObjectEntity object.</summary>
		/// <returns>A new, empty VdvLayoutObjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvLayoutObjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvLayoutObject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvLoadKeyMessageEntity objects.</summary>
	[Serializable]
	public partial class VdvLoadKeyMessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvLoadKeyMessageEntityFactory() : base("VdvLoadKeyMessageEntity", VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity) { }

		/// <summary>Creates a new, empty VdvLoadKeyMessageEntity object.</summary>
		/// <returns>A new, empty VdvLoadKeyMessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvLoadKeyMessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvLoadKeyMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvProductEntity objects.</summary>
	[Serializable]
	public partial class VdvProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvProductEntityFactory() : base("VdvProductEntity", VarioSL.Entities.EntityType.VdvProductEntity) { }

		/// <summary>Creates a new, empty VdvProductEntity object.</summary>
		/// <returns>A new, empty VdvProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvSamStatusEntity objects.</summary>
	[Serializable]
	public partial class VdvSamStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvSamStatusEntityFactory() : base("VdvSamStatusEntity", VarioSL.Entities.EntityType.VdvSamStatusEntity) { }

		/// <summary>Creates a new, empty VdvSamStatusEntity object.</summary>
		/// <returns>A new, empty VdvSamStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvSamStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvSamStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvTagEntity objects.</summary>
	[Serializable]
	public partial class VdvTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvTagEntityFactory() : base("VdvTagEntity", VarioSL.Entities.EntityType.VdvTagEntity) { }

		/// <summary>Creates a new, empty VdvTagEntity object.</summary>
		/// <returns>A new, empty VdvTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvTerminalEntity objects.</summary>
	[Serializable]
	public partial class VdvTerminalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvTerminalEntityFactory() : base("VdvTerminalEntity", VarioSL.Entities.EntityType.VdvTerminalEntity) { }

		/// <summary>Creates a new, empty VdvTerminalEntity object.</summary>
		/// <returns>A new, empty VdvTerminalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvTerminalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvTerminal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvTypeEntity objects.</summary>
	[Serializable]
	public partial class VdvTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvTypeEntityFactory() : base("VdvTypeEntity", VarioSL.Entities.EntityType.VdvTypeEntity) { }

		/// <summary>Creates a new, empty VdvTypeEntity object.</summary>
		/// <returns>A new, empty VdvTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkstationEntity objects.</summary>
	[Serializable]
	public partial class WorkstationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkstationEntityFactory() : base("WorkstationEntity", VarioSL.Entities.EntityType.WorkstationEntity) { }

		/// <summary>Creates a new, empty WorkstationEntity object.</summary>
		/// <returns>A new, empty WorkstationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkstationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkstation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingCancelRecTapCmlSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingCancelRecTapCmlSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingCancelRecTapCmlSettledEntityFactory() : base("AccountingCancelRecTapCmlSettledEntity", VarioSL.Entities.EntityType.AccountingCancelRecTapCmlSettledEntity) { }

		/// <summary>Creates a new, empty AccountingCancelRecTapCmlSettledEntity object.</summary>
		/// <returns>A new, empty AccountingCancelRecTapCmlSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingCancelRecTapCmlSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingCancelRecTapCmlSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingCancelRecTapTabSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingCancelRecTapTabSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingCancelRecTapTabSettledEntityFactory() : base("AccountingCancelRecTapTabSettledEntity", VarioSL.Entities.EntityType.AccountingCancelRecTapTabSettledEntity) { }

		/// <summary>Creates a new, empty AccountingCancelRecTapTabSettledEntity object.</summary>
		/// <returns>A new, empty AccountingCancelRecTapTabSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingCancelRecTapTabSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingCancelRecTapTabSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingMethodEntity objects.</summary>
	[Serializable]
	public partial class AccountingMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingMethodEntityFactory() : base("AccountingMethodEntity", VarioSL.Entities.EntityType.AccountingMethodEntity) { }

		/// <summary>Creates a new, empty AccountingMethodEntity object.</summary>
		/// <returns>A new, empty AccountingMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingModeEntity objects.</summary>
	[Serializable]
	public partial class AccountingModeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingModeEntityFactory() : base("AccountingModeEntity", VarioSL.Entities.EntityType.AccountingModeEntity) { }

		/// <summary>Creates a new, empty AccountingModeEntity object.</summary>
		/// <returns>A new, empty AccountingModeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingModeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingMode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecLoadUseCmlSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecLoadUseCmlSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecLoadUseCmlSettledEntityFactory() : base("AccountingRecLoadUseCmlSettledEntity", VarioSL.Entities.EntityType.AccountingRecLoadUseCmlSettledEntity) { }

		/// <summary>Creates a new, empty AccountingRecLoadUseCmlSettledEntity object.</summary>
		/// <returns>A new, empty AccountingRecLoadUseCmlSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecLoadUseCmlSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecLoadUseCmlSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecLoadUseTabSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecLoadUseTabSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecLoadUseTabSettledEntityFactory() : base("AccountingRecLoadUseTabSettledEntity", VarioSL.Entities.EntityType.AccountingRecLoadUseTabSettledEntity) { }

		/// <summary>Creates a new, empty AccountingRecLoadUseTabSettledEntity object.</summary>
		/// <returns>A new, empty AccountingRecLoadUseTabSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecLoadUseTabSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecLoadUseTabSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedLoadAndUseEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedLoadAndUseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedLoadAndUseEntityFactory() : base("AccountingRecognizedLoadAndUseEntity", VarioSL.Entities.EntityType.AccountingRecognizedLoadAndUseEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedLoadAndUseEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedLoadAndUseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedLoadAndUseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedLoadAndUse
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedPaymentEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedPaymentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedPaymentEntityFactory() : base("AccountingRecognizedPaymentEntity", VarioSL.Entities.EntityType.AccountingRecognizedPaymentEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedPaymentEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedPaymentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedPaymentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedPayment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedTapEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedTapEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedTapEntityFactory() : base("AccountingRecognizedTapEntity", VarioSL.Entities.EntityType.AccountingRecognizedTapEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedTapEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedTapEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedTapEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedTap
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedTapCancellationEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedTapCancellationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedTapCancellationEntityFactory() : base("AccountingRecognizedTapCancellationEntity", VarioSL.Entities.EntityType.AccountingRecognizedTapCancellationEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedTapCancellationEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedTapCancellationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedTapCancellationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedTapCancellation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedTapCmlSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedTapCmlSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedTapCmlSettledEntityFactory() : base("AccountingRecognizedTapCmlSettledEntity", VarioSL.Entities.EntityType.AccountingRecognizedTapCmlSettledEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedTapCmlSettledEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedTapCmlSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedTapCmlSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedTapCmlSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingRecognizedTapTabSettledEntity objects.</summary>
	[Serializable]
	public partial class AccountingRecognizedTapTabSettledEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingRecognizedTapTabSettledEntityFactory() : base("AccountingRecognizedTapTabSettledEntity", VarioSL.Entities.EntityType.AccountingRecognizedTapTabSettledEntity) { }

		/// <summary>Creates a new, empty AccountingRecognizedTapTabSettledEntity object.</summary>
		/// <returns>A new, empty AccountingRecognizedTapTabSettledEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingRecognizedTapTabSettledEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingRecognizedTapTabSettled
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountingReconciledPaymentEntity objects.</summary>
	[Serializable]
	public partial class AccountingReconciledPaymentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountingReconciledPaymentEntityFactory() : base("AccountingReconciledPaymentEntity", VarioSL.Entities.EntityType.AccountingReconciledPaymentEntity) { }

		/// <summary>Creates a new, empty AccountingReconciledPaymentEntity object.</summary>
		/// <returns>A new, empty AccountingReconciledPaymentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountingReconciledPaymentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountingReconciledPayment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountMessageSettingEntity objects.</summary>
	[Serializable]
	public partial class AccountMessageSettingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountMessageSettingEntityFactory() : base("AccountMessageSettingEntity", VarioSL.Entities.EntityType.AccountMessageSettingEntity) { }

		/// <summary>Creates a new, empty AccountMessageSettingEntity object.</summary>
		/// <returns>A new, empty AccountMessageSettingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountMessageSettingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountMessageSetting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdditionalDataEntity objects.</summary>
	[Serializable]
	public partial class AdditionalDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdditionalDataEntityFactory() : base("AdditionalDataEntity", VarioSL.Entities.EntityType.AdditionalDataEntity) { }

		/// <summary>Creates a new, empty AdditionalDataEntity object.</summary>
		/// <returns>A new, empty AdditionalDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdditionalDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdditionalData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AddressEntity objects.</summary>
	[Serializable]
	public partial class AddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AddressEntityFactory() : base("AddressEntity", VarioSL.Entities.EntityType.AddressEntity) { }

		/// <summary>Creates a new, empty AddressEntity object.</summary>
		/// <returns>A new, empty AddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AddressBookEntryEntity objects.</summary>
	[Serializable]
	public partial class AddressBookEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AddressBookEntryEntityFactory() : base("AddressBookEntryEntity", VarioSL.Entities.EntityType.AddressBookEntryEntity) { }

		/// <summary>Creates a new, empty AddressBookEntryEntity object.</summary>
		/// <returns>A new, empty AddressBookEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AddressBookEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAddressBookEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AddressBookEntryTypeEntity objects.</summary>
	[Serializable]
	public partial class AddressBookEntryTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AddressBookEntryTypeEntityFactory() : base("AddressBookEntryTypeEntity", VarioSL.Entities.EntityType.AddressBookEntryTypeEntity) { }

		/// <summary>Creates a new, empty AddressBookEntryTypeEntity object.</summary>
		/// <returns>A new, empty AddressBookEntryTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AddressBookEntryTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAddressBookEntryType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AddressTypeEntity objects.</summary>
	[Serializable]
	public partial class AddressTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AddressTypeEntityFactory() : base("AddressTypeEntity", VarioSL.Entities.EntityType.AddressTypeEntity) { }

		/// <summary>Creates a new, empty AddressTypeEntity object.</summary>
		/// <returns>A new, empty AddressTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AddressTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAddressType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AggregationFunctionEntity objects.</summary>
	[Serializable]
	public partial class AggregationFunctionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AggregationFunctionEntityFactory() : base("AggregationFunctionEntity", VarioSL.Entities.EntityType.AggregationFunctionEntity) { }

		/// <summary>Creates a new, empty AggregationFunctionEntity object.</summary>
		/// <returns>A new, empty AggregationFunctionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AggregationFunctionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAggregationFunction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AggregationTypeEntity objects.</summary>
	[Serializable]
	public partial class AggregationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AggregationTypeEntityFactory() : base("AggregationTypeEntity", VarioSL.Entities.EntityType.AggregationTypeEntity) { }

		/// <summary>Creates a new, empty AggregationTypeEntity object.</summary>
		/// <returns>A new, empty AggregationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AggregationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAggregationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApportionEntity objects.</summary>
	[Serializable]
	public partial class ApportionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApportionEntityFactory() : base("ApportionEntity", VarioSL.Entities.EntityType.ApportionEntity) { }

		/// <summary>Creates a new, empty ApportionEntity object.</summary>
		/// <returns>A new, empty ApportionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApportionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApportion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApportionPassRevenueRecognitionEntity objects.</summary>
	[Serializable]
	public partial class ApportionPassRevenueRecognitionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApportionPassRevenueRecognitionEntityFactory() : base("ApportionPassRevenueRecognitionEntity", VarioSL.Entities.EntityType.ApportionPassRevenueRecognitionEntity) { }

		/// <summary>Creates a new, empty ApportionPassRevenueRecognitionEntity object.</summary>
		/// <returns>A new, empty ApportionPassRevenueRecognitionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApportionPassRevenueRecognitionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApportionPassRevenueRecognition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ArchiveStateEntity objects.</summary>
	[Serializable]
	public partial class ArchiveStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ArchiveStateEntityFactory() : base("ArchiveStateEntity", VarioSL.Entities.EntityType.ArchiveStateEntity) { }

		/// <summary>Creates a new, empty ArchiveStateEntity object.</summary>
		/// <returns>A new, empty ArchiveStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ArchiveStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewArchiveState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttributeToMobilityProviderEntity objects.</summary>
	[Serializable]
	public partial class AttributeToMobilityProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttributeToMobilityProviderEntityFactory() : base("AttributeToMobilityProviderEntity", VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity) { }

		/// <summary>Creates a new, empty AttributeToMobilityProviderEntity object.</summary>
		/// <returns>A new, empty AttributeToMobilityProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttributeToMobilityProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttributeToMobilityProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AuditRegisterEntity objects.</summary>
	[Serializable]
	public partial class AuditRegisterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AuditRegisterEntityFactory() : base("AuditRegisterEntity", VarioSL.Entities.EntityType.AuditRegisterEntity) { }

		/// <summary>Creates a new, empty AuditRegisterEntity object.</summary>
		/// <returns>A new, empty AuditRegisterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AuditRegisterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAuditRegister
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AuditRegisterValueEntity objects.</summary>
	[Serializable]
	public partial class AuditRegisterValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AuditRegisterValueEntityFactory() : base("AuditRegisterValueEntity", VarioSL.Entities.EntityType.AuditRegisterValueEntity) { }

		/// <summary>Creates a new, empty AuditRegisterValueEntity object.</summary>
		/// <returns>A new, empty AuditRegisterValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AuditRegisterValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAuditRegisterValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AuditRegisterValueTypeEntity objects.</summary>
	[Serializable]
	public partial class AuditRegisterValueTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AuditRegisterValueTypeEntityFactory() : base("AuditRegisterValueTypeEntity", VarioSL.Entities.EntityType.AuditRegisterValueTypeEntity) { }

		/// <summary>Creates a new, empty AuditRegisterValueTypeEntity object.</summary>
		/// <returns>A new, empty AuditRegisterValueTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AuditRegisterValueTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAuditRegisterValueType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AutoloadSettingEntity objects.</summary>
	[Serializable]
	public partial class AutoloadSettingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AutoloadSettingEntityFactory() : base("AutoloadSettingEntity", VarioSL.Entities.EntityType.AutoloadSettingEntity) { }

		/// <summary>Creates a new, empty AutoloadSettingEntity object.</summary>
		/// <returns>A new, empty AutoloadSettingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AutoloadSettingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAutoloadSetting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AutoloadToPaymentOptionEntity objects.</summary>
	[Serializable]
	public partial class AutoloadToPaymentOptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AutoloadToPaymentOptionEntityFactory() : base("AutoloadToPaymentOptionEntity", VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity) { }

		/// <summary>Creates a new, empty AutoloadToPaymentOptionEntity object.</summary>
		/// <returns>A new, empty AutoloadToPaymentOptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AutoloadToPaymentOptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAutoloadToPaymentOption
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BadCardEntity objects.</summary>
	[Serializable]
	public partial class BadCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BadCardEntityFactory() : base("BadCardEntity", VarioSL.Entities.EntityType.BadCardEntity) { }

		/// <summary>Creates a new, empty BadCardEntity object.</summary>
		/// <returns>A new, empty BadCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BadCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBadCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BadCheckEntity objects.</summary>
	[Serializable]
	public partial class BadCheckEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BadCheckEntityFactory() : base("BadCheckEntity", VarioSL.Entities.EntityType.BadCheckEntity) { }

		/// <summary>Creates a new, empty BadCheckEntity object.</summary>
		/// <returns>A new, empty BadCheckEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BadCheckEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBadCheck
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankConnectionDataEntity objects.</summary>
	[Serializable]
	public partial class BankConnectionDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankConnectionDataEntityFactory() : base("BankConnectionDataEntity", VarioSL.Entities.EntityType.BankConnectionDataEntity) { }

		/// <summary>Creates a new, empty BankConnectionDataEntity object.</summary>
		/// <returns>A new, empty BankConnectionDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankConnectionDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBankConnectionData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankStatementEntity objects.</summary>
	[Serializable]
	public partial class BankStatementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankStatementEntityFactory() : base("BankStatementEntity", VarioSL.Entities.EntityType.BankStatementEntity) { }

		/// <summary>Creates a new, empty BankStatementEntity object.</summary>
		/// <returns>A new, empty BankStatementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankStatementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBankStatement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankStatementRecordTypeEntity objects.</summary>
	[Serializable]
	public partial class BankStatementRecordTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankStatementRecordTypeEntityFactory() : base("BankStatementRecordTypeEntity", VarioSL.Entities.EntityType.BankStatementRecordTypeEntity) { }

		/// <summary>Creates a new, empty BankStatementRecordTypeEntity object.</summary>
		/// <returns>A new, empty BankStatementRecordTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankStatementRecordTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBankStatementRecordType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankStatementStateEntity objects.</summary>
	[Serializable]
	public partial class BankStatementStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankStatementStateEntityFactory() : base("BankStatementStateEntity", VarioSL.Entities.EntityType.BankStatementStateEntity) { }

		/// <summary>Creates a new, empty BankStatementStateEntity object.</summary>
		/// <returns>A new, empty BankStatementStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankStatementStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBankStatementState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BankStatementVerificationEntity objects.</summary>
	[Serializable]
	public partial class BankStatementVerificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BankStatementVerificationEntityFactory() : base("BankStatementVerificationEntity", VarioSL.Entities.EntityType.BankStatementVerificationEntity) { }

		/// <summary>Creates a new, empty BankStatementVerificationEntity object.</summary>
		/// <returns>A new, empty BankStatementVerificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BankStatementVerificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBankStatementVerification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BookingEntity objects.</summary>
	[Serializable]
	public partial class BookingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BookingEntityFactory() : base("BookingEntity", VarioSL.Entities.EntityType.BookingEntity) { }

		/// <summary>Creates a new, empty BookingEntity object.</summary>
		/// <returns>A new, empty BookingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BookingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBooking
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BookingItemEntity objects.</summary>
	[Serializable]
	public partial class BookingItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BookingItemEntityFactory() : base("BookingItemEntity", VarioSL.Entities.EntityType.BookingItemEntity) { }

		/// <summary>Creates a new, empty BookingItemEntity object.</summary>
		/// <returns>A new, empty BookingItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BookingItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBookingItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BookingItemPaymentEntity objects.</summary>
	[Serializable]
	public partial class BookingItemPaymentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BookingItemPaymentEntityFactory() : base("BookingItemPaymentEntity", VarioSL.Entities.EntityType.BookingItemPaymentEntity) { }

		/// <summary>Creates a new, empty BookingItemPaymentEntity object.</summary>
		/// <returns>A new, empty BookingItemPaymentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BookingItemPaymentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBookingItemPayment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BookingItemPaymentTypeEntity objects.</summary>
	[Serializable]
	public partial class BookingItemPaymentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BookingItemPaymentTypeEntityFactory() : base("BookingItemPaymentTypeEntity", VarioSL.Entities.EntityType.BookingItemPaymentTypeEntity) { }

		/// <summary>Creates a new, empty BookingItemPaymentTypeEntity object.</summary>
		/// <returns>A new, empty BookingItemPaymentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BookingItemPaymentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBookingItemPaymentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BookingTypeEntity objects.</summary>
	[Serializable]
	public partial class BookingTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BookingTypeEntityFactory() : base("BookingTypeEntity", VarioSL.Entities.EntityType.BookingTypeEntity) { }

		/// <summary>Creates a new, empty BookingTypeEntity object.</summary>
		/// <returns>A new, empty BookingTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BookingTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBookingType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CappingJournalEntity objects.</summary>
	[Serializable]
	public partial class CappingJournalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CappingJournalEntityFactory() : base("CappingJournalEntity", VarioSL.Entities.EntityType.CappingJournalEntity) { }

		/// <summary>Creates a new, empty CappingJournalEntity object.</summary>
		/// <returns>A new, empty CappingJournalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CappingJournalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCappingJournal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CappingStateEntity objects.</summary>
	[Serializable]
	public partial class CappingStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CappingStateEntityFactory() : base("CappingStateEntity", VarioSL.Entities.EntityType.CappingStateEntity) { }

		/// <summary>Creates a new, empty CappingStateEntity object.</summary>
		/// <returns>A new, empty CappingStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CappingStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCappingState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardEntity objects.</summary>
	[Serializable]
	public partial class CardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardEntityFactory() : base("CardEntity", VarioSL.Entities.EntityType.CardEntity) { }

		/// <summary>Creates a new, empty CardEntity object.</summary>
		/// <returns>A new, empty CardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardBodyTypeEntity objects.</summary>
	[Serializable]
	public partial class CardBodyTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardBodyTypeEntityFactory() : base("CardBodyTypeEntity", VarioSL.Entities.EntityType.CardBodyTypeEntity) { }

		/// <summary>Creates a new, empty CardBodyTypeEntity object.</summary>
		/// <returns>A new, empty CardBodyTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardBodyTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardBodyType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardDormancyEntity objects.</summary>
	[Serializable]
	public partial class CardDormancyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardDormancyEntityFactory() : base("CardDormancyEntity", VarioSL.Entities.EntityType.CardDormancyEntity) { }

		/// <summary>Creates a new, empty CardDormancyEntity object.</summary>
		/// <returns>A new, empty CardDormancyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardDormancyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardDormancy
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardEventEntity objects.</summary>
	[Serializable]
	public partial class CardEventEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardEventEntityFactory() : base("CardEventEntity", VarioSL.Entities.EntityType.CardEventEntity) { }

		/// <summary>Creates a new, empty CardEventEntity object.</summary>
		/// <returns>A new, empty CardEventEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardEventEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardEvent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardEventTypeEntity objects.</summary>
	[Serializable]
	public partial class CardEventTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardEventTypeEntityFactory() : base("CardEventTypeEntity", VarioSL.Entities.EntityType.CardEventTypeEntity) { }

		/// <summary>Creates a new, empty CardEventTypeEntity object.</summary>
		/// <returns>A new, empty CardEventTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardEventTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardEventType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardFulfillmentStatusEntity objects.</summary>
	[Serializable]
	public partial class CardFulfillmentStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardFulfillmentStatusEntityFactory() : base("CardFulfillmentStatusEntity", VarioSL.Entities.EntityType.CardFulfillmentStatusEntity) { }

		/// <summary>Creates a new, empty CardFulfillmentStatusEntity object.</summary>
		/// <returns>A new, empty CardFulfillmentStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardFulfillmentStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardFulfillmentStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardHolderEntity objects.</summary>
	[Serializable]
	public partial class CardHolderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardHolderEntityFactory() : base("CardHolderEntity", VarioSL.Entities.EntityType.CardHolderEntity) { }

		/// <summary>Creates a new, empty CardHolderEntity object.</summary>
		/// <returns>A new, empty CardHolderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardHolderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardHolder
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("CardHolderEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardHolderOrganizationEntity objects.</summary>
	[Serializable]
	public partial class CardHolderOrganizationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardHolderOrganizationEntityFactory() : base("CardHolderOrganizationEntity", VarioSL.Entities.EntityType.CardHolderOrganizationEntity) { }

		/// <summary>Creates a new, empty CardHolderOrganizationEntity object.</summary>
		/// <returns>A new, empty CardHolderOrganizationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardHolderOrganizationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardHolderOrganization
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardKeyEntity objects.</summary>
	[Serializable]
	public partial class CardKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardKeyEntityFactory() : base("CardKeyEntity", VarioSL.Entities.EntityType.CardKeyEntity) { }

		/// <summary>Creates a new, empty CardKeyEntity object.</summary>
		/// <returns>A new, empty CardKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardLiabilityEntity objects.</summary>
	[Serializable]
	public partial class CardLiabilityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardLiabilityEntityFactory() : base("CardLiabilityEntity", VarioSL.Entities.EntityType.CardLiabilityEntity) { }

		/// <summary>Creates a new, empty CardLiabilityEntity object.</summary>
		/// <returns>A new, empty CardLiabilityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardLiabilityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardLiability
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardLinkEntity objects.</summary>
	[Serializable]
	public partial class CardLinkEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardLinkEntityFactory() : base("CardLinkEntity", VarioSL.Entities.EntityType.CardLinkEntity) { }

		/// <summary>Creates a new, empty CardLinkEntity object.</summary>
		/// <returns>A new, empty CardLinkEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardLinkEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardLink
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardOrderDetailEntity objects.</summary>
	[Serializable]
	public partial class CardOrderDetailEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardOrderDetailEntityFactory() : base("CardOrderDetailEntity", VarioSL.Entities.EntityType.CardOrderDetailEntity) { }

		/// <summary>Creates a new, empty CardOrderDetailEntity object.</summary>
		/// <returns>A new, empty CardOrderDetailEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardOrderDetailEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardOrderDetail
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardPhysicalDetailEntity objects.</summary>
	[Serializable]
	public partial class CardPhysicalDetailEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardPhysicalDetailEntityFactory() : base("CardPhysicalDetailEntity", VarioSL.Entities.EntityType.CardPhysicalDetailEntity) { }

		/// <summary>Creates a new, empty CardPhysicalDetailEntity object.</summary>
		/// <returns>A new, empty CardPhysicalDetailEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardPhysicalDetailEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardPhysicalDetail
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardPrintingTypeEntity objects.</summary>
	[Serializable]
	public partial class CardPrintingTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardPrintingTypeEntityFactory() : base("CardPrintingTypeEntity", VarioSL.Entities.EntityType.CardPrintingTypeEntity) { }

		/// <summary>Creates a new, empty CardPrintingTypeEntity object.</summary>
		/// <returns>A new, empty CardPrintingTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardPrintingTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardPrintingType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardStockTransferEntity objects.</summary>
	[Serializable]
	public partial class CardStockTransferEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardStockTransferEntityFactory() : base("CardStockTransferEntity", VarioSL.Entities.EntityType.CardStockTransferEntity) { }

		/// <summary>Creates a new, empty CardStockTransferEntity object.</summary>
		/// <returns>A new, empty CardStockTransferEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardStockTransferEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardStockTransfer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardToContractEntity objects.</summary>
	[Serializable]
	public partial class CardToContractEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardToContractEntityFactory() : base("CardToContractEntity", VarioSL.Entities.EntityType.CardToContractEntity) { }

		/// <summary>Creates a new, empty CardToContractEntity object.</summary>
		/// <returns>A new, empty CardToContractEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardToContractEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardToContract
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardToRuleViolationEntity objects.</summary>
	[Serializable]
	public partial class CardToRuleViolationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardToRuleViolationEntityFactory() : base("CardToRuleViolationEntity", VarioSL.Entities.EntityType.CardToRuleViolationEntity) { }

		/// <summary>Creates a new, empty CardToRuleViolationEntity object.</summary>
		/// <returns>A new, empty CardToRuleViolationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardToRuleViolationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardToRuleViolation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CardWorkItemEntity objects.</summary>
	[Serializable]
	public partial class CardWorkItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CardWorkItemEntityFactory() : base("CardWorkItemEntity", VarioSL.Entities.EntityType.CardWorkItemEntity) { }

		/// <summary>Creates a new, empty CardWorkItemEntity object.</summary>
		/// <returns>A new, empty CardWorkItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CardWorkItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCardWorkItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("CardWorkItemEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CertificateEntity objects.</summary>
	[Serializable]
	public partial class CertificateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CertificateEntityFactory() : base("CertificateEntity", VarioSL.Entities.EntityType.CertificateEntity) { }

		/// <summary>Creates a new, empty CertificateEntity object.</summary>
		/// <returns>A new, empty CertificateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CertificateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCertificate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CertificateFormatEntity objects.</summary>
	[Serializable]
	public partial class CertificateFormatEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CertificateFormatEntityFactory() : base("CertificateFormatEntity", VarioSL.Entities.EntityType.CertificateFormatEntity) { }

		/// <summary>Creates a new, empty CertificateFormatEntity object.</summary>
		/// <returns>A new, empty CertificateFormatEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CertificateFormatEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCertificateFormat
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CertificatePurposeEntity objects.</summary>
	[Serializable]
	public partial class CertificatePurposeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CertificatePurposeEntityFactory() : base("CertificatePurposeEntity", VarioSL.Entities.EntityType.CertificatePurposeEntity) { }

		/// <summary>Creates a new, empty CertificatePurposeEntity object.</summary>
		/// <returns>A new, empty CertificatePurposeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CertificatePurposeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCertificatePurpose
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CertificateTypeEntity objects.</summary>
	[Serializable]
	public partial class CertificateTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CertificateTypeEntityFactory() : base("CertificateTypeEntity", VarioSL.Entities.EntityType.CertificateTypeEntity) { }

		/// <summary>Creates a new, empty CertificateTypeEntity object.</summary>
		/// <returns>A new, empty CertificateTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CertificateTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCertificateType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClaimEntity objects.</summary>
	[Serializable]
	public partial class ClaimEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClaimEntityFactory() : base("ClaimEntity", VarioSL.Entities.EntityType.ClaimEntity) { }

		/// <summary>Creates a new, empty ClaimEntity object.</summary>
		/// <returns>A new, empty ClaimEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClaimEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClaim
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientFilterTypeEntity objects.</summary>
	[Serializable]
	public partial class ClientFilterTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientFilterTypeEntityFactory() : base("ClientFilterTypeEntity", VarioSL.Entities.EntityType.ClientFilterTypeEntity) { }

		/// <summary>Creates a new, empty ClientFilterTypeEntity object.</summary>
		/// <returns>A new, empty ClientFilterTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientFilterTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientFilterType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloseoutPeriodEntity objects.</summary>
	[Serializable]
	public partial class CloseoutPeriodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloseoutPeriodEntityFactory() : base("CloseoutPeriodEntity", VarioSL.Entities.EntityType.CloseoutPeriodEntity) { }

		/// <summary>Creates a new, empty CloseoutPeriodEntity object.</summary>
		/// <returns>A new, empty CloseoutPeriodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloseoutPeriodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloseoutPeriod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloseoutStateEntity objects.</summary>
	[Serializable]
	public partial class CloseoutStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloseoutStateEntityFactory() : base("CloseoutStateEntity", VarioSL.Entities.EntityType.CloseoutStateEntity) { }

		/// <summary>Creates a new, empty CloseoutStateEntity object.</summary>
		/// <returns>A new, empty CloseoutStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloseoutStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloseoutState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloseoutTypeEntity objects.</summary>
	[Serializable]
	public partial class CloseoutTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloseoutTypeEntityFactory() : base("CloseoutTypeEntity", VarioSL.Entities.EntityType.CloseoutTypeEntity) { }

		/// <summary>Creates a new, empty CloseoutTypeEntity object.</summary>
		/// <returns>A new, empty CloseoutTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloseoutTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloseoutType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CommentEntity objects.</summary>
	[Serializable]
	public partial class CommentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CommentEntityFactory() : base("CommentEntity", VarioSL.Entities.EntityType.CommentEntity) { }

		/// <summary>Creates a new, empty CommentEntity object.</summary>
		/// <returns>A new, empty CommentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CommentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewComment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CommentPriorityEntity objects.</summary>
	[Serializable]
	public partial class CommentPriorityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CommentPriorityEntityFactory() : base("CommentPriorityEntity", VarioSL.Entities.EntityType.CommentPriorityEntity) { }

		/// <summary>Creates a new, empty CommentPriorityEntity object.</summary>
		/// <returns>A new, empty CommentPriorityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CommentPriorityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCommentPriority
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CommentTypeEntity objects.</summary>
	[Serializable]
	public partial class CommentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CommentTypeEntityFactory() : base("CommentTypeEntity", VarioSL.Entities.EntityType.CommentTypeEntity) { }

		/// <summary>Creates a new, empty CommentTypeEntity object.</summary>
		/// <returns>A new, empty CommentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CommentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCommentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConfigurationEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConfigurationEntityFactory() : base("ConfigurationEntity", VarioSL.Entities.EntityType.ConfigurationEntity) { }

		/// <summary>Creates a new, empty ConfigurationEntity object.</summary>
		/// <returns>A new, empty ConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConfigurationDataTypeEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationDataTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConfigurationDataTypeEntityFactory() : base("ConfigurationDataTypeEntity", VarioSL.Entities.EntityType.ConfigurationDataTypeEntity) { }

		/// <summary>Creates a new, empty ConfigurationDataTypeEntity object.</summary>
		/// <returns>A new, empty ConfigurationDataTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConfigurationDataTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfigurationDataType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConfigurationDefinitionEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationDefinitionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConfigurationDefinitionEntityFactory() : base("ConfigurationDefinitionEntity", VarioSL.Entities.EntityType.ConfigurationDefinitionEntity) { }

		/// <summary>Creates a new, empty ConfigurationDefinitionEntity object.</summary>
		/// <returns>A new, empty ConfigurationDefinitionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConfigurationDefinitionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfigurationDefinition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConfigurationOverwriteEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationOverwriteEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConfigurationOverwriteEntityFactory() : base("ConfigurationOverwriteEntity", VarioSL.Entities.EntityType.ConfigurationOverwriteEntity) { }

		/// <summary>Creates a new, empty ConfigurationOverwriteEntity object.</summary>
		/// <returns>A new, empty ConfigurationOverwriteEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConfigurationOverwriteEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfigurationOverwrite
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContentItemEntity objects.</summary>
	[Serializable]
	public partial class ContentItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContentItemEntityFactory() : base("ContentItemEntity", VarioSL.Entities.EntityType.ContentItemEntity) { }

		/// <summary>Creates a new, empty ContentItemEntity object.</summary>
		/// <returns>A new, empty ContentItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContentItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContentItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContentTypeEntity objects.</summary>
	[Serializable]
	public partial class ContentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContentTypeEntityFactory() : base("ContentTypeEntity", VarioSL.Entities.EntityType.ContentTypeEntity) { }

		/// <summary>Creates a new, empty ContentTypeEntity object.</summary>
		/// <returns>A new, empty ContentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractEntity objects.</summary>
	[Serializable]
	public partial class ContractEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractEntityFactory() : base("ContractEntity", VarioSL.Entities.EntityType.ContractEntity) { }

		/// <summary>Creates a new, empty ContractEntity object.</summary>
		/// <returns>A new, empty ContractEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContract
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractAddressEntity objects.</summary>
	[Serializable]
	public partial class ContractAddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractAddressEntityFactory() : base("ContractAddressEntity", VarioSL.Entities.EntityType.ContractAddressEntity) { }

		/// <summary>Creates a new, empty ContractAddressEntity object.</summary>
		/// <returns>A new, empty ContractAddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractAddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractDetailsEntity objects.</summary>
	[Serializable]
	public partial class ContractDetailsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractDetailsEntityFactory() : base("ContractDetailsEntity", VarioSL.Entities.EntityType.ContractDetailsEntity) { }

		/// <summary>Creates a new, empty ContractDetailsEntity object.</summary>
		/// <returns>A new, empty ContractDetailsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractDetailsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractDetails
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractHistoryEntity objects.</summary>
	[Serializable]
	public partial class ContractHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractHistoryEntityFactory() : base("ContractHistoryEntity", VarioSL.Entities.EntityType.ContractHistoryEntity) { }

		/// <summary>Creates a new, empty ContractHistoryEntity object.</summary>
		/// <returns>A new, empty ContractHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractLinkEntity objects.</summary>
	[Serializable]
	public partial class ContractLinkEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractLinkEntityFactory() : base("ContractLinkEntity", VarioSL.Entities.EntityType.ContractLinkEntity) { }

		/// <summary>Creates a new, empty ContractLinkEntity object.</summary>
		/// <returns>A new, empty ContractLinkEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractLinkEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractLink
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractStateEntity objects.</summary>
	[Serializable]
	public partial class ContractStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractStateEntityFactory() : base("ContractStateEntity", VarioSL.Entities.EntityType.ContractStateEntity) { }

		/// <summary>Creates a new, empty ContractStateEntity object.</summary>
		/// <returns>A new, empty ContractStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractTerminationEntity objects.</summary>
	[Serializable]
	public partial class ContractTerminationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractTerminationEntityFactory() : base("ContractTerminationEntity", VarioSL.Entities.EntityType.ContractTerminationEntity) { }

		/// <summary>Creates a new, empty ContractTerminationEntity object.</summary>
		/// <returns>A new, empty ContractTerminationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractTerminationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractTermination
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractTerminationTypeEntity objects.</summary>
	[Serializable]
	public partial class ContractTerminationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractTerminationTypeEntityFactory() : base("ContractTerminationTypeEntity", VarioSL.Entities.EntityType.ContractTerminationTypeEntity) { }

		/// <summary>Creates a new, empty ContractTerminationTypeEntity object.</summary>
		/// <returns>A new, empty ContractTerminationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractTerminationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractTerminationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractToPaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class ContractToPaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractToPaymentMethodEntityFactory() : base("ContractToPaymentMethodEntity", VarioSL.Entities.EntityType.ContractToPaymentMethodEntity) { }

		/// <summary>Creates a new, empty ContractToPaymentMethodEntity object.</summary>
		/// <returns>A new, empty ContractToPaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractToPaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractToPaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractToProviderEntity objects.</summary>
	[Serializable]
	public partial class ContractToProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractToProviderEntityFactory() : base("ContractToProviderEntity", VarioSL.Entities.EntityType.ContractToProviderEntity) { }

		/// <summary>Creates a new, empty ContractToProviderEntity object.</summary>
		/// <returns>A new, empty ContractToProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractToProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractToProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractTypeEntity objects.</summary>
	[Serializable]
	public partial class ContractTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractTypeEntityFactory() : base("ContractTypeEntity", VarioSL.Entities.EntityType.ContractTypeEntity) { }

		/// <summary>Creates a new, empty ContractTypeEntity object.</summary>
		/// <returns>A new, empty ContractTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ContractWorkItemEntity objects.</summary>
	[Serializable]
	public partial class ContractWorkItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ContractWorkItemEntityFactory() : base("ContractWorkItemEntity", VarioSL.Entities.EntityType.ContractWorkItemEntity) { }

		/// <summary>Creates a new, empty ContractWorkItemEntity object.</summary>
		/// <returns>A new, empty ContractWorkItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ContractWorkItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewContractWorkItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("ContractWorkItemEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CoordinateEntity objects.</summary>
	[Serializable]
	public partial class CoordinateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CoordinateEntityFactory() : base("CoordinateEntity", VarioSL.Entities.EntityType.CoordinateEntity) { }

		/// <summary>Creates a new, empty CoordinateEntity object.</summary>
		/// <returns>A new, empty CoordinateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CoordinateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCoordinate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CountryEntity objects.</summary>
	[Serializable]
	public partial class CountryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CountryEntityFactory() : base("CountryEntity", VarioSL.Entities.EntityType.CountryEntity) { }

		/// <summary>Creates a new, empty CountryEntity object.</summary>
		/// <returns>A new, empty CountryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CountryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCountry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditCardAuthorizationEntity objects.</summary>
	[Serializable]
	public partial class CreditCardAuthorizationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditCardAuthorizationEntityFactory() : base("CreditCardAuthorizationEntity", VarioSL.Entities.EntityType.CreditCardAuthorizationEntity) { }

		/// <summary>Creates a new, empty CreditCardAuthorizationEntity object.</summary>
		/// <returns>A new, empty CreditCardAuthorizationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditCardAuthorizationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditCardAuthorization
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditCardAuthorizationLogEntity objects.</summary>
	[Serializable]
	public partial class CreditCardAuthorizationLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditCardAuthorizationLogEntityFactory() : base("CreditCardAuthorizationLogEntity", VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity) { }

		/// <summary>Creates a new, empty CreditCardAuthorizationLogEntity object.</summary>
		/// <returns>A new, empty CreditCardAuthorizationLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditCardAuthorizationLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditCardAuthorizationLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditCardMerchantEntity objects.</summary>
	[Serializable]
	public partial class CreditCardMerchantEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditCardMerchantEntityFactory() : base("CreditCardMerchantEntity", VarioSL.Entities.EntityType.CreditCardMerchantEntity) { }

		/// <summary>Creates a new, empty CreditCardMerchantEntity object.</summary>
		/// <returns>A new, empty CreditCardMerchantEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditCardMerchantEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditCardMerchant
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditCardNetworkEntity objects.</summary>
	[Serializable]
	public partial class CreditCardNetworkEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditCardNetworkEntityFactory() : base("CreditCardNetworkEntity", VarioSL.Entities.EntityType.CreditCardNetworkEntity) { }

		/// <summary>Creates a new, empty CreditCardNetworkEntity object.</summary>
		/// <returns>A new, empty CreditCardNetworkEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditCardNetworkEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditCardNetwork
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CreditCardTerminalEntity objects.</summary>
	[Serializable]
	public partial class CreditCardTerminalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CreditCardTerminalEntityFactory() : base("CreditCardTerminalEntity", VarioSL.Entities.EntityType.CreditCardTerminalEntity) { }

		/// <summary>Creates a new, empty CreditCardTerminalEntity object.</summary>
		/// <returns>A new, empty CreditCardTerminalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CreditCardTerminalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCreditCardTerminal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoCertificateEntity objects.</summary>
	[Serializable]
	public partial class CryptoCertificateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoCertificateEntityFactory() : base("CryptoCertificateEntity", VarioSL.Entities.EntityType.CryptoCertificateEntity) { }

		/// <summary>Creates a new, empty CryptoCertificateEntity object.</summary>
		/// <returns>A new, empty CryptoCertificateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoCertificateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoCertificate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoContentEntity objects.</summary>
	[Serializable]
	public partial class CryptoContentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoContentEntityFactory() : base("CryptoContentEntity", VarioSL.Entities.EntityType.CryptoContentEntity) { }

		/// <summary>Creates a new, empty CryptoContentEntity object.</summary>
		/// <returns>A new, empty CryptoContentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoContentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoContent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoCryptogramArchiveEntity objects.</summary>
	[Serializable]
	public partial class CryptoCryptogramArchiveEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoCryptogramArchiveEntityFactory() : base("CryptoCryptogramArchiveEntity", VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity) { }

		/// <summary>Creates a new, empty CryptoCryptogramArchiveEntity object.</summary>
		/// <returns>A new, empty CryptoCryptogramArchiveEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoCryptogramArchiveEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoCryptogramArchive
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptogramCertificateEntity objects.</summary>
	[Serializable]
	public partial class CryptogramCertificateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptogramCertificateEntityFactory() : base("CryptogramCertificateEntity", VarioSL.Entities.EntityType.CryptogramCertificateEntity) { }

		/// <summary>Creates a new, empty CryptogramCertificateEntity object.</summary>
		/// <returns>A new, empty CryptogramCertificateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptogramCertificateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptogramCertificate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptogramCertificateTypeEntity objects.</summary>
	[Serializable]
	public partial class CryptogramCertificateTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptogramCertificateTypeEntityFactory() : base("CryptogramCertificateTypeEntity", VarioSL.Entities.EntityType.CryptogramCertificateTypeEntity) { }

		/// <summary>Creates a new, empty CryptogramCertificateTypeEntity object.</summary>
		/// <returns>A new, empty CryptogramCertificateTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptogramCertificateTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptogramCertificateType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptogramErrorTypeEntity objects.</summary>
	[Serializable]
	public partial class CryptogramErrorTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptogramErrorTypeEntityFactory() : base("CryptogramErrorTypeEntity", VarioSL.Entities.EntityType.CryptogramErrorTypeEntity) { }

		/// <summary>Creates a new, empty CryptogramErrorTypeEntity object.</summary>
		/// <returns>A new, empty CryptogramErrorTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptogramErrorTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptogramErrorType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptogramLoadStatusEntity objects.</summary>
	[Serializable]
	public partial class CryptogramLoadStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptogramLoadStatusEntityFactory() : base("CryptogramLoadStatusEntity", VarioSL.Entities.EntityType.CryptogramLoadStatusEntity) { }

		/// <summary>Creates a new, empty CryptogramLoadStatusEntity object.</summary>
		/// <returns>A new, empty CryptogramLoadStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptogramLoadStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptogramLoadStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoOperatorActivationkeyEntity objects.</summary>
	[Serializable]
	public partial class CryptoOperatorActivationkeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoOperatorActivationkeyEntityFactory() : base("CryptoOperatorActivationkeyEntity", VarioSL.Entities.EntityType.CryptoOperatorActivationkeyEntity) { }

		/// <summary>Creates a new, empty CryptoOperatorActivationkeyEntity object.</summary>
		/// <returns>A new, empty CryptoOperatorActivationkeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoOperatorActivationkeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoOperatorActivationkey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoQArchiveEntity objects.</summary>
	[Serializable]
	public partial class CryptoQArchiveEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoQArchiveEntityFactory() : base("CryptoQArchiveEntity", VarioSL.Entities.EntityType.CryptoQArchiveEntity) { }

		/// <summary>Creates a new, empty CryptoQArchiveEntity object.</summary>
		/// <returns>A new, empty CryptoQArchiveEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoQArchiveEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoQArchive
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoResourceEntity objects.</summary>
	[Serializable]
	public partial class CryptoResourceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoResourceEntityFactory() : base("CryptoResourceEntity", VarioSL.Entities.EntityType.CryptoResourceEntity) { }

		/// <summary>Creates a new, empty CryptoResourceEntity object.</summary>
		/// <returns>A new, empty CryptoResourceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoResourceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoResource
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoResourceDataEntity objects.</summary>
	[Serializable]
	public partial class CryptoResourceDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoResourceDataEntityFactory() : base("CryptoResourceDataEntity", VarioSL.Entities.EntityType.CryptoResourceDataEntity) { }

		/// <summary>Creates a new, empty CryptoResourceDataEntity object.</summary>
		/// <returns>A new, empty CryptoResourceDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoResourceDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoResourceData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoSamSignCertificateEntity objects.</summary>
	[Serializable]
	public partial class CryptoSamSignCertificateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoSamSignCertificateEntityFactory() : base("CryptoSamSignCertificateEntity", VarioSL.Entities.EntityType.CryptoSamSignCertificateEntity) { }

		/// <summary>Creates a new, empty CryptoSamSignCertificateEntity object.</summary>
		/// <returns>A new, empty CryptoSamSignCertificateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoSamSignCertificateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoSamSignCertificate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CryptoUicCertificateEntity objects.</summary>
	[Serializable]
	public partial class CryptoUicCertificateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CryptoUicCertificateEntityFactory() : base("CryptoUicCertificateEntity", VarioSL.Entities.EntityType.CryptoUicCertificateEntity) { }

		/// <summary>Creates a new, empty CryptoUicCertificateEntity object.</summary>
		/// <returns>A new, empty CryptoUicCertificateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CryptoUicCertificateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCryptoUicCertificate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomAttributeEntity objects.</summary>
	[Serializable]
	public partial class CustomAttributeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomAttributeEntityFactory() : base("CustomAttributeEntity", VarioSL.Entities.EntityType.CustomAttributeEntity) { }

		/// <summary>Creates a new, empty CustomAttributeEntity object.</summary>
		/// <returns>A new, empty CustomAttributeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomAttributeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomAttribute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomAttributeValueEntity objects.</summary>
	[Serializable]
	public partial class CustomAttributeValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomAttributeValueEntityFactory() : base("CustomAttributeValueEntity", VarioSL.Entities.EntityType.CustomAttributeValueEntity) { }

		/// <summary>Creates a new, empty CustomAttributeValueEntity object.</summary>
		/// <returns>A new, empty CustomAttributeValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomAttributeValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomAttributeValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomAttributeValueToPersonEntity objects.</summary>
	[Serializable]
	public partial class CustomAttributeValueToPersonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomAttributeValueToPersonEntityFactory() : base("CustomAttributeValueToPersonEntity", VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity) { }

		/// <summary>Creates a new, empty CustomAttributeValueToPersonEntity object.</summary>
		/// <returns>A new, empty CustomAttributeValueToPersonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomAttributeValueToPersonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomAttributeValueToPerson
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerAccountEntity objects.</summary>
	[Serializable]
	public partial class CustomerAccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerAccountEntityFactory() : base("CustomerAccountEntity", VarioSL.Entities.EntityType.CustomerAccountEntity) { }

		/// <summary>Creates a new, empty CustomerAccountEntity object.</summary>
		/// <returns>A new, empty CustomerAccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerAccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerAccountNotificationEntity objects.</summary>
	[Serializable]
	public partial class CustomerAccountNotificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerAccountNotificationEntityFactory() : base("CustomerAccountNotificationEntity", VarioSL.Entities.EntityType.CustomerAccountNotificationEntity) { }

		/// <summary>Creates a new, empty CustomerAccountNotificationEntity object.</summary>
		/// <returns>A new, empty CustomerAccountNotificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerAccountNotificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerAccountNotification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerAccountRoleEntity objects.</summary>
	[Serializable]
	public partial class CustomerAccountRoleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerAccountRoleEntityFactory() : base("CustomerAccountRoleEntity", VarioSL.Entities.EntityType.CustomerAccountRoleEntity) { }

		/// <summary>Creates a new, empty CustomerAccountRoleEntity object.</summary>
		/// <returns>A new, empty CustomerAccountRoleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerAccountRoleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerAccountRole
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerAccountStateEntity objects.</summary>
	[Serializable]
	public partial class CustomerAccountStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerAccountStateEntityFactory() : base("CustomerAccountStateEntity", VarioSL.Entities.EntityType.CustomerAccountStateEntity) { }

		/// <summary>Creates a new, empty CustomerAccountStateEntity object.</summary>
		/// <returns>A new, empty CustomerAccountStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerAccountStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerAccountState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerAccountToVerificationAttemptTypeEntity objects.</summary>
	[Serializable]
	public partial class CustomerAccountToVerificationAttemptTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerAccountToVerificationAttemptTypeEntityFactory() : base("CustomerAccountToVerificationAttemptTypeEntity", VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity) { }

		/// <summary>Creates a new, empty CustomerAccountToVerificationAttemptTypeEntity object.</summary>
		/// <returns>A new, empty CustomerAccountToVerificationAttemptTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerAccountToVerificationAttemptTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerAccountToVerificationAttemptType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerLanguageEntity objects.</summary>
	[Serializable]
	public partial class CustomerLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerLanguageEntityFactory() : base("CustomerLanguageEntity", VarioSL.Entities.EntityType.CustomerLanguageEntity) { }

		/// <summary>Creates a new, empty CustomerLanguageEntity object.</summary>
		/// <returns>A new, empty CustomerLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomerLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceCommunicationHistoryEntity objects.</summary>
	[Serializable]
	public partial class DeviceCommunicationHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceCommunicationHistoryEntityFactory() : base("DeviceCommunicationHistoryEntity", VarioSL.Entities.EntityType.DeviceCommunicationHistoryEntity) { }

		/// <summary>Creates a new, empty DeviceCommunicationHistoryEntity object.</summary>
		/// <returns>A new, empty DeviceCommunicationHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceCommunicationHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceCommunicationHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderEntityFactory() : base("DeviceReaderEntity", VarioSL.Entities.EntityType.DeviceReaderEntity) { }

		/// <summary>Creates a new, empty DeviceReaderEntity object.</summary>
		/// <returns>A new, empty DeviceReaderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReader
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderCryptoResourceEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderCryptoResourceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderCryptoResourceEntityFactory() : base("DeviceReaderCryptoResourceEntity", VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity) { }

		/// <summary>Creates a new, empty DeviceReaderCryptoResourceEntity object.</summary>
		/// <returns>A new, empty DeviceReaderCryptoResourceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderCryptoResourceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReaderCryptoResource
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderJobEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderJobEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderJobEntityFactory() : base("DeviceReaderJobEntity", VarioSL.Entities.EntityType.DeviceReaderJobEntity) { }

		/// <summary>Creates a new, empty DeviceReaderJobEntity object.</summary>
		/// <returns>A new, empty DeviceReaderJobEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderJobEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReaderJob
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderKeyEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderKeyEntityFactory() : base("DeviceReaderKeyEntity", VarioSL.Entities.EntityType.DeviceReaderKeyEntity) { }

		/// <summary>Creates a new, empty DeviceReaderKeyEntity object.</summary>
		/// <returns>A new, empty DeviceReaderKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReaderKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderKeyToCertEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderKeyToCertEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderKeyToCertEntityFactory() : base("DeviceReaderKeyToCertEntity", VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity) { }

		/// <summary>Creates a new, empty DeviceReaderKeyToCertEntity object.</summary>
		/// <returns>A new, empty DeviceReaderKeyToCertEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderKeyToCertEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReaderKeyToCert
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceReaderToCertEntity objects.</summary>
	[Serializable]
	public partial class DeviceReaderToCertEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceReaderToCertEntityFactory() : base("DeviceReaderToCertEntity", VarioSL.Entities.EntityType.DeviceReaderToCertEntity) { }

		/// <summary>Creates a new, empty DeviceReaderToCertEntity object.</summary>
		/// <returns>A new, empty DeviceReaderToCertEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceReaderToCertEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceReaderToCert
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceTokenEntity objects.</summary>
	[Serializable]
	public partial class DeviceTokenEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceTokenEntityFactory() : base("DeviceTokenEntity", VarioSL.Entities.EntityType.DeviceTokenEntity) { }

		/// <summary>Creates a new, empty DeviceTokenEntity object.</summary>
		/// <returns>A new, empty DeviceTokenEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceTokenEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceToken
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceTokenStateEntity objects.</summary>
	[Serializable]
	public partial class DeviceTokenStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceTokenStateEntityFactory() : base("DeviceTokenStateEntity", VarioSL.Entities.EntityType.DeviceTokenStateEntity) { }

		/// <summary>Creates a new, empty DeviceTokenStateEntity object.</summary>
		/// <returns>A new, empty DeviceTokenStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceTokenStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceTokenState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DocumentHistoryEntity objects.</summary>
	[Serializable]
	public partial class DocumentHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DocumentHistoryEntityFactory() : base("DocumentHistoryEntity", VarioSL.Entities.EntityType.DocumentHistoryEntity) { }

		/// <summary>Creates a new, empty DocumentHistoryEntity object.</summary>
		/// <returns>A new, empty DocumentHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DocumentHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDocumentHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DocumentTypeEntity objects.</summary>
	[Serializable]
	public partial class DocumentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DocumentTypeEntityFactory() : base("DocumentTypeEntity", VarioSL.Entities.EntityType.DocumentTypeEntity) { }

		/// <summary>Creates a new, empty DocumentTypeEntity object.</summary>
		/// <returns>A new, empty DocumentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DocumentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDocumentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DormancyNotificationEntity objects.</summary>
	[Serializable]
	public partial class DormancyNotificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DormancyNotificationEntityFactory() : base("DormancyNotificationEntity", VarioSL.Entities.EntityType.DormancyNotificationEntity) { }

		/// <summary>Creates a new, empty DormancyNotificationEntity object.</summary>
		/// <returns>A new, empty DormancyNotificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DormancyNotificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDormancyNotification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningActionEntity objects.</summary>
	[Serializable]
	public partial class DunningActionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningActionEntityFactory() : base("DunningActionEntity", VarioSL.Entities.EntityType.DunningActionEntity) { }

		/// <summary>Creates a new, empty DunningActionEntity object.</summary>
		/// <returns>A new, empty DunningActionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningActionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningAction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningLevelConfigurationEntity objects.</summary>
	[Serializable]
	public partial class DunningLevelConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningLevelConfigurationEntityFactory() : base("DunningLevelConfigurationEntity", VarioSL.Entities.EntityType.DunningLevelConfigurationEntity) { }

		/// <summary>Creates a new, empty DunningLevelConfigurationEntity object.</summary>
		/// <returns>A new, empty DunningLevelConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningLevelConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningLevelConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningLevelPostingKeyEntity objects.</summary>
	[Serializable]
	public partial class DunningLevelPostingKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningLevelPostingKeyEntityFactory() : base("DunningLevelPostingKeyEntity", VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity) { }

		/// <summary>Creates a new, empty DunningLevelPostingKeyEntity object.</summary>
		/// <returns>A new, empty DunningLevelPostingKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningLevelPostingKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningLevelPostingKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningProcessEntity objects.</summary>
	[Serializable]
	public partial class DunningProcessEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningProcessEntityFactory() : base("DunningProcessEntity", VarioSL.Entities.EntityType.DunningProcessEntity) { }

		/// <summary>Creates a new, empty DunningProcessEntity object.</summary>
		/// <returns>A new, empty DunningProcessEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningProcessEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningProcess
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningToInvoiceEntity objects.</summary>
	[Serializable]
	public partial class DunningToInvoiceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningToInvoiceEntityFactory() : base("DunningToInvoiceEntity", VarioSL.Entities.EntityType.DunningToInvoiceEntity) { }

		/// <summary>Creates a new, empty DunningToInvoiceEntity object.</summary>
		/// <returns>A new, empty DunningToInvoiceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningToInvoiceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningToInvoice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningToPostingEntity objects.</summary>
	[Serializable]
	public partial class DunningToPostingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningToPostingEntityFactory() : base("DunningToPostingEntity", VarioSL.Entities.EntityType.DunningToPostingEntity) { }

		/// <summary>Creates a new, empty DunningToPostingEntity object.</summary>
		/// <returns>A new, empty DunningToPostingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningToPostingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningToPosting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DunningToProductEntity objects.</summary>
	[Serializable]
	public partial class DunningToProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DunningToProductEntityFactory() : base("DunningToProductEntity", VarioSL.Entities.EntityType.DunningToProductEntity) { }

		/// <summary>Creates a new, empty DunningToProductEntity object.</summary>
		/// <returns>A new, empty DunningToProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DunningToProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDunningToProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EmailEventEntity objects.</summary>
	[Serializable]
	public partial class EmailEventEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EmailEventEntityFactory() : base("EmailEventEntity", VarioSL.Entities.EntityType.EmailEventEntity) { }

		/// <summary>Creates a new, empty EmailEventEntity object.</summary>
		/// <returns>A new, empty EmailEventEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EmailEventEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmailEvent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EmailEventResendLockEntity objects.</summary>
	[Serializable]
	public partial class EmailEventResendLockEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EmailEventResendLockEntityFactory() : base("EmailEventResendLockEntity", VarioSL.Entities.EntityType.EmailEventResendLockEntity) { }

		/// <summary>Creates a new, empty EmailEventResendLockEntity object.</summary>
		/// <returns>A new, empty EmailEventResendLockEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EmailEventResendLockEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmailEventResendLock
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EmailMessageEntity objects.</summary>
	[Serializable]
	public partial class EmailMessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EmailMessageEntityFactory() : base("EmailMessageEntity", VarioSL.Entities.EntityType.EmailMessageEntity) { }

		/// <summary>Creates a new, empty EmailMessageEntity object.</summary>
		/// <returns>A new, empty EmailMessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EmailMessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEmailMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntitlementEntity objects.</summary>
	[Serializable]
	public partial class EntitlementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntitlementEntityFactory() : base("EntitlementEntity", VarioSL.Entities.EntityType.EntitlementEntity) { }

		/// <summary>Creates a new, empty EntitlementEntity object.</summary>
		/// <returns>A new, empty EntitlementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntitlementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntitlement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntitlementToProductEntity objects.</summary>
	[Serializable]
	public partial class EntitlementToProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntitlementToProductEntityFactory() : base("EntitlementToProductEntity", VarioSL.Entities.EntityType.EntitlementToProductEntity) { }

		/// <summary>Creates a new, empty EntitlementToProductEntity object.</summary>
		/// <returns>A new, empty EntitlementToProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntitlementToProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntitlementToProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntitlementTypeEntity objects.</summary>
	[Serializable]
	public partial class EntitlementTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntitlementTypeEntityFactory() : base("EntitlementTypeEntity", VarioSL.Entities.EntityType.EntitlementTypeEntity) { }

		/// <summary>Creates a new, empty EntitlementTypeEntity object.</summary>
		/// <returns>A new, empty EntitlementTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntitlementTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntitlementType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EventSettingEntity objects.</summary>
	[Serializable]
	public partial class EventSettingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EventSettingEntityFactory() : base("EventSettingEntity", VarioSL.Entities.EntityType.EventSettingEntity) { }

		/// <summary>Creates a new, empty EventSettingEntity object.</summary>
		/// <returns>A new, empty EventSettingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EventSettingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEventSetting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareChangeCauseEntity objects.</summary>
	[Serializable]
	public partial class FareChangeCauseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareChangeCauseEntityFactory() : base("FareChangeCauseEntity", VarioSL.Entities.EntityType.FareChangeCauseEntity) { }

		/// <summary>Creates a new, empty FareChangeCauseEntity object.</summary>
		/// <returns>A new, empty FareChangeCauseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareChangeCauseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareChangeCause
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionBehaviourEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionBehaviourEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionBehaviourEntityFactory() : base("FareEvasionBehaviourEntity", VarioSL.Entities.EntityType.FareEvasionBehaviourEntity) { }

		/// <summary>Creates a new, empty FareEvasionBehaviourEntity object.</summary>
		/// <returns>A new, empty FareEvasionBehaviourEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionBehaviourEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionBehaviour
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionIncidentEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionIncidentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionIncidentEntityFactory() : base("FareEvasionIncidentEntity", VarioSL.Entities.EntityType.FareEvasionIncidentEntity) { }

		/// <summary>Creates a new, empty FareEvasionIncidentEntity object.</summary>
		/// <returns>A new, empty FareEvasionIncidentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionIncidentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionIncident
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionInspectionEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionInspectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionInspectionEntityFactory() : base("FareEvasionInspectionEntity", VarioSL.Entities.EntityType.FareEvasionInspectionEntity) { }

		/// <summary>Creates a new, empty FareEvasionInspectionEntity object.</summary>
		/// <returns>A new, empty FareEvasionInspectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionInspectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionInspection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FareEvasionStateEntity objects.</summary>
	[Serializable]
	public partial class FareEvasionStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FareEvasionStateEntityFactory() : base("FareEvasionStateEntity", VarioSL.Entities.EntityType.FareEvasionStateEntity) { }

		/// <summary>Creates a new, empty FareEvasionStateEntity object.</summary>
		/// <returns>A new, empty FareEvasionStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FareEvasionStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFareEvasionState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FarePaymentErrorEntity objects.</summary>
	[Serializable]
	public partial class FarePaymentErrorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FarePaymentErrorEntityFactory() : base("FarePaymentErrorEntity", VarioSL.Entities.EntityType.FarePaymentErrorEntity) { }

		/// <summary>Creates a new, empty FarePaymentErrorEntity object.</summary>
		/// <returns>A new, empty FarePaymentErrorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FarePaymentErrorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFarePaymentError
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FarePaymentResultTypeEntity objects.</summary>
	[Serializable]
	public partial class FarePaymentResultTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FarePaymentResultTypeEntityFactory() : base("FarePaymentResultTypeEntity", VarioSL.Entities.EntityType.FarePaymentResultTypeEntity) { }

		/// <summary>Creates a new, empty FarePaymentResultTypeEntity object.</summary>
		/// <returns>A new, empty FarePaymentResultTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FarePaymentResultTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFarePaymentResultType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FastcardEntity objects.</summary>
	[Serializable]
	public partial class FastcardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FastcardEntityFactory() : base("FastcardEntity", VarioSL.Entities.EntityType.FastcardEntity) { }

		/// <summary>Creates a new, empty FastcardEntity object.</summary>
		/// <returns>A new, empty FastcardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FastcardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFastcard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterCategoryEntity objects.</summary>
	[Serializable]
	public partial class FilterCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterCategoryEntityFactory() : base("FilterCategoryEntity", VarioSL.Entities.EntityType.FilterCategoryEntity) { }

		/// <summary>Creates a new, empty FilterCategoryEntity object.</summary>
		/// <returns>A new, empty FilterCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterElementEntity objects.</summary>
	[Serializable]
	public partial class FilterElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterElementEntityFactory() : base("FilterElementEntity", VarioSL.Entities.EntityType.FilterElementEntity) { }

		/// <summary>Creates a new, empty FilterElementEntity object.</summary>
		/// <returns>A new, empty FilterElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterListEntity objects.</summary>
	[Serializable]
	public partial class FilterListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterListEntityFactory() : base("FilterListEntity", VarioSL.Entities.EntityType.FilterListEntity) { }

		/// <summary>Creates a new, empty FilterListEntity object.</summary>
		/// <returns>A new, empty FilterListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterListElementEntity objects.</summary>
	[Serializable]
	public partial class FilterListElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterListElementEntityFactory() : base("FilterListElementEntity", VarioSL.Entities.EntityType.FilterListElementEntity) { }

		/// <summary>Creates a new, empty FilterListElementEntity object.</summary>
		/// <returns>A new, empty FilterListElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterListElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterListElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterTypeEntity objects.</summary>
	[Serializable]
	public partial class FilterTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterTypeEntityFactory() : base("FilterTypeEntity", VarioSL.Entities.EntityType.FilterTypeEntity) { }

		/// <summary>Creates a new, empty FilterTypeEntity object.</summary>
		/// <returns>A new, empty FilterTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterValueEntity objects.</summary>
	[Serializable]
	public partial class FilterValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterValueEntityFactory() : base("FilterValueEntity", VarioSL.Entities.EntityType.FilterValueEntity) { }

		/// <summary>Creates a new, empty FilterValueEntity object.</summary>
		/// <returns>A new, empty FilterValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FilterValueSetEntity objects.</summary>
	[Serializable]
	public partial class FilterValueSetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FilterValueSetEntityFactory() : base("FilterValueSetEntity", VarioSL.Entities.EntityType.FilterValueSetEntity) { }

		/// <summary>Creates a new, empty FilterValueSetEntity object.</summary>
		/// <returns>A new, empty FilterValueSetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FilterValueSetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFilterValueSet
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FlexValueEntity objects.</summary>
	[Serializable]
	public partial class FlexValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FlexValueEntityFactory() : base("FlexValueEntity", VarioSL.Entities.EntityType.FlexValueEntity) { }

		/// <summary>Creates a new, empty FlexValueEntity object.</summary>
		/// <returns>A new, empty FlexValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FlexValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFlexValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FormEntity objects.</summary>
	[Serializable]
	public partial class FormEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FormEntityFactory() : base("FormEntity", VarioSL.Entities.EntityType.FormEntity) { }

		/// <summary>Creates a new, empty FormEntity object.</summary>
		/// <returns>A new, empty FormEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FormEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewForm
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FormTemplateEntity objects.</summary>
	[Serializable]
	public partial class FormTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FormTemplateEntityFactory() : base("FormTemplateEntity", VarioSL.Entities.EntityType.FormTemplateEntity) { }

		/// <summary>Creates a new, empty FormTemplateEntity object.</summary>
		/// <returns>A new, empty FormTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FormTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFormTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FrequencyEntity objects.</summary>
	[Serializable]
	public partial class FrequencyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FrequencyEntityFactory() : base("FrequencyEntity", VarioSL.Entities.EntityType.FrequencyEntity) { }

		/// <summary>Creates a new, empty FrequencyEntity object.</summary>
		/// <returns>A new, empty FrequencyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FrequencyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFrequency
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenderEntity objects.</summary>
	[Serializable]
	public partial class GenderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenderEntityFactory() : base("GenderEntity", VarioSL.Entities.EntityType.GenderEntity) { }

		/// <summary>Creates a new, empty GenderEntity object.</summary>
		/// <returns>A new, empty GenderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGender
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty IdentificationTypeEntity objects.</summary>
	[Serializable]
	public partial class IdentificationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public IdentificationTypeEntityFactory() : base("IdentificationTypeEntity", VarioSL.Entities.EntityType.IdentificationTypeEntity) { }

		/// <summary>Creates a new, empty IdentificationTypeEntity object.</summary>
		/// <returns>A new, empty IdentificationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new IdentificationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIdentificationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InspectionCriterionEntity objects.</summary>
	[Serializable]
	public partial class InspectionCriterionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InspectionCriterionEntityFactory() : base("InspectionCriterionEntity", VarioSL.Entities.EntityType.InspectionCriterionEntity) { }

		/// <summary>Creates a new, empty InspectionCriterionEntity object.</summary>
		/// <returns>A new, empty InspectionCriterionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InspectionCriterionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInspectionCriterion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InspectionReportEntity objects.</summary>
	[Serializable]
	public partial class InspectionReportEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InspectionReportEntityFactory() : base("InspectionReportEntity", VarioSL.Entities.EntityType.InspectionReportEntity) { }

		/// <summary>Creates a new, empty InspectionReportEntity object.</summary>
		/// <returns>A new, empty InspectionReportEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InspectionReportEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInspectionReport
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InspectionResultEntity objects.</summary>
	[Serializable]
	public partial class InspectionResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InspectionResultEntityFactory() : base("InspectionResultEntity", VarioSL.Entities.EntityType.InspectionResultEntity) { }

		/// <summary>Creates a new, empty InspectionResultEntity object.</summary>
		/// <returns>A new, empty InspectionResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InspectionResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInspectionResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InventoryStateEntity objects.</summary>
	[Serializable]
	public partial class InventoryStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InventoryStateEntityFactory() : base("InventoryStateEntity", VarioSL.Entities.EntityType.InventoryStateEntity) { }

		/// <summary>Creates a new, empty InventoryStateEntity object.</summary>
		/// <returns>A new, empty InventoryStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InventoryStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInventoryState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoiceEntity objects.</summary>
	[Serializable]
	public partial class InvoiceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoiceEntityFactory() : base("InvoiceEntity", VarioSL.Entities.EntityType.InvoiceEntity) { }

		/// <summary>Creates a new, empty InvoiceEntity object.</summary>
		/// <returns>A new, empty InvoiceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoiceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoiceEntryEntity objects.</summary>
	[Serializable]
	public partial class InvoiceEntryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoiceEntryEntityFactory() : base("InvoiceEntryEntity", VarioSL.Entities.EntityType.InvoiceEntryEntity) { }

		/// <summary>Creates a new, empty InvoiceEntryEntity object.</summary>
		/// <returns>A new, empty InvoiceEntryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoiceEntryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoiceEntry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoiceTypeEntity objects.</summary>
	[Serializable]
	public partial class InvoiceTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoiceTypeEntityFactory() : base("InvoiceTypeEntity", VarioSL.Entities.EntityType.InvoiceTypeEntity) { }

		/// <summary>Creates a new, empty InvoiceTypeEntity object.</summary>
		/// <returns>A new, empty InvoiceTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoiceTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoiceType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoicingEntity objects.</summary>
	[Serializable]
	public partial class InvoicingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoicingEntityFactory() : base("InvoicingEntity", VarioSL.Entities.EntityType.InvoicingEntity) { }

		/// <summary>Creates a new, empty InvoicingEntity object.</summary>
		/// <returns>A new, empty InvoicingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoicingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoicing
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoicingFilterTypeEntity objects.</summary>
	[Serializable]
	public partial class InvoicingFilterTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoicingFilterTypeEntityFactory() : base("InvoicingFilterTypeEntity", VarioSL.Entities.EntityType.InvoicingFilterTypeEntity) { }

		/// <summary>Creates a new, empty InvoicingFilterTypeEntity object.</summary>
		/// <returns>A new, empty InvoicingFilterTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoicingFilterTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoicingFilterType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InvoicingTypeEntity objects.</summary>
	[Serializable]
	public partial class InvoicingTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InvoicingTypeEntityFactory() : base("InvoicingTypeEntity", VarioSL.Entities.EntityType.InvoicingTypeEntity) { }

		/// <summary>Creates a new, empty InvoicingTypeEntity object.</summary>
		/// <returns>A new, empty InvoicingTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InvoicingTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInvoicingType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobEntity objects.</summary>
	[Serializable]
	public partial class JobEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobEntityFactory() : base("JobEntity", VarioSL.Entities.EntityType.JobEntity) { }

		/// <summary>Creates a new, empty JobEntity object.</summary>
		/// <returns>A new, empty JobEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJob
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobBulkImportEntity objects.</summary>
	[Serializable]
	public partial class JobBulkImportEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobBulkImportEntityFactory() : base("JobBulkImportEntity", VarioSL.Entities.EntityType.JobBulkImportEntity) { }

		/// <summary>Creates a new, empty JobBulkImportEntity object.</summary>
		/// <returns>A new, empty JobBulkImportEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobBulkImportEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobBulkImport
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobCardImportEntity objects.</summary>
	[Serializable]
	public partial class JobCardImportEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobCardImportEntityFactory() : base("JobCardImportEntity", VarioSL.Entities.EntityType.JobCardImportEntity) { }

		/// <summary>Creates a new, empty JobCardImportEntity object.</summary>
		/// <returns>A new, empty JobCardImportEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobCardImportEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobCardImport
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobFileTypeEntity objects.</summary>
	[Serializable]
	public partial class JobFileTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobFileTypeEntityFactory() : base("JobFileTypeEntity", VarioSL.Entities.EntityType.JobFileTypeEntity) { }

		/// <summary>Creates a new, empty JobFileTypeEntity object.</summary>
		/// <returns>A new, empty JobFileTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobFileTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobFileType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobStateEntity objects.</summary>
	[Serializable]
	public partial class JobStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobStateEntityFactory() : base("JobStateEntity", VarioSL.Entities.EntityType.JobStateEntity) { }

		/// <summary>Creates a new, empty JobStateEntity object.</summary>
		/// <returns>A new, empty JobStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty JobTypeEntity objects.</summary>
	[Serializable]
	public partial class JobTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public JobTypeEntityFactory() : base("JobTypeEntity", VarioSL.Entities.EntityType.JobTypeEntity) { }

		/// <summary>Creates a new, empty JobTypeEntity object.</summary>
		/// <returns>A new, empty JobTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new JobTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewJobType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LookupAddressEntity objects.</summary>
	[Serializable]
	public partial class LookupAddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LookupAddressEntityFactory() : base("LookupAddressEntity", VarioSL.Entities.EntityType.LookupAddressEntity) { }

		/// <summary>Creates a new, empty LookupAddressEntity object.</summary>
		/// <returns>A new, empty LookupAddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LookupAddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLookupAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaEligibilityRequirementEntity objects.</summary>
	[Serializable]
	public partial class MediaEligibilityRequirementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaEligibilityRequirementEntityFactory() : base("MediaEligibilityRequirementEntity", VarioSL.Entities.EntityType.MediaEligibilityRequirementEntity) { }

		/// <summary>Creates a new, empty MediaEligibilityRequirementEntity object.</summary>
		/// <returns>A new, empty MediaEligibilityRequirementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaEligibilityRequirementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaEligibilityRequirement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaSalePreconditionEntity objects.</summary>
	[Serializable]
	public partial class MediaSalePreconditionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaSalePreconditionEntityFactory() : base("MediaSalePreconditionEntity", VarioSL.Entities.EntityType.MediaSalePreconditionEntity) { }

		/// <summary>Creates a new, empty MediaSalePreconditionEntity object.</summary>
		/// <returns>A new, empty MediaSalePreconditionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaSalePreconditionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaSalePrecondition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MerchantEntity objects.</summary>
	[Serializable]
	public partial class MerchantEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MerchantEntityFactory() : base("MerchantEntity", VarioSL.Entities.EntityType.MerchantEntity) { }

		/// <summary>Creates a new, empty MerchantEntity object.</summary>
		/// <returns>A new, empty MerchantEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MerchantEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMerchant
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageEntity objects.</summary>
	[Serializable]
	public partial class MessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageEntityFactory() : base("MessageEntity", VarioSL.Entities.EntityType.MessageEntity) { }

		/// <summary>Creates a new, empty MessageEntity object.</summary>
		/// <returns>A new, empty MessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageFormatEntity objects.</summary>
	[Serializable]
	public partial class MessageFormatEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageFormatEntityFactory() : base("MessageFormatEntity", VarioSL.Entities.EntityType.MessageFormatEntity) { }

		/// <summary>Creates a new, empty MessageFormatEntity object.</summary>
		/// <returns>A new, empty MessageFormatEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageFormatEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageFormat
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageTypeEntity objects.</summary>
	[Serializable]
	public partial class MessageTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageTypeEntityFactory() : base("MessageTypeEntity", VarioSL.Entities.EntityType.MessageTypeEntity) { }

		/// <summary>Creates a new, empty MessageTypeEntity object.</summary>
		/// <returns>A new, empty MessageTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MobilityProviderEntity objects.</summary>
	[Serializable]
	public partial class MobilityProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MobilityProviderEntityFactory() : base("MobilityProviderEntity", VarioSL.Entities.EntityType.MobilityProviderEntity) { }

		/// <summary>Creates a new, empty MobilityProviderEntity object.</summary>
		/// <returns>A new, empty MobilityProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MobilityProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMobilityProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NotificationDeviceEntity objects.</summary>
	[Serializable]
	public partial class NotificationDeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NotificationDeviceEntityFactory() : base("NotificationDeviceEntity", VarioSL.Entities.EntityType.NotificationDeviceEntity) { }

		/// <summary>Creates a new, empty NotificationDeviceEntity object.</summary>
		/// <returns>A new, empty NotificationDeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NotificationDeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNotificationDevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NotificationMessageEntity objects.</summary>
	[Serializable]
	public partial class NotificationMessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NotificationMessageEntityFactory() : base("NotificationMessageEntity", VarioSL.Entities.EntityType.NotificationMessageEntity) { }

		/// <summary>Creates a new, empty NotificationMessageEntity object.</summary>
		/// <returns>A new, empty NotificationMessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NotificationMessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNotificationMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NotificationMessageOwnerEntity objects.</summary>
	[Serializable]
	public partial class NotificationMessageOwnerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NotificationMessageOwnerEntityFactory() : base("NotificationMessageOwnerEntity", VarioSL.Entities.EntityType.NotificationMessageOwnerEntity) { }

		/// <summary>Creates a new, empty NotificationMessageOwnerEntity object.</summary>
		/// <returns>A new, empty NotificationMessageOwnerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NotificationMessageOwnerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNotificationMessageOwner
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NotificationMessageToCustomerEntity objects.</summary>
	[Serializable]
	public partial class NotificationMessageToCustomerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NotificationMessageToCustomerEntityFactory() : base("NotificationMessageToCustomerEntity", VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity) { }

		/// <summary>Creates a new, empty NotificationMessageToCustomerEntity object.</summary>
		/// <returns>A new, empty NotificationMessageToCustomerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NotificationMessageToCustomerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNotificationMessageToCustomer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NumberGroupEntity objects.</summary>
	[Serializable]
	public partial class NumberGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NumberGroupEntityFactory() : base("NumberGroupEntity", VarioSL.Entities.EntityType.NumberGroupEntity) { }

		/// <summary>Creates a new, empty NumberGroupEntity object.</summary>
		/// <returns>A new, empty NumberGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NumberGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNumberGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NumberGroupRandomizedEntity objects.</summary>
	[Serializable]
	public partial class NumberGroupRandomizedEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NumberGroupRandomizedEntityFactory() : base("NumberGroupRandomizedEntity", VarioSL.Entities.EntityType.NumberGroupRandomizedEntity) { }

		/// <summary>Creates a new, empty NumberGroupRandomizedEntity object.</summary>
		/// <returns>A new, empty NumberGroupRandomizedEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NumberGroupRandomizedEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNumberGroupRandomized
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NumberGroupScopeEntity objects.</summary>
	[Serializable]
	public partial class NumberGroupScopeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NumberGroupScopeEntityFactory() : base("NumberGroupScopeEntity", VarioSL.Entities.EntityType.NumberGroupScopeEntity) { }

		/// <summary>Creates a new, empty NumberGroupScopeEntity object.</summary>
		/// <returns>A new, empty NumberGroupScopeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NumberGroupScopeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNumberGroupScope
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderEntity objects.</summary>
	[Serializable]
	public partial class OrderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderEntityFactory() : base("OrderEntity", VarioSL.Entities.EntityType.OrderEntity) { }

		/// <summary>Creates a new, empty OrderEntity object.</summary>
		/// <returns>A new, empty OrderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrder
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderDetailEntity objects.</summary>
	[Serializable]
	public partial class OrderDetailEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderDetailEntityFactory() : base("OrderDetailEntity", VarioSL.Entities.EntityType.OrderDetailEntity) { }

		/// <summary>Creates a new, empty OrderDetailEntity object.</summary>
		/// <returns>A new, empty OrderDetailEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderDetailEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderDetail
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderDetailToCardEntity objects.</summary>
	[Serializable]
	public partial class OrderDetailToCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderDetailToCardEntityFactory() : base("OrderDetailToCardEntity", VarioSL.Entities.EntityType.OrderDetailToCardEntity) { }

		/// <summary>Creates a new, empty OrderDetailToCardEntity object.</summary>
		/// <returns>A new, empty OrderDetailToCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderDetailToCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderDetailToCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderDetailToCardHolderEntity objects.</summary>
	[Serializable]
	public partial class OrderDetailToCardHolderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderDetailToCardHolderEntityFactory() : base("OrderDetailToCardHolderEntity", VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity) { }

		/// <summary>Creates a new, empty OrderDetailToCardHolderEntity object.</summary>
		/// <returns>A new, empty OrderDetailToCardHolderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderDetailToCardHolderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderDetailToCardHolder
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderHistoryEntity objects.</summary>
	[Serializable]
	public partial class OrderHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderHistoryEntityFactory() : base("OrderHistoryEntity", VarioSL.Entities.EntityType.OrderHistoryEntity) { }

		/// <summary>Creates a new, empty OrderHistoryEntity object.</summary>
		/// <returns>A new, empty OrderHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderProcessingCleanupEntity objects.</summary>
	[Serializable]
	public partial class OrderProcessingCleanupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderProcessingCleanupEntityFactory() : base("OrderProcessingCleanupEntity", VarioSL.Entities.EntityType.OrderProcessingCleanupEntity) { }

		/// <summary>Creates a new, empty OrderProcessingCleanupEntity object.</summary>
		/// <returns>A new, empty OrderProcessingCleanupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderProcessingCleanupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderProcessingCleanup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderStateEntity objects.</summary>
	[Serializable]
	public partial class OrderStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderStateEntityFactory() : base("OrderStateEntity", VarioSL.Entities.EntityType.OrderStateEntity) { }

		/// <summary>Creates a new, empty OrderStateEntity object.</summary>
		/// <returns>A new, empty OrderStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderTypeEntity objects.</summary>
	[Serializable]
	public partial class OrderTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderTypeEntityFactory() : base("OrderTypeEntity", VarioSL.Entities.EntityType.OrderTypeEntity) { }

		/// <summary>Creates a new, empty OrderTypeEntity object.</summary>
		/// <returns>A new, empty OrderTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderWithTotalEntity objects.</summary>
	[Serializable]
	public partial class OrderWithTotalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderWithTotalEntityFactory() : base("OrderWithTotalEntity", VarioSL.Entities.EntityType.OrderWithTotalEntity) { }

		/// <summary>Creates a new, empty OrderWithTotalEntity object.</summary>
		/// <returns>A new, empty OrderWithTotalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderWithTotalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderWithTotal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderWorkItemEntity objects.</summary>
	[Serializable]
	public partial class OrderWorkItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderWorkItemEntityFactory() : base("OrderWorkItemEntity", VarioSL.Entities.EntityType.OrderWorkItemEntity) { }

		/// <summary>Creates a new, empty OrderWorkItemEntity object.</summary>
		/// <returns>A new, empty OrderWorkItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderWorkItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderWorkItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("OrderWorkItemEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrganizationEntity objects.</summary>
	[Serializable]
	public partial class OrganizationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrganizationEntityFactory() : base("OrganizationEntity", VarioSL.Entities.EntityType.OrganizationEntity) { }

		/// <summary>Creates a new, empty OrganizationEntity object.</summary>
		/// <returns>A new, empty OrganizationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrganizationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrganization
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrganizationAddressEntity objects.</summary>
	[Serializable]
	public partial class OrganizationAddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrganizationAddressEntityFactory() : base("OrganizationAddressEntity", VarioSL.Entities.EntityType.OrganizationAddressEntity) { }

		/// <summary>Creates a new, empty OrganizationAddressEntity object.</summary>
		/// <returns>A new, empty OrganizationAddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrganizationAddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrganizationAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrganizationParticipantEntity objects.</summary>
	[Serializable]
	public partial class OrganizationParticipantEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrganizationParticipantEntityFactory() : base("OrganizationParticipantEntity", VarioSL.Entities.EntityType.OrganizationParticipantEntity) { }

		/// <summary>Creates a new, empty OrganizationParticipantEntity object.</summary>
		/// <returns>A new, empty OrganizationParticipantEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrganizationParticipantEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrganizationParticipant
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrganizationSubtypeEntity objects.</summary>
	[Serializable]
	public partial class OrganizationSubtypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrganizationSubtypeEntityFactory() : base("OrganizationSubtypeEntity", VarioSL.Entities.EntityType.OrganizationSubtypeEntity) { }

		/// <summary>Creates a new, empty OrganizationSubtypeEntity object.</summary>
		/// <returns>A new, empty OrganizationSubtypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrganizationSubtypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrganizationSubtype
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrganizationTypeEntity objects.</summary>
	[Serializable]
	public partial class OrganizationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrganizationTypeEntityFactory() : base("OrganizationTypeEntity", VarioSL.Entities.EntityType.OrganizationTypeEntity) { }

		/// <summary>Creates a new, empty OrganizationTypeEntity object.</summary>
		/// <returns>A new, empty OrganizationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrganizationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrganizationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageContentEntity objects.</summary>
	[Serializable]
	public partial class PageContentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageContentEntityFactory() : base("PageContentEntity", VarioSL.Entities.EntityType.PageContentEntity) { }

		/// <summary>Creates a new, empty PageContentEntity object.</summary>
		/// <returns>A new, empty PageContentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageContentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageContent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterEntity objects.</summary>
	[Serializable]
	public partial class ParameterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterEntityFactory() : base("ParameterEntity", VarioSL.Entities.EntityType.ParameterEntity) { }

		/// <summary>Creates a new, empty ParameterEntity object.</summary>
		/// <returns>A new, empty ParameterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameter
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterArchiveEntity objects.</summary>
	[Serializable]
	public partial class ParameterArchiveEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterArchiveEntityFactory() : base("ParameterArchiveEntity", VarioSL.Entities.EntityType.ParameterArchiveEntity) { }

		/// <summary>Creates a new, empty ParameterArchiveEntity object.</summary>
		/// <returns>A new, empty ParameterArchiveEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterArchiveEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterArchive
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterArchiveReleaseEntity objects.</summary>
	[Serializable]
	public partial class ParameterArchiveReleaseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterArchiveReleaseEntityFactory() : base("ParameterArchiveReleaseEntity", VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity) { }

		/// <summary>Creates a new, empty ParameterArchiveReleaseEntity object.</summary>
		/// <returns>A new, empty ParameterArchiveReleaseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterArchiveReleaseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterArchiveRelease
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterArchiveResultViewEntity objects.</summary>
	[Serializable]
	public partial class ParameterArchiveResultViewEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterArchiveResultViewEntityFactory() : base("ParameterArchiveResultViewEntity", VarioSL.Entities.EntityType.ParameterArchiveResultViewEntity) { }

		/// <summary>Creates a new, empty ParameterArchiveResultViewEntity object.</summary>
		/// <returns>A new, empty ParameterArchiveResultViewEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterArchiveResultViewEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterArchiveResultView
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterGroupEntity objects.</summary>
	[Serializable]
	public partial class ParameterGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterGroupEntityFactory() : base("ParameterGroupEntity", VarioSL.Entities.EntityType.ParameterGroupEntity) { }

		/// <summary>Creates a new, empty ParameterGroupEntity object.</summary>
		/// <returns>A new, empty ParameterGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterGroupToParameterEntity objects.</summary>
	[Serializable]
	public partial class ParameterGroupToParameterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterGroupToParameterEntityFactory() : base("ParameterGroupToParameterEntity", VarioSL.Entities.EntityType.ParameterGroupToParameterEntity) { }

		/// <summary>Creates a new, empty ParameterGroupToParameterEntity object.</summary>
		/// <returns>A new, empty ParameterGroupToParameterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterGroupToParameterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterGroupToParameter
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterTypeEntity objects.</summary>
	[Serializable]
	public partial class ParameterTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterTypeEntityFactory() : base("ParameterTypeEntity", VarioSL.Entities.EntityType.ParameterTypeEntity) { }

		/// <summary>Creates a new, empty ParameterTypeEntity object.</summary>
		/// <returns>A new, empty ParameterTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParameterValueEntity objects.</summary>
	[Serializable]
	public partial class ParameterValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParameterValueEntityFactory() : base("ParameterValueEntity", VarioSL.Entities.EntityType.ParameterValueEntity) { }

		/// <summary>Creates a new, empty ParameterValueEntity object.</summary>
		/// <returns>A new, empty ParameterValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParameterValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParameterValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParaTransAttributeEntity objects.</summary>
	[Serializable]
	public partial class ParaTransAttributeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParaTransAttributeEntityFactory() : base("ParaTransAttributeEntity", VarioSL.Entities.EntityType.ParaTransAttributeEntity) { }

		/// <summary>Creates a new, empty ParaTransAttributeEntity object.</summary>
		/// <returns>A new, empty ParaTransAttributeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParaTransAttributeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParaTransAttribute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParaTransAttributeTypeEntity objects.</summary>
	[Serializable]
	public partial class ParaTransAttributeTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParaTransAttributeTypeEntityFactory() : base("ParaTransAttributeTypeEntity", VarioSL.Entities.EntityType.ParaTransAttributeTypeEntity) { }

		/// <summary>Creates a new, empty ParaTransAttributeTypeEntity object.</summary>
		/// <returns>A new, empty ParaTransAttributeTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParaTransAttributeTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParaTransAttributeType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParaTransJournalEntity objects.</summary>
	[Serializable]
	public partial class ParaTransJournalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParaTransJournalEntityFactory() : base("ParaTransJournalEntity", VarioSL.Entities.EntityType.ParaTransJournalEntity) { }

		/// <summary>Creates a new, empty ParaTransJournalEntity object.</summary>
		/// <returns>A new, empty ParaTransJournalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParaTransJournalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParaTransJournal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParaTransJournalTypeEntity objects.</summary>
	[Serializable]
	public partial class ParaTransJournalTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParaTransJournalTypeEntityFactory() : base("ParaTransJournalTypeEntity", VarioSL.Entities.EntityType.ParaTransJournalTypeEntity) { }

		/// <summary>Creates a new, empty ParaTransJournalTypeEntity object.</summary>
		/// <returns>A new, empty ParaTransJournalTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParaTransJournalTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParaTransJournalType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParaTransPaymentTypeEntity objects.</summary>
	[Serializable]
	public partial class ParaTransPaymentTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParaTransPaymentTypeEntityFactory() : base("ParaTransPaymentTypeEntity", VarioSL.Entities.EntityType.ParaTransPaymentTypeEntity) { }

		/// <summary>Creates a new, empty ParaTransPaymentTypeEntity object.</summary>
		/// <returns>A new, empty ParaTransPaymentTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParaTransPaymentTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParaTransPaymentType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ParticipantGroupEntity objects.</summary>
	[Serializable]
	public partial class ParticipantGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ParticipantGroupEntityFactory() : base("ParticipantGroupEntity", VarioSL.Entities.EntityType.ParticipantGroupEntity) { }

		/// <summary>Creates a new, empty ParticipantGroupEntity object.</summary>
		/// <returns>A new, empty ParticipantGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ParticipantGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewParticipantGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PassExpiryNotificationEntity objects.</summary>
	[Serializable]
	public partial class PassExpiryNotificationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PassExpiryNotificationEntityFactory() : base("PassExpiryNotificationEntity", VarioSL.Entities.EntityType.PassExpiryNotificationEntity) { }

		/// <summary>Creates a new, empty PassExpiryNotificationEntity object.</summary>
		/// <returns>A new, empty PassExpiryNotificationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PassExpiryNotificationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPassExpiryNotification
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentEntity objects.</summary>
	[Serializable]
	public partial class PaymentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentEntityFactory() : base("PaymentEntity", VarioSL.Entities.EntityType.PaymentEntity) { }

		/// <summary>Creates a new, empty PaymentEntity object.</summary>
		/// <returns>A new, empty PaymentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPayment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentJournalEntity objects.</summary>
	[Serializable]
	public partial class PaymentJournalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentJournalEntityFactory() : base("PaymentJournalEntity", VarioSL.Entities.EntityType.PaymentJournalEntity) { }

		/// <summary>Creates a new, empty PaymentJournalEntity object.</summary>
		/// <returns>A new, empty PaymentJournalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentJournalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentJournal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentJournalToComponentEntity objects.</summary>
	[Serializable]
	public partial class PaymentJournalToComponentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentJournalToComponentEntityFactory() : base("PaymentJournalToComponentEntity", VarioSL.Entities.EntityType.PaymentJournalToComponentEntity) { }

		/// <summary>Creates a new, empty PaymentJournalToComponentEntity object.</summary>
		/// <returns>A new, empty PaymentJournalToComponentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentJournalToComponentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentJournalToComponent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class PaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentMethodEntityFactory() : base("PaymentMethodEntity", VarioSL.Entities.EntityType.PaymentMethodEntity) { }

		/// <summary>Creates a new, empty PaymentMethodEntity object.</summary>
		/// <returns>A new, empty PaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentModalityEntity objects.</summary>
	[Serializable]
	public partial class PaymentModalityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentModalityEntityFactory() : base("PaymentModalityEntity", VarioSL.Entities.EntityType.PaymentModalityEntity) { }

		/// <summary>Creates a new, empty PaymentModalityEntity object.</summary>
		/// <returns>A new, empty PaymentModalityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentModalityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentModality
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentOptionEntity objects.</summary>
	[Serializable]
	public partial class PaymentOptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentOptionEntityFactory() : base("PaymentOptionEntity", VarioSL.Entities.EntityType.PaymentOptionEntity) { }

		/// <summary>Creates a new, empty PaymentOptionEntity object.</summary>
		/// <returns>A new, empty PaymentOptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentOptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentOption
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentProviderEntity objects.</summary>
	[Serializable]
	public partial class PaymentProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentProviderEntityFactory() : base("PaymentProviderEntity", VarioSL.Entities.EntityType.PaymentProviderEntity) { }

		/// <summary>Creates a new, empty PaymentProviderEntity object.</summary>
		/// <returns>A new, empty PaymentProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentProviderAccountEntity objects.</summary>
	[Serializable]
	public partial class PaymentProviderAccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentProviderAccountEntityFactory() : base("PaymentProviderAccountEntity", VarioSL.Entities.EntityType.PaymentProviderAccountEntity) { }

		/// <summary>Creates a new, empty PaymentProviderAccountEntity object.</summary>
		/// <returns>A new, empty PaymentProviderAccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentProviderAccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentProviderAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentRecognitionEntity objects.</summary>
	[Serializable]
	public partial class PaymentRecognitionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentRecognitionEntityFactory() : base("PaymentRecognitionEntity", VarioSL.Entities.EntityType.PaymentRecognitionEntity) { }

		/// <summary>Creates a new, empty PaymentRecognitionEntity object.</summary>
		/// <returns>A new, empty PaymentRecognitionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentRecognitionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentRecognition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentReconciliationEntity objects.</summary>
	[Serializable]
	public partial class PaymentReconciliationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentReconciliationEntityFactory() : base("PaymentReconciliationEntity", VarioSL.Entities.EntityType.PaymentReconciliationEntity) { }

		/// <summary>Creates a new, empty PaymentReconciliationEntity object.</summary>
		/// <returns>A new, empty PaymentReconciliationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentReconciliationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentReconciliation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentReconciliationStateEntity objects.</summary>
	[Serializable]
	public partial class PaymentReconciliationStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentReconciliationStateEntityFactory() : base("PaymentReconciliationStateEntity", VarioSL.Entities.EntityType.PaymentReconciliationStateEntity) { }

		/// <summary>Creates a new, empty PaymentReconciliationStateEntity object.</summary>
		/// <returns>A new, empty PaymentReconciliationStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentReconciliationStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentReconciliationState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentStateEntity objects.</summary>
	[Serializable]
	public partial class PaymentStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentStateEntityFactory() : base("PaymentStateEntity", VarioSL.Entities.EntityType.PaymentStateEntity) { }

		/// <summary>Creates a new, empty PaymentStateEntity object.</summary>
		/// <returns>A new, empty PaymentStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PendingOrderEntity objects.</summary>
	[Serializable]
	public partial class PendingOrderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PendingOrderEntityFactory() : base("PendingOrderEntity", VarioSL.Entities.EntityType.PendingOrderEntity) { }

		/// <summary>Creates a new, empty PendingOrderEntity object.</summary>
		/// <returns>A new, empty PendingOrderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PendingOrderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPendingOrder
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PerformanceEntity objects.</summary>
	[Serializable]
	public partial class PerformanceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PerformanceEntityFactory() : base("PerformanceEntity", VarioSL.Entities.EntityType.PerformanceEntity) { }

		/// <summary>Creates a new, empty PerformanceEntity object.</summary>
		/// <returns>A new, empty PerformanceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PerformanceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPerformance
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PerformanceAggregationEntity objects.</summary>
	[Serializable]
	public partial class PerformanceAggregationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PerformanceAggregationEntityFactory() : base("PerformanceAggregationEntity", VarioSL.Entities.EntityType.PerformanceAggregationEntity) { }

		/// <summary>Creates a new, empty PerformanceAggregationEntity object.</summary>
		/// <returns>A new, empty PerformanceAggregationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PerformanceAggregationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPerformanceAggregation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PerformanceComponentEntity objects.</summary>
	[Serializable]
	public partial class PerformanceComponentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PerformanceComponentEntityFactory() : base("PerformanceComponentEntity", VarioSL.Entities.EntityType.PerformanceComponentEntity) { }

		/// <summary>Creates a new, empty PerformanceComponentEntity object.</summary>
		/// <returns>A new, empty PerformanceComponentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PerformanceComponentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPerformanceComponent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PerformanceIndicatorEntity objects.</summary>
	[Serializable]
	public partial class PerformanceIndicatorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PerformanceIndicatorEntityFactory() : base("PerformanceIndicatorEntity", VarioSL.Entities.EntityType.PerformanceIndicatorEntity) { }

		/// <summary>Creates a new, empty PerformanceIndicatorEntity object.</summary>
		/// <returns>A new, empty PerformanceIndicatorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PerformanceIndicatorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPerformanceIndicator
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PersonEntity objects.</summary>
	[Serializable]
	public partial class PersonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PersonEntityFactory() : base("PersonEntity", VarioSL.Entities.EntityType.PersonEntity) { }

		/// <summary>Creates a new, empty PersonEntity object.</summary>
		/// <returns>A new, empty PersonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PersonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPerson
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("PersonEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PersonAddressEntity objects.</summary>
	[Serializable]
	public partial class PersonAddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PersonAddressEntityFactory() : base("PersonAddressEntity", VarioSL.Entities.EntityType.PersonAddressEntity) { }

		/// <summary>Creates a new, empty PersonAddressEntity object.</summary>
		/// <returns>A new, empty PersonAddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PersonAddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPersonAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PhotographEntity objects.</summary>
	[Serializable]
	public partial class PhotographEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PhotographEntityFactory() : base("PhotographEntity", VarioSL.Entities.EntityType.PhotographEntity) { }

		/// <summary>Creates a new, empty PhotographEntity object.</summary>
		/// <returns>A new, empty PhotographEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PhotographEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPhotograph
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PickupLocationEntity objects.</summary>
	[Serializable]
	public partial class PickupLocationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PickupLocationEntityFactory() : base("PickupLocationEntity", VarioSL.Entities.EntityType.PickupLocationEntity) { }

		/// <summary>Creates a new, empty PickupLocationEntity object.</summary>
		/// <returns>A new, empty PickupLocationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PickupLocationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPickupLocation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PickupLocationToTicketEntity objects.</summary>
	[Serializable]
	public partial class PickupLocationToTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PickupLocationToTicketEntityFactory() : base("PickupLocationToTicketEntity", VarioSL.Entities.EntityType.PickupLocationToTicketEntity) { }

		/// <summary>Creates a new, empty PickupLocationToTicketEntity object.</summary>
		/// <returns>A new, empty PickupLocationToTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PickupLocationToTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPickupLocationToTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PostingEntity objects.</summary>
	[Serializable]
	public partial class PostingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PostingEntityFactory() : base("PostingEntity", VarioSL.Entities.EntityType.PostingEntity) { }

		/// <summary>Creates a new, empty PostingEntity object.</summary>
		/// <returns>A new, empty PostingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PostingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PostingKeyEntity objects.</summary>
	[Serializable]
	public partial class PostingKeyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PostingKeyEntityFactory() : base("PostingKeyEntity", VarioSL.Entities.EntityType.PostingKeyEntity) { }

		/// <summary>Creates a new, empty PostingKeyEntity object.</summary>
		/// <returns>A new, empty PostingKeyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PostingKeyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPostingKey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PostingKeyCategoryEntity objects.</summary>
	[Serializable]
	public partial class PostingKeyCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PostingKeyCategoryEntityFactory() : base("PostingKeyCategoryEntity", VarioSL.Entities.EntityType.PostingKeyCategoryEntity) { }

		/// <summary>Creates a new, empty PostingKeyCategoryEntity object.</summary>
		/// <returns>A new, empty PostingKeyCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PostingKeyCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPostingKeyCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PostingScopeEntity objects.</summary>
	[Serializable]
	public partial class PostingScopeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PostingScopeEntityFactory() : base("PostingScopeEntity", VarioSL.Entities.EntityType.PostingScopeEntity) { }

		/// <summary>Creates a new, empty PostingScopeEntity object.</summary>
		/// <returns>A new, empty PostingScopeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PostingScopeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPostingScope
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceAdjustmentEntity objects.</summary>
	[Serializable]
	public partial class PriceAdjustmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceAdjustmentEntityFactory() : base("PriceAdjustmentEntity", VarioSL.Entities.EntityType.PriceAdjustmentEntity) { }

		/// <summary>Creates a new, empty PriceAdjustmentEntity object.</summary>
		/// <returns>A new, empty PriceAdjustmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceAdjustmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceAdjustment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceAdjustmentTemplateEntity objects.</summary>
	[Serializable]
	public partial class PriceAdjustmentTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceAdjustmentTemplateEntityFactory() : base("PriceAdjustmentTemplateEntity", VarioSL.Entities.EntityType.PriceAdjustmentTemplateEntity) { }

		/// <summary>Creates a new, empty PriceAdjustmentTemplateEntity object.</summary>
		/// <returns>A new, empty PriceAdjustmentTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceAdjustmentTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceAdjustmentTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrinterEntity objects.</summary>
	[Serializable]
	public partial class PrinterEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrinterEntityFactory() : base("PrinterEntity", VarioSL.Entities.EntityType.PrinterEntity) { }

		/// <summary>Creates a new, empty PrinterEntity object.</summary>
		/// <returns>A new, empty PrinterEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrinterEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrinter
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrinterStateEntity objects.</summary>
	[Serializable]
	public partial class PrinterStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrinterStateEntityFactory() : base("PrinterStateEntity", VarioSL.Entities.EntityType.PrinterStateEntity) { }

		/// <summary>Creates a new, empty PrinterStateEntity object.</summary>
		/// <returns>A new, empty PrinterStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrinterStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrinterState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrinterToUnitEntity objects.</summary>
	[Serializable]
	public partial class PrinterToUnitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrinterToUnitEntityFactory() : base("PrinterToUnitEntity", VarioSL.Entities.EntityType.PrinterToUnitEntity) { }

		/// <summary>Creates a new, empty PrinterToUnitEntity object.</summary>
		/// <returns>A new, empty PrinterToUnitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrinterToUnitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrinterToUnit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PrinterTypeEntity objects.</summary>
	[Serializable]
	public partial class PrinterTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PrinterTypeEntityFactory() : base("PrinterTypeEntity", VarioSL.Entities.EntityType.PrinterTypeEntity) { }

		/// <summary>Creates a new, empty PrinterTypeEntity object.</summary>
		/// <returns>A new, empty PrinterTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PrinterTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPrinterType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriorityEntity objects.</summary>
	[Serializable]
	public partial class PriorityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriorityEntityFactory() : base("PriorityEntity", VarioSL.Entities.EntityType.PriorityEntity) { }

		/// <summary>Creates a new, empty PriorityEntity object.</summary>
		/// <returns>A new, empty PriorityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriorityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriority
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductEntity objects.</summary>
	[Serializable]
	public partial class ProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductEntityFactory() : base("ProductEntity", VarioSL.Entities.EntityType.ProductEntity) { }

		/// <summary>Creates a new, empty ProductEntity object.</summary>
		/// <returns>A new, empty ProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductRelationEntity objects.</summary>
	[Serializable]
	public partial class ProductRelationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductRelationEntityFactory() : base("ProductRelationEntity", VarioSL.Entities.EntityType.ProductRelationEntity) { }

		/// <summary>Creates a new, empty ProductRelationEntity object.</summary>
		/// <returns>A new, empty ProductRelationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductRelationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductRelation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductSubsidyEntity objects.</summary>
	[Serializable]
	public partial class ProductSubsidyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductSubsidyEntityFactory() : base("ProductSubsidyEntity", VarioSL.Entities.EntityType.ProductSubsidyEntity) { }

		/// <summary>Creates a new, empty ProductSubsidyEntity object.</summary>
		/// <returns>A new, empty ProductSubsidyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductSubsidyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductSubsidy
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductTerminationEntity objects.</summary>
	[Serializable]
	public partial class ProductTerminationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductTerminationEntityFactory() : base("ProductTerminationEntity", VarioSL.Entities.EntityType.ProductTerminationEntity) { }

		/// <summary>Creates a new, empty ProductTerminationEntity object.</summary>
		/// <returns>A new, empty ProductTerminationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductTerminationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductTermination
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductTerminationTypeEntity objects.</summary>
	[Serializable]
	public partial class ProductTerminationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductTerminationTypeEntityFactory() : base("ProductTerminationTypeEntity", VarioSL.Entities.EntityType.ProductTerminationTypeEntity) { }

		/// <summary>Creates a new, empty ProductTerminationTypeEntity object.</summary>
		/// <returns>A new, empty ProductTerminationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductTerminationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductTerminationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductTypeEntity objects.</summary>
	[Serializable]
	public partial class ProductTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductTypeEntityFactory() : base("ProductTypeEntity", VarioSL.Entities.EntityType.ProductTypeEntity) { }

		/// <summary>Creates a new, empty ProductTypeEntity object.</summary>
		/// <returns>A new, empty ProductTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PromoCodeEntity objects.</summary>
	[Serializable]
	public partial class PromoCodeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PromoCodeEntityFactory() : base("PromoCodeEntity", VarioSL.Entities.EntityType.PromoCodeEntity) { }

		/// <summary>Creates a new, empty PromoCodeEntity object.</summary>
		/// <returns>A new, empty PromoCodeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PromoCodeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPromoCode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PromoCodeStatusEntity objects.</summary>
	[Serializable]
	public partial class PromoCodeStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PromoCodeStatusEntityFactory() : base("PromoCodeStatusEntity", VarioSL.Entities.EntityType.PromoCodeStatusEntity) { }

		/// <summary>Creates a new, empty PromoCodeStatusEntity object.</summary>
		/// <returns>A new, empty PromoCodeStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PromoCodeStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPromoCodeStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProviderAccountStateEntity objects.</summary>
	[Serializable]
	public partial class ProviderAccountStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProviderAccountStateEntityFactory() : base("ProviderAccountStateEntity", VarioSL.Entities.EntityType.ProviderAccountStateEntity) { }

		/// <summary>Creates a new, empty ProviderAccountStateEntity object.</summary>
		/// <returns>A new, empty ProviderAccountStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProviderAccountStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProviderAccountState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProvisioningStateEntity objects.</summary>
	[Serializable]
	public partial class ProvisioningStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProvisioningStateEntityFactory() : base("ProvisioningStateEntity", VarioSL.Entities.EntityType.ProvisioningStateEntity) { }

		/// <summary>Creates a new, empty ProvisioningStateEntity object.</summary>
		/// <returns>A new, empty ProvisioningStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProvisioningStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProvisioningState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RecipientGroupEntity objects.</summary>
	[Serializable]
	public partial class RecipientGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RecipientGroupEntityFactory() : base("RecipientGroupEntity", VarioSL.Entities.EntityType.RecipientGroupEntity) { }

		/// <summary>Creates a new, empty RecipientGroupEntity object.</summary>
		/// <returns>A new, empty RecipientGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RecipientGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRecipientGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RecipientGroupMemberEntity objects.</summary>
	[Serializable]
	public partial class RecipientGroupMemberEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RecipientGroupMemberEntityFactory() : base("RecipientGroupMemberEntity", VarioSL.Entities.EntityType.RecipientGroupMemberEntity) { }

		/// <summary>Creates a new, empty RecipientGroupMemberEntity object.</summary>
		/// <returns>A new, empty RecipientGroupMemberEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RecipientGroupMemberEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRecipientGroupMember
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RefundEntity objects.</summary>
	[Serializable]
	public partial class RefundEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RefundEntityFactory() : base("RefundEntity", VarioSL.Entities.EntityType.RefundEntity) { }

		/// <summary>Creates a new, empty RefundEntity object.</summary>
		/// <returns>A new, empty RefundEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RefundEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRefund
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RefundReasonEntity objects.</summary>
	[Serializable]
	public partial class RefundReasonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RefundReasonEntityFactory() : base("RefundReasonEntity", VarioSL.Entities.EntityType.RefundReasonEntity) { }

		/// <summary>Creates a new, empty RefundReasonEntity object.</summary>
		/// <returns>A new, empty RefundReasonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RefundReasonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRefundReason
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RentalStateEntity objects.</summary>
	[Serializable]
	public partial class RentalStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RentalStateEntityFactory() : base("RentalStateEntity", VarioSL.Entities.EntityType.RentalStateEntity) { }

		/// <summary>Creates a new, empty RentalStateEntity object.</summary>
		/// <returns>A new, empty RentalStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RentalStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRentalState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RentalTypeEntity objects.</summary>
	[Serializable]
	public partial class RentalTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RentalTypeEntityFactory() : base("RentalTypeEntity", VarioSL.Entities.EntityType.RentalTypeEntity) { }

		/// <summary>Creates a new, empty RentalTypeEntity object.</summary>
		/// <returns>A new, empty RentalTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RentalTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRentalType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportEntity objects.</summary>
	[Serializable]
	public partial class ReportEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportEntityFactory() : base("ReportEntity", VarioSL.Entities.EntityType.ReportEntity) { }

		/// <summary>Creates a new, empty ReportEntity object.</summary>
		/// <returns>A new, empty ReportEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReport
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportCategoryEntity objects.</summary>
	[Serializable]
	public partial class ReportCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportCategoryEntityFactory() : base("ReportCategoryEntity", VarioSL.Entities.EntityType.ReportCategoryEntity) { }

		/// <summary>Creates a new, empty ReportCategoryEntity object.</summary>
		/// <returns>A new, empty ReportCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportDataFileEntity objects.</summary>
	[Serializable]
	public partial class ReportDataFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportDataFileEntityFactory() : base("ReportDataFileEntity", VarioSL.Entities.EntityType.ReportDataFileEntity) { }

		/// <summary>Creates a new, empty ReportDataFileEntity object.</summary>
		/// <returns>A new, empty ReportDataFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportDataFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportDataFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportJobEntity objects.</summary>
	[Serializable]
	public partial class ReportJobEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportJobEntityFactory() : base("ReportJobEntity", VarioSL.Entities.EntityType.ReportJobEntity) { }

		/// <summary>Creates a new, empty ReportJobEntity object.</summary>
		/// <returns>A new, empty ReportJobEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportJobEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportJob
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportJobResultEntity objects.</summary>
	[Serializable]
	public partial class ReportJobResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportJobResultEntityFactory() : base("ReportJobResultEntity", VarioSL.Entities.EntityType.ReportJobResultEntity) { }

		/// <summary>Creates a new, empty ReportJobResultEntity object.</summary>
		/// <returns>A new, empty ReportJobResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportJobResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportJobResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportToClientEntity objects.</summary>
	[Serializable]
	public partial class ReportToClientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportToClientEntityFactory() : base("ReportToClientEntity", VarioSL.Entities.EntityType.ReportToClientEntity) { }

		/// <summary>Creates a new, empty ReportToClientEntity object.</summary>
		/// <returns>A new, empty ReportToClientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportToClientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportToClient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RevenueRecognitionEntity objects.</summary>
	[Serializable]
	public partial class RevenueRecognitionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RevenueRecognitionEntityFactory() : base("RevenueRecognitionEntity", VarioSL.Entities.EntityType.RevenueRecognitionEntity) { }

		/// <summary>Creates a new, empty RevenueRecognitionEntity object.</summary>
		/// <returns>A new, empty RevenueRecognitionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RevenueRecognitionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueRecognition
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RevenueSettlementEntity objects.</summary>
	[Serializable]
	public partial class RevenueSettlementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RevenueSettlementEntityFactory() : base("RevenueSettlementEntity", VarioSL.Entities.EntityType.RevenueSettlementEntity) { }

		/// <summary>Creates a new, empty RevenueSettlementEntity object.</summary>
		/// <returns>A new, empty RevenueSettlementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RevenueSettlementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRevenueSettlement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleViolationEntity objects.</summary>
	[Serializable]
	public partial class RuleViolationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleViolationEntityFactory() : base("RuleViolationEntity", VarioSL.Entities.EntityType.RuleViolationEntity) { }

		/// <summary>Creates a new, empty RuleViolationEntity object.</summary>
		/// <returns>A new, empty RuleViolationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleViolationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleViolation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleViolationProviderEntity objects.</summary>
	[Serializable]
	public partial class RuleViolationProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleViolationProviderEntityFactory() : base("RuleViolationProviderEntity", VarioSL.Entities.EntityType.RuleViolationProviderEntity) { }

		/// <summary>Creates a new, empty RuleViolationProviderEntity object.</summary>
		/// <returns>A new, empty RuleViolationProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleViolationProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleViolationProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleViolationSourceTypeEntity objects.</summary>
	[Serializable]
	public partial class RuleViolationSourceTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleViolationSourceTypeEntityFactory() : base("RuleViolationSourceTypeEntity", VarioSL.Entities.EntityType.RuleViolationSourceTypeEntity) { }

		/// <summary>Creates a new, empty RuleViolationSourceTypeEntity object.</summary>
		/// <returns>A new, empty RuleViolationSourceTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleViolationSourceTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleViolationSourceType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleViolationToTransactionEntity objects.</summary>
	[Serializable]
	public partial class RuleViolationToTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleViolationToTransactionEntityFactory() : base("RuleViolationToTransactionEntity", VarioSL.Entities.EntityType.RuleViolationToTransactionEntity) { }

		/// <summary>Creates a new, empty RuleViolationToTransactionEntity object.</summary>
		/// <returns>A new, empty RuleViolationToTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleViolationToTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleViolationToTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RuleViolationTypeEntity objects.</summary>
	[Serializable]
	public partial class RuleViolationTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RuleViolationTypeEntityFactory() : base("RuleViolationTypeEntity", VarioSL.Entities.EntityType.RuleViolationTypeEntity) { }

		/// <summary>Creates a new, empty RuleViolationTypeEntity object.</summary>
		/// <returns>A new, empty RuleViolationTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RuleViolationTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRuleViolationType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SaleEntity objects.</summary>
	[Serializable]
	public partial class SaleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SaleEntityFactory() : base("SaleEntity", VarioSL.Entities.EntityType.SaleEntity) { }

		/// <summary>Creates a new, empty SaleEntity object.</summary>
		/// <returns>A new, empty SaleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SaleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSale
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SalesChannelToPaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class SalesChannelToPaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SalesChannelToPaymentMethodEntityFactory() : base("SalesChannelToPaymentMethodEntity", VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity) { }

		/// <summary>Creates a new, empty SalesChannelToPaymentMethodEntity object.</summary>
		/// <returns>A new, empty SalesChannelToPaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SalesChannelToPaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSalesChannelToPaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SalesRevenueEntity objects.</summary>
	[Serializable]
	public partial class SalesRevenueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SalesRevenueEntityFactory() : base("SalesRevenueEntity", VarioSL.Entities.EntityType.SalesRevenueEntity) { }

		/// <summary>Creates a new, empty SalesRevenueEntity object.</summary>
		/// <returns>A new, empty SalesRevenueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SalesRevenueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSalesRevenue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SaleToComponentEntity objects.</summary>
	[Serializable]
	public partial class SaleToComponentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SaleToComponentEntityFactory() : base("SaleToComponentEntity", VarioSL.Entities.EntityType.SaleToComponentEntity) { }

		/// <summary>Creates a new, empty SaleToComponentEntity object.</summary>
		/// <returns>A new, empty SaleToComponentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SaleToComponentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSaleToComponent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SaleTypeEntity objects.</summary>
	[Serializable]
	public partial class SaleTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SaleTypeEntityFactory() : base("SaleTypeEntity", VarioSL.Entities.EntityType.SaleTypeEntity) { }

		/// <summary>Creates a new, empty SaleTypeEntity object.</summary>
		/// <returns>A new, empty SaleTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SaleTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSaleType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SamModuleEntity objects.</summary>
	[Serializable]
	public partial class SamModuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SamModuleEntityFactory() : base("SamModuleEntity", VarioSL.Entities.EntityType.SamModuleEntity) { }

		/// <summary>Creates a new, empty SamModuleEntity object.</summary>
		/// <returns>A new, empty SamModuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SamModuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSamModule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SamModuleStatusEntity objects.</summary>
	[Serializable]
	public partial class SamModuleStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SamModuleStatusEntityFactory() : base("SamModuleStatusEntity", VarioSL.Entities.EntityType.SamModuleStatusEntity) { }

		/// <summary>Creates a new, empty SamModuleStatusEntity object.</summary>
		/// <returns>A new, empty SamModuleStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SamModuleStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSamModuleStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SchoolYearEntity objects.</summary>
	[Serializable]
	public partial class SchoolYearEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SchoolYearEntityFactory() : base("SchoolYearEntity", VarioSL.Entities.EntityType.SchoolYearEntity) { }

		/// <summary>Creates a new, empty SchoolYearEntity object.</summary>
		/// <returns>A new, empty SchoolYearEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SchoolYearEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSchoolYear
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SecurityQuestionEntity objects.</summary>
	[Serializable]
	public partial class SecurityQuestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SecurityQuestionEntityFactory() : base("SecurityQuestionEntity", VarioSL.Entities.EntityType.SecurityQuestionEntity) { }

		/// <summary>Creates a new, empty SecurityQuestionEntity object.</summary>
		/// <returns>A new, empty SecurityQuestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SecurityQuestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSecurityQuestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SecurityResponseEntity objects.</summary>
	[Serializable]
	public partial class SecurityResponseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SecurityResponseEntityFactory() : base("SecurityResponseEntity", VarioSL.Entities.EntityType.SecurityResponseEntity) { }

		/// <summary>Creates a new, empty SecurityResponseEntity object.</summary>
		/// <returns>A new, empty SecurityResponseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SecurityResponseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSecurityResponse
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SepaFrequencyTypeEntity objects.</summary>
	[Serializable]
	public partial class SepaFrequencyTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SepaFrequencyTypeEntityFactory() : base("SepaFrequencyTypeEntity", VarioSL.Entities.EntityType.SepaFrequencyTypeEntity) { }

		/// <summary>Creates a new, empty SepaFrequencyTypeEntity object.</summary>
		/// <returns>A new, empty SepaFrequencyTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SepaFrequencyTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSepaFrequencyType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SepaOwnerTypeEntity objects.</summary>
	[Serializable]
	public partial class SepaOwnerTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SepaOwnerTypeEntityFactory() : base("SepaOwnerTypeEntity", VarioSL.Entities.EntityType.SepaOwnerTypeEntity) { }

		/// <summary>Creates a new, empty SepaOwnerTypeEntity object.</summary>
		/// <returns>A new, empty SepaOwnerTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SepaOwnerTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSepaOwnerType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServerResponseTimeDataEntity objects.</summary>
	[Serializable]
	public partial class ServerResponseTimeDataEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServerResponseTimeDataEntityFactory() : base("ServerResponseTimeDataEntity", VarioSL.Entities.EntityType.ServerResponseTimeDataEntity) { }

		/// <summary>Creates a new, empty ServerResponseTimeDataEntity object.</summary>
		/// <returns>A new, empty ServerResponseTimeDataEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServerResponseTimeDataEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServerResponseTimeData
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServerResponseTimeDataAggregationEntity objects.</summary>
	[Serializable]
	public partial class ServerResponseTimeDataAggregationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServerResponseTimeDataAggregationEntityFactory() : base("ServerResponseTimeDataAggregationEntity", VarioSL.Entities.EntityType.ServerResponseTimeDataAggregationEntity) { }

		/// <summary>Creates a new, empty ServerResponseTimeDataAggregationEntity object.</summary>
		/// <returns>A new, empty ServerResponseTimeDataAggregationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServerResponseTimeDataAggregationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServerResponseTimeDataAggregation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettledRevenueEntity objects.</summary>
	[Serializable]
	public partial class SettledRevenueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettledRevenueEntityFactory() : base("SettledRevenueEntity", VarioSL.Entities.EntityType.SettledRevenueEntity) { }

		/// <summary>Creates a new, empty SettledRevenueEntity object.</summary>
		/// <returns>A new, empty SettledRevenueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettledRevenueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettledRevenue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementAccountEntity objects.</summary>
	[Serializable]
	public partial class SettlementAccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementAccountEntityFactory() : base("SettlementAccountEntity", VarioSL.Entities.EntityType.SettlementAccountEntity) { }

		/// <summary>Creates a new, empty SettlementAccountEntity object.</summary>
		/// <returns>A new, empty SettlementAccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementAccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementCalendarEntity objects.</summary>
	[Serializable]
	public partial class SettlementCalendarEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementCalendarEntityFactory() : base("SettlementCalendarEntity", VarioSL.Entities.EntityType.SettlementCalendarEntity) { }

		/// <summary>Creates a new, empty SettlementCalendarEntity object.</summary>
		/// <returns>A new, empty SettlementCalendarEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementCalendarEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementCalendar
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementDistributionPolicyEntity objects.</summary>
	[Serializable]
	public partial class SettlementDistributionPolicyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementDistributionPolicyEntityFactory() : base("SettlementDistributionPolicyEntity", VarioSL.Entities.EntityType.SettlementDistributionPolicyEntity) { }

		/// <summary>Creates a new, empty SettlementDistributionPolicyEntity object.</summary>
		/// <returns>A new, empty SettlementDistributionPolicyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementDistributionPolicyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementDistributionPolicy
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementHolidayEntity objects.</summary>
	[Serializable]
	public partial class SettlementHolidayEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementHolidayEntityFactory() : base("SettlementHolidayEntity", VarioSL.Entities.EntityType.SettlementHolidayEntity) { }

		/// <summary>Creates a new, empty SettlementHolidayEntity object.</summary>
		/// <returns>A new, empty SettlementHolidayEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementHolidayEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementHoliday
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementOperationEntity objects.</summary>
	[Serializable]
	public partial class SettlementOperationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementOperationEntityFactory() : base("SettlementOperationEntity", VarioSL.Entities.EntityType.SettlementOperationEntity) { }

		/// <summary>Creates a new, empty SettlementOperationEntity object.</summary>
		/// <returns>A new, empty SettlementOperationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementOperationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementOperation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementQuerySettingEntity objects.</summary>
	[Serializable]
	public partial class SettlementQuerySettingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementQuerySettingEntityFactory() : base("SettlementQuerySettingEntity", VarioSL.Entities.EntityType.SettlementQuerySettingEntity) { }

		/// <summary>Creates a new, empty SettlementQuerySettingEntity object.</summary>
		/// <returns>A new, empty SettlementQuerySettingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementQuerySettingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementQuerySetting
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementQuerySettingToTicketEntity objects.</summary>
	[Serializable]
	public partial class SettlementQuerySettingToTicketEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementQuerySettingToTicketEntityFactory() : base("SettlementQuerySettingToTicketEntity", VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity) { }

		/// <summary>Creates a new, empty SettlementQuerySettingToTicketEntity object.</summary>
		/// <returns>A new, empty SettlementQuerySettingToTicketEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementQuerySettingToTicketEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementQuerySettingToTicket
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementQueryValueEntity objects.</summary>
	[Serializable]
	public partial class SettlementQueryValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementQueryValueEntityFactory() : base("SettlementQueryValueEntity", VarioSL.Entities.EntityType.SettlementQueryValueEntity) { }

		/// <summary>Creates a new, empty SettlementQueryValueEntity object.</summary>
		/// <returns>A new, empty SettlementQueryValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementQueryValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementQueryValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementReleaseStateEntity objects.</summary>
	[Serializable]
	public partial class SettlementReleaseStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementReleaseStateEntityFactory() : base("SettlementReleaseStateEntity", VarioSL.Entities.EntityType.SettlementReleaseStateEntity) { }

		/// <summary>Creates a new, empty SettlementReleaseStateEntity object.</summary>
		/// <returns>A new, empty SettlementReleaseStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementReleaseStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementReleaseState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementResultEntity objects.</summary>
	[Serializable]
	public partial class SettlementResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementResultEntityFactory() : base("SettlementResultEntity", VarioSL.Entities.EntityType.SettlementResultEntity) { }

		/// <summary>Creates a new, empty SettlementResultEntity object.</summary>
		/// <returns>A new, empty SettlementResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementRunEntity objects.</summary>
	[Serializable]
	public partial class SettlementRunEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementRunEntityFactory() : base("SettlementRunEntity", VarioSL.Entities.EntityType.SettlementRunEntity) { }

		/// <summary>Creates a new, empty SettlementRunEntity object.</summary>
		/// <returns>A new, empty SettlementRunEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementRunEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementRun
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementRunValueEntity objects.</summary>
	[Serializable]
	public partial class SettlementRunValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementRunValueEntityFactory() : base("SettlementRunValueEntity", VarioSL.Entities.EntityType.SettlementRunValueEntity) { }

		/// <summary>Creates a new, empty SettlementRunValueEntity object.</summary>
		/// <returns>A new, empty SettlementRunValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementRunValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementRunValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementRunValueToVarioSettlementEntity objects.</summary>
	[Serializable]
	public partial class SettlementRunValueToVarioSettlementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementRunValueToVarioSettlementEntityFactory() : base("SettlementRunValueToVarioSettlementEntity", VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity) { }

		/// <summary>Creates a new, empty SettlementRunValueToVarioSettlementEntity object.</summary>
		/// <returns>A new, empty SettlementRunValueToVarioSettlementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementRunValueToVarioSettlementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementRunValueToVarioSettlement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementSetupEntity objects.</summary>
	[Serializable]
	public partial class SettlementSetupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementSetupEntityFactory() : base("SettlementSetupEntity", VarioSL.Entities.EntityType.SettlementSetupEntity) { }

		/// <summary>Creates a new, empty SettlementSetupEntity object.</summary>
		/// <returns>A new, empty SettlementSetupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementSetupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementSetup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SettlementTypeEntity objects.</summary>
	[Serializable]
	public partial class SettlementTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SettlementTypeEntityFactory() : base("SettlementTypeEntity", VarioSL.Entities.EntityType.SettlementTypeEntity) { }

		/// <summary>Creates a new, empty SettlementTypeEntity object.</summary>
		/// <returns>A new, empty SettlementTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SettlementTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSettlementType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SourceEntity objects.</summary>
	[Serializable]
	public partial class SourceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SourceEntityFactory() : base("SourceEntity", VarioSL.Entities.EntityType.SourceEntity) { }

		/// <summary>Creates a new, empty SourceEntity object.</summary>
		/// <returns>A new, empty SourceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SourceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSource
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StatementOfAccountEntity objects.</summary>
	[Serializable]
	public partial class StatementOfAccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StatementOfAccountEntityFactory() : base("StatementOfAccountEntity", VarioSL.Entities.EntityType.StatementOfAccountEntity) { }

		/// <summary>Creates a new, empty StatementOfAccountEntity object.</summary>
		/// <returns>A new, empty StatementOfAccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StatementOfAccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStatementOfAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StateOfCountryEntity objects.</summary>
	[Serializable]
	public partial class StateOfCountryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StateOfCountryEntityFactory() : base("StateOfCountryEntity", VarioSL.Entities.EntityType.StateOfCountryEntity) { }

		/// <summary>Creates a new, empty StateOfCountryEntity object.</summary>
		/// <returns>A new, empty StateOfCountryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StateOfCountryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStateOfCountry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StockTransferEntity objects.</summary>
	[Serializable]
	public partial class StockTransferEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StockTransferEntityFactory() : base("StockTransferEntity", VarioSL.Entities.EntityType.StockTransferEntity) { }

		/// <summary>Creates a new, empty StockTransferEntity object.</summary>
		/// <returns>A new, empty StockTransferEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StockTransferEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStockTransfer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StorageLocationEntity objects.</summary>
	[Serializable]
	public partial class StorageLocationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StorageLocationEntityFactory() : base("StorageLocationEntity", VarioSL.Entities.EntityType.StorageLocationEntity) { }

		/// <summary>Creates a new, empty StorageLocationEntity object.</summary>
		/// <returns>A new, empty StorageLocationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StorageLocationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStorageLocation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SubsidyLimitEntity objects.</summary>
	[Serializable]
	public partial class SubsidyLimitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SubsidyLimitEntityFactory() : base("SubsidyLimitEntity", VarioSL.Entities.EntityType.SubsidyLimitEntity) { }

		/// <summary>Creates a new, empty SubsidyLimitEntity object.</summary>
		/// <returns>A new, empty SubsidyLimitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SubsidyLimitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSubsidyLimit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SubsidyTransactionEntity objects.</summary>
	[Serializable]
	public partial class SubsidyTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SubsidyTransactionEntityFactory() : base("SubsidyTransactionEntity", VarioSL.Entities.EntityType.SubsidyTransactionEntity) { }

		/// <summary>Creates a new, empty SubsidyTransactionEntity object.</summary>
		/// <returns>A new, empty SubsidyTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SubsidyTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSubsidyTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyEntity objects.</summary>
	[Serializable]
	public partial class SurveyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyEntityFactory() : base("SurveyEntity", VarioSL.Entities.EntityType.SurveyEntity) { }

		/// <summary>Creates a new, empty SurveyEntity object.</summary>
		/// <returns>A new, empty SurveyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurvey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyAnswerEntity objects.</summary>
	[Serializable]
	public partial class SurveyAnswerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyAnswerEntityFactory() : base("SurveyAnswerEntity", VarioSL.Entities.EntityType.SurveyAnswerEntity) { }

		/// <summary>Creates a new, empty SurveyAnswerEntity object.</summary>
		/// <returns>A new, empty SurveyAnswerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyAnswerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyAnswer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyChoiceEntity objects.</summary>
	[Serializable]
	public partial class SurveyChoiceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyChoiceEntityFactory() : base("SurveyChoiceEntity", VarioSL.Entities.EntityType.SurveyChoiceEntity) { }

		/// <summary>Creates a new, empty SurveyChoiceEntity object.</summary>
		/// <returns>A new, empty SurveyChoiceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyChoiceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyChoice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyDescriptionEntity objects.</summary>
	[Serializable]
	public partial class SurveyDescriptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyDescriptionEntityFactory() : base("SurveyDescriptionEntity", VarioSL.Entities.EntityType.SurveyDescriptionEntity) { }

		/// <summary>Creates a new, empty SurveyDescriptionEntity object.</summary>
		/// <returns>A new, empty SurveyDescriptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyDescriptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyDescription
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyQuestionEntity objects.</summary>
	[Serializable]
	public partial class SurveyQuestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyQuestionEntityFactory() : base("SurveyQuestionEntity", VarioSL.Entities.EntityType.SurveyQuestionEntity) { }

		/// <summary>Creates a new, empty SurveyQuestionEntity object.</summary>
		/// <returns>A new, empty SurveyQuestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyQuestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyQuestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyResponseEntity objects.</summary>
	[Serializable]
	public partial class SurveyResponseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyResponseEntityFactory() : base("SurveyResponseEntity", VarioSL.Entities.EntityType.SurveyResponseEntity) { }

		/// <summary>Creates a new, empty SurveyResponseEntity object.</summary>
		/// <returns>A new, empty SurveyResponseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyResponseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyResponse
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TenantEntity objects.</summary>
	[Serializable]
	public partial class TenantEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TenantEntityFactory() : base("TenantEntity", VarioSL.Entities.EntityType.TenantEntity) { }

		/// <summary>Creates a new, empty TenantEntity object.</summary>
		/// <returns>A new, empty TenantEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TenantEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenant
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TenantHistoryEntity objects.</summary>
	[Serializable]
	public partial class TenantHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TenantHistoryEntityFactory() : base("TenantHistoryEntity", VarioSL.Entities.EntityType.TenantHistoryEntity) { }

		/// <summary>Creates a new, empty TenantHistoryEntity object.</summary>
		/// <returns>A new, empty TenantHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TenantHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenantHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TenantPersonEntity objects.</summary>
	[Serializable]
	public partial class TenantPersonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TenantPersonEntityFactory() : base("TenantPersonEntity", VarioSL.Entities.EntityType.TenantPersonEntity) { }

		/// <summary>Creates a new, empty TenantPersonEntity object.</summary>
		/// <returns>A new, empty TenantPersonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TenantPersonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenantPerson
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TenantStateEntity objects.</summary>
	[Serializable]
	public partial class TenantStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TenantStateEntityFactory() : base("TenantStateEntity", VarioSL.Entities.EntityType.TenantStateEntity) { }

		/// <summary>Creates a new, empty TenantStateEntity object.</summary>
		/// <returns>A new, empty TenantStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TenantStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTenantState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminationStatusEntity objects.</summary>
	[Serializable]
	public partial class TerminationStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminationStatusEntityFactory() : base("TerminationStatusEntity", VarioSL.Entities.EntityType.TerminationStatusEntity) { }

		/// <summary>Creates a new, empty TerminationStatusEntity object.</summary>
		/// <returns>A new, empty TerminationStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminationStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminationStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketAssignmentEntity objects.</summary>
	[Serializable]
	public partial class TicketAssignmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketAssignmentEntityFactory() : base("TicketAssignmentEntity", VarioSL.Entities.EntityType.TicketAssignmentEntity) { }

		/// <summary>Creates a new, empty TicketAssignmentEntity object.</summary>
		/// <returns>A new, empty TicketAssignmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketAssignmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketAssignment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketAssignmentStatusEntity objects.</summary>
	[Serializable]
	public partial class TicketAssignmentStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketAssignmentStatusEntityFactory() : base("TicketAssignmentStatusEntity", VarioSL.Entities.EntityType.TicketAssignmentStatusEntity) { }

		/// <summary>Creates a new, empty TicketAssignmentStatusEntity object.</summary>
		/// <returns>A new, empty TicketAssignmentStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketAssignmentStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketAssignmentStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketSerialNumberEntity objects.</summary>
	[Serializable]
	public partial class TicketSerialNumberEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketSerialNumberEntityFactory() : base("TicketSerialNumberEntity", VarioSL.Entities.EntityType.TicketSerialNumberEntity) { }

		/// <summary>Creates a new, empty TicketSerialNumberEntity object.</summary>
		/// <returns>A new, empty TicketSerialNumberEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketSerialNumberEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketSerialNumber
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TicketSerialNumberStatusEntity objects.</summary>
	[Serializable]
	public partial class TicketSerialNumberStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TicketSerialNumberStatusEntityFactory() : base("TicketSerialNumberStatusEntity", VarioSL.Entities.EntityType.TicketSerialNumberStatusEntity) { }

		/// <summary>Creates a new, empty TicketSerialNumberStatusEntity object.</summary>
		/// <returns>A new, empty TicketSerialNumberStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TicketSerialNumberStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTicketSerialNumberStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionJournalEntity objects.</summary>
	[Serializable]
	public partial class TransactionJournalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionJournalEntityFactory() : base("TransactionJournalEntity", VarioSL.Entities.EntityType.TransactionJournalEntity) { }

		/// <summary>Creates a new, empty TransactionJournalEntity object.</summary>
		/// <returns>A new, empty TransactionJournalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionJournalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionJournal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionJournalCorrectionEntity objects.</summary>
	[Serializable]
	public partial class TransactionJournalCorrectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionJournalCorrectionEntityFactory() : base("TransactionJournalCorrectionEntity", VarioSL.Entities.EntityType.TransactionJournalCorrectionEntity) { }

		/// <summary>Creates a new, empty TransactionJournalCorrectionEntity object.</summary>
		/// <returns>A new, empty TransactionJournalCorrectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionJournalCorrectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionJournalCorrection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionJourneyHistoryEntity objects.</summary>
	[Serializable]
	public partial class TransactionJourneyHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionJourneyHistoryEntityFactory() : base("TransactionJourneyHistoryEntity", VarioSL.Entities.EntityType.TransactionJourneyHistoryEntity) { }

		/// <summary>Creates a new, empty TransactionJourneyHistoryEntity object.</summary>
		/// <returns>A new, empty TransactionJourneyHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionJourneyHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionJourneyHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionToInspectionEntity objects.</summary>
	[Serializable]
	public partial class TransactionToInspectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionToInspectionEntityFactory() : base("TransactionToInspectionEntity", VarioSL.Entities.EntityType.TransactionToInspectionEntity) { }

		/// <summary>Creates a new, empty TransactionToInspectionEntity object.</summary>
		/// <returns>A new, empty TransactionToInspectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionToInspectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionToInspection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransactionTypeEntity objects.</summary>
	[Serializable]
	public partial class TransactionTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransactionTypeEntityFactory() : base("TransactionTypeEntity", VarioSL.Entities.EntityType.TransactionTypeEntity) { }

		/// <summary>Creates a new, empty TransactionTypeEntity object.</summary>
		/// <returns>A new, empty TransactionTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransactionTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransactionType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TransportCompanyEntity objects.</summary>
	[Serializable]
	public partial class TransportCompanyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TransportCompanyEntityFactory() : base("TransportCompanyEntity", VarioSL.Entities.EntityType.TransportCompanyEntity) { }

		/// <summary>Creates a new, empty TransportCompanyEntity object.</summary>
		/// <returns>A new, empty TransportCompanyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TransportCompanyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTransportCompany
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TriggerTypeEntity objects.</summary>
	[Serializable]
	public partial class TriggerTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TriggerTypeEntityFactory() : base("TriggerTypeEntity", VarioSL.Entities.EntityType.TriggerTypeEntity) { }

		/// <summary>Creates a new, empty TriggerTypeEntity object.</summary>
		/// <returns>A new, empty TriggerTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TriggerTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTriggerType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UnitCollectionEntity objects.</summary>
	[Serializable]
	public partial class UnitCollectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UnitCollectionEntityFactory() : base("UnitCollectionEntity", VarioSL.Entities.EntityType.UnitCollectionEntity) { }

		/// <summary>Creates a new, empty UnitCollectionEntity object.</summary>
		/// <returns>A new, empty UnitCollectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UnitCollectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUnitCollection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UnitCollectionToUnitEntity objects.</summary>
	[Serializable]
	public partial class UnitCollectionToUnitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UnitCollectionToUnitEntityFactory() : base("UnitCollectionToUnitEntity", VarioSL.Entities.EntityType.UnitCollectionToUnitEntity) { }

		/// <summary>Creates a new, empty UnitCollectionToUnitEntity object.</summary>
		/// <returns>A new, empty UnitCollectionToUnitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UnitCollectionToUnitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUnitCollectionToUnit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationResultEntity objects.</summary>
	[Serializable]
	public partial class ValidationResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationResultEntityFactory() : base("ValidationResultEntity", VarioSL.Entities.EntityType.ValidationResultEntity) { }

		/// <summary>Creates a new, empty ValidationResultEntity object.</summary>
		/// <returns>A new, empty ValidationResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationResultResolveTypeEntity objects.</summary>
	[Serializable]
	public partial class ValidationResultResolveTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationResultResolveTypeEntityFactory() : base("ValidationResultResolveTypeEntity", VarioSL.Entities.EntityType.ValidationResultResolveTypeEntity) { }

		/// <summary>Creates a new, empty ValidationResultResolveTypeEntity object.</summary>
		/// <returns>A new, empty ValidationResultResolveTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationResultResolveTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationResultResolveType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationRuleEntity objects.</summary>
	[Serializable]
	public partial class ValidationRuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationRuleEntityFactory() : base("ValidationRuleEntity", VarioSL.Entities.EntityType.ValidationRuleEntity) { }

		/// <summary>Creates a new, empty ValidationRuleEntity object.</summary>
		/// <returns>A new, empty ValidationRuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationRuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationRule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationRunEntity objects.</summary>
	[Serializable]
	public partial class ValidationRunEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationRunEntityFactory() : base("ValidationRunEntity", VarioSL.Entities.EntityType.ValidationRunEntity) { }

		/// <summary>Creates a new, empty ValidationRunEntity object.</summary>
		/// <returns>A new, empty ValidationRunEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationRunEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationRun
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationRunRuleEntity objects.</summary>
	[Serializable]
	public partial class ValidationRunRuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationRunRuleEntityFactory() : base("ValidationRunRuleEntity", VarioSL.Entities.EntityType.ValidationRunRuleEntity) { }

		/// <summary>Creates a new, empty ValidationRunRuleEntity object.</summary>
		/// <returns>A new, empty ValidationRunRuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationRunRuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationRunRule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationStatusEntity objects.</summary>
	[Serializable]
	public partial class ValidationStatusEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationStatusEntityFactory() : base("ValidationStatusEntity", VarioSL.Entities.EntityType.ValidationStatusEntity) { }

		/// <summary>Creates a new, empty ValidationStatusEntity object.</summary>
		/// <returns>A new, empty ValidationStatusEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationStatusEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationStatus
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ValidationValueEntity objects.</summary>
	[Serializable]
	public partial class ValidationValueEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ValidationValueEntityFactory() : base("ValidationValueEntity", VarioSL.Entities.EntityType.ValidationValueEntity) { }

		/// <summary>Creates a new, empty ValidationValueEntity object.</summary>
		/// <returns>A new, empty ValidationValueEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ValidationValueEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewValidationValue
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VdvTransactionEntity objects.</summary>
	[Serializable]
	public partial class VdvTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VdvTransactionEntityFactory() : base("VdvTransactionEntity", VarioSL.Entities.EntityType.VdvTransactionEntity) { }

		/// <summary>Creates a new, empty VdvTransactionEntity object.</summary>
		/// <returns>A new, empty VdvTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VdvTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVdvTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VerificationAttemptEntity objects.</summary>
	[Serializable]
	public partial class VerificationAttemptEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VerificationAttemptEntityFactory() : base("VerificationAttemptEntity", VarioSL.Entities.EntityType.VerificationAttemptEntity) { }

		/// <summary>Creates a new, empty VerificationAttemptEntity object.</summary>
		/// <returns>A new, empty VerificationAttemptEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VerificationAttemptEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVerificationAttempt
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VerificationAttemptTypeEntity objects.</summary>
	[Serializable]
	public partial class VerificationAttemptTypeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VerificationAttemptTypeEntityFactory() : base("VerificationAttemptTypeEntity", VarioSL.Entities.EntityType.VerificationAttemptTypeEntity) { }

		/// <summary>Creates a new, empty VerificationAttemptTypeEntity object.</summary>
		/// <returns>A new, empty VerificationAttemptTypeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VerificationAttemptTypeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVerificationAttemptType
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VirtualCardEntity objects.</summary>
	[Serializable]
	public partial class VirtualCardEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VirtualCardEntityFactory() : base("VirtualCardEntity", VarioSL.Entities.EntityType.VirtualCardEntity) { }

		/// <summary>Creates a new, empty VirtualCardEntity object.</summary>
		/// <returns>A new, empty VirtualCardEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VirtualCardEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVirtualCard
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VoucherEntity objects.</summary>
	[Serializable]
	public partial class VoucherEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VoucherEntityFactory() : base("VoucherEntity", VarioSL.Entities.EntityType.VoucherEntity) { }

		/// <summary>Creates a new, empty VoucherEntity object.</summary>
		/// <returns>A new, empty VoucherEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VoucherEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVoucher
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WhitelistEntity objects.</summary>
	[Serializable]
	public partial class WhitelistEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WhitelistEntityFactory() : base("WhitelistEntity", VarioSL.Entities.EntityType.WhitelistEntity) { }

		/// <summary>Creates a new, empty WhitelistEntity object.</summary>
		/// <returns>A new, empty WhitelistEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WhitelistEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWhitelist
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WhitelistJournalEntity objects.</summary>
	[Serializable]
	public partial class WhitelistJournalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WhitelistJournalEntityFactory() : base("WhitelistJournalEntity", VarioSL.Entities.EntityType.WhitelistJournalEntity) { }

		/// <summary>Creates a new, empty WhitelistJournalEntity object.</summary>
		/// <returns>A new, empty WhitelistJournalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WhitelistJournalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWhitelistJournal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemEntity objects.</summary>
	[Serializable]
	public partial class WorkItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemEntityFactory() : base("WorkItemEntity", VarioSL.Entities.EntityType.WorkItemEntity) { }

		/// <summary>Creates a new, empty WorkItemEntity object.</summary>
		/// <returns>A new, empty WorkItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WorkItemEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemCategoryEntity objects.</summary>
	[Serializable]
	public partial class WorkItemCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemCategoryEntityFactory() : base("WorkItemCategoryEntity", VarioSL.Entities.EntityType.WorkItemCategoryEntity) { }

		/// <summary>Creates a new, empty WorkItemCategoryEntity object.</summary>
		/// <returns>A new, empty WorkItemCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItemCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemHistoryEntity objects.</summary>
	[Serializable]
	public partial class WorkItemHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemHistoryEntityFactory() : base("WorkItemHistoryEntity", VarioSL.Entities.EntityType.WorkItemHistoryEntity) { }

		/// <summary>Creates a new, empty WorkItemHistoryEntity object.</summary>
		/// <returns>A new, empty WorkItemHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItemHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemStateEntity objects.</summary>
	[Serializable]
	public partial class WorkItemStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemStateEntityFactory() : base("WorkItemStateEntity", VarioSL.Entities.EntityType.WorkItemStateEntity) { }

		/// <summary>Creates a new, empty WorkItemStateEntity object.</summary>
		/// <returns>A new, empty WorkItemStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItemState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemSubjectEntity objects.</summary>
	[Serializable]
	public partial class WorkItemSubjectEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemSubjectEntityFactory() : base("WorkItemSubjectEntity", VarioSL.Entities.EntityType.WorkItemSubjectEntity) { }

		/// <summary>Creates a new, empty WorkItemSubjectEntity object.</summary>
		/// <returns>A new, empty WorkItemSubjectEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemSubjectEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItemSubject
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WorkItemViewEntity objects.</summary>
	[Serializable]
	public partial class WorkItemViewEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WorkItemViewEntityFactory() : base("WorkItemViewEntity", VarioSL.Entities.EntityType.WorkItemViewEntity) { }

		/// <summary>Creates a new, empty WorkItemViewEntity object.</summary>
		/// <returns>A new, empty WorkItemViewEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WorkItemViewEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWorkItemView
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new entity collection objects</summary>
	[Serializable]
	public partial class GeneralEntityCollectionFactory
	{
		/// <summary>Creates a new entity collection</summary>
		/// <param name="typeToUse">The entity type to create the collection for.</param>
		/// <returns>A new entity collection object.</returns>
		public static IEntityCollection Create(VarioSL.Entities.EntityType typeToUse)
		{
			switch(typeToUse)
			{
				case VarioSL.Entities.EntityType.AbortedTransactionEntity:
					return new AbortedTransactionCollection();
				case VarioSL.Entities.EntityType.AccountBalanceEntity:
					return new AccountBalanceCollection();
				case VarioSL.Entities.EntityType.AccountEntryEntity:
					return new AccountEntryCollection();
				case VarioSL.Entities.EntityType.AccountEntryNumberEntity:
					return new AccountEntryNumberCollection();
				case VarioSL.Entities.EntityType.AccountPostingKeyEntity:
					return new AccountPostingKeyCollection();
				case VarioSL.Entities.EntityType.ApplicationEntity:
					return new ApplicationCollection();
				case VarioSL.Entities.EntityType.ApplicationVersionEntity:
					return new ApplicationVersionCollection();
				case VarioSL.Entities.EntityType.ApportionmentEntity:
					return new ApportionmentCollection();
				case VarioSL.Entities.EntityType.ApportionmentResultEntity:
					return new ApportionmentResultCollection();
				case VarioSL.Entities.EntityType.AreaListEntity:
					return new AreaListCollection();
				case VarioSL.Entities.EntityType.AreaListElementEntity:
					return new AreaListElementCollection();
				case VarioSL.Entities.EntityType.AreaListElementGroupEntity:
					return new AreaListElementGroupCollection();
				case VarioSL.Entities.EntityType.AreaTypeEntity:
					return new AreaTypeCollection();
				case VarioSL.Entities.EntityType.AttributeEntity:
					return new AttributeCollection();
				case VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity:
					return new AttributeBitmapLayoutObjectCollection();
				case VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity:
					return new AttributeTextLayoutObjectCollection();
				case VarioSL.Entities.EntityType.AttributeValueEntity:
					return new AttributeValueCollection();
				case VarioSL.Entities.EntityType.AttributeValueListEntity:
					return new AttributeValueListCollection();
				case VarioSL.Entities.EntityType.AutomatEntity:
					return new AutomatCollection();
				case VarioSL.Entities.EntityType.BankEntity:
					return new BankCollection();
				case VarioSL.Entities.EntityType.BinaryDataEntity:
					return new BinaryDataCollection();
				case VarioSL.Entities.EntityType.BlockingReasonEntity:
					return new BlockingReasonCollection();
				case VarioSL.Entities.EntityType.BusinessRuleEntity:
					return new BusinessRuleCollection();
				case VarioSL.Entities.EntityType.BusinessRuleClauseEntity:
					return new BusinessRuleClauseCollection();
				case VarioSL.Entities.EntityType.BusinessRuleConditionEntity:
					return new BusinessRuleConditionCollection();
				case VarioSL.Entities.EntityType.BusinessRuleResultEntity:
					return new BusinessRuleResultCollection();
				case VarioSL.Entities.EntityType.BusinessRuleTypeEntity:
					return new BusinessRuleTypeCollection();
				case VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity:
					return new BusinessRuleTypeToVariableCollection();
				case VarioSL.Entities.EntityType.BusinessRuleVariableEntity:
					return new BusinessRuleVariableCollection();
				case VarioSL.Entities.EntityType.CalendarEntity:
					return new CalendarCollection();
				case VarioSL.Entities.EntityType.CalendarEntryEntity:
					return new CalendarEntryCollection();
				case VarioSL.Entities.EntityType.CardActionAttributeEntity:
					return new CardActionAttributeCollection();
				case VarioSL.Entities.EntityType.CardActionRequestEntity:
					return new CardActionRequestCollection();
				case VarioSL.Entities.EntityType.CardActionRequestTypeEntity:
					return new CardActionRequestTypeCollection();
				case VarioSL.Entities.EntityType.CardChipTypeEntity:
					return new CardChipTypeCollection();
				case VarioSL.Entities.EntityType.CardStateEntity:
					return new CardStateCollection();
				case VarioSL.Entities.EntityType.CardTicketEntity:
					return new CardTicketCollection();
				case VarioSL.Entities.EntityType.CardTicketToTicketEntity:
					return new CardTicketToTicketCollection();
				case VarioSL.Entities.EntityType.CashServiceEntity:
					return new CashServiceCollection();
				case VarioSL.Entities.EntityType.CashServiceBalanceEntity:
					return new CashServiceBalanceCollection();
				case VarioSL.Entities.EntityType.CashServiceDataEntity:
					return new CashServiceDataCollection();
				case VarioSL.Entities.EntityType.CashUnitEntity:
					return new CashUnitCollection();
				case VarioSL.Entities.EntityType.ChoiceEntity:
					return new ChoiceCollection();
				case VarioSL.Entities.EntityType.ClearingEntity:
					return new ClearingCollection();
				case VarioSL.Entities.EntityType.ClearingClassificationEntity:
					return new ClearingClassificationCollection();
				case VarioSL.Entities.EntityType.ClearingDetailEntity:
					return new ClearingDetailCollection();
				case VarioSL.Entities.EntityType.ClearingResultEntity:
					return new ClearingResultCollection();
				case VarioSL.Entities.EntityType.ClearingResultLevelEntity:
					return new ClearingResultLevelCollection();
				case VarioSL.Entities.EntityType.ClearingStateEntity:
					return new ClearingStateCollection();
				case VarioSL.Entities.EntityType.ClearingSumEntity:
					return new ClearingSumCollection();
				case VarioSL.Entities.EntityType.ClearingTransactionEntity:
					return new ClearingTransactionCollection();
				case VarioSL.Entities.EntityType.ClientEntity:
					return new ClientCollection();
				case VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity:
					return new ClientAdaptedLayoutObjectCollection();
				case VarioSL.Entities.EntityType.ComponentEntity:
					return new ComponentCollection();
				case VarioSL.Entities.EntityType.ComponentClearingEntity:
					return new ComponentClearingCollection();
				case VarioSL.Entities.EntityType.ComponentFillingEntity:
					return new ComponentFillingCollection();
				case VarioSL.Entities.EntityType.ComponentStateEntity:
					return new ComponentStateCollection();
				case VarioSL.Entities.EntityType.ComponentTypeEntity:
					return new ComponentTypeCollection();
				case VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity:
					return new ConditionalSubPageLayoutObjectCollection();
				case VarioSL.Entities.EntityType.CreditScreeningEntity:
					return new CreditScreeningCollection();
				case VarioSL.Entities.EntityType.DayTypeEntity:
					return new DayTypeCollection();
				case VarioSL.Entities.EntityType.DebtorEntity:
					return new DebtorCollection();
				case VarioSL.Entities.EntityType.DebtorCardEntity:
					return new DebtorCardCollection();
				case VarioSL.Entities.EntityType.DebtorCardHistoryEntity:
					return new DebtorCardHistoryCollection();
				case VarioSL.Entities.EntityType.DefaultPinEntity:
					return new DefaultPinCollection();
				case VarioSL.Entities.EntityType.DepotEntity:
					return new DepotCollection();
				case VarioSL.Entities.EntityType.DeviceEntity:
					return new DeviceCollection();
				case VarioSL.Entities.EntityType.DeviceBookingStateEntity:
					return new DeviceBookingStateCollection();
				case VarioSL.Entities.EntityType.DeviceClassEntity:
					return new DeviceClassCollection();
				case VarioSL.Entities.EntityType.DevicePaymentMethodEntity:
					return new DevicePaymentMethodCollection();
				case VarioSL.Entities.EntityType.DirectionEntity:
					return new DirectionCollection();
				case VarioSL.Entities.EntityType.DunningLevelEntity:
					return new DunningLevelCollection();
				case VarioSL.Entities.EntityType.EticketEntity:
					return new EticketCollection();
				case VarioSL.Entities.EntityType.ExportInfoEntity:
					return new ExportInfoCollection();
				case VarioSL.Entities.EntityType.ExternalCardEntity:
					return new ExternalCardCollection();
				case VarioSL.Entities.EntityType.ExternalDataEntity:
					return new ExternalDataCollection();
				case VarioSL.Entities.EntityType.ExternalEffortEntity:
					return new ExternalEffortCollection();
				case VarioSL.Entities.EntityType.ExternalPacketEntity:
					return new ExternalPacketCollection();
				case VarioSL.Entities.EntityType.ExternalPacketEffortEntity:
					return new ExternalPacketEffortCollection();
				case VarioSL.Entities.EntityType.ExternalTypeEntity:
					return new ExternalTypeCollection();
				case VarioSL.Entities.EntityType.FareEvasionCategoryEntity:
					return new FareEvasionCategoryCollection();
				case VarioSL.Entities.EntityType.FareEvasionReasonEntity:
					return new FareEvasionReasonCollection();
				case VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity:
					return new FareEvasionReasonToCategoryCollection();
				case VarioSL.Entities.EntityType.FareMatrixEntity:
					return new FareMatrixCollection();
				case VarioSL.Entities.EntityType.FareMatrixEntryEntity:
					return new FareMatrixEntryCollection();
				case VarioSL.Entities.EntityType.FareStageEntity:
					return new FareStageCollection();
				case VarioSL.Entities.EntityType.FareStageAliasEntity:
					return new FareStageAliasCollection();
				case VarioSL.Entities.EntityType.FareStageHierarchieLevelEntity:
					return new FareStageHierarchieLevelCollection();
				case VarioSL.Entities.EntityType.FareStageListEntity:
					return new FareStageListCollection();
				case VarioSL.Entities.EntityType.FareStageTypeEntity:
					return new FareStageTypeCollection();
				case VarioSL.Entities.EntityType.FareTableEntity:
					return new FareTableCollection();
				case VarioSL.Entities.EntityType.FareTableEntryEntity:
					return new FareTableEntryCollection();
				case VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity:
					return new FixedBitmapLayoutObjectCollection();
				case VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity:
					return new FixedTextLayoutObjectCollection();
				case VarioSL.Entities.EntityType.GuiDefEntity:
					return new GuiDefCollection();
				case VarioSL.Entities.EntityType.KeyAttributeTransfromEntity:
					return new KeyAttributeTransfromCollection();
				case VarioSL.Entities.EntityType.KeySelectionModeEntity:
					return new KeySelectionModeCollection();
				case VarioSL.Entities.EntityType.KVVSubscriptionEntity:
					return new KVVSubscriptionCollection();
				case VarioSL.Entities.EntityType.LanguageEntity:
					return new LanguageCollection();
				case VarioSL.Entities.EntityType.LayoutEntity:
					return new LayoutCollection();
				case VarioSL.Entities.EntityType.LineEntity:
					return new LineCollection();
				case VarioSL.Entities.EntityType.LineGroupEntity:
					return new LineGroupCollection();
				case VarioSL.Entities.EntityType.LineGroupToLineGroupEntity:
					return new LineGroupToLineGroupCollection();
				case VarioSL.Entities.EntityType.LineToLineGroupEntity:
					return new LineToLineGroupCollection();
				case VarioSL.Entities.EntityType.ListLayoutObjectEntity:
					return new ListLayoutObjectCollection();
				case VarioSL.Entities.EntityType.ListLayoutTypeEntity:
					return new ListLayoutTypeCollection();
				case VarioSL.Entities.EntityType.LogoEntity:
					return new LogoCollection();
				case VarioSL.Entities.EntityType.NetEntity:
					return new NetCollection();
				case VarioSL.Entities.EntityType.NumberRangeEntity:
					return new NumberRangeCollection();
				case VarioSL.Entities.EntityType.OperatorEntity:
					return new OperatorCollection();
				case VarioSL.Entities.EntityType.OutputDeviceEntity:
					return new OutputDeviceCollection();
				case VarioSL.Entities.EntityType.PageContentGroupEntity:
					return new PageContentGroupCollection();
				case VarioSL.Entities.EntityType.PageContentGroupToContentEntity:
					return new PageContentGroupToContentCollection();
				case VarioSL.Entities.EntityType.PanelEntity:
					return new PanelCollection();
				case VarioSL.Entities.EntityType.ParameterAttributeValueEntity:
					return new ParameterAttributeValueCollection();
				case VarioSL.Entities.EntityType.ParameterFareStageEntity:
					return new ParameterFareStageCollection();
				case VarioSL.Entities.EntityType.ParameterTariffEntity:
					return new ParameterTariffCollection();
				case VarioSL.Entities.EntityType.ParameterTicketEntity:
					return new ParameterTicketCollection();
				case VarioSL.Entities.EntityType.PaymentDetailEntity:
					return new PaymentDetailCollection();
				case VarioSL.Entities.EntityType.PaymentIntervalEntity:
					return new PaymentIntervalCollection();
				case VarioSL.Entities.EntityType.PointOfSaleEntity:
					return new PointOfSaleCollection();
				case VarioSL.Entities.EntityType.PredefinedKeyEntity:
					return new PredefinedKeyCollection();
				case VarioSL.Entities.EntityType.PriceTypeEntity:
					return new PriceTypeCollection();
				case VarioSL.Entities.EntityType.PrimalKeyEntity:
					return new PrimalKeyCollection();
				case VarioSL.Entities.EntityType.PrintTextEntity:
					return new PrintTextCollection();
				case VarioSL.Entities.EntityType.ProductOnCardEntity:
					return new ProductOnCardCollection();
				case VarioSL.Entities.EntityType.ProtocolEntity:
					return new ProtocolCollection();
				case VarioSL.Entities.EntityType.ProtocolActionEntity:
					return new ProtocolActionCollection();
				case VarioSL.Entities.EntityType.ProtocolFunctionEntity:
					return new ProtocolFunctionCollection();
				case VarioSL.Entities.EntityType.ProtocolFunctionGroupEntity:
					return new ProtocolFunctionGroupCollection();
				case VarioSL.Entities.EntityType.ProtocolMessageEntity:
					return new ProtocolMessageCollection();
				case VarioSL.Entities.EntityType.QualificationEntity:
					return new QualificationCollection();
				case VarioSL.Entities.EntityType.ResponsibilityEntity:
					return new ResponsibilityCollection();
				case VarioSL.Entities.EntityType.RevenueTransactionTypeEntity:
					return new RevenueTransactionTypeCollection();
				case VarioSL.Entities.EntityType.RmPaymentEntity:
					return new RmPaymentCollection();
				case VarioSL.Entities.EntityType.RouteEntity:
					return new RouteCollection();
				case VarioSL.Entities.EntityType.RouteNameEntity:
					return new RouteNameCollection();
				case VarioSL.Entities.EntityType.RuleCappingEntity:
					return new RuleCappingCollection();
				case VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity:
					return new RuleCappingToFtEntryCollection();
				case VarioSL.Entities.EntityType.RuleCappingToTicketEntity:
					return new RuleCappingToTicketCollection();
				case VarioSL.Entities.EntityType.RulePeriodEntity:
					return new RulePeriodCollection();
				case VarioSL.Entities.EntityType.RuleTypeEntity:
					return new RuleTypeCollection();
				case VarioSL.Entities.EntityType.SalutationEntity:
					return new SalutationCollection();
				case VarioSL.Entities.EntityType.ServiceAllocationEntity:
					return new ServiceAllocationCollection();
				case VarioSL.Entities.EntityType.ServiceIdToCardEntity:
					return new ServiceIdToCardCollection();
				case VarioSL.Entities.EntityType.ServicePercentageEntity:
					return new ServicePercentageCollection();
				case VarioSL.Entities.EntityType.ShiftEntity:
					return new ShiftCollection();
				case VarioSL.Entities.EntityType.ShiftInventoryEntity:
					return new ShiftInventoryCollection();
				case VarioSL.Entities.EntityType.ShiftStateEntity:
					return new ShiftStateCollection();
				case VarioSL.Entities.EntityType.ShortDistanceEntity:
					return new ShortDistanceCollection();
				case VarioSL.Entities.EntityType.SpecialReceiptEntity:
					return new SpecialReceiptCollection();
				case VarioSL.Entities.EntityType.StopEntity:
					return new StopCollection();
				case VarioSL.Entities.EntityType.SystemFieldEntity:
					return new SystemFieldCollection();
				case VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity:
					return new SystemFieldBarcodeLayoutObjectCollection();
				case VarioSL.Entities.EntityType.SystemFieldDynamicGraphicLayoutObjectEntity:
					return new SystemFieldDynamicGraphicLayoutObjectCollection();
				case VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity:
					return new SystemFieldTextLayoutObjectCollection();
				case VarioSL.Entities.EntityType.SystemTextEntity:
					return new SystemTextCollection();
				case VarioSL.Entities.EntityType.TariffEntity:
					return new TariffCollection();
				case VarioSL.Entities.EntityType.TariffParameterEntity:
					return new TariffParameterCollection();
				case VarioSL.Entities.EntityType.TariffReleaseEntity:
					return new TariffReleaseCollection();
				case VarioSL.Entities.EntityType.TemporalTypeEntity:
					return new TemporalTypeCollection();
				case VarioSL.Entities.EntityType.TicketEntity:
					return new TicketCollection();
				case VarioSL.Entities.EntityType.TicketCancellationTypeEntity:
					return new TicketCancellationTypeCollection();
				case VarioSL.Entities.EntityType.TicketCategoryEntity:
					return new TicketCategoryCollection();
				case VarioSL.Entities.EntityType.TicketDayTypeEntity:
					return new TicketDayTypeCollection();
				case VarioSL.Entities.EntityType.TicketDeviceClassEntity:
					return new TicketDeviceClassCollection();
				case VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity:
					return new TicketDeviceClassOutputDeviceCollection();
				case VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity:
					return new TicketDeviceClassPaymentMethodCollection();
				case VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity:
					return new TicketDevicePaymentMethodCollection();
				case VarioSL.Entities.EntityType.TicketGroupEntity:
					return new TicketGroupCollection();
				case VarioSL.Entities.EntityType.TicketOrganizationEntity:
					return new TicketOrganizationCollection();
				case VarioSL.Entities.EntityType.TicketOutputdeviceEntity:
					return new TicketOutputdeviceCollection();
				case VarioSL.Entities.EntityType.TicketPaymentIntervalEntity:
					return new TicketPaymentIntervalCollection();
				case VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity:
					return new TicketPhysicalCardTypeCollection();
				case VarioSL.Entities.EntityType.TicketSelectionModeEntity:
					return new TicketSelectionModeCollection();
				case VarioSL.Entities.EntityType.TicketServicesPermittedEntity:
					return new TicketServicesPermittedCollection();
				case VarioSL.Entities.EntityType.TicketToGroupEntity:
					return new TicketToGroupCollection();
				case VarioSL.Entities.EntityType.TicketTypeEntity:
					return new TicketTypeCollection();
				case VarioSL.Entities.EntityType.TicketVendingClientEntity:
					return new TicketVendingClientCollection();
				case VarioSL.Entities.EntityType.TransactionEntity:
					return new TransactionCollection();
				case VarioSL.Entities.EntityType.TransactionAdditionalEntity:
					return new TransactionAdditionalCollection();
				case VarioSL.Entities.EntityType.TransactionBackupEntity:
					return new TransactionBackupCollection();
				case VarioSL.Entities.EntityType.TransactionExtensionEntity:
					return new TransactionExtensionCollection();
				case VarioSL.Entities.EntityType.TranslationEntity:
					return new TranslationCollection();
				case VarioSL.Entities.EntityType.TypeOfCardEntity:
					return new TypeOfCardCollection();
				case VarioSL.Entities.EntityType.TypeOfUnitEntity:
					return new TypeOfUnitCollection();
				case VarioSL.Entities.EntityType.UnitEntity:
					return new UnitCollection();
				case VarioSL.Entities.EntityType.UserConfigEntity:
					return new UserConfigCollection();
				case VarioSL.Entities.EntityType.UserGroupEntity:
					return new UserGroupCollection();
				case VarioSL.Entities.EntityType.UserGroupRightEntity:
					return new UserGroupRightCollection();
				case VarioSL.Entities.EntityType.UserIsMemberEntity:
					return new UserIsMemberCollection();
				case VarioSL.Entities.EntityType.UserKeyEntity:
					return new UserKeyCollection();
				case VarioSL.Entities.EntityType.UserLanguageEntity:
					return new UserLanguageCollection();
				case VarioSL.Entities.EntityType.UserListEntity:
					return new UserListCollection();
				case VarioSL.Entities.EntityType.UserResourceEntity:
					return new UserResourceCollection();
				case VarioSL.Entities.EntityType.UserStatusEntity:
					return new UserStatusCollection();
				case VarioSL.Entities.EntityType.UserToWorkstationEntity:
					return new UserToWorkstationCollection();
				case VarioSL.Entities.EntityType.ValidationTransactionEntity:
					return new ValidationTransactionCollection();
				case VarioSL.Entities.EntityType.VarioAddressEntity:
					return new VarioAddressCollection();
				case VarioSL.Entities.EntityType.VarioSettlementEntity:
					return new VarioSettlementCollection();
				case VarioSL.Entities.EntityType.VarioTypeOfSettlementEntity:
					return new VarioTypeOfSettlementCollection();
				case VarioSL.Entities.EntityType.VdvKeyInfoEntity:
					return new VdvKeyInfoCollection();
				case VarioSL.Entities.EntityType.VdvKeySetEntity:
					return new VdvKeySetCollection();
				case VarioSL.Entities.EntityType.VdvLayoutEntity:
					return new VdvLayoutCollection();
				case VarioSL.Entities.EntityType.VdvLayoutObjectEntity:
					return new VdvLayoutObjectCollection();
				case VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity:
					return new VdvLoadKeyMessageCollection();
				case VarioSL.Entities.EntityType.VdvProductEntity:
					return new VdvProductCollection();
				case VarioSL.Entities.EntityType.VdvSamStatusEntity:
					return new VdvSamStatusCollection();
				case VarioSL.Entities.EntityType.VdvTagEntity:
					return new VdvTagCollection();
				case VarioSL.Entities.EntityType.VdvTerminalEntity:
					return new VdvTerminalCollection();
				case VarioSL.Entities.EntityType.VdvTypeEntity:
					return new VdvTypeCollection();
				case VarioSL.Entities.EntityType.WorkstationEntity:
					return new WorkstationCollection();
				case VarioSL.Entities.EntityType.AccountingCancelRecTapCmlSettledEntity:
					return new AccountingCancelRecTapCmlSettledCollection();
				case VarioSL.Entities.EntityType.AccountingCancelRecTapTabSettledEntity:
					return new AccountingCancelRecTapTabSettledCollection();
				case VarioSL.Entities.EntityType.AccountingMethodEntity:
					return new AccountingMethodCollection();
				case VarioSL.Entities.EntityType.AccountingModeEntity:
					return new AccountingModeCollection();
				case VarioSL.Entities.EntityType.AccountingRecLoadUseCmlSettledEntity:
					return new AccountingRecLoadUseCmlSettledCollection();
				case VarioSL.Entities.EntityType.AccountingRecLoadUseTabSettledEntity:
					return new AccountingRecLoadUseTabSettledCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedLoadAndUseEntity:
					return new AccountingRecognizedLoadAndUseCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedPaymentEntity:
					return new AccountingRecognizedPaymentCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedTapEntity:
					return new AccountingRecognizedTapCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCancellationEntity:
					return new AccountingRecognizedTapCancellationCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCmlSettledEntity:
					return new AccountingRecognizedTapCmlSettledCollection();
				case VarioSL.Entities.EntityType.AccountingRecognizedTapTabSettledEntity:
					return new AccountingRecognizedTapTabSettledCollection();
				case VarioSL.Entities.EntityType.AccountingReconciledPaymentEntity:
					return new AccountingReconciledPaymentCollection();
				case VarioSL.Entities.EntityType.AccountMessageSettingEntity:
					return new AccountMessageSettingCollection();
				case VarioSL.Entities.EntityType.AdditionalDataEntity:
					return new AdditionalDataCollection();
				case VarioSL.Entities.EntityType.AddressEntity:
					return new AddressCollection();
				case VarioSL.Entities.EntityType.AddressBookEntryEntity:
					return new AddressBookEntryCollection();
				case VarioSL.Entities.EntityType.AddressBookEntryTypeEntity:
					return new AddressBookEntryTypeCollection();
				case VarioSL.Entities.EntityType.AddressTypeEntity:
					return new AddressTypeCollection();
				case VarioSL.Entities.EntityType.AggregationFunctionEntity:
					return new AggregationFunctionCollection();
				case VarioSL.Entities.EntityType.AggregationTypeEntity:
					return new AggregationTypeCollection();
				case VarioSL.Entities.EntityType.ApportionEntity:
					return new ApportionCollection();
				case VarioSL.Entities.EntityType.ApportionPassRevenueRecognitionEntity:
					return new ApportionPassRevenueRecognitionCollection();
				case VarioSL.Entities.EntityType.ArchiveStateEntity:
					return new ArchiveStateCollection();
				case VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity:
					return new AttributeToMobilityProviderCollection();
				case VarioSL.Entities.EntityType.AuditRegisterEntity:
					return new AuditRegisterCollection();
				case VarioSL.Entities.EntityType.AuditRegisterValueEntity:
					return new AuditRegisterValueCollection();
				case VarioSL.Entities.EntityType.AuditRegisterValueTypeEntity:
					return new AuditRegisterValueTypeCollection();
				case VarioSL.Entities.EntityType.AutoloadSettingEntity:
					return new AutoloadSettingCollection();
				case VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity:
					return new AutoloadToPaymentOptionCollection();
				case VarioSL.Entities.EntityType.BadCardEntity:
					return new BadCardCollection();
				case VarioSL.Entities.EntityType.BadCheckEntity:
					return new BadCheckCollection();
				case VarioSL.Entities.EntityType.BankConnectionDataEntity:
					return new BankConnectionDataCollection();
				case VarioSL.Entities.EntityType.BankStatementEntity:
					return new BankStatementCollection();
				case VarioSL.Entities.EntityType.BankStatementRecordTypeEntity:
					return new BankStatementRecordTypeCollection();
				case VarioSL.Entities.EntityType.BankStatementStateEntity:
					return new BankStatementStateCollection();
				case VarioSL.Entities.EntityType.BankStatementVerificationEntity:
					return new BankStatementVerificationCollection();
				case VarioSL.Entities.EntityType.BookingEntity:
					return new BookingCollection();
				case VarioSL.Entities.EntityType.BookingItemEntity:
					return new BookingItemCollection();
				case VarioSL.Entities.EntityType.BookingItemPaymentEntity:
					return new BookingItemPaymentCollection();
				case VarioSL.Entities.EntityType.BookingItemPaymentTypeEntity:
					return new BookingItemPaymentTypeCollection();
				case VarioSL.Entities.EntityType.BookingTypeEntity:
					return new BookingTypeCollection();
				case VarioSL.Entities.EntityType.CappingJournalEntity:
					return new CappingJournalCollection();
				case VarioSL.Entities.EntityType.CappingStateEntity:
					return new CappingStateCollection();
				case VarioSL.Entities.EntityType.CardEntity:
					return new CardCollection();
				case VarioSL.Entities.EntityType.CardBodyTypeEntity:
					return new CardBodyTypeCollection();
				case VarioSL.Entities.EntityType.CardDormancyEntity:
					return new CardDormancyCollection();
				case VarioSL.Entities.EntityType.CardEventEntity:
					return new CardEventCollection();
				case VarioSL.Entities.EntityType.CardEventTypeEntity:
					return new CardEventTypeCollection();
				case VarioSL.Entities.EntityType.CardFulfillmentStatusEntity:
					return new CardFulfillmentStatusCollection();
				case VarioSL.Entities.EntityType.CardHolderEntity:
					return new CardHolderCollection();
				case VarioSL.Entities.EntityType.CardHolderOrganizationEntity:
					return new CardHolderOrganizationCollection();
				case VarioSL.Entities.EntityType.CardKeyEntity:
					return new CardKeyCollection();
				case VarioSL.Entities.EntityType.CardLiabilityEntity:
					return new CardLiabilityCollection();
				case VarioSL.Entities.EntityType.CardLinkEntity:
					return new CardLinkCollection();
				case VarioSL.Entities.EntityType.CardOrderDetailEntity:
					return new CardOrderDetailCollection();
				case VarioSL.Entities.EntityType.CardPhysicalDetailEntity:
					return new CardPhysicalDetailCollection();
				case VarioSL.Entities.EntityType.CardPrintingTypeEntity:
					return new CardPrintingTypeCollection();
				case VarioSL.Entities.EntityType.CardStockTransferEntity:
					return new CardStockTransferCollection();
				case VarioSL.Entities.EntityType.CardToContractEntity:
					return new CardToContractCollection();
				case VarioSL.Entities.EntityType.CardToRuleViolationEntity:
					return new CardToRuleViolationCollection();
				case VarioSL.Entities.EntityType.CardWorkItemEntity:
					return new CardWorkItemCollection();
				case VarioSL.Entities.EntityType.CertificateEntity:
					return new CertificateCollection();
				case VarioSL.Entities.EntityType.CertificateFormatEntity:
					return new CertificateFormatCollection();
				case VarioSL.Entities.EntityType.CertificatePurposeEntity:
					return new CertificatePurposeCollection();
				case VarioSL.Entities.EntityType.CertificateTypeEntity:
					return new CertificateTypeCollection();
				case VarioSL.Entities.EntityType.ClaimEntity:
					return new ClaimCollection();
				case VarioSL.Entities.EntityType.ClientFilterTypeEntity:
					return new ClientFilterTypeCollection();
				case VarioSL.Entities.EntityType.CloseoutPeriodEntity:
					return new CloseoutPeriodCollection();
				case VarioSL.Entities.EntityType.CloseoutStateEntity:
					return new CloseoutStateCollection();
				case VarioSL.Entities.EntityType.CloseoutTypeEntity:
					return new CloseoutTypeCollection();
				case VarioSL.Entities.EntityType.CommentEntity:
					return new CommentCollection();
				case VarioSL.Entities.EntityType.CommentPriorityEntity:
					return new CommentPriorityCollection();
				case VarioSL.Entities.EntityType.CommentTypeEntity:
					return new CommentTypeCollection();
				case VarioSL.Entities.EntityType.ConfigurationEntity:
					return new ConfigurationCollection();
				case VarioSL.Entities.EntityType.ConfigurationDataTypeEntity:
					return new ConfigurationDataTypeCollection();
				case VarioSL.Entities.EntityType.ConfigurationDefinitionEntity:
					return new ConfigurationDefinitionCollection();
				case VarioSL.Entities.EntityType.ConfigurationOverwriteEntity:
					return new ConfigurationOverwriteCollection();
				case VarioSL.Entities.EntityType.ContentItemEntity:
					return new ContentItemCollection();
				case VarioSL.Entities.EntityType.ContentTypeEntity:
					return new ContentTypeCollection();
				case VarioSL.Entities.EntityType.ContractEntity:
					return new ContractCollection();
				case VarioSL.Entities.EntityType.ContractAddressEntity:
					return new ContractAddressCollection();
				case VarioSL.Entities.EntityType.ContractDetailsEntity:
					return new ContractDetailsCollection();
				case VarioSL.Entities.EntityType.ContractHistoryEntity:
					return new ContractHistoryCollection();
				case VarioSL.Entities.EntityType.ContractLinkEntity:
					return new ContractLinkCollection();
				case VarioSL.Entities.EntityType.ContractStateEntity:
					return new ContractStateCollection();
				case VarioSL.Entities.EntityType.ContractTerminationEntity:
					return new ContractTerminationCollection();
				case VarioSL.Entities.EntityType.ContractTerminationTypeEntity:
					return new ContractTerminationTypeCollection();
				case VarioSL.Entities.EntityType.ContractToPaymentMethodEntity:
					return new ContractToPaymentMethodCollection();
				case VarioSL.Entities.EntityType.ContractToProviderEntity:
					return new ContractToProviderCollection();
				case VarioSL.Entities.EntityType.ContractTypeEntity:
					return new ContractTypeCollection();
				case VarioSL.Entities.EntityType.ContractWorkItemEntity:
					return new ContractWorkItemCollection();
				case VarioSL.Entities.EntityType.CoordinateEntity:
					return new CoordinateCollection();
				case VarioSL.Entities.EntityType.CountryEntity:
					return new CountryCollection();
				case VarioSL.Entities.EntityType.CreditCardAuthorizationEntity:
					return new CreditCardAuthorizationCollection();
				case VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity:
					return new CreditCardAuthorizationLogCollection();
				case VarioSL.Entities.EntityType.CreditCardMerchantEntity:
					return new CreditCardMerchantCollection();
				case VarioSL.Entities.EntityType.CreditCardNetworkEntity:
					return new CreditCardNetworkCollection();
				case VarioSL.Entities.EntityType.CreditCardTerminalEntity:
					return new CreditCardTerminalCollection();
				case VarioSL.Entities.EntityType.CryptoCertificateEntity:
					return new CryptoCertificateCollection();
				case VarioSL.Entities.EntityType.CryptoContentEntity:
					return new CryptoContentCollection();
				case VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity:
					return new CryptoCryptogramArchiveCollection();
				case VarioSL.Entities.EntityType.CryptogramCertificateEntity:
					return new CryptogramCertificateCollection();
				case VarioSL.Entities.EntityType.CryptogramCertificateTypeEntity:
					return new CryptogramCertificateTypeCollection();
				case VarioSL.Entities.EntityType.CryptogramErrorTypeEntity:
					return new CryptogramErrorTypeCollection();
				case VarioSL.Entities.EntityType.CryptogramLoadStatusEntity:
					return new CryptogramLoadStatusCollection();
				case VarioSL.Entities.EntityType.CryptoOperatorActivationkeyEntity:
					return new CryptoOperatorActivationkeyCollection();
				case VarioSL.Entities.EntityType.CryptoQArchiveEntity:
					return new CryptoQArchiveCollection();
				case VarioSL.Entities.EntityType.CryptoResourceEntity:
					return new CryptoResourceCollection();
				case VarioSL.Entities.EntityType.CryptoResourceDataEntity:
					return new CryptoResourceDataCollection();
				case VarioSL.Entities.EntityType.CryptoSamSignCertificateEntity:
					return new CryptoSamSignCertificateCollection();
				case VarioSL.Entities.EntityType.CryptoUicCertificateEntity:
					return new CryptoUicCertificateCollection();
				case VarioSL.Entities.EntityType.CustomAttributeEntity:
					return new CustomAttributeCollection();
				case VarioSL.Entities.EntityType.CustomAttributeValueEntity:
					return new CustomAttributeValueCollection();
				case VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity:
					return new CustomAttributeValueToPersonCollection();
				case VarioSL.Entities.EntityType.CustomerAccountEntity:
					return new CustomerAccountCollection();
				case VarioSL.Entities.EntityType.CustomerAccountNotificationEntity:
					return new CustomerAccountNotificationCollection();
				case VarioSL.Entities.EntityType.CustomerAccountRoleEntity:
					return new CustomerAccountRoleCollection();
				case VarioSL.Entities.EntityType.CustomerAccountStateEntity:
					return new CustomerAccountStateCollection();
				case VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity:
					return new CustomerAccountToVerificationAttemptTypeCollection();
				case VarioSL.Entities.EntityType.CustomerLanguageEntity:
					return new CustomerLanguageCollection();
				case VarioSL.Entities.EntityType.DeviceCommunicationHistoryEntity:
					return new DeviceCommunicationHistoryCollection();
				case VarioSL.Entities.EntityType.DeviceReaderEntity:
					return new DeviceReaderCollection();
				case VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity:
					return new DeviceReaderCryptoResourceCollection();
				case VarioSL.Entities.EntityType.DeviceReaderJobEntity:
					return new DeviceReaderJobCollection();
				case VarioSL.Entities.EntityType.DeviceReaderKeyEntity:
					return new DeviceReaderKeyCollection();
				case VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity:
					return new DeviceReaderKeyToCertCollection();
				case VarioSL.Entities.EntityType.DeviceReaderToCertEntity:
					return new DeviceReaderToCertCollection();
				case VarioSL.Entities.EntityType.DeviceTokenEntity:
					return new DeviceTokenCollection();
				case VarioSL.Entities.EntityType.DeviceTokenStateEntity:
					return new DeviceTokenStateCollection();
				case VarioSL.Entities.EntityType.DocumentHistoryEntity:
					return new DocumentHistoryCollection();
				case VarioSL.Entities.EntityType.DocumentTypeEntity:
					return new DocumentTypeCollection();
				case VarioSL.Entities.EntityType.DormancyNotificationEntity:
					return new DormancyNotificationCollection();
				case VarioSL.Entities.EntityType.DunningActionEntity:
					return new DunningActionCollection();
				case VarioSL.Entities.EntityType.DunningLevelConfigurationEntity:
					return new DunningLevelConfigurationCollection();
				case VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity:
					return new DunningLevelPostingKeyCollection();
				case VarioSL.Entities.EntityType.DunningProcessEntity:
					return new DunningProcessCollection();
				case VarioSL.Entities.EntityType.DunningToInvoiceEntity:
					return new DunningToInvoiceCollection();
				case VarioSL.Entities.EntityType.DunningToPostingEntity:
					return new DunningToPostingCollection();
				case VarioSL.Entities.EntityType.DunningToProductEntity:
					return new DunningToProductCollection();
				case VarioSL.Entities.EntityType.EmailEventEntity:
					return new EmailEventCollection();
				case VarioSL.Entities.EntityType.EmailEventResendLockEntity:
					return new EmailEventResendLockCollection();
				case VarioSL.Entities.EntityType.EmailMessageEntity:
					return new EmailMessageCollection();
				case VarioSL.Entities.EntityType.EntitlementEntity:
					return new EntitlementCollection();
				case VarioSL.Entities.EntityType.EntitlementToProductEntity:
					return new EntitlementToProductCollection();
				case VarioSL.Entities.EntityType.EntitlementTypeEntity:
					return new EntitlementTypeCollection();
				case VarioSL.Entities.EntityType.EventSettingEntity:
					return new EventSettingCollection();
				case VarioSL.Entities.EntityType.FareChangeCauseEntity:
					return new FareChangeCauseCollection();
				case VarioSL.Entities.EntityType.FareEvasionBehaviourEntity:
					return new FareEvasionBehaviourCollection();
				case VarioSL.Entities.EntityType.FareEvasionIncidentEntity:
					return new FareEvasionIncidentCollection();
				case VarioSL.Entities.EntityType.FareEvasionInspectionEntity:
					return new FareEvasionInspectionCollection();
				case VarioSL.Entities.EntityType.FareEvasionStateEntity:
					return new FareEvasionStateCollection();
				case VarioSL.Entities.EntityType.FarePaymentErrorEntity:
					return new FarePaymentErrorCollection();
				case VarioSL.Entities.EntityType.FarePaymentResultTypeEntity:
					return new FarePaymentResultTypeCollection();
				case VarioSL.Entities.EntityType.FastcardEntity:
					return new FastcardCollection();
				case VarioSL.Entities.EntityType.FilterCategoryEntity:
					return new FilterCategoryCollection();
				case VarioSL.Entities.EntityType.FilterElementEntity:
					return new FilterElementCollection();
				case VarioSL.Entities.EntityType.FilterListEntity:
					return new FilterListCollection();
				case VarioSL.Entities.EntityType.FilterListElementEntity:
					return new FilterListElementCollection();
				case VarioSL.Entities.EntityType.FilterTypeEntity:
					return new FilterTypeCollection();
				case VarioSL.Entities.EntityType.FilterValueEntity:
					return new FilterValueCollection();
				case VarioSL.Entities.EntityType.FilterValueSetEntity:
					return new FilterValueSetCollection();
				case VarioSL.Entities.EntityType.FlexValueEntity:
					return new FlexValueCollection();
				case VarioSL.Entities.EntityType.FormEntity:
					return new FormCollection();
				case VarioSL.Entities.EntityType.FormTemplateEntity:
					return new FormTemplateCollection();
				case VarioSL.Entities.EntityType.FrequencyEntity:
					return new FrequencyCollection();
				case VarioSL.Entities.EntityType.GenderEntity:
					return new GenderCollection();
				case VarioSL.Entities.EntityType.IdentificationTypeEntity:
					return new IdentificationTypeCollection();
				case VarioSL.Entities.EntityType.InspectionCriterionEntity:
					return new InspectionCriterionCollection();
				case VarioSL.Entities.EntityType.InspectionReportEntity:
					return new InspectionReportCollection();
				case VarioSL.Entities.EntityType.InspectionResultEntity:
					return new InspectionResultCollection();
				case VarioSL.Entities.EntityType.InventoryStateEntity:
					return new InventoryStateCollection();
				case VarioSL.Entities.EntityType.InvoiceEntity:
					return new InvoiceCollection();
				case VarioSL.Entities.EntityType.InvoiceEntryEntity:
					return new InvoiceEntryCollection();
				case VarioSL.Entities.EntityType.InvoiceTypeEntity:
					return new InvoiceTypeCollection();
				case VarioSL.Entities.EntityType.InvoicingEntity:
					return new InvoicingCollection();
				case VarioSL.Entities.EntityType.InvoicingFilterTypeEntity:
					return new InvoicingFilterTypeCollection();
				case VarioSL.Entities.EntityType.InvoicingTypeEntity:
					return new InvoicingTypeCollection();
				case VarioSL.Entities.EntityType.JobEntity:
					return new JobCollection();
				case VarioSL.Entities.EntityType.JobBulkImportEntity:
					return new JobBulkImportCollection();
				case VarioSL.Entities.EntityType.JobCardImportEntity:
					return new JobCardImportCollection();
				case VarioSL.Entities.EntityType.JobFileTypeEntity:
					return new JobFileTypeCollection();
				case VarioSL.Entities.EntityType.JobStateEntity:
					return new JobStateCollection();
				case VarioSL.Entities.EntityType.JobTypeEntity:
					return new JobTypeCollection();
				case VarioSL.Entities.EntityType.LookupAddressEntity:
					return new LookupAddressCollection();
				case VarioSL.Entities.EntityType.MediaEligibilityRequirementEntity:
					return new MediaEligibilityRequirementCollection();
				case VarioSL.Entities.EntityType.MediaSalePreconditionEntity:
					return new MediaSalePreconditionCollection();
				case VarioSL.Entities.EntityType.MerchantEntity:
					return new MerchantCollection();
				case VarioSL.Entities.EntityType.MessageEntity:
					return new MessageCollection();
				case VarioSL.Entities.EntityType.MessageFormatEntity:
					return new MessageFormatCollection();
				case VarioSL.Entities.EntityType.MessageTypeEntity:
					return new MessageTypeCollection();
				case VarioSL.Entities.EntityType.MobilityProviderEntity:
					return new MobilityProviderCollection();
				case VarioSL.Entities.EntityType.NotificationDeviceEntity:
					return new NotificationDeviceCollection();
				case VarioSL.Entities.EntityType.NotificationMessageEntity:
					return new NotificationMessageCollection();
				case VarioSL.Entities.EntityType.NotificationMessageOwnerEntity:
					return new NotificationMessageOwnerCollection();
				case VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity:
					return new NotificationMessageToCustomerCollection();
				case VarioSL.Entities.EntityType.NumberGroupEntity:
					return new NumberGroupCollection();
				case VarioSL.Entities.EntityType.NumberGroupRandomizedEntity:
					return new NumberGroupRandomizedCollection();
				case VarioSL.Entities.EntityType.NumberGroupScopeEntity:
					return new NumberGroupScopeCollection();
				case VarioSL.Entities.EntityType.OrderEntity:
					return new OrderCollection();
				case VarioSL.Entities.EntityType.OrderDetailEntity:
					return new OrderDetailCollection();
				case VarioSL.Entities.EntityType.OrderDetailToCardEntity:
					return new OrderDetailToCardCollection();
				case VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity:
					return new OrderDetailToCardHolderCollection();
				case VarioSL.Entities.EntityType.OrderHistoryEntity:
					return new OrderHistoryCollection();
				case VarioSL.Entities.EntityType.OrderProcessingCleanupEntity:
					return new OrderProcessingCleanupCollection();
				case VarioSL.Entities.EntityType.OrderStateEntity:
					return new OrderStateCollection();
				case VarioSL.Entities.EntityType.OrderTypeEntity:
					return new OrderTypeCollection();
				case VarioSL.Entities.EntityType.OrderWithTotalEntity:
					return new OrderWithTotalCollection();
				case VarioSL.Entities.EntityType.OrderWorkItemEntity:
					return new OrderWorkItemCollection();
				case VarioSL.Entities.EntityType.OrganizationEntity:
					return new OrganizationCollection();
				case VarioSL.Entities.EntityType.OrganizationAddressEntity:
					return new OrganizationAddressCollection();
				case VarioSL.Entities.EntityType.OrganizationParticipantEntity:
					return new OrganizationParticipantCollection();
				case VarioSL.Entities.EntityType.OrganizationSubtypeEntity:
					return new OrganizationSubtypeCollection();
				case VarioSL.Entities.EntityType.OrganizationTypeEntity:
					return new OrganizationTypeCollection();
				case VarioSL.Entities.EntityType.PageContentEntity:
					return new PageContentCollection();
				case VarioSL.Entities.EntityType.ParameterEntity:
					return new ParameterCollection();
				case VarioSL.Entities.EntityType.ParameterArchiveEntity:
					return new ParameterArchiveCollection();
				case VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity:
					return new ParameterArchiveReleaseCollection();
				case VarioSL.Entities.EntityType.ParameterArchiveResultViewEntity:
					return new ParameterArchiveResultViewCollection();
				case VarioSL.Entities.EntityType.ParameterGroupEntity:
					return new ParameterGroupCollection();
				case VarioSL.Entities.EntityType.ParameterGroupToParameterEntity:
					return new ParameterGroupToParameterCollection();
				case VarioSL.Entities.EntityType.ParameterTypeEntity:
					return new ParameterTypeCollection();
				case VarioSL.Entities.EntityType.ParameterValueEntity:
					return new ParameterValueCollection();
				case VarioSL.Entities.EntityType.ParaTransAttributeEntity:
					return new ParaTransAttributeCollection();
				case VarioSL.Entities.EntityType.ParaTransAttributeTypeEntity:
					return new ParaTransAttributeTypeCollection();
				case VarioSL.Entities.EntityType.ParaTransJournalEntity:
					return new ParaTransJournalCollection();
				case VarioSL.Entities.EntityType.ParaTransJournalTypeEntity:
					return new ParaTransJournalTypeCollection();
				case VarioSL.Entities.EntityType.ParaTransPaymentTypeEntity:
					return new ParaTransPaymentTypeCollection();
				case VarioSL.Entities.EntityType.ParticipantGroupEntity:
					return new ParticipantGroupCollection();
				case VarioSL.Entities.EntityType.PassExpiryNotificationEntity:
					return new PassExpiryNotificationCollection();
				case VarioSL.Entities.EntityType.PaymentEntity:
					return new PaymentCollection();
				case VarioSL.Entities.EntityType.PaymentJournalEntity:
					return new PaymentJournalCollection();
				case VarioSL.Entities.EntityType.PaymentJournalToComponentEntity:
					return new PaymentJournalToComponentCollection();
				case VarioSL.Entities.EntityType.PaymentMethodEntity:
					return new PaymentMethodCollection();
				case VarioSL.Entities.EntityType.PaymentModalityEntity:
					return new PaymentModalityCollection();
				case VarioSL.Entities.EntityType.PaymentOptionEntity:
					return new PaymentOptionCollection();
				case VarioSL.Entities.EntityType.PaymentProviderEntity:
					return new PaymentProviderCollection();
				case VarioSL.Entities.EntityType.PaymentProviderAccountEntity:
					return new PaymentProviderAccountCollection();
				case VarioSL.Entities.EntityType.PaymentRecognitionEntity:
					return new PaymentRecognitionCollection();
				case VarioSL.Entities.EntityType.PaymentReconciliationEntity:
					return new PaymentReconciliationCollection();
				case VarioSL.Entities.EntityType.PaymentReconciliationStateEntity:
					return new PaymentReconciliationStateCollection();
				case VarioSL.Entities.EntityType.PaymentStateEntity:
					return new PaymentStateCollection();
				case VarioSL.Entities.EntityType.PendingOrderEntity:
					return new PendingOrderCollection();
				case VarioSL.Entities.EntityType.PerformanceEntity:
					return new PerformanceCollection();
				case VarioSL.Entities.EntityType.PerformanceAggregationEntity:
					return new PerformanceAggregationCollection();
				case VarioSL.Entities.EntityType.PerformanceComponentEntity:
					return new PerformanceComponentCollection();
				case VarioSL.Entities.EntityType.PerformanceIndicatorEntity:
					return new PerformanceIndicatorCollection();
				case VarioSL.Entities.EntityType.PersonEntity:
					return new PersonCollection();
				case VarioSL.Entities.EntityType.PersonAddressEntity:
					return new PersonAddressCollection();
				case VarioSL.Entities.EntityType.PhotographEntity:
					return new PhotographCollection();
				case VarioSL.Entities.EntityType.PickupLocationEntity:
					return new PickupLocationCollection();
				case VarioSL.Entities.EntityType.PickupLocationToTicketEntity:
					return new PickupLocationToTicketCollection();
				case VarioSL.Entities.EntityType.PostingEntity:
					return new PostingCollection();
				case VarioSL.Entities.EntityType.PostingKeyEntity:
					return new PostingKeyCollection();
				case VarioSL.Entities.EntityType.PostingKeyCategoryEntity:
					return new PostingKeyCategoryCollection();
				case VarioSL.Entities.EntityType.PostingScopeEntity:
					return new PostingScopeCollection();
				case VarioSL.Entities.EntityType.PriceAdjustmentEntity:
					return new PriceAdjustmentCollection();
				case VarioSL.Entities.EntityType.PriceAdjustmentTemplateEntity:
					return new PriceAdjustmentTemplateCollection();
				case VarioSL.Entities.EntityType.PrinterEntity:
					return new PrinterCollection();
				case VarioSL.Entities.EntityType.PrinterStateEntity:
					return new PrinterStateCollection();
				case VarioSL.Entities.EntityType.PrinterToUnitEntity:
					return new PrinterToUnitCollection();
				case VarioSL.Entities.EntityType.PrinterTypeEntity:
					return new PrinterTypeCollection();
				case VarioSL.Entities.EntityType.PriorityEntity:
					return new PriorityCollection();
				case VarioSL.Entities.EntityType.ProductEntity:
					return new ProductCollection();
				case VarioSL.Entities.EntityType.ProductRelationEntity:
					return new ProductRelationCollection();
				case VarioSL.Entities.EntityType.ProductSubsidyEntity:
					return new ProductSubsidyCollection();
				case VarioSL.Entities.EntityType.ProductTerminationEntity:
					return new ProductTerminationCollection();
				case VarioSL.Entities.EntityType.ProductTerminationTypeEntity:
					return new ProductTerminationTypeCollection();
				case VarioSL.Entities.EntityType.ProductTypeEntity:
					return new ProductTypeCollection();
				case VarioSL.Entities.EntityType.PromoCodeEntity:
					return new PromoCodeCollection();
				case VarioSL.Entities.EntityType.PromoCodeStatusEntity:
					return new PromoCodeStatusCollection();
				case VarioSL.Entities.EntityType.ProviderAccountStateEntity:
					return new ProviderAccountStateCollection();
				case VarioSL.Entities.EntityType.ProvisioningStateEntity:
					return new ProvisioningStateCollection();
				case VarioSL.Entities.EntityType.RecipientGroupEntity:
					return new RecipientGroupCollection();
				case VarioSL.Entities.EntityType.RecipientGroupMemberEntity:
					return new RecipientGroupMemberCollection();
				case VarioSL.Entities.EntityType.RefundEntity:
					return new RefundCollection();
				case VarioSL.Entities.EntityType.RefundReasonEntity:
					return new RefundReasonCollection();
				case VarioSL.Entities.EntityType.RentalStateEntity:
					return new RentalStateCollection();
				case VarioSL.Entities.EntityType.RentalTypeEntity:
					return new RentalTypeCollection();
				case VarioSL.Entities.EntityType.ReportEntity:
					return new ReportCollection();
				case VarioSL.Entities.EntityType.ReportCategoryEntity:
					return new ReportCategoryCollection();
				case VarioSL.Entities.EntityType.ReportDataFileEntity:
					return new ReportDataFileCollection();
				case VarioSL.Entities.EntityType.ReportJobEntity:
					return new ReportJobCollection();
				case VarioSL.Entities.EntityType.ReportJobResultEntity:
					return new ReportJobResultCollection();
				case VarioSL.Entities.EntityType.ReportToClientEntity:
					return new ReportToClientCollection();
				case VarioSL.Entities.EntityType.RevenueRecognitionEntity:
					return new RevenueRecognitionCollection();
				case VarioSL.Entities.EntityType.RevenueSettlementEntity:
					return new RevenueSettlementCollection();
				case VarioSL.Entities.EntityType.RuleViolationEntity:
					return new RuleViolationCollection();
				case VarioSL.Entities.EntityType.RuleViolationProviderEntity:
					return new RuleViolationProviderCollection();
				case VarioSL.Entities.EntityType.RuleViolationSourceTypeEntity:
					return new RuleViolationSourceTypeCollection();
				case VarioSL.Entities.EntityType.RuleViolationToTransactionEntity:
					return new RuleViolationToTransactionCollection();
				case VarioSL.Entities.EntityType.RuleViolationTypeEntity:
					return new RuleViolationTypeCollection();
				case VarioSL.Entities.EntityType.SaleEntity:
					return new SaleCollection();
				case VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity:
					return new SalesChannelToPaymentMethodCollection();
				case VarioSL.Entities.EntityType.SalesRevenueEntity:
					return new SalesRevenueCollection();
				case VarioSL.Entities.EntityType.SaleToComponentEntity:
					return new SaleToComponentCollection();
				case VarioSL.Entities.EntityType.SaleTypeEntity:
					return new SaleTypeCollection();
				case VarioSL.Entities.EntityType.SamModuleEntity:
					return new SamModuleCollection();
				case VarioSL.Entities.EntityType.SamModuleStatusEntity:
					return new SamModuleStatusCollection();
				case VarioSL.Entities.EntityType.SchoolYearEntity:
					return new SchoolYearCollection();
				case VarioSL.Entities.EntityType.SecurityQuestionEntity:
					return new SecurityQuestionCollection();
				case VarioSL.Entities.EntityType.SecurityResponseEntity:
					return new SecurityResponseCollection();
				case VarioSL.Entities.EntityType.SepaFrequencyTypeEntity:
					return new SepaFrequencyTypeCollection();
				case VarioSL.Entities.EntityType.SepaOwnerTypeEntity:
					return new SepaOwnerTypeCollection();
				case VarioSL.Entities.EntityType.ServerResponseTimeDataEntity:
					return new ServerResponseTimeDataCollection();
				case VarioSL.Entities.EntityType.ServerResponseTimeDataAggregationEntity:
					return new ServerResponseTimeDataAggregationCollection();
				case VarioSL.Entities.EntityType.SettledRevenueEntity:
					return new SettledRevenueCollection();
				case VarioSL.Entities.EntityType.SettlementAccountEntity:
					return new SettlementAccountCollection();
				case VarioSL.Entities.EntityType.SettlementCalendarEntity:
					return new SettlementCalendarCollection();
				case VarioSL.Entities.EntityType.SettlementDistributionPolicyEntity:
					return new SettlementDistributionPolicyCollection();
				case VarioSL.Entities.EntityType.SettlementHolidayEntity:
					return new SettlementHolidayCollection();
				case VarioSL.Entities.EntityType.SettlementOperationEntity:
					return new SettlementOperationCollection();
				case VarioSL.Entities.EntityType.SettlementQuerySettingEntity:
					return new SettlementQuerySettingCollection();
				case VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity:
					return new SettlementQuerySettingToTicketCollection();
				case VarioSL.Entities.EntityType.SettlementQueryValueEntity:
					return new SettlementQueryValueCollection();
				case VarioSL.Entities.EntityType.SettlementReleaseStateEntity:
					return new SettlementReleaseStateCollection();
				case VarioSL.Entities.EntityType.SettlementResultEntity:
					return new SettlementResultCollection();
				case VarioSL.Entities.EntityType.SettlementRunEntity:
					return new SettlementRunCollection();
				case VarioSL.Entities.EntityType.SettlementRunValueEntity:
					return new SettlementRunValueCollection();
				case VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity:
					return new SettlementRunValueToVarioSettlementCollection();
				case VarioSL.Entities.EntityType.SettlementSetupEntity:
					return new SettlementSetupCollection();
				case VarioSL.Entities.EntityType.SettlementTypeEntity:
					return new SettlementTypeCollection();
				case VarioSL.Entities.EntityType.SourceEntity:
					return new SourceCollection();
				case VarioSL.Entities.EntityType.StatementOfAccountEntity:
					return new StatementOfAccountCollection();
				case VarioSL.Entities.EntityType.StateOfCountryEntity:
					return new StateOfCountryCollection();
				case VarioSL.Entities.EntityType.StockTransferEntity:
					return new StockTransferCollection();
				case VarioSL.Entities.EntityType.StorageLocationEntity:
					return new StorageLocationCollection();
				case VarioSL.Entities.EntityType.SubsidyLimitEntity:
					return new SubsidyLimitCollection();
				case VarioSL.Entities.EntityType.SubsidyTransactionEntity:
					return new SubsidyTransactionCollection();
				case VarioSL.Entities.EntityType.SurveyEntity:
					return new SurveyCollection();
				case VarioSL.Entities.EntityType.SurveyAnswerEntity:
					return new SurveyAnswerCollection();
				case VarioSL.Entities.EntityType.SurveyChoiceEntity:
					return new SurveyChoiceCollection();
				case VarioSL.Entities.EntityType.SurveyDescriptionEntity:
					return new SurveyDescriptionCollection();
				case VarioSL.Entities.EntityType.SurveyQuestionEntity:
					return new SurveyQuestionCollection();
				case VarioSL.Entities.EntityType.SurveyResponseEntity:
					return new SurveyResponseCollection();
				case VarioSL.Entities.EntityType.TenantEntity:
					return new TenantCollection();
				case VarioSL.Entities.EntityType.TenantHistoryEntity:
					return new TenantHistoryCollection();
				case VarioSL.Entities.EntityType.TenantPersonEntity:
					return new TenantPersonCollection();
				case VarioSL.Entities.EntityType.TenantStateEntity:
					return new TenantStateCollection();
				case VarioSL.Entities.EntityType.TerminationStatusEntity:
					return new TerminationStatusCollection();
				case VarioSL.Entities.EntityType.TicketAssignmentEntity:
					return new TicketAssignmentCollection();
				case VarioSL.Entities.EntityType.TicketAssignmentStatusEntity:
					return new TicketAssignmentStatusCollection();
				case VarioSL.Entities.EntityType.TicketSerialNumberEntity:
					return new TicketSerialNumberCollection();
				case VarioSL.Entities.EntityType.TicketSerialNumberStatusEntity:
					return new TicketSerialNumberStatusCollection();
				case VarioSL.Entities.EntityType.TransactionJournalEntity:
					return new TransactionJournalCollection();
				case VarioSL.Entities.EntityType.TransactionJournalCorrectionEntity:
					return new TransactionJournalCorrectionCollection();
				case VarioSL.Entities.EntityType.TransactionJourneyHistoryEntity:
					return new TransactionJourneyHistoryCollection();
				case VarioSL.Entities.EntityType.TransactionToInspectionEntity:
					return new TransactionToInspectionCollection();
				case VarioSL.Entities.EntityType.TransactionTypeEntity:
					return new TransactionTypeCollection();
				case VarioSL.Entities.EntityType.TransportCompanyEntity:
					return new TransportCompanyCollection();
				case VarioSL.Entities.EntityType.TriggerTypeEntity:
					return new TriggerTypeCollection();
				case VarioSL.Entities.EntityType.UnitCollectionEntity:
					return new UnitCollectionCollection();
				case VarioSL.Entities.EntityType.UnitCollectionToUnitEntity:
					return new UnitCollectionToUnitCollection();
				case VarioSL.Entities.EntityType.ValidationResultEntity:
					return new ValidationResultCollection();
				case VarioSL.Entities.EntityType.ValidationResultResolveTypeEntity:
					return new ValidationResultResolveTypeCollection();
				case VarioSL.Entities.EntityType.ValidationRuleEntity:
					return new ValidationRuleCollection();
				case VarioSL.Entities.EntityType.ValidationRunEntity:
					return new ValidationRunCollection();
				case VarioSL.Entities.EntityType.ValidationRunRuleEntity:
					return new ValidationRunRuleCollection();
				case VarioSL.Entities.EntityType.ValidationStatusEntity:
					return new ValidationStatusCollection();
				case VarioSL.Entities.EntityType.ValidationValueEntity:
					return new ValidationValueCollection();
				case VarioSL.Entities.EntityType.VdvTransactionEntity:
					return new VdvTransactionCollection();
				case VarioSL.Entities.EntityType.VerificationAttemptEntity:
					return new VerificationAttemptCollection();
				case VarioSL.Entities.EntityType.VerificationAttemptTypeEntity:
					return new VerificationAttemptTypeCollection();
				case VarioSL.Entities.EntityType.VirtualCardEntity:
					return new VirtualCardCollection();
				case VarioSL.Entities.EntityType.VoucherEntity:
					return new VoucherCollection();
				case VarioSL.Entities.EntityType.WhitelistEntity:
					return new WhitelistCollection();
				case VarioSL.Entities.EntityType.WhitelistJournalEntity:
					return new WhitelistJournalCollection();
				case VarioSL.Entities.EntityType.WorkItemEntity:
					return new WorkItemCollection();
				case VarioSL.Entities.EntityType.WorkItemCategoryEntity:
					return new WorkItemCategoryCollection();
				case VarioSL.Entities.EntityType.WorkItemHistoryEntity:
					return new WorkItemHistoryCollection();
				case VarioSL.Entities.EntityType.WorkItemStateEntity:
					return new WorkItemStateCollection();
				case VarioSL.Entities.EntityType.WorkItemSubjectEntity:
					return new WorkItemSubjectCollection();
				case VarioSL.Entities.EntityType.WorkItemViewEntity:
					return new WorkItemViewCollection();
				default:
					return null;
			}
		}		
	}
	
	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity Create(VarioSL.Entities.EntityType entityTypeToCreate)
		{
			IEntityFactory factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case VarioSL.Entities.EntityType.AbortedTransactionEntity:
					factoryToUse = new AbortedTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountBalanceEntity:
					factoryToUse = new AccountBalanceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountEntryEntity:
					factoryToUse = new AccountEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountEntryNumberEntity:
					factoryToUse = new AccountEntryNumberEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountPostingKeyEntity:
					factoryToUse = new AccountPostingKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApplicationEntity:
					factoryToUse = new ApplicationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApplicationVersionEntity:
					factoryToUse = new ApplicationVersionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApportionmentEntity:
					factoryToUse = new ApportionmentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApportionmentResultEntity:
					factoryToUse = new ApportionmentResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AreaListEntity:
					factoryToUse = new AreaListEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AreaListElementEntity:
					factoryToUse = new AreaListElementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AreaListElementGroupEntity:
					factoryToUse = new AreaListElementGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AreaTypeEntity:
					factoryToUse = new AreaTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeEntity:
					factoryToUse = new AttributeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity:
					factoryToUse = new AttributeBitmapLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity:
					factoryToUse = new AttributeTextLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeValueEntity:
					factoryToUse = new AttributeValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeValueListEntity:
					factoryToUse = new AttributeValueListEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AutomatEntity:
					factoryToUse = new AutomatEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankEntity:
					factoryToUse = new BankEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BinaryDataEntity:
					factoryToUse = new BinaryDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BlockingReasonEntity:
					factoryToUse = new BlockingReasonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleEntity:
					factoryToUse = new BusinessRuleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleClauseEntity:
					factoryToUse = new BusinessRuleClauseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleConditionEntity:
					factoryToUse = new BusinessRuleConditionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleResultEntity:
					factoryToUse = new BusinessRuleResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleTypeEntity:
					factoryToUse = new BusinessRuleTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity:
					factoryToUse = new BusinessRuleTypeToVariableEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BusinessRuleVariableEntity:
					factoryToUse = new BusinessRuleVariableEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CalendarEntity:
					factoryToUse = new CalendarEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CalendarEntryEntity:
					factoryToUse = new CalendarEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardActionAttributeEntity:
					factoryToUse = new CardActionAttributeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardActionRequestEntity:
					factoryToUse = new CardActionRequestEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardActionRequestTypeEntity:
					factoryToUse = new CardActionRequestTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardChipTypeEntity:
					factoryToUse = new CardChipTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardStateEntity:
					factoryToUse = new CardStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardTicketEntity:
					factoryToUse = new CardTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardTicketToTicketEntity:
					factoryToUse = new CardTicketToTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CashServiceEntity:
					factoryToUse = new CashServiceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CashServiceBalanceEntity:
					factoryToUse = new CashServiceBalanceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CashServiceDataEntity:
					factoryToUse = new CashServiceDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CashUnitEntity:
					factoryToUse = new CashUnitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ChoiceEntity:
					factoryToUse = new ChoiceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingEntity:
					factoryToUse = new ClearingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingClassificationEntity:
					factoryToUse = new ClearingClassificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingDetailEntity:
					factoryToUse = new ClearingDetailEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingResultEntity:
					factoryToUse = new ClearingResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingResultLevelEntity:
					factoryToUse = new ClearingResultLevelEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingStateEntity:
					factoryToUse = new ClearingStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingSumEntity:
					factoryToUse = new ClearingSumEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClearingTransactionEntity:
					factoryToUse = new ClearingTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClientEntity:
					factoryToUse = new ClientEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity:
					factoryToUse = new ClientAdaptedLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ComponentEntity:
					factoryToUse = new ComponentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ComponentClearingEntity:
					factoryToUse = new ComponentClearingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ComponentFillingEntity:
					factoryToUse = new ComponentFillingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ComponentStateEntity:
					factoryToUse = new ComponentStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ComponentTypeEntity:
					factoryToUse = new ComponentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity:
					factoryToUse = new ConditionalSubPageLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditScreeningEntity:
					factoryToUse = new CreditScreeningEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DayTypeEntity:
					factoryToUse = new DayTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DebtorEntity:
					factoryToUse = new DebtorEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DebtorCardEntity:
					factoryToUse = new DebtorCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DebtorCardHistoryEntity:
					factoryToUse = new DebtorCardHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DefaultPinEntity:
					factoryToUse = new DefaultPinEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DepotEntity:
					factoryToUse = new DepotEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceEntity:
					factoryToUse = new DeviceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceBookingStateEntity:
					factoryToUse = new DeviceBookingStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceClassEntity:
					factoryToUse = new DeviceClassEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DevicePaymentMethodEntity:
					factoryToUse = new DevicePaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DirectionEntity:
					factoryToUse = new DirectionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningLevelEntity:
					factoryToUse = new DunningLevelEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EticketEntity:
					factoryToUse = new EticketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExportInfoEntity:
					factoryToUse = new ExportInfoEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalCardEntity:
					factoryToUse = new ExternalCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalDataEntity:
					factoryToUse = new ExternalDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalEffortEntity:
					factoryToUse = new ExternalEffortEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalPacketEntity:
					factoryToUse = new ExternalPacketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalPacketEffortEntity:
					factoryToUse = new ExternalPacketEffortEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ExternalTypeEntity:
					factoryToUse = new ExternalTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionCategoryEntity:
					factoryToUse = new FareEvasionCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionReasonEntity:
					factoryToUse = new FareEvasionReasonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity:
					factoryToUse = new FareEvasionReasonToCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareMatrixEntity:
					factoryToUse = new FareMatrixEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareMatrixEntryEntity:
					factoryToUse = new FareMatrixEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareStageEntity:
					factoryToUse = new FareStageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareStageAliasEntity:
					factoryToUse = new FareStageAliasEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareStageHierarchieLevelEntity:
					factoryToUse = new FareStageHierarchieLevelEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareStageListEntity:
					factoryToUse = new FareStageListEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareStageTypeEntity:
					factoryToUse = new FareStageTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareTableEntity:
					factoryToUse = new FareTableEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareTableEntryEntity:
					factoryToUse = new FareTableEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity:
					factoryToUse = new FixedBitmapLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity:
					factoryToUse = new FixedTextLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.GuiDefEntity:
					factoryToUse = new GuiDefEntityFactory();
					break;
				case VarioSL.Entities.EntityType.KeyAttributeTransfromEntity:
					factoryToUse = new KeyAttributeTransfromEntityFactory();
					break;
				case VarioSL.Entities.EntityType.KeySelectionModeEntity:
					factoryToUse = new KeySelectionModeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.KVVSubscriptionEntity:
					factoryToUse = new KVVSubscriptionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LanguageEntity:
					factoryToUse = new LanguageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LayoutEntity:
					factoryToUse = new LayoutEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LineEntity:
					factoryToUse = new LineEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LineGroupEntity:
					factoryToUse = new LineGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LineGroupToLineGroupEntity:
					factoryToUse = new LineGroupToLineGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LineToLineGroupEntity:
					factoryToUse = new LineToLineGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ListLayoutObjectEntity:
					factoryToUse = new ListLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ListLayoutTypeEntity:
					factoryToUse = new ListLayoutTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LogoEntity:
					factoryToUse = new LogoEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NetEntity:
					factoryToUse = new NetEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NumberRangeEntity:
					factoryToUse = new NumberRangeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OperatorEntity:
					factoryToUse = new OperatorEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OutputDeviceEntity:
					factoryToUse = new OutputDeviceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PageContentGroupEntity:
					factoryToUse = new PageContentGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PageContentGroupToContentEntity:
					factoryToUse = new PageContentGroupToContentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PanelEntity:
					factoryToUse = new PanelEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterAttributeValueEntity:
					factoryToUse = new ParameterAttributeValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterFareStageEntity:
					factoryToUse = new ParameterFareStageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterTariffEntity:
					factoryToUse = new ParameterTariffEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterTicketEntity:
					factoryToUse = new ParameterTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentDetailEntity:
					factoryToUse = new PaymentDetailEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentIntervalEntity:
					factoryToUse = new PaymentIntervalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PointOfSaleEntity:
					factoryToUse = new PointOfSaleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PredefinedKeyEntity:
					factoryToUse = new PredefinedKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PriceTypeEntity:
					factoryToUse = new PriceTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrimalKeyEntity:
					factoryToUse = new PrimalKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrintTextEntity:
					factoryToUse = new PrintTextEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductOnCardEntity:
					factoryToUse = new ProductOnCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProtocolEntity:
					factoryToUse = new ProtocolEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProtocolActionEntity:
					factoryToUse = new ProtocolActionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProtocolFunctionEntity:
					factoryToUse = new ProtocolFunctionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProtocolFunctionGroupEntity:
					factoryToUse = new ProtocolFunctionGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProtocolMessageEntity:
					factoryToUse = new ProtocolMessageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.QualificationEntity:
					factoryToUse = new QualificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ResponsibilityEntity:
					factoryToUse = new ResponsibilityEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RevenueTransactionTypeEntity:
					factoryToUse = new RevenueTransactionTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RmPaymentEntity:
					factoryToUse = new RmPaymentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RouteEntity:
					factoryToUse = new RouteEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RouteNameEntity:
					factoryToUse = new RouteNameEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleCappingEntity:
					factoryToUse = new RuleCappingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity:
					factoryToUse = new RuleCappingToFtEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleCappingToTicketEntity:
					factoryToUse = new RuleCappingToTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RulePeriodEntity:
					factoryToUse = new RulePeriodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleTypeEntity:
					factoryToUse = new RuleTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SalutationEntity:
					factoryToUse = new SalutationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ServiceAllocationEntity:
					factoryToUse = new ServiceAllocationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ServiceIdToCardEntity:
					factoryToUse = new ServiceIdToCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ServicePercentageEntity:
					factoryToUse = new ServicePercentageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ShiftEntity:
					factoryToUse = new ShiftEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ShiftInventoryEntity:
					factoryToUse = new ShiftInventoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ShiftStateEntity:
					factoryToUse = new ShiftStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ShortDistanceEntity:
					factoryToUse = new ShortDistanceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SpecialReceiptEntity:
					factoryToUse = new SpecialReceiptEntityFactory();
					break;
				case VarioSL.Entities.EntityType.StopEntity:
					factoryToUse = new StopEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SystemFieldEntity:
					factoryToUse = new SystemFieldEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity:
					factoryToUse = new SystemFieldBarcodeLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SystemFieldDynamicGraphicLayoutObjectEntity:
					factoryToUse = new SystemFieldDynamicGraphicLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity:
					factoryToUse = new SystemFieldTextLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SystemTextEntity:
					factoryToUse = new SystemTextEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TariffEntity:
					factoryToUse = new TariffEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TariffParameterEntity:
					factoryToUse = new TariffParameterEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TariffReleaseEntity:
					factoryToUse = new TariffReleaseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TemporalTypeEntity:
					factoryToUse = new TemporalTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketEntity:
					factoryToUse = new TicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketCancellationTypeEntity:
					factoryToUse = new TicketCancellationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketCategoryEntity:
					factoryToUse = new TicketCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketDayTypeEntity:
					factoryToUse = new TicketDayTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassEntity:
					factoryToUse = new TicketDeviceClassEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity:
					factoryToUse = new TicketDeviceClassOutputDeviceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity:
					factoryToUse = new TicketDeviceClassPaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity:
					factoryToUse = new TicketDevicePaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketGroupEntity:
					factoryToUse = new TicketGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketOrganizationEntity:
					factoryToUse = new TicketOrganizationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketOutputdeviceEntity:
					factoryToUse = new TicketOutputdeviceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketPaymentIntervalEntity:
					factoryToUse = new TicketPaymentIntervalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity:
					factoryToUse = new TicketPhysicalCardTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketSelectionModeEntity:
					factoryToUse = new TicketSelectionModeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketServicesPermittedEntity:
					factoryToUse = new TicketServicesPermittedEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketToGroupEntity:
					factoryToUse = new TicketToGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketTypeEntity:
					factoryToUse = new TicketTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketVendingClientEntity:
					factoryToUse = new TicketVendingClientEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionEntity:
					factoryToUse = new TransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionAdditionalEntity:
					factoryToUse = new TransactionAdditionalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionBackupEntity:
					factoryToUse = new TransactionBackupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionExtensionEntity:
					factoryToUse = new TransactionExtensionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TranslationEntity:
					factoryToUse = new TranslationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TypeOfCardEntity:
					factoryToUse = new TypeOfCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TypeOfUnitEntity:
					factoryToUse = new TypeOfUnitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UnitEntity:
					factoryToUse = new UnitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserConfigEntity:
					factoryToUse = new UserConfigEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserGroupEntity:
					factoryToUse = new UserGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserGroupRightEntity:
					factoryToUse = new UserGroupRightEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserIsMemberEntity:
					factoryToUse = new UserIsMemberEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserKeyEntity:
					factoryToUse = new UserKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserLanguageEntity:
					factoryToUse = new UserLanguageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserListEntity:
					factoryToUse = new UserListEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserResourceEntity:
					factoryToUse = new UserResourceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserStatusEntity:
					factoryToUse = new UserStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UserToWorkstationEntity:
					factoryToUse = new UserToWorkstationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationTransactionEntity:
					factoryToUse = new ValidationTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VarioAddressEntity:
					factoryToUse = new VarioAddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VarioSettlementEntity:
					factoryToUse = new VarioSettlementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VarioTypeOfSettlementEntity:
					factoryToUse = new VarioTypeOfSettlementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvKeyInfoEntity:
					factoryToUse = new VdvKeyInfoEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvKeySetEntity:
					factoryToUse = new VdvKeySetEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvLayoutEntity:
					factoryToUse = new VdvLayoutEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvLayoutObjectEntity:
					factoryToUse = new VdvLayoutObjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity:
					factoryToUse = new VdvLoadKeyMessageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvProductEntity:
					factoryToUse = new VdvProductEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvSamStatusEntity:
					factoryToUse = new VdvSamStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvTagEntity:
					factoryToUse = new VdvTagEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvTerminalEntity:
					factoryToUse = new VdvTerminalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvTypeEntity:
					factoryToUse = new VdvTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkstationEntity:
					factoryToUse = new WorkstationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingCancelRecTapCmlSettledEntity:
					factoryToUse = new AccountingCancelRecTapCmlSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingCancelRecTapTabSettledEntity:
					factoryToUse = new AccountingCancelRecTapTabSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingMethodEntity:
					factoryToUse = new AccountingMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingModeEntity:
					factoryToUse = new AccountingModeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecLoadUseCmlSettledEntity:
					factoryToUse = new AccountingRecLoadUseCmlSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecLoadUseTabSettledEntity:
					factoryToUse = new AccountingRecLoadUseTabSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedLoadAndUseEntity:
					factoryToUse = new AccountingRecognizedLoadAndUseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedPaymentEntity:
					factoryToUse = new AccountingRecognizedPaymentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapEntity:
					factoryToUse = new AccountingRecognizedTapEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCancellationEntity:
					factoryToUse = new AccountingRecognizedTapCancellationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCmlSettledEntity:
					factoryToUse = new AccountingRecognizedTapCmlSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapTabSettledEntity:
					factoryToUse = new AccountingRecognizedTapTabSettledEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountingReconciledPaymentEntity:
					factoryToUse = new AccountingReconciledPaymentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AccountMessageSettingEntity:
					factoryToUse = new AccountMessageSettingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AdditionalDataEntity:
					factoryToUse = new AdditionalDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AddressEntity:
					factoryToUse = new AddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AddressBookEntryEntity:
					factoryToUse = new AddressBookEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AddressBookEntryTypeEntity:
					factoryToUse = new AddressBookEntryTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AddressTypeEntity:
					factoryToUse = new AddressTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AggregationFunctionEntity:
					factoryToUse = new AggregationFunctionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AggregationTypeEntity:
					factoryToUse = new AggregationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApportionEntity:
					factoryToUse = new ApportionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ApportionPassRevenueRecognitionEntity:
					factoryToUse = new ApportionPassRevenueRecognitionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ArchiveStateEntity:
					factoryToUse = new ArchiveStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity:
					factoryToUse = new AttributeToMobilityProviderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AuditRegisterEntity:
					factoryToUse = new AuditRegisterEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AuditRegisterValueEntity:
					factoryToUse = new AuditRegisterValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AuditRegisterValueTypeEntity:
					factoryToUse = new AuditRegisterValueTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AutoloadSettingEntity:
					factoryToUse = new AutoloadSettingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity:
					factoryToUse = new AutoloadToPaymentOptionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BadCardEntity:
					factoryToUse = new BadCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BadCheckEntity:
					factoryToUse = new BadCheckEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankConnectionDataEntity:
					factoryToUse = new BankConnectionDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankStatementEntity:
					factoryToUse = new BankStatementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankStatementRecordTypeEntity:
					factoryToUse = new BankStatementRecordTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankStatementStateEntity:
					factoryToUse = new BankStatementStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BankStatementVerificationEntity:
					factoryToUse = new BankStatementVerificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BookingEntity:
					factoryToUse = new BookingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BookingItemEntity:
					factoryToUse = new BookingItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BookingItemPaymentEntity:
					factoryToUse = new BookingItemPaymentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BookingItemPaymentTypeEntity:
					factoryToUse = new BookingItemPaymentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.BookingTypeEntity:
					factoryToUse = new BookingTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CappingJournalEntity:
					factoryToUse = new CappingJournalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CappingStateEntity:
					factoryToUse = new CappingStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardEntity:
					factoryToUse = new CardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardBodyTypeEntity:
					factoryToUse = new CardBodyTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardDormancyEntity:
					factoryToUse = new CardDormancyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardEventEntity:
					factoryToUse = new CardEventEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardEventTypeEntity:
					factoryToUse = new CardEventTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardFulfillmentStatusEntity:
					factoryToUse = new CardFulfillmentStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardHolderEntity:
					factoryToUse = new CardHolderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardHolderOrganizationEntity:
					factoryToUse = new CardHolderOrganizationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardKeyEntity:
					factoryToUse = new CardKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardLiabilityEntity:
					factoryToUse = new CardLiabilityEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardLinkEntity:
					factoryToUse = new CardLinkEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardOrderDetailEntity:
					factoryToUse = new CardOrderDetailEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardPhysicalDetailEntity:
					factoryToUse = new CardPhysicalDetailEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardPrintingTypeEntity:
					factoryToUse = new CardPrintingTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardStockTransferEntity:
					factoryToUse = new CardStockTransferEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardToContractEntity:
					factoryToUse = new CardToContractEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardToRuleViolationEntity:
					factoryToUse = new CardToRuleViolationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CardWorkItemEntity:
					factoryToUse = new CardWorkItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CertificateEntity:
					factoryToUse = new CertificateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CertificateFormatEntity:
					factoryToUse = new CertificateFormatEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CertificatePurposeEntity:
					factoryToUse = new CertificatePurposeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CertificateTypeEntity:
					factoryToUse = new CertificateTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClaimEntity:
					factoryToUse = new ClaimEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ClientFilterTypeEntity:
					factoryToUse = new ClientFilterTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CloseoutPeriodEntity:
					factoryToUse = new CloseoutPeriodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CloseoutStateEntity:
					factoryToUse = new CloseoutStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CloseoutTypeEntity:
					factoryToUse = new CloseoutTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CommentEntity:
					factoryToUse = new CommentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CommentPriorityEntity:
					factoryToUse = new CommentPriorityEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CommentTypeEntity:
					factoryToUse = new CommentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ConfigurationEntity:
					factoryToUse = new ConfigurationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ConfigurationDataTypeEntity:
					factoryToUse = new ConfigurationDataTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ConfigurationDefinitionEntity:
					factoryToUse = new ConfigurationDefinitionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ConfigurationOverwriteEntity:
					factoryToUse = new ConfigurationOverwriteEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContentItemEntity:
					factoryToUse = new ContentItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContentTypeEntity:
					factoryToUse = new ContentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractEntity:
					factoryToUse = new ContractEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractAddressEntity:
					factoryToUse = new ContractAddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractDetailsEntity:
					factoryToUse = new ContractDetailsEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractHistoryEntity:
					factoryToUse = new ContractHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractLinkEntity:
					factoryToUse = new ContractLinkEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractStateEntity:
					factoryToUse = new ContractStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractTerminationEntity:
					factoryToUse = new ContractTerminationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractTerminationTypeEntity:
					factoryToUse = new ContractTerminationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractToPaymentMethodEntity:
					factoryToUse = new ContractToPaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractToProviderEntity:
					factoryToUse = new ContractToProviderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractTypeEntity:
					factoryToUse = new ContractTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ContractWorkItemEntity:
					factoryToUse = new ContractWorkItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CoordinateEntity:
					factoryToUse = new CoordinateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CountryEntity:
					factoryToUse = new CountryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditCardAuthorizationEntity:
					factoryToUse = new CreditCardAuthorizationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity:
					factoryToUse = new CreditCardAuthorizationLogEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditCardMerchantEntity:
					factoryToUse = new CreditCardMerchantEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditCardNetworkEntity:
					factoryToUse = new CreditCardNetworkEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CreditCardTerminalEntity:
					factoryToUse = new CreditCardTerminalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoCertificateEntity:
					factoryToUse = new CryptoCertificateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoContentEntity:
					factoryToUse = new CryptoContentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity:
					factoryToUse = new CryptoCryptogramArchiveEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptogramCertificateEntity:
					factoryToUse = new CryptogramCertificateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptogramCertificateTypeEntity:
					factoryToUse = new CryptogramCertificateTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptogramErrorTypeEntity:
					factoryToUse = new CryptogramErrorTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptogramLoadStatusEntity:
					factoryToUse = new CryptogramLoadStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoOperatorActivationkeyEntity:
					factoryToUse = new CryptoOperatorActivationkeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoQArchiveEntity:
					factoryToUse = new CryptoQArchiveEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoResourceEntity:
					factoryToUse = new CryptoResourceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoResourceDataEntity:
					factoryToUse = new CryptoResourceDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoSamSignCertificateEntity:
					factoryToUse = new CryptoSamSignCertificateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CryptoUicCertificateEntity:
					factoryToUse = new CryptoUicCertificateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomAttributeEntity:
					factoryToUse = new CustomAttributeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomAttributeValueEntity:
					factoryToUse = new CustomAttributeValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity:
					factoryToUse = new CustomAttributeValueToPersonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerAccountEntity:
					factoryToUse = new CustomerAccountEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerAccountNotificationEntity:
					factoryToUse = new CustomerAccountNotificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerAccountRoleEntity:
					factoryToUse = new CustomerAccountRoleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerAccountStateEntity:
					factoryToUse = new CustomerAccountStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity:
					factoryToUse = new CustomerAccountToVerificationAttemptTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.CustomerLanguageEntity:
					factoryToUse = new CustomerLanguageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceCommunicationHistoryEntity:
					factoryToUse = new DeviceCommunicationHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderEntity:
					factoryToUse = new DeviceReaderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity:
					factoryToUse = new DeviceReaderCryptoResourceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderJobEntity:
					factoryToUse = new DeviceReaderJobEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderKeyEntity:
					factoryToUse = new DeviceReaderKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity:
					factoryToUse = new DeviceReaderKeyToCertEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceReaderToCertEntity:
					factoryToUse = new DeviceReaderToCertEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceTokenEntity:
					factoryToUse = new DeviceTokenEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DeviceTokenStateEntity:
					factoryToUse = new DeviceTokenStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DocumentHistoryEntity:
					factoryToUse = new DocumentHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DocumentTypeEntity:
					factoryToUse = new DocumentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DormancyNotificationEntity:
					factoryToUse = new DormancyNotificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningActionEntity:
					factoryToUse = new DunningActionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningLevelConfigurationEntity:
					factoryToUse = new DunningLevelConfigurationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity:
					factoryToUse = new DunningLevelPostingKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningProcessEntity:
					factoryToUse = new DunningProcessEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningToInvoiceEntity:
					factoryToUse = new DunningToInvoiceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningToPostingEntity:
					factoryToUse = new DunningToPostingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.DunningToProductEntity:
					factoryToUse = new DunningToProductEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EmailEventEntity:
					factoryToUse = new EmailEventEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EmailEventResendLockEntity:
					factoryToUse = new EmailEventResendLockEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EmailMessageEntity:
					factoryToUse = new EmailMessageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EntitlementEntity:
					factoryToUse = new EntitlementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EntitlementToProductEntity:
					factoryToUse = new EntitlementToProductEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EntitlementTypeEntity:
					factoryToUse = new EntitlementTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.EventSettingEntity:
					factoryToUse = new EventSettingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareChangeCauseEntity:
					factoryToUse = new FareChangeCauseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionBehaviourEntity:
					factoryToUse = new FareEvasionBehaviourEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionIncidentEntity:
					factoryToUse = new FareEvasionIncidentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionInspectionEntity:
					factoryToUse = new FareEvasionInspectionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FareEvasionStateEntity:
					factoryToUse = new FareEvasionStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FarePaymentErrorEntity:
					factoryToUse = new FarePaymentErrorEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FarePaymentResultTypeEntity:
					factoryToUse = new FarePaymentResultTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FastcardEntity:
					factoryToUse = new FastcardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterCategoryEntity:
					factoryToUse = new FilterCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterElementEntity:
					factoryToUse = new FilterElementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterListEntity:
					factoryToUse = new FilterListEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterListElementEntity:
					factoryToUse = new FilterListElementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterTypeEntity:
					factoryToUse = new FilterTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterValueEntity:
					factoryToUse = new FilterValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FilterValueSetEntity:
					factoryToUse = new FilterValueSetEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FlexValueEntity:
					factoryToUse = new FlexValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FormEntity:
					factoryToUse = new FormEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FormTemplateEntity:
					factoryToUse = new FormTemplateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.FrequencyEntity:
					factoryToUse = new FrequencyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.GenderEntity:
					factoryToUse = new GenderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.IdentificationTypeEntity:
					factoryToUse = new IdentificationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InspectionCriterionEntity:
					factoryToUse = new InspectionCriterionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InspectionReportEntity:
					factoryToUse = new InspectionReportEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InspectionResultEntity:
					factoryToUse = new InspectionResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InventoryStateEntity:
					factoryToUse = new InventoryStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoiceEntity:
					factoryToUse = new InvoiceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoiceEntryEntity:
					factoryToUse = new InvoiceEntryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoiceTypeEntity:
					factoryToUse = new InvoiceTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoicingEntity:
					factoryToUse = new InvoicingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoicingFilterTypeEntity:
					factoryToUse = new InvoicingFilterTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.InvoicingTypeEntity:
					factoryToUse = new InvoicingTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobEntity:
					factoryToUse = new JobEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobBulkImportEntity:
					factoryToUse = new JobBulkImportEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobCardImportEntity:
					factoryToUse = new JobCardImportEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobFileTypeEntity:
					factoryToUse = new JobFileTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobStateEntity:
					factoryToUse = new JobStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.JobTypeEntity:
					factoryToUse = new JobTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.LookupAddressEntity:
					factoryToUse = new LookupAddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MediaEligibilityRequirementEntity:
					factoryToUse = new MediaEligibilityRequirementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MediaSalePreconditionEntity:
					factoryToUse = new MediaSalePreconditionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MerchantEntity:
					factoryToUse = new MerchantEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MessageEntity:
					factoryToUse = new MessageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MessageFormatEntity:
					factoryToUse = new MessageFormatEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MessageTypeEntity:
					factoryToUse = new MessageTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.MobilityProviderEntity:
					factoryToUse = new MobilityProviderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NotificationDeviceEntity:
					factoryToUse = new NotificationDeviceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NotificationMessageEntity:
					factoryToUse = new NotificationMessageEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NotificationMessageOwnerEntity:
					factoryToUse = new NotificationMessageOwnerEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity:
					factoryToUse = new NotificationMessageToCustomerEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NumberGroupEntity:
					factoryToUse = new NumberGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NumberGroupRandomizedEntity:
					factoryToUse = new NumberGroupRandomizedEntityFactory();
					break;
				case VarioSL.Entities.EntityType.NumberGroupScopeEntity:
					factoryToUse = new NumberGroupScopeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderEntity:
					factoryToUse = new OrderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderDetailEntity:
					factoryToUse = new OrderDetailEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderDetailToCardEntity:
					factoryToUse = new OrderDetailToCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity:
					factoryToUse = new OrderDetailToCardHolderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderHistoryEntity:
					factoryToUse = new OrderHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderProcessingCleanupEntity:
					factoryToUse = new OrderProcessingCleanupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderStateEntity:
					factoryToUse = new OrderStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderTypeEntity:
					factoryToUse = new OrderTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderWithTotalEntity:
					factoryToUse = new OrderWithTotalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrderWorkItemEntity:
					factoryToUse = new OrderWorkItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrganizationEntity:
					factoryToUse = new OrganizationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrganizationAddressEntity:
					factoryToUse = new OrganizationAddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrganizationParticipantEntity:
					factoryToUse = new OrganizationParticipantEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrganizationSubtypeEntity:
					factoryToUse = new OrganizationSubtypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.OrganizationTypeEntity:
					factoryToUse = new OrganizationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PageContentEntity:
					factoryToUse = new PageContentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterEntity:
					factoryToUse = new ParameterEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveEntity:
					factoryToUse = new ParameterArchiveEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity:
					factoryToUse = new ParameterArchiveReleaseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveResultViewEntity:
					factoryToUse = new ParameterArchiveResultViewEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterGroupEntity:
					factoryToUse = new ParameterGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterGroupToParameterEntity:
					factoryToUse = new ParameterGroupToParameterEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterTypeEntity:
					factoryToUse = new ParameterTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParameterValueEntity:
					factoryToUse = new ParameterValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParaTransAttributeEntity:
					factoryToUse = new ParaTransAttributeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParaTransAttributeTypeEntity:
					factoryToUse = new ParaTransAttributeTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParaTransJournalEntity:
					factoryToUse = new ParaTransJournalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParaTransJournalTypeEntity:
					factoryToUse = new ParaTransJournalTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParaTransPaymentTypeEntity:
					factoryToUse = new ParaTransPaymentTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ParticipantGroupEntity:
					factoryToUse = new ParticipantGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PassExpiryNotificationEntity:
					factoryToUse = new PassExpiryNotificationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentEntity:
					factoryToUse = new PaymentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentJournalEntity:
					factoryToUse = new PaymentJournalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentJournalToComponentEntity:
					factoryToUse = new PaymentJournalToComponentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentMethodEntity:
					factoryToUse = new PaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentModalityEntity:
					factoryToUse = new PaymentModalityEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentOptionEntity:
					factoryToUse = new PaymentOptionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentProviderEntity:
					factoryToUse = new PaymentProviderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentProviderAccountEntity:
					factoryToUse = new PaymentProviderAccountEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentRecognitionEntity:
					factoryToUse = new PaymentRecognitionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentReconciliationEntity:
					factoryToUse = new PaymentReconciliationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentReconciliationStateEntity:
					factoryToUse = new PaymentReconciliationStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PaymentStateEntity:
					factoryToUse = new PaymentStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PendingOrderEntity:
					factoryToUse = new PendingOrderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PerformanceEntity:
					factoryToUse = new PerformanceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PerformanceAggregationEntity:
					factoryToUse = new PerformanceAggregationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PerformanceComponentEntity:
					factoryToUse = new PerformanceComponentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PerformanceIndicatorEntity:
					factoryToUse = new PerformanceIndicatorEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PersonEntity:
					factoryToUse = new PersonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PersonAddressEntity:
					factoryToUse = new PersonAddressEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PhotographEntity:
					factoryToUse = new PhotographEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PickupLocationEntity:
					factoryToUse = new PickupLocationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PickupLocationToTicketEntity:
					factoryToUse = new PickupLocationToTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PostingEntity:
					factoryToUse = new PostingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PostingKeyEntity:
					factoryToUse = new PostingKeyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PostingKeyCategoryEntity:
					factoryToUse = new PostingKeyCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PostingScopeEntity:
					factoryToUse = new PostingScopeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PriceAdjustmentEntity:
					factoryToUse = new PriceAdjustmentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PriceAdjustmentTemplateEntity:
					factoryToUse = new PriceAdjustmentTemplateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrinterEntity:
					factoryToUse = new PrinterEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrinterStateEntity:
					factoryToUse = new PrinterStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrinterToUnitEntity:
					factoryToUse = new PrinterToUnitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PrinterTypeEntity:
					factoryToUse = new PrinterTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PriorityEntity:
					factoryToUse = new PriorityEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductEntity:
					factoryToUse = new ProductEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductRelationEntity:
					factoryToUse = new ProductRelationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductSubsidyEntity:
					factoryToUse = new ProductSubsidyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductTerminationEntity:
					factoryToUse = new ProductTerminationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductTerminationTypeEntity:
					factoryToUse = new ProductTerminationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProductTypeEntity:
					factoryToUse = new ProductTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PromoCodeEntity:
					factoryToUse = new PromoCodeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.PromoCodeStatusEntity:
					factoryToUse = new PromoCodeStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProviderAccountStateEntity:
					factoryToUse = new ProviderAccountStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ProvisioningStateEntity:
					factoryToUse = new ProvisioningStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RecipientGroupEntity:
					factoryToUse = new RecipientGroupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RecipientGroupMemberEntity:
					factoryToUse = new RecipientGroupMemberEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RefundEntity:
					factoryToUse = new RefundEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RefundReasonEntity:
					factoryToUse = new RefundReasonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RentalStateEntity:
					factoryToUse = new RentalStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RentalTypeEntity:
					factoryToUse = new RentalTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportEntity:
					factoryToUse = new ReportEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportCategoryEntity:
					factoryToUse = new ReportCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportDataFileEntity:
					factoryToUse = new ReportDataFileEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportJobEntity:
					factoryToUse = new ReportJobEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportJobResultEntity:
					factoryToUse = new ReportJobResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ReportToClientEntity:
					factoryToUse = new ReportToClientEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RevenueRecognitionEntity:
					factoryToUse = new RevenueRecognitionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RevenueSettlementEntity:
					factoryToUse = new RevenueSettlementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleViolationEntity:
					factoryToUse = new RuleViolationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleViolationProviderEntity:
					factoryToUse = new RuleViolationProviderEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleViolationSourceTypeEntity:
					factoryToUse = new RuleViolationSourceTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleViolationToTransactionEntity:
					factoryToUse = new RuleViolationToTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.RuleViolationTypeEntity:
					factoryToUse = new RuleViolationTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SaleEntity:
					factoryToUse = new SaleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity:
					factoryToUse = new SalesChannelToPaymentMethodEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SalesRevenueEntity:
					factoryToUse = new SalesRevenueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SaleToComponentEntity:
					factoryToUse = new SaleToComponentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SaleTypeEntity:
					factoryToUse = new SaleTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SamModuleEntity:
					factoryToUse = new SamModuleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SamModuleStatusEntity:
					factoryToUse = new SamModuleStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SchoolYearEntity:
					factoryToUse = new SchoolYearEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SecurityQuestionEntity:
					factoryToUse = new SecurityQuestionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SecurityResponseEntity:
					factoryToUse = new SecurityResponseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SepaFrequencyTypeEntity:
					factoryToUse = new SepaFrequencyTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SepaOwnerTypeEntity:
					factoryToUse = new SepaOwnerTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ServerResponseTimeDataEntity:
					factoryToUse = new ServerResponseTimeDataEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ServerResponseTimeDataAggregationEntity:
					factoryToUse = new ServerResponseTimeDataAggregationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettledRevenueEntity:
					factoryToUse = new SettledRevenueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementAccountEntity:
					factoryToUse = new SettlementAccountEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementCalendarEntity:
					factoryToUse = new SettlementCalendarEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementDistributionPolicyEntity:
					factoryToUse = new SettlementDistributionPolicyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementHolidayEntity:
					factoryToUse = new SettlementHolidayEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementOperationEntity:
					factoryToUse = new SettlementOperationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementQuerySettingEntity:
					factoryToUse = new SettlementQuerySettingEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity:
					factoryToUse = new SettlementQuerySettingToTicketEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementQueryValueEntity:
					factoryToUse = new SettlementQueryValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementReleaseStateEntity:
					factoryToUse = new SettlementReleaseStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementResultEntity:
					factoryToUse = new SettlementResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementRunEntity:
					factoryToUse = new SettlementRunEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementRunValueEntity:
					factoryToUse = new SettlementRunValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity:
					factoryToUse = new SettlementRunValueToVarioSettlementEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementSetupEntity:
					factoryToUse = new SettlementSetupEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SettlementTypeEntity:
					factoryToUse = new SettlementTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SourceEntity:
					factoryToUse = new SourceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.StatementOfAccountEntity:
					factoryToUse = new StatementOfAccountEntityFactory();
					break;
				case VarioSL.Entities.EntityType.StateOfCountryEntity:
					factoryToUse = new StateOfCountryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.StockTransferEntity:
					factoryToUse = new StockTransferEntityFactory();
					break;
				case VarioSL.Entities.EntityType.StorageLocationEntity:
					factoryToUse = new StorageLocationEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SubsidyLimitEntity:
					factoryToUse = new SubsidyLimitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SubsidyTransactionEntity:
					factoryToUse = new SubsidyTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyEntity:
					factoryToUse = new SurveyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyAnswerEntity:
					factoryToUse = new SurveyAnswerEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyChoiceEntity:
					factoryToUse = new SurveyChoiceEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyDescriptionEntity:
					factoryToUse = new SurveyDescriptionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyQuestionEntity:
					factoryToUse = new SurveyQuestionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.SurveyResponseEntity:
					factoryToUse = new SurveyResponseEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TenantEntity:
					factoryToUse = new TenantEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TenantHistoryEntity:
					factoryToUse = new TenantHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TenantPersonEntity:
					factoryToUse = new TenantPersonEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TenantStateEntity:
					factoryToUse = new TenantStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TerminationStatusEntity:
					factoryToUse = new TerminationStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketAssignmentEntity:
					factoryToUse = new TicketAssignmentEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketAssignmentStatusEntity:
					factoryToUse = new TicketAssignmentStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketSerialNumberEntity:
					factoryToUse = new TicketSerialNumberEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TicketSerialNumberStatusEntity:
					factoryToUse = new TicketSerialNumberStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionJournalEntity:
					factoryToUse = new TransactionJournalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionJournalCorrectionEntity:
					factoryToUse = new TransactionJournalCorrectionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionJourneyHistoryEntity:
					factoryToUse = new TransactionJourneyHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionToInspectionEntity:
					factoryToUse = new TransactionToInspectionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransactionTypeEntity:
					factoryToUse = new TransactionTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TransportCompanyEntity:
					factoryToUse = new TransportCompanyEntityFactory();
					break;
				case VarioSL.Entities.EntityType.TriggerTypeEntity:
					factoryToUse = new TriggerTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UnitCollectionEntity:
					factoryToUse = new UnitCollectionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.UnitCollectionToUnitEntity:
					factoryToUse = new UnitCollectionToUnitEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationResultEntity:
					factoryToUse = new ValidationResultEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationResultResolveTypeEntity:
					factoryToUse = new ValidationResultResolveTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationRuleEntity:
					factoryToUse = new ValidationRuleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationRunEntity:
					factoryToUse = new ValidationRunEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationRunRuleEntity:
					factoryToUse = new ValidationRunRuleEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationStatusEntity:
					factoryToUse = new ValidationStatusEntityFactory();
					break;
				case VarioSL.Entities.EntityType.ValidationValueEntity:
					factoryToUse = new ValidationValueEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VdvTransactionEntity:
					factoryToUse = new VdvTransactionEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VerificationAttemptEntity:
					factoryToUse = new VerificationAttemptEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VerificationAttemptTypeEntity:
					factoryToUse = new VerificationAttemptTypeEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VirtualCardEntity:
					factoryToUse = new VirtualCardEntityFactory();
					break;
				case VarioSL.Entities.EntityType.VoucherEntity:
					factoryToUse = new VoucherEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WhitelistEntity:
					factoryToUse = new WhitelistEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WhitelistJournalEntity:
					factoryToUse = new WhitelistJournalEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemEntity:
					factoryToUse = new WorkItemEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemCategoryEntity:
					factoryToUse = new WorkItemCategoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemHistoryEntity:
					factoryToUse = new WorkItemHistoryEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemStateEntity:
					factoryToUse = new WorkItemStateEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemSubjectEntity:
					factoryToUse = new WorkItemSubjectEntityFactory();
					break;
				case VarioSL.Entities.EntityType.WorkItemViewEntity:
					factoryToUse = new WorkItemViewEntityFactory();
					break;
			}
			IEntity toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
	
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
		private static readonly Dictionary<Type, IEntityFactory> _factoryPerType = new Dictionary<Type, IEntityFactory>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(VarioSL.Entities.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity dummy = GeneralEntityFactory.Create((VarioSL.Entities.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Type typeOfEntity)
		{
			IEntityFactory toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the VarioSL.Entities.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(VarioSL.Entities.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
	}
	
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator
	{
		/// <summary>Gets the factory of the Entity type with the VarioSL.Entities.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(int entityTypeValue)
		{
			return (IEntityFactory)this.GetFactoryImpl(entityTypeValue);
		}

		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}
		
		/// <summary>Gets an instance of the TypedListDAO class to execute dynamic lists and projections.</summary>
		/// <returns>ready to use typedlistDAO</returns>
		public IDao GetTypedListDao()
		{
			return new TypedListDAO();
		}
		
		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (VarioSL.Entities.EntityType)Enum.Parse(typeof(VarioSL.Entities.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((VarioSL.Entities.EntityType)Enum.Parse(typeof(VarioSL.Entities.EntityType), leftOperandEntityName, false), joinType, (VarioSL.Entities.EntityType)Enum.Parse(typeof(VarioSL.Entities.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}

		/// <summary>Implementation of the routine which gets the factory of the Entity type with the VarioSL.Entities.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((VarioSL.Entities.EntityType)entityTypeValue);
		}
	
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}

	}
}
