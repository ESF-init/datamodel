﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.FactoryClasses
{
	/// <summary>
	/// Generic factory for DAO objects. 
	/// </summary>
	public partial class DAOFactory
	{
		/// <summary>
		/// Private CTor, no instantiation possible.
		/// </summary>
		private DAOFactory()
		{
		}

		/// <summary>Creates a new AbortedTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for AbortedTransaction Entities</returns>
		public static AbortedTransactionDAO CreateAbortedTransactionDAO()
		{
			return new AbortedTransactionDAO();
		}

		/// <summary>Creates a new AccountBalanceDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountBalance Entities</returns>
		public static AccountBalanceDAO CreateAccountBalanceDAO()
		{
			return new AccountBalanceDAO();
		}

		/// <summary>Creates a new AccountEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountEntry Entities</returns>
		public static AccountEntryDAO CreateAccountEntryDAO()
		{
			return new AccountEntryDAO();
		}

		/// <summary>Creates a new AccountEntryNumberDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountEntryNumber Entities</returns>
		public static AccountEntryNumberDAO CreateAccountEntryNumberDAO()
		{
			return new AccountEntryNumberDAO();
		}

		/// <summary>Creates a new AccountPostingKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountPostingKey Entities</returns>
		public static AccountPostingKeyDAO CreateAccountPostingKeyDAO()
		{
			return new AccountPostingKeyDAO();
		}

		/// <summary>Creates a new ApplicationDAO object</summary>
		/// <returns>the new DAO object ready to use for Application Entities</returns>
		public static ApplicationDAO CreateApplicationDAO()
		{
			return new ApplicationDAO();
		}

		/// <summary>Creates a new ApplicationVersionDAO object</summary>
		/// <returns>the new DAO object ready to use for ApplicationVersion Entities</returns>
		public static ApplicationVersionDAO CreateApplicationVersionDAO()
		{
			return new ApplicationVersionDAO();
		}

		/// <summary>Creates a new ApportionmentDAO object</summary>
		/// <returns>the new DAO object ready to use for Apportionment Entities</returns>
		public static ApportionmentDAO CreateApportionmentDAO()
		{
			return new ApportionmentDAO();
		}

		/// <summary>Creates a new ApportionmentResultDAO object</summary>
		/// <returns>the new DAO object ready to use for ApportionmentResult Entities</returns>
		public static ApportionmentResultDAO CreateApportionmentResultDAO()
		{
			return new ApportionmentResultDAO();
		}

		/// <summary>Creates a new AreaListDAO object</summary>
		/// <returns>the new DAO object ready to use for AreaList Entities</returns>
		public static AreaListDAO CreateAreaListDAO()
		{
			return new AreaListDAO();
		}

		/// <summary>Creates a new AreaListElementDAO object</summary>
		/// <returns>the new DAO object ready to use for AreaListElement Entities</returns>
		public static AreaListElementDAO CreateAreaListElementDAO()
		{
			return new AreaListElementDAO();
		}

		/// <summary>Creates a new AreaListElementGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for AreaListElementGroup Entities</returns>
		public static AreaListElementGroupDAO CreateAreaListElementGroupDAO()
		{
			return new AreaListElementGroupDAO();
		}

		/// <summary>Creates a new AreaTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for AreaType Entities</returns>
		public static AreaTypeDAO CreateAreaTypeDAO()
		{
			return new AreaTypeDAO();
		}

		/// <summary>Creates a new AttributeDAO object</summary>
		/// <returns>the new DAO object ready to use for Attribute Entities</returns>
		public static AttributeDAO CreateAttributeDAO()
		{
			return new AttributeDAO();
		}

		/// <summary>Creates a new AttributeBitmapLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for AttributeBitmapLayoutObject Entities</returns>
		public static AttributeBitmapLayoutObjectDAO CreateAttributeBitmapLayoutObjectDAO()
		{
			return new AttributeBitmapLayoutObjectDAO();
		}

		/// <summary>Creates a new AttributeTextLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for AttributeTextLayoutObject Entities</returns>
		public static AttributeTextLayoutObjectDAO CreateAttributeTextLayoutObjectDAO()
		{
			return new AttributeTextLayoutObjectDAO();
		}

		/// <summary>Creates a new AttributeValueDAO object</summary>
		/// <returns>the new DAO object ready to use for AttributeValue Entities</returns>
		public static AttributeValueDAO CreateAttributeValueDAO()
		{
			return new AttributeValueDAO();
		}

		/// <summary>Creates a new AttributeValueListDAO object</summary>
		/// <returns>the new DAO object ready to use for AttributeValueList Entities</returns>
		public static AttributeValueListDAO CreateAttributeValueListDAO()
		{
			return new AttributeValueListDAO();
		}

		/// <summary>Creates a new AutomatDAO object</summary>
		/// <returns>the new DAO object ready to use for Automat Entities</returns>
		public static AutomatDAO CreateAutomatDAO()
		{
			return new AutomatDAO();
		}

		/// <summary>Creates a new BankDAO object</summary>
		/// <returns>the new DAO object ready to use for Bank Entities</returns>
		public static BankDAO CreateBankDAO()
		{
			return new BankDAO();
		}

		/// <summary>Creates a new BinaryDataDAO object</summary>
		/// <returns>the new DAO object ready to use for BinaryData Entities</returns>
		public static BinaryDataDAO CreateBinaryDataDAO()
		{
			return new BinaryDataDAO();
		}

		/// <summary>Creates a new BlockingReasonDAO object</summary>
		/// <returns>the new DAO object ready to use for BlockingReason Entities</returns>
		public static BlockingReasonDAO CreateBlockingReasonDAO()
		{
			return new BlockingReasonDAO();
		}

		/// <summary>Creates a new BusinessRuleDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRule Entities</returns>
		public static BusinessRuleDAO CreateBusinessRuleDAO()
		{
			return new BusinessRuleDAO();
		}

		/// <summary>Creates a new BusinessRuleClauseDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleClause Entities</returns>
		public static BusinessRuleClauseDAO CreateBusinessRuleClauseDAO()
		{
			return new BusinessRuleClauseDAO();
		}

		/// <summary>Creates a new BusinessRuleConditionDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleCondition Entities</returns>
		public static BusinessRuleConditionDAO CreateBusinessRuleConditionDAO()
		{
			return new BusinessRuleConditionDAO();
		}

		/// <summary>Creates a new BusinessRuleResultDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleResult Entities</returns>
		public static BusinessRuleResultDAO CreateBusinessRuleResultDAO()
		{
			return new BusinessRuleResultDAO();
		}

		/// <summary>Creates a new BusinessRuleTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleType Entities</returns>
		public static BusinessRuleTypeDAO CreateBusinessRuleTypeDAO()
		{
			return new BusinessRuleTypeDAO();
		}

		/// <summary>Creates a new BusinessRuleTypeToVariableDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleTypeToVariable Entities</returns>
		public static BusinessRuleTypeToVariableDAO CreateBusinessRuleTypeToVariableDAO()
		{
			return new BusinessRuleTypeToVariableDAO();
		}

		/// <summary>Creates a new BusinessRuleVariableDAO object</summary>
		/// <returns>the new DAO object ready to use for BusinessRuleVariable Entities</returns>
		public static BusinessRuleVariableDAO CreateBusinessRuleVariableDAO()
		{
			return new BusinessRuleVariableDAO();
		}

		/// <summary>Creates a new CalendarDAO object</summary>
		/// <returns>the new DAO object ready to use for Calendar Entities</returns>
		public static CalendarDAO CreateCalendarDAO()
		{
			return new CalendarDAO();
		}

		/// <summary>Creates a new CalendarEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for CalendarEntry Entities</returns>
		public static CalendarEntryDAO CreateCalendarEntryDAO()
		{
			return new CalendarEntryDAO();
		}

		/// <summary>Creates a new CardActionAttributeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardActionAttribute Entities</returns>
		public static CardActionAttributeDAO CreateCardActionAttributeDAO()
		{
			return new CardActionAttributeDAO();
		}

		/// <summary>Creates a new CardActionRequestDAO object</summary>
		/// <returns>the new DAO object ready to use for CardActionRequest Entities</returns>
		public static CardActionRequestDAO CreateCardActionRequestDAO()
		{
			return new CardActionRequestDAO();
		}

		/// <summary>Creates a new CardActionRequestTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardActionRequestType Entities</returns>
		public static CardActionRequestTypeDAO CreateCardActionRequestTypeDAO()
		{
			return new CardActionRequestTypeDAO();
		}

		/// <summary>Creates a new CardChipTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardChipType Entities</returns>
		public static CardChipTypeDAO CreateCardChipTypeDAO()
		{
			return new CardChipTypeDAO();
		}

		/// <summary>Creates a new CardStateDAO object</summary>
		/// <returns>the new DAO object ready to use for CardState Entities</returns>
		public static CardStateDAO CreateCardStateDAO()
		{
			return new CardStateDAO();
		}

		/// <summary>Creates a new CardTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for CardTicket Entities</returns>
		public static CardTicketDAO CreateCardTicketDAO()
		{
			return new CardTicketDAO();
		}

		/// <summary>Creates a new CardTicketToTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for CardTicketToTicket Entities</returns>
		public static CardTicketToTicketDAO CreateCardTicketToTicketDAO()
		{
			return new CardTicketToTicketDAO();
		}

		/// <summary>Creates a new CashServiceDAO object</summary>
		/// <returns>the new DAO object ready to use for CashService Entities</returns>
		public static CashServiceDAO CreateCashServiceDAO()
		{
			return new CashServiceDAO();
		}

		/// <summary>Creates a new CashServiceBalanceDAO object</summary>
		/// <returns>the new DAO object ready to use for CashServiceBalance Entities</returns>
		public static CashServiceBalanceDAO CreateCashServiceBalanceDAO()
		{
			return new CashServiceBalanceDAO();
		}

		/// <summary>Creates a new CashServiceDataDAO object</summary>
		/// <returns>the new DAO object ready to use for CashServiceData Entities</returns>
		public static CashServiceDataDAO CreateCashServiceDataDAO()
		{
			return new CashServiceDataDAO();
		}

		/// <summary>Creates a new CashUnitDAO object</summary>
		/// <returns>the new DAO object ready to use for CashUnit Entities</returns>
		public static CashUnitDAO CreateCashUnitDAO()
		{
			return new CashUnitDAO();
		}

		/// <summary>Creates a new ChoiceDAO object</summary>
		/// <returns>the new DAO object ready to use for Choice Entities</returns>
		public static ChoiceDAO CreateChoiceDAO()
		{
			return new ChoiceDAO();
		}

		/// <summary>Creates a new ClearingDAO object</summary>
		/// <returns>the new DAO object ready to use for Clearing Entities</returns>
		public static ClearingDAO CreateClearingDAO()
		{
			return new ClearingDAO();
		}

		/// <summary>Creates a new ClearingClassificationDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingClassification Entities</returns>
		public static ClearingClassificationDAO CreateClearingClassificationDAO()
		{
			return new ClearingClassificationDAO();
		}

		/// <summary>Creates a new ClearingDetailDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingDetail Entities</returns>
		public static ClearingDetailDAO CreateClearingDetailDAO()
		{
			return new ClearingDetailDAO();
		}

		/// <summary>Creates a new ClearingResultDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingResult Entities</returns>
		public static ClearingResultDAO CreateClearingResultDAO()
		{
			return new ClearingResultDAO();
		}

		/// <summary>Creates a new ClearingResultLevelDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingResultLevel Entities</returns>
		public static ClearingResultLevelDAO CreateClearingResultLevelDAO()
		{
			return new ClearingResultLevelDAO();
		}

		/// <summary>Creates a new ClearingStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingState Entities</returns>
		public static ClearingStateDAO CreateClearingStateDAO()
		{
			return new ClearingStateDAO();
		}

		/// <summary>Creates a new ClearingSumDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingSum Entities</returns>
		public static ClearingSumDAO CreateClearingSumDAO()
		{
			return new ClearingSumDAO();
		}

		/// <summary>Creates a new ClearingTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for ClearingTransaction Entities</returns>
		public static ClearingTransactionDAO CreateClearingTransactionDAO()
		{
			return new ClearingTransactionDAO();
		}

		/// <summary>Creates a new ClientDAO object</summary>
		/// <returns>the new DAO object ready to use for Client Entities</returns>
		public static ClientDAO CreateClientDAO()
		{
			return new ClientDAO();
		}

		/// <summary>Creates a new ClientAdaptedLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientAdaptedLayoutObject Entities</returns>
		public static ClientAdaptedLayoutObjectDAO CreateClientAdaptedLayoutObjectDAO()
		{
			return new ClientAdaptedLayoutObjectDAO();
		}

		/// <summary>Creates a new ComponentDAO object</summary>
		/// <returns>the new DAO object ready to use for Component Entities</returns>
		public static ComponentDAO CreateComponentDAO()
		{
			return new ComponentDAO();
		}

		/// <summary>Creates a new ComponentClearingDAO object</summary>
		/// <returns>the new DAO object ready to use for ComponentClearing Entities</returns>
		public static ComponentClearingDAO CreateComponentClearingDAO()
		{
			return new ComponentClearingDAO();
		}

		/// <summary>Creates a new ComponentFillingDAO object</summary>
		/// <returns>the new DAO object ready to use for ComponentFilling Entities</returns>
		public static ComponentFillingDAO CreateComponentFillingDAO()
		{
			return new ComponentFillingDAO();
		}

		/// <summary>Creates a new ComponentStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ComponentState Entities</returns>
		public static ComponentStateDAO CreateComponentStateDAO()
		{
			return new ComponentStateDAO();
		}

		/// <summary>Creates a new ComponentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ComponentType Entities</returns>
		public static ComponentTypeDAO CreateComponentTypeDAO()
		{
			return new ComponentTypeDAO();
		}

		/// <summary>Creates a new ConditionalSubPageLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for ConditionalSubPageLayoutObject Entities</returns>
		public static ConditionalSubPageLayoutObjectDAO CreateConditionalSubPageLayoutObjectDAO()
		{
			return new ConditionalSubPageLayoutObjectDAO();
		}

		/// <summary>Creates a new CreditScreeningDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditScreening Entities</returns>
		public static CreditScreeningDAO CreateCreditScreeningDAO()
		{
			return new CreditScreeningDAO();
		}

		/// <summary>Creates a new DayTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for DayType Entities</returns>
		public static DayTypeDAO CreateDayTypeDAO()
		{
			return new DayTypeDAO();
		}

		/// <summary>Creates a new DebtorDAO object</summary>
		/// <returns>the new DAO object ready to use for Debtor Entities</returns>
		public static DebtorDAO CreateDebtorDAO()
		{
			return new DebtorDAO();
		}

		/// <summary>Creates a new DebtorCardDAO object</summary>
		/// <returns>the new DAO object ready to use for DebtorCard Entities</returns>
		public static DebtorCardDAO CreateDebtorCardDAO()
		{
			return new DebtorCardDAO();
		}

		/// <summary>Creates a new DebtorCardHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for DebtorCardHistory Entities</returns>
		public static DebtorCardHistoryDAO CreateDebtorCardHistoryDAO()
		{
			return new DebtorCardHistoryDAO();
		}

		/// <summary>Creates a new DefaultPinDAO object</summary>
		/// <returns>the new DAO object ready to use for DefaultPin Entities</returns>
		public static DefaultPinDAO CreateDefaultPinDAO()
		{
			return new DefaultPinDAO();
		}

		/// <summary>Creates a new DepotDAO object</summary>
		/// <returns>the new DAO object ready to use for Depot Entities</returns>
		public static DepotDAO CreateDepotDAO()
		{
			return new DepotDAO();
		}

		/// <summary>Creates a new DeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for Device Entities</returns>
		public static DeviceDAO CreateDeviceDAO()
		{
			return new DeviceDAO();
		}

		/// <summary>Creates a new DeviceBookingStateDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceBookingState Entities</returns>
		public static DeviceBookingStateDAO CreateDeviceBookingStateDAO()
		{
			return new DeviceBookingStateDAO();
		}

		/// <summary>Creates a new DeviceClassDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceClass Entities</returns>
		public static DeviceClassDAO CreateDeviceClassDAO()
		{
			return new DeviceClassDAO();
		}

		/// <summary>Creates a new DevicePaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for DevicePaymentMethod Entities</returns>
		public static DevicePaymentMethodDAO CreateDevicePaymentMethodDAO()
		{
			return new DevicePaymentMethodDAO();
		}

		/// <summary>Creates a new DirectionDAO object</summary>
		/// <returns>the new DAO object ready to use for Direction Entities</returns>
		public static DirectionDAO CreateDirectionDAO()
		{
			return new DirectionDAO();
		}

		/// <summary>Creates a new DunningLevelDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningLevel Entities</returns>
		public static DunningLevelDAO CreateDunningLevelDAO()
		{
			return new DunningLevelDAO();
		}

		/// <summary>Creates a new EticketDAO object</summary>
		/// <returns>the new DAO object ready to use for Eticket Entities</returns>
		public static EticketDAO CreateEticketDAO()
		{
			return new EticketDAO();
		}

		/// <summary>Creates a new ExportInfoDAO object</summary>
		/// <returns>the new DAO object ready to use for ExportInfo Entities</returns>
		public static ExportInfoDAO CreateExportInfoDAO()
		{
			return new ExportInfoDAO();
		}

		/// <summary>Creates a new ExternalCardDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalCard Entities</returns>
		public static ExternalCardDAO CreateExternalCardDAO()
		{
			return new ExternalCardDAO();
		}

		/// <summary>Creates a new ExternalDataDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalData Entities</returns>
		public static ExternalDataDAO CreateExternalDataDAO()
		{
			return new ExternalDataDAO();
		}

		/// <summary>Creates a new ExternalEffortDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalEffort Entities</returns>
		public static ExternalEffortDAO CreateExternalEffortDAO()
		{
			return new ExternalEffortDAO();
		}

		/// <summary>Creates a new ExternalPacketDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalPacket Entities</returns>
		public static ExternalPacketDAO CreateExternalPacketDAO()
		{
			return new ExternalPacketDAO();
		}

		/// <summary>Creates a new ExternalPacketEffortDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalPacketEffort Entities</returns>
		public static ExternalPacketEffortDAO CreateExternalPacketEffortDAO()
		{
			return new ExternalPacketEffortDAO();
		}

		/// <summary>Creates a new ExternalTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalType Entities</returns>
		public static ExternalTypeDAO CreateExternalTypeDAO()
		{
			return new ExternalTypeDAO();
		}

		/// <summary>Creates a new FareEvasionCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionCategory Entities</returns>
		public static FareEvasionCategoryDAO CreateFareEvasionCategoryDAO()
		{
			return new FareEvasionCategoryDAO();
		}

		/// <summary>Creates a new FareEvasionReasonDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionReason Entities</returns>
		public static FareEvasionReasonDAO CreateFareEvasionReasonDAO()
		{
			return new FareEvasionReasonDAO();
		}

		/// <summary>Creates a new FareEvasionReasonToCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionReasonToCategory Entities</returns>
		public static FareEvasionReasonToCategoryDAO CreateFareEvasionReasonToCategoryDAO()
		{
			return new FareEvasionReasonToCategoryDAO();
		}

		/// <summary>Creates a new FareMatrixDAO object</summary>
		/// <returns>the new DAO object ready to use for FareMatrix Entities</returns>
		public static FareMatrixDAO CreateFareMatrixDAO()
		{
			return new FareMatrixDAO();
		}

		/// <summary>Creates a new FareMatrixEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for FareMatrixEntry Entities</returns>
		public static FareMatrixEntryDAO CreateFareMatrixEntryDAO()
		{
			return new FareMatrixEntryDAO();
		}

		/// <summary>Creates a new FareStageDAO object</summary>
		/// <returns>the new DAO object ready to use for FareStage Entities</returns>
		public static FareStageDAO CreateFareStageDAO()
		{
			return new FareStageDAO();
		}

		/// <summary>Creates a new FareStageAliasDAO object</summary>
		/// <returns>the new DAO object ready to use for FareStageAlias Entities</returns>
		public static FareStageAliasDAO CreateFareStageAliasDAO()
		{
			return new FareStageAliasDAO();
		}

		/// <summary>Creates a new FareStageHierarchieLevelDAO object</summary>
		/// <returns>the new DAO object ready to use for FareStageHierarchieLevel Entities</returns>
		public static FareStageHierarchieLevelDAO CreateFareStageHierarchieLevelDAO()
		{
			return new FareStageHierarchieLevelDAO();
		}

		/// <summary>Creates a new FareStageListDAO object</summary>
		/// <returns>the new DAO object ready to use for FareStageList Entities</returns>
		public static FareStageListDAO CreateFareStageListDAO()
		{
			return new FareStageListDAO();
		}

		/// <summary>Creates a new FareStageTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for FareStageType Entities</returns>
		public static FareStageTypeDAO CreateFareStageTypeDAO()
		{
			return new FareStageTypeDAO();
		}

		/// <summary>Creates a new FareTableDAO object</summary>
		/// <returns>the new DAO object ready to use for FareTable Entities</returns>
		public static FareTableDAO CreateFareTableDAO()
		{
			return new FareTableDAO();
		}

		/// <summary>Creates a new FareTableEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for FareTableEntry Entities</returns>
		public static FareTableEntryDAO CreateFareTableEntryDAO()
		{
			return new FareTableEntryDAO();
		}

		/// <summary>Creates a new FixedBitmapLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for FixedBitmapLayoutObject Entities</returns>
		public static FixedBitmapLayoutObjectDAO CreateFixedBitmapLayoutObjectDAO()
		{
			return new FixedBitmapLayoutObjectDAO();
		}

		/// <summary>Creates a new FixedTextLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for FixedTextLayoutObject Entities</returns>
		public static FixedTextLayoutObjectDAO CreateFixedTextLayoutObjectDAO()
		{
			return new FixedTextLayoutObjectDAO();
		}

		/// <summary>Creates a new GuiDefDAO object</summary>
		/// <returns>the new DAO object ready to use for GuiDef Entities</returns>
		public static GuiDefDAO CreateGuiDefDAO()
		{
			return new GuiDefDAO();
		}

		/// <summary>Creates a new KeyAttributeTransfromDAO object</summary>
		/// <returns>the new DAO object ready to use for KeyAttributeTransfrom Entities</returns>
		public static KeyAttributeTransfromDAO CreateKeyAttributeTransfromDAO()
		{
			return new KeyAttributeTransfromDAO();
		}

		/// <summary>Creates a new KeySelectionModeDAO object</summary>
		/// <returns>the new DAO object ready to use for KeySelectionMode Entities</returns>
		public static KeySelectionModeDAO CreateKeySelectionModeDAO()
		{
			return new KeySelectionModeDAO();
		}

		/// <summary>Creates a new KVVSubscriptionDAO object</summary>
		/// <returns>the new DAO object ready to use for KVVSubscription Entities</returns>
		public static KVVSubscriptionDAO CreateKVVSubscriptionDAO()
		{
			return new KVVSubscriptionDAO();
		}

		/// <summary>Creates a new LanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for Language Entities</returns>
		public static LanguageDAO CreateLanguageDAO()
		{
			return new LanguageDAO();
		}

		/// <summary>Creates a new LayoutDAO object</summary>
		/// <returns>the new DAO object ready to use for Layout Entities</returns>
		public static LayoutDAO CreateLayoutDAO()
		{
			return new LayoutDAO();
		}

		/// <summary>Creates a new LineDAO object</summary>
		/// <returns>the new DAO object ready to use for Line Entities</returns>
		public static LineDAO CreateLineDAO()
		{
			return new LineDAO();
		}

		/// <summary>Creates a new LineGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for LineGroup Entities</returns>
		public static LineGroupDAO CreateLineGroupDAO()
		{
			return new LineGroupDAO();
		}

		/// <summary>Creates a new LineGroupToLineGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for LineGroupToLineGroup Entities</returns>
		public static LineGroupToLineGroupDAO CreateLineGroupToLineGroupDAO()
		{
			return new LineGroupToLineGroupDAO();
		}

		/// <summary>Creates a new LineToLineGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for LineToLineGroup Entities</returns>
		public static LineToLineGroupDAO CreateLineToLineGroupDAO()
		{
			return new LineToLineGroupDAO();
		}

		/// <summary>Creates a new ListLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for ListLayoutObject Entities</returns>
		public static ListLayoutObjectDAO CreateListLayoutObjectDAO()
		{
			return new ListLayoutObjectDAO();
		}

		/// <summary>Creates a new ListLayoutTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ListLayoutType Entities</returns>
		public static ListLayoutTypeDAO CreateListLayoutTypeDAO()
		{
			return new ListLayoutTypeDAO();
		}

		/// <summary>Creates a new LogoDAO object</summary>
		/// <returns>the new DAO object ready to use for Logo Entities</returns>
		public static LogoDAO CreateLogoDAO()
		{
			return new LogoDAO();
		}

		/// <summary>Creates a new NetDAO object</summary>
		/// <returns>the new DAO object ready to use for Net Entities</returns>
		public static NetDAO CreateNetDAO()
		{
			return new NetDAO();
		}

		/// <summary>Creates a new NumberRangeDAO object</summary>
		/// <returns>the new DAO object ready to use for NumberRange Entities</returns>
		public static NumberRangeDAO CreateNumberRangeDAO()
		{
			return new NumberRangeDAO();
		}

		/// <summary>Creates a new OperatorDAO object</summary>
		/// <returns>the new DAO object ready to use for Operator Entities</returns>
		public static OperatorDAO CreateOperatorDAO()
		{
			return new OperatorDAO();
		}

		/// <summary>Creates a new OutputDeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for OutputDevice Entities</returns>
		public static OutputDeviceDAO CreateOutputDeviceDAO()
		{
			return new OutputDeviceDAO();
		}

		/// <summary>Creates a new PageContentGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for PageContentGroup Entities</returns>
		public static PageContentGroupDAO CreatePageContentGroupDAO()
		{
			return new PageContentGroupDAO();
		}

		/// <summary>Creates a new PageContentGroupToContentDAO object</summary>
		/// <returns>the new DAO object ready to use for PageContentGroupToContent Entities</returns>
		public static PageContentGroupToContentDAO CreatePageContentGroupToContentDAO()
		{
			return new PageContentGroupToContentDAO();
		}

		/// <summary>Creates a new PanelDAO object</summary>
		/// <returns>the new DAO object ready to use for Panel Entities</returns>
		public static PanelDAO CreatePanelDAO()
		{
			return new PanelDAO();
		}

		/// <summary>Creates a new ParameterAttributeValueDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterAttributeValue Entities</returns>
		public static ParameterAttributeValueDAO CreateParameterAttributeValueDAO()
		{
			return new ParameterAttributeValueDAO();
		}

		/// <summary>Creates a new ParameterFareStageDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterFareStage Entities</returns>
		public static ParameterFareStageDAO CreateParameterFareStageDAO()
		{
			return new ParameterFareStageDAO();
		}

		/// <summary>Creates a new ParameterTariffDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterTariff Entities</returns>
		public static ParameterTariffDAO CreateParameterTariffDAO()
		{
			return new ParameterTariffDAO();
		}

		/// <summary>Creates a new ParameterTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterTicket Entities</returns>
		public static ParameterTicketDAO CreateParameterTicketDAO()
		{
			return new ParameterTicketDAO();
		}

		/// <summary>Creates a new PaymentDetailDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentDetail Entities</returns>
		public static PaymentDetailDAO CreatePaymentDetailDAO()
		{
			return new PaymentDetailDAO();
		}

		/// <summary>Creates a new PaymentIntervalDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentInterval Entities</returns>
		public static PaymentIntervalDAO CreatePaymentIntervalDAO()
		{
			return new PaymentIntervalDAO();
		}

		/// <summary>Creates a new PointOfSaleDAO object</summary>
		/// <returns>the new DAO object ready to use for PointOfSale Entities</returns>
		public static PointOfSaleDAO CreatePointOfSaleDAO()
		{
			return new PointOfSaleDAO();
		}

		/// <summary>Creates a new PredefinedKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for PredefinedKey Entities</returns>
		public static PredefinedKeyDAO CreatePredefinedKeyDAO()
		{
			return new PredefinedKeyDAO();
		}

		/// <summary>Creates a new PriceTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceType Entities</returns>
		public static PriceTypeDAO CreatePriceTypeDAO()
		{
			return new PriceTypeDAO();
		}

		/// <summary>Creates a new PrimalKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for PrimalKey Entities</returns>
		public static PrimalKeyDAO CreatePrimalKeyDAO()
		{
			return new PrimalKeyDAO();
		}

		/// <summary>Creates a new PrintTextDAO object</summary>
		/// <returns>the new DAO object ready to use for PrintText Entities</returns>
		public static PrintTextDAO CreatePrintTextDAO()
		{
			return new PrintTextDAO();
		}

		/// <summary>Creates a new ProductOnCardDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductOnCard Entities</returns>
		public static ProductOnCardDAO CreateProductOnCardDAO()
		{
			return new ProductOnCardDAO();
		}

		/// <summary>Creates a new ProtocolDAO object</summary>
		/// <returns>the new DAO object ready to use for Protocol Entities</returns>
		public static ProtocolDAO CreateProtocolDAO()
		{
			return new ProtocolDAO();
		}

		/// <summary>Creates a new ProtocolActionDAO object</summary>
		/// <returns>the new DAO object ready to use for ProtocolAction Entities</returns>
		public static ProtocolActionDAO CreateProtocolActionDAO()
		{
			return new ProtocolActionDAO();
		}

		/// <summary>Creates a new ProtocolFunctionDAO object</summary>
		/// <returns>the new DAO object ready to use for ProtocolFunction Entities</returns>
		public static ProtocolFunctionDAO CreateProtocolFunctionDAO()
		{
			return new ProtocolFunctionDAO();
		}

		/// <summary>Creates a new ProtocolFunctionGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for ProtocolFunctionGroup Entities</returns>
		public static ProtocolFunctionGroupDAO CreateProtocolFunctionGroupDAO()
		{
			return new ProtocolFunctionGroupDAO();
		}

		/// <summary>Creates a new ProtocolMessageDAO object</summary>
		/// <returns>the new DAO object ready to use for ProtocolMessage Entities</returns>
		public static ProtocolMessageDAO CreateProtocolMessageDAO()
		{
			return new ProtocolMessageDAO();
		}

		/// <summary>Creates a new QualificationDAO object</summary>
		/// <returns>the new DAO object ready to use for Qualification Entities</returns>
		public static QualificationDAO CreateQualificationDAO()
		{
			return new QualificationDAO();
		}

		/// <summary>Creates a new ResponsibilityDAO object</summary>
		/// <returns>the new DAO object ready to use for Responsibility Entities</returns>
		public static ResponsibilityDAO CreateResponsibilityDAO()
		{
			return new ResponsibilityDAO();
		}

		/// <summary>Creates a new RevenueTransactionTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for RevenueTransactionType Entities</returns>
		public static RevenueTransactionTypeDAO CreateRevenueTransactionTypeDAO()
		{
			return new RevenueTransactionTypeDAO();
		}

		/// <summary>Creates a new RmPaymentDAO object</summary>
		/// <returns>the new DAO object ready to use for RmPayment Entities</returns>
		public static RmPaymentDAO CreateRmPaymentDAO()
		{
			return new RmPaymentDAO();
		}

		/// <summary>Creates a new RouteDAO object</summary>
		/// <returns>the new DAO object ready to use for Route Entities</returns>
		public static RouteDAO CreateRouteDAO()
		{
			return new RouteDAO();
		}

		/// <summary>Creates a new RouteNameDAO object</summary>
		/// <returns>the new DAO object ready to use for RouteName Entities</returns>
		public static RouteNameDAO CreateRouteNameDAO()
		{
			return new RouteNameDAO();
		}

		/// <summary>Creates a new RuleCappingDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleCapping Entities</returns>
		public static RuleCappingDAO CreateRuleCappingDAO()
		{
			return new RuleCappingDAO();
		}

		/// <summary>Creates a new RuleCappingToFtEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleCappingToFtEntry Entities</returns>
		public static RuleCappingToFtEntryDAO CreateRuleCappingToFtEntryDAO()
		{
			return new RuleCappingToFtEntryDAO();
		}

		/// <summary>Creates a new RuleCappingToTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleCappingToTicket Entities</returns>
		public static RuleCappingToTicketDAO CreateRuleCappingToTicketDAO()
		{
			return new RuleCappingToTicketDAO();
		}

		/// <summary>Creates a new RulePeriodDAO object</summary>
		/// <returns>the new DAO object ready to use for RulePeriod Entities</returns>
		public static RulePeriodDAO CreateRulePeriodDAO()
		{
			return new RulePeriodDAO();
		}

		/// <summary>Creates a new RuleTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleType Entities</returns>
		public static RuleTypeDAO CreateRuleTypeDAO()
		{
			return new RuleTypeDAO();
		}

		/// <summary>Creates a new SalutationDAO object</summary>
		/// <returns>the new DAO object ready to use for Salutation Entities</returns>
		public static SalutationDAO CreateSalutationDAO()
		{
			return new SalutationDAO();
		}

		/// <summary>Creates a new ServiceAllocationDAO object</summary>
		/// <returns>the new DAO object ready to use for ServiceAllocation Entities</returns>
		public static ServiceAllocationDAO CreateServiceAllocationDAO()
		{
			return new ServiceAllocationDAO();
		}

		/// <summary>Creates a new ServiceIdToCardDAO object</summary>
		/// <returns>the new DAO object ready to use for ServiceIdToCard Entities</returns>
		public static ServiceIdToCardDAO CreateServiceIdToCardDAO()
		{
			return new ServiceIdToCardDAO();
		}

		/// <summary>Creates a new ServicePercentageDAO object</summary>
		/// <returns>the new DAO object ready to use for ServicePercentage Entities</returns>
		public static ServicePercentageDAO CreateServicePercentageDAO()
		{
			return new ServicePercentageDAO();
		}

		/// <summary>Creates a new ShiftDAO object</summary>
		/// <returns>the new DAO object ready to use for Shift Entities</returns>
		public static ShiftDAO CreateShiftDAO()
		{
			return new ShiftDAO();
		}

		/// <summary>Creates a new ShiftInventoryDAO object</summary>
		/// <returns>the new DAO object ready to use for ShiftInventory Entities</returns>
		public static ShiftInventoryDAO CreateShiftInventoryDAO()
		{
			return new ShiftInventoryDAO();
		}

		/// <summary>Creates a new ShiftStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ShiftState Entities</returns>
		public static ShiftStateDAO CreateShiftStateDAO()
		{
			return new ShiftStateDAO();
		}

		/// <summary>Creates a new ShortDistanceDAO object</summary>
		/// <returns>the new DAO object ready to use for ShortDistance Entities</returns>
		public static ShortDistanceDAO CreateShortDistanceDAO()
		{
			return new ShortDistanceDAO();
		}

		/// <summary>Creates a new SpecialReceiptDAO object</summary>
		/// <returns>the new DAO object ready to use for SpecialReceipt Entities</returns>
		public static SpecialReceiptDAO CreateSpecialReceiptDAO()
		{
			return new SpecialReceiptDAO();
		}

		/// <summary>Creates a new StopDAO object</summary>
		/// <returns>the new DAO object ready to use for Stop Entities</returns>
		public static StopDAO CreateStopDAO()
		{
			return new StopDAO();
		}

		/// <summary>Creates a new SystemFieldDAO object</summary>
		/// <returns>the new DAO object ready to use for SystemField Entities</returns>
		public static SystemFieldDAO CreateSystemFieldDAO()
		{
			return new SystemFieldDAO();
		}

		/// <summary>Creates a new SystemFieldBarcodeLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for SystemFieldBarcodeLayoutObject Entities</returns>
		public static SystemFieldBarcodeLayoutObjectDAO CreateSystemFieldBarcodeLayoutObjectDAO()
		{
			return new SystemFieldBarcodeLayoutObjectDAO();
		}

		/// <summary>Creates a new SystemFieldDynamicGraphicLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for SystemFieldDynamicGraphicLayoutObject Entities</returns>
		public static SystemFieldDynamicGraphicLayoutObjectDAO CreateSystemFieldDynamicGraphicLayoutObjectDAO()
		{
			return new SystemFieldDynamicGraphicLayoutObjectDAO();
		}

		/// <summary>Creates a new SystemFieldTextLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for SystemFieldTextLayoutObject Entities</returns>
		public static SystemFieldTextLayoutObjectDAO CreateSystemFieldTextLayoutObjectDAO()
		{
			return new SystemFieldTextLayoutObjectDAO();
		}

		/// <summary>Creates a new SystemTextDAO object</summary>
		/// <returns>the new DAO object ready to use for SystemText Entities</returns>
		public static SystemTextDAO CreateSystemTextDAO()
		{
			return new SystemTextDAO();
		}

		/// <summary>Creates a new TariffDAO object</summary>
		/// <returns>the new DAO object ready to use for Tariff Entities</returns>
		public static TariffDAO CreateTariffDAO()
		{
			return new TariffDAO();
		}

		/// <summary>Creates a new TariffParameterDAO object</summary>
		/// <returns>the new DAO object ready to use for TariffParameter Entities</returns>
		public static TariffParameterDAO CreateTariffParameterDAO()
		{
			return new TariffParameterDAO();
		}

		/// <summary>Creates a new TariffReleaseDAO object</summary>
		/// <returns>the new DAO object ready to use for TariffRelease Entities</returns>
		public static TariffReleaseDAO CreateTariffReleaseDAO()
		{
			return new TariffReleaseDAO();
		}

		/// <summary>Creates a new TemporalTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TemporalType Entities</returns>
		public static TemporalTypeDAO CreateTemporalTypeDAO()
		{
			return new TemporalTypeDAO();
		}

		/// <summary>Creates a new TicketDAO object</summary>
		/// <returns>the new DAO object ready to use for Ticket Entities</returns>
		public static TicketDAO CreateTicketDAO()
		{
			return new TicketDAO();
		}

		/// <summary>Creates a new TicketCancellationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketCancellationType Entities</returns>
		public static TicketCancellationTypeDAO CreateTicketCancellationTypeDAO()
		{
			return new TicketCancellationTypeDAO();
		}

		/// <summary>Creates a new TicketCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketCategory Entities</returns>
		public static TicketCategoryDAO CreateTicketCategoryDAO()
		{
			return new TicketCategoryDAO();
		}

		/// <summary>Creates a new TicketDayTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketDayType Entities</returns>
		public static TicketDayTypeDAO CreateTicketDayTypeDAO()
		{
			return new TicketDayTypeDAO();
		}

		/// <summary>Creates a new TicketDeviceClassDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketDeviceClass Entities</returns>
		public static TicketDeviceClassDAO CreateTicketDeviceClassDAO()
		{
			return new TicketDeviceClassDAO();
		}

		/// <summary>Creates a new TicketDeviceClassOutputDeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketDeviceClassOutputDevice Entities</returns>
		public static TicketDeviceClassOutputDeviceDAO CreateTicketDeviceClassOutputDeviceDAO()
		{
			return new TicketDeviceClassOutputDeviceDAO();
		}

		/// <summary>Creates a new TicketDeviceClassPaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketDeviceClassPaymentMethod Entities</returns>
		public static TicketDeviceClassPaymentMethodDAO CreateTicketDeviceClassPaymentMethodDAO()
		{
			return new TicketDeviceClassPaymentMethodDAO();
		}

		/// <summary>Creates a new TicketDevicePaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketDevicePaymentMethod Entities</returns>
		public static TicketDevicePaymentMethodDAO CreateTicketDevicePaymentMethodDAO()
		{
			return new TicketDevicePaymentMethodDAO();
		}

		/// <summary>Creates a new TicketGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketGroup Entities</returns>
		public static TicketGroupDAO CreateTicketGroupDAO()
		{
			return new TicketGroupDAO();
		}

		/// <summary>Creates a new TicketOrganizationDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketOrganization Entities</returns>
		public static TicketOrganizationDAO CreateTicketOrganizationDAO()
		{
			return new TicketOrganizationDAO();
		}

		/// <summary>Creates a new TicketOutputdeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketOutputdevice Entities</returns>
		public static TicketOutputdeviceDAO CreateTicketOutputdeviceDAO()
		{
			return new TicketOutputdeviceDAO();
		}

		/// <summary>Creates a new TicketPaymentIntervalDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketPaymentInterval Entities</returns>
		public static TicketPaymentIntervalDAO CreateTicketPaymentIntervalDAO()
		{
			return new TicketPaymentIntervalDAO();
		}

		/// <summary>Creates a new TicketPhysicalCardTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketPhysicalCardType Entities</returns>
		public static TicketPhysicalCardTypeDAO CreateTicketPhysicalCardTypeDAO()
		{
			return new TicketPhysicalCardTypeDAO();
		}

		/// <summary>Creates a new TicketSelectionModeDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketSelectionMode Entities</returns>
		public static TicketSelectionModeDAO CreateTicketSelectionModeDAO()
		{
			return new TicketSelectionModeDAO();
		}

		/// <summary>Creates a new TicketServicesPermittedDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketServicesPermitted Entities</returns>
		public static TicketServicesPermittedDAO CreateTicketServicesPermittedDAO()
		{
			return new TicketServicesPermittedDAO();
		}

		/// <summary>Creates a new TicketToGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketToGroup Entities</returns>
		public static TicketToGroupDAO CreateTicketToGroupDAO()
		{
			return new TicketToGroupDAO();
		}

		/// <summary>Creates a new TicketTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketType Entities</returns>
		public static TicketTypeDAO CreateTicketTypeDAO()
		{
			return new TicketTypeDAO();
		}

		/// <summary>Creates a new TicketVendingClientDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketVendingClient Entities</returns>
		public static TicketVendingClientDAO CreateTicketVendingClientDAO()
		{
			return new TicketVendingClientDAO();
		}

		/// <summary>Creates a new TransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for Transaction Entities</returns>
		public static TransactionDAO CreateTransactionDAO()
		{
			return new TransactionDAO();
		}

		/// <summary>Creates a new TransactionAdditionalDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionAdditional Entities</returns>
		public static TransactionAdditionalDAO CreateTransactionAdditionalDAO()
		{
			return new TransactionAdditionalDAO();
		}

		/// <summary>Creates a new TransactionBackupDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionBackup Entities</returns>
		public static TransactionBackupDAO CreateTransactionBackupDAO()
		{
			return new TransactionBackupDAO();
		}

		/// <summary>Creates a new TransactionExtensionDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionExtension Entities</returns>
		public static TransactionExtensionDAO CreateTransactionExtensionDAO()
		{
			return new TransactionExtensionDAO();
		}

		/// <summary>Creates a new TranslationDAO object</summary>
		/// <returns>the new DAO object ready to use for Translation Entities</returns>
		public static TranslationDAO CreateTranslationDAO()
		{
			return new TranslationDAO();
		}

		/// <summary>Creates a new TypeOfCardDAO object</summary>
		/// <returns>the new DAO object ready to use for TypeOfCard Entities</returns>
		public static TypeOfCardDAO CreateTypeOfCardDAO()
		{
			return new TypeOfCardDAO();
		}

		/// <summary>Creates a new TypeOfUnitDAO object</summary>
		/// <returns>the new DAO object ready to use for TypeOfUnit Entities</returns>
		public static TypeOfUnitDAO CreateTypeOfUnitDAO()
		{
			return new TypeOfUnitDAO();
		}

		/// <summary>Creates a new UnitDAO object</summary>
		/// <returns>the new DAO object ready to use for Unit Entities</returns>
		public static UnitDAO CreateUnitDAO()
		{
			return new UnitDAO();
		}

		/// <summary>Creates a new UserConfigDAO object</summary>
		/// <returns>the new DAO object ready to use for UserConfig Entities</returns>
		public static UserConfigDAO CreateUserConfigDAO()
		{
			return new UserConfigDAO();
		}

		/// <summary>Creates a new UserGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for UserGroup Entities</returns>
		public static UserGroupDAO CreateUserGroupDAO()
		{
			return new UserGroupDAO();
		}

		/// <summary>Creates a new UserGroupRightDAO object</summary>
		/// <returns>the new DAO object ready to use for UserGroupRight Entities</returns>
		public static UserGroupRightDAO CreateUserGroupRightDAO()
		{
			return new UserGroupRightDAO();
		}

		/// <summary>Creates a new UserIsMemberDAO object</summary>
		/// <returns>the new DAO object ready to use for UserIsMember Entities</returns>
		public static UserIsMemberDAO CreateUserIsMemberDAO()
		{
			return new UserIsMemberDAO();
		}

		/// <summary>Creates a new UserKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for UserKey Entities</returns>
		public static UserKeyDAO CreateUserKeyDAO()
		{
			return new UserKeyDAO();
		}

		/// <summary>Creates a new UserLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for UserLanguage Entities</returns>
		public static UserLanguageDAO CreateUserLanguageDAO()
		{
			return new UserLanguageDAO();
		}

		/// <summary>Creates a new UserListDAO object</summary>
		/// <returns>the new DAO object ready to use for UserList Entities</returns>
		public static UserListDAO CreateUserListDAO()
		{
			return new UserListDAO();
		}

		/// <summary>Creates a new UserResourceDAO object</summary>
		/// <returns>the new DAO object ready to use for UserResource Entities</returns>
		public static UserResourceDAO CreateUserResourceDAO()
		{
			return new UserResourceDAO();
		}

		/// <summary>Creates a new UserStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for UserStatus Entities</returns>
		public static UserStatusDAO CreateUserStatusDAO()
		{
			return new UserStatusDAO();
		}

		/// <summary>Creates a new UserToWorkstationDAO object</summary>
		/// <returns>the new DAO object ready to use for UserToWorkstation Entities</returns>
		public static UserToWorkstationDAO CreateUserToWorkstationDAO()
		{
			return new UserToWorkstationDAO();
		}

		/// <summary>Creates a new ValidationTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationTransaction Entities</returns>
		public static ValidationTransactionDAO CreateValidationTransactionDAO()
		{
			return new ValidationTransactionDAO();
		}

		/// <summary>Creates a new VarioAddressDAO object</summary>
		/// <returns>the new DAO object ready to use for VarioAddress Entities</returns>
		public static VarioAddressDAO CreateVarioAddressDAO()
		{
			return new VarioAddressDAO();
		}

		/// <summary>Creates a new VarioSettlementDAO object</summary>
		/// <returns>the new DAO object ready to use for VarioSettlement Entities</returns>
		public static VarioSettlementDAO CreateVarioSettlementDAO()
		{
			return new VarioSettlementDAO();
		}

		/// <summary>Creates a new VarioTypeOfSettlementDAO object</summary>
		/// <returns>the new DAO object ready to use for VarioTypeOfSettlement Entities</returns>
		public static VarioTypeOfSettlementDAO CreateVarioTypeOfSettlementDAO()
		{
			return new VarioTypeOfSettlementDAO();
		}

		/// <summary>Creates a new VdvKeyInfoDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvKeyInfo Entities</returns>
		public static VdvKeyInfoDAO CreateVdvKeyInfoDAO()
		{
			return new VdvKeyInfoDAO();
		}

		/// <summary>Creates a new VdvKeySetDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvKeySet Entities</returns>
		public static VdvKeySetDAO CreateVdvKeySetDAO()
		{
			return new VdvKeySetDAO();
		}

		/// <summary>Creates a new VdvLayoutDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvLayout Entities</returns>
		public static VdvLayoutDAO CreateVdvLayoutDAO()
		{
			return new VdvLayoutDAO();
		}

		/// <summary>Creates a new VdvLayoutObjectDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvLayoutObject Entities</returns>
		public static VdvLayoutObjectDAO CreateVdvLayoutObjectDAO()
		{
			return new VdvLayoutObjectDAO();
		}

		/// <summary>Creates a new VdvLoadKeyMessageDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvLoadKeyMessage Entities</returns>
		public static VdvLoadKeyMessageDAO CreateVdvLoadKeyMessageDAO()
		{
			return new VdvLoadKeyMessageDAO();
		}

		/// <summary>Creates a new VdvProductDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvProduct Entities</returns>
		public static VdvProductDAO CreateVdvProductDAO()
		{
			return new VdvProductDAO();
		}

		/// <summary>Creates a new VdvSamStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvSamStatus Entities</returns>
		public static VdvSamStatusDAO CreateVdvSamStatusDAO()
		{
			return new VdvSamStatusDAO();
		}

		/// <summary>Creates a new VdvTagDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvTag Entities</returns>
		public static VdvTagDAO CreateVdvTagDAO()
		{
			return new VdvTagDAO();
		}

		/// <summary>Creates a new VdvTerminalDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvTerminal Entities</returns>
		public static VdvTerminalDAO CreateVdvTerminalDAO()
		{
			return new VdvTerminalDAO();
		}

		/// <summary>Creates a new VdvTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvType Entities</returns>
		public static VdvTypeDAO CreateVdvTypeDAO()
		{
			return new VdvTypeDAO();
		}

		/// <summary>Creates a new WorkstationDAO object</summary>
		/// <returns>the new DAO object ready to use for Workstation Entities</returns>
		public static WorkstationDAO CreateWorkstationDAO()
		{
			return new WorkstationDAO();
		}

		/// <summary>Creates a new AccountingCancelRecTapCmlSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingCancelRecTapCmlSettled Entities</returns>
		public static AccountingCancelRecTapCmlSettledDAO CreateAccountingCancelRecTapCmlSettledDAO()
		{
			return new AccountingCancelRecTapCmlSettledDAO();
		}

		/// <summary>Creates a new AccountingCancelRecTapTabSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingCancelRecTapTabSettled Entities</returns>
		public static AccountingCancelRecTapTabSettledDAO CreateAccountingCancelRecTapTabSettledDAO()
		{
			return new AccountingCancelRecTapTabSettledDAO();
		}

		/// <summary>Creates a new AccountingMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingMethod Entities</returns>
		public static AccountingMethodDAO CreateAccountingMethodDAO()
		{
			return new AccountingMethodDAO();
		}

		/// <summary>Creates a new AccountingModeDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingMode Entities</returns>
		public static AccountingModeDAO CreateAccountingModeDAO()
		{
			return new AccountingModeDAO();
		}

		/// <summary>Creates a new AccountingRecLoadUseCmlSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecLoadUseCmlSettled Entities</returns>
		public static AccountingRecLoadUseCmlSettledDAO CreateAccountingRecLoadUseCmlSettledDAO()
		{
			return new AccountingRecLoadUseCmlSettledDAO();
		}

		/// <summary>Creates a new AccountingRecLoadUseTabSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecLoadUseTabSettled Entities</returns>
		public static AccountingRecLoadUseTabSettledDAO CreateAccountingRecLoadUseTabSettledDAO()
		{
			return new AccountingRecLoadUseTabSettledDAO();
		}

		/// <summary>Creates a new AccountingRecognizedLoadAndUseDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedLoadAndUse Entities</returns>
		public static AccountingRecognizedLoadAndUseDAO CreateAccountingRecognizedLoadAndUseDAO()
		{
			return new AccountingRecognizedLoadAndUseDAO();
		}

		/// <summary>Creates a new AccountingRecognizedPaymentDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedPayment Entities</returns>
		public static AccountingRecognizedPaymentDAO CreateAccountingRecognizedPaymentDAO()
		{
			return new AccountingRecognizedPaymentDAO();
		}

		/// <summary>Creates a new AccountingRecognizedTapDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedTap Entities</returns>
		public static AccountingRecognizedTapDAO CreateAccountingRecognizedTapDAO()
		{
			return new AccountingRecognizedTapDAO();
		}

		/// <summary>Creates a new AccountingRecognizedTapCancellationDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedTapCancellation Entities</returns>
		public static AccountingRecognizedTapCancellationDAO CreateAccountingRecognizedTapCancellationDAO()
		{
			return new AccountingRecognizedTapCancellationDAO();
		}

		/// <summary>Creates a new AccountingRecognizedTapCmlSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedTapCmlSettled Entities</returns>
		public static AccountingRecognizedTapCmlSettledDAO CreateAccountingRecognizedTapCmlSettledDAO()
		{
			return new AccountingRecognizedTapCmlSettledDAO();
		}

		/// <summary>Creates a new AccountingRecognizedTapTabSettledDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingRecognizedTapTabSettled Entities</returns>
		public static AccountingRecognizedTapTabSettledDAO CreateAccountingRecognizedTapTabSettledDAO()
		{
			return new AccountingRecognizedTapTabSettledDAO();
		}

		/// <summary>Creates a new AccountingReconciledPaymentDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountingReconciledPayment Entities</returns>
		public static AccountingReconciledPaymentDAO CreateAccountingReconciledPaymentDAO()
		{
			return new AccountingReconciledPaymentDAO();
		}

		/// <summary>Creates a new AccountMessageSettingDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountMessageSetting Entities</returns>
		public static AccountMessageSettingDAO CreateAccountMessageSettingDAO()
		{
			return new AccountMessageSettingDAO();
		}

		/// <summary>Creates a new AdditionalDataDAO object</summary>
		/// <returns>the new DAO object ready to use for AdditionalData Entities</returns>
		public static AdditionalDataDAO CreateAdditionalDataDAO()
		{
			return new AdditionalDataDAO();
		}

		/// <summary>Creates a new AddressDAO object</summary>
		/// <returns>the new DAO object ready to use for Address Entities</returns>
		public static AddressDAO CreateAddressDAO()
		{
			return new AddressDAO();
		}

		/// <summary>Creates a new AddressBookEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for AddressBookEntry Entities</returns>
		public static AddressBookEntryDAO CreateAddressBookEntryDAO()
		{
			return new AddressBookEntryDAO();
		}

		/// <summary>Creates a new AddressBookEntryTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for AddressBookEntryType Entities</returns>
		public static AddressBookEntryTypeDAO CreateAddressBookEntryTypeDAO()
		{
			return new AddressBookEntryTypeDAO();
		}

		/// <summary>Creates a new AddressTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for AddressType Entities</returns>
		public static AddressTypeDAO CreateAddressTypeDAO()
		{
			return new AddressTypeDAO();
		}

		/// <summary>Creates a new AggregationFunctionDAO object</summary>
		/// <returns>the new DAO object ready to use for AggregationFunction Entities</returns>
		public static AggregationFunctionDAO CreateAggregationFunctionDAO()
		{
			return new AggregationFunctionDAO();
		}

		/// <summary>Creates a new AggregationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for AggregationType Entities</returns>
		public static AggregationTypeDAO CreateAggregationTypeDAO()
		{
			return new AggregationTypeDAO();
		}

		/// <summary>Creates a new ApportionDAO object</summary>
		/// <returns>the new DAO object ready to use for Apportion Entities</returns>
		public static ApportionDAO CreateApportionDAO()
		{
			return new ApportionDAO();
		}

		/// <summary>Creates a new ApportionPassRevenueRecognitionDAO object</summary>
		/// <returns>the new DAO object ready to use for ApportionPassRevenueRecognition Entities</returns>
		public static ApportionPassRevenueRecognitionDAO CreateApportionPassRevenueRecognitionDAO()
		{
			return new ApportionPassRevenueRecognitionDAO();
		}

		/// <summary>Creates a new ArchiveStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ArchiveState Entities</returns>
		public static ArchiveStateDAO CreateArchiveStateDAO()
		{
			return new ArchiveStateDAO();
		}

		/// <summary>Creates a new AttributeToMobilityProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for AttributeToMobilityProvider Entities</returns>
		public static AttributeToMobilityProviderDAO CreateAttributeToMobilityProviderDAO()
		{
			return new AttributeToMobilityProviderDAO();
		}

		/// <summary>Creates a new AuditRegisterDAO object</summary>
		/// <returns>the new DAO object ready to use for AuditRegister Entities</returns>
		public static AuditRegisterDAO CreateAuditRegisterDAO()
		{
			return new AuditRegisterDAO();
		}

		/// <summary>Creates a new AuditRegisterValueDAO object</summary>
		/// <returns>the new DAO object ready to use for AuditRegisterValue Entities</returns>
		public static AuditRegisterValueDAO CreateAuditRegisterValueDAO()
		{
			return new AuditRegisterValueDAO();
		}

		/// <summary>Creates a new AuditRegisterValueTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for AuditRegisterValueType Entities</returns>
		public static AuditRegisterValueTypeDAO CreateAuditRegisterValueTypeDAO()
		{
			return new AuditRegisterValueTypeDAO();
		}

		/// <summary>Creates a new AutoloadSettingDAO object</summary>
		/// <returns>the new DAO object ready to use for AutoloadSetting Entities</returns>
		public static AutoloadSettingDAO CreateAutoloadSettingDAO()
		{
			return new AutoloadSettingDAO();
		}

		/// <summary>Creates a new AutoloadToPaymentOptionDAO object</summary>
		/// <returns>the new DAO object ready to use for AutoloadToPaymentOption Entities</returns>
		public static AutoloadToPaymentOptionDAO CreateAutoloadToPaymentOptionDAO()
		{
			return new AutoloadToPaymentOptionDAO();
		}

		/// <summary>Creates a new BadCardDAO object</summary>
		/// <returns>the new DAO object ready to use for BadCard Entities</returns>
		public static BadCardDAO CreateBadCardDAO()
		{
			return new BadCardDAO();
		}

		/// <summary>Creates a new BadCheckDAO object</summary>
		/// <returns>the new DAO object ready to use for BadCheck Entities</returns>
		public static BadCheckDAO CreateBadCheckDAO()
		{
			return new BadCheckDAO();
		}

		/// <summary>Creates a new BankConnectionDataDAO object</summary>
		/// <returns>the new DAO object ready to use for BankConnectionData Entities</returns>
		public static BankConnectionDataDAO CreateBankConnectionDataDAO()
		{
			return new BankConnectionDataDAO();
		}

		/// <summary>Creates a new BankStatementDAO object</summary>
		/// <returns>the new DAO object ready to use for BankStatement Entities</returns>
		public static BankStatementDAO CreateBankStatementDAO()
		{
			return new BankStatementDAO();
		}

		/// <summary>Creates a new BankStatementRecordTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for BankStatementRecordType Entities</returns>
		public static BankStatementRecordTypeDAO CreateBankStatementRecordTypeDAO()
		{
			return new BankStatementRecordTypeDAO();
		}

		/// <summary>Creates a new BankStatementStateDAO object</summary>
		/// <returns>the new DAO object ready to use for BankStatementState Entities</returns>
		public static BankStatementStateDAO CreateBankStatementStateDAO()
		{
			return new BankStatementStateDAO();
		}

		/// <summary>Creates a new BankStatementVerificationDAO object</summary>
		/// <returns>the new DAO object ready to use for BankStatementVerification Entities</returns>
		public static BankStatementVerificationDAO CreateBankStatementVerificationDAO()
		{
			return new BankStatementVerificationDAO();
		}

		/// <summary>Creates a new BookingDAO object</summary>
		/// <returns>the new DAO object ready to use for Booking Entities</returns>
		public static BookingDAO CreateBookingDAO()
		{
			return new BookingDAO();
		}

		/// <summary>Creates a new BookingItemDAO object</summary>
		/// <returns>the new DAO object ready to use for BookingItem Entities</returns>
		public static BookingItemDAO CreateBookingItemDAO()
		{
			return new BookingItemDAO();
		}

		/// <summary>Creates a new BookingItemPaymentDAO object</summary>
		/// <returns>the new DAO object ready to use for BookingItemPayment Entities</returns>
		public static BookingItemPaymentDAO CreateBookingItemPaymentDAO()
		{
			return new BookingItemPaymentDAO();
		}

		/// <summary>Creates a new BookingItemPaymentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for BookingItemPaymentType Entities</returns>
		public static BookingItemPaymentTypeDAO CreateBookingItemPaymentTypeDAO()
		{
			return new BookingItemPaymentTypeDAO();
		}

		/// <summary>Creates a new BookingTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for BookingType Entities</returns>
		public static BookingTypeDAO CreateBookingTypeDAO()
		{
			return new BookingTypeDAO();
		}

		/// <summary>Creates a new CappingJournalDAO object</summary>
		/// <returns>the new DAO object ready to use for CappingJournal Entities</returns>
		public static CappingJournalDAO CreateCappingJournalDAO()
		{
			return new CappingJournalDAO();
		}

		/// <summary>Creates a new CappingStateDAO object</summary>
		/// <returns>the new DAO object ready to use for CappingState Entities</returns>
		public static CappingStateDAO CreateCappingStateDAO()
		{
			return new CappingStateDAO();
		}

		/// <summary>Creates a new CardDAO object</summary>
		/// <returns>the new DAO object ready to use for Card Entities</returns>
		public static CardDAO CreateCardDAO()
		{
			return new CardDAO();
		}

		/// <summary>Creates a new CardBodyTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardBodyType Entities</returns>
		public static CardBodyTypeDAO CreateCardBodyTypeDAO()
		{
			return new CardBodyTypeDAO();
		}

		/// <summary>Creates a new CardDormancyDAO object</summary>
		/// <returns>the new DAO object ready to use for CardDormancy Entities</returns>
		public static CardDormancyDAO CreateCardDormancyDAO()
		{
			return new CardDormancyDAO();
		}

		/// <summary>Creates a new CardEventDAO object</summary>
		/// <returns>the new DAO object ready to use for CardEvent Entities</returns>
		public static CardEventDAO CreateCardEventDAO()
		{
			return new CardEventDAO();
		}

		/// <summary>Creates a new CardEventTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardEventType Entities</returns>
		public static CardEventTypeDAO CreateCardEventTypeDAO()
		{
			return new CardEventTypeDAO();
		}

		/// <summary>Creates a new CardFulfillmentStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for CardFulfillmentStatus Entities</returns>
		public static CardFulfillmentStatusDAO CreateCardFulfillmentStatusDAO()
		{
			return new CardFulfillmentStatusDAO();
		}

		/// <summary>Creates a new CardHolderDAO object</summary>
		/// <returns>the new DAO object ready to use for CardHolder Entities</returns>
		public static CardHolderDAO CreateCardHolderDAO()
		{
			return new CardHolderDAO();
		}

		/// <summary>Creates a new CardHolderOrganizationDAO object</summary>
		/// <returns>the new DAO object ready to use for CardHolderOrganization Entities</returns>
		public static CardHolderOrganizationDAO CreateCardHolderOrganizationDAO()
		{
			return new CardHolderOrganizationDAO();
		}

		/// <summary>Creates a new CardKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for CardKey Entities</returns>
		public static CardKeyDAO CreateCardKeyDAO()
		{
			return new CardKeyDAO();
		}

		/// <summary>Creates a new CardLiabilityDAO object</summary>
		/// <returns>the new DAO object ready to use for CardLiability Entities</returns>
		public static CardLiabilityDAO CreateCardLiabilityDAO()
		{
			return new CardLiabilityDAO();
		}

		/// <summary>Creates a new CardLinkDAO object</summary>
		/// <returns>the new DAO object ready to use for CardLink Entities</returns>
		public static CardLinkDAO CreateCardLinkDAO()
		{
			return new CardLinkDAO();
		}

		/// <summary>Creates a new CardOrderDetailDAO object</summary>
		/// <returns>the new DAO object ready to use for CardOrderDetail Entities</returns>
		public static CardOrderDetailDAO CreateCardOrderDetailDAO()
		{
			return new CardOrderDetailDAO();
		}

		/// <summary>Creates a new CardPhysicalDetailDAO object</summary>
		/// <returns>the new DAO object ready to use for CardPhysicalDetail Entities</returns>
		public static CardPhysicalDetailDAO CreateCardPhysicalDetailDAO()
		{
			return new CardPhysicalDetailDAO();
		}

		/// <summary>Creates a new CardPrintingTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CardPrintingType Entities</returns>
		public static CardPrintingTypeDAO CreateCardPrintingTypeDAO()
		{
			return new CardPrintingTypeDAO();
		}

		/// <summary>Creates a new CardStockTransferDAO object</summary>
		/// <returns>the new DAO object ready to use for CardStockTransfer Entities</returns>
		public static CardStockTransferDAO CreateCardStockTransferDAO()
		{
			return new CardStockTransferDAO();
		}

		/// <summary>Creates a new CardToContractDAO object</summary>
		/// <returns>the new DAO object ready to use for CardToContract Entities</returns>
		public static CardToContractDAO CreateCardToContractDAO()
		{
			return new CardToContractDAO();
		}

		/// <summary>Creates a new CardToRuleViolationDAO object</summary>
		/// <returns>the new DAO object ready to use for CardToRuleViolation Entities</returns>
		public static CardToRuleViolationDAO CreateCardToRuleViolationDAO()
		{
			return new CardToRuleViolationDAO();
		}

		/// <summary>Creates a new CardWorkItemDAO object</summary>
		/// <returns>the new DAO object ready to use for CardWorkItem Entities</returns>
		public static CardWorkItemDAO CreateCardWorkItemDAO()
		{
			return new CardWorkItemDAO();
		}

		/// <summary>Creates a new CertificateDAO object</summary>
		/// <returns>the new DAO object ready to use for Certificate Entities</returns>
		public static CertificateDAO CreateCertificateDAO()
		{
			return new CertificateDAO();
		}

		/// <summary>Creates a new CertificateFormatDAO object</summary>
		/// <returns>the new DAO object ready to use for CertificateFormat Entities</returns>
		public static CertificateFormatDAO CreateCertificateFormatDAO()
		{
			return new CertificateFormatDAO();
		}

		/// <summary>Creates a new CertificatePurposeDAO object</summary>
		/// <returns>the new DAO object ready to use for CertificatePurpose Entities</returns>
		public static CertificatePurposeDAO CreateCertificatePurposeDAO()
		{
			return new CertificatePurposeDAO();
		}

		/// <summary>Creates a new CertificateTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CertificateType Entities</returns>
		public static CertificateTypeDAO CreateCertificateTypeDAO()
		{
			return new CertificateTypeDAO();
		}

		/// <summary>Creates a new ClaimDAO object</summary>
		/// <returns>the new DAO object ready to use for Claim Entities</returns>
		public static ClaimDAO CreateClaimDAO()
		{
			return new ClaimDAO();
		}

		/// <summary>Creates a new ClientFilterTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientFilterType Entities</returns>
		public static ClientFilterTypeDAO CreateClientFilterTypeDAO()
		{
			return new ClientFilterTypeDAO();
		}

		/// <summary>Creates a new CloseoutPeriodDAO object</summary>
		/// <returns>the new DAO object ready to use for CloseoutPeriod Entities</returns>
		public static CloseoutPeriodDAO CreateCloseoutPeriodDAO()
		{
			return new CloseoutPeriodDAO();
		}

		/// <summary>Creates a new CloseoutStateDAO object</summary>
		/// <returns>the new DAO object ready to use for CloseoutState Entities</returns>
		public static CloseoutStateDAO CreateCloseoutStateDAO()
		{
			return new CloseoutStateDAO();
		}

		/// <summary>Creates a new CloseoutTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CloseoutType Entities</returns>
		public static CloseoutTypeDAO CreateCloseoutTypeDAO()
		{
			return new CloseoutTypeDAO();
		}

		/// <summary>Creates a new CommentDAO object</summary>
		/// <returns>the new DAO object ready to use for Comment Entities</returns>
		public static CommentDAO CreateCommentDAO()
		{
			return new CommentDAO();
		}

		/// <summary>Creates a new CommentPriorityDAO object</summary>
		/// <returns>the new DAO object ready to use for CommentPriority Entities</returns>
		public static CommentPriorityDAO CreateCommentPriorityDAO()
		{
			return new CommentPriorityDAO();
		}

		/// <summary>Creates a new CommentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CommentType Entities</returns>
		public static CommentTypeDAO CreateCommentTypeDAO()
		{
			return new CommentTypeDAO();
		}

		/// <summary>Creates a new ConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for Configuration Entities</returns>
		public static ConfigurationDAO CreateConfigurationDAO()
		{
			return new ConfigurationDAO();
		}

		/// <summary>Creates a new ConfigurationDataTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ConfigurationDataType Entities</returns>
		public static ConfigurationDataTypeDAO CreateConfigurationDataTypeDAO()
		{
			return new ConfigurationDataTypeDAO();
		}

		/// <summary>Creates a new ConfigurationDefinitionDAO object</summary>
		/// <returns>the new DAO object ready to use for ConfigurationDefinition Entities</returns>
		public static ConfigurationDefinitionDAO CreateConfigurationDefinitionDAO()
		{
			return new ConfigurationDefinitionDAO();
		}

		/// <summary>Creates a new ConfigurationOverwriteDAO object</summary>
		/// <returns>the new DAO object ready to use for ConfigurationOverwrite Entities</returns>
		public static ConfigurationOverwriteDAO CreateConfigurationOverwriteDAO()
		{
			return new ConfigurationOverwriteDAO();
		}

		/// <summary>Creates a new ContentItemDAO object</summary>
		/// <returns>the new DAO object ready to use for ContentItem Entities</returns>
		public static ContentItemDAO CreateContentItemDAO()
		{
			return new ContentItemDAO();
		}

		/// <summary>Creates a new ContentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ContentType Entities</returns>
		public static ContentTypeDAO CreateContentTypeDAO()
		{
			return new ContentTypeDAO();
		}

		/// <summary>Creates a new ContractDAO object</summary>
		/// <returns>the new DAO object ready to use for Contract Entities</returns>
		public static ContractDAO CreateContractDAO()
		{
			return new ContractDAO();
		}

		/// <summary>Creates a new ContractAddressDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractAddress Entities</returns>
		public static ContractAddressDAO CreateContractAddressDAO()
		{
			return new ContractAddressDAO();
		}

		/// <summary>Creates a new ContractDetailsDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractDetails Entities</returns>
		public static ContractDetailsDAO CreateContractDetailsDAO()
		{
			return new ContractDetailsDAO();
		}

		/// <summary>Creates a new ContractHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractHistory Entities</returns>
		public static ContractHistoryDAO CreateContractHistoryDAO()
		{
			return new ContractHistoryDAO();
		}

		/// <summary>Creates a new ContractLinkDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractLink Entities</returns>
		public static ContractLinkDAO CreateContractLinkDAO()
		{
			return new ContractLinkDAO();
		}

		/// <summary>Creates a new ContractStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractState Entities</returns>
		public static ContractStateDAO CreateContractStateDAO()
		{
			return new ContractStateDAO();
		}

		/// <summary>Creates a new ContractTerminationDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractTermination Entities</returns>
		public static ContractTerminationDAO CreateContractTerminationDAO()
		{
			return new ContractTerminationDAO();
		}

		/// <summary>Creates a new ContractTerminationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractTerminationType Entities</returns>
		public static ContractTerminationTypeDAO CreateContractTerminationTypeDAO()
		{
			return new ContractTerminationTypeDAO();
		}

		/// <summary>Creates a new ContractToPaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractToPaymentMethod Entities</returns>
		public static ContractToPaymentMethodDAO CreateContractToPaymentMethodDAO()
		{
			return new ContractToPaymentMethodDAO();
		}

		/// <summary>Creates a new ContractToProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractToProvider Entities</returns>
		public static ContractToProviderDAO CreateContractToProviderDAO()
		{
			return new ContractToProviderDAO();
		}

		/// <summary>Creates a new ContractTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractType Entities</returns>
		public static ContractTypeDAO CreateContractTypeDAO()
		{
			return new ContractTypeDAO();
		}

		/// <summary>Creates a new ContractWorkItemDAO object</summary>
		/// <returns>the new DAO object ready to use for ContractWorkItem Entities</returns>
		public static ContractWorkItemDAO CreateContractWorkItemDAO()
		{
			return new ContractWorkItemDAO();
		}

		/// <summary>Creates a new CoordinateDAO object</summary>
		/// <returns>the new DAO object ready to use for Coordinate Entities</returns>
		public static CoordinateDAO CreateCoordinateDAO()
		{
			return new CoordinateDAO();
		}

		/// <summary>Creates a new CountryDAO object</summary>
		/// <returns>the new DAO object ready to use for Country Entities</returns>
		public static CountryDAO CreateCountryDAO()
		{
			return new CountryDAO();
		}

		/// <summary>Creates a new CreditCardAuthorizationDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditCardAuthorization Entities</returns>
		public static CreditCardAuthorizationDAO CreateCreditCardAuthorizationDAO()
		{
			return new CreditCardAuthorizationDAO();
		}

		/// <summary>Creates a new CreditCardAuthorizationLogDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditCardAuthorizationLog Entities</returns>
		public static CreditCardAuthorizationLogDAO CreateCreditCardAuthorizationLogDAO()
		{
			return new CreditCardAuthorizationLogDAO();
		}

		/// <summary>Creates a new CreditCardMerchantDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditCardMerchant Entities</returns>
		public static CreditCardMerchantDAO CreateCreditCardMerchantDAO()
		{
			return new CreditCardMerchantDAO();
		}

		/// <summary>Creates a new CreditCardNetworkDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditCardNetwork Entities</returns>
		public static CreditCardNetworkDAO CreateCreditCardNetworkDAO()
		{
			return new CreditCardNetworkDAO();
		}

		/// <summary>Creates a new CreditCardTerminalDAO object</summary>
		/// <returns>the new DAO object ready to use for CreditCardTerminal Entities</returns>
		public static CreditCardTerminalDAO CreateCreditCardTerminalDAO()
		{
			return new CreditCardTerminalDAO();
		}

		/// <summary>Creates a new CryptoCertificateDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoCertificate Entities</returns>
		public static CryptoCertificateDAO CreateCryptoCertificateDAO()
		{
			return new CryptoCertificateDAO();
		}

		/// <summary>Creates a new CryptoContentDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoContent Entities</returns>
		public static CryptoContentDAO CreateCryptoContentDAO()
		{
			return new CryptoContentDAO();
		}

		/// <summary>Creates a new CryptoCryptogramArchiveDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoCryptogramArchive Entities</returns>
		public static CryptoCryptogramArchiveDAO CreateCryptoCryptogramArchiveDAO()
		{
			return new CryptoCryptogramArchiveDAO();
		}

		/// <summary>Creates a new CryptogramCertificateDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptogramCertificate Entities</returns>
		public static CryptogramCertificateDAO CreateCryptogramCertificateDAO()
		{
			return new CryptogramCertificateDAO();
		}

		/// <summary>Creates a new CryptogramCertificateTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptogramCertificateType Entities</returns>
		public static CryptogramCertificateTypeDAO CreateCryptogramCertificateTypeDAO()
		{
			return new CryptogramCertificateTypeDAO();
		}

		/// <summary>Creates a new CryptogramErrorTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptogramErrorType Entities</returns>
		public static CryptogramErrorTypeDAO CreateCryptogramErrorTypeDAO()
		{
			return new CryptogramErrorTypeDAO();
		}

		/// <summary>Creates a new CryptogramLoadStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptogramLoadStatus Entities</returns>
		public static CryptogramLoadStatusDAO CreateCryptogramLoadStatusDAO()
		{
			return new CryptogramLoadStatusDAO();
		}

		/// <summary>Creates a new CryptoOperatorActivationkeyDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoOperatorActivationkey Entities</returns>
		public static CryptoOperatorActivationkeyDAO CreateCryptoOperatorActivationkeyDAO()
		{
			return new CryptoOperatorActivationkeyDAO();
		}

		/// <summary>Creates a new CryptoQArchiveDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoQArchive Entities</returns>
		public static CryptoQArchiveDAO CreateCryptoQArchiveDAO()
		{
			return new CryptoQArchiveDAO();
		}

		/// <summary>Creates a new CryptoResourceDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoResource Entities</returns>
		public static CryptoResourceDAO CreateCryptoResourceDAO()
		{
			return new CryptoResourceDAO();
		}

		/// <summary>Creates a new CryptoResourceDataDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoResourceData Entities</returns>
		public static CryptoResourceDataDAO CreateCryptoResourceDataDAO()
		{
			return new CryptoResourceDataDAO();
		}

		/// <summary>Creates a new CryptoSamSignCertificateDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoSamSignCertificate Entities</returns>
		public static CryptoSamSignCertificateDAO CreateCryptoSamSignCertificateDAO()
		{
			return new CryptoSamSignCertificateDAO();
		}

		/// <summary>Creates a new CryptoUicCertificateDAO object</summary>
		/// <returns>the new DAO object ready to use for CryptoUicCertificate Entities</returns>
		public static CryptoUicCertificateDAO CreateCryptoUicCertificateDAO()
		{
			return new CryptoUicCertificateDAO();
		}

		/// <summary>Creates a new CustomAttributeDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomAttribute Entities</returns>
		public static CustomAttributeDAO CreateCustomAttributeDAO()
		{
			return new CustomAttributeDAO();
		}

		/// <summary>Creates a new CustomAttributeValueDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomAttributeValue Entities</returns>
		public static CustomAttributeValueDAO CreateCustomAttributeValueDAO()
		{
			return new CustomAttributeValueDAO();
		}

		/// <summary>Creates a new CustomAttributeValueToPersonDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomAttributeValueToPerson Entities</returns>
		public static CustomAttributeValueToPersonDAO CreateCustomAttributeValueToPersonDAO()
		{
			return new CustomAttributeValueToPersonDAO();
		}

		/// <summary>Creates a new CustomerAccountDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerAccount Entities</returns>
		public static CustomerAccountDAO CreateCustomerAccountDAO()
		{
			return new CustomerAccountDAO();
		}

		/// <summary>Creates a new CustomerAccountNotificationDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerAccountNotification Entities</returns>
		public static CustomerAccountNotificationDAO CreateCustomerAccountNotificationDAO()
		{
			return new CustomerAccountNotificationDAO();
		}

		/// <summary>Creates a new CustomerAccountRoleDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerAccountRole Entities</returns>
		public static CustomerAccountRoleDAO CreateCustomerAccountRoleDAO()
		{
			return new CustomerAccountRoleDAO();
		}

		/// <summary>Creates a new CustomerAccountStateDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerAccountState Entities</returns>
		public static CustomerAccountStateDAO CreateCustomerAccountStateDAO()
		{
			return new CustomerAccountStateDAO();
		}

		/// <summary>Creates a new CustomerAccountToVerificationAttemptTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerAccountToVerificationAttemptType Entities</returns>
		public static CustomerAccountToVerificationAttemptTypeDAO CreateCustomerAccountToVerificationAttemptTypeDAO()
		{
			return new CustomerAccountToVerificationAttemptTypeDAO();
		}

		/// <summary>Creates a new CustomerLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomerLanguage Entities</returns>
		public static CustomerLanguageDAO CreateCustomerLanguageDAO()
		{
			return new CustomerLanguageDAO();
		}

		/// <summary>Creates a new DeviceCommunicationHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceCommunicationHistory Entities</returns>
		public static DeviceCommunicationHistoryDAO CreateDeviceCommunicationHistoryDAO()
		{
			return new DeviceCommunicationHistoryDAO();
		}

		/// <summary>Creates a new DeviceReaderDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReader Entities</returns>
		public static DeviceReaderDAO CreateDeviceReaderDAO()
		{
			return new DeviceReaderDAO();
		}

		/// <summary>Creates a new DeviceReaderCryptoResourceDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReaderCryptoResource Entities</returns>
		public static DeviceReaderCryptoResourceDAO CreateDeviceReaderCryptoResourceDAO()
		{
			return new DeviceReaderCryptoResourceDAO();
		}

		/// <summary>Creates a new DeviceReaderJobDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReaderJob Entities</returns>
		public static DeviceReaderJobDAO CreateDeviceReaderJobDAO()
		{
			return new DeviceReaderJobDAO();
		}

		/// <summary>Creates a new DeviceReaderKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReaderKey Entities</returns>
		public static DeviceReaderKeyDAO CreateDeviceReaderKeyDAO()
		{
			return new DeviceReaderKeyDAO();
		}

		/// <summary>Creates a new DeviceReaderKeyToCertDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReaderKeyToCert Entities</returns>
		public static DeviceReaderKeyToCertDAO CreateDeviceReaderKeyToCertDAO()
		{
			return new DeviceReaderKeyToCertDAO();
		}

		/// <summary>Creates a new DeviceReaderToCertDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceReaderToCert Entities</returns>
		public static DeviceReaderToCertDAO CreateDeviceReaderToCertDAO()
		{
			return new DeviceReaderToCertDAO();
		}

		/// <summary>Creates a new DeviceTokenDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceToken Entities</returns>
		public static DeviceTokenDAO CreateDeviceTokenDAO()
		{
			return new DeviceTokenDAO();
		}

		/// <summary>Creates a new DeviceTokenStateDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceTokenState Entities</returns>
		public static DeviceTokenStateDAO CreateDeviceTokenStateDAO()
		{
			return new DeviceTokenStateDAO();
		}

		/// <summary>Creates a new DocumentHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for DocumentHistory Entities</returns>
		public static DocumentHistoryDAO CreateDocumentHistoryDAO()
		{
			return new DocumentHistoryDAO();
		}

		/// <summary>Creates a new DocumentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for DocumentType Entities</returns>
		public static DocumentTypeDAO CreateDocumentTypeDAO()
		{
			return new DocumentTypeDAO();
		}

		/// <summary>Creates a new DormancyNotificationDAO object</summary>
		/// <returns>the new DAO object ready to use for DormancyNotification Entities</returns>
		public static DormancyNotificationDAO CreateDormancyNotificationDAO()
		{
			return new DormancyNotificationDAO();
		}

		/// <summary>Creates a new DunningActionDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningAction Entities</returns>
		public static DunningActionDAO CreateDunningActionDAO()
		{
			return new DunningActionDAO();
		}

		/// <summary>Creates a new DunningLevelConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningLevelConfiguration Entities</returns>
		public static DunningLevelConfigurationDAO CreateDunningLevelConfigurationDAO()
		{
			return new DunningLevelConfigurationDAO();
		}

		/// <summary>Creates a new DunningLevelPostingKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningLevelPostingKey Entities</returns>
		public static DunningLevelPostingKeyDAO CreateDunningLevelPostingKeyDAO()
		{
			return new DunningLevelPostingKeyDAO();
		}

		/// <summary>Creates a new DunningProcessDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningProcess Entities</returns>
		public static DunningProcessDAO CreateDunningProcessDAO()
		{
			return new DunningProcessDAO();
		}

		/// <summary>Creates a new DunningToInvoiceDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningToInvoice Entities</returns>
		public static DunningToInvoiceDAO CreateDunningToInvoiceDAO()
		{
			return new DunningToInvoiceDAO();
		}

		/// <summary>Creates a new DunningToPostingDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningToPosting Entities</returns>
		public static DunningToPostingDAO CreateDunningToPostingDAO()
		{
			return new DunningToPostingDAO();
		}

		/// <summary>Creates a new DunningToProductDAO object</summary>
		/// <returns>the new DAO object ready to use for DunningToProduct Entities</returns>
		public static DunningToProductDAO CreateDunningToProductDAO()
		{
			return new DunningToProductDAO();
		}

		/// <summary>Creates a new EmailEventDAO object</summary>
		/// <returns>the new DAO object ready to use for EmailEvent Entities</returns>
		public static EmailEventDAO CreateEmailEventDAO()
		{
			return new EmailEventDAO();
		}

		/// <summary>Creates a new EmailEventResendLockDAO object</summary>
		/// <returns>the new DAO object ready to use for EmailEventResendLock Entities</returns>
		public static EmailEventResendLockDAO CreateEmailEventResendLockDAO()
		{
			return new EmailEventResendLockDAO();
		}

		/// <summary>Creates a new EmailMessageDAO object</summary>
		/// <returns>the new DAO object ready to use for EmailMessage Entities</returns>
		public static EmailMessageDAO CreateEmailMessageDAO()
		{
			return new EmailMessageDAO();
		}

		/// <summary>Creates a new EntitlementDAO object</summary>
		/// <returns>the new DAO object ready to use for Entitlement Entities</returns>
		public static EntitlementDAO CreateEntitlementDAO()
		{
			return new EntitlementDAO();
		}

		/// <summary>Creates a new EntitlementToProductDAO object</summary>
		/// <returns>the new DAO object ready to use for EntitlementToProduct Entities</returns>
		public static EntitlementToProductDAO CreateEntitlementToProductDAO()
		{
			return new EntitlementToProductDAO();
		}

		/// <summary>Creates a new EntitlementTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for EntitlementType Entities</returns>
		public static EntitlementTypeDAO CreateEntitlementTypeDAO()
		{
			return new EntitlementTypeDAO();
		}

		/// <summary>Creates a new EventSettingDAO object</summary>
		/// <returns>the new DAO object ready to use for EventSetting Entities</returns>
		public static EventSettingDAO CreateEventSettingDAO()
		{
			return new EventSettingDAO();
		}

		/// <summary>Creates a new FareChangeCauseDAO object</summary>
		/// <returns>the new DAO object ready to use for FareChangeCause Entities</returns>
		public static FareChangeCauseDAO CreateFareChangeCauseDAO()
		{
			return new FareChangeCauseDAO();
		}

		/// <summary>Creates a new FareEvasionBehaviourDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionBehaviour Entities</returns>
		public static FareEvasionBehaviourDAO CreateFareEvasionBehaviourDAO()
		{
			return new FareEvasionBehaviourDAO();
		}

		/// <summary>Creates a new FareEvasionIncidentDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionIncident Entities</returns>
		public static FareEvasionIncidentDAO CreateFareEvasionIncidentDAO()
		{
			return new FareEvasionIncidentDAO();
		}

		/// <summary>Creates a new FareEvasionInspectionDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionInspection Entities</returns>
		public static FareEvasionInspectionDAO CreateFareEvasionInspectionDAO()
		{
			return new FareEvasionInspectionDAO();
		}

		/// <summary>Creates a new FareEvasionStateDAO object</summary>
		/// <returns>the new DAO object ready to use for FareEvasionState Entities</returns>
		public static FareEvasionStateDAO CreateFareEvasionStateDAO()
		{
			return new FareEvasionStateDAO();
		}

		/// <summary>Creates a new FarePaymentErrorDAO object</summary>
		/// <returns>the new DAO object ready to use for FarePaymentError Entities</returns>
		public static FarePaymentErrorDAO CreateFarePaymentErrorDAO()
		{
			return new FarePaymentErrorDAO();
		}

		/// <summary>Creates a new FarePaymentResultTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for FarePaymentResultType Entities</returns>
		public static FarePaymentResultTypeDAO CreateFarePaymentResultTypeDAO()
		{
			return new FarePaymentResultTypeDAO();
		}

		/// <summary>Creates a new FastcardDAO object</summary>
		/// <returns>the new DAO object ready to use for Fastcard Entities</returns>
		public static FastcardDAO CreateFastcardDAO()
		{
			return new FastcardDAO();
		}

		/// <summary>Creates a new FilterCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterCategory Entities</returns>
		public static FilterCategoryDAO CreateFilterCategoryDAO()
		{
			return new FilterCategoryDAO();
		}

		/// <summary>Creates a new FilterElementDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterElement Entities</returns>
		public static FilterElementDAO CreateFilterElementDAO()
		{
			return new FilterElementDAO();
		}

		/// <summary>Creates a new FilterListDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterList Entities</returns>
		public static FilterListDAO CreateFilterListDAO()
		{
			return new FilterListDAO();
		}

		/// <summary>Creates a new FilterListElementDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterListElement Entities</returns>
		public static FilterListElementDAO CreateFilterListElementDAO()
		{
			return new FilterListElementDAO();
		}

		/// <summary>Creates a new FilterTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterType Entities</returns>
		public static FilterTypeDAO CreateFilterTypeDAO()
		{
			return new FilterTypeDAO();
		}

		/// <summary>Creates a new FilterValueDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterValue Entities</returns>
		public static FilterValueDAO CreateFilterValueDAO()
		{
			return new FilterValueDAO();
		}

		/// <summary>Creates a new FilterValueSetDAO object</summary>
		/// <returns>the new DAO object ready to use for FilterValueSet Entities</returns>
		public static FilterValueSetDAO CreateFilterValueSetDAO()
		{
			return new FilterValueSetDAO();
		}

		/// <summary>Creates a new FlexValueDAO object</summary>
		/// <returns>the new DAO object ready to use for FlexValue Entities</returns>
		public static FlexValueDAO CreateFlexValueDAO()
		{
			return new FlexValueDAO();
		}

		/// <summary>Creates a new FormDAO object</summary>
		/// <returns>the new DAO object ready to use for Form Entities</returns>
		public static FormDAO CreateFormDAO()
		{
			return new FormDAO();
		}

		/// <summary>Creates a new FormTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for FormTemplate Entities</returns>
		public static FormTemplateDAO CreateFormTemplateDAO()
		{
			return new FormTemplateDAO();
		}

		/// <summary>Creates a new FrequencyDAO object</summary>
		/// <returns>the new DAO object ready to use for Frequency Entities</returns>
		public static FrequencyDAO CreateFrequencyDAO()
		{
			return new FrequencyDAO();
		}

		/// <summary>Creates a new GenderDAO object</summary>
		/// <returns>the new DAO object ready to use for Gender Entities</returns>
		public static GenderDAO CreateGenderDAO()
		{
			return new GenderDAO();
		}

		/// <summary>Creates a new IdentificationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for IdentificationType Entities</returns>
		public static IdentificationTypeDAO CreateIdentificationTypeDAO()
		{
			return new IdentificationTypeDAO();
		}

		/// <summary>Creates a new InspectionCriterionDAO object</summary>
		/// <returns>the new DAO object ready to use for InspectionCriterion Entities</returns>
		public static InspectionCriterionDAO CreateInspectionCriterionDAO()
		{
			return new InspectionCriterionDAO();
		}

		/// <summary>Creates a new InspectionReportDAO object</summary>
		/// <returns>the new DAO object ready to use for InspectionReport Entities</returns>
		public static InspectionReportDAO CreateInspectionReportDAO()
		{
			return new InspectionReportDAO();
		}

		/// <summary>Creates a new InspectionResultDAO object</summary>
		/// <returns>the new DAO object ready to use for InspectionResult Entities</returns>
		public static InspectionResultDAO CreateInspectionResultDAO()
		{
			return new InspectionResultDAO();
		}

		/// <summary>Creates a new InventoryStateDAO object</summary>
		/// <returns>the new DAO object ready to use for InventoryState Entities</returns>
		public static InventoryStateDAO CreateInventoryStateDAO()
		{
			return new InventoryStateDAO();
		}

		/// <summary>Creates a new InvoiceDAO object</summary>
		/// <returns>the new DAO object ready to use for Invoice Entities</returns>
		public static InvoiceDAO CreateInvoiceDAO()
		{
			return new InvoiceDAO();
		}

		/// <summary>Creates a new InvoiceEntryDAO object</summary>
		/// <returns>the new DAO object ready to use for InvoiceEntry Entities</returns>
		public static InvoiceEntryDAO CreateInvoiceEntryDAO()
		{
			return new InvoiceEntryDAO();
		}

		/// <summary>Creates a new InvoiceTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for InvoiceType Entities</returns>
		public static InvoiceTypeDAO CreateInvoiceTypeDAO()
		{
			return new InvoiceTypeDAO();
		}

		/// <summary>Creates a new InvoicingDAO object</summary>
		/// <returns>the new DAO object ready to use for Invoicing Entities</returns>
		public static InvoicingDAO CreateInvoicingDAO()
		{
			return new InvoicingDAO();
		}

		/// <summary>Creates a new InvoicingFilterTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for InvoicingFilterType Entities</returns>
		public static InvoicingFilterTypeDAO CreateInvoicingFilterTypeDAO()
		{
			return new InvoicingFilterTypeDAO();
		}

		/// <summary>Creates a new InvoicingTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for InvoicingType Entities</returns>
		public static InvoicingTypeDAO CreateInvoicingTypeDAO()
		{
			return new InvoicingTypeDAO();
		}

		/// <summary>Creates a new JobDAO object</summary>
		/// <returns>the new DAO object ready to use for Job Entities</returns>
		public static JobDAO CreateJobDAO()
		{
			return new JobDAO();
		}

		/// <summary>Creates a new JobBulkImportDAO object</summary>
		/// <returns>the new DAO object ready to use for JobBulkImport Entities</returns>
		public static JobBulkImportDAO CreateJobBulkImportDAO()
		{
			return new JobBulkImportDAO();
		}

		/// <summary>Creates a new JobCardImportDAO object</summary>
		/// <returns>the new DAO object ready to use for JobCardImport Entities</returns>
		public static JobCardImportDAO CreateJobCardImportDAO()
		{
			return new JobCardImportDAO();
		}

		/// <summary>Creates a new JobFileTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for JobFileType Entities</returns>
		public static JobFileTypeDAO CreateJobFileTypeDAO()
		{
			return new JobFileTypeDAO();
		}

		/// <summary>Creates a new JobStateDAO object</summary>
		/// <returns>the new DAO object ready to use for JobState Entities</returns>
		public static JobStateDAO CreateJobStateDAO()
		{
			return new JobStateDAO();
		}

		/// <summary>Creates a new JobTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for JobType Entities</returns>
		public static JobTypeDAO CreateJobTypeDAO()
		{
			return new JobTypeDAO();
		}

		/// <summary>Creates a new LookupAddressDAO object</summary>
		/// <returns>the new DAO object ready to use for LookupAddress Entities</returns>
		public static LookupAddressDAO CreateLookupAddressDAO()
		{
			return new LookupAddressDAO();
		}

		/// <summary>Creates a new MediaEligibilityRequirementDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaEligibilityRequirement Entities</returns>
		public static MediaEligibilityRequirementDAO CreateMediaEligibilityRequirementDAO()
		{
			return new MediaEligibilityRequirementDAO();
		}

		/// <summary>Creates a new MediaSalePreconditionDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaSalePrecondition Entities</returns>
		public static MediaSalePreconditionDAO CreateMediaSalePreconditionDAO()
		{
			return new MediaSalePreconditionDAO();
		}

		/// <summary>Creates a new MerchantDAO object</summary>
		/// <returns>the new DAO object ready to use for Merchant Entities</returns>
		public static MerchantDAO CreateMerchantDAO()
		{
			return new MerchantDAO();
		}

		/// <summary>Creates a new MessageDAO object</summary>
		/// <returns>the new DAO object ready to use for Message Entities</returns>
		public static MessageDAO CreateMessageDAO()
		{
			return new MessageDAO();
		}

		/// <summary>Creates a new MessageFormatDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageFormat Entities</returns>
		public static MessageFormatDAO CreateMessageFormatDAO()
		{
			return new MessageFormatDAO();
		}

		/// <summary>Creates a new MessageTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageType Entities</returns>
		public static MessageTypeDAO CreateMessageTypeDAO()
		{
			return new MessageTypeDAO();
		}

		/// <summary>Creates a new MobilityProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for MobilityProvider Entities</returns>
		public static MobilityProviderDAO CreateMobilityProviderDAO()
		{
			return new MobilityProviderDAO();
		}

		/// <summary>Creates a new NotificationDeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for NotificationDevice Entities</returns>
		public static NotificationDeviceDAO CreateNotificationDeviceDAO()
		{
			return new NotificationDeviceDAO();
		}

		/// <summary>Creates a new NotificationMessageDAO object</summary>
		/// <returns>the new DAO object ready to use for NotificationMessage Entities</returns>
		public static NotificationMessageDAO CreateNotificationMessageDAO()
		{
			return new NotificationMessageDAO();
		}

		/// <summary>Creates a new NotificationMessageOwnerDAO object</summary>
		/// <returns>the new DAO object ready to use for NotificationMessageOwner Entities</returns>
		public static NotificationMessageOwnerDAO CreateNotificationMessageOwnerDAO()
		{
			return new NotificationMessageOwnerDAO();
		}

		/// <summary>Creates a new NotificationMessageToCustomerDAO object</summary>
		/// <returns>the new DAO object ready to use for NotificationMessageToCustomer Entities</returns>
		public static NotificationMessageToCustomerDAO CreateNotificationMessageToCustomerDAO()
		{
			return new NotificationMessageToCustomerDAO();
		}

		/// <summary>Creates a new NumberGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for NumberGroup Entities</returns>
		public static NumberGroupDAO CreateNumberGroupDAO()
		{
			return new NumberGroupDAO();
		}

		/// <summary>Creates a new NumberGroupRandomizedDAO object</summary>
		/// <returns>the new DAO object ready to use for NumberGroupRandomized Entities</returns>
		public static NumberGroupRandomizedDAO CreateNumberGroupRandomizedDAO()
		{
			return new NumberGroupRandomizedDAO();
		}

		/// <summary>Creates a new NumberGroupScopeDAO object</summary>
		/// <returns>the new DAO object ready to use for NumberGroupScope Entities</returns>
		public static NumberGroupScopeDAO CreateNumberGroupScopeDAO()
		{
			return new NumberGroupScopeDAO();
		}

		/// <summary>Creates a new OrderDAO object</summary>
		/// <returns>the new DAO object ready to use for Order Entities</returns>
		public static OrderDAO CreateOrderDAO()
		{
			return new OrderDAO();
		}

		/// <summary>Creates a new OrderDetailDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderDetail Entities</returns>
		public static OrderDetailDAO CreateOrderDetailDAO()
		{
			return new OrderDetailDAO();
		}

		/// <summary>Creates a new OrderDetailToCardDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderDetailToCard Entities</returns>
		public static OrderDetailToCardDAO CreateOrderDetailToCardDAO()
		{
			return new OrderDetailToCardDAO();
		}

		/// <summary>Creates a new OrderDetailToCardHolderDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderDetailToCardHolder Entities</returns>
		public static OrderDetailToCardHolderDAO CreateOrderDetailToCardHolderDAO()
		{
			return new OrderDetailToCardHolderDAO();
		}

		/// <summary>Creates a new OrderHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderHistory Entities</returns>
		public static OrderHistoryDAO CreateOrderHistoryDAO()
		{
			return new OrderHistoryDAO();
		}

		/// <summary>Creates a new OrderProcessingCleanupDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderProcessingCleanup Entities</returns>
		public static OrderProcessingCleanupDAO CreateOrderProcessingCleanupDAO()
		{
			return new OrderProcessingCleanupDAO();
		}

		/// <summary>Creates a new OrderStateDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderState Entities</returns>
		public static OrderStateDAO CreateOrderStateDAO()
		{
			return new OrderStateDAO();
		}

		/// <summary>Creates a new OrderTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderType Entities</returns>
		public static OrderTypeDAO CreateOrderTypeDAO()
		{
			return new OrderTypeDAO();
		}

		/// <summary>Creates a new OrderWithTotalDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderWithTotal Entities</returns>
		public static OrderWithTotalDAO CreateOrderWithTotalDAO()
		{
			return new OrderWithTotalDAO();
		}

		/// <summary>Creates a new OrderWorkItemDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderWorkItem Entities</returns>
		public static OrderWorkItemDAO CreateOrderWorkItemDAO()
		{
			return new OrderWorkItemDAO();
		}

		/// <summary>Creates a new OrganizationDAO object</summary>
		/// <returns>the new DAO object ready to use for Organization Entities</returns>
		public static OrganizationDAO CreateOrganizationDAO()
		{
			return new OrganizationDAO();
		}

		/// <summary>Creates a new OrganizationAddressDAO object</summary>
		/// <returns>the new DAO object ready to use for OrganizationAddress Entities</returns>
		public static OrganizationAddressDAO CreateOrganizationAddressDAO()
		{
			return new OrganizationAddressDAO();
		}

		/// <summary>Creates a new OrganizationParticipantDAO object</summary>
		/// <returns>the new DAO object ready to use for OrganizationParticipant Entities</returns>
		public static OrganizationParticipantDAO CreateOrganizationParticipantDAO()
		{
			return new OrganizationParticipantDAO();
		}

		/// <summary>Creates a new OrganizationSubtypeDAO object</summary>
		/// <returns>the new DAO object ready to use for OrganizationSubtype Entities</returns>
		public static OrganizationSubtypeDAO CreateOrganizationSubtypeDAO()
		{
			return new OrganizationSubtypeDAO();
		}

		/// <summary>Creates a new OrganizationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for OrganizationType Entities</returns>
		public static OrganizationTypeDAO CreateOrganizationTypeDAO()
		{
			return new OrganizationTypeDAO();
		}

		/// <summary>Creates a new PageContentDAO object</summary>
		/// <returns>the new DAO object ready to use for PageContent Entities</returns>
		public static PageContentDAO CreatePageContentDAO()
		{
			return new PageContentDAO();
		}

		/// <summary>Creates a new ParameterDAO object</summary>
		/// <returns>the new DAO object ready to use for Parameter Entities</returns>
		public static ParameterDAO CreateParameterDAO()
		{
			return new ParameterDAO();
		}

		/// <summary>Creates a new ParameterArchiveDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterArchive Entities</returns>
		public static ParameterArchiveDAO CreateParameterArchiveDAO()
		{
			return new ParameterArchiveDAO();
		}

		/// <summary>Creates a new ParameterArchiveReleaseDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterArchiveRelease Entities</returns>
		public static ParameterArchiveReleaseDAO CreateParameterArchiveReleaseDAO()
		{
			return new ParameterArchiveReleaseDAO();
		}

		/// <summary>Creates a new ParameterArchiveResultViewDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterArchiveResultView Entities</returns>
		public static ParameterArchiveResultViewDAO CreateParameterArchiveResultViewDAO()
		{
			return new ParameterArchiveResultViewDAO();
		}

		/// <summary>Creates a new ParameterGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterGroup Entities</returns>
		public static ParameterGroupDAO CreateParameterGroupDAO()
		{
			return new ParameterGroupDAO();
		}

		/// <summary>Creates a new ParameterGroupToParameterDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterGroupToParameter Entities</returns>
		public static ParameterGroupToParameterDAO CreateParameterGroupToParameterDAO()
		{
			return new ParameterGroupToParameterDAO();
		}

		/// <summary>Creates a new ParameterTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterType Entities</returns>
		public static ParameterTypeDAO CreateParameterTypeDAO()
		{
			return new ParameterTypeDAO();
		}

		/// <summary>Creates a new ParameterValueDAO object</summary>
		/// <returns>the new DAO object ready to use for ParameterValue Entities</returns>
		public static ParameterValueDAO CreateParameterValueDAO()
		{
			return new ParameterValueDAO();
		}

		/// <summary>Creates a new ParaTransAttributeDAO object</summary>
		/// <returns>the new DAO object ready to use for ParaTransAttribute Entities</returns>
		public static ParaTransAttributeDAO CreateParaTransAttributeDAO()
		{
			return new ParaTransAttributeDAO();
		}

		/// <summary>Creates a new ParaTransAttributeTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ParaTransAttributeType Entities</returns>
		public static ParaTransAttributeTypeDAO CreateParaTransAttributeTypeDAO()
		{
			return new ParaTransAttributeTypeDAO();
		}

		/// <summary>Creates a new ParaTransJournalDAO object</summary>
		/// <returns>the new DAO object ready to use for ParaTransJournal Entities</returns>
		public static ParaTransJournalDAO CreateParaTransJournalDAO()
		{
			return new ParaTransJournalDAO();
		}

		/// <summary>Creates a new ParaTransJournalTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ParaTransJournalType Entities</returns>
		public static ParaTransJournalTypeDAO CreateParaTransJournalTypeDAO()
		{
			return new ParaTransJournalTypeDAO();
		}

		/// <summary>Creates a new ParaTransPaymentTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ParaTransPaymentType Entities</returns>
		public static ParaTransPaymentTypeDAO CreateParaTransPaymentTypeDAO()
		{
			return new ParaTransPaymentTypeDAO();
		}

		/// <summary>Creates a new ParticipantGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for ParticipantGroup Entities</returns>
		public static ParticipantGroupDAO CreateParticipantGroupDAO()
		{
			return new ParticipantGroupDAO();
		}

		/// <summary>Creates a new PassExpiryNotificationDAO object</summary>
		/// <returns>the new DAO object ready to use for PassExpiryNotification Entities</returns>
		public static PassExpiryNotificationDAO CreatePassExpiryNotificationDAO()
		{
			return new PassExpiryNotificationDAO();
		}

		/// <summary>Creates a new PaymentDAO object</summary>
		/// <returns>the new DAO object ready to use for Payment Entities</returns>
		public static PaymentDAO CreatePaymentDAO()
		{
			return new PaymentDAO();
		}

		/// <summary>Creates a new PaymentJournalDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentJournal Entities</returns>
		public static PaymentJournalDAO CreatePaymentJournalDAO()
		{
			return new PaymentJournalDAO();
		}

		/// <summary>Creates a new PaymentJournalToComponentDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentJournalToComponent Entities</returns>
		public static PaymentJournalToComponentDAO CreatePaymentJournalToComponentDAO()
		{
			return new PaymentJournalToComponentDAO();
		}

		/// <summary>Creates a new PaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentMethod Entities</returns>
		public static PaymentMethodDAO CreatePaymentMethodDAO()
		{
			return new PaymentMethodDAO();
		}

		/// <summary>Creates a new PaymentModalityDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentModality Entities</returns>
		public static PaymentModalityDAO CreatePaymentModalityDAO()
		{
			return new PaymentModalityDAO();
		}

		/// <summary>Creates a new PaymentOptionDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentOption Entities</returns>
		public static PaymentOptionDAO CreatePaymentOptionDAO()
		{
			return new PaymentOptionDAO();
		}

		/// <summary>Creates a new PaymentProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentProvider Entities</returns>
		public static PaymentProviderDAO CreatePaymentProviderDAO()
		{
			return new PaymentProviderDAO();
		}

		/// <summary>Creates a new PaymentProviderAccountDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentProviderAccount Entities</returns>
		public static PaymentProviderAccountDAO CreatePaymentProviderAccountDAO()
		{
			return new PaymentProviderAccountDAO();
		}

		/// <summary>Creates a new PaymentRecognitionDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentRecognition Entities</returns>
		public static PaymentRecognitionDAO CreatePaymentRecognitionDAO()
		{
			return new PaymentRecognitionDAO();
		}

		/// <summary>Creates a new PaymentReconciliationDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentReconciliation Entities</returns>
		public static PaymentReconciliationDAO CreatePaymentReconciliationDAO()
		{
			return new PaymentReconciliationDAO();
		}

		/// <summary>Creates a new PaymentReconciliationStateDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentReconciliationState Entities</returns>
		public static PaymentReconciliationStateDAO CreatePaymentReconciliationStateDAO()
		{
			return new PaymentReconciliationStateDAO();
		}

		/// <summary>Creates a new PaymentStateDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentState Entities</returns>
		public static PaymentStateDAO CreatePaymentStateDAO()
		{
			return new PaymentStateDAO();
		}

		/// <summary>Creates a new PendingOrderDAO object</summary>
		/// <returns>the new DAO object ready to use for PendingOrder Entities</returns>
		public static PendingOrderDAO CreatePendingOrderDAO()
		{
			return new PendingOrderDAO();
		}

		/// <summary>Creates a new PerformanceDAO object</summary>
		/// <returns>the new DAO object ready to use for Performance Entities</returns>
		public static PerformanceDAO CreatePerformanceDAO()
		{
			return new PerformanceDAO();
		}

		/// <summary>Creates a new PerformanceAggregationDAO object</summary>
		/// <returns>the new DAO object ready to use for PerformanceAggregation Entities</returns>
		public static PerformanceAggregationDAO CreatePerformanceAggregationDAO()
		{
			return new PerformanceAggregationDAO();
		}

		/// <summary>Creates a new PerformanceComponentDAO object</summary>
		/// <returns>the new DAO object ready to use for PerformanceComponent Entities</returns>
		public static PerformanceComponentDAO CreatePerformanceComponentDAO()
		{
			return new PerformanceComponentDAO();
		}

		/// <summary>Creates a new PerformanceIndicatorDAO object</summary>
		/// <returns>the new DAO object ready to use for PerformanceIndicator Entities</returns>
		public static PerformanceIndicatorDAO CreatePerformanceIndicatorDAO()
		{
			return new PerformanceIndicatorDAO();
		}

		/// <summary>Creates a new PersonDAO object</summary>
		/// <returns>the new DAO object ready to use for Person Entities</returns>
		public static PersonDAO CreatePersonDAO()
		{
			return new PersonDAO();
		}

		/// <summary>Creates a new PersonAddressDAO object</summary>
		/// <returns>the new DAO object ready to use for PersonAddress Entities</returns>
		public static PersonAddressDAO CreatePersonAddressDAO()
		{
			return new PersonAddressDAO();
		}

		/// <summary>Creates a new PhotographDAO object</summary>
		/// <returns>the new DAO object ready to use for Photograph Entities</returns>
		public static PhotographDAO CreatePhotographDAO()
		{
			return new PhotographDAO();
		}

		/// <summary>Creates a new PickupLocationDAO object</summary>
		/// <returns>the new DAO object ready to use for PickupLocation Entities</returns>
		public static PickupLocationDAO CreatePickupLocationDAO()
		{
			return new PickupLocationDAO();
		}

		/// <summary>Creates a new PickupLocationToTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for PickupLocationToTicket Entities</returns>
		public static PickupLocationToTicketDAO CreatePickupLocationToTicketDAO()
		{
			return new PickupLocationToTicketDAO();
		}

		/// <summary>Creates a new PostingDAO object</summary>
		/// <returns>the new DAO object ready to use for Posting Entities</returns>
		public static PostingDAO CreatePostingDAO()
		{
			return new PostingDAO();
		}

		/// <summary>Creates a new PostingKeyDAO object</summary>
		/// <returns>the new DAO object ready to use for PostingKey Entities</returns>
		public static PostingKeyDAO CreatePostingKeyDAO()
		{
			return new PostingKeyDAO();
		}

		/// <summary>Creates a new PostingKeyCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for PostingKeyCategory Entities</returns>
		public static PostingKeyCategoryDAO CreatePostingKeyCategoryDAO()
		{
			return new PostingKeyCategoryDAO();
		}

		/// <summary>Creates a new PostingScopeDAO object</summary>
		/// <returns>the new DAO object ready to use for PostingScope Entities</returns>
		public static PostingScopeDAO CreatePostingScopeDAO()
		{
			return new PostingScopeDAO();
		}

		/// <summary>Creates a new PriceAdjustmentDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceAdjustment Entities</returns>
		public static PriceAdjustmentDAO CreatePriceAdjustmentDAO()
		{
			return new PriceAdjustmentDAO();
		}

		/// <summary>Creates a new PriceAdjustmentTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceAdjustmentTemplate Entities</returns>
		public static PriceAdjustmentTemplateDAO CreatePriceAdjustmentTemplateDAO()
		{
			return new PriceAdjustmentTemplateDAO();
		}

		/// <summary>Creates a new PrinterDAO object</summary>
		/// <returns>the new DAO object ready to use for Printer Entities</returns>
		public static PrinterDAO CreatePrinterDAO()
		{
			return new PrinterDAO();
		}

		/// <summary>Creates a new PrinterStateDAO object</summary>
		/// <returns>the new DAO object ready to use for PrinterState Entities</returns>
		public static PrinterStateDAO CreatePrinterStateDAO()
		{
			return new PrinterStateDAO();
		}

		/// <summary>Creates a new PrinterToUnitDAO object</summary>
		/// <returns>the new DAO object ready to use for PrinterToUnit Entities</returns>
		public static PrinterToUnitDAO CreatePrinterToUnitDAO()
		{
			return new PrinterToUnitDAO();
		}

		/// <summary>Creates a new PrinterTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for PrinterType Entities</returns>
		public static PrinterTypeDAO CreatePrinterTypeDAO()
		{
			return new PrinterTypeDAO();
		}

		/// <summary>Creates a new PriorityDAO object</summary>
		/// <returns>the new DAO object ready to use for Priority Entities</returns>
		public static PriorityDAO CreatePriorityDAO()
		{
			return new PriorityDAO();
		}

		/// <summary>Creates a new ProductDAO object</summary>
		/// <returns>the new DAO object ready to use for Product Entities</returns>
		public static ProductDAO CreateProductDAO()
		{
			return new ProductDAO();
		}

		/// <summary>Creates a new ProductRelationDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductRelation Entities</returns>
		public static ProductRelationDAO CreateProductRelationDAO()
		{
			return new ProductRelationDAO();
		}

		/// <summary>Creates a new ProductSubsidyDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductSubsidy Entities</returns>
		public static ProductSubsidyDAO CreateProductSubsidyDAO()
		{
			return new ProductSubsidyDAO();
		}

		/// <summary>Creates a new ProductTerminationDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductTermination Entities</returns>
		public static ProductTerminationDAO CreateProductTerminationDAO()
		{
			return new ProductTerminationDAO();
		}

		/// <summary>Creates a new ProductTerminationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductTerminationType Entities</returns>
		public static ProductTerminationTypeDAO CreateProductTerminationTypeDAO()
		{
			return new ProductTerminationTypeDAO();
		}

		/// <summary>Creates a new ProductTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductType Entities</returns>
		public static ProductTypeDAO CreateProductTypeDAO()
		{
			return new ProductTypeDAO();
		}

		/// <summary>Creates a new PromoCodeDAO object</summary>
		/// <returns>the new DAO object ready to use for PromoCode Entities</returns>
		public static PromoCodeDAO CreatePromoCodeDAO()
		{
			return new PromoCodeDAO();
		}

		/// <summary>Creates a new PromoCodeStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for PromoCodeStatus Entities</returns>
		public static PromoCodeStatusDAO CreatePromoCodeStatusDAO()
		{
			return new PromoCodeStatusDAO();
		}

		/// <summary>Creates a new ProviderAccountStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ProviderAccountState Entities</returns>
		public static ProviderAccountStateDAO CreateProviderAccountStateDAO()
		{
			return new ProviderAccountStateDAO();
		}

		/// <summary>Creates a new ProvisioningStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ProvisioningState Entities</returns>
		public static ProvisioningStateDAO CreateProvisioningStateDAO()
		{
			return new ProvisioningStateDAO();
		}

		/// <summary>Creates a new RecipientGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for RecipientGroup Entities</returns>
		public static RecipientGroupDAO CreateRecipientGroupDAO()
		{
			return new RecipientGroupDAO();
		}

		/// <summary>Creates a new RecipientGroupMemberDAO object</summary>
		/// <returns>the new DAO object ready to use for RecipientGroupMember Entities</returns>
		public static RecipientGroupMemberDAO CreateRecipientGroupMemberDAO()
		{
			return new RecipientGroupMemberDAO();
		}

		/// <summary>Creates a new RefundDAO object</summary>
		/// <returns>the new DAO object ready to use for Refund Entities</returns>
		public static RefundDAO CreateRefundDAO()
		{
			return new RefundDAO();
		}

		/// <summary>Creates a new RefundReasonDAO object</summary>
		/// <returns>the new DAO object ready to use for RefundReason Entities</returns>
		public static RefundReasonDAO CreateRefundReasonDAO()
		{
			return new RefundReasonDAO();
		}

		/// <summary>Creates a new RentalStateDAO object</summary>
		/// <returns>the new DAO object ready to use for RentalState Entities</returns>
		public static RentalStateDAO CreateRentalStateDAO()
		{
			return new RentalStateDAO();
		}

		/// <summary>Creates a new RentalTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for RentalType Entities</returns>
		public static RentalTypeDAO CreateRentalTypeDAO()
		{
			return new RentalTypeDAO();
		}

		/// <summary>Creates a new ReportDAO object</summary>
		/// <returns>the new DAO object ready to use for Report Entities</returns>
		public static ReportDAO CreateReportDAO()
		{
			return new ReportDAO();
		}

		/// <summary>Creates a new ReportCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportCategory Entities</returns>
		public static ReportCategoryDAO CreateReportCategoryDAO()
		{
			return new ReportCategoryDAO();
		}

		/// <summary>Creates a new ReportDataFileDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportDataFile Entities</returns>
		public static ReportDataFileDAO CreateReportDataFileDAO()
		{
			return new ReportDataFileDAO();
		}

		/// <summary>Creates a new ReportJobDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportJob Entities</returns>
		public static ReportJobDAO CreateReportJobDAO()
		{
			return new ReportJobDAO();
		}

		/// <summary>Creates a new ReportJobResultDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportJobResult Entities</returns>
		public static ReportJobResultDAO CreateReportJobResultDAO()
		{
			return new ReportJobResultDAO();
		}

		/// <summary>Creates a new ReportToClientDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportToClient Entities</returns>
		public static ReportToClientDAO CreateReportToClientDAO()
		{
			return new ReportToClientDAO();
		}

		/// <summary>Creates a new RevenueRecognitionDAO object</summary>
		/// <returns>the new DAO object ready to use for RevenueRecognition Entities</returns>
		public static RevenueRecognitionDAO CreateRevenueRecognitionDAO()
		{
			return new RevenueRecognitionDAO();
		}

		/// <summary>Creates a new RevenueSettlementDAO object</summary>
		/// <returns>the new DAO object ready to use for RevenueSettlement Entities</returns>
		public static RevenueSettlementDAO CreateRevenueSettlementDAO()
		{
			return new RevenueSettlementDAO();
		}

		/// <summary>Creates a new RuleViolationDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleViolation Entities</returns>
		public static RuleViolationDAO CreateRuleViolationDAO()
		{
			return new RuleViolationDAO();
		}

		/// <summary>Creates a new RuleViolationProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleViolationProvider Entities</returns>
		public static RuleViolationProviderDAO CreateRuleViolationProviderDAO()
		{
			return new RuleViolationProviderDAO();
		}

		/// <summary>Creates a new RuleViolationSourceTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleViolationSourceType Entities</returns>
		public static RuleViolationSourceTypeDAO CreateRuleViolationSourceTypeDAO()
		{
			return new RuleViolationSourceTypeDAO();
		}

		/// <summary>Creates a new RuleViolationToTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleViolationToTransaction Entities</returns>
		public static RuleViolationToTransactionDAO CreateRuleViolationToTransactionDAO()
		{
			return new RuleViolationToTransactionDAO();
		}

		/// <summary>Creates a new RuleViolationTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for RuleViolationType Entities</returns>
		public static RuleViolationTypeDAO CreateRuleViolationTypeDAO()
		{
			return new RuleViolationTypeDAO();
		}

		/// <summary>Creates a new SaleDAO object</summary>
		/// <returns>the new DAO object ready to use for Sale Entities</returns>
		public static SaleDAO CreateSaleDAO()
		{
			return new SaleDAO();
		}

		/// <summary>Creates a new SalesChannelToPaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for SalesChannelToPaymentMethod Entities</returns>
		public static SalesChannelToPaymentMethodDAO CreateSalesChannelToPaymentMethodDAO()
		{
			return new SalesChannelToPaymentMethodDAO();
		}

		/// <summary>Creates a new SalesRevenueDAO object</summary>
		/// <returns>the new DAO object ready to use for SalesRevenue Entities</returns>
		public static SalesRevenueDAO CreateSalesRevenueDAO()
		{
			return new SalesRevenueDAO();
		}

		/// <summary>Creates a new SaleToComponentDAO object</summary>
		/// <returns>the new DAO object ready to use for SaleToComponent Entities</returns>
		public static SaleToComponentDAO CreateSaleToComponentDAO()
		{
			return new SaleToComponentDAO();
		}

		/// <summary>Creates a new SaleTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for SaleType Entities</returns>
		public static SaleTypeDAO CreateSaleTypeDAO()
		{
			return new SaleTypeDAO();
		}

		/// <summary>Creates a new SamModuleDAO object</summary>
		/// <returns>the new DAO object ready to use for SamModule Entities</returns>
		public static SamModuleDAO CreateSamModuleDAO()
		{
			return new SamModuleDAO();
		}

		/// <summary>Creates a new SamModuleStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for SamModuleStatus Entities</returns>
		public static SamModuleStatusDAO CreateSamModuleStatusDAO()
		{
			return new SamModuleStatusDAO();
		}

		/// <summary>Creates a new SchoolYearDAO object</summary>
		/// <returns>the new DAO object ready to use for SchoolYear Entities</returns>
		public static SchoolYearDAO CreateSchoolYearDAO()
		{
			return new SchoolYearDAO();
		}

		/// <summary>Creates a new SecurityQuestionDAO object</summary>
		/// <returns>the new DAO object ready to use for SecurityQuestion Entities</returns>
		public static SecurityQuestionDAO CreateSecurityQuestionDAO()
		{
			return new SecurityQuestionDAO();
		}

		/// <summary>Creates a new SecurityResponseDAO object</summary>
		/// <returns>the new DAO object ready to use for SecurityResponse Entities</returns>
		public static SecurityResponseDAO CreateSecurityResponseDAO()
		{
			return new SecurityResponseDAO();
		}

		/// <summary>Creates a new SepaFrequencyTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for SepaFrequencyType Entities</returns>
		public static SepaFrequencyTypeDAO CreateSepaFrequencyTypeDAO()
		{
			return new SepaFrequencyTypeDAO();
		}

		/// <summary>Creates a new SepaOwnerTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for SepaOwnerType Entities</returns>
		public static SepaOwnerTypeDAO CreateSepaOwnerTypeDAO()
		{
			return new SepaOwnerTypeDAO();
		}

		/// <summary>Creates a new ServerResponseTimeDataDAO object</summary>
		/// <returns>the new DAO object ready to use for ServerResponseTimeData Entities</returns>
		public static ServerResponseTimeDataDAO CreateServerResponseTimeDataDAO()
		{
			return new ServerResponseTimeDataDAO();
		}

		/// <summary>Creates a new ServerResponseTimeDataAggregationDAO object</summary>
		/// <returns>the new DAO object ready to use for ServerResponseTimeDataAggregation Entities</returns>
		public static ServerResponseTimeDataAggregationDAO CreateServerResponseTimeDataAggregationDAO()
		{
			return new ServerResponseTimeDataAggregationDAO();
		}

		/// <summary>Creates a new SettledRevenueDAO object</summary>
		/// <returns>the new DAO object ready to use for SettledRevenue Entities</returns>
		public static SettledRevenueDAO CreateSettledRevenueDAO()
		{
			return new SettledRevenueDAO();
		}

		/// <summary>Creates a new SettlementAccountDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementAccount Entities</returns>
		public static SettlementAccountDAO CreateSettlementAccountDAO()
		{
			return new SettlementAccountDAO();
		}

		/// <summary>Creates a new SettlementCalendarDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementCalendar Entities</returns>
		public static SettlementCalendarDAO CreateSettlementCalendarDAO()
		{
			return new SettlementCalendarDAO();
		}

		/// <summary>Creates a new SettlementDistributionPolicyDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementDistributionPolicy Entities</returns>
		public static SettlementDistributionPolicyDAO CreateSettlementDistributionPolicyDAO()
		{
			return new SettlementDistributionPolicyDAO();
		}

		/// <summary>Creates a new SettlementHolidayDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementHoliday Entities</returns>
		public static SettlementHolidayDAO CreateSettlementHolidayDAO()
		{
			return new SettlementHolidayDAO();
		}

		/// <summary>Creates a new SettlementOperationDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementOperation Entities</returns>
		public static SettlementOperationDAO CreateSettlementOperationDAO()
		{
			return new SettlementOperationDAO();
		}

		/// <summary>Creates a new SettlementQuerySettingDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementQuerySetting Entities</returns>
		public static SettlementQuerySettingDAO CreateSettlementQuerySettingDAO()
		{
			return new SettlementQuerySettingDAO();
		}

		/// <summary>Creates a new SettlementQuerySettingToTicketDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementQuerySettingToTicket Entities</returns>
		public static SettlementQuerySettingToTicketDAO CreateSettlementQuerySettingToTicketDAO()
		{
			return new SettlementQuerySettingToTicketDAO();
		}

		/// <summary>Creates a new SettlementQueryValueDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementQueryValue Entities</returns>
		public static SettlementQueryValueDAO CreateSettlementQueryValueDAO()
		{
			return new SettlementQueryValueDAO();
		}

		/// <summary>Creates a new SettlementReleaseStateDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementReleaseState Entities</returns>
		public static SettlementReleaseStateDAO CreateSettlementReleaseStateDAO()
		{
			return new SettlementReleaseStateDAO();
		}

		/// <summary>Creates a new SettlementResultDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementResult Entities</returns>
		public static SettlementResultDAO CreateSettlementResultDAO()
		{
			return new SettlementResultDAO();
		}

		/// <summary>Creates a new SettlementRunDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementRun Entities</returns>
		public static SettlementRunDAO CreateSettlementRunDAO()
		{
			return new SettlementRunDAO();
		}

		/// <summary>Creates a new SettlementRunValueDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementRunValue Entities</returns>
		public static SettlementRunValueDAO CreateSettlementRunValueDAO()
		{
			return new SettlementRunValueDAO();
		}

		/// <summary>Creates a new SettlementRunValueToVarioSettlementDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementRunValueToVarioSettlement Entities</returns>
		public static SettlementRunValueToVarioSettlementDAO CreateSettlementRunValueToVarioSettlementDAO()
		{
			return new SettlementRunValueToVarioSettlementDAO();
		}

		/// <summary>Creates a new SettlementSetupDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementSetup Entities</returns>
		public static SettlementSetupDAO CreateSettlementSetupDAO()
		{
			return new SettlementSetupDAO();
		}

		/// <summary>Creates a new SettlementTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for SettlementType Entities</returns>
		public static SettlementTypeDAO CreateSettlementTypeDAO()
		{
			return new SettlementTypeDAO();
		}

		/// <summary>Creates a new SourceDAO object</summary>
		/// <returns>the new DAO object ready to use for Source Entities</returns>
		public static SourceDAO CreateSourceDAO()
		{
			return new SourceDAO();
		}

		/// <summary>Creates a new StatementOfAccountDAO object</summary>
		/// <returns>the new DAO object ready to use for StatementOfAccount Entities</returns>
		public static StatementOfAccountDAO CreateStatementOfAccountDAO()
		{
			return new StatementOfAccountDAO();
		}

		/// <summary>Creates a new StateOfCountryDAO object</summary>
		/// <returns>the new DAO object ready to use for StateOfCountry Entities</returns>
		public static StateOfCountryDAO CreateStateOfCountryDAO()
		{
			return new StateOfCountryDAO();
		}

		/// <summary>Creates a new StockTransferDAO object</summary>
		/// <returns>the new DAO object ready to use for StockTransfer Entities</returns>
		public static StockTransferDAO CreateStockTransferDAO()
		{
			return new StockTransferDAO();
		}

		/// <summary>Creates a new StorageLocationDAO object</summary>
		/// <returns>the new DAO object ready to use for StorageLocation Entities</returns>
		public static StorageLocationDAO CreateStorageLocationDAO()
		{
			return new StorageLocationDAO();
		}

		/// <summary>Creates a new SubsidyLimitDAO object</summary>
		/// <returns>the new DAO object ready to use for SubsidyLimit Entities</returns>
		public static SubsidyLimitDAO CreateSubsidyLimitDAO()
		{
			return new SubsidyLimitDAO();
		}

		/// <summary>Creates a new SubsidyTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for SubsidyTransaction Entities</returns>
		public static SubsidyTransactionDAO CreateSubsidyTransactionDAO()
		{
			return new SubsidyTransactionDAO();
		}

		/// <summary>Creates a new SurveyDAO object</summary>
		/// <returns>the new DAO object ready to use for Survey Entities</returns>
		public static SurveyDAO CreateSurveyDAO()
		{
			return new SurveyDAO();
		}

		/// <summary>Creates a new SurveyAnswerDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyAnswer Entities</returns>
		public static SurveyAnswerDAO CreateSurveyAnswerDAO()
		{
			return new SurveyAnswerDAO();
		}

		/// <summary>Creates a new SurveyChoiceDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyChoice Entities</returns>
		public static SurveyChoiceDAO CreateSurveyChoiceDAO()
		{
			return new SurveyChoiceDAO();
		}

		/// <summary>Creates a new SurveyDescriptionDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyDescription Entities</returns>
		public static SurveyDescriptionDAO CreateSurveyDescriptionDAO()
		{
			return new SurveyDescriptionDAO();
		}

		/// <summary>Creates a new SurveyQuestionDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyQuestion Entities</returns>
		public static SurveyQuestionDAO CreateSurveyQuestionDAO()
		{
			return new SurveyQuestionDAO();
		}

		/// <summary>Creates a new SurveyResponseDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyResponse Entities</returns>
		public static SurveyResponseDAO CreateSurveyResponseDAO()
		{
			return new SurveyResponseDAO();
		}

		/// <summary>Creates a new TenantDAO object</summary>
		/// <returns>the new DAO object ready to use for Tenant Entities</returns>
		public static TenantDAO CreateTenantDAO()
		{
			return new TenantDAO();
		}

		/// <summary>Creates a new TenantHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for TenantHistory Entities</returns>
		public static TenantHistoryDAO CreateTenantHistoryDAO()
		{
			return new TenantHistoryDAO();
		}

		/// <summary>Creates a new TenantPersonDAO object</summary>
		/// <returns>the new DAO object ready to use for TenantPerson Entities</returns>
		public static TenantPersonDAO CreateTenantPersonDAO()
		{
			return new TenantPersonDAO();
		}

		/// <summary>Creates a new TenantStateDAO object</summary>
		/// <returns>the new DAO object ready to use for TenantState Entities</returns>
		public static TenantStateDAO CreateTenantStateDAO()
		{
			return new TenantStateDAO();
		}

		/// <summary>Creates a new TerminationStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminationStatus Entities</returns>
		public static TerminationStatusDAO CreateTerminationStatusDAO()
		{
			return new TerminationStatusDAO();
		}

		/// <summary>Creates a new TicketAssignmentDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketAssignment Entities</returns>
		public static TicketAssignmentDAO CreateTicketAssignmentDAO()
		{
			return new TicketAssignmentDAO();
		}

		/// <summary>Creates a new TicketAssignmentStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketAssignmentStatus Entities</returns>
		public static TicketAssignmentStatusDAO CreateTicketAssignmentStatusDAO()
		{
			return new TicketAssignmentStatusDAO();
		}

		/// <summary>Creates a new TicketSerialNumberDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketSerialNumber Entities</returns>
		public static TicketSerialNumberDAO CreateTicketSerialNumberDAO()
		{
			return new TicketSerialNumberDAO();
		}

		/// <summary>Creates a new TicketSerialNumberStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for TicketSerialNumberStatus Entities</returns>
		public static TicketSerialNumberStatusDAO CreateTicketSerialNumberStatusDAO()
		{
			return new TicketSerialNumberStatusDAO();
		}

		/// <summary>Creates a new TransactionJournalDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionJournal Entities</returns>
		public static TransactionJournalDAO CreateTransactionJournalDAO()
		{
			return new TransactionJournalDAO();
		}

		/// <summary>Creates a new TransactionJournalCorrectionDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionJournalCorrection Entities</returns>
		public static TransactionJournalCorrectionDAO CreateTransactionJournalCorrectionDAO()
		{
			return new TransactionJournalCorrectionDAO();
		}

		/// <summary>Creates a new TransactionJourneyHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionJourneyHistory Entities</returns>
		public static TransactionJourneyHistoryDAO CreateTransactionJourneyHistoryDAO()
		{
			return new TransactionJourneyHistoryDAO();
		}

		/// <summary>Creates a new TransactionToInspectionDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionToInspection Entities</returns>
		public static TransactionToInspectionDAO CreateTransactionToInspectionDAO()
		{
			return new TransactionToInspectionDAO();
		}

		/// <summary>Creates a new TransactionTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TransactionType Entities</returns>
		public static TransactionTypeDAO CreateTransactionTypeDAO()
		{
			return new TransactionTypeDAO();
		}

		/// <summary>Creates a new TransportCompanyDAO object</summary>
		/// <returns>the new DAO object ready to use for TransportCompany Entities</returns>
		public static TransportCompanyDAO CreateTransportCompanyDAO()
		{
			return new TransportCompanyDAO();
		}

		/// <summary>Creates a new TriggerTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for TriggerType Entities</returns>
		public static TriggerTypeDAO CreateTriggerTypeDAO()
		{
			return new TriggerTypeDAO();
		}

		/// <summary>Creates a new UnitCollectionDAO object</summary>
		/// <returns>the new DAO object ready to use for UnitCollection Entities</returns>
		public static UnitCollectionDAO CreateUnitCollectionDAO()
		{
			return new UnitCollectionDAO();
		}

		/// <summary>Creates a new UnitCollectionToUnitDAO object</summary>
		/// <returns>the new DAO object ready to use for UnitCollectionToUnit Entities</returns>
		public static UnitCollectionToUnitDAO CreateUnitCollectionToUnitDAO()
		{
			return new UnitCollectionToUnitDAO();
		}

		/// <summary>Creates a new ValidationResultDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationResult Entities</returns>
		public static ValidationResultDAO CreateValidationResultDAO()
		{
			return new ValidationResultDAO();
		}

		/// <summary>Creates a new ValidationResultResolveTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationResultResolveType Entities</returns>
		public static ValidationResultResolveTypeDAO CreateValidationResultResolveTypeDAO()
		{
			return new ValidationResultResolveTypeDAO();
		}

		/// <summary>Creates a new ValidationRuleDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationRule Entities</returns>
		public static ValidationRuleDAO CreateValidationRuleDAO()
		{
			return new ValidationRuleDAO();
		}

		/// <summary>Creates a new ValidationRunDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationRun Entities</returns>
		public static ValidationRunDAO CreateValidationRunDAO()
		{
			return new ValidationRunDAO();
		}

		/// <summary>Creates a new ValidationRunRuleDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationRunRule Entities</returns>
		public static ValidationRunRuleDAO CreateValidationRunRuleDAO()
		{
			return new ValidationRunRuleDAO();
		}

		/// <summary>Creates a new ValidationStatusDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationStatus Entities</returns>
		public static ValidationStatusDAO CreateValidationStatusDAO()
		{
			return new ValidationStatusDAO();
		}

		/// <summary>Creates a new ValidationValueDAO object</summary>
		/// <returns>the new DAO object ready to use for ValidationValue Entities</returns>
		public static ValidationValueDAO CreateValidationValueDAO()
		{
			return new ValidationValueDAO();
		}

		/// <summary>Creates a new VdvTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for VdvTransaction Entities</returns>
		public static VdvTransactionDAO CreateVdvTransactionDAO()
		{
			return new VdvTransactionDAO();
		}

		/// <summary>Creates a new VerificationAttemptDAO object</summary>
		/// <returns>the new DAO object ready to use for VerificationAttempt Entities</returns>
		public static VerificationAttemptDAO CreateVerificationAttemptDAO()
		{
			return new VerificationAttemptDAO();
		}

		/// <summary>Creates a new VerificationAttemptTypeDAO object</summary>
		/// <returns>the new DAO object ready to use for VerificationAttemptType Entities</returns>
		public static VerificationAttemptTypeDAO CreateVerificationAttemptTypeDAO()
		{
			return new VerificationAttemptTypeDAO();
		}

		/// <summary>Creates a new VirtualCardDAO object</summary>
		/// <returns>the new DAO object ready to use for VirtualCard Entities</returns>
		public static VirtualCardDAO CreateVirtualCardDAO()
		{
			return new VirtualCardDAO();
		}

		/// <summary>Creates a new VoucherDAO object</summary>
		/// <returns>the new DAO object ready to use for Voucher Entities</returns>
		public static VoucherDAO CreateVoucherDAO()
		{
			return new VoucherDAO();
		}

		/// <summary>Creates a new WhitelistDAO object</summary>
		/// <returns>the new DAO object ready to use for Whitelist Entities</returns>
		public static WhitelistDAO CreateWhitelistDAO()
		{
			return new WhitelistDAO();
		}

		/// <summary>Creates a new WhitelistJournalDAO object</summary>
		/// <returns>the new DAO object ready to use for WhitelistJournal Entities</returns>
		public static WhitelistJournalDAO CreateWhitelistJournalDAO()
		{
			return new WhitelistJournalDAO();
		}

		/// <summary>Creates a new WorkItemDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItem Entities</returns>
		public static WorkItemDAO CreateWorkItemDAO()
		{
			return new WorkItemDAO();
		}

		/// <summary>Creates a new WorkItemCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItemCategory Entities</returns>
		public static WorkItemCategoryDAO CreateWorkItemCategoryDAO()
		{
			return new WorkItemCategoryDAO();
		}

		/// <summary>Creates a new WorkItemHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItemHistory Entities</returns>
		public static WorkItemHistoryDAO CreateWorkItemHistoryDAO()
		{
			return new WorkItemHistoryDAO();
		}

		/// <summary>Creates a new WorkItemStateDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItemState Entities</returns>
		public static WorkItemStateDAO CreateWorkItemStateDAO()
		{
			return new WorkItemStateDAO();
		}

		/// <summary>Creates a new WorkItemSubjectDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItemSubject Entities</returns>
		public static WorkItemSubjectDAO CreateWorkItemSubjectDAO()
		{
			return new WorkItemSubjectDAO();
		}

		/// <summary>Creates a new WorkItemViewDAO object</summary>
		/// <returns>the new DAO object ready to use for WorkItemView Entities</returns>
		public static WorkItemViewDAO CreateWorkItemViewDAO()
		{
			return new WorkItemViewDAO();
		}

		/// <summary>Creates a new TypedListDAO object</summary>
		/// <returns>The new DAO object ready to use for Typed Lists</returns>
		public static TypedListDAO CreateTypedListDAO()
		{
			return new TypedListDAO();
		}

		#region Included Code

		#endregion
	}
}
