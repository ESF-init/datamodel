﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
////////////////////////////////////////////////////////////// 
using System;
using System.Linq;
using VarioSL.Entities.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace VarioSL.Entities.FactoryClasses
{
	/// <summary>Factory class to produce DynamicQuery instances and EntityQuery instances</summary>
	public partial class QueryFactory
	{
		private int _aliasCounter = 0;

		/// <summary>Creates a new DynamicQuery instance with no alias set.</summary>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create()
		{
			return Create(string.Empty);
		}

		/// <summary>Creates a new DynamicQuery instance with the alias specified as the alias set.</summary>
		/// <param name="alias">The alias.</param>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create(string alias)
		{
			return new DynamicQuery(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}

		/// <summary>Creates a new DynamicQuery which wraps the specified TableValuedFunction call</summary>
		/// <param name="toWrap">The table valued function call to wrap.</param>
		/// <returns>toWrap wrapped in a DynamicQuery.</returns>
		public DynamicQuery Create(TableValuedFunctionCall toWrap)
		{
			return this.Create().From(new TvfCallWrapper(toWrap)).Select(toWrap.GetFieldsAsArray().Select(f => this.Field(toWrap.Alias, f.Alias)).ToArray());
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with no alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>()
			where TEntity : IEntityCore
		{
			return Create<TEntity>(string.Empty);
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with the alias specified as the alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <param name="alias">The alias.</param>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>(string alias)
			where TEntity : IEntityCore
		{
			return new EntityQuery<TEntity>(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
				
		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string fieldName)
		{
			return Field<object>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string targetAlias, string fieldName)
		{
			return Field<object>(targetAlias, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value represented by the field.</typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string fieldName)
		{
			return Field<TValue>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string targetAlias, string fieldName)
		{
			return new EntityField(fieldName, targetAlias, typeof(TValue));
		}
						
		/// <summary>Gets the next alias counter value to produce artifical aliases with</summary>
		private int GetNextAliasCounterValue()
		{
			_aliasCounter++;
			return _aliasCounter;
		}
		

		/// <summary>Creates and returns a new EntityQuery for the AbortedTransaction entity</summary>
		public EntityQuery<AbortedTransactionEntity> AbortedTransaction
		{
			get { return Create<AbortedTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountBalance entity</summary>
		public EntityQuery<AccountBalanceEntity> AccountBalance
		{
			get { return Create<AccountBalanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountEntry entity</summary>
		public EntityQuery<AccountEntryEntity> AccountEntry
		{
			get { return Create<AccountEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountEntryNumber entity</summary>
		public EntityQuery<AccountEntryNumberEntity> AccountEntryNumber
		{
			get { return Create<AccountEntryNumberEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountPostingKey entity</summary>
		public EntityQuery<AccountPostingKeyEntity> AccountPostingKey
		{
			get { return Create<AccountPostingKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Application entity</summary>
		public EntityQuery<ApplicationEntity> Application
		{
			get { return Create<ApplicationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ApplicationVersion entity</summary>
		public EntityQuery<ApplicationVersionEntity> ApplicationVersion
		{
			get { return Create<ApplicationVersionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Apportionment entity</summary>
		public EntityQuery<ApportionmentEntity> Apportionment
		{
			get { return Create<ApportionmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ApportionmentResult entity</summary>
		public EntityQuery<ApportionmentResultEntity> ApportionmentResult
		{
			get { return Create<ApportionmentResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AreaList entity</summary>
		public EntityQuery<AreaListEntity> AreaList
		{
			get { return Create<AreaListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AreaListElement entity</summary>
		public EntityQuery<AreaListElementEntity> AreaListElement
		{
			get { return Create<AreaListElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AreaListElementGroup entity</summary>
		public EntityQuery<AreaListElementGroupEntity> AreaListElementGroup
		{
			get { return Create<AreaListElementGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AreaType entity</summary>
		public EntityQuery<AreaTypeEntity> AreaType
		{
			get { return Create<AreaTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Attribute entity</summary>
		public EntityQuery<AttributeEntity> Attribute
		{
			get { return Create<AttributeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttributeBitmapLayoutObject entity</summary>
		public EntityQuery<AttributeBitmapLayoutObjectEntity> AttributeBitmapLayoutObject
		{
			get { return Create<AttributeBitmapLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttributeTextLayoutObject entity</summary>
		public EntityQuery<AttributeTextLayoutObjectEntity> AttributeTextLayoutObject
		{
			get { return Create<AttributeTextLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttributeValue entity</summary>
		public EntityQuery<AttributeValueEntity> AttributeValue
		{
			get { return Create<AttributeValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttributeValueList entity</summary>
		public EntityQuery<AttributeValueListEntity> AttributeValueList
		{
			get { return Create<AttributeValueListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Automat entity</summary>
		public EntityQuery<AutomatEntity> Automat
		{
			get { return Create<AutomatEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Bank entity</summary>
		public EntityQuery<BankEntity> Bank
		{
			get { return Create<BankEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BinaryData entity</summary>
		public EntityQuery<BinaryDataEntity> BinaryData
		{
			get { return Create<BinaryDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BlockingReason entity</summary>
		public EntityQuery<BlockingReasonEntity> BlockingReason
		{
			get { return Create<BlockingReasonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRule entity</summary>
		public EntityQuery<BusinessRuleEntity> BusinessRule
		{
			get { return Create<BusinessRuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleClause entity</summary>
		public EntityQuery<BusinessRuleClauseEntity> BusinessRuleClause
		{
			get { return Create<BusinessRuleClauseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleCondition entity</summary>
		public EntityQuery<BusinessRuleConditionEntity> BusinessRuleCondition
		{
			get { return Create<BusinessRuleConditionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleResult entity</summary>
		public EntityQuery<BusinessRuleResultEntity> BusinessRuleResult
		{
			get { return Create<BusinessRuleResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleType entity</summary>
		public EntityQuery<BusinessRuleTypeEntity> BusinessRuleType
		{
			get { return Create<BusinessRuleTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleTypeToVariable entity</summary>
		public EntityQuery<BusinessRuleTypeToVariableEntity> BusinessRuleTypeToVariable
		{
			get { return Create<BusinessRuleTypeToVariableEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BusinessRuleVariable entity</summary>
		public EntityQuery<BusinessRuleVariableEntity> BusinessRuleVariable
		{
			get { return Create<BusinessRuleVariableEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Calendar entity</summary>
		public EntityQuery<CalendarEntity> Calendar
		{
			get { return Create<CalendarEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CalendarEntry entity</summary>
		public EntityQuery<CalendarEntryEntity> CalendarEntry
		{
			get { return Create<CalendarEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardActionAttribute entity</summary>
		public EntityQuery<CardActionAttributeEntity> CardActionAttribute
		{
			get { return Create<CardActionAttributeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardActionRequest entity</summary>
		public EntityQuery<CardActionRequestEntity> CardActionRequest
		{
			get { return Create<CardActionRequestEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardActionRequestType entity</summary>
		public EntityQuery<CardActionRequestTypeEntity> CardActionRequestType
		{
			get { return Create<CardActionRequestTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardChipType entity</summary>
		public EntityQuery<CardChipTypeEntity> CardChipType
		{
			get { return Create<CardChipTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardState entity</summary>
		public EntityQuery<CardStateEntity> CardState
		{
			get { return Create<CardStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardTicket entity</summary>
		public EntityQuery<CardTicketEntity> CardTicket
		{
			get { return Create<CardTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardTicketToTicket entity</summary>
		public EntityQuery<CardTicketToTicketEntity> CardTicketToTicket
		{
			get { return Create<CardTicketToTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CashService entity</summary>
		public EntityQuery<CashServiceEntity> CashService
		{
			get { return Create<CashServiceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CashServiceBalance entity</summary>
		public EntityQuery<CashServiceBalanceEntity> CashServiceBalance
		{
			get { return Create<CashServiceBalanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CashServiceData entity</summary>
		public EntityQuery<CashServiceDataEntity> CashServiceData
		{
			get { return Create<CashServiceDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CashUnit entity</summary>
		public EntityQuery<CashUnitEntity> CashUnit
		{
			get { return Create<CashUnitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Choice entity</summary>
		public EntityQuery<ChoiceEntity> Choice
		{
			get { return Create<ChoiceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Clearing entity</summary>
		public EntityQuery<ClearingEntity> Clearing
		{
			get { return Create<ClearingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingClassification entity</summary>
		public EntityQuery<ClearingClassificationEntity> ClearingClassification
		{
			get { return Create<ClearingClassificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingDetail entity</summary>
		public EntityQuery<ClearingDetailEntity> ClearingDetail
		{
			get { return Create<ClearingDetailEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingResult entity</summary>
		public EntityQuery<ClearingResultEntity> ClearingResult
		{
			get { return Create<ClearingResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingResultLevel entity</summary>
		public EntityQuery<ClearingResultLevelEntity> ClearingResultLevel
		{
			get { return Create<ClearingResultLevelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingState entity</summary>
		public EntityQuery<ClearingStateEntity> ClearingState
		{
			get { return Create<ClearingStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingSum entity</summary>
		public EntityQuery<ClearingSumEntity> ClearingSum
		{
			get { return Create<ClearingSumEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClearingTransaction entity</summary>
		public EntityQuery<ClearingTransactionEntity> ClearingTransaction
		{
			get { return Create<ClearingTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Client entity</summary>
		public EntityQuery<ClientEntity> Client
		{
			get { return Create<ClientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientAdaptedLayoutObject entity</summary>
		public EntityQuery<ClientAdaptedLayoutObjectEntity> ClientAdaptedLayoutObject
		{
			get { return Create<ClientAdaptedLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Component entity</summary>
		public EntityQuery<ComponentEntity> Component
		{
			get { return Create<ComponentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ComponentClearing entity</summary>
		public EntityQuery<ComponentClearingEntity> ComponentClearing
		{
			get { return Create<ComponentClearingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ComponentFilling entity</summary>
		public EntityQuery<ComponentFillingEntity> ComponentFilling
		{
			get { return Create<ComponentFillingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ComponentState entity</summary>
		public EntityQuery<ComponentStateEntity> ComponentState
		{
			get { return Create<ComponentStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ComponentType entity</summary>
		public EntityQuery<ComponentTypeEntity> ComponentType
		{
			get { return Create<ComponentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ConditionalSubPageLayoutObject entity</summary>
		public EntityQuery<ConditionalSubPageLayoutObjectEntity> ConditionalSubPageLayoutObject
		{
			get { return Create<ConditionalSubPageLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditScreening entity</summary>
		public EntityQuery<CreditScreeningEntity> CreditScreening
		{
			get { return Create<CreditScreeningEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DayType entity</summary>
		public EntityQuery<DayTypeEntity> DayType
		{
			get { return Create<DayTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Debtor entity</summary>
		public EntityQuery<DebtorEntity> Debtor
		{
			get { return Create<DebtorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DebtorCard entity</summary>
		public EntityQuery<DebtorCardEntity> DebtorCard
		{
			get { return Create<DebtorCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DebtorCardHistory entity</summary>
		public EntityQuery<DebtorCardHistoryEntity> DebtorCardHistory
		{
			get { return Create<DebtorCardHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DefaultPin entity</summary>
		public EntityQuery<DefaultPinEntity> DefaultPin
		{
			get { return Create<DefaultPinEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Depot entity</summary>
		public EntityQuery<DepotEntity> Depot
		{
			get { return Create<DepotEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Device entity</summary>
		public EntityQuery<DeviceEntity> Device
		{
			get { return Create<DeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceBookingState entity</summary>
		public EntityQuery<DeviceBookingStateEntity> DeviceBookingState
		{
			get { return Create<DeviceBookingStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceClass entity</summary>
		public EntityQuery<DeviceClassEntity> DeviceClass
		{
			get { return Create<DeviceClassEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DevicePaymentMethod entity</summary>
		public EntityQuery<DevicePaymentMethodEntity> DevicePaymentMethod
		{
			get { return Create<DevicePaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Direction entity</summary>
		public EntityQuery<DirectionEntity> Direction
		{
			get { return Create<DirectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningLevel entity</summary>
		public EntityQuery<DunningLevelEntity> DunningLevel
		{
			get { return Create<DunningLevelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Eticket entity</summary>
		public EntityQuery<EticketEntity> Eticket
		{
			get { return Create<EticketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExportInfo entity</summary>
		public EntityQuery<ExportInfoEntity> ExportInfo
		{
			get { return Create<ExportInfoEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalCard entity</summary>
		public EntityQuery<ExternalCardEntity> ExternalCard
		{
			get { return Create<ExternalCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalData entity</summary>
		public EntityQuery<ExternalDataEntity> ExternalData
		{
			get { return Create<ExternalDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalEffort entity</summary>
		public EntityQuery<ExternalEffortEntity> ExternalEffort
		{
			get { return Create<ExternalEffortEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalPacket entity</summary>
		public EntityQuery<ExternalPacketEntity> ExternalPacket
		{
			get { return Create<ExternalPacketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalPacketEffort entity</summary>
		public EntityQuery<ExternalPacketEffortEntity> ExternalPacketEffort
		{
			get { return Create<ExternalPacketEffortEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalType entity</summary>
		public EntityQuery<ExternalTypeEntity> ExternalType
		{
			get { return Create<ExternalTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionCategory entity</summary>
		public EntityQuery<FareEvasionCategoryEntity> FareEvasionCategory
		{
			get { return Create<FareEvasionCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionReason entity</summary>
		public EntityQuery<FareEvasionReasonEntity> FareEvasionReason
		{
			get { return Create<FareEvasionReasonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionReasonToCategory entity</summary>
		public EntityQuery<FareEvasionReasonToCategoryEntity> FareEvasionReasonToCategory
		{
			get { return Create<FareEvasionReasonToCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareMatrix entity</summary>
		public EntityQuery<FareMatrixEntity> FareMatrix
		{
			get { return Create<FareMatrixEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareMatrixEntry entity</summary>
		public EntityQuery<FareMatrixEntryEntity> FareMatrixEntry
		{
			get { return Create<FareMatrixEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareStage entity</summary>
		public EntityQuery<FareStageEntity> FareStage
		{
			get { return Create<FareStageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareStageAlias entity</summary>
		public EntityQuery<FareStageAliasEntity> FareStageAlias
		{
			get { return Create<FareStageAliasEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareStageHierarchieLevel entity</summary>
		public EntityQuery<FareStageHierarchieLevelEntity> FareStageHierarchieLevel
		{
			get { return Create<FareStageHierarchieLevelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareStageList entity</summary>
		public EntityQuery<FareStageListEntity> FareStageList
		{
			get { return Create<FareStageListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareStageType entity</summary>
		public EntityQuery<FareStageTypeEntity> FareStageType
		{
			get { return Create<FareStageTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareTable entity</summary>
		public EntityQuery<FareTableEntity> FareTable
		{
			get { return Create<FareTableEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareTableEntry entity</summary>
		public EntityQuery<FareTableEntryEntity> FareTableEntry
		{
			get { return Create<FareTableEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FixedBitmapLayoutObject entity</summary>
		public EntityQuery<FixedBitmapLayoutObjectEntity> FixedBitmapLayoutObject
		{
			get { return Create<FixedBitmapLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FixedTextLayoutObject entity</summary>
		public EntityQuery<FixedTextLayoutObjectEntity> FixedTextLayoutObject
		{
			get { return Create<FixedTextLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GuiDef entity</summary>
		public EntityQuery<GuiDefEntity> GuiDef
		{
			get { return Create<GuiDefEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the KeyAttributeTransfrom entity</summary>
		public EntityQuery<KeyAttributeTransfromEntity> KeyAttributeTransfrom
		{
			get { return Create<KeyAttributeTransfromEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the KeySelectionMode entity</summary>
		public EntityQuery<KeySelectionModeEntity> KeySelectionMode
		{
			get { return Create<KeySelectionModeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the KVVSubscription entity</summary>
		public EntityQuery<KVVSubscriptionEntity> KVVSubscription
		{
			get { return Create<KVVSubscriptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Language entity</summary>
		public EntityQuery<LanguageEntity> Language
		{
			get { return Create<LanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Layout entity</summary>
		public EntityQuery<LayoutEntity> Layout
		{
			get { return Create<LayoutEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Line entity</summary>
		public EntityQuery<LineEntity> Line
		{
			get { return Create<LineEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LineGroup entity</summary>
		public EntityQuery<LineGroupEntity> LineGroup
		{
			get { return Create<LineGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LineGroupToLineGroup entity</summary>
		public EntityQuery<LineGroupToLineGroupEntity> LineGroupToLineGroup
		{
			get { return Create<LineGroupToLineGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LineToLineGroup entity</summary>
		public EntityQuery<LineToLineGroupEntity> LineToLineGroup
		{
			get { return Create<LineToLineGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ListLayoutObject entity</summary>
		public EntityQuery<ListLayoutObjectEntity> ListLayoutObject
		{
			get { return Create<ListLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ListLayoutType entity</summary>
		public EntityQuery<ListLayoutTypeEntity> ListLayoutType
		{
			get { return Create<ListLayoutTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Logo entity</summary>
		public EntityQuery<LogoEntity> Logo
		{
			get { return Create<LogoEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Net entity</summary>
		public EntityQuery<NetEntity> Net
		{
			get { return Create<NetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NumberRange entity</summary>
		public EntityQuery<NumberRangeEntity> NumberRange
		{
			get { return Create<NumberRangeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Operator entity</summary>
		public EntityQuery<OperatorEntity> Operator
		{
			get { return Create<OperatorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OutputDevice entity</summary>
		public EntityQuery<OutputDeviceEntity> OutputDevice
		{
			get { return Create<OutputDeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageContentGroup entity</summary>
		public EntityQuery<PageContentGroupEntity> PageContentGroup
		{
			get { return Create<PageContentGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageContentGroupToContent entity</summary>
		public EntityQuery<PageContentGroupToContentEntity> PageContentGroupToContent
		{
			get { return Create<PageContentGroupToContentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Panel entity</summary>
		public EntityQuery<PanelEntity> Panel
		{
			get { return Create<PanelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterAttributeValue entity</summary>
		public EntityQuery<ParameterAttributeValueEntity> ParameterAttributeValue
		{
			get { return Create<ParameterAttributeValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterFareStage entity</summary>
		public EntityQuery<ParameterFareStageEntity> ParameterFareStage
		{
			get { return Create<ParameterFareStageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterTariff entity</summary>
		public EntityQuery<ParameterTariffEntity> ParameterTariff
		{
			get { return Create<ParameterTariffEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterTicket entity</summary>
		public EntityQuery<ParameterTicketEntity> ParameterTicket
		{
			get { return Create<ParameterTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentDetail entity</summary>
		public EntityQuery<PaymentDetailEntity> PaymentDetail
		{
			get { return Create<PaymentDetailEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentInterval entity</summary>
		public EntityQuery<PaymentIntervalEntity> PaymentInterval
		{
			get { return Create<PaymentIntervalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PointOfSale entity</summary>
		public EntityQuery<PointOfSaleEntity> PointOfSale
		{
			get { return Create<PointOfSaleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PredefinedKey entity</summary>
		public EntityQuery<PredefinedKeyEntity> PredefinedKey
		{
			get { return Create<PredefinedKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceType entity</summary>
		public EntityQuery<PriceTypeEntity> PriceType
		{
			get { return Create<PriceTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PrimalKey entity</summary>
		public EntityQuery<PrimalKeyEntity> PrimalKey
		{
			get { return Create<PrimalKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PrintText entity</summary>
		public EntityQuery<PrintTextEntity> PrintText
		{
			get { return Create<PrintTextEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductOnCard entity</summary>
		public EntityQuery<ProductOnCardEntity> ProductOnCard
		{
			get { return Create<ProductOnCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Protocol entity</summary>
		public EntityQuery<ProtocolEntity> Protocol
		{
			get { return Create<ProtocolEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProtocolAction entity</summary>
		public EntityQuery<ProtocolActionEntity> ProtocolAction
		{
			get { return Create<ProtocolActionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProtocolFunction entity</summary>
		public EntityQuery<ProtocolFunctionEntity> ProtocolFunction
		{
			get { return Create<ProtocolFunctionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProtocolFunctionGroup entity</summary>
		public EntityQuery<ProtocolFunctionGroupEntity> ProtocolFunctionGroup
		{
			get { return Create<ProtocolFunctionGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProtocolMessage entity</summary>
		public EntityQuery<ProtocolMessageEntity> ProtocolMessage
		{
			get { return Create<ProtocolMessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Qualification entity</summary>
		public EntityQuery<QualificationEntity> Qualification
		{
			get { return Create<QualificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Responsibility entity</summary>
		public EntityQuery<ResponsibilityEntity> Responsibility
		{
			get { return Create<ResponsibilityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RevenueTransactionType entity</summary>
		public EntityQuery<RevenueTransactionTypeEntity> RevenueTransactionType
		{
			get { return Create<RevenueTransactionTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RmPayment entity</summary>
		public EntityQuery<RmPaymentEntity> RmPayment
		{
			get { return Create<RmPaymentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Route entity</summary>
		public EntityQuery<RouteEntity> Route
		{
			get { return Create<RouteEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RouteName entity</summary>
		public EntityQuery<RouteNameEntity> RouteName
		{
			get { return Create<RouteNameEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleCapping entity</summary>
		public EntityQuery<RuleCappingEntity> RuleCapping
		{
			get { return Create<RuleCappingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleCappingToFtEntry entity</summary>
		public EntityQuery<RuleCappingToFtEntryEntity> RuleCappingToFtEntry
		{
			get { return Create<RuleCappingToFtEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleCappingToTicket entity</summary>
		public EntityQuery<RuleCappingToTicketEntity> RuleCappingToTicket
		{
			get { return Create<RuleCappingToTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RulePeriod entity</summary>
		public EntityQuery<RulePeriodEntity> RulePeriod
		{
			get { return Create<RulePeriodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleType entity</summary>
		public EntityQuery<RuleTypeEntity> RuleType
		{
			get { return Create<RuleTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Salutation entity</summary>
		public EntityQuery<SalutationEntity> Salutation
		{
			get { return Create<SalutationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServiceAllocation entity</summary>
		public EntityQuery<ServiceAllocationEntity> ServiceAllocation
		{
			get { return Create<ServiceAllocationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServiceIdToCard entity</summary>
		public EntityQuery<ServiceIdToCardEntity> ServiceIdToCard
		{
			get { return Create<ServiceIdToCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServicePercentage entity</summary>
		public EntityQuery<ServicePercentageEntity> ServicePercentage
		{
			get { return Create<ServicePercentageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Shift entity</summary>
		public EntityQuery<ShiftEntity> Shift
		{
			get { return Create<ShiftEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ShiftInventory entity</summary>
		public EntityQuery<ShiftInventoryEntity> ShiftInventory
		{
			get { return Create<ShiftInventoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ShiftState entity</summary>
		public EntityQuery<ShiftStateEntity> ShiftState
		{
			get { return Create<ShiftStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ShortDistance entity</summary>
		public EntityQuery<ShortDistanceEntity> ShortDistance
		{
			get { return Create<ShortDistanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SpecialReceipt entity</summary>
		public EntityQuery<SpecialReceiptEntity> SpecialReceipt
		{
			get { return Create<SpecialReceiptEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Stop entity</summary>
		public EntityQuery<StopEntity> Stop
		{
			get { return Create<StopEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SystemField entity</summary>
		public EntityQuery<SystemFieldEntity> SystemField
		{
			get { return Create<SystemFieldEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SystemFieldBarcodeLayoutObject entity</summary>
		public EntityQuery<SystemFieldBarcodeLayoutObjectEntity> SystemFieldBarcodeLayoutObject
		{
			get { return Create<SystemFieldBarcodeLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SystemFieldDynamicGraphicLayoutObject entity</summary>
		public EntityQuery<SystemFieldDynamicGraphicLayoutObjectEntity> SystemFieldDynamicGraphicLayoutObject
		{
			get { return Create<SystemFieldDynamicGraphicLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SystemFieldTextLayoutObject entity</summary>
		public EntityQuery<SystemFieldTextLayoutObjectEntity> SystemFieldTextLayoutObject
		{
			get { return Create<SystemFieldTextLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SystemText entity</summary>
		public EntityQuery<SystemTextEntity> SystemText
		{
			get { return Create<SystemTextEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Tariff entity</summary>
		public EntityQuery<TariffEntity> Tariff
		{
			get { return Create<TariffEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TariffParameter entity</summary>
		public EntityQuery<TariffParameterEntity> TariffParameter
		{
			get { return Create<TariffParameterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TariffRelease entity</summary>
		public EntityQuery<TariffReleaseEntity> TariffRelease
		{
			get { return Create<TariffReleaseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TemporalType entity</summary>
		public EntityQuery<TemporalTypeEntity> TemporalType
		{
			get { return Create<TemporalTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Ticket entity</summary>
		public EntityQuery<TicketEntity> Ticket
		{
			get { return Create<TicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketCancellationType entity</summary>
		public EntityQuery<TicketCancellationTypeEntity> TicketCancellationType
		{
			get { return Create<TicketCancellationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketCategory entity</summary>
		public EntityQuery<TicketCategoryEntity> TicketCategory
		{
			get { return Create<TicketCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketDayType entity</summary>
		public EntityQuery<TicketDayTypeEntity> TicketDayType
		{
			get { return Create<TicketDayTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketDeviceClass entity</summary>
		public EntityQuery<TicketDeviceClassEntity> TicketDeviceClass
		{
			get { return Create<TicketDeviceClassEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketDeviceClassOutputDevice entity</summary>
		public EntityQuery<TicketDeviceClassOutputDeviceEntity> TicketDeviceClassOutputDevice
		{
			get { return Create<TicketDeviceClassOutputDeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketDeviceClassPaymentMethod entity</summary>
		public EntityQuery<TicketDeviceClassPaymentMethodEntity> TicketDeviceClassPaymentMethod
		{
			get { return Create<TicketDeviceClassPaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketDevicePaymentMethod entity</summary>
		public EntityQuery<TicketDevicePaymentMethodEntity> TicketDevicePaymentMethod
		{
			get { return Create<TicketDevicePaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketGroup entity</summary>
		public EntityQuery<TicketGroupEntity> TicketGroup
		{
			get { return Create<TicketGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketOrganization entity</summary>
		public EntityQuery<TicketOrganizationEntity> TicketOrganization
		{
			get { return Create<TicketOrganizationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketOutputdevice entity</summary>
		public EntityQuery<TicketOutputdeviceEntity> TicketOutputdevice
		{
			get { return Create<TicketOutputdeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketPaymentInterval entity</summary>
		public EntityQuery<TicketPaymentIntervalEntity> TicketPaymentInterval
		{
			get { return Create<TicketPaymentIntervalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketPhysicalCardType entity</summary>
		public EntityQuery<TicketPhysicalCardTypeEntity> TicketPhysicalCardType
		{
			get { return Create<TicketPhysicalCardTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketSelectionMode entity</summary>
		public EntityQuery<TicketSelectionModeEntity> TicketSelectionMode
		{
			get { return Create<TicketSelectionModeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketServicesPermitted entity</summary>
		public EntityQuery<TicketServicesPermittedEntity> TicketServicesPermitted
		{
			get { return Create<TicketServicesPermittedEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketToGroup entity</summary>
		public EntityQuery<TicketToGroupEntity> TicketToGroup
		{
			get { return Create<TicketToGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketType entity</summary>
		public EntityQuery<TicketTypeEntity> TicketType
		{
			get { return Create<TicketTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketVendingClient entity</summary>
		public EntityQuery<TicketVendingClientEntity> TicketVendingClient
		{
			get { return Create<TicketVendingClientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Transaction entity</summary>
		public EntityQuery<TransactionEntity> Transaction
		{
			get { return Create<TransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionAdditional entity</summary>
		public EntityQuery<TransactionAdditionalEntity> TransactionAdditional
		{
			get { return Create<TransactionAdditionalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionBackup entity</summary>
		public EntityQuery<TransactionBackupEntity> TransactionBackup
		{
			get { return Create<TransactionBackupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionExtension entity</summary>
		public EntityQuery<TransactionExtensionEntity> TransactionExtension
		{
			get { return Create<TransactionExtensionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Translation entity</summary>
		public EntityQuery<TranslationEntity> Translation
		{
			get { return Create<TranslationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TypeOfCard entity</summary>
		public EntityQuery<TypeOfCardEntity> TypeOfCard
		{
			get { return Create<TypeOfCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TypeOfUnit entity</summary>
		public EntityQuery<TypeOfUnitEntity> TypeOfUnit
		{
			get { return Create<TypeOfUnitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Unit entity</summary>
		public EntityQuery<UnitEntity> Unit
		{
			get { return Create<UnitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserConfig entity</summary>
		public EntityQuery<UserConfigEntity> UserConfig
		{
			get { return Create<UserConfigEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserGroup entity</summary>
		public EntityQuery<UserGroupEntity> UserGroup
		{
			get { return Create<UserGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserGroupRight entity</summary>
		public EntityQuery<UserGroupRightEntity> UserGroupRight
		{
			get { return Create<UserGroupRightEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserIsMember entity</summary>
		public EntityQuery<UserIsMemberEntity> UserIsMember
		{
			get { return Create<UserIsMemberEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserKey entity</summary>
		public EntityQuery<UserKeyEntity> UserKey
		{
			get { return Create<UserKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserLanguage entity</summary>
		public EntityQuery<UserLanguageEntity> UserLanguage
		{
			get { return Create<UserLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserList entity</summary>
		public EntityQuery<UserListEntity> UserList
		{
			get { return Create<UserListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserResource entity</summary>
		public EntityQuery<UserResourceEntity> UserResource
		{
			get { return Create<UserResourceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserStatus entity</summary>
		public EntityQuery<UserStatusEntity> UserStatus
		{
			get { return Create<UserStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserToWorkstation entity</summary>
		public EntityQuery<UserToWorkstationEntity> UserToWorkstation
		{
			get { return Create<UserToWorkstationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationTransaction entity</summary>
		public EntityQuery<ValidationTransactionEntity> ValidationTransaction
		{
			get { return Create<ValidationTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VarioAddress entity</summary>
		public EntityQuery<VarioAddressEntity> VarioAddress
		{
			get { return Create<VarioAddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VarioSettlement entity</summary>
		public EntityQuery<VarioSettlementEntity> VarioSettlement
		{
			get { return Create<VarioSettlementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VarioTypeOfSettlement entity</summary>
		public EntityQuery<VarioTypeOfSettlementEntity> VarioTypeOfSettlement
		{
			get { return Create<VarioTypeOfSettlementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvKeyInfo entity</summary>
		public EntityQuery<VdvKeyInfoEntity> VdvKeyInfo
		{
			get { return Create<VdvKeyInfoEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvKeySet entity</summary>
		public EntityQuery<VdvKeySetEntity> VdvKeySet
		{
			get { return Create<VdvKeySetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvLayout entity</summary>
		public EntityQuery<VdvLayoutEntity> VdvLayout
		{
			get { return Create<VdvLayoutEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvLayoutObject entity</summary>
		public EntityQuery<VdvLayoutObjectEntity> VdvLayoutObject
		{
			get { return Create<VdvLayoutObjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvLoadKeyMessage entity</summary>
		public EntityQuery<VdvLoadKeyMessageEntity> VdvLoadKeyMessage
		{
			get { return Create<VdvLoadKeyMessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvProduct entity</summary>
		public EntityQuery<VdvProductEntity> VdvProduct
		{
			get { return Create<VdvProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvSamStatus entity</summary>
		public EntityQuery<VdvSamStatusEntity> VdvSamStatus
		{
			get { return Create<VdvSamStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvTag entity</summary>
		public EntityQuery<VdvTagEntity> VdvTag
		{
			get { return Create<VdvTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvTerminal entity</summary>
		public EntityQuery<VdvTerminalEntity> VdvTerminal
		{
			get { return Create<VdvTerminalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvType entity</summary>
		public EntityQuery<VdvTypeEntity> VdvType
		{
			get { return Create<VdvTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Workstation entity</summary>
		public EntityQuery<WorkstationEntity> Workstation
		{
			get { return Create<WorkstationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingCancelRecTapCmlSettled entity</summary>
		public EntityQuery<AccountingCancelRecTapCmlSettledEntity> AccountingCancelRecTapCmlSettled
		{
			get { return Create<AccountingCancelRecTapCmlSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingCancelRecTapTabSettled entity</summary>
		public EntityQuery<AccountingCancelRecTapTabSettledEntity> AccountingCancelRecTapTabSettled
		{
			get { return Create<AccountingCancelRecTapTabSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingMethod entity</summary>
		public EntityQuery<AccountingMethodEntity> AccountingMethod
		{
			get { return Create<AccountingMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingMode entity</summary>
		public EntityQuery<AccountingModeEntity> AccountingMode
		{
			get { return Create<AccountingModeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecLoadUseCmlSettled entity</summary>
		public EntityQuery<AccountingRecLoadUseCmlSettledEntity> AccountingRecLoadUseCmlSettled
		{
			get { return Create<AccountingRecLoadUseCmlSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecLoadUseTabSettled entity</summary>
		public EntityQuery<AccountingRecLoadUseTabSettledEntity> AccountingRecLoadUseTabSettled
		{
			get { return Create<AccountingRecLoadUseTabSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedLoadAndUse entity</summary>
		public EntityQuery<AccountingRecognizedLoadAndUseEntity> AccountingRecognizedLoadAndUse
		{
			get { return Create<AccountingRecognizedLoadAndUseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedPayment entity</summary>
		public EntityQuery<AccountingRecognizedPaymentEntity> AccountingRecognizedPayment
		{
			get { return Create<AccountingRecognizedPaymentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedTap entity</summary>
		public EntityQuery<AccountingRecognizedTapEntity> AccountingRecognizedTap
		{
			get { return Create<AccountingRecognizedTapEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedTapCancellation entity</summary>
		public EntityQuery<AccountingRecognizedTapCancellationEntity> AccountingRecognizedTapCancellation
		{
			get { return Create<AccountingRecognizedTapCancellationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedTapCmlSettled entity</summary>
		public EntityQuery<AccountingRecognizedTapCmlSettledEntity> AccountingRecognizedTapCmlSettled
		{
			get { return Create<AccountingRecognizedTapCmlSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingRecognizedTapTabSettled entity</summary>
		public EntityQuery<AccountingRecognizedTapTabSettledEntity> AccountingRecognizedTapTabSettled
		{
			get { return Create<AccountingRecognizedTapTabSettledEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountingReconciledPayment entity</summary>
		public EntityQuery<AccountingReconciledPaymentEntity> AccountingReconciledPayment
		{
			get { return Create<AccountingReconciledPaymentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountMessageSetting entity</summary>
		public EntityQuery<AccountMessageSettingEntity> AccountMessageSetting
		{
			get { return Create<AccountMessageSettingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdditionalData entity</summary>
		public EntityQuery<AdditionalDataEntity> AdditionalData
		{
			get { return Create<AdditionalDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Address entity</summary>
		public EntityQuery<AddressEntity> Address
		{
			get { return Create<AddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AddressBookEntry entity</summary>
		public EntityQuery<AddressBookEntryEntity> AddressBookEntry
		{
			get { return Create<AddressBookEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AddressBookEntryType entity</summary>
		public EntityQuery<AddressBookEntryTypeEntity> AddressBookEntryType
		{
			get { return Create<AddressBookEntryTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AddressType entity</summary>
		public EntityQuery<AddressTypeEntity> AddressType
		{
			get { return Create<AddressTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AggregationFunction entity</summary>
		public EntityQuery<AggregationFunctionEntity> AggregationFunction
		{
			get { return Create<AggregationFunctionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AggregationType entity</summary>
		public EntityQuery<AggregationTypeEntity> AggregationType
		{
			get { return Create<AggregationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Apportion entity</summary>
		public EntityQuery<ApportionEntity> Apportion
		{
			get { return Create<ApportionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ApportionPassRevenueRecognition entity</summary>
		public EntityQuery<ApportionPassRevenueRecognitionEntity> ApportionPassRevenueRecognition
		{
			get { return Create<ApportionPassRevenueRecognitionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ArchiveState entity</summary>
		public EntityQuery<ArchiveStateEntity> ArchiveState
		{
			get { return Create<ArchiveStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttributeToMobilityProvider entity</summary>
		public EntityQuery<AttributeToMobilityProviderEntity> AttributeToMobilityProvider
		{
			get { return Create<AttributeToMobilityProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AuditRegister entity</summary>
		public EntityQuery<AuditRegisterEntity> AuditRegister
		{
			get { return Create<AuditRegisterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AuditRegisterValue entity</summary>
		public EntityQuery<AuditRegisterValueEntity> AuditRegisterValue
		{
			get { return Create<AuditRegisterValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AuditRegisterValueType entity</summary>
		public EntityQuery<AuditRegisterValueTypeEntity> AuditRegisterValueType
		{
			get { return Create<AuditRegisterValueTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AutoloadSetting entity</summary>
		public EntityQuery<AutoloadSettingEntity> AutoloadSetting
		{
			get { return Create<AutoloadSettingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AutoloadToPaymentOption entity</summary>
		public EntityQuery<AutoloadToPaymentOptionEntity> AutoloadToPaymentOption
		{
			get { return Create<AutoloadToPaymentOptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BadCard entity</summary>
		public EntityQuery<BadCardEntity> BadCard
		{
			get { return Create<BadCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BadCheck entity</summary>
		public EntityQuery<BadCheckEntity> BadCheck
		{
			get { return Create<BadCheckEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BankConnectionData entity</summary>
		public EntityQuery<BankConnectionDataEntity> BankConnectionData
		{
			get { return Create<BankConnectionDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BankStatement entity</summary>
		public EntityQuery<BankStatementEntity> BankStatement
		{
			get { return Create<BankStatementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BankStatementRecordType entity</summary>
		public EntityQuery<BankStatementRecordTypeEntity> BankStatementRecordType
		{
			get { return Create<BankStatementRecordTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BankStatementState entity</summary>
		public EntityQuery<BankStatementStateEntity> BankStatementState
		{
			get { return Create<BankStatementStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BankStatementVerification entity</summary>
		public EntityQuery<BankStatementVerificationEntity> BankStatementVerification
		{
			get { return Create<BankStatementVerificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Booking entity</summary>
		public EntityQuery<BookingEntity> Booking
		{
			get { return Create<BookingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BookingItem entity</summary>
		public EntityQuery<BookingItemEntity> BookingItem
		{
			get { return Create<BookingItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BookingItemPayment entity</summary>
		public EntityQuery<BookingItemPaymentEntity> BookingItemPayment
		{
			get { return Create<BookingItemPaymentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BookingItemPaymentType entity</summary>
		public EntityQuery<BookingItemPaymentTypeEntity> BookingItemPaymentType
		{
			get { return Create<BookingItemPaymentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BookingType entity</summary>
		public EntityQuery<BookingTypeEntity> BookingType
		{
			get { return Create<BookingTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CappingJournal entity</summary>
		public EntityQuery<CappingJournalEntity> CappingJournal
		{
			get { return Create<CappingJournalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CappingState entity</summary>
		public EntityQuery<CappingStateEntity> CappingState
		{
			get { return Create<CappingStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Card entity</summary>
		public EntityQuery<CardEntity> Card
		{
			get { return Create<CardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardBodyType entity</summary>
		public EntityQuery<CardBodyTypeEntity> CardBodyType
		{
			get { return Create<CardBodyTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardDormancy entity</summary>
		public EntityQuery<CardDormancyEntity> CardDormancy
		{
			get { return Create<CardDormancyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardEvent entity</summary>
		public EntityQuery<CardEventEntity> CardEvent
		{
			get { return Create<CardEventEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardEventType entity</summary>
		public EntityQuery<CardEventTypeEntity> CardEventType
		{
			get { return Create<CardEventTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardFulfillmentStatus entity</summary>
		public EntityQuery<CardFulfillmentStatusEntity> CardFulfillmentStatus
		{
			get { return Create<CardFulfillmentStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardHolder entity</summary>
		public EntityQuery<CardHolderEntity> CardHolder
		{
			get { return Create<CardHolderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardHolderOrganization entity</summary>
		public EntityQuery<CardHolderOrganizationEntity> CardHolderOrganization
		{
			get { return Create<CardHolderOrganizationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardKey entity</summary>
		public EntityQuery<CardKeyEntity> CardKey
		{
			get { return Create<CardKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardLiability entity</summary>
		public EntityQuery<CardLiabilityEntity> CardLiability
		{
			get { return Create<CardLiabilityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardLink entity</summary>
		public EntityQuery<CardLinkEntity> CardLink
		{
			get { return Create<CardLinkEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardOrderDetail entity</summary>
		public EntityQuery<CardOrderDetailEntity> CardOrderDetail
		{
			get { return Create<CardOrderDetailEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardPhysicalDetail entity</summary>
		public EntityQuery<CardPhysicalDetailEntity> CardPhysicalDetail
		{
			get { return Create<CardPhysicalDetailEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardPrintingType entity</summary>
		public EntityQuery<CardPrintingTypeEntity> CardPrintingType
		{
			get { return Create<CardPrintingTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardStockTransfer entity</summary>
		public EntityQuery<CardStockTransferEntity> CardStockTransfer
		{
			get { return Create<CardStockTransferEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardToContract entity</summary>
		public EntityQuery<CardToContractEntity> CardToContract
		{
			get { return Create<CardToContractEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardToRuleViolation entity</summary>
		public EntityQuery<CardToRuleViolationEntity> CardToRuleViolation
		{
			get { return Create<CardToRuleViolationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CardWorkItem entity</summary>
		public EntityQuery<CardWorkItemEntity> CardWorkItem
		{
			get { return Create<CardWorkItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Certificate entity</summary>
		public EntityQuery<CertificateEntity> Certificate
		{
			get { return Create<CertificateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CertificateFormat entity</summary>
		public EntityQuery<CertificateFormatEntity> CertificateFormat
		{
			get { return Create<CertificateFormatEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CertificatePurpose entity</summary>
		public EntityQuery<CertificatePurposeEntity> CertificatePurpose
		{
			get { return Create<CertificatePurposeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CertificateType entity</summary>
		public EntityQuery<CertificateTypeEntity> CertificateType
		{
			get { return Create<CertificateTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Claim entity</summary>
		public EntityQuery<ClaimEntity> Claim
		{
			get { return Create<ClaimEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientFilterType entity</summary>
		public EntityQuery<ClientFilterTypeEntity> ClientFilterType
		{
			get { return Create<ClientFilterTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloseoutPeriod entity</summary>
		public EntityQuery<CloseoutPeriodEntity> CloseoutPeriod
		{
			get { return Create<CloseoutPeriodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloseoutState entity</summary>
		public EntityQuery<CloseoutStateEntity> CloseoutState
		{
			get { return Create<CloseoutStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloseoutType entity</summary>
		public EntityQuery<CloseoutTypeEntity> CloseoutType
		{
			get { return Create<CloseoutTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Comment entity</summary>
		public EntityQuery<CommentEntity> Comment
		{
			get { return Create<CommentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CommentPriority entity</summary>
		public EntityQuery<CommentPriorityEntity> CommentPriority
		{
			get { return Create<CommentPriorityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CommentType entity</summary>
		public EntityQuery<CommentTypeEntity> CommentType
		{
			get { return Create<CommentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Configuration entity</summary>
		public EntityQuery<ConfigurationEntity> Configuration
		{
			get { return Create<ConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ConfigurationDataType entity</summary>
		public EntityQuery<ConfigurationDataTypeEntity> ConfigurationDataType
		{
			get { return Create<ConfigurationDataTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ConfigurationDefinition entity</summary>
		public EntityQuery<ConfigurationDefinitionEntity> ConfigurationDefinition
		{
			get { return Create<ConfigurationDefinitionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ConfigurationOverwrite entity</summary>
		public EntityQuery<ConfigurationOverwriteEntity> ConfigurationOverwrite
		{
			get { return Create<ConfigurationOverwriteEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContentItem entity</summary>
		public EntityQuery<ContentItemEntity> ContentItem
		{
			get { return Create<ContentItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContentType entity</summary>
		public EntityQuery<ContentTypeEntity> ContentType
		{
			get { return Create<ContentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Contract entity</summary>
		public EntityQuery<ContractEntity> Contract
		{
			get { return Create<ContractEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractAddress entity</summary>
		public EntityQuery<ContractAddressEntity> ContractAddress
		{
			get { return Create<ContractAddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractDetails entity</summary>
		public EntityQuery<ContractDetailsEntity> ContractDetails
		{
			get { return Create<ContractDetailsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractHistory entity</summary>
		public EntityQuery<ContractHistoryEntity> ContractHistory
		{
			get { return Create<ContractHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractLink entity</summary>
		public EntityQuery<ContractLinkEntity> ContractLink
		{
			get { return Create<ContractLinkEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractState entity</summary>
		public EntityQuery<ContractStateEntity> ContractState
		{
			get { return Create<ContractStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractTermination entity</summary>
		public EntityQuery<ContractTerminationEntity> ContractTermination
		{
			get { return Create<ContractTerminationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractTerminationType entity</summary>
		public EntityQuery<ContractTerminationTypeEntity> ContractTerminationType
		{
			get { return Create<ContractTerminationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractToPaymentMethod entity</summary>
		public EntityQuery<ContractToPaymentMethodEntity> ContractToPaymentMethod
		{
			get { return Create<ContractToPaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractToProvider entity</summary>
		public EntityQuery<ContractToProviderEntity> ContractToProvider
		{
			get { return Create<ContractToProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractType entity</summary>
		public EntityQuery<ContractTypeEntity> ContractType
		{
			get { return Create<ContractTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ContractWorkItem entity</summary>
		public EntityQuery<ContractWorkItemEntity> ContractWorkItem
		{
			get { return Create<ContractWorkItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Coordinate entity</summary>
		public EntityQuery<CoordinateEntity> Coordinate
		{
			get { return Create<CoordinateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Country entity</summary>
		public EntityQuery<CountryEntity> Country
		{
			get { return Create<CountryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditCardAuthorization entity</summary>
		public EntityQuery<CreditCardAuthorizationEntity> CreditCardAuthorization
		{
			get { return Create<CreditCardAuthorizationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditCardAuthorizationLog entity</summary>
		public EntityQuery<CreditCardAuthorizationLogEntity> CreditCardAuthorizationLog
		{
			get { return Create<CreditCardAuthorizationLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditCardMerchant entity</summary>
		public EntityQuery<CreditCardMerchantEntity> CreditCardMerchant
		{
			get { return Create<CreditCardMerchantEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditCardNetwork entity</summary>
		public EntityQuery<CreditCardNetworkEntity> CreditCardNetwork
		{
			get { return Create<CreditCardNetworkEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CreditCardTerminal entity</summary>
		public EntityQuery<CreditCardTerminalEntity> CreditCardTerminal
		{
			get { return Create<CreditCardTerminalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoCertificate entity</summary>
		public EntityQuery<CryptoCertificateEntity> CryptoCertificate
		{
			get { return Create<CryptoCertificateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoContent entity</summary>
		public EntityQuery<CryptoContentEntity> CryptoContent
		{
			get { return Create<CryptoContentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoCryptogramArchive entity</summary>
		public EntityQuery<CryptoCryptogramArchiveEntity> CryptoCryptogramArchive
		{
			get { return Create<CryptoCryptogramArchiveEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptogramCertificate entity</summary>
		public EntityQuery<CryptogramCertificateEntity> CryptogramCertificate
		{
			get { return Create<CryptogramCertificateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptogramCertificateType entity</summary>
		public EntityQuery<CryptogramCertificateTypeEntity> CryptogramCertificateType
		{
			get { return Create<CryptogramCertificateTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptogramErrorType entity</summary>
		public EntityQuery<CryptogramErrorTypeEntity> CryptogramErrorType
		{
			get { return Create<CryptogramErrorTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptogramLoadStatus entity</summary>
		public EntityQuery<CryptogramLoadStatusEntity> CryptogramLoadStatus
		{
			get { return Create<CryptogramLoadStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoOperatorActivationkey entity</summary>
		public EntityQuery<CryptoOperatorActivationkeyEntity> CryptoOperatorActivationkey
		{
			get { return Create<CryptoOperatorActivationkeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoQArchive entity</summary>
		public EntityQuery<CryptoQArchiveEntity> CryptoQArchive
		{
			get { return Create<CryptoQArchiveEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoResource entity</summary>
		public EntityQuery<CryptoResourceEntity> CryptoResource
		{
			get { return Create<CryptoResourceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoResourceData entity</summary>
		public EntityQuery<CryptoResourceDataEntity> CryptoResourceData
		{
			get { return Create<CryptoResourceDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoSamSignCertificate entity</summary>
		public EntityQuery<CryptoSamSignCertificateEntity> CryptoSamSignCertificate
		{
			get { return Create<CryptoSamSignCertificateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CryptoUicCertificate entity</summary>
		public EntityQuery<CryptoUicCertificateEntity> CryptoUicCertificate
		{
			get { return Create<CryptoUicCertificateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomAttribute entity</summary>
		public EntityQuery<CustomAttributeEntity> CustomAttribute
		{
			get { return Create<CustomAttributeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomAttributeValue entity</summary>
		public EntityQuery<CustomAttributeValueEntity> CustomAttributeValue
		{
			get { return Create<CustomAttributeValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomAttributeValueToPerson entity</summary>
		public EntityQuery<CustomAttributeValueToPersonEntity> CustomAttributeValueToPerson
		{
			get { return Create<CustomAttributeValueToPersonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerAccount entity</summary>
		public EntityQuery<CustomerAccountEntity> CustomerAccount
		{
			get { return Create<CustomerAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerAccountNotification entity</summary>
		public EntityQuery<CustomerAccountNotificationEntity> CustomerAccountNotification
		{
			get { return Create<CustomerAccountNotificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerAccountRole entity</summary>
		public EntityQuery<CustomerAccountRoleEntity> CustomerAccountRole
		{
			get { return Create<CustomerAccountRoleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerAccountState entity</summary>
		public EntityQuery<CustomerAccountStateEntity> CustomerAccountState
		{
			get { return Create<CustomerAccountStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerAccountToVerificationAttemptType entity</summary>
		public EntityQuery<CustomerAccountToVerificationAttemptTypeEntity> CustomerAccountToVerificationAttemptType
		{
			get { return Create<CustomerAccountToVerificationAttemptTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomerLanguage entity</summary>
		public EntityQuery<CustomerLanguageEntity> CustomerLanguage
		{
			get { return Create<CustomerLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceCommunicationHistory entity</summary>
		public EntityQuery<DeviceCommunicationHistoryEntity> DeviceCommunicationHistory
		{
			get { return Create<DeviceCommunicationHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReader entity</summary>
		public EntityQuery<DeviceReaderEntity> DeviceReader
		{
			get { return Create<DeviceReaderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReaderCryptoResource entity</summary>
		public EntityQuery<DeviceReaderCryptoResourceEntity> DeviceReaderCryptoResource
		{
			get { return Create<DeviceReaderCryptoResourceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReaderJob entity</summary>
		public EntityQuery<DeviceReaderJobEntity> DeviceReaderJob
		{
			get { return Create<DeviceReaderJobEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReaderKey entity</summary>
		public EntityQuery<DeviceReaderKeyEntity> DeviceReaderKey
		{
			get { return Create<DeviceReaderKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReaderKeyToCert entity</summary>
		public EntityQuery<DeviceReaderKeyToCertEntity> DeviceReaderKeyToCert
		{
			get { return Create<DeviceReaderKeyToCertEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceReaderToCert entity</summary>
		public EntityQuery<DeviceReaderToCertEntity> DeviceReaderToCert
		{
			get { return Create<DeviceReaderToCertEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceToken entity</summary>
		public EntityQuery<DeviceTokenEntity> DeviceToken
		{
			get { return Create<DeviceTokenEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceTokenState entity</summary>
		public EntityQuery<DeviceTokenStateEntity> DeviceTokenState
		{
			get { return Create<DeviceTokenStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DocumentHistory entity</summary>
		public EntityQuery<DocumentHistoryEntity> DocumentHistory
		{
			get { return Create<DocumentHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DocumentType entity</summary>
		public EntityQuery<DocumentTypeEntity> DocumentType
		{
			get { return Create<DocumentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DormancyNotification entity</summary>
		public EntityQuery<DormancyNotificationEntity> DormancyNotification
		{
			get { return Create<DormancyNotificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningAction entity</summary>
		public EntityQuery<DunningActionEntity> DunningAction
		{
			get { return Create<DunningActionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningLevelConfiguration entity</summary>
		public EntityQuery<DunningLevelConfigurationEntity> DunningLevelConfiguration
		{
			get { return Create<DunningLevelConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningLevelPostingKey entity</summary>
		public EntityQuery<DunningLevelPostingKeyEntity> DunningLevelPostingKey
		{
			get { return Create<DunningLevelPostingKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningProcess entity</summary>
		public EntityQuery<DunningProcessEntity> DunningProcess
		{
			get { return Create<DunningProcessEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningToInvoice entity</summary>
		public EntityQuery<DunningToInvoiceEntity> DunningToInvoice
		{
			get { return Create<DunningToInvoiceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningToPosting entity</summary>
		public EntityQuery<DunningToPostingEntity> DunningToPosting
		{
			get { return Create<DunningToPostingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DunningToProduct entity</summary>
		public EntityQuery<DunningToProductEntity> DunningToProduct
		{
			get { return Create<DunningToProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EmailEvent entity</summary>
		public EntityQuery<EmailEventEntity> EmailEvent
		{
			get { return Create<EmailEventEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EmailEventResendLock entity</summary>
		public EntityQuery<EmailEventResendLockEntity> EmailEventResendLock
		{
			get { return Create<EmailEventResendLockEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EmailMessage entity</summary>
		public EntityQuery<EmailMessageEntity> EmailMessage
		{
			get { return Create<EmailMessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Entitlement entity</summary>
		public EntityQuery<EntitlementEntity> Entitlement
		{
			get { return Create<EntitlementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntitlementToProduct entity</summary>
		public EntityQuery<EntitlementToProductEntity> EntitlementToProduct
		{
			get { return Create<EntitlementToProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntitlementType entity</summary>
		public EntityQuery<EntitlementTypeEntity> EntitlementType
		{
			get { return Create<EntitlementTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EventSetting entity</summary>
		public EntityQuery<EventSettingEntity> EventSetting
		{
			get { return Create<EventSettingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareChangeCause entity</summary>
		public EntityQuery<FareChangeCauseEntity> FareChangeCause
		{
			get { return Create<FareChangeCauseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionBehaviour entity</summary>
		public EntityQuery<FareEvasionBehaviourEntity> FareEvasionBehaviour
		{
			get { return Create<FareEvasionBehaviourEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionIncident entity</summary>
		public EntityQuery<FareEvasionIncidentEntity> FareEvasionIncident
		{
			get { return Create<FareEvasionIncidentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionInspection entity</summary>
		public EntityQuery<FareEvasionInspectionEntity> FareEvasionInspection
		{
			get { return Create<FareEvasionInspectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FareEvasionState entity</summary>
		public EntityQuery<FareEvasionStateEntity> FareEvasionState
		{
			get { return Create<FareEvasionStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FarePaymentError entity</summary>
		public EntityQuery<FarePaymentErrorEntity> FarePaymentError
		{
			get { return Create<FarePaymentErrorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FarePaymentResultType entity</summary>
		public EntityQuery<FarePaymentResultTypeEntity> FarePaymentResultType
		{
			get { return Create<FarePaymentResultTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Fastcard entity</summary>
		public EntityQuery<FastcardEntity> Fastcard
		{
			get { return Create<FastcardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterCategory entity</summary>
		public EntityQuery<FilterCategoryEntity> FilterCategory
		{
			get { return Create<FilterCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterElement entity</summary>
		public EntityQuery<FilterElementEntity> FilterElement
		{
			get { return Create<FilterElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterList entity</summary>
		public EntityQuery<FilterListEntity> FilterList
		{
			get { return Create<FilterListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterListElement entity</summary>
		public EntityQuery<FilterListElementEntity> FilterListElement
		{
			get { return Create<FilterListElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterType entity</summary>
		public EntityQuery<FilterTypeEntity> FilterType
		{
			get { return Create<FilterTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterValue entity</summary>
		public EntityQuery<FilterValueEntity> FilterValue
		{
			get { return Create<FilterValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FilterValueSet entity</summary>
		public EntityQuery<FilterValueSetEntity> FilterValueSet
		{
			get { return Create<FilterValueSetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FlexValue entity</summary>
		public EntityQuery<FlexValueEntity> FlexValue
		{
			get { return Create<FlexValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Form entity</summary>
		public EntityQuery<FormEntity> Form
		{
			get { return Create<FormEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FormTemplate entity</summary>
		public EntityQuery<FormTemplateEntity> FormTemplate
		{
			get { return Create<FormTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Frequency entity</summary>
		public EntityQuery<FrequencyEntity> Frequency
		{
			get { return Create<FrequencyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Gender entity</summary>
		public EntityQuery<GenderEntity> Gender
		{
			get { return Create<GenderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the IdentificationType entity</summary>
		public EntityQuery<IdentificationTypeEntity> IdentificationType
		{
			get { return Create<IdentificationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InspectionCriterion entity</summary>
		public EntityQuery<InspectionCriterionEntity> InspectionCriterion
		{
			get { return Create<InspectionCriterionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InspectionReport entity</summary>
		public EntityQuery<InspectionReportEntity> InspectionReport
		{
			get { return Create<InspectionReportEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InspectionResult entity</summary>
		public EntityQuery<InspectionResultEntity> InspectionResult
		{
			get { return Create<InspectionResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InventoryState entity</summary>
		public EntityQuery<InventoryStateEntity> InventoryState
		{
			get { return Create<InventoryStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Invoice entity</summary>
		public EntityQuery<InvoiceEntity> Invoice
		{
			get { return Create<InvoiceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InvoiceEntry entity</summary>
		public EntityQuery<InvoiceEntryEntity> InvoiceEntry
		{
			get { return Create<InvoiceEntryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InvoiceType entity</summary>
		public EntityQuery<InvoiceTypeEntity> InvoiceType
		{
			get { return Create<InvoiceTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Invoicing entity</summary>
		public EntityQuery<InvoicingEntity> Invoicing
		{
			get { return Create<InvoicingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InvoicingFilterType entity</summary>
		public EntityQuery<InvoicingFilterTypeEntity> InvoicingFilterType
		{
			get { return Create<InvoicingFilterTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InvoicingType entity</summary>
		public EntityQuery<InvoicingTypeEntity> InvoicingType
		{
			get { return Create<InvoicingTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Job entity</summary>
		public EntityQuery<JobEntity> Job
		{
			get { return Create<JobEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the JobBulkImport entity</summary>
		public EntityQuery<JobBulkImportEntity> JobBulkImport
		{
			get { return Create<JobBulkImportEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the JobCardImport entity</summary>
		public EntityQuery<JobCardImportEntity> JobCardImport
		{
			get { return Create<JobCardImportEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the JobFileType entity</summary>
		public EntityQuery<JobFileTypeEntity> JobFileType
		{
			get { return Create<JobFileTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the JobState entity</summary>
		public EntityQuery<JobStateEntity> JobState
		{
			get { return Create<JobStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the JobType entity</summary>
		public EntityQuery<JobTypeEntity> JobType
		{
			get { return Create<JobTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LookupAddress entity</summary>
		public EntityQuery<LookupAddressEntity> LookupAddress
		{
			get { return Create<LookupAddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaEligibilityRequirement entity</summary>
		public EntityQuery<MediaEligibilityRequirementEntity> MediaEligibilityRequirement
		{
			get { return Create<MediaEligibilityRequirementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaSalePrecondition entity</summary>
		public EntityQuery<MediaSalePreconditionEntity> MediaSalePrecondition
		{
			get { return Create<MediaSalePreconditionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Merchant entity</summary>
		public EntityQuery<MerchantEntity> Merchant
		{
			get { return Create<MerchantEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Message entity</summary>
		public EntityQuery<MessageEntity> Message
		{
			get { return Create<MessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageFormat entity</summary>
		public EntityQuery<MessageFormatEntity> MessageFormat
		{
			get { return Create<MessageFormatEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageType entity</summary>
		public EntityQuery<MessageTypeEntity> MessageType
		{
			get { return Create<MessageTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MobilityProvider entity</summary>
		public EntityQuery<MobilityProviderEntity> MobilityProvider
		{
			get { return Create<MobilityProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NotificationDevice entity</summary>
		public EntityQuery<NotificationDeviceEntity> NotificationDevice
		{
			get { return Create<NotificationDeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NotificationMessage entity</summary>
		public EntityQuery<NotificationMessageEntity> NotificationMessage
		{
			get { return Create<NotificationMessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NotificationMessageOwner entity</summary>
		public EntityQuery<NotificationMessageOwnerEntity> NotificationMessageOwner
		{
			get { return Create<NotificationMessageOwnerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NotificationMessageToCustomer entity</summary>
		public EntityQuery<NotificationMessageToCustomerEntity> NotificationMessageToCustomer
		{
			get { return Create<NotificationMessageToCustomerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NumberGroup entity</summary>
		public EntityQuery<NumberGroupEntity> NumberGroup
		{
			get { return Create<NumberGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NumberGroupRandomized entity</summary>
		public EntityQuery<NumberGroupRandomizedEntity> NumberGroupRandomized
		{
			get { return Create<NumberGroupRandomizedEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NumberGroupScope entity</summary>
		public EntityQuery<NumberGroupScopeEntity> NumberGroupScope
		{
			get { return Create<NumberGroupScopeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Order entity</summary>
		public EntityQuery<OrderEntity> Order
		{
			get { return Create<OrderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderDetail entity</summary>
		public EntityQuery<OrderDetailEntity> OrderDetail
		{
			get { return Create<OrderDetailEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderDetailToCard entity</summary>
		public EntityQuery<OrderDetailToCardEntity> OrderDetailToCard
		{
			get { return Create<OrderDetailToCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderDetailToCardHolder entity</summary>
		public EntityQuery<OrderDetailToCardHolderEntity> OrderDetailToCardHolder
		{
			get { return Create<OrderDetailToCardHolderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderHistory entity</summary>
		public EntityQuery<OrderHistoryEntity> OrderHistory
		{
			get { return Create<OrderHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderProcessingCleanup entity</summary>
		public EntityQuery<OrderProcessingCleanupEntity> OrderProcessingCleanup
		{
			get { return Create<OrderProcessingCleanupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderState entity</summary>
		public EntityQuery<OrderStateEntity> OrderState
		{
			get { return Create<OrderStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderType entity</summary>
		public EntityQuery<OrderTypeEntity> OrderType
		{
			get { return Create<OrderTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderWithTotal entity</summary>
		public EntityQuery<OrderWithTotalEntity> OrderWithTotal
		{
			get { return Create<OrderWithTotalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderWorkItem entity</summary>
		public EntityQuery<OrderWorkItemEntity> OrderWorkItem
		{
			get { return Create<OrderWorkItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Organization entity</summary>
		public EntityQuery<OrganizationEntity> Organization
		{
			get { return Create<OrganizationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrganizationAddress entity</summary>
		public EntityQuery<OrganizationAddressEntity> OrganizationAddress
		{
			get { return Create<OrganizationAddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrganizationParticipant entity</summary>
		public EntityQuery<OrganizationParticipantEntity> OrganizationParticipant
		{
			get { return Create<OrganizationParticipantEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrganizationSubtype entity</summary>
		public EntityQuery<OrganizationSubtypeEntity> OrganizationSubtype
		{
			get { return Create<OrganizationSubtypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrganizationType entity</summary>
		public EntityQuery<OrganizationTypeEntity> OrganizationType
		{
			get { return Create<OrganizationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageContent entity</summary>
		public EntityQuery<PageContentEntity> PageContent
		{
			get { return Create<PageContentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Parameter entity</summary>
		public EntityQuery<ParameterEntity> Parameter
		{
			get { return Create<ParameterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterArchive entity</summary>
		public EntityQuery<ParameterArchiveEntity> ParameterArchive
		{
			get { return Create<ParameterArchiveEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterArchiveRelease entity</summary>
		public EntityQuery<ParameterArchiveReleaseEntity> ParameterArchiveRelease
		{
			get { return Create<ParameterArchiveReleaseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterArchiveResultView entity</summary>
		public EntityQuery<ParameterArchiveResultViewEntity> ParameterArchiveResultView
		{
			get { return Create<ParameterArchiveResultViewEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterGroup entity</summary>
		public EntityQuery<ParameterGroupEntity> ParameterGroup
		{
			get { return Create<ParameterGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterGroupToParameter entity</summary>
		public EntityQuery<ParameterGroupToParameterEntity> ParameterGroupToParameter
		{
			get { return Create<ParameterGroupToParameterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterType entity</summary>
		public EntityQuery<ParameterTypeEntity> ParameterType
		{
			get { return Create<ParameterTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParameterValue entity</summary>
		public EntityQuery<ParameterValueEntity> ParameterValue
		{
			get { return Create<ParameterValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParaTransAttribute entity</summary>
		public EntityQuery<ParaTransAttributeEntity> ParaTransAttribute
		{
			get { return Create<ParaTransAttributeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParaTransAttributeType entity</summary>
		public EntityQuery<ParaTransAttributeTypeEntity> ParaTransAttributeType
		{
			get { return Create<ParaTransAttributeTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParaTransJournal entity</summary>
		public EntityQuery<ParaTransJournalEntity> ParaTransJournal
		{
			get { return Create<ParaTransJournalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParaTransJournalType entity</summary>
		public EntityQuery<ParaTransJournalTypeEntity> ParaTransJournalType
		{
			get { return Create<ParaTransJournalTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParaTransPaymentType entity</summary>
		public EntityQuery<ParaTransPaymentTypeEntity> ParaTransPaymentType
		{
			get { return Create<ParaTransPaymentTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ParticipantGroup entity</summary>
		public EntityQuery<ParticipantGroupEntity> ParticipantGroup
		{
			get { return Create<ParticipantGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PassExpiryNotification entity</summary>
		public EntityQuery<PassExpiryNotificationEntity> PassExpiryNotification
		{
			get { return Create<PassExpiryNotificationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Payment entity</summary>
		public EntityQuery<PaymentEntity> Payment
		{
			get { return Create<PaymentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentJournal entity</summary>
		public EntityQuery<PaymentJournalEntity> PaymentJournal
		{
			get { return Create<PaymentJournalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentJournalToComponent entity</summary>
		public EntityQuery<PaymentJournalToComponentEntity> PaymentJournalToComponent
		{
			get { return Create<PaymentJournalToComponentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentMethod entity</summary>
		public EntityQuery<PaymentMethodEntity> PaymentMethod
		{
			get { return Create<PaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentModality entity</summary>
		public EntityQuery<PaymentModalityEntity> PaymentModality
		{
			get { return Create<PaymentModalityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentOption entity</summary>
		public EntityQuery<PaymentOptionEntity> PaymentOption
		{
			get { return Create<PaymentOptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentProvider entity</summary>
		public EntityQuery<PaymentProviderEntity> PaymentProvider
		{
			get { return Create<PaymentProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentProviderAccount entity</summary>
		public EntityQuery<PaymentProviderAccountEntity> PaymentProviderAccount
		{
			get { return Create<PaymentProviderAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentRecognition entity</summary>
		public EntityQuery<PaymentRecognitionEntity> PaymentRecognition
		{
			get { return Create<PaymentRecognitionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentReconciliation entity</summary>
		public EntityQuery<PaymentReconciliationEntity> PaymentReconciliation
		{
			get { return Create<PaymentReconciliationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentReconciliationState entity</summary>
		public EntityQuery<PaymentReconciliationStateEntity> PaymentReconciliationState
		{
			get { return Create<PaymentReconciliationStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentState entity</summary>
		public EntityQuery<PaymentStateEntity> PaymentState
		{
			get { return Create<PaymentStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PendingOrder entity</summary>
		public EntityQuery<PendingOrderEntity> PendingOrder
		{
			get { return Create<PendingOrderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Performance entity</summary>
		public EntityQuery<PerformanceEntity> Performance
		{
			get { return Create<PerformanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PerformanceAggregation entity</summary>
		public EntityQuery<PerformanceAggregationEntity> PerformanceAggregation
		{
			get { return Create<PerformanceAggregationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PerformanceComponent entity</summary>
		public EntityQuery<PerformanceComponentEntity> PerformanceComponent
		{
			get { return Create<PerformanceComponentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PerformanceIndicator entity</summary>
		public EntityQuery<PerformanceIndicatorEntity> PerformanceIndicator
		{
			get { return Create<PerformanceIndicatorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Person entity</summary>
		public EntityQuery<PersonEntity> Person
		{
			get { return Create<PersonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PersonAddress entity</summary>
		public EntityQuery<PersonAddressEntity> PersonAddress
		{
			get { return Create<PersonAddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Photograph entity</summary>
		public EntityQuery<PhotographEntity> Photograph
		{
			get { return Create<PhotographEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PickupLocation entity</summary>
		public EntityQuery<PickupLocationEntity> PickupLocation
		{
			get { return Create<PickupLocationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PickupLocationToTicket entity</summary>
		public EntityQuery<PickupLocationToTicketEntity> PickupLocationToTicket
		{
			get { return Create<PickupLocationToTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posting entity</summary>
		public EntityQuery<PostingEntity> Posting
		{
			get { return Create<PostingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PostingKey entity</summary>
		public EntityQuery<PostingKeyEntity> PostingKey
		{
			get { return Create<PostingKeyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PostingKeyCategory entity</summary>
		public EntityQuery<PostingKeyCategoryEntity> PostingKeyCategory
		{
			get { return Create<PostingKeyCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PostingScope entity</summary>
		public EntityQuery<PostingScopeEntity> PostingScope
		{
			get { return Create<PostingScopeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceAdjustment entity</summary>
		public EntityQuery<PriceAdjustmentEntity> PriceAdjustment
		{
			get { return Create<PriceAdjustmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceAdjustmentTemplate entity</summary>
		public EntityQuery<PriceAdjustmentTemplateEntity> PriceAdjustmentTemplate
		{
			get { return Create<PriceAdjustmentTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Printer entity</summary>
		public EntityQuery<PrinterEntity> Printer
		{
			get { return Create<PrinterEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PrinterState entity</summary>
		public EntityQuery<PrinterStateEntity> PrinterState
		{
			get { return Create<PrinterStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PrinterToUnit entity</summary>
		public EntityQuery<PrinterToUnitEntity> PrinterToUnit
		{
			get { return Create<PrinterToUnitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PrinterType entity</summary>
		public EntityQuery<PrinterTypeEntity> PrinterType
		{
			get { return Create<PrinterTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Priority entity</summary>
		public EntityQuery<PriorityEntity> Priority
		{
			get { return Create<PriorityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Product entity</summary>
		public EntityQuery<ProductEntity> Product
		{
			get { return Create<ProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductRelation entity</summary>
		public EntityQuery<ProductRelationEntity> ProductRelation
		{
			get { return Create<ProductRelationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductSubsidy entity</summary>
		public EntityQuery<ProductSubsidyEntity> ProductSubsidy
		{
			get { return Create<ProductSubsidyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductTermination entity</summary>
		public EntityQuery<ProductTerminationEntity> ProductTermination
		{
			get { return Create<ProductTerminationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductTerminationType entity</summary>
		public EntityQuery<ProductTerminationTypeEntity> ProductTerminationType
		{
			get { return Create<ProductTerminationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductType entity</summary>
		public EntityQuery<ProductTypeEntity> ProductType
		{
			get { return Create<ProductTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PromoCode entity</summary>
		public EntityQuery<PromoCodeEntity> PromoCode
		{
			get { return Create<PromoCodeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PromoCodeStatus entity</summary>
		public EntityQuery<PromoCodeStatusEntity> PromoCodeStatus
		{
			get { return Create<PromoCodeStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProviderAccountState entity</summary>
		public EntityQuery<ProviderAccountStateEntity> ProviderAccountState
		{
			get { return Create<ProviderAccountStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProvisioningState entity</summary>
		public EntityQuery<ProvisioningStateEntity> ProvisioningState
		{
			get { return Create<ProvisioningStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RecipientGroup entity</summary>
		public EntityQuery<RecipientGroupEntity> RecipientGroup
		{
			get { return Create<RecipientGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RecipientGroupMember entity</summary>
		public EntityQuery<RecipientGroupMemberEntity> RecipientGroupMember
		{
			get { return Create<RecipientGroupMemberEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Refund entity</summary>
		public EntityQuery<RefundEntity> Refund
		{
			get { return Create<RefundEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RefundReason entity</summary>
		public EntityQuery<RefundReasonEntity> RefundReason
		{
			get { return Create<RefundReasonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RentalState entity</summary>
		public EntityQuery<RentalStateEntity> RentalState
		{
			get { return Create<RentalStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RentalType entity</summary>
		public EntityQuery<RentalTypeEntity> RentalType
		{
			get { return Create<RentalTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Report entity</summary>
		public EntityQuery<ReportEntity> Report
		{
			get { return Create<ReportEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportCategory entity</summary>
		public EntityQuery<ReportCategoryEntity> ReportCategory
		{
			get { return Create<ReportCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportDataFile entity</summary>
		public EntityQuery<ReportDataFileEntity> ReportDataFile
		{
			get { return Create<ReportDataFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportJob entity</summary>
		public EntityQuery<ReportJobEntity> ReportJob
		{
			get { return Create<ReportJobEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportJobResult entity</summary>
		public EntityQuery<ReportJobResultEntity> ReportJobResult
		{
			get { return Create<ReportJobResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportToClient entity</summary>
		public EntityQuery<ReportToClientEntity> ReportToClient
		{
			get { return Create<ReportToClientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RevenueRecognition entity</summary>
		public EntityQuery<RevenueRecognitionEntity> RevenueRecognition
		{
			get { return Create<RevenueRecognitionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RevenueSettlement entity</summary>
		public EntityQuery<RevenueSettlementEntity> RevenueSettlement
		{
			get { return Create<RevenueSettlementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleViolation entity</summary>
		public EntityQuery<RuleViolationEntity> RuleViolation
		{
			get { return Create<RuleViolationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleViolationProvider entity</summary>
		public EntityQuery<RuleViolationProviderEntity> RuleViolationProvider
		{
			get { return Create<RuleViolationProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleViolationSourceType entity</summary>
		public EntityQuery<RuleViolationSourceTypeEntity> RuleViolationSourceType
		{
			get { return Create<RuleViolationSourceTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleViolationToTransaction entity</summary>
		public EntityQuery<RuleViolationToTransactionEntity> RuleViolationToTransaction
		{
			get { return Create<RuleViolationToTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RuleViolationType entity</summary>
		public EntityQuery<RuleViolationTypeEntity> RuleViolationType
		{
			get { return Create<RuleViolationTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Sale entity</summary>
		public EntityQuery<SaleEntity> Sale
		{
			get { return Create<SaleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SalesChannelToPaymentMethod entity</summary>
		public EntityQuery<SalesChannelToPaymentMethodEntity> SalesChannelToPaymentMethod
		{
			get { return Create<SalesChannelToPaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SalesRevenue entity</summary>
		public EntityQuery<SalesRevenueEntity> SalesRevenue
		{
			get { return Create<SalesRevenueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SaleToComponent entity</summary>
		public EntityQuery<SaleToComponentEntity> SaleToComponent
		{
			get { return Create<SaleToComponentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SaleType entity</summary>
		public EntityQuery<SaleTypeEntity> SaleType
		{
			get { return Create<SaleTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SamModule entity</summary>
		public EntityQuery<SamModuleEntity> SamModule
		{
			get { return Create<SamModuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SamModuleStatus entity</summary>
		public EntityQuery<SamModuleStatusEntity> SamModuleStatus
		{
			get { return Create<SamModuleStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SchoolYear entity</summary>
		public EntityQuery<SchoolYearEntity> SchoolYear
		{
			get { return Create<SchoolYearEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SecurityQuestion entity</summary>
		public EntityQuery<SecurityQuestionEntity> SecurityQuestion
		{
			get { return Create<SecurityQuestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SecurityResponse entity</summary>
		public EntityQuery<SecurityResponseEntity> SecurityResponse
		{
			get { return Create<SecurityResponseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SepaFrequencyType entity</summary>
		public EntityQuery<SepaFrequencyTypeEntity> SepaFrequencyType
		{
			get { return Create<SepaFrequencyTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SepaOwnerType entity</summary>
		public EntityQuery<SepaOwnerTypeEntity> SepaOwnerType
		{
			get { return Create<SepaOwnerTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServerResponseTimeData entity</summary>
		public EntityQuery<ServerResponseTimeDataEntity> ServerResponseTimeData
		{
			get { return Create<ServerResponseTimeDataEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServerResponseTimeDataAggregation entity</summary>
		public EntityQuery<ServerResponseTimeDataAggregationEntity> ServerResponseTimeDataAggregation
		{
			get { return Create<ServerResponseTimeDataAggregationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettledRevenue entity</summary>
		public EntityQuery<SettledRevenueEntity> SettledRevenue
		{
			get { return Create<SettledRevenueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementAccount entity</summary>
		public EntityQuery<SettlementAccountEntity> SettlementAccount
		{
			get { return Create<SettlementAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementCalendar entity</summary>
		public EntityQuery<SettlementCalendarEntity> SettlementCalendar
		{
			get { return Create<SettlementCalendarEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementDistributionPolicy entity</summary>
		public EntityQuery<SettlementDistributionPolicyEntity> SettlementDistributionPolicy
		{
			get { return Create<SettlementDistributionPolicyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementHoliday entity</summary>
		public EntityQuery<SettlementHolidayEntity> SettlementHoliday
		{
			get { return Create<SettlementHolidayEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementOperation entity</summary>
		public EntityQuery<SettlementOperationEntity> SettlementOperation
		{
			get { return Create<SettlementOperationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementQuerySetting entity</summary>
		public EntityQuery<SettlementQuerySettingEntity> SettlementQuerySetting
		{
			get { return Create<SettlementQuerySettingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementQuerySettingToTicket entity</summary>
		public EntityQuery<SettlementQuerySettingToTicketEntity> SettlementQuerySettingToTicket
		{
			get { return Create<SettlementQuerySettingToTicketEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementQueryValue entity</summary>
		public EntityQuery<SettlementQueryValueEntity> SettlementQueryValue
		{
			get { return Create<SettlementQueryValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementReleaseState entity</summary>
		public EntityQuery<SettlementReleaseStateEntity> SettlementReleaseState
		{
			get { return Create<SettlementReleaseStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementResult entity</summary>
		public EntityQuery<SettlementResultEntity> SettlementResult
		{
			get { return Create<SettlementResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementRun entity</summary>
		public EntityQuery<SettlementRunEntity> SettlementRun
		{
			get { return Create<SettlementRunEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementRunValue entity</summary>
		public EntityQuery<SettlementRunValueEntity> SettlementRunValue
		{
			get { return Create<SettlementRunValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementRunValueToVarioSettlement entity</summary>
		public EntityQuery<SettlementRunValueToVarioSettlementEntity> SettlementRunValueToVarioSettlement
		{
			get { return Create<SettlementRunValueToVarioSettlementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementSetup entity</summary>
		public EntityQuery<SettlementSetupEntity> SettlementSetup
		{
			get { return Create<SettlementSetupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SettlementType entity</summary>
		public EntityQuery<SettlementTypeEntity> SettlementType
		{
			get { return Create<SettlementTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Source entity</summary>
		public EntityQuery<SourceEntity> Source
		{
			get { return Create<SourceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StatementOfAccount entity</summary>
		public EntityQuery<StatementOfAccountEntity> StatementOfAccount
		{
			get { return Create<StatementOfAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StateOfCountry entity</summary>
		public EntityQuery<StateOfCountryEntity> StateOfCountry
		{
			get { return Create<StateOfCountryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StockTransfer entity</summary>
		public EntityQuery<StockTransferEntity> StockTransfer
		{
			get { return Create<StockTransferEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StorageLocation entity</summary>
		public EntityQuery<StorageLocationEntity> StorageLocation
		{
			get { return Create<StorageLocationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SubsidyLimit entity</summary>
		public EntityQuery<SubsidyLimitEntity> SubsidyLimit
		{
			get { return Create<SubsidyLimitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SubsidyTransaction entity</summary>
		public EntityQuery<SubsidyTransactionEntity> SubsidyTransaction
		{
			get { return Create<SubsidyTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Survey entity</summary>
		public EntityQuery<SurveyEntity> Survey
		{
			get { return Create<SurveyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyAnswer entity</summary>
		public EntityQuery<SurveyAnswerEntity> SurveyAnswer
		{
			get { return Create<SurveyAnswerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyChoice entity</summary>
		public EntityQuery<SurveyChoiceEntity> SurveyChoice
		{
			get { return Create<SurveyChoiceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyDescription entity</summary>
		public EntityQuery<SurveyDescriptionEntity> SurveyDescription
		{
			get { return Create<SurveyDescriptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyQuestion entity</summary>
		public EntityQuery<SurveyQuestionEntity> SurveyQuestion
		{
			get { return Create<SurveyQuestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyResponse entity</summary>
		public EntityQuery<SurveyResponseEntity> SurveyResponse
		{
			get { return Create<SurveyResponseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Tenant entity</summary>
		public EntityQuery<TenantEntity> Tenant
		{
			get { return Create<TenantEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TenantHistory entity</summary>
		public EntityQuery<TenantHistoryEntity> TenantHistory
		{
			get { return Create<TenantHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TenantPerson entity</summary>
		public EntityQuery<TenantPersonEntity> TenantPerson
		{
			get { return Create<TenantPersonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TenantState entity</summary>
		public EntityQuery<TenantStateEntity> TenantState
		{
			get { return Create<TenantStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminationStatus entity</summary>
		public EntityQuery<TerminationStatusEntity> TerminationStatus
		{
			get { return Create<TerminationStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketAssignment entity</summary>
		public EntityQuery<TicketAssignmentEntity> TicketAssignment
		{
			get { return Create<TicketAssignmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketAssignmentStatus entity</summary>
		public EntityQuery<TicketAssignmentStatusEntity> TicketAssignmentStatus
		{
			get { return Create<TicketAssignmentStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketSerialNumber entity</summary>
		public EntityQuery<TicketSerialNumberEntity> TicketSerialNumber
		{
			get { return Create<TicketSerialNumberEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TicketSerialNumberStatus entity</summary>
		public EntityQuery<TicketSerialNumberStatusEntity> TicketSerialNumberStatus
		{
			get { return Create<TicketSerialNumberStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionJournal entity</summary>
		public EntityQuery<TransactionJournalEntity> TransactionJournal
		{
			get { return Create<TransactionJournalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionJournalCorrection entity</summary>
		public EntityQuery<TransactionJournalCorrectionEntity> TransactionJournalCorrection
		{
			get { return Create<TransactionJournalCorrectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionJourneyHistory entity</summary>
		public EntityQuery<TransactionJourneyHistoryEntity> TransactionJourneyHistory
		{
			get { return Create<TransactionJourneyHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionToInspection entity</summary>
		public EntityQuery<TransactionToInspectionEntity> TransactionToInspection
		{
			get { return Create<TransactionToInspectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransactionType entity</summary>
		public EntityQuery<TransactionTypeEntity> TransactionType
		{
			get { return Create<TransactionTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TransportCompany entity</summary>
		public EntityQuery<TransportCompanyEntity> TransportCompany
		{
			get { return Create<TransportCompanyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TriggerType entity</summary>
		public EntityQuery<TriggerTypeEntity> TriggerType
		{
			get { return Create<TriggerTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UnitCollection entity</summary>
		public EntityQuery<UnitCollectionEntity> UnitCollection
		{
			get { return Create<UnitCollectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UnitCollectionToUnit entity</summary>
		public EntityQuery<UnitCollectionToUnitEntity> UnitCollectionToUnit
		{
			get { return Create<UnitCollectionToUnitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationResult entity</summary>
		public EntityQuery<ValidationResultEntity> ValidationResult
		{
			get { return Create<ValidationResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationResultResolveType entity</summary>
		public EntityQuery<ValidationResultResolveTypeEntity> ValidationResultResolveType
		{
			get { return Create<ValidationResultResolveTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationRule entity</summary>
		public EntityQuery<ValidationRuleEntity> ValidationRule
		{
			get { return Create<ValidationRuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationRun entity</summary>
		public EntityQuery<ValidationRunEntity> ValidationRun
		{
			get { return Create<ValidationRunEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationRunRule entity</summary>
		public EntityQuery<ValidationRunRuleEntity> ValidationRunRule
		{
			get { return Create<ValidationRunRuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationStatus entity</summary>
		public EntityQuery<ValidationStatusEntity> ValidationStatus
		{
			get { return Create<ValidationStatusEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ValidationValue entity</summary>
		public EntityQuery<ValidationValueEntity> ValidationValue
		{
			get { return Create<ValidationValueEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VdvTransaction entity</summary>
		public EntityQuery<VdvTransactionEntity> VdvTransaction
		{
			get { return Create<VdvTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VerificationAttempt entity</summary>
		public EntityQuery<VerificationAttemptEntity> VerificationAttempt
		{
			get { return Create<VerificationAttemptEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VerificationAttemptType entity</summary>
		public EntityQuery<VerificationAttemptTypeEntity> VerificationAttemptType
		{
			get { return Create<VerificationAttemptTypeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VirtualCard entity</summary>
		public EntityQuery<VirtualCardEntity> VirtualCard
		{
			get { return Create<VirtualCardEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Voucher entity</summary>
		public EntityQuery<VoucherEntity> Voucher
		{
			get { return Create<VoucherEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Whitelist entity</summary>
		public EntityQuery<WhitelistEntity> Whitelist
		{
			get { return Create<WhitelistEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WhitelistJournal entity</summary>
		public EntityQuery<WhitelistJournalEntity> WhitelistJournal
		{
			get { return Create<WhitelistJournalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItem entity</summary>
		public EntityQuery<WorkItemEntity> WorkItem
		{
			get { return Create<WorkItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItemCategory entity</summary>
		public EntityQuery<WorkItemCategoryEntity> WorkItemCategory
		{
			get { return Create<WorkItemCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItemHistory entity</summary>
		public EntityQuery<WorkItemHistoryEntity> WorkItemHistory
		{
			get { return Create<WorkItemHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItemState entity</summary>
		public EntityQuery<WorkItemStateEntity> WorkItemState
		{
			get { return Create<WorkItemStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItemSubject entity</summary>
		public EntityQuery<WorkItemSubjectEntity> WorkItemSubject
		{
			get { return Create<WorkItemSubjectEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WorkItemView entity</summary>
		public EntityQuery<WorkItemViewEntity> WorkItemView
		{
			get { return Create<WorkItemViewEntity>(); }
		}

 

	}
}