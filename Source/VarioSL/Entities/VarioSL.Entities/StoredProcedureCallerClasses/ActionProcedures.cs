﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using VarioSL.Entities.DaoClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses.SelfServicingSpecific;

namespace VarioSL.Entities.StoredProcedureCallerClasses
{
	/// <summary>Class which contains the static logic to execute action stored procedures in the database.</summary>
	public static partial class ActionProcedures
	{

		/// <summary>Delegate definition for stored procedure 'P_WRITEMESSAGE' to be used in combination of a UnitOfWork object.</summary>
		public delegate int WriteMessageCallBack(System.Int64 protocolMessageID, System.Int64 protocolActionID, System.Int64 protocolID, System.Int64 protocolMessagePriority, System.String protocolMessageText, System.DateTime protocolMessageDate,  ITransaction transactionToUse);
		/// <summary>Delegate definition for stored procedure 'DSGVO.P$SL_ANONYMIZECONTRACTID' to be used in combination of a UnitOfWork object.</summary>
		public delegate int AnonymizeContractIDCallBack(System.Decimal pNcontractid, System.Decimal pNclientid,  ITransaction transactionToUse);
		/// <summary>Delegate definition for stored procedure 'P_CH_DELETEFAILEDCLEARINGDATA' to be used in combination of a UnitOfWork object.</summary>
		public delegate int DeleteFailedClearingDataCallBack(System.Decimal clearingID, ref System.Decimal returnCode,  ITransaction transactionToUse);
		/// <summary>Delegate definition for stored procedure 'REQUESTLOCK' to be used in combination of a UnitOfWork object.</summary>
		public delegate int RequestlockCallBack(System.String lockname, System.Decimal timeoutseconds, System.Decimal locknameexpiryseconds, ref System.String lockhandle, ref System.Decimal retcode,  ITransaction transactionToUse);


		/// <summary>Calls stored procedure 'P_WRITEMESSAGE'.<br/><br/></summary>
		/// <param name="protocolMessageID">Input parameter of stored procedure. </param>
		/// <param name="protocolActionID">Input parameter of stored procedure. </param>
		/// <param name="protocolID">Input parameter of stored procedure. </param>
		/// <param name="protocolMessagePriority">Input parameter of stored procedure. </param>
		/// <param name="protocolMessageText">Input parameter of stored procedure. </param>
		/// <param name="protocolMessageDate">Input parameter of stored procedure. </param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int WriteMessage(System.Int64 protocolMessageID, System.Int64 protocolActionID, System.Int64 protocolID, System.Int64 protocolMessagePriority, System.String protocolMessageText, System.DateTime protocolMessageDate)
		{
			return WriteMessage(protocolMessageID, protocolActionID, protocolID, protocolMessagePriority, protocolMessageText, protocolMessageDate, null);
		}

		/// <summary>Calls stored procedure 'P_WRITEMESSAGE'.<br/><br/></summary>
		/// <param name="protocolMessageID">Input parameter of stored procedure. </param>
		/// <param name="protocolActionID">Input parameter of stored procedure. </param>
		/// <param name="protocolID">Input parameter of stored procedure. </param>
		/// <param name="protocolMessagePriority">Input parameter of stored procedure. </param>
		/// <param name="protocolMessageText">Input parameter of stored procedure. </param>
		/// <param name="protocolMessageDate">Input parameter of stored procedure. </param>
		/// <param name="transactionToUse">the transaction to use, or null if no transaction is available.</param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int WriteMessage(System.Int64 protocolMessageID, System.Int64 protocolActionID, System.Int64 protocolID, System.Int64 protocolMessagePriority, System.String protocolMessageText, System.DateTime protocolMessageDate,  ITransaction transactionToUse)
		{
			using(StoredProcedureCall call = CreateWriteMessageCall(new DataAccessCoreImpl(new CommonDaoBase(), transactionToUse), protocolMessageID, protocolActionID, protocolID, protocolMessagePriority, protocolMessageText, protocolMessageDate))
			{
				int toReturn = call.Call();
				return toReturn;
			}
		}

		/// <summary>Calls stored procedure 'DSGVO.P$SL_ANONYMIZECONTRACTID'.<br/><br/></summary>
		/// <param name="pNcontractid">Input parameter of stored procedure. </param>
		/// <param name="pNclientid">Input parameter of stored procedure. </param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AnonymizeContractID(System.Decimal pNcontractid, System.Decimal pNclientid)
		{
			return AnonymizeContractID(pNcontractid, pNclientid, null);
		}

		/// <summary>Calls stored procedure 'DSGVO.P$SL_ANONYMIZECONTRACTID'.<br/><br/></summary>
		/// <param name="pNcontractid">Input parameter of stored procedure. </param>
		/// <param name="pNclientid">Input parameter of stored procedure. </param>
		/// <param name="transactionToUse">the transaction to use, or null if no transaction is available.</param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int AnonymizeContractID(System.Decimal pNcontractid, System.Decimal pNclientid,  ITransaction transactionToUse)
		{
			using(StoredProcedureCall call = CreateAnonymizeContractIDCall(new DataAccessCoreImpl(new CommonDaoBase(), transactionToUse), pNcontractid, pNclientid))
			{
				int toReturn = call.Call();
				return toReturn;
			}
		}

		/// <summary>Calls stored procedure 'P_CH_DELETEFAILEDCLEARINGDATA'.<br/><br/></summary>
		/// <param name="clearingID">Input parameter of stored procedure. </param>
		/// <param name="returnCode">Output parameter of stored procedure. </param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int DeleteFailedClearingData(System.Decimal clearingID, ref System.Decimal returnCode)
		{
			return DeleteFailedClearingData(clearingID, ref returnCode, null);
		}

		/// <summary>Calls stored procedure 'P_CH_DELETEFAILEDCLEARINGDATA'.<br/><br/></summary>
		/// <param name="clearingID">Input parameter of stored procedure. </param>
		/// <param name="returnCode">Output parameter of stored procedure. </param>
		/// <param name="transactionToUse">the transaction to use, or null if no transaction is available.</param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int DeleteFailedClearingData(System.Decimal clearingID, ref System.Decimal returnCode,  ITransaction transactionToUse)
		{
			using(StoredProcedureCall call = CreateDeleteFailedClearingDataCall(new DataAccessCoreImpl(new CommonDaoBase(), transactionToUse), clearingID, returnCode))
			{
				int toReturn = call.Call();
				returnCode = call.GetParameterValue<System.Decimal>(1);
				return toReturn;
			}
		}

		/// <summary>Calls stored procedure 'REQUESTLOCK'.<br/><br/></summary>
		/// <param name="lockhandle">Output parameter of stored procedure. </param>
		/// <param name="retcode">Output parameter of stored procedure. </param>
		/// <param name="lockname">Input parameter of stored procedure. </param>
		/// <param name="timeoutseconds">Input parameter of stored procedure. </param>
		/// <param name="locknameexpiryseconds">Input parameter of stored procedure. </param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int Requestlock(System.String lockname, System.Decimal timeoutseconds, System.Decimal locknameexpiryseconds, ref System.String lockhandle, ref System.Decimal retcode)
		{
			return Requestlock(lockname, timeoutseconds, locknameexpiryseconds, ref lockhandle, ref retcode, null);
		}

		/// <summary>Calls stored procedure 'REQUESTLOCK'.<br/><br/></summary>
		/// <param name="lockhandle">Output parameter of stored procedure. </param>
		/// <param name="retcode">Output parameter of stored procedure. </param>
		/// <param name="lockname">Input parameter of stored procedure. </param>
		/// <param name="timeoutseconds">Input parameter of stored procedure. </param>
		/// <param name="locknameexpiryseconds">Input parameter of stored procedure. </param>
		/// <param name="transactionToUse">the transaction to use, or null if no transaction is available.</param>
		/// <returns>Number of rows affected, if the database / routine doesn't surpress rowcounting.</returns>
		public static int Requestlock(System.String lockname, System.Decimal timeoutseconds, System.Decimal locknameexpiryseconds, ref System.String lockhandle, ref System.Decimal retcode,  ITransaction transactionToUse)
		{
			using(StoredProcedureCall call = CreateRequestlockCall(new DataAccessCoreImpl(new CommonDaoBase(), transactionToUse), lockname, timeoutseconds, locknameexpiryseconds, lockhandle, retcode))
			{
				int toReturn = call.Call();
				lockhandle = call.GetParameterValue<System.String>(0);
				retcode = call.GetParameterValue<System.Decimal>(1);
				return toReturn;
			}
		}

		/// <summary>Creates the call object for the call 'WriteMessage' to stored procedure 'P_WRITEMESSAGE'.</summary>
		/// <param name="dataAccessProvider">The data access provider.</param>
		/// <param name="protocolMessageID">Input parameter</param>
		/// <param name="protocolActionID">Input parameter</param>
		/// <param name="protocolID">Input parameter</param>
		/// <param name="protocolMessagePriority">Input parameter</param>
		/// <param name="protocolMessageText">Input parameter</param>
		/// <param name="protocolMessageDate">Input parameter</param>
		/// <returns>Ready to use StoredProcedureCall object</returns>
		private static StoredProcedureCall CreateWriteMessageCall(IDataAccessCore dataAccessProvider, System.Int64 protocolMessageID, System.Int64 protocolActionID, System.Int64 protocolID, System.Int64 protocolMessagePriority, System.String protocolMessageText, System.DateTime protocolMessageDate)
		{
			return new StoredProcedureCall(dataAccessProvider, "\"SACOG\".\"P_WRITEMESSAGE\"", "WriteMessage")
							.AddParameter("NPROTMESSAGEID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, new SD.LLBLGen.Pro.ORMSupportClasses.Int64DecimalConverter().ConvertTo(null, null, protocolMessageID, typeof(System.Decimal)))
							.AddParameter("NPROTACTIONID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, new SD.LLBLGen.Pro.ORMSupportClasses.Int64DecimalConverter().ConvertTo(null, null, protocolActionID, typeof(System.Decimal)))
							.AddParameter("NPROTID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, new SD.LLBLGen.Pro.ORMSupportClasses.Int64DecimalConverter().ConvertTo(null, null, protocolID, typeof(System.Decimal)))
							.AddParameter("NPROTMESSAGEPRIO", "Decimal", 0, ParameterDirection.Input, true, 38, 38, new SD.LLBLGen.Pro.ORMSupportClasses.Int64DecimalConverter().ConvertTo(null, null, protocolMessagePriority, typeof(System.Decimal)))
							.AddParameter("SPROTMESSAGETEXT", "Varchar2", 4000, ParameterDirection.Input, true, 0, 0, protocolMessageText)
							.AddParameter("DPROTMESSAGEDATE", "Date", 0, ParameterDirection.Input, true, 0, 0, protocolMessageDate);
		}

		/// <summary>Creates the call object for the call 'AnonymizeContractID' to stored procedure 'DSGVO.P$SL_ANONYMIZECONTRACTID'.</summary>
		/// <param name="dataAccessProvider">The data access provider.</param>
		/// <param name="pNcontractid">Input parameter</param>
		/// <param name="pNclientid">Input parameter</param>
		/// <returns>Ready to use StoredProcedureCall object</returns>
		private static StoredProcedureCall CreateAnonymizeContractIDCall(IDataAccessCore dataAccessProvider, System.Decimal pNcontractid, System.Decimal pNclientid)
		{
			return new StoredProcedureCall(dataAccessProvider, "\"SACOG\".\"DSGVO.P$SL_ANONYMIZECONTRACTID\"", "AnonymizeContractID")
							.AddParameter("P_NCONTRACTID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, pNcontractid)
							.AddParameter("P_NCLIENTID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, pNclientid);
		}

		/// <summary>Creates the call object for the call 'DeleteFailedClearingData' to stored procedure 'P_CH_DELETEFAILEDCLEARINGDATA'.</summary>
		/// <param name="dataAccessProvider">The data access provider.</param>
		/// <param name="clearingID">Input parameter</param>
		/// <param name="returnCode">Output parameter</param>
		/// <returns>Ready to use StoredProcedureCall object</returns>
		private static StoredProcedureCall CreateDeleteFailedClearingDataCall(IDataAccessCore dataAccessProvider, System.Decimal clearingID, System.Decimal returnCode)
		{
			return new StoredProcedureCall(dataAccessProvider, "\"SACOG\".\"P_CH_DELETEFAILEDCLEARINGDATA\"", "DeleteFailedClearingData")
							.AddParameter("CLEARING_ID", "Decimal", 0, ParameterDirection.Input, true, 38, 38, clearingID)
							.AddParameter("RETURN_CODE", "Decimal", 0, ParameterDirection.Output, true, 38, 38, returnCode);
		}

		/// <summary>Creates the call object for the call 'Requestlock' to stored procedure 'REQUESTLOCK'.</summary>
		/// <param name="dataAccessProvider">The data access provider.</param>
		/// <param name="lockhandle">Output parameter</param>
		/// <param name="retcode">Output parameter</param>
		/// <param name="lockname">Input parameter</param>
		/// <param name="timeoutseconds">Input parameter</param>
		/// <param name="locknameexpiryseconds">Input parameter</param>
		/// <returns>Ready to use StoredProcedureCall object</returns>
		private static StoredProcedureCall CreateRequestlockCall(IDataAccessCore dataAccessProvider, System.String lockname, System.Decimal timeoutseconds, System.Decimal locknameexpiryseconds, System.String lockhandle, System.Decimal retcode)
		{
			return new StoredProcedureCall(dataAccessProvider, "\"SACOG\".\"REQUESTLOCK\"", "Requestlock")
							.AddParameter("LOCKHANDLE", "Varchar2", 4000, ParameterDirection.Output, true, 0, 0, lockhandle)
							.AddParameter("RETCODE", "Decimal", 0, ParameterDirection.Output, true, 38, 38, retcode)
							.AddParameter("LOCKNAME", "Varchar2", 4000, ParameterDirection.Input, true, 0, 0, lockname)
							.AddParameter("TIMEOUTSECONDS", "Decimal", 0, ParameterDirection.Input, true, 38, 38, timeoutseconds)
							.AddParameter("LOCKNAMEEXPIRYSECONDS", "Decimal", 0, ParameterDirection.Input, true, 38, 38, locknameexpiryseconds);
		}



		#region Included Code

		#endregion
	}
}
