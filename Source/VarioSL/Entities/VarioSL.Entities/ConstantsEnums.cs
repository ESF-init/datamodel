﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace VarioSL.Entities
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AbortedTransaction.</summary>
	public enum AbortedTransactionFieldIndex
	{
		///<summary>CancellationID. </summary>
		CancellationID,
		///<summary>CardBalance. </summary>
		CardBalance,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardIssuerID. </summary>
		CardIssuerID,
		///<summary>CardReferenceTransactionNumber. </summary>
		CardReferenceTransactionNumber,
		///<summary>CardTransactionNumber. </summary>
		CardTransactionNumber,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DeviceBookingState. </summary>
		DeviceBookingState,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>DevicePaymentMethod. </summary>
		DevicePaymentMethod,
		///<summary>ImportDateTime. </summary>
		ImportDateTime,
		///<summary>Line. </summary>
		Line,
		///<summary>LineName. </summary>
		LineName,
		///<summary>PayCardNumber. </summary>
		PayCardNumber,
		///<summary>Price. </summary>
		Price,
		///<summary>RouteNumber. </summary>
		RouteNumber,
		///<summary>Saleschannel. </summary>
		Saleschannel,
		///<summary>ShiftID. </summary>
		ShiftID,
		///<summary>StopFrom. </summary>
		StopFrom,
		///<summary>StopTo. </summary>
		StopTo,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketInstanceID. </summary>
		TicketInstanceID,
		///<summary>TikNumber. </summary>
		TikNumber,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TripDateTime. </summary>
		TripDateTime,
		///<summary>TripSerialNumber. </summary>
		TripSerialNumber,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>ValueOfRide. </summary>
		ValueOfRide,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountBalance.</summary>
	public enum AccountBalanceFieldIndex
	{
		///<summary>Balance. </summary>
		Balance,
		///<summary>BalanceID. </summary>
		BalanceID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Month. </summary>
		Month,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountEntry.</summary>
	public enum AccountEntryFieldIndex
	{
		///<summary>AccountType. </summary>
		AccountType,
		///<summary>AccSettlementID. </summary>
		AccSettlementID,
		///<summary>Amount. </summary>
		Amount,
		///<summary>AmountCashDelivered. </summary>
		AmountCashDelivered,
		///<summary>AmountCashDeposit. </summary>
		AmountCashDeposit,
		///<summary>AmountPosted. </summary>
		AmountPosted,
		///<summary>AmountSubContractor. </summary>
		AmountSubContractor,
		///<summary>BillID. </summary>
		BillID,
		///<summary>BookDateTime. </summary>
		BookDateTime,
		///<summary>BookingKeyID. </summary>
		BookingKeyID,
		///<summary>BookingSummaryID. </summary>
		BookingSummaryID,
		///<summary>CancelationEntryID. </summary>
		CancelationEntryID,
		///<summary>CashCoverage. </summary>
		CashCoverage,
		///<summary>CashDeficit. </summary>
		CashDeficit,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CostLocationID. </summary>
		CostLocationID,
		///<summary>DutyBalanceID. </summary>
		DutyBalanceID,
		///<summary>EntryID. </summary>
		EntryID,
		///<summary>EntryNumber. </summary>
		EntryNumber,
		///<summary>EntryTypeNumber. </summary>
		EntryTypeNumber,
		///<summary>ExportDateTime. </summary>
		ExportDateTime,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>FromStopID. </summary>
		FromStopID,
		///<summary>IncomingTypeID. </summary>
		IncomingTypeID,
		///<summary>IntermediateFareStageID. </summary>
		IntermediateFareStageID,
		///<summary>InvoiceNumber. </summary>
		InvoiceNumber,
		///<summary>IsCleared. </summary>
		IsCleared,
		///<summary>LineFreeText. </summary>
		LineFreeText,
		///<summary>LineID. </summary>
		LineID,
		///<summary>PaymentMethodID. </summary>
		PaymentMethodID,
		///<summary>PersonQuantity. </summary>
		PersonQuantity,
		///<summary>PostingText. </summary>
		PostingText,
		///<summary>PrintCount. </summary>
		PrintCount,
		///<summary>RoeBookingKeyID. </summary>
		RoeBookingKeyID,
		///<summary>RoeCostLocationID. </summary>
		RoeCostLocationID,
		///<summary>RoeIncomingTypeID. </summary>
		RoeIncomingTypeID,
		///<summary>RoeQuantityPerson. </summary>
		RoeQuantityPerson,
		///<summary>RoeTicketSpeciesID. </summary>
		RoeTicketSpeciesID,
		///<summary>SalesTaxID. </summary>
		SalesTaxID,
		///<summary>SeasonTicketID. </summary>
		SeasonTicketID,
		///<summary>TicketSpeciesID. </summary>
		TicketSpeciesID,
		///<summary>ToStopID. </summary>
		ToStopID,
		///<summary>UserID. </summary>
		UserID,
		///<summary>ValueDateTime. </summary>
		ValueDateTime,
		///<summary>VoucherAmount. </summary>
		VoucherAmount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountEntryNumber.</summary>
	public enum AccountEntryNumberFieldIndex
	{
		///<summary>MaximumEntryNumber. </summary>
		MaximumEntryNumber,
		///<summary>TrafficCompanyID. </summary>
		TrafficCompanyID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountPostingKey.</summary>
	public enum AccountPostingKeyFieldIndex
	{
		///<summary>BillHidden. </summary>
		BillHidden,
		///<summary>EntryRangeID. </summary>
		EntryRangeID,
		///<summary>EntryTypeName. </summary>
		EntryTypeName,
		///<summary>EntryTypeNumber. </summary>
		EntryTypeNumber,
		///<summary>IsBillClear. </summary>
		IsBillClear,
		///<summary>IsBillShown. </summary>
		IsBillShown,
		///<summary>IsCancelation. </summary>
		IsCancelation,
		///<summary>IsDebitEntry. </summary>
		IsDebitEntry,
		///<summary>IsExport. </summary>
		IsExport,
		///<summary>IsManual. </summary>
		IsManual,
		///<summary>IsReturnDebit. </summary>
		IsReturnDebit,
		///<summary>Sign. </summary>
		Sign,
		///<summary>SortSequence. </summary>
		SortSequence,
		///<summary>TaxRate. </summary>
		TaxRate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Application.</summary>
	public enum ApplicationFieldIndex
	{
		///<summary>ApplicationID. </summary>
		ApplicationID,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ApplicationVersion.</summary>
	public enum ApplicationVersionFieldIndex
	{
		///<summary>ApplicationDate. </summary>
		ApplicationDate,
		///<summary>ApplicationID. </summary>
		ApplicationID,
		///<summary>ApplicationVersionID. </summary>
		ApplicationVersionID,
		///<summary>LastRun. </summary>
		LastRun,
		///<summary>Version. </summary>
		Version,
		///<summary>Workstation. </summary>
		Workstation,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Apportionment.</summary>
	public enum ApportionmentFieldIndex
	{
		///<summary>ApportionmentID. </summary>
		ApportionmentID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PeriodEndDate. </summary>
		PeriodEndDate,
		///<summary>PeriodStartDate. </summary>
		PeriodStartDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ApportionmentResult.</summary>
	public enum ApportionmentResultFieldIndex
	{
		///<summary>AcquirerID. </summary>
		AcquirerID,
		///<summary>Amount. </summary>
		Amount,
		///<summary>ApportionmentID. </summary>
		ApportionmentID,
		///<summary>ApportionmentResultID. </summary>
		ApportionmentResultID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>Direction. </summary>
		Direction,
		///<summary>FromClientID. </summary>
		FromClientID,
		///<summary>FromTransactionID. </summary>
		FromTransactionID,
		///<summary>JourneyReference. </summary>
		JourneyReference,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TikNumber. </summary>
		TikNumber,
		///<summary>ToClientID. </summary>
		ToClientID,
		///<summary>ToTransactionID. </summary>
		ToTransactionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AreaList.</summary>
	public enum AreaListFieldIndex
	{
		///<summary>AreaListElementGroupId. </summary>
		AreaListElementGroupId,
		///<summary>AreaListID. </summary>
		AreaListID,
		///<summary>AreaListKey. </summary>
		AreaListKey,
		///<summary>AreaListKeyGroup. </summary>
		AreaListKeyGroup,
		///<summary>AreaListTypeID. </summary>
		AreaListTypeID,
		///<summary>OrgID. </summary>
		OrgID,
		///<summary>Sequential. </summary>
		Sequential,
		///<summary>SpatialReferenceNumber. </summary>
		SpatialReferenceNumber,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketGroupAttributeID. </summary>
		TicketGroupAttributeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AreaListElement.</summary>
	public enum AreaListElementFieldIndex
	{
		///<summary>AreaInstanceTypeID. </summary>
		AreaInstanceTypeID,
		///<summary>AreaListElementID. </summary>
		AreaListElementID,
		///<summary>AreaListID. </summary>
		AreaListID,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>FareStageID. </summary>
		FareStageID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AreaListElementGroup.</summary>
	public enum AreaListElementGroupFieldIndex
	{
		///<summary>AreaListeElementGroupId. </summary>
		AreaListeElementGroupId,
		///<summary>GroupName. </summary>
		GroupName,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AreaType.</summary>
	public enum AreaTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>TypeGroupID. </summary>
		TypeGroupID,
		///<summary>TypeID. </summary>
		TypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Attribute.</summary>
	public enum AttributeFieldIndex
	{
		///<summary>AttribclassID. </summary>
		AttribclassID,
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayColumn. </summary>
		DisplayColumn,
		///<summary>DisplayLength. </summary>
		DisplayLength,
		///<summary>DisplayRow. </summary>
		DisplayRow,
		///<summary>DotnetTypeOfName. </summary>
		DotnetTypeOfName,
		///<summary>HasMultipleValues. </summary>
		HasMultipleValues,
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>ReadOnly. </summary>
		ReadOnly,
		///<summary>Showinguixml. </summary>
		Showinguixml,
		///<summary>ShowInMatrix. </summary>
		ShowInMatrix,
		///<summary>ShowInTickets. </summary>
		ShowInTickets,
		///<summary>TarifID. </summary>
		TarifID,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeBitmapLayoutObject.</summary>
	public enum AttributeBitmapLayoutObjectFieldIndex
	{
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>Column. </summary>
		Column,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>Row. </summary>
		Row,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeTextLayoutObject.</summary>
	public enum AttributeTextLayoutObjectFieldIndex
	{
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>Column. </summary>
		Column,
		///<summary>FieldWidth. </summary>
		FieldWidth,
		///<summary>FontSize. </summary>
		FontSize,
		///<summary>Height. </summary>
		Height,
		///<summary>IsBold. </summary>
		IsBold,
		///<summary>IsDouble. </summary>
		IsDouble,
		///<summary>IsInverse. </summary>
		IsInverse,
		///<summary>IsUnderlined. </summary>
		IsUnderlined,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>MaxLength. </summary>
		MaxLength,
		///<summary>MinLength. </summary>
		MinLength,
		///<summary>NoLayoutText. </summary>
		NoLayoutText,
		///<summary>Row. </summary>
		Row,
		///<summary>TextAlignment. </summary>
		TextAlignment,
		///<summary>Width. </summary>
		Width,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeValue.</summary>
	public enum AttributeValueFieldIndex
	{
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayText. </summary>
		DisplayText,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>EtickLayoutText. </summary>
		EtickLayoutText,
		///<summary>FistLayoutText. </summary>
		FistLayoutText,
		///<summary>Logo1ID. </summary>
		Logo1ID,
		///<summary>Logo2ID. </summary>
		Logo2ID,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>Restricted. </summary>
		Restricted,
		///<summary>SecondLayoutText. </summary>
		SecondLayoutText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeValueList.</summary>
	public enum AttributeValueListFieldIndex
	{
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Automat.</summary>
	public enum AutomatFieldIndex
	{
		///<summary>AutomatID. </summary>
		AutomatID,
		///<summary>AutomatNumber. </summary>
		AutomatNumber,
		///<summary>CashAmount. </summary>
		CashAmount,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>DeviceClass. </summary>
		DeviceClass,
		///<summary>Error. </summary>
		Error,
		///<summary>LastAlarm. </summary>
		LastAlarm,
		///<summary>LastData. </summary>
		LastData,
		///<summary>MountingPlateNumber. </summary>
		MountingPlateNumber,
		///<summary>Notice. </summary>
		Notice,
		///<summary>VehicleNo. </summary>
		VehicleNo,
		///<summary>Warning. </summary>
		Warning,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Bank.</summary>
	public enum BankFieldIndex
	{
		///<summary>BankCode. </summary>
		BankCode,
		///<summary>BankID. </summary>
		BankID,
		///<summary>BIC. </summary>
		BIC,
		///<summary>Location. </summary>
		Location,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BinaryData.</summary>
	public enum BinaryDataFieldIndex
	{
		///<summary>BinaryDataID. </summary>
		BinaryDataID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Data. </summary>
		Data,
		///<summary>DataType. </summary>
		DataType,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>HashValue. </summary>
		HashValue,
		///<summary>Length. </summary>
		Length,
		///<summary>ShiftID. </summary>
		ShiftID,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BlockingReason.</summary>
	public enum BlockingReasonFieldIndex
	{
		///<summary>CodeNo. </summary>
		CodeNo,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Language. </summary>
		Language,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRule.</summary>
	public enum BusinessRuleFieldIndex
	{
		///<summary>BusinessRuleID. </summary>
		BusinessRuleID,
		///<summary>BusinessRuleName. </summary>
		BusinessRuleName,
		///<summary>BusinessRuleTypeID. </summary>
		BusinessRuleTypeID,
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>ReadOnly. </summary>
		ReadOnly,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleClause.</summary>
	public enum BusinessRuleClauseFieldIndex
	{
		///<summary>Accumulate. </summary>
		Accumulate,
		///<summary>BusinessRuleClauseID. </summary>
		BusinessRuleClauseID,
		///<summary>BusinessRuleConditionID. </summary>
		BusinessRuleConditionID,
		///<summary>BusinessRuleID. </summary>
		BusinessRuleID,
		///<summary>BusinessruleResultID. </summary>
		BusinessruleResultID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleCondition.</summary>
	public enum BusinessRuleConditionFieldIndex
	{
		///<summary>BusinessRuleConditionID. </summary>
		BusinessRuleConditionID,
		///<summary>BusinessRuleTypeID. </summary>
		BusinessRuleTypeID,
		///<summary>ConditionName. </summary>
		ConditionName,
		///<summary>Criteria. </summary>
		Criteria,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleResult.</summary>
	public enum BusinessRuleResultFieldIndex
	{
		///<summary>BusinessRuleResultID. </summary>
		BusinessRuleResultID,
		///<summary>BusinessRuleTypeID. </summary>
		BusinessRuleTypeID,
		///<summary>DestinationFarestageID. </summary>
		DestinationFarestageID,
		///<summary>ExternalTicketName. </summary>
		ExternalTicketName,
		///<summary>GuiPanelName. </summary>
		GuiPanelName,
		///<summary>InfoText. </summary>
		InfoText,
		///<summary>KeySelectionModeID. </summary>
		KeySelectionModeID,
		///<summary>LineNo. </summary>
		LineNo,
		///<summary>NxErrorCode. </summary>
		NxErrorCode,
		///<summary>OfflineCredit. </summary>
		OfflineCredit,
		///<summary>PeriodCalculationModeID. </summary>
		PeriodCalculationModeID,
		///<summary>PriceCalculationModeID. </summary>
		PriceCalculationModeID,
		///<summary>ResultName. </summary>
		ResultName,
		///<summary>SupplementTicketInternalNumber. </summary>
		SupplementTicketInternalNumber,
		///<summary>Surcharge. </summary>
		Surcharge,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>Ticket. </summary>
		Ticket,
		///<summary>TransactionTypeID. </summary>
		TransactionTypeID,
		///<summary>TransferTime. </summary>
		TransferTime,
		///<summary>UserGroupAttributeValue. </summary>
		UserGroupAttributeValue,
		///<summary>Valid. </summary>
		Valid,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleType.</summary>
	public enum BusinessRuleTypeFieldIndex
	{
		///<summary>AllowAccumulation. </summary>
		AllowAccumulation,
		///<summary>BusinessRuleTypeGroup. </summary>
		BusinessRuleTypeGroup,
		///<summary>BusinessRuleTypeID. </summary>
		BusinessRuleTypeID,
		///<summary>BusinessRuleTypeName. </summary>
		BusinessRuleTypeName,
		///<summary>Description. </summary>
		Description,
		///<summary>StandAlone. </summary>
		StandAlone,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleTypeToVariable.</summary>
	public enum BusinessRuleTypeToVariableFieldIndex
	{
		///<summary>BusinessRuleTypeID. </summary>
		BusinessRuleTypeID,
		///<summary>BusinessRuleVariableID. </summary>
		BusinessRuleVariableID,
		///<summary>Mandatory. </summary>
		Mandatory,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BusinessRuleVariable.</summary>
	public enum BusinessRuleVariableFieldIndex
	{
		///<summary>BusinessRuleVariableID. </summary>
		BusinessRuleVariableID,
		///<summary>BusinessruleVariableName. </summary>
		BusinessruleVariableName,
		///<summary>BusinessruleVariableType. </summary>
		BusinessruleVariableType,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>Direction. </summary>
		Direction,
		///<summary>DotNetType. </summary>
		DotNetType,
		///<summary>KeyString. </summary>
		KeyString,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Calendar.</summary>
	public enum CalendarFieldIndex
	{
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>OwnerClientId. </summary>
		OwnerClientId,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CalendarEntry.</summary>
	public enum CalendarEntryFieldIndex
	{
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>CalndarEntryId. </summary>
		CalndarEntryId,
		///<summary>Date. </summary>
		Date,
		///<summary>DayTypeEntryID. </summary>
		DayTypeEntryID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardActionAttribute.</summary>
	public enum CardActionAttributeFieldIndex
	{
		///<summary>CardActionAttributeID. </summary>
		CardActionAttributeID,
		///<summary>CardActionRequestID. </summary>
		CardActionRequestID,
		///<summary>DateOfBirth. </summary>
		DateOfBirth,
		///<summary>Deposit. </summary>
		Deposit,
		///<summary>ExpiryDate. </summary>
		ExpiryDate,
		///<summary>GroupExpiry. </summary>
		GroupExpiry,
		///<summary>IssueFlag. </summary>
		IssueFlag,
		///<summary>IssuerID. </summary>
		IssuerID,
		///<summary>PrintedCardNo. </summary>
		PrintedCardNo,
		///<summary>Registration. </summary>
		Registration,
		///<summary>RemoveBirthday. </summary>
		RemoveBirthday,
		///<summary>UserGroupID. </summary>
		UserGroupID,
		///<summary>VoiceFlag. </summary>
		VoiceFlag,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardActionRequest.</summary>
	public enum CardActionRequestFieldIndex
	{
		///<summary>CardActionRequestID. </summary>
		CardActionRequestID,
		///<summary>CardNo. </summary>
		CardNo,
		///<summary>ChargeValue. </summary>
		ChargeValue,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>IsProcessed. </summary>
		IsProcessed,
		///<summary>IsValid. </summary>
		IsValid,
		///<summary>ProcessDate. </summary>
		ProcessDate,
		///<summary>RequestNo. </summary>
		RequestNo,
		///<summary>RequestSource. </summary>
		RequestSource,
		///<summary>RequestType. </summary>
		RequestType,
		///<summary>Transactionid. </summary>
		Transactionid,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardActionRequestType.</summary>
	public enum CardActionRequestTypeFieldIndex
	{
		///<summary>AddToActionList. </summary>
		AddToActionList,
		///<summary>CardActionRequestTypeID. </summary>
		CardActionRequestTypeID,
		///<summary>RequestName. </summary>
		RequestName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardChipType.</summary>
	public enum CardChipTypeFieldIndex
	{
		///<summary>Abbreviation. </summary>
		Abbreviation,
		///<summary>CardChipTypeID. </summary>
		CardChipTypeID,
		///<summary>Category. </summary>
		Category,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceMappingNumber. </summary>
		DeviceMappingNumber,
		///<summary>MaximumCappingPotCount. </summary>
		MaximumCappingPotCount,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardState.</summary>
	public enum CardStateFieldIndex
	{
		///<summary>CodeNo. </summary>
		CodeNo,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Language. </summary>
		Language,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardTicket.</summary>
	public enum CardTicketFieldIndex
	{
		///<summary>CardPhysicalDetailID. </summary>
		CardPhysicalDetailID,
		///<summary>CardTicketID. </summary>
		CardTicketID,
		///<summary>CardTicketName. </summary>
		CardTicketName,
		///<summary>FormID. </summary>
		FormID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardTicketToTicket.</summary>
	public enum CardTicketToTicketFieldIndex
	{
		///<summary>CardTicketID. </summary>
		CardTicketID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CashService.</summary>
	public enum CashServiceFieldIndex
	{
		///<summary>AutomatID. </summary>
		AutomatID,
		///<summary>CashAmountBegin. </summary>
		CashAmountBegin,
		///<summary>CashAmountEnd. </summary>
		CashAmountEnd,
		///<summary>Cashpaymentid. </summary>
		Cashpaymentid,
		///<summary>CashServiceBegin. </summary>
		CashServiceBegin,
		///<summary>CashServiceEnd. </summary>
		CashServiceEnd,
		///<summary>CashServiceID. </summary>
		CashServiceID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>IsComplete. </summary>
		IsComplete,
		///<summary>IsLastCashService. </summary>
		IsLastCashService,
		///<summary>ServiceStaffNumber. </summary>
		ServiceStaffNumber,
		///<summary>SettlementID. </summary>
		SettlementID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CashServiceBalance.</summary>
	public enum CashServiceBalanceFieldIndex
	{
		///<summary>BalanceID. </summary>
		BalanceID,
		///<summary>CashCredit. </summary>
		CashCredit,
		///<summary>CashDebit. </summary>
		CashDebit,
		///<summary>CashServiceID. </summary>
		CashServiceID,
		///<summary>ComponentNumber. </summary>
		ComponentNumber,
		///<summary>ComponentTypeID. </summary>
		ComponentTypeID,
		///<summary>Slot. </summary>
		Slot,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CashServiceData.</summary>
	public enum CashServiceDataFieldIndex
	{
		///<summary>CashCredit. </summary>
		CashCredit,
		///<summary>CashDebit. </summary>
		CashDebit,
		///<summary>CashServiceDataID. </summary>
		CashServiceDataID,
		///<summary>CashServiceID. </summary>
		CashServiceID,
		///<summary>CashUnitID. </summary>
		CashUnitID,
		///<summary>ChangeReason. </summary>
		ChangeReason,
		///<summary>ComponentNumber. </summary>
		ComponentNumber,
		///<summary>ComponentStateID. </summary>
		ComponentStateID,
		///<summary>ComponentTypeID. </summary>
		ComponentTypeID,
		///<summary>CurrentValue. </summary>
		CurrentValue,
		///<summary>Slot. </summary>
		Slot,
		///<summary>ValueDate. </summary>
		ValueDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CashUnit.</summary>
	public enum CashUnitFieldIndex
	{
		///<summary>CashUnitID. </summary>
		CashUnitID,
		///<summary>CashUnitName. </summary>
		CashUnitName,
		///<summary>CashUnitWorth. </summary>
		CashUnitWorth,
		///<summary>Description. </summary>
		Description,
		///<summary>IsCoin. </summary>
		IsCoin,
		///<summary>Literal. </summary>
		Literal,
		///<summary>MaxCount. </summary>
		MaxCount,
		///<summary>MinCount. </summary>
		MinCount,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Choice.</summary>
	public enum ChoiceFieldIndex
	{
		///<summary>Arealistkey. </summary>
		Arealistkey,
		///<summary>Arealistkeyback. </summary>
		Arealistkeyback,
		///<summary>ChoiceID. </summary>
		ChoiceID,
		///<summary>Directionattributevalueid. </summary>
		Directionattributevalueid,
		///<summary>DistanceAttributeID. </summary>
		DistanceAttributeID,
		///<summary>RouteNameID. </summary>
		RouteNameID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TextString. </summary>
		TextString,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>ValueString. </summary>
		ValueString,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Clearing.</summary>
	public enum ClearingFieldIndex
	{
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClearingState. </summary>
		ClearingState,
		///<summary>ClearingType. </summary>
		ClearingType,
		///<summary>CreateDate. </summary>
		CreateDate,
		///<summary>Liability. </summary>
		Liability,
		///<summary>PeriodEndDate. </summary>
		PeriodEndDate,
		///<summary>PeriodStartDate. </summary>
		PeriodStartDate,
		///<summary>PreviousClearingID. </summary>
		PreviousClearingID,
		///<summary>SettlementID. </summary>
		SettlementID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingClassification.</summary>
	public enum ClearingClassificationFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingDetail.</summary>
	public enum ClearingDetailFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ClearingDetailID. </summary>
		ClearingDetailID,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>ZoneID. </summary>
		ZoneID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingResult.</summary>
	public enum ClearingResultFieldIndex
	{
		///<summary>BadAmount. </summary>
		BadAmount,
		///<summary>BadAmountTaxed. </summary>
		BadAmountTaxed,
		///<summary>BadCount. </summary>
		BadCount,
		///<summary>CardIssuerID. </summary>
		CardIssuerID,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClearingResultID. </summary>
		ClearingResultID,
		///<summary>ClearingResultLevel. </summary>
		ClearingResultLevel,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractorID. </summary>
		ContractorID,
		///<summary>CorrectedAmount. </summary>
		CorrectedAmount,
		///<summary>CorrectedAmountTaxed. </summary>
		CorrectedAmountTaxed,
		///<summary>CorrectedCount. </summary>
		CorrectedCount,
		///<summary>CustomerGroupID. </summary>
		CustomerGroupID,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>FromClient. </summary>
		FromClient,
		///<summary>GoodAmount. </summary>
		GoodAmount,
		///<summary>GoodAmountTaxed. </summary>
		GoodAmountTaxed,
		///<summary>GoodCount. </summary>
		GoodCount,
		///<summary>IsCancellation. </summary>
		IsCancellation,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>LineID. </summary>
		LineID,
		///<summary>PtomUnit. </summary>
		PtomUnit,
		///<summary>QuarantinedAmount. </summary>
		QuarantinedAmount,
		///<summary>QuarantinedAmountTaxed. </summary>
		QuarantinedAmountTaxed,
		///<summary>QuarantinedCount. </summary>
		QuarantinedCount,
		///<summary>ResponsibleClientID. </summary>
		ResponsibleClientID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketType. </summary>
		TicketType,
		///<summary>ToClient. </summary>
		ToClient,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		///<summary>TransactionTypeID. </summary>
		TransactionTypeID,
		///<summary>TripCode. </summary>
		TripCode,
		///<summary>ValidationAmount. </summary>
		ValidationAmount,
		///<summary>ValidationAmountTaxed. </summary>
		ValidationAmountTaxed,
		///<summary>ValidationCount. </summary>
		ValidationCount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingResultLevel.</summary>
	public enum ClearingResultLevelFieldIndex
	{
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingState.</summary>
	public enum ClearingStateFieldIndex
	{
		///<summary>ClearingStateID. </summary>
		ClearingStateID,
		///<summary>ClearingStateName. </summary>
		ClearingStateName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingSum.</summary>
	public enum ClearingSumFieldIndex
	{
		///<summary>ClearingAmount. </summary>
		ClearingAmount,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClearingSumID. </summary>
		ClearingSumID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DiscountAmount. </summary>
		DiscountAmount,
		///<summary>ManualAdjustmentAmount. </summary>
		ManualAdjustmentAmount,
		///<summary>ReloadAmount. </summary>
		ReloadAmount,
		///<summary>SalesAmountCard. </summary>
		SalesAmountCard,
		///<summary>SalesAmountTickets. </summary>
		SalesAmountTickets,
		///<summary>UnusedAmount. </summary>
		UnusedAmount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClearingTransaction.</summary>
	public enum ClearingTransactionFieldIndex
	{
		///<summary>ClearingClassification. </summary>
		ClearingClassification,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClearingTransactionID. </summary>
		ClearingTransactionID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Client.</summary>
	public enum ClientFieldIndex
	{
		///<summary>Addition. </summary>
		Addition,
		///<summary>BankAccountNo. </summary>
		BankAccountNo,
		///<summary>BankLocation. </summary>
		BankLocation,
		///<summary>BankName. </summary>
		BankName,
		///<summary>BankNo. </summary>
		BankNo,
		///<summary>BIC. </summary>
		BIC,
		///<summary>City. </summary>
		City,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ClientKey. </summary>
		ClientKey,
		///<summary>CompanyID. </summary>
		CompanyID,
		///<summary>CompanyName. </summary>
		CompanyName,
		///<summary>CreditorID. </summary>
		CreditorID,
		///<summary>DefaultLineNoBundle. </summary>
		DefaultLineNoBundle,
		///<summary>Description. </summary>
		Description,
		///<summary>Email. </summary>
		Email,
		///<summary>ExternalCompanyNo. </summary>
		ExternalCompanyNo,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Fon. </summary>
		Fon,
		///<summary>HandlingChargedDebitEntry. </summary>
		HandlingChargedDebitEntry,
		///<summary>IBAN. </summary>
		IBAN,
		///<summary>LongName. </summary>
		LongName,
		///<summary>MasterClientID. </summary>
		MasterClientID,
		///<summary>PoBox. </summary>
		PoBox,
		///<summary>PostalCodePoBox. </summary>
		PostalCodePoBox,
		///<summary>PostalCodeStreet. </summary>
		PostalCodeStreet,
		///<summary>Sign. </summary>
		Sign,
		///<summary>Street. </summary>
		Street,
		///<summary>StreetNo. </summary>
		StreetNo,
		///<summary>TermOfPayment. </summary>
		TermOfPayment,
		///<summary>VolNo. </summary>
		VolNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientAdaptedLayoutObject.</summary>
	public enum ClientAdaptedLayoutObjectFieldIndex
	{
		///<summary>ClientAdaptedLayoutObjectId. </summary>
		ClientAdaptedLayoutObjectId,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>FixedBitmapName. </summary>
		FixedBitmapName,
		///<summary>FixedText. </summary>
		FixedText,
		///<summary>LayoutName. </summary>
		LayoutName,
		///<summary>LayoutNumber. </summary>
		LayoutNumber,
		///<summary>LayoutObjectNumber. </summary>
		LayoutObjectNumber,
		///<summary>LayoutObjectType. </summary>
		LayoutObjectType,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Component.</summary>
	public enum ComponentFieldIndex
	{
		///<summary>AutomatID. </summary>
		AutomatID,
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>ComponentNo. </summary>
		ComponentNo,
		///<summary>ComponentTypeID. </summary>
		ComponentTypeID,
		///<summary>PrintedComponentNo. </summary>
		PrintedComponentNo,
		///<summary>ResetDate. </summary>
		ResetDate,
		///<summary>Slot. </summary>
		Slot,
		///<summary>StatusID. </summary>
		StatusID,
		///<summary>StockWarning. </summary>
		StockWarning,
		///<summary>ValueDate. </summary>
		ValueDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComponentClearing.</summary>
	public enum ComponentClearingFieldIndex
	{
		///<summary>AutomatID. </summary>
		AutomatID,
		///<summary>BookingNo. </summary>
		BookingNo,
		///<summary>CashUnitID. </summary>
		CashUnitID,
		///<summary>ClearingDate. </summary>
		ClearingDate,
		///<summary>ComponentClearingID. </summary>
		ComponentClearingID,
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>CountingAmount. </summary>
		CountingAmount,
		///<summary>Description. </summary>
		Description,
		///<summary>FeedDate. </summary>
		FeedDate,
		///<summary>IsBooked. </summary>
		IsBooked,
		///<summary>LocationMark. </summary>
		LocationMark,
		///<summary>LoginDate. </summary>
		LoginDate,
		///<summary>MaintenanceStaffID. </summary>
		MaintenanceStaffID,
		///<summary>MountingPlateNumber. </summary>
		MountingPlateNumber,
		///<summary>NumberOfCoins. </summary>
		NumberOfCoins,
		///<summary>PersonnelClearingID. </summary>
		PersonnelClearingID,
		///<summary>ReadingAmount. </summary>
		ReadingAmount,
		///<summary>StorageMediumName. </summary>
		StorageMediumName,
		///<summary>StorageMediumNo. </summary>
		StorageMediumNo,
		///<summary>VehicleNo. </summary>
		VehicleNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComponentFilling.</summary>
	public enum ComponentFillingFieldIndex
	{
		///<summary>AutomatID. </summary>
		AutomatID,
		///<summary>BookingNo. </summary>
		BookingNo,
		///<summary>CashUnitID. </summary>
		CashUnitID,
		///<summary>ComponentFillingID. </summary>
		ComponentFillingID,
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>FeedDate. </summary>
		FeedDate,
		///<summary>FillingDate. </summary>
		FillingDate,
		///<summary>IsBooked. </summary>
		IsBooked,
		///<summary>LocationMark. </summary>
		LocationMark,
		///<summary>LoginDate. </summary>
		LoginDate,
		///<summary>MaintenanceStaffID. </summary>
		MaintenanceStaffID,
		///<summary>MountingPlateNumber. </summary>
		MountingPlateNumber,
		///<summary>NumberOfCoins. </summary>
		NumberOfCoins,
		///<summary>PersonnelFillingID. </summary>
		PersonnelFillingID,
		///<summary>StorageMediumName. </summary>
		StorageMediumName,
		///<summary>StorageMediumNo. </summary>
		StorageMediumNo,
		///<summary>VehicleNo. </summary>
		VehicleNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComponentState.</summary>
	public enum ComponentStateFieldIndex
	{
		///<summary>ComponentStateID. </summary>
		ComponentStateID,
		///<summary>ComponentStateName. </summary>
		ComponentStateName,
		///<summary>ComponentTypeID. </summary>
		ComponentTypeID,
		///<summary>IsError. </summary>
		IsError,
		///<summary>StateID. </summary>
		StateID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ComponentType.</summary>
	public enum ComponentTypeFieldIndex
	{
		///<summary>ComponentName. </summary>
		ComponentName,
		///<summary>ComponentTypeID. </summary>
		ComponentTypeID,
		///<summary>MaxWarnLevel. </summary>
		MaxWarnLevel,
		///<summary>MinWarnLevel. </summary>
		MinWarnLevel,
		///<summary>Unit. </summary>
		Unit,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ConditionalSubPageLayoutObject.</summary>
	public enum ConditionalSubPageLayoutObjectFieldIndex
	{
		///<summary>Column. </summary>
		Column,
		///<summary>Condition. </summary>
		Condition,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>PageID. </summary>
		PageID,
		///<summary>Row. </summary>
		Row,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditScreening.</summary>
	public enum CreditScreeningFieldIndex
	{
		///<summary>BankconnectionDataID. </summary>
		BankconnectionDataID,
		///<summary>CreditScreeningID. </summary>
		CreditScreeningID,
		///<summary>ExecutionTime. </summary>
		ExecutionTime,
		///<summary>ReferenceID. </summary>
		ReferenceID,
		///<summary>ReturnCode. </summary>
		ReturnCode,
		///<summary>ScoreValue. </summary>
		ScoreValue,
		///<summary>TrafficLightColor. </summary>
		TrafficLightColor,
		///<summary>TtkzText. </summary>
		TtkzText,
		///<summary>UserID. </summary>
		UserID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DayType.</summary>
	public enum DayTypeFieldIndex
	{
		///<summary>Begin1. </summary>
		Begin1,
		///<summary>Begin2. </summary>
		Begin2,
		///<summary>Begin3. </summary>
		Begin3,
		///<summary>DayTypeID. </summary>
		DayTypeID,
		///<summary>Description. </summary>
		Description,
		///<summary>End1. </summary>
		End1,
		///<summary>End2. </summary>
		End2,
		///<summary>End3. </summary>
		End3,
		///<summary>Name. </summary>
		Name,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Debtor.</summary>
	public enum DebtorFieldIndex
	{
		///<summary>AccountBalance. </summary>
		AccountBalance,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>BalancingDebitor. </summary>
		BalancingDebitor,
		///<summary>Changestock. </summary>
		Changestock,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractorID. </summary>
		ContractorID,
		///<summary>CreditorAccountNumber. </summary>
		CreditorAccountNumber,
		///<summary>DebitBalance. </summary>
		DebitBalance,
		///<summary>DebtorAccountNumber. </summary>
		DebtorAccountNumber,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>DebtorNumber. </summary>
		DebtorNumber,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>DepotLastLogon. </summary>
		DepotLastLogon,
		///<summary>Description. </summary>
		Description,
		///<summary>ExitDate. </summary>
		ExitDate,
		///<summary>ExternalDebtorNumber. </summary>
		ExternalDebtorNumber,
		///<summary>ExternalNumberType. </summary>
		ExternalNumberType,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>IsTraining. </summary>
		IsTraining,
		///<summary>LastBalanceDate. </summary>
		LastBalanceDate,
		///<summary>LastName. </summary>
		LastName,
		///<summary>Lockernumber. </summary>
		Lockernumber,
		///<summary>MaintenanceLevel. </summary>
		MaintenanceLevel,
		///<summary>MaxDaysSinceLastClearance. </summary>
		MaxDaysSinceLastClearance,
		///<summary>MinAllowedAccountBalance. </summary>
		MinAllowedAccountBalance,
		///<summary>OrganisationCode. </summary>
		OrganisationCode,
		///<summary>Pin. </summary>
		Pin,
		///<summary>PreferredLanguage. </summary>
		PreferredLanguage,
		///<summary>Salutation. </summary>
		Salutation,
		///<summary>ServiceLevel. </summary>
		ServiceLevel,
		///<summary>StaffIdentifier. </summary>
		StaffIdentifier,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>VehicleLastLogon. </summary>
		VehicleLastLogon,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DebtorCard.</summary>
	public enum DebtorCardFieldIndex
	{
		///<summary>Blocked. </summary>
		Blocked,
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DateOfNoticeLost. </summary>
		DateOfNoticeLost,
		///<summary>DebtorCardID. </summary>
		DebtorCardID,
		///<summary>DebtorNumber. </summary>
		DebtorNumber,
		///<summary>IsTraining. </summary>
		IsTraining,
		///<summary>LastReadoutTime. </summary>
		LastReadoutTime,
		///<summary>PinHash. </summary>
		PinHash,
		///<summary>PrintedNumber. </summary>
		PrintedNumber,
		///<summary>ReplaceCardNumber. </summary>
		ReplaceCardNumber,
		///<summary>SerialNumber. </summary>
		SerialNumber,
		///<summary>ServiceLevel. </summary>
		ServiceLevel,
		///<summary>State. </summary>
		State,
		///<summary>TypeOfCardID. </summary>
		TypeOfCardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DebtorCardHistory.</summary>
	public enum DebtorCardHistoryFieldIndex
	{
		///<summary>DebtorCardHistoryID. </summary>
		DebtorCardHistoryID,
		///<summary>DebtorCardID. </summary>
		DebtorCardID,
		///<summary>MachineName. </summary>
		MachineName,
		///<summary>Message. </summary>
		Message,
		///<summary>MessageDateTime. </summary>
		MessageDateTime,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DefaultPin.</summary>
	public enum DefaultPinFieldIndex
	{
		///<summary>DefaultPin. </summary>
		DefaultPin,
		///<summary>DefaultPinID. </summary>
		DefaultPinID,
		///<summary>TypeOfCardID. </summary>
		TypeOfCardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Depot.</summary>
	public enum DepotFieldIndex
	{
		///<summary>AccountID. </summary>
		AccountID,
		///<summary>AccountID2. </summary>
		AccountID2,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>CashID. </summary>
		CashID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CustomerNumber. </summary>
		CustomerNumber,
		///<summary>DepotDescription. </summary>
		DepotDescription,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>DepotLongName. </summary>
		DepotLongName,
		///<summary>DepotName. </summary>
		DepotName,
		///<summary>DepotNumber. </summary>
		DepotNumber,
		///<summary>LastTimeAvailabe. </summary>
		LastTimeAvailabe,
		///<summary>NotificationAddress. </summary>
		NotificationAddress,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Device.</summary>
	public enum DeviceFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DeviceID. </summary>
		DeviceID,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>ExternalDeviceNo. </summary>
		ExternalDeviceNo,
		///<summary>InventoryNumber. </summary>
		InventoryNumber,
		///<summary>LastData. </summary>
		LastData,
		///<summary>LockState. </summary>
		LockState,
		///<summary>MountingPlate. </summary>
		MountingPlate,
		///<summary>Name. </summary>
		Name,
		///<summary>RequestedLockState. </summary>
		RequestedLockState,
		///<summary>State. </summary>
		State,
		///<summary>UnitID. </summary>
		UnitID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceBookingState.</summary>
	public enum DeviceBookingStateFieldIndex
	{
		///<summary>DeviceBookingNumber. </summary>
		DeviceBookingNumber,
		///<summary>DeviceBookingStateID. </summary>
		DeviceBookingStateID,
		///<summary>IsBooked. </summary>
		IsBooked,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceClass.</summary>
	public enum DeviceClassFieldIndex
	{
		///<summary>CreateLineDefDat. </summary>
		CreateLineDefDat,
		///<summary>CreateLs. </summary>
		CreateLs,
		///<summary>CreateParameterArchive. </summary>
		CreateParameterArchive,
		///<summary>CreateTariffArchive. </summary>
		CreateTariffArchive,
		///<summary>CryptoReleaseVisibility. </summary>
		CryptoReleaseVisibility,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>GneralAccount. </summary>
		GneralAccount,
		///<summary>ImageFile. </summary>
		ImageFile,
		///<summary>ImsApplicationName. </summary>
		ImsApplicationName,
		///<summary>Lockable. </summary>
		Lockable,
		///<summary>Name. </summary>
		Name,
		///<summary>ParameterReleaseVisibility. </summary>
		ParameterReleaseVisibility,
		///<summary>ReleaseName. </summary>
		ReleaseName,
		///<summary>ReleaseVisibility. </summary>
		ReleaseVisibility,
		///<summary>SaleDevice. </summary>
		SaleDevice,
		///<summary>ShortName. </summary>
		ShortName,
		///<summary>TmVisibility. </summary>
		TmVisibility,
		///<summary>TypeOfUnitID. </summary>
		TypeOfUnitID,
		///<summary>UnzipEnabled. </summary>
		UnzipEnabled,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DevicePaymentMethod.</summary>
	public enum DevicePaymentMethodFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>DisplayText. </summary>
		DisplayText,
		///<summary>EvendMask. </summary>
		EvendMask,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Direction.</summary>
	public enum DirectionFieldIndex
	{
		///<summary>Code. </summary>
		Code,
		///<summary>DirectionNo. </summary>
		DirectionNo,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningLevel.</summary>
	public enum DunningLevelFieldIndex
	{
		///<summary>CancelContract. </summary>
		CancelContract,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>DunningLevelID. </summary>
		DunningLevelID,
		///<summary>Fee. </summary>
		Fee,
		///<summary>Language. </summary>
		Language,
		///<summary>LockBilling. </summary>
		LockBilling,
		///<summary>LockCards. </summary>
		LockCards,
		///<summary>NextDunningLevel. </summary>
		NextDunningLevel,
		///<summary>NoDaysForPayment. </summary>
		NoDaysForPayment,
		///<summary>OrderNo. </summary>
		OrderNo,
		///<summary>PaymentMethodID. </summary>
		PaymentMethodID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Eticket.</summary>
	public enum EticketFieldIndex
	{
		///<summary>EtickDueTime. </summary>
		EtickDueTime,
		///<summary>EtickID. </summary>
		EtickID,
		///<summary>EtickKey. </summary>
		EtickKey,
		///<summary>EtickLayoutID. </summary>
		EtickLayoutID,
		///<summary>EtickName. </summary>
		EtickName,
		///<summary>EtickOffsetUnit. </summary>
		EtickOffsetUnit,
		///<summary>EtickOffsetValue. </summary>
		EtickOffsetValue,
		///<summary>EtickValidUnit. </summary>
		EtickValidUnit,
		///<summary>EtickValidValue. </summary>
		EtickValidValue,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExportInfo.</summary>
	public enum ExportInfoFieldIndex
	{
		///<summary>AllowTestExport. </summary>
		AllowTestExport,
		///<summary>ExportID. </summary>
		ExportID,
		///<summary>ExportName. </summary>
		ExportName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalCard.</summary>
	public enum ExternalCardFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardNumber. </summary>
		CardNumber,
		///<summary>Description. </summary>
		Description,
		///<summary>EffortID. </summary>
		EffortID,
		///<summary>PacketID. </summary>
		PacketID,
		///<summary>TariffId. </summary>
		TariffId,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalData.</summary>
	public enum ExternalDataFieldIndex
	{
		///<summary>CardOperator. </summary>
		CardOperator,
		///<summary>EndSequenceNumber. </summary>
		EndSequenceNumber,
		///<summary>ExtBrandNumber. </summary>
		ExtBrandNumber,
		///<summary>ExtCardType. </summary>
		ExtCardType,
		///<summary>ExtCommunityNumber. </summary>
		ExtCommunityNumber,
		///<summary>ExtDataCheckResult. </summary>
		ExtDataCheckResult,
		///<summary>ExtDataID. </summary>
		ExtDataID,
		///<summary>ExtDataState. </summary>
		ExtDataState,
		///<summary>ExtDataValue1. </summary>
		ExtDataValue1,
		///<summary>ExtEffortNumber. </summary>
		ExtEffortNumber,
		///<summary>ExtMediaType. </summary>
		ExtMediaType,
		///<summary>ExtPacketNumber. </summary>
		ExtPacketNumber,
		///<summary>Proxerror. </summary>
		Proxerror,
		///<summary>SamID. </summary>
		SamID,
		///<summary>SamPrintedNumber. </summary>
		SamPrintedNumber,
		///<summary>StartSequenceNumber. </summary>
		StartSequenceNumber,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalEffort.</summary>
	public enum ExternalEffortFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EffortID. </summary>
		EffortID,
		///<summary>EffortNumber. </summary>
		EffortNumber,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalPacket.</summary>
	public enum ExternalPacketFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>PacketID. </summary>
		PacketID,
		///<summary>PacketNumber. </summary>
		PacketNumber,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalPacketEffort.</summary>
	public enum ExternalPacketEffortFieldIndex
	{
		///<summary>CardTypeId. </summary>
		CardTypeId,
		///<summary>EffortID. </summary>
		EffortID,
		///<summary>FareStageID. </summary>
		FareStageID,
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>LineID. </summary>
		LineID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PacketEffortID. </summary>
		PacketEffortID,
		///<summary>PacketID. </summary>
		PacketID,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalType.</summary>
	public enum ExternalTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>TypeNumber. </summary>
		TypeNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionCategory.</summary>
	public enum FareEvasionCategoryFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>FareEvasionCategoryID. </summary>
		FareEvasionCategoryID,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionReason.</summary>
	public enum FareEvasionReasonFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>FareEvasionReasonID. </summary>
		FareEvasionReasonID,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionReasonToCategory.</summary>
	public enum FareEvasionReasonToCategoryFieldIndex
	{
		///<summary>FareEvasionCategoryID. </summary>
		FareEvasionCategoryID,
		///<summary>FareEvasionReasonID. </summary>
		FareEvasionReasonID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareMatrix.</summary>
	public enum FareMatrixFieldIndex
	{
		///<summary>Depth. </summary>
		Depth,
		///<summary>FareMatrixID. </summary>
		FareMatrixID,
		///<summary>FareStageListID. </summary>
		FareStageListID,
		///<summary>Formula. </summary>
		Formula,
		///<summary>MasterMatrixID. </summary>
		MasterMatrixID,
		///<summary>Name. </summary>
		Name,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>ScaleID. </summary>
		ScaleID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareMatrixEntry.</summary>
	public enum FareMatrixEntryFieldIndex
	{
		///<summary>AreaListKeyBack. </summary>
		AreaListKeyBack,
		///<summary>AreaListKeyForth. </summary>
		AreaListKeyForth,
		///<summary>AverageDistance. </summary>
		AverageDistance,
		///<summary>BoardingID. </summary>
		BoardingID,
		///<summary>Description. </summary>
		Description,
		///<summary>DescriptionReference. </summary>
		DescriptionReference,
		///<summary>DestinationID. </summary>
		DestinationID,
		///<summary>DirectionAttributeID. </summary>
		DirectionAttributeID,
		///<summary>Distance. </summary>
		Distance,
		///<summary>DistanceAttributeID. </summary>
		DistanceAttributeID,
		///<summary>Fare. </summary>
		Fare,
		///<summary>FareMatrixEntryID. </summary>
		FareMatrixEntryID,
		///<summary>FareMatrixID. </summary>
		FareMatrixID,
		///<summary>LineFilterAttributeID. </summary>
		LineFilterAttributeID,
		///<summary>Locked. </summary>
		Locked,
		///<summary>LongName. </summary>
		LongName,
		///<summary>Priority. </summary>
		Priority,
		///<summary>RealDistance. </summary>
		RealDistance,
		///<summary>RouteNameID. </summary>
		RouteNameID,
		///<summary>SalesFlag. </summary>
		SalesFlag,
		///<summary>ServiceAllocationID. </summary>
		ServiceAllocationID,
		///<summary>ShortName. </summary>
		ShortName,
		///<summary>TariffAttributeID. </summary>
		TariffAttributeID,
		///<summary>TicketGroupID. </summary>
		TicketGroupID,
		///<summary>UseDaysAttributeID. </summary>
		UseDaysAttributeID,
		///<summary>ViaID. </summary>
		ViaID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareStage.</summary>
	public enum FareStageFieldIndex
	{
		///<summary>Externalnumber. </summary>
		Externalnumber,
		///<summary>FareStageClass. </summary>
		FareStageClass,
		///<summary>FareStageID. </summary>
		FareStageID,
		///<summary>FareStageListID. </summary>
		FareStageListID,
		///<summary>FareStageTypeID. </summary>
		FareStageTypeID,
		///<summary>HierarchieLevel. </summary>
		HierarchieLevel,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>Numberoverride. </summary>
		Numberoverride,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>ParentID. </summary>
		ParentID,
		///<summary>Selectable. </summary>
		Selectable,
		///<summary>ShortName. </summary>
		ShortName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareStageAlias.</summary>
	public enum FareStageAliasFieldIndex
	{
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>Description. </summary>
		Description,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>FareStageAliasID. </summary>
		FareStageAliasID,
		///<summary>FareStageClass. </summary>
		FareStageClass,
		///<summary>FareStageID. </summary>
		FareStageID,
		///<summary>FareStageTypeID. </summary>
		FareStageTypeID,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareStageHierarchieLevel.</summary>
	public enum FareStageHierarchieLevelFieldIndex
	{
		///<summary>LevelID. </summary>
		LevelID,
		///<summary>LevelName. </summary>
		LevelName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareStageList.</summary>
	public enum FareStageListFieldIndex
	{
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>Description. </summary>
		Description,
		///<summary>FareStageListID. </summary>
		FareStageListID,
		///<summary>Name. </summary>
		Name,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareStageType.</summary>
	public enum FareStageTypeFieldIndex
	{
		///<summary>FarestageTypeID. </summary>
		FarestageTypeID,
		///<summary>FarestageTypeNumber. </summary>
		FarestageTypeNumber,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareTable.</summary>
	public enum FareTableFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>FareTableID. </summary>
		FareTableID,
		///<summary>Formula. </summary>
		Formula,
		///<summary>MasterFareTableID. </summary>
		MasterFareTableID,
		///<summary>MaxValue. </summary>
		MaxValue,
		///<summary>MinValue. </summary>
		MinValue,
		///<summary>Name. </summary>
		Name,
		///<summary>OwnerCLientID. </summary>
		OwnerCLientID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareTableEntry.</summary>
	public enum FareTableEntryFieldIndex
	{
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>Distance. </summary>
		Distance,
		///<summary>ExternalName. </summary>
		ExternalName,
		///<summary>Fare1. </summary>
		Fare1,
		///<summary>Fare2. </summary>
		Fare2,
		///<summary>FareTableEntryID. </summary>
		FareTableEntryID,
		///<summary>FareTableID. </summary>
		FareTableID,
		///<summary>MasterFareTableEntryID. </summary>
		MasterFareTableEntryID,
		///<summary>RulePeriodID. </summary>
		RulePeriodID,
		///<summary>ShortCode. </summary>
		ShortCode,
		///<summary>ValidityPeriod. </summary>
		ValidityPeriod,
		///<summary>ValidityText. </summary>
		ValidityText,
		///<summary>Vat. </summary>
		Vat,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FixedBitmapLayoutObject.</summary>
	public enum FixedBitmapLayoutObjectFieldIndex
	{
		///<summary>ClientAdaptable. </summary>
		ClientAdaptable,
		///<summary>Column. </summary>
		Column,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>LogoID. </summary>
		LogoID,
		///<summary>Number. </summary>
		Number,
		///<summary>Row. </summary>
		Row,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FixedTextLayoutObject.</summary>
	public enum FixedTextLayoutObjectFieldIndex
	{
		///<summary>ClientAdaptable. </summary>
		ClientAdaptable,
		///<summary>Column. </summary>
		Column,
		///<summary>FieldWidth. </summary>
		FieldWidth,
		///<summary>FontSize. </summary>
		FontSize,
		///<summary>Height. </summary>
		Height,
		///<summary>IsBold. </summary>
		IsBold,
		///<summary>IsDouble. </summary>
		IsDouble,
		///<summary>IsInverse. </summary>
		IsInverse,
		///<summary>IsUnderlined. </summary>
		IsUnderlined,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>Number. </summary>
		Number,
		///<summary>Row. </summary>
		Row,
		///<summary>Text. </summary>
		Text,
		///<summary>TextAlignment. </summary>
		TextAlignment,
		///<summary>Width. </summary>
		Width,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GuiDef.</summary>
	public enum GuiDefFieldIndex
	{
		///<summary>Active. </summary>
		Active,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>GuiDataXml. </summary>
		GuiDataXml,
		///<summary>GuiDataXsd. </summary>
		GuiDataXsd,
		///<summary>GuiDef. </summary>
		GuiDef,
		///<summary>GuiDefXml. </summary>
		GuiDefXml,
		///<summary>Name. </summary>
		Name,
		///<summary>PanelID. </summary>
		PanelID,
		///<summary>Sequence. </summary>
		Sequence,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KeyAttributeTransfrom.</summary>
	public enum KeyAttributeTransfromFieldIndex
	{
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>AttributeValueFromID. </summary>
		AttributeValueFromID,
		///<summary>AttributeValueToID. </summary>
		AttributeValueToID,
		///<summary>KeyAttributeTransfromId. </summary>
		KeyAttributeTransfromId,
		///<summary>KeyID. </summary>
		KeyID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KeySelectionMode.</summary>
	public enum KeySelectionModeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>KeySelectionModeID. </summary>
		KeySelectionModeID,
		///<summary>KeySelectionModeName. </summary>
		KeySelectionModeName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: KVVSubscription.</summary>
	public enum KVVSubscriptionFieldIndex
	{
		///<summary>BirthDate. </summary>
		BirthDate,
		///<summary>FareZoneFromDescription. </summary>
		FareZoneFromDescription,
		///<summary>FareZoneFromID. </summary>
		FareZoneFromID,
		///<summary>FareZoneID. </summary>
		FareZoneID,
		///<summary>FareZoneToDescription. </summary>
		FareZoneToDescription,
		///<summary>FareZoneToID. </summary>
		FareZoneToID,
		///<summary>IBANPattern. </summary>
		IBANPattern,
		///<summary>KVVSubscriptionID. </summary>
		KVVSubscriptionID,
		///<summary>OldFareZoneFromID. </summary>
		OldFareZoneFromID,
		///<summary>OldFareZoneToID. </summary>
		OldFareZoneToID,
		///<summary>ProductDescription. </summary>
		ProductDescription,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>SubscriptionReference. </summary>
		SubscriptionReference,
		///<summary>SubscriptionWasUpdated. </summary>
		SubscriptionWasUpdated,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Language.</summary>
	public enum LanguageFieldIndex
	{
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>LanguageName. </summary>
		LanguageName,
		///<summary>LanguageSymbol. </summary>
		LanguageSymbol,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Layout.</summary>
	public enum LayoutFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DevMask. </summary>
		DevMask,
		///<summary>Formid. </summary>
		Formid,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutKind. </summary>
		LayoutKind,
		///<summary>LayoutNumber. </summary>
		LayoutNumber,
		///<summary>Length. </summary>
		Length,
		///<summary>Name. </summary>
		Name,
		///<summary>OutDeviceID. </summary>
		OutDeviceID,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>Rotation. </summary>
		Rotation,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Line.</summary>
	public enum LineFieldIndex
	{
		///<summary>ClearingLine. </summary>
		ClearingLine,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>LineCode. </summary>
		LineCode,
		///<summary>LineCodeExtern. </summary>
		LineCodeExtern,
		///<summary>LineID. </summary>
		LineID,
		///<summary>LineName. </summary>
		LineName,
		///<summary>LineNo. </summary>
		LineNo,
		///<summary>NetID. </summary>
		NetID,
		///<summary>NetworkLine. </summary>
		NetworkLine,
		///<summary>PayRollRelevant. </summary>
		PayRollRelevant,
		///<summary>RaillingSchedule. </summary>
		RaillingSchedule,
		///<summary>SystemLine. </summary>
		SystemLine,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LineGroup.</summary>
	public enum LineGroupFieldIndex
	{
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>LineGroupName. </summary>
		LineGroupName,
		///<summary>LineGroupType. </summary>
		LineGroupType,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LineGroupToLineGroup.</summary>
	public enum LineGroupToLineGroupFieldIndex
	{
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>MasterLineGroupID. </summary>
		MasterLineGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LineToLineGroup.</summary>
	public enum LineToLineGroupFieldIndex
	{
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>LineNo. </summary>
		LineNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ListLayoutObject.</summary>
	public enum ListLayoutObjectFieldIndex
	{
		///<summary>Column. </summary>
		Column,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>LayoutTypeId. </summary>
		LayoutTypeId,
		///<summary>Name. </summary>
		Name,
		///<summary>PageID. </summary>
		PageID,
		///<summary>Row. </summary>
		Row,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ListLayoutType.</summary>
	public enum ListLayoutTypeFieldIndex
	{
		///<summary>ListLayoutID. </summary>
		ListLayoutID,
		///<summary>Name. </summary>
		Name,
		///<summary>NxName. </summary>
		NxName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Logo.</summary>
	public enum LogoFieldIndex
	{
		///<summary>Bitmap. </summary>
		Bitmap,
		///<summary>Description. </summary>
		Description,
		///<summary>LogoID. </summary>
		LogoID,
		///<summary>Name. </summary>
		Name,
		///<summary>OriginalImage. </summary>
		OriginalImage,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Net.</summary>
	public enum NetFieldIndex
	{
		///<summary>AutoUpdate. </summary>
		AutoUpdate,
		///<summary>BaseVersion. </summary>
		BaseVersion,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>CreationTime. </summary>
		CreationTime,
		///<summary>NetID. </summary>
		NetID,
		///<summary>NetworkVersion. </summary>
		NetworkVersion,
		///<summary>Note. </summary>
		Note,
		///<summary>TimeTableVersion. </summary>
		TimeTableVersion,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NumberRange.</summary>
	public enum NumberRangeFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Cycle. </summary>
		Cycle,
		///<summary>Depot. </summary>
		Depot,
		///<summary>Description. </summary>
		Description,
		///<summary>EndValue. </summary>
		EndValue,
		///<summary>Increment. </summary>
		Increment,
		///<summary>Name. </summary>
		Name,
		///<summary>NextValue. </summary>
		NextValue,
		///<summary>NumberRangeId. </summary>
		NumberRangeId,
		///<summary>Prefix. </summary>
		Prefix,
		///<summary>StartValue. </summary>
		StartValue,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Operator.</summary>
	public enum OperatorFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>OperatorID. </summary>
		OperatorID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OutputDevice.</summary>
	public enum OutputDeviceFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EvendMask. </summary>
		EvendMask,
		///<summary>Name. </summary>
		Name,
		///<summary>OutputDeviceID. </summary>
		OutputDeviceID,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageContentGroup.</summary>
	public enum PageContentGroupFieldIndex
	{
		///<summary>PageContentGroupID. </summary>
		PageContentGroupID,
		///<summary>PageContentGroupName. </summary>
		PageContentGroupName,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageContentGroupToContent.</summary>
	public enum PageContentGroupToContentFieldIndex
	{
		///<summary>PageContentGroupid. </summary>
		PageContentGroupid,
		///<summary>PageContentID. </summary>
		PageContentID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Panel.</summary>
	public enum PanelFieldIndex
	{
		///<summary>ApplyCardTicketFilter. </summary>
		ApplyCardTicketFilter,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>EvendNumber. </summary>
		EvendNumber,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>PanelID. </summary>
		PanelID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterAttributeValue.</summary>
	public enum ParameterAttributeValueFieldIndex
	{
		///<summary>AttributeValueID. </summary>
		AttributeValueID,
		///<summary>AttributeValuepParameterValueID. </summary>
		AttributeValuepParameterValueID,
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterFareStage.</summary>
	public enum ParameterFareStageFieldIndex
	{
		///<summary>FareStageID. </summary>
		FareStageID,
		///<summary>FareStageParameterValueID. </summary>
		FareStageParameterValueID,
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterTariff.</summary>
	public enum ParameterTariffFieldIndex
	{
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TariffParameterValueID. </summary>
		TariffParameterValueID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterTicket.</summary>
	public enum ParameterTicketFieldIndex
	{
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketParameterValueID. </summary>
		TicketParameterValueID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentDetail.</summary>
	public enum PaymentDetailFieldIndex
	{
		///<summary>AuthenticationCode. </summary>
		AuthenticationCode,
		///<summary>Expiry. </summary>
		Expiry,
		///<summary>Issuer. </summary>
		Issuer,
		///<summary>MaskedPan. </summary>
		MaskedPan,
		///<summary>PaymentDetailID. </summary>
		PaymentDetailID,
		///<summary>PaymentType. </summary>
		PaymentType,
		///<summary>ReferenceNumber. </summary>
		ReferenceNumber,
		///<summary>Resultcode. </summary>
		Resultcode,
		///<summary>SequenceNumber. </summary>
		SequenceNumber,
		///<summary>TerminalID. </summary>
		TerminalID,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>ValidationMode. </summary>
		ValidationMode,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentInterval.</summary>
	public enum PaymentIntervalFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>PaymentIntervalID. </summary>
		PaymentIntervalID,
		///<summary>PaymentIntervalName. </summary>
		PaymentIntervalName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PointOfSale.</summary>
	public enum PointOfSaleFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CommissionBasePercentage. </summary>
		CommissionBasePercentage,
		///<summary>CommissionDeduction. </summary>
		CommissionDeduction,
		///<summary>CommissionEndDate. </summary>
		CommissionEndDate,
		///<summary>CommissionFixed. </summary>
		CommissionFixed,
		///<summary>CommissionIntervalTypeID. </summary>
		CommissionIntervalTypeID,
		///<summary>CommissionMax. </summary>
		CommissionMax,
		///<summary>CommissionMin. </summary>
		CommissionMin,
		///<summary>CommissionStartDate. </summary>
		CommissionStartDate,
		///<summary>CommissionTargetRevenue. </summary>
		CommissionTargetRevenue,
		///<summary>CommissionTypeID. </summary>
		CommissionTypeID,
		///<summary>ContractEndDate. </summary>
		ContractEndDate,
		///<summary>ContractorID. </summary>
		ContractorID,
		///<summary>ContractStartDate. </summary>
		ContractStartDate,
		///<summary>CostCenter. </summary>
		CostCenter,
		///<summary>CountryID. </summary>
		CountryID,
		///<summary>CreditorID. </summary>
		CreditorID,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>Description. </summary>
		Description,
		///<summary>EmailAddress. </summary>
		EmailAddress,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>LastAdvancePaymentDate. </summary>
		LastAdvancePaymentDate,
		///<summary>Notes. </summary>
		Notes,
		///<summary>OpeningHours. </summary>
		OpeningHours,
		///<summary>OrganisationID. </summary>
		OrganisationID,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>PointOfSaleID. </summary>
		PointOfSaleID,
		///<summary>PointofSaleLocation. </summary>
		PointofSaleLocation,
		///<summary>PointOfSaleName. </summary>
		PointOfSaleName,
		///<summary>PointOfSaleNumber. </summary>
		PointOfSaleNumber,
		///<summary>PointOfSaleTypeID. </summary>
		PointOfSaleTypeID,
		///<summary>SalesTaxID. </summary>
		SalesTaxID,
		///<summary>Surname. </summary>
		Surname,
		///<summary>TitleID. </summary>
		TitleID,
		///<summary>VatTypeID. </summary>
		VatTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PredefinedKey.</summary>
	public enum PredefinedKeyFieldIndex
	{
		///<summary>Caption. </summary>
		Caption,
		///<summary>Description. </summary>
		Description,
		///<summary>KeyID. </summary>
		KeyID,
		///<summary>KeyNumber. </summary>
		KeyNumber,
		///<summary>PanelID. </summary>
		PanelID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TariffKey. </summary>
		TariffKey,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceType.</summary>
	public enum PriceTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>PriceTypeID. </summary>
		PriceTypeID,
		///<summary>PriceTypeName. </summary>
		PriceTypeName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PrimalKey.</summary>
	public enum PrimalKeyFieldIndex
	{
		///<summary>Branch. </summary>
		Branch,
		///<summary>KeyID. </summary>
		KeyID,
		///<summary>KeyLevel. </summary>
		KeyLevel,
		///<summary>PrimalKeyID. </summary>
		PrimalKeyID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PrintText.</summary>
	public enum PrintTextFieldIndex
	{
		///<summary>PrintText. </summary>
		PrintText,
		///<summary>PrintTextID. </summary>
		PrintTextID,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductOnCard.</summary>
	public enum ProductOnCardFieldIndex
	{
		///<summary>CardActionRequestID. </summary>
		CardActionRequestID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>Destination. </summary>
		Destination,
		///<summary>Expiry. </summary>
		Expiry,
		///<summary>Farestage. </summary>
		Farestage,
		///<summary>InstanceNumber. </summary>
		InstanceNumber,
		///<summary>IsCanceled. </summary>
		IsCanceled,
		///<summary>Origin. </summary>
		Origin,
		///<summary>ProductOnCardID. </summary>
		ProductOnCardID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TripCount. </summary>
		TripCount,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>Via. </summary>
		Via,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Protocol.</summary>
	public enum ProtocolFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DeviceNo. </summary>
		DeviceNo,
		///<summary>FunctionDate. </summary>
		FunctionDate,
		///<summary>FunctionID. </summary>
		FunctionID,
		///<summary>HostName. </summary>
		HostName,
		///<summary>MessageCount. </summary>
		MessageCount,
		///<summary>PayrollRelevant. </summary>
		PayrollRelevant,
		///<summary>Priority. </summary>
		Priority,
		///<summary>ProtocolID. </summary>
		ProtocolID,
		///<summary>ReadValue. </summary>
		ReadValue,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProtocolAction.</summary>
	public enum ProtocolActionFieldIndex
	{
		///<summary>ActionName. </summary>
		ActionName,
		///<summary>ActionText. </summary>
		ActionText,
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>PayrollRelevant. </summary>
		PayrollRelevant,
		///<summary>PriorityCorrection. </summary>
		PriorityCorrection,
		///<summary>ProtocolActionID. </summary>
		ProtocolActionID,
		///<summary>TimeToLive. </summary>
		TimeToLive,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProtocolFunction.</summary>
	public enum ProtocolFunctionFieldIndex
	{
		///<summary>Frequency. </summary>
		Frequency,
		///<summary>FunctionName. </summary>
		FunctionName,
		///<summary>GroupID. </summary>
		GroupID,
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>MaximumAge. </summary>
		MaximumAge,
		///<summary>MinimumPriority. </summary>
		MinimumPriority,
		///<summary>ProtocolFunctionID. </summary>
		ProtocolFunctionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProtocolFunctionGroup.</summary>
	public enum ProtocolFunctionGroupFieldIndex
	{
		///<summary>GroupName. </summary>
		GroupName,
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>ProtocolFunctionGroupID. </summary>
		ProtocolFunctionGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProtocolMessage.</summary>
	public enum ProtocolMessageFieldIndex
	{
		///<summary>ActionID. </summary>
		ActionID,
		///<summary>MessageDate. </summary>
		MessageDate,
		///<summary>MessageText. </summary>
		MessageText,
		///<summary>Priority. </summary>
		Priority,
		///<summary>ProtocolID. </summary>
		ProtocolID,
		///<summary>ProtocolMessageID. </summary>
		ProtocolMessageID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Qualification.</summary>
	public enum QualificationFieldIndex
	{
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>Description. </summary>
		Description,
		///<summary>QualificationID. </summary>
		QualificationID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Responsibility.</summary>
	public enum ResponsibilityFieldIndex
	{
		///<summary>ResponsibilityID. </summary>
		ResponsibilityID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ResponsibleClientID. </summary>
		ResponsibleClientID,
		///<summary>DataTypeID. </summary>
		DataTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevenueTransactionType.</summary>
	public enum RevenueTransactionTypeFieldIndex
	{
		///<summary>CreateAccountEntry. </summary>
		CreateAccountEntry,
		///<summary>PostingKeyAutomatic. </summary>
		PostingKeyAutomatic,
		///<summary>PostingKeyManual. </summary>
		PostingKeyManual,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>TypeName. </summary>
		TypeName,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RmPayment.</summary>
	public enum RmPaymentFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>PaymentID. </summary>
		PaymentID,
		///<summary>ReceiptNumber. </summary>
		ReceiptNumber,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Route.</summary>
	public enum RouteFieldIndex
	{
		///<summary>BoardingInfo. </summary>
		BoardingInfo,
		///<summary>DestinationInfo. </summary>
		DestinationInfo,
		///<summary>Exchangeable. </summary>
		Exchangeable,
		///<summary>PrintText1ID. </summary>
		PrintText1ID,
		///<summary>PrintText2ID. </summary>
		PrintText2ID,
		///<summary>PrintText3ID. </summary>
		PrintText3ID,
		///<summary>RouteID. </summary>
		RouteID,
		///<summary>RouteNameID. </summary>
		RouteNameID,
		///<summary>RouteNameObsolete. </summary>
		RouteNameObsolete,
		///<summary>RouteText. </summary>
		RouteText,
		///<summary>TariffAttributeID. </summary>
		TariffAttributeID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TextFixed. </summary>
		TextFixed,
		///<summary>TicketCategoryID. </summary>
		TicketCategoryID,
		///<summary>TicketGroupAttributeID. </summary>
		TicketGroupAttributeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RouteName.</summary>
	public enum RouteNameFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>RouteNameid. </summary>
		RouteNameid,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleCapping.</summary>
	public enum RuleCappingFieldIndex
	{
		///<summary>RuleCappingID. </summary>
		RuleCappingID,
		///<summary>RulePeriodID. </summary>
		RulePeriodID,
		///<summary>RuleTypeID. </summary>
		RuleTypeID,
		///<summary>TypeOfCardID. </summary>
		TypeOfCardID,
		///<summary>PotNumber. </summary>
		PotNumber,
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>Amount. </summary>
		Amount,
		///<summary>CappingName. </summary>
		CappingName,
		///<summary>Description. </summary>
		Description,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>VirtualTicketInternalNumber. </summary>
		VirtualTicketInternalNumber,
		///<summary>MaxChargeAmount. </summary>
		MaxChargeAmount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleCappingToFtEntry.</summary>
	public enum RuleCappingToFtEntryFieldIndex
	{
		///<summary>FareTableEntryID. </summary>
		FareTableEntryID,
		///<summary>RuleCappingID. </summary>
		RuleCappingID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleCappingToTicket.</summary>
	public enum RuleCappingToTicketFieldIndex
	{
		///<summary>RuleCappingID. </summary>
		RuleCappingID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RulePeriod.</summary>
	public enum RulePeriodFieldIndex
	{
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>DefaultFrom. </summary>
		DefaultFrom,
		///<summary>Description. </summary>
		Description,
		///<summary>ExtCalendarID. </summary>
		ExtCalendarID,
		///<summary>ExternalName1. </summary>
		ExternalName1,
		///<summary>ExtTimeUnit. </summary>
		ExtTimeUnit,
		///<summary>ExtTimeValue. </summary>
		ExtTimeValue,
		///<summary>Name. </summary>
		Name,
		///<summary>PeriodStart. </summary>
		PeriodStart,
		///<summary>PeriodUnit. </summary>
		PeriodUnit,
		///<summary>PeriodValue. </summary>
		PeriodValue,
		///<summary>Readonly. </summary>
		Readonly,
		///<summary>RulePeriodID. </summary>
		RulePeriodID,
		///<summary>RulePeriodNumber. </summary>
		RulePeriodNumber,
		///<summary>Selectable. </summary>
		Selectable,
		///<summary>SellableFrom. </summary>
		SellableFrom,
		///<summary>SellableUntil. </summary>
		SellableUntil,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>ValidationType. </summary>
		ValidationType,
		///<summary>ValidityFromFaretable. </summary>
		ValidityFromFaretable,
		///<summary>ValidTimeUnit. </summary>
		ValidTimeUnit,
		///<summary>ValidTimeValue. </summary>
		ValidTimeValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleType.</summary>
	public enum RuleTypeFieldIndex
	{
		///<summary>RuleTypeID. </summary>
		RuleTypeID,
		///<summary>RuleTypeName. </summary>
		RuleTypeName,
		///<summary>Description. </summary>
		Description,
		///<summary>RuleTypeGroupID. </summary>
		RuleTypeGroupID,
		///<summary>RuleTypeNumber. </summary>
		RuleTypeNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Salutation.</summary>
	public enum SalutationFieldIndex
	{
		///<summary>CodeNo. </summary>
		CodeNo,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Language. </summary>
		Language,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServiceAllocation.</summary>
	public enum ServiceAllocationFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>ServiceAllocationID. </summary>
		ServiceAllocationID,
		///<summary>TariffId. </summary>
		TariffId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServiceIdToCard.</summary>
	public enum ServiceIdToCardFieldIndex
	{
		///<summary>CardType. </summary>
		CardType,
		///<summary>Description. </summary>
		Description,
		///<summary>ID. </summary>
		ID,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServicePercentage.</summary>
	public enum ServicePercentageFieldIndex
	{
		///<summary>Percentage. </summary>
		Percentage,
		///<summary>ServiceAllocationID. </summary>
		ServiceAllocationID,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>ServiceName. </summary>
		ServiceName,
		///<summary>ServicePercentageID. </summary>
		ServicePercentageID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Shift.</summary>
	public enum ShiftFieldIndex
	{
		///<summary>BalanceNo. </summary>
		BalanceNo,
		///<summary>BankCardRevenue. </summary>
		BankCardRevenue,
		///<summary>CancellationAmount. </summary>
		CancellationAmount,
		///<summary>CancellationCount. </summary>
		CancellationCount,
		///<summary>CardNo. </summary>
		CardNo,
		///<summary>CashContentBegin. </summary>
		CashContentBegin,
		///<summary>CashContentEnd. </summary>
		CashContentEnd,
		///<summary>CashReceipt. </summary>
		CashReceipt,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CourseNo. </summary>
		CourseNo,
		///<summary>CustomerCardRevenue. </summary>
		CustomerCardRevenue,
		///<summary>DebitAccount. </summary>
		DebitAccount,
		///<summary>DebtorNo. </summary>
		DebtorNo,
		///<summary>DepositNumber. </summary>
		DepositNumber,
		///<summary>DeviceNo. </summary>
		DeviceNo,
		///<summary>DeviceType. </summary>
		DeviceType,
		///<summary>ExportDate. </summary>
		ExportDate,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>FirstTicketNo. </summary>
		FirstTicketNo,
		///<summary>HashValue. </summary>
		HashValue,
		///<summary>ImportDate. </summary>
		ImportDate,
		///<summary>IsBroken. </summary>
		IsBroken,
		///<summary>IsDst. </summary>
		IsDst,
		///<summary>LastTicketNo. </summary>
		LastTicketNo,
		///<summary>LineNo. </summary>
		LineNo,
		///<summary>LocationNo. </summary>
		LocationNo,
		///<summary>LogOffDebtor. </summary>
		LogOffDebtor,
		///<summary>MountingPlateNo. </summary>
		MountingPlateNo,
		///<summary>PosPaymentID. </summary>
		PosPaymentID,
		///<summary>PriceReduction. </summary>
		PriceReduction,
		///<summary>RouteNo. </summary>
		RouteNo,
		///<summary>SalesVolume. </summary>
		SalesVolume,
		///<summary>SettlementID. </summary>
		SettlementID,
		///<summary>ShiftBegin. </summary>
		ShiftBegin,
		///<summary>ShiftCounter. </summary>
		ShiftCounter,
		///<summary>ShiftEnd. </summary>
		ShiftEnd,
		///<summary>ShiftID. </summary>
		ShiftID,
		///<summary>ShiftNumber. </summary>
		ShiftNumber,
		///<summary>ShiftStateID. </summary>
		ShiftStateID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ShiftInventory.</summary>
	public enum ShiftInventoryFieldIndex
	{
		///<summary>BankCardRevenue. </summary>
		BankCardRevenue,
		///<summary>CardNo. </summary>
		CardNo,
		///<summary>CashReceipt. </summary>
		CashReceipt,
		///<summary>CustomerCardRevenue. </summary>
		CustomerCardRevenue,
		///<summary>DebtorNo. </summary>
		DebtorNo,
		///<summary>DeviceClass. </summary>
		DeviceClass,
		///<summary>DeviceNo. </summary>
		DeviceNo,
		///<summary>FirstSequentialNo. </summary>
		FirstSequentialNo,
		///<summary>HashValue. </summary>
		HashValue,
		///<summary>IsDst. </summary>
		IsDst,
		///<summary>IsTraining. </summary>
		IsTraining,
		///<summary>LastSequentialNo. </summary>
		LastSequentialNo,
		///<summary>SalesVolume. </summary>
		SalesVolume,
		///<summary>ShiftBegin. </summary>
		ShiftBegin,
		///<summary>ShiftEnd. </summary>
		ShiftEnd,
		///<summary>Shiftid. </summary>
		Shiftid,
		///<summary>VehicleNo. </summary>
		VehicleNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ShiftState.</summary>
	public enum ShiftStateFieldIndex
	{
		///<summary>Book. </summary>
		Book,
		///<summary>ShiftStateID. </summary>
		ShiftStateID,
		///<summary>StateName. </summary>
		StateName,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ShortDistance.</summary>
	public enum ShortDistanceFieldIndex
	{
		///<summary>Boarding. </summary>
		Boarding,
		///<summary>Destination. </summary>
		Destination,
		///<summary>ShortDistanceID. </summary>
		ShortDistanceID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SpecialReceipt.</summary>
	public enum SpecialReceiptFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>EvendFtName. </summary>
		EvendFtName,
		///<summary>EvendFtNumber. </summary>
		EvendFtNumber,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>Name. </summary>
		Name,
		///<summary>SpecialReceiptID. </summary>
		SpecialReceiptID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Stop.</summary>
	public enum StopFieldIndex
	{
		///<summary>AdministrationDistrictCode. </summary>
		AdministrationDistrictCode,
		///<summary>AdministrationDistrictName. </summary>
		AdministrationDistrictName,
		///<summary>AdministrationDistrictNo. </summary>
		AdministrationDistrictNo,
		///<summary>CityDistrictName. </summary>
		CityDistrictName,
		///<summary>CityDistrictNo. </summary>
		CityDistrictNo,
		///<summary>CityName. </summary>
		CityName,
		///<summary>CityNo. </summary>
		CityNo,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CoordinateID. </summary>
		CoordinateID,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>CountryName. </summary>
		CountryName,
		///<summary>CountryNo. </summary>
		CountryNo,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>FederalStateName. </summary>
		FederalStateName,
		///<summary>FederalStateNo. </summary>
		FederalStateNo,
		///<summary>Location. </summary>
		Location,
		///<summary>NetId. </summary>
		NetId,
		///<summary>RegionCode. </summary>
		RegionCode,
		///<summary>RegionName. </summary>
		RegionName,
		///<summary>RegionNo. </summary>
		RegionNo,
		///<summary>StopCode. </summary>
		StopCode,
		///<summary>StopID. </summary>
		StopID,
		///<summary>StopLongName. </summary>
		StopLongName,
		///<summary>StopName. </summary>
		StopName,
		///<summary>StopNameExternal. </summary>
		StopNameExternal,
		///<summary>StopNo. </summary>
		StopNo,
		///<summary>StopNoExtern. </summary>
		StopNoExtern,
		///<summary>TariffZoneNo1. </summary>
		TariffZoneNo1,
		///<summary>TariffZoneNo2. </summary>
		TariffZoneNo2,
		///<summary>TariffZoneNo3. </summary>
		TariffZoneNo3,
		///<summary>TariffZoneNo4. </summary>
		TariffZoneNo4,
		///<summary>TicketPrintName. </summary>
		TicketPrintName,
		///<summary>TicketPrintShortName. </summary>
		TicketPrintShortName,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemField.</summary>
	public enum SystemFieldFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>FormatString. </summary>
		FormatString,
		///<summary>LayoutObjectWidth. </summary>
		LayoutObjectWidth,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>NxName. </summary>
		NxName,
		///<summary>SampleText. </summary>
		SampleText,
		///<summary>SampleTextFunction. </summary>
		SampleTextFunction,
		///<summary>SystemFieldID. </summary>
		SystemFieldID,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemFieldBarcodeLayoutObject.</summary>
	public enum SystemFieldBarcodeLayoutObjectFieldIndex
	{
		///<summary>Column. </summary>
		Column,
		///<summary>FieldWidth. </summary>
		FieldWidth,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>Row. </summary>
		Row,
		///<summary>SystemFieldID. </summary>
		SystemFieldID,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemFieldDynamicGraphicLayoutObject.</summary>
	public enum SystemFieldDynamicGraphicLayoutObjectFieldIndex
	{
		///<summary>Col_. </summary>
		Col_,
		///<summary>Height. </summary>
		Height,
		///<summary>Isinverse. </summary>
		Isinverse,
		///<summary>Layoutid. </summary>
		Layoutid,
		///<summary>Layoutobjectid. </summary>
		Layoutobjectid,
		///<summary>Row_. </summary>
		Row_,
		///<summary>Systemfieldid. </summary>
		Systemfieldid,
		///<summary>Width. </summary>
		Width,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemFieldTextLayoutObject.</summary>
	public enum SystemFieldTextLayoutObjectFieldIndex
	{
		///<summary>Column. </summary>
		Column,
		///<summary>FieldWidth. </summary>
		FieldWidth,
		///<summary>FontSize. </summary>
		FontSize,
		///<summary>Height. </summary>
		Height,
		///<summary>IsBold. </summary>
		IsBold,
		///<summary>IsDouble. </summary>
		IsDouble,
		///<summary>IsInverse. </summary>
		IsInverse,
		///<summary>IsUnderlined. </summary>
		IsUnderlined,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>MaxLength. </summary>
		MaxLength,
		///<summary>MinLength. </summary>
		MinLength,
		///<summary>Row. </summary>
		Row,
		///<summary>SystemFieldID. </summary>
		SystemFieldID,
		///<summary>TextAlignment. </summary>
		TextAlignment,
		///<summary>Width. </summary>
		Width,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SystemText.</summary>
	public enum SystemTextFieldIndex
	{
		///<summary>Originaltext. </summary>
		Originaltext,
		///<summary>SystemTextID. </summary>
		SystemTextID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>Text. </summary>
		Text,
		///<summary>TextKey. </summary>
		TextKey,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Tariff.</summary>
	public enum TariffFieldIndex
	{
		///<summary>BaseTariffID. </summary>
		BaseTariffID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>ExternalVersion. </summary>
		ExternalVersion,
		///<summary>MatrixTariffID. </summary>
		MatrixTariffID,
		///<summary>NetID. </summary>
		NetID,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>ReleaseCounter. </summary>
		ReleaseCounter,
		///<summary>State. </summary>
		State,
		///<summary>SubReleaseCounter. </summary>
		SubReleaseCounter,
		///<summary>TarifID. </summary>
		TarifID,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidFromTime. </summary>
		ValidFromTime,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TariffParameter.</summary>
	public enum TariffParameterFieldIndex
	{
		///<summary>DotNetType. </summary>
		DotNetType,
		///<summary>NumberOnDevice. </summary>
		NumberOnDevice,
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterName. </summary>
		ParameterName,
		///<summary>ParameterObjectType. </summary>
		ParameterObjectType,
		///<summary>ParemeterDescription. </summary>
		ParemeterDescription,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TariffRelease.</summary>
	public enum TariffReleaseFieldIndex
	{
		///<summary>ReleaseID. </summary>
		ReleaseID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>ReleaseType. </summary>
		ReleaseType,
		///<summary>ReleaseDateTime. </summary>
		ReleaseDateTime,
		///<summary>Description. </summary>
		Description,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TemporalType.</summary>
	public enum TemporalTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>NxID. </summary>
		NxID,
		///<summary>TemporalTypeID. </summary>
		TemporalTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Ticket.</summary>
	public enum TicketFieldIndex
	{
		///<summary>AttributeValueListID. </summary>
		AttributeValueListID,
		///<summary>Barcode. </summary>
		Barcode,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>BrType10ID. </summary>
		BrType10ID,
		///<summary>BrType1ID. </summary>
		BrType1ID,
		///<summary>BrType2ID. </summary>
		BrType2ID,
		///<summary>BrType9ID. </summary>
		BrType9ID,
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>CancellationLayoutID. </summary>
		CancellationLayoutID,
		///<summary>CancellationQuantity. </summary>
		CancellationQuantity,
		///<summary>CategoryID. </summary>
		CategoryID,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>Discount. </summary>
		Discount,
		///<summary>EtickCancellationQuantity. </summary>
		EtickCancellationQuantity,
		///<summary>EtickEmission. </summary>
		EtickEmission,
		///<summary>EtickID. </summary>
		EtickID,
		///<summary>ExternalName. </summary>
		ExternalName,
		///<summary>ExternalName2. </summary>
		ExternalName2,
		///<summary>ExternalName3. </summary>
		ExternalName3,
		///<summary>ExternalName4. </summary>
		ExternalName4,
		///<summary>ExternalName5. </summary>
		ExternalName5,
		///<summary>ExternalName6. </summary>
		ExternalName6,
		///<summary>ExternalName7. </summary>
		ExternalName7,
		///<summary>ExternalName8. </summary>
		ExternalName8,
		///<summary>ExternalName9. </summary>
		ExternalName9,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>FareEvasionCategoryID. </summary>
		FareEvasionCategoryID,
		///<summary>FareStageListID. </summary>
		FareStageListID,
		///<summary>FareTable2ID. </summary>
		FareTable2ID,
		///<summary>FareTableID. </summary>
		FareTableID,
		///<summary>FormularName. </summary>
		FormularName,
		///<summary>GroupLayoutID. </summary>
		GroupLayoutID,
		///<summary>InfoText1. </summary>
		InfoText1,
		///<summary>InfoText2. </summary>
		InfoText2,
		///<summary>InternalNumber. </summary>
		InternalNumber,
		///<summary>IsOfflineAvailable. </summary>
		IsOfflineAvailable,
		///<summary>IsSupplement. </summary>
		IsSupplement,
		///<summary>Key1. </summary>
		Key1,
		///<summary>Key2. </summary>
		Key2,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>MatrixID. </summary>
		MatrixID,
		///<summary>MaxFactor. </summary>
		MaxFactor,
		///<summary>MinFactor. </summary>
		MinFactor,
		///<summary>Name. </summary>
		Name,
		///<summary>NumberOfPrintouts. </summary>
		NumberOfPrintouts,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>PageContentGroupID. </summary>
		PageContentGroupID,
		///<summary>Price. </summary>
		Price,
		///<summary>Price2. </summary>
		Price2,
		///<summary>PriceTypeID. </summary>
		PriceTypeID,
		///<summary>PrintLayoutID. </summary>
		PrintLayoutID,
		///<summary>PrintText. </summary>
		PrintText,
		///<summary>ReceiptLayoutID. </summary>
		ReceiptLayoutID,
		///<summary>RulePeriodID. </summary>
		RulePeriodID,
		///<summary>SelectionMode. </summary>
		SelectionMode,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>ShortCode. </summary>
		ShortCode,
		///<summary>TarifID. </summary>
		TarifID,
		///<summary>TemporalTypeID. </summary>
		TemporalTypeID,
		///<summary>TicketCancellationTypeID. </summary>
		TicketCancellationTypeID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketPropertyID. </summary>
		TicketPropertyID,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		///<summary>TripFrequency. </summary>
		TripFrequency,
		///<summary>ValuePassCredit. </summary>
		ValuePassCredit,
		///<summary>Vat. </summary>
		Vat,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketCancellationType.</summary>
	public enum TicketCancellationTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>NxID. </summary>
		NxID,
		///<summary>TicketCancellationTypeID. </summary>
		TicketCancellationTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketCategory.</summary>
	public enum TicketCategoryFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>TicketCategoryID. </summary>
		TicketCategoryID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketDayType.</summary>
	public enum TicketDayTypeFieldIndex
	{
		///<summary>DayTypeID. </summary>
		DayTypeID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketDeviceClass.</summary>
	public enum TicketDeviceClassFieldIndex
	{
		///<summary>Active. </summary>
		Active,
		///<summary>CalendarID. </summary>
		CalendarID,
		///<summary>CancellationLayoutID. </summary>
		CancellationLayoutID,
		///<summary>CancellationQuantity. </summary>
		CancellationQuantity,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>EtickCancellationQuantity. </summary>
		EtickCancellationQuantity,
		///<summary>EtickEmission. </summary>
		EtickEmission,
		///<summary>EtickID. </summary>
		EtickID,
		///<summary>GroupLayoutID. </summary>
		GroupLayoutID,
		///<summary>Key1. </summary>
		Key1,
		///<summary>Key2. </summary>
		Key2,
		///<summary>MatrixID. </summary>
		MatrixID,
		///<summary>Name. </summary>
		Name,
		///<summary>Price. </summary>
		Price,
		///<summary>PrintLayoutID. </summary>
		PrintLayoutID,
		///<summary>ReceiptLayoutID. </summary>
		ReceiptLayoutID,
		///<summary>RulePeriodID. </summary>
		RulePeriodID,
		///<summary>SelectionMode. </summary>
		SelectionMode,
		///<summary>TicketDeviceClassID. </summary>
		TicketDeviceClassID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketDeviceClassOutputDevice.</summary>
	public enum TicketDeviceClassOutputDeviceFieldIndex
	{
		///<summary>OutputDeviceID. </summary>
		OutputDeviceID,
		///<summary>TicketDeviceClassID. </summary>
		TicketDeviceClassID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketDeviceClassPaymentMethod.</summary>
	public enum TicketDeviceClassPaymentMethodFieldIndex
	{
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>TicketDeviceClassID. </summary>
		TicketDeviceClassID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketDevicePaymentMethod.</summary>
	public enum TicketDevicePaymentMethodFieldIndex
	{
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketGroup.</summary>
	public enum TicketGroupFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketGroupID. </summary>
		TicketGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketOrganization.</summary>
	public enum TicketOrganizationFieldIndex
	{
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketOutputdevice.</summary>
	public enum TicketOutputdeviceFieldIndex
	{
		///<summary>OutputDeviceID. </summary>
		OutputDeviceID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketPaymentInterval.</summary>
	public enum TicketPaymentIntervalFieldIndex
	{
		///<summary>PaymentIntervalID. </summary>
		PaymentIntervalID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketPhysicalCardType.</summary>
	public enum TicketPhysicalCardTypeFieldIndex
	{
		///<summary>PhysicalCardTypeID. </summary>
		PhysicalCardTypeID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketSelectionMode.</summary>
	public enum TicketSelectionModeFieldIndex
	{
		///<summary>SelectionModeName. </summary>
		SelectionModeName,
		///<summary>SelectionModeNumber. </summary>
		SelectionModeNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketServicesPermitted.</summary>
	public enum TicketServicesPermittedFieldIndex
	{
		///<summary>LineID. </summary>
		LineID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketToGroup.</summary>
	public enum TicketToGroupFieldIndex
	{
		///<summary>TicketGroupID. </summary>
		TicketGroupID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketType.</summary>
	public enum TicketTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>EvendBalancing. </summary>
		EvendBalancing,
		///<summary>EvendPayMask. </summary>
		EvendPayMask,
		///<summary>EvendTypeID. </summary>
		EvendTypeID,
		///<summary>Literal. </summary>
		Literal,
		///<summary>Name. </summary>
		Name,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		///<summary>TransactionTypeID. </summary>
		TransactionTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketVendingClient.</summary>
	public enum TicketVendingClientFieldIndex
	{
		///<summary>Clientid. </summary>
		Clientid,
		///<summary>Ticketid. </summary>
		Ticketid,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Transaction.</summary>
	public enum TransactionFieldIndex
	{
		///<summary>AccountEntryID. </summary>
		AccountEntryID,
		///<summary>AllocatedAmount. </summary>
		AllocatedAmount,
		///<summary>Barcode. </summary>
		Barcode,
		///<summary>BestPricerunID. </summary>
		BestPricerunID,
		///<summary>CancellationID. </summary>
		CancellationID,
		///<summary>CappingReason. </summary>
		CappingReason,
		///<summary>CardBalance. </summary>
		CardBalance,
		///<summary>CardChargeTan. </summary>
		CardChargeTan,
		///<summary>CardCredit. </summary>
		CardCredit,
		///<summary>Carddebit. </summary>
		Carddebit,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardIssuerID. </summary>
		CardIssuerID,
		///<summary>CardRefTransactionNo. </summary>
		CardRefTransactionNo,
		///<summary>CardTransactionNo. </summary>
		CardTransactionNo,
		///<summary>ClearingID. </summary>
		ClearingID,
		///<summary>ClearingState. </summary>
		ClearingState,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CourseNo. </summary>
		CourseNo,
		///<summary>Deduction. </summary>
		Deduction,
		///<summary>DeviceBookingState. </summary>
		DeviceBookingState,
		///<summary>DeviceNo. </summary>
		DeviceNo,
		///<summary>DevicePaymentMethod. </summary>
		DevicePaymentMethod,
		///<summary>Direction. </summary>
		Direction,
		///<summary>Distance. </summary>
		Distance,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ExtTikName. </summary>
		ExtTikName,
		///<summary>Factor. </summary>
		Factor,
		///<summary>FareMatrixEntryID. </summary>
		FareMatrixEntryID,
		///<summary>FarePaymentTransactionID. </summary>
		FarePaymentTransactionID,
		///<summary>FareStage. </summary>
		FareStage,
		///<summary>FromStopCodeExtern. </summary>
		FromStopCodeExtern,
		///<summary>FromStopNo. </summary>
		FromStopNo,
		///<summary>FromTariffZone. </summary>
		FromTariffZone,
		///<summary>ImportDateTime. </summary>
		ImportDateTime,
		///<summary>JourneyLegCounter. </summary>
		JourneyLegCounter,
		///<summary>Line. </summary>
		Line,
		///<summary>LineName. </summary>
		LineName,
		///<summary>MaxDistance. </summary>
		MaxDistance,
		///<summary>NoAdults. </summary>
		NoAdults,
		///<summary>NoChildren. </summary>
		NoChildren,
		///<summary>NoUser0. </summary>
		NoUser0,
		///<summary>NoUser1. </summary>
		NoUser1,
		///<summary>NoUser2. </summary>
		NoUser2,
		///<summary>NoUser3. </summary>
		NoUser3,
		///<summary>NoUser4. </summary>
		NoUser4,
		///<summary>OutputDevice. </summary>
		OutputDevice,
		///<summary>PayCardNo. </summary>
		PayCardNo,
		///<summary>PayrollRelevant. </summary>
		PayrollRelevant,
		///<summary>Price. </summary>
		Price,
		///<summary>PriceCorrection. </summary>
		PriceCorrection,
		///<summary>PriceLevel. </summary>
		PriceLevel,
		///<summary>RemainingRides. </summary>
		RemainingRides,
		///<summary>RevokeID. </summary>
		RevokeID,
		///<summary>RouteNo. </summary>
		RouteNo,
		///<summary>SalesChannel. </summary>
		SalesChannel,
		///<summary>SalesPostProcessingID. </summary>
		SalesPostProcessingID,
		///<summary>SalesTransactionID. </summary>
		SalesTransactionID,
		///<summary>ServiceID. </summary>
		ServiceID,
		///<summary>ShiftID. </summary>
		ShiftID,
		///<summary>ShoppingCart. </summary>
		ShoppingCart,
		///<summary>State. </summary>
		State,
		///<summary>StopFrom. </summary>
		StopFrom,
		///<summary>StopTo. </summary>
		StopTo,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TariffNetworkNo. </summary>
		TariffNetworkNo,
		///<summary>TicketInstanceID. </summary>
		TicketInstanceID,
		///<summary>TikNo. </summary>
		TikNo,
		///<summary>ToTariffZone. </summary>
		ToTariffZone,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TripDateTime. </summary>
		TripDateTime,
		///<summary>TripSerialNo. </summary>
		TripSerialNo,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		///<summary>ValueOfRide. </summary>
		ValueOfRide,
		///<summary>VatRatePerMille. </summary>
		VatRatePerMille,
		///<summary>ViaNo. </summary>
		ViaNo,
		///<summary>ViaTariffZone. </summary>
		ViaTariffZone,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionAdditional.</summary>
	public enum TransactionAdditionalFieldIndex
	{
		///<summary>ClearingClassification. </summary>
		ClearingClassification,
		///<summary>CorrectedBy. </summary>
		CorrectedBy,
		///<summary>CorrectionDate. </summary>
		CorrectionDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Originalshiftstate. </summary>
		Originalshiftstate,
		///<summary>ResponsibleClientID. </summary>
		ResponsibleClientID,
		///<summary>Shiftid. </summary>
		Shiftid,
		///<summary>TransactionAdditionalID. </summary>
		TransactionAdditionalID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TypeID. </summary>
		TypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionBackup.</summary>
	public enum TransactionBackupFieldIndex
	{
		///<summary>Allocatedamount. </summary>
		Allocatedamount,
		///<summary>Clearingid. </summary>
		Clearingid,
		///<summary>Clearingstate. </summary>
		Clearingstate,
		///<summary>Id. </summary>
		Id,
		///<summary>Recalcclearingid. </summary>
		Recalcclearingid,
		///<summary>Transactionid. </summary>
		Transactionid,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionExtension.</summary>
	public enum TransactionExtensionFieldIndex
	{
		///<summary>Code. </summary>
		Code,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>Note. </summary>
		Note,
		///<summary>RouteCode. </summary>
		RouteCode,
		///<summary>TicketReference. </summary>
		TicketReference,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>Tripcode. </summary>
		Tripcode,
		///<summary>Trippurpose. </summary>
		Trippurpose,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Translation.</summary>
	public enum TranslationFieldIndex
	{
		///<summary>BobjectID. </summary>
		BobjectID,
		///<summary>ClassLabel. </summary>
		ClassLabel,
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>PropertyLabel. </summary>
		PropertyLabel,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>Text. </summary>
		Text,
		///<summary>TranslationID. </summary>
		TranslationID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TypeOfCard.</summary>
	public enum TypeOfCardFieldIndex
	{
		///<summary>CodeNo. </summary>
		CodeNo,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>FormularID. </summary>
		FormularID,
		///<summary>Language. </summary>
		Language,
		///<summary>Literal. </summary>
		Literal,
		///<summary>MaxCappingPotCount. </summary>
		MaxCappingPotCount,
		///<summary>MocaFileSystem. </summary>
		MocaFileSystem,
		///<summary>TypeOfCardID. </summary>
		TypeOfCardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TypeOfUnit.</summary>
	public enum TypeOfUnitFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>TypeOfUnitID. </summary>
		TypeOfUnitID,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Unit.</summary>
	public enum UnitFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>Description. </summary>
		Description,
		///<summary>EquipmentConfigurationID. </summary>
		EquipmentConfigurationID,
		///<summary>GarageCode. </summary>
		GarageCode,
		///<summary>IsAutoGenerated. </summary>
		IsAutoGenerated,
		///<summary>LastGprsConnection. </summary>
		LastGprsConnection,
		///<summary>LastWlanConnection. </summary>
		LastWlanConnection,
		///<summary>License. </summary>
		License,
		///<summary>NotificationAddress. </summary>
		NotificationAddress,
		///<summary>TypeOfUnitID. </summary>
		TypeOfUnitID,
		///<summary>UnitID. </summary>
		UnitID,
		///<summary>UnitName. </summary>
		UnitName,
		///<summary>UnitNumber. </summary>
		UnitNumber,
		///<summary>UnitState. </summary>
		UnitState,
		///<summary>VisibleUnitNumber. </summary>
		VisibleUnitNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserConfig.</summary>
	public enum UserConfigFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ConfigID. </summary>
		ConfigID,
		///<summary>ConfigValue. </summary>
		ConfigValue,
		///<summary>IsGlobal. </summary>
		IsGlobal,
		///<summary>Remark. </summary>
		Remark,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserGroup.</summary>
	public enum UserGroupFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>UserGroupDescription. </summary>
		UserGroupDescription,
		///<summary>UserGroupID. </summary>
		UserGroupID,
		///<summary>UserGroupName. </summary>
		UserGroupName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserGroupRight.</summary>
	public enum UserGroupRightFieldIndex
	{
		///<summary>LevelOfRight. </summary>
		LevelOfRight,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>UserGroupID. </summary>
		UserGroupID,
		///<summary>UserGroupRightID. </summary>
		UserGroupRightID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserIsMember.</summary>
	public enum UserIsMemberFieldIndex
	{
		///<summary>UserGroupID. </summary>
		UserGroupID,
		///<summary>UserID. </summary>
		UserID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserKey.</summary>
	public enum UserKeyFieldIndex
	{
		///<summary>Caption. </summary>
		Caption,
		///<summary>Description. </summary>
		Description,
		///<summary>DestinationPanelID. </summary>
		DestinationPanelID,
		///<summary>KeyID. </summary>
		KeyID,
		///<summary>KeyNumber. </summary>
		KeyNumber,
		///<summary>KeyStyleID. </summary>
		KeyStyleID,
		///<summary>PanelID. </summary>
		PanelID,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserLanguage.</summary>
	public enum UserLanguageFieldIndex
	{
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>LanguageName. </summary>
		LanguageName,
		///<summary>LocalInfo. </summary>
		LocalInfo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserList.</summary>
	public enum UserListFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>BusinessEmail. </summary>
		BusinessEmail,
		///<summary>BusinessFaxNo. </summary>
		BusinessFaxNo,
		///<summary>BusinessPhoneNo. </summary>
		BusinessPhoneNo,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>CreationTime. </summary>
		CreationTime,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>Description. </summary>
		Description,
		///<summary>Division. </summary>
		Division,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>Password. </summary>
		Password,
		///<summary>PasswordChanged. </summary>
		PasswordChanged,
		///<summary>Shortname. </summary>
		Shortname,
		///<summary>Status. </summary>
		Status,
		///<summary>UserID. </summary>
		UserID,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserResource.</summary>
	public enum UserResourceFieldIndex
	{
		///<summary>LanguageID. </summary>
		LanguageID,
		///<summary>ResourceDescription. </summary>
		ResourceDescription,
		///<summary>ResourceGroup. </summary>
		ResourceGroup,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>ResourceName. </summary>
		ResourceName,
		///<summary>ResourceType. </summary>
		ResourceType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserStatus.</summary>
	public enum UserStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserToWorkstation.</summary>
	public enum UserToWorkstationFieldIndex
	{
		///<summary>UserID. </summary>
		UserID,
		///<summary>WorkstationID. </summary>
		WorkstationID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationTransaction.</summary>
	public enum ValidationTransactionFieldIndex
	{
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>IsCritical. </summary>
		IsCritical,
		///<summary>IsValid. </summary>
		IsValid,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Resolved. </summary>
		Resolved,
		///<summary>Rule10IsValid. </summary>
		Rule10IsValid,
		///<summary>Rule11IsValid. </summary>
		Rule11IsValid,
		///<summary>Rule1IsValid. </summary>
		Rule1IsValid,
		///<summary>Rule2IsValid. </summary>
		Rule2IsValid,
		///<summary>Rule3IsValid. </summary>
		Rule3IsValid,
		///<summary>Rule4IsValid. </summary>
		Rule4IsValid,
		///<summary>Rule5IsValid. </summary>
		Rule5IsValid,
		///<summary>Rule6IsValid. </summary>
		Rule6IsValid,
		///<summary>Rule7IsValid. </summary>
		Rule7IsValid,
		///<summary>Rule8IsValid. </summary>
		Rule8IsValid,
		///<summary>Rule9IsValid. </summary>
		Rule9IsValid,
		///<summary>RuleID. </summary>
		RuleID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>ValidationResultDetail. </summary>
		ValidationResultDetail,
		///<summary>ValidationRunID. </summary>
		ValidationRunID,
		///<summary>ValidationTransactionID. </summary>
		ValidationTransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VarioAddress.</summary>
	public enum VarioAddressFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>City. </summary>
		City,
		///<summary>Country. </summary>
		Country,
		///<summary>Email. </summary>
		Email,
		///<summary>Fax. </summary>
		Fax,
		///<summary>PostalCode. </summary>
		PostalCode,
		///<summary>PrivateMobilePhoneNumber. </summary>
		PrivateMobilePhoneNumber,
		///<summary>PrivatePhoneNumber. </summary>
		PrivatePhoneNumber,
		///<summary>Street. </summary>
		Street,
		///<summary>StreetNumber. </summary>
		StreetNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VarioSettlement.</summary>
	public enum VarioSettlementFieldIndex
	{
		///<summary>BillMonth. </summary>
		BillMonth,
		///<summary>CashID. </summary>
		CashID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>DepotID. </summary>
		DepotID,
		///<summary>ExportFile. </summary>
		ExportFile,
		///<summary>ID. </summary>
		ID,
		///<summary>RecordCount. </summary>
		RecordCount,
		///<summary>SettlementNumber. </summary>
		SettlementNumber,
		///<summary>State. </summary>
		State,
		///<summary>TypeOfClosing. </summary>
		TypeOfClosing,
		///<summary>TypeOfSettlement. </summary>
		TypeOfSettlement,
		///<summary>UserID. </summary>
		UserID,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VarioTypeOfSettlement.</summary>
	public enum VarioTypeOfSettlementFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvKeyInfo.</summary>
	public enum VdvKeyInfoFieldIndex
	{
		///<summary>Counter. </summary>
		Counter,
		///<summary>ID. </summary>
		ID,
		///<summary>KeyID. </summary>
		KeyID,
		///<summary>KeyVersion. </summary>
		KeyVersion,
		///<summary>Limit. </summary>
		Limit,
		///<summary>OrganisationID. </summary>
		OrganisationID,
		///<summary>SamID. </summary>
		SamID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvKeySet.</summary>
	public enum VdvKeySetFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>KeySetID. </summary>
		KeySetID,
		///<summary>KeyVersionAuth. </summary>
		KeyVersionAuth,
		///<summary>KeyVersionKVP. </summary>
		KeyVersionKVP,
		///<summary>KeyVersionPV. </summary>
		KeyVersionPV,
		///<summary>KeyVersionPVNM. </summary>
		KeyVersionPVNM,
		///<summary>KeyversionPvSe. </summary>
		KeyversionPvSe,
		///<summary>OrgIdKVP. </summary>
		OrgIdKVP,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvLayout.</summary>
	public enum VdvLayoutFieldIndex
	{
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>Description. </summary>
		Description,
		///<summary>LayoutName. </summary>
		LayoutName,
		///<summary>TariffID. </summary>
		TariffID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvLayoutObject.</summary>
	public enum VdvLayoutObjectFieldIndex
	{
		///<summary>LayoutObjectID. </summary>
		LayoutObjectID,
		///<summary>LayoutID. </summary>
		LayoutID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>ParentLayoutObjectID. </summary>
		ParentLayoutObjectID,
		///<summary>VdvTagID. </summary>
		VdvTagID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvLoadKeyMessage.</summary>
	public enum VdvLoadKeyMessageFieldIndex
	{
		///<summary>LoadKeyCounter. </summary>
		LoadKeyCounter,
		///<summary>LoadKeyNumber. </summary>
		LoadKeyNumber,
		///<summary>LoadKeyResult. </summary>
		LoadKeyResult,
		///<summary>LoadKeyStatus. </summary>
		LoadKeyStatus,
		///<summary>MessageDate. </summary>
		MessageDate,
		///<summary>MessageID. </summary>
		MessageID,
		///<summary>SamID. </summary>
		SamID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvProduct.</summary>
	public enum VdvProductFieldIndex
	{
		///<summary>Brtype2Id. </summary>
		Brtype2Id,
		///<summary>CompanionType1. </summary>
		CompanionType1,
		///<summary>CompanionType2. </summary>
		CompanionType2,
		///<summary>Description. </summary>
		Description,
		///<summary>DistanceAttributeValueID. </summary>
		DistanceAttributeValueID,
		///<summary>FareScale. </summary>
		FareScale,
		///<summary>IssueBarcode. </summary>
		IssueBarcode,
		///<summary>IssueChip. </summary>
		IssueChip,
		///<summary>IssueModeNm. </summary>
		IssueModeNm,
		///<summary>IssueModeSe. </summary>
		IssueModeSe,
		///<summary>KeyProductOrgID. </summary>
		KeyProductOrgID,
		///<summary>KeyVersionAuth. </summary>
		KeyVersionAuth,
		///<summary>KeyVersionKVP. </summary>
		KeyVersionKVP,
		///<summary>KeyVersionPV. </summary>
		KeyVersionPV,
		///<summary>MaxCompanions1. </summary>
		MaxCompanions1,
		///<summary>MaxCompanions2. </summary>
		MaxCompanions2,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>PassengerType. </summary>
		PassengerType,
		///<summary>PeriodID. </summary>
		PeriodID,
		///<summary>PriorityMode. </summary>
		PriorityMode,
		///<summary>ProductNumber. </summary>
		ProductNumber,
		///<summary>ProductResponsible. </summary>
		ProductResponsible,
		///<summary>SellProductNumber. </summary>
		SellProductNumber,
		///<summary>ServiceClass. </summary>
		ServiceClass,
		///<summary>TariffID. </summary>
		TariffID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TransportCategory. </summary>
		TransportCategory,
		///<summary>VdvInfoText. </summary>
		VdvInfoText,
		///<summary>VdvLayoutID. </summary>
		VdvLayoutID,
		///<summary>VdvProductID. </summary>
		VdvProductID,
		///<summary>WritePrice. </summary>
		WritePrice,
		///<summary>WriteVAT. </summary>
		WriteVAT,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvSamStatus.</summary>
	public enum VdvSamStatusFieldIndex
	{
		///<summary>CertificateEndOfValidity. </summary>
		CertificateEndOfValidity,
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>ExportCounter. </summary>
		ExportCounter,
		///<summary>ExportId. </summary>
		ExportId,
		///<summary>ExportSequenceId. </summary>
		ExportSequenceId,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>LastData. </summary>
		LastData,
		///<summary>LoadKeyCounter. </summary>
		LoadKeyCounter,
		///<summary>SamID. </summary>
		SamID,
		///<summary>SamOperatorKeyID. </summary>
		SamOperatorKeyID,
		///<summary>SamSeqenceNumber. </summary>
		SamSeqenceNumber,
		///<summary>SamVersion. </summary>
		SamVersion,
		///<summary>StatusID. </summary>
		StatusID,
		///<summary>TerminalID. </summary>
		TerminalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvTag.</summary>
	public enum VdvTagFieldIndex
	{
		///<summary>TagID. </summary>
		TagID,
		///<summary>Description. </summary>
		Description,
		///<summary>IdString. </summary>
		IdString,
		///<summary>IsContainerObject. </summary>
		IsContainerObject,
		///<summary>IsRootObject. </summary>
		IsRootObject,
		///<summary>TagName. </summary>
		TagName,
		///<summary>TagNumber. </summary>
		TagNumber,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvTerminal.</summary>
	public enum VdvTerminalFieldIndex
	{
		///<summary>ID. </summary>
		ID,
		///<summary>Number. </summary>
		Number,
		///<summary>OrganisationID. </summary>
		OrganisationID,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvType.</summary>
	public enum VdvTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>TypeGroupID. </summary>
		TypeGroupID,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>TypeName. </summary>
		TypeName,
		///<summary>TypeNumber. </summary>
		TypeNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Workstation.</summary>
	public enum WorkstationFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ComputerName. </summary>
		ComputerName,
		///<summary>WorkstationID. </summary>
		WorkstationID,
		///<summary>WorkstationNo. </summary>
		WorkstationNo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingCancelRecTapCmlSettled.</summary>
	public enum AccountingCancelRecTapCmlSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingCancelRecTapTabSettled.</summary>
	public enum AccountingCancelRecTapTabSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationFeferenceGuid. </summary>
		CancellationFeferenceGuid,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingMethod.</summary>
	public enum AccountingMethodFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingMode.</summary>
	public enum AccountingModeFieldIndex
	{
		///<summary>AccountingMethod. </summary>
		AccountingMethod,
		///<summary>AccountingModeID. </summary>
		AccountingModeID,
		///<summary>ContractType. </summary>
		ContractType,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecLoadUseCmlSettled.</summary>
	public enum AccountingRecLoadUseCmlSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecLoadUseTabSettled.</summary>
	public enum AccountingRecLoadUseTabSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedLoadAndUse.</summary>
	public enum AccountingRecognizedLoadAndUseFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedPayment.</summary>
	public enum AccountingRecognizedPaymentFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>IsoOrganizational. </summary>
		IsoOrganizational,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MerchantNumber. </summary>
		MerchantNumber,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentRecognitionID. </summary>
		PaymentRecognitionID,
		///<summary>PaymentType. </summary>
		PaymentType,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SaleType. </summary>
		SaleType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedTap.</summary>
	public enum AccountingRecognizedTapFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>BoardingGuid. </summary>
		BoardingGuid,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedTapCancellation.</summary>
	public enum AccountingRecognizedTapCancellationFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodtTo. </summary>
		PeriodtTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedTapCmlSettled.</summary>
	public enum AccountingRecognizedTapCmlSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingRecognizedTapTabSettled.</summary>
	public enum AccountingRecognizedTapTabSettledFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CancelledTap. </summary>
		CancelledTap,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>Line. </summary>
		Line,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>State. </summary>
		State,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionOperatorID. </summary>
		TransactionOperatorID,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountingReconciledPayment.</summary>
	public enum AccountingReconciledPaymentFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Merchantnumber. </summary>
		Merchantnumber,
		///<summary>PaymentjournalID. </summary>
		PaymentjournalID,
		///<summary>PaymentReconciliationID. </summary>
		PaymentReconciliationID,
		///<summary>PaymentType. </summary>
		PaymentType,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Reconciled. </summary>
		Reconciled,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SaleType. </summary>
		SaleType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountMessageSetting.</summary>
	public enum AccountMessageSettingFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdditionalData.</summary>
	public enum AdditionalDataFieldIndex
	{
		///<summary>AdditionalDataID. </summary>
		AdditionalDataID,
		///<summary>HostName. </summary>
		HostName,
		///<summary>PerformanceID. </summary>
		PerformanceID,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Address.</summary>
	public enum AddressFieldIndex
	{
		///<summary>Addressee. </summary>
		Addressee,
		///<summary>AddressField1. </summary>
		AddressField1,
		///<summary>AddressField2. </summary>
		AddressField2,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>City. </summary>
		City,
		///<summary>CoordinateID. </summary>
		CoordinateID,
		///<summary>Country. </summary>
		Country,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PostalCode. </summary>
		PostalCode,
		///<summary>Region. </summary>
		Region,
		///<summary>Salutation. </summary>
		Salutation,
		///<summary>Street. </summary>
		Street,
		///<summary>StreetNumber. </summary>
		StreetNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AddressBookEntry.</summary>
	public enum AddressBookEntryFieldIndex
	{
		///<summary>AddressBookEntryID. </summary>
		AddressBookEntryID,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>BankConnectionDataID. </summary>
		BankConnectionDataID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>EntryType. </summary>
		EntryType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AddressBookEntryType.</summary>
	public enum AddressBookEntryTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AddressType.</summary>
	public enum AddressTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AggregationFunction.</summary>
	public enum AggregationFunctionFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AggregationType.</summary>
	public enum AggregationTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Apportion.</summary>
	public enum ApportionFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>Apportion. </summary>
		Apportion,
		///<summary>ApportionID. </summary>
		ApportionID,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>BoardingGuid. </summary>
		BoardingGuid,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>Premium. </summary>
		Premium,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ApportionPassRevenueRecognition.</summary>
	public enum ApportionPassRevenueRecognitionFieldIndex
	{
		///<summary>PassAmount. </summary>
		PassAmount,
		///<summary>PassCloseoutPeriodID. </summary>
		PassCloseoutPeriodID,
		///<summary>PassRevenueRecognitionID. </summary>
		PassRevenueRecognitionID,
		///<summary>PassRevenueSettlementID. </summary>
		PassRevenueSettlementID,
		///<summary>PassTicketID. </summary>
		PassTicketID,
		///<summary>PassTicketInternalNumber. </summary>
		PassTicketInternalNumber,
		///<summary>PassTransactionJournalID. </summary>
		PassTransactionJournalID,
		///<summary>PassTransitAccountID. </summary>
		PassTransitAccountID,
		///<summary>PassValidFrom. </summary>
		PassValidFrom,
		///<summary>PassValidTo. </summary>
		PassValidTo,
		///<summary>TapAppliedPassTicketID. </summary>
		TapAppliedPassTicketID,
		///<summary>TapBaseFare. </summary>
		TapBaseFare,
		///<summary>TapBoardingGuid. </summary>
		TapBoardingGuid,
		///<summary>TapCancellationReference. </summary>
		TapCancellationReference,
		///<summary>TapCancellationReferenceGuid. </summary>
		TapCancellationReferenceGuid,
		///<summary>TapCloseoutPeriodID. </summary>
		TapCloseoutPeriodID,
		///<summary>TapDeviceTime. </summary>
		TapDeviceTime,
		///<summary>TapOperatorID. </summary>
		TapOperatorID,
		///<summary>TapOriginalSingleFare. </summary>
		TapOriginalSingleFare,
		///<summary>TapPurseCredit. </summary>
		TapPurseCredit,
		///<summary>TapRevenueRecognitionAmount. </summary>
		TapRevenueRecognitionAmount,
		///<summary>TapRevenueRecognitionID. </summary>
		TapRevenueRecognitionID,
		///<summary>TapRevenueSettlementID. </summary>
		TapRevenueSettlementID,
		///<summary>TapTicketID. </summary>
		TapTicketID,
		///<summary>TapTicketNumber. </summary>
		TapTicketNumber,
		///<summary>TapTransactionjournalID. </summary>
		TapTransactionjournalID,
		///<summary>TapTransactionType. </summary>
		TapTransactionType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ArchiveState.</summary>
	public enum ArchiveStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttributeToMobilityProvider.</summary>
	public enum AttributeToMobilityProviderFieldIndex
	{
		///<summary>AttributeID. </summary>
		AttributeID,
		///<summary>MobilityProviderID. </summary>
		MobilityProviderID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AuditRegister.</summary>
	public enum AuditRegisterFieldIndex
	{
		///<summary>AuditRegisterID. </summary>
		AuditRegisterID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ReportDateTime. </summary>
		ReportDateTime,
		///<summary>ShiftID. </summary>
		ShiftID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AuditRegisterValue.</summary>
	public enum AuditRegisterValueFieldIndex
	{
		///<summary>AuditRegisterID. </summary>
		AuditRegisterID,
		///<summary>AuditRegisterValueID. </summary>
		AuditRegisterValueID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RegisterValue. </summary>
		RegisterValue,
		///<summary>RegisterValueType. </summary>
		RegisterValueType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AuditRegisterValueType.</summary>
	public enum AuditRegisterValueTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AutoloadSetting.</summary>
	public enum AutoloadSettingFieldIndex
	{
		///<summary>AlternativePaymentOptionId. </summary>
		AlternativePaymentOptionId,
		///<summary>Amount. </summary>
		Amount,
		///<summary>AutoloadSettingID. </summary>
		AutoloadSettingID,
		///<summary>Condition. </summary>
		Condition,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>PeriodDayOfMonth. </summary>
		PeriodDayOfMonth,
		///<summary>PeriodRunDate. </summary>
		PeriodRunDate,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>SuspendedFrom. </summary>
		SuspendedFrom,
		///<summary>SuspendedTo. </summary>
		SuspendedTo,
		///<summary>ThresholdValue. </summary>
		ThresholdValue,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TriggerType. </summary>
		TriggerType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AutoloadToPaymentOption.</summary>
	public enum AutoloadToPaymentOptionFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>AutoloadSettingID. </summary>
		AutoloadSettingID,
		///<summary>AutoloadToPaymentOptionID. </summary>
		AutoloadToPaymentOptionID,
		///<summary>BackupPaymentOptionID. </summary>
		BackupPaymentOptionID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BadCard.</summary>
	public enum BadCardFieldIndex
	{
		///<summary>BadCardID. </summary>
		BadCardID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CreateDate. </summary>
		CreateDate,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>FirstBadTransactionNumber. </summary>
		FirstBadTransactionNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BadCheck.</summary>
	public enum BadCheckFieldIndex
	{
		///<summary>BadCheckID. </summary>
		BadCheckID,
		///<summary>BranchRouting. </summary>
		BranchRouting,
		///<summary>CheckingAccount. </summary>
		CheckingAccount,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MaskedCheckingAccount. </summary>
		MaskedCheckingAccount,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BankConnectionData.</summary>
	public enum BankConnectionDataFieldIndex
	{
		///<summary>AccountOwnerID. </summary>
		AccountOwnerID,
		///<summary>BankConnectionDataID. </summary>
		BankConnectionDataID,
		///<summary>Bic. </summary>
		Bic,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>Description. </summary>
		Description,
		///<summary>FrequencyType. </summary>
		FrequencyType,
		///<summary>Iban. </summary>
		Iban,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUsage. </summary>
		LastUsage,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MandateReference. </summary>
		MandateReference,
		///<summary>OwnerType. </summary>
		OwnerType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BankStatement.</summary>
	public enum BankStatementFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>AuthorizationCode. </summary>
		AuthorizationCode,
		///<summary>BankStatementID. </summary>
		BankStatementID,
		///<summary>CardBrandType. </summary>
		CardBrandType,
		///<summary>ChargebackCode. </summary>
		ChargebackCode,
		///<summary>ChargebackDescription. </summary>
		ChargebackDescription,
		///<summary>DisputeDate. </summary>
		DisputeDate,
		///<summary>FileDate. </summary>
		FileDate,
		///<summary>FileReference. </summary>
		FileReference,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MaskedPan. </summary>
		MaskedPan,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentReference. </summary>
		PaymentReference,
		///<summary>RecordType. </summary>
		RecordType,
		///<summary>ShortPan. </summary>
		ShortPan,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		///<summary>Transactiontime. </summary>
		Transactiontime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BankStatementRecordType.</summary>
	public enum BankStatementRecordTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BankStatementState.</summary>
	public enum BankStatementStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BankStatementVerification.</summary>
	public enum BankStatementVerificationFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BankStatementID. </summary>
		BankStatementID,
		///<summary>BankStatementState. </summary>
		BankStatementState,
		///<summary>MaskedPan. </summary>
		MaskedPan,
		///<summary>MatchedStatements. </summary>
		MatchedStatements,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentReference. </summary>
		PaymentReference,
		///<summary>PaymentState. </summary>
		PaymentState,
		///<summary>ReconciliationState. </summary>
		ReconciliationState,
		///<summary>RecordType. </summary>
		RecordType,
		///<summary>SaleID. </summary>
		SaleID,
		///<summary>ShortPan. </summary>
		ShortPan,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Booking.</summary>
	public enum BookingFieldIndex
	{
		///<summary>BookingDate. </summary>
		BookingDate,
		///<summary>BookingID. </summary>
		BookingID,
		///<summary>CardId. </summary>
		CardId,
		///<summary>Datarowcreationdate. </summary>
		Datarowcreationdate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BookingItem.</summary>
	public enum BookingItemFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>Barcode. </summary>
		Barcode,
		///<summary>BikeType. </summary>
		BikeType,
		///<summary>BookingID. </summary>
		BookingID,
		///<summary>BookingItemID. </summary>
		BookingItemID,
		///<summary>BookingOpen. </summary>
		BookingOpen,
		///<summary>BookingType. </summary>
		BookingType,
		///<summary>BookingTypeTime. </summary>
		BookingTypeTime,
		///<summary>CancellationID. </summary>
		CancellationID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>EndPlace. </summary>
		EndPlace,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>MobilityProviderID. </summary>
		MobilityProviderID,
		///<summary>OptionalFeatureData. </summary>
		OptionalFeatureData,
		///<summary>OptionalFeatureReference. </summary>
		OptionalFeatureReference,
		///<summary>OptionalFeatureText. </summary>
		OptionalFeatureText,
		///<summary>PaymentReference. </summary>
		PaymentReference,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ProductName. </summary>
		ProductName,
		///<summary>ProductType. </summary>
		ProductType,
		///<summary>PurposeReference. </summary>
		PurposeReference,
		///<summary>ReceivedReference. </summary>
		ReceivedReference,
		///<summary>RentalState. </summary>
		RentalState,
		///<summary>RentalStateTime. </summary>
		RentalStateTime,
		///<summary>SaleType. </summary>
		SaleType,
		///<summary>StartPlace. </summary>
		StartPlace,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VehicleReference. </summary>
		VehicleReference,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BookingItemPayment.</summary>
	public enum BookingItemPaymentFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BookingItemID. </summary>
		BookingItemID,
		///<summary>BookingItemPaymentID. </summary>
		BookingItemPaymentID,
		///<summary>BookingItemPaymentType. </summary>
		BookingItemPaymentType,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>Description. </summary>
		Description,
		///<summary>ExecutionTime. </summary>
		ExecutionTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentMethod. </summary>
		PaymentMethod,
		///<summary>PaymentReference. </summary>
		PaymentReference,
		///<summary>PaymentState. </summary>
		PaymentState,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VatAmount. </summary>
		VatAmount,
		///<summary>VatRate. </summary>
		VatRate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BookingItemPaymentType.</summary>
	public enum BookingItemPaymentTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BookingType.</summary>
	public enum BookingTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CappingJournal.</summary>
	public enum CappingJournalFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CappingJournalID. </summary>
		CappingJournalID,
		///<summary>LastIncrement. </summary>
		LastIncrement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MissingAmount. </summary>
		MissingAmount,
		///<summary>PotID. </summary>
		PotID,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CappingState.</summary>
	public enum CappingStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Card.</summary>
	public enum CardFieldIndex
	{
		///<summary>BatchNumber. </summary>
		BatchNumber,
		///<summary>Blocked. </summary>
		Blocked,
		///<summary>BlockingComment. </summary>
		BlockingComment,
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>CardHolderID. </summary>
		CardHolderID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardKeyID. </summary>
		CardKeyID,
		///<summary>CardPhysicalDetailID. </summary>
		CardPhysicalDetailID,
		///<summary>CardType. </summary>
		CardType,
		///<summary>ChargeCounter. </summary>
		ChargeCounter,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>Expiration. </summary>
		Expiration,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>FareMediaID. </summary>
		FareMediaID,
		///<summary>Imported. </summary>
		Imported,
		///<summary>InventoryState. </summary>
		InventoryState,
		///<summary>IsProductPool. </summary>
		IsProductPool,
		///<summary>IsShared. </summary>
		IsShared,
		///<summary>IsTraining. </summary>
		IsTraining,
		///<summary>IsVoiceEnabled. </summary>
		IsVoiceEnabled,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUsed. </summary>
		LastUsed,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LoadCounter. </summary>
		LoadCounter,
		///<summary>NumberOfPrints. </summary>
		NumberOfPrints,
		///<summary>OrderIdentifier. </summary>
		OrderIdentifier,
		///<summary>ParticipantID. </summary>
		ParticipantID,
		///<summary>PaymentAccountReference. </summary>
		PaymentAccountReference,
		///<summary>PinCode. </summary>
		PinCode,
		///<summary>PrintedDate. </summary>
		PrintedDate,
		///<summary>PrintedNumber. </summary>
		PrintedNumber,
		///<summary>ProductionDate. </summary>
		ProductionDate,
		///<summary>ProjectIdentifier. </summary>
		ProjectIdentifier,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>ReplacedCardID. </summary>
		ReplacedCardID,
		///<summary>Revision. </summary>
		Revision,
		///<summary>SecurityCode. </summary>
		SecurityCode,
		///<summary>SequentialNumber. </summary>
		SequentialNumber,
		///<summary>SerialNumber. </summary>
		SerialNumber,
		///<summary>ShortCode. </summary>
		ShortCode,
		///<summary>State. </summary>
		State,
		///<summary>StorageLocationID. </summary>
		StorageLocationID,
		///<summary>SubsidyLimit. </summary>
		SubsidyLimit,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserGroupID. </summary>
		UserGroupID,
		///<summary>WrongPinAttempt. </summary>
		WrongPinAttempt,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardBodyType.</summary>
	public enum CardBodyTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardDormancy.</summary>
	public enum CardDormancyFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardType. </summary>
		CardType,
		///<summary>Expiration. </summary>
		Expiration,
		///<summary>FareMediaID. </summary>
		FareMediaID,
		///<summary>LastTransaction. </summary>
		LastTransaction,
		///<summary>NotificationDate. </summary>
		NotificationDate,
		///<summary>PrintedNumber. </summary>
		PrintedNumber,
		///<summary>State. </summary>
		State,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardEvent.</summary>
	public enum CardEventFieldIndex
	{
		///<summary>CardEventID. </summary>
		CardEventID,
		///<summary>CardEventType. </summary>
		CardEventType,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Description. </summary>
		Description,
		///<summary>EventTime. </summary>
		EventTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Reason. </summary>
		Reason,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardEventType.</summary>
	public enum CardEventTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardFulfillmentStatus.</summary>
	public enum CardFulfillmentStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardHolder.</summary>
	public enum CardHolderFieldIndex
	{
		///<summary>AcceptTermsAndConditions. Inherited from Person</summary>
		AcceptTermsAndConditions,
		///<summary>AddressID. Inherited from Person</summary>
		AddressID,
		///<summary>Authenticated. Inherited from Person</summary>
		Authenticated,
		///<summary>CellPhoneNumber. Inherited from Person</summary>
		CellPhoneNumber,
		///<summary>DataProtectionNoticeAccepted. Inherited from Person</summary>
		DataProtectionNoticeAccepted,
		///<summary>DateOfBirth. Inherited from Person</summary>
		DateOfBirth,
		///<summary>Email. Inherited from Person</summary>
		Email,
		///<summary>FaxNumber. Inherited from Person</summary>
		FaxNumber,
		///<summary>FirstName. Inherited from Person</summary>
		FirstName,
		///<summary>Gender. Inherited from Person</summary>
		Gender,
		///<summary>HasDrivingLicense. Inherited from Person</summary>
		HasDrivingLicense,
		///<summary>Identifier. Inherited from Person</summary>
		Identifier,
		///<summary>IsCellPhoneNumberConfirmed. Inherited from Person</summary>
		IsCellPhoneNumberConfirmed,
		///<summary>IsEmailConfirmed. Inherited from Person</summary>
		IsEmailConfirmed,
		///<summary>IsEmailEnabled. Inherited from Person</summary>
		IsEmailEnabled,
		///<summary>IsNewsletterAccepted. Inherited from Person</summary>
		IsNewsletterAccepted,
		///<summary>JobTitle. Inherited from Person</summary>
		JobTitle,
		///<summary>LastModified. Inherited from Person</summary>
		LastModified,
		///<summary>LastName. Inherited from Person</summary>
		LastName,
		///<summary>LastUser. Inherited from Person</summary>
		LastUser,
		///<summary>MiddleName. Inherited from Person</summary>
		MiddleName,
		///<summary>OrganizationalIdentifier. Inherited from Person</summary>
		OrganizationalIdentifier,
		///<summary>PersonID. Inherited from Person</summary>
		PersonID,
		///<summary>PhoneNumber. Inherited from Person</summary>
		PhoneNumber,
		///<summary>Salutation. Inherited from Person</summary>
		Salutation,
		///<summary>Title. Inherited from Person</summary>
		Title,
		///<summary>TransactionCounter. Inherited from Person</summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. Inherited from Person</summary>
		TypeDiscriminator,
		///<summary>AlternativeAdressID. </summary>
		AlternativeAdressID,
		///<summary>CommentAlarm. </summary>
		CommentAlarm,
		///<summary>CommentText. </summary>
		CommentText,
		///<summary>CommentText2. </summary>
		CommentText2,
		///<summary>CommentText3. </summary>
		CommentText3,
		///<summary>CommentText4. </summary>
		CommentText4,
		///<summary>CommentText5. </summary>
		CommentText5,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Grade. </summary>
		Grade,
		///<summary>Municipality. </summary>
		Municipality,
		///<summary>PersonalAssistent. </summary>
		PersonalAssistent,
		///<summary>SchoolYearID. </summary>
		SchoolYearID,
		///<summary>UserGroup. </summary>
		UserGroup,
		///<summary>UserGroupExpiry. </summary>
		UserGroupExpiry,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardHolderOrganization.</summary>
	public enum CardHolderOrganizationFieldIndex
	{
		///<summary>CardHolderID. </summary>
		CardHolderID,
		///<summary>Created. </summary>
		Created,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardKey.</summary>
	public enum CardKeyFieldIndex
	{
		///<summary>CardKeyID. </summary>
		CardKeyID,
		///<summary>CardKeyVersion. </summary>
		CardKeyVersion,
		///<summary>DataFileVersion. </summary>
		DataFileVersion,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SignatureKeyVersion. </summary>
		SignatureKeyVersion,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionFileVersion. </summary>
		TransactionFileVersion,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardLiability.</summary>
	public enum CardLiabilityFieldIndex
	{
		///<summary>CardBalance. </summary>
		CardBalance,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardLiabilityID. </summary>
		CardLiabilityID,
		///<summary>CardTransactionCounter. </summary>
		CardTransactionCounter,
		///<summary>FinanceDate. </summary>
		FinanceDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardLink.</summary>
	public enum CardLinkFieldIndex
	{
		///<summary>SourceCardID. </summary>
		SourceCardID,
		///<summary>TargetCardID. </summary>
		TargetCardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardOrderDetail.</summary>
	public enum CardOrderDetailFieldIndex
	{
		///<summary>CardHolderID. </summary>
		CardHolderID,
		///<summary>CardOrderDetailID. </summary>
		CardOrderDetailID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderDetailID. </summary>
		OrderDetailID,
		///<summary>PendingOrderID. </summary>
		PendingOrderID,
		///<summary>ReplacedCardID. </summary>
		ReplacedCardID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardPhysicalDetail.</summary>
	public enum CardPhysicalDetailFieldIndex
	{
		///<summary>BodyType. </summary>
		BodyType,
		///<summary>CardPhysicalDetailID. </summary>
		CardPhysicalDetailID,
		///<summary>ChipTypeID. </summary>
		ChipTypeID,
		///<summary>Description. </summary>
		Description,
		///<summary>IsPrinted. </summary>
		IsPrinted,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardPrintingType.</summary>
	public enum CardPrintingTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardStockTransfer.</summary>
	public enum CardStockTransferFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>StockTransferID. </summary>
		StockTransferID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardToContract.</summary>
	public enum CardToContractFieldIndex
	{
		///<summary>AssociationType. </summary>
		AssociationType,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Description. </summary>
		Description,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardToRuleViolation.</summary>
	public enum CardToRuleViolationFieldIndex
	{
		///<summary>AggregationFrom. </summary>
		AggregationFrom,
		///<summary>AggregationTo. </summary>
		AggregationTo,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardToRuleViolationID. </summary>
		CardToRuleViolationID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RuleViolationCounter. </summary>
		RuleViolationCounter,
		///<summary>RuleViolationID. </summary>
		RuleViolationID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CardWorkItem.</summary>
	public enum CardWorkItemFieldIndex
	{
		///<summary>Assigned. Inherited from WorkItem</summary>
		Assigned,
		///<summary>Assignee. Inherited from WorkItem</summary>
		Assignee,
		///<summary>Created. Inherited from WorkItem</summary>
		Created,
		///<summary>CreatedBy. Inherited from WorkItem</summary>
		CreatedBy,
		///<summary>Executed. Inherited from WorkItem</summary>
		Executed,
		///<summary>LastModified. Inherited from WorkItem</summary>
		LastModified,
		///<summary>LastUser. Inherited from WorkItem</summary>
		LastUser,
		///<summary>Memo. Inherited from WorkItem</summary>
		Memo,
		///<summary>Source. Inherited from WorkItem</summary>
		Source,
		///<summary>State. Inherited from WorkItem</summary>
		State,
		///<summary>Title. Inherited from WorkItem</summary>
		Title,
		///<summary>TransactionCounter. Inherited from WorkItem</summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. Inherited from WorkItem</summary>
		TypeDiscriminator,
		///<summary>WorkItemID. Inherited from WorkItem</summary>
		WorkItemID,
		///<summary>WorkItemSubjectID. Inherited from WorkItem</summary>
		WorkItemSubjectID,
		///<summary>CardID. </summary>
		CardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Certificate.</summary>
	public enum CertificateFieldIndex
	{
		///<summary>ApiType. </summary>
		ApiType,
		///<summary>Certificate. </summary>
		Certificate,
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>CertificateSignature. </summary>
		CertificateSignature,
		///<summary>CertificateStatus. </summary>
		CertificateStatus,
		///<summary>CertificateType. </summary>
		CertificateType,
		///<summary>CryptoArchiveID. </summary>
		CryptoArchiveID,
		///<summary>CryptoClientID. </summary>
		CryptoClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>IncludeInQArchive. </summary>
		IncludeInQArchive,
		///<summary>IssuerName. </summary>
		IssuerName,
		///<summary>KeyType. </summary>
		KeyType,
		///<summary>Label. </summary>
		Label,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>SubjectName. </summary>
		SubjectName,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CertificateFormat.</summary>
	public enum CertificateFormatFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CertificatePurpose.</summary>
	public enum CertificatePurposeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CertificateType.</summary>
	public enum CertificateTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Claim.</summary>
	public enum ClaimFieldIndex
	{
		///<summary>ClaimID. </summary>
		ClaimID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>URI. </summary>
		URI,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientFilterType.</summary>
	public enum ClientFilterTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloseoutPeriod.</summary>
	public enum CloseoutPeriodFieldIndex
	{
		///<summary>CloseoutDate. </summary>
		CloseoutDate,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CloseoutType. </summary>
		CloseoutType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PeriodFrom. </summary>
		PeriodFrom,
		///<summary>PeriodTo. </summary>
		PeriodTo,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloseoutState.</summary>
	public enum CloseoutStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloseoutType.</summary>
	public enum CloseoutTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Comment.</summary>
	public enum CommentFieldIndex
	{
		///<summary>CommentID. </summary>
		CommentID,
		///<summary>CommentType. </summary>
		CommentType,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Priority. </summary>
		Priority,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CommentPriority.</summary>
	public enum CommentPriorityFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CommentType.</summary>
	public enum CommentTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Configuration.</summary>
	public enum ConfigurationFieldIndex
	{
		///<summary>ConfigurationID. </summary>
		ConfigurationID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>Scope. </summary>
		Scope,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ConfigurationDataType.</summary>
	public enum ConfigurationDataTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ConfigurationDefinition.</summary>
	public enum ConfigurationDefinitionFieldIndex
	{
		///<summary>AccessLevel. </summary>
		AccessLevel,
		///<summary>ConfigurationDefinitionID. </summary>
		ConfigurationDefinitionID,
		///<summary>DataType. </summary>
		DataType,
		///<summary>DefaultValue. </summary>
		DefaultValue,
		///<summary>Description. </summary>
		Description,
		///<summary>GroupKey. </summary>
		GroupKey,
		///<summary>IsDefaultConfigurable. </summary>
		IsDefaultConfigurable,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>OwnerClientID. </summary>
		OwnerClientID,
		///<summary>Scope. </summary>
		Scope,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ConfigurationOverwrite.</summary>
	public enum ConfigurationOverwriteFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ConfigurationDefinitionID. </summary>
		ConfigurationDefinitionID,
		///<summary>ConfigurationOverwriteID. </summary>
		ConfigurationOverwriteID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SpecifierType. </summary>
		SpecifierType,
		///<summary>SpecifierValue. </summary>
		SpecifierValue,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContentItem.</summary>
	public enum ContentItemFieldIndex
	{
		///<summary>Caption. </summary>
		Caption,
		///<summary>Content. </summary>
		Content,
		///<summary>ContentItemID. </summary>
		ContentItemID,
		///<summary>Image. </summary>
		Image,
		///<summary>IsRelevant. </summary>
		IsRelevant,
		///<summary>Language. </summary>
		Language,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PageContentID. </summary>
		PageContentID,
		///<summary>SequenceNumber. </summary>
		SequenceNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContentType.</summary>
	public enum ContentTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Contract.</summary>
	public enum ContractFieldIndex
	{
		///<summary>AccountingModeID. </summary>
		AccountingModeID,
		///<summary>AdditionalText. </summary>
		AdditionalText,
		///<summary>ClientAuthenticationToken. </summary>
		ClientAuthenticationToken,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ContractNumber. </summary>
		ContractNumber,
		///<summary>ContractType. </summary>
		ContractType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>PaymentModalityID. </summary>
		PaymentModalityID,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>VanpoolClientID. </summary>
		VanpoolClientID,
		///<summary>VanpoolGroupID. </summary>
		VanpoolGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractAddress.</summary>
	public enum ContractAddressFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>AddressType. </summary>
		AddressType,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Created. </summary>
		Created,
		///<summary>IsRetired. </summary>
		IsRetired,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractDetails.</summary>
	public enum ContractDetailsFieldIndex
	{
		///<summary>AccountBalanceEndLastMonth. </summary>
		AccountBalanceEndLastMonth,
		///<summary>AccountBalanceStartLastMonth. </summary>
		AccountBalanceStartLastMonth,
		///<summary>AccountBalanceToday. </summary>
		AccountBalanceToday,
		///<summary>AccountingModeID. </summary>
		AccountingModeID,
		///<summary>AdditionalText. </summary>
		AdditionalText,
		///<summary>AddressAddressee. </summary>
		AddressAddressee,
		///<summary>AddressAddressField1. </summary>
		AddressAddressField1,
		///<summary>AddressAddressField2. </summary>
		AddressAddressField2,
		///<summary>AddressCity. </summary>
		AddressCity,
		///<summary>AddressCountry. </summary>
		AddressCountry,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>AddressPostalCode. </summary>
		AddressPostalCode,
		///<summary>AddressRegion. </summary>
		AddressRegion,
		///<summary>AddressStreet. </summary>
		AddressStreet,
		///<summary>AddressStreetNumber. </summary>
		AddressStreetNumber,
		///<summary>BankConnectionDataBic. </summary>
		BankConnectionDataBic,
		///<summary>BankConnectionDataIban. </summary>
		BankConnectionDataIban,
		///<summary>BankConnectionDataLastUsage. </summary>
		BankConnectionDataLastUsage,
		///<summary>ClientAuthenticationToken. </summary>
		ClientAuthenticationToken,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CommentPriorityName. </summary>
		CommentPriorityName,
		///<summary>CommentPriorityNumber. </summary>
		CommentPriorityNumber,
		///<summary>CommentText. </summary>
		CommentText,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ContractNumber. </summary>
		ContractNumber,
		///<summary>ContractType. </summary>
		ContractType,
		///<summary>DunningCreationDate. </summary>
		DunningCreationDate,
		///<summary>DunningInvoiceNumber. </summary>
		DunningInvoiceNumber,
		///<summary>DunningLevelName. </summary>
		DunningLevelName,
		///<summary>DunningProcessNumber. </summary>
		DunningProcessNumber,
		///<summary>LastInvoiceCreationDate. </summary>
		LastInvoiceCreationDate,
		///<summary>LastInvoiceNumber. </summary>
		LastInvoiceNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationAbbreviation. </summary>
		OrganizationAbbreviation,
		///<summary>OrganizationDescription. </summary>
		OrganizationDescription,
		///<summary>OrganizationExternalNumber. </summary>
		OrganizationExternalNumber,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>OrganizationName. </summary>
		OrganizationName,
		///<summary>OrganizationNumber. </summary>
		OrganizationNumber,
		///<summary>PaymentModalityID. </summary>
		PaymentModalityID,
		///<summary>PaymentModalityPaymentMethod. </summary>
		PaymentModalityPaymentMethod,
		///<summary>PaymentModalityPaymentMethodID. </summary>
		PaymentModalityPaymentMethodID,
		///<summary>PersonCellPhoneNumber. </summary>
		PersonCellPhoneNumber,
		///<summary>PersonDateOfBirth. </summary>
		PersonDateOfBirth,
		///<summary>PersonEmail. </summary>
		PersonEmail,
		///<summary>PersonFaxNumber. </summary>
		PersonFaxNumber,
		///<summary>PersonFirstName. </summary>
		PersonFirstName,
		///<summary>PersonGender. </summary>
		PersonGender,
		///<summary>PersonLastName. </summary>
		PersonLastName,
		///<summary>PersonMiddleName. </summary>
		PersonMiddleName,
		///<summary>PersonPersonID. </summary>
		PersonPersonID,
		///<summary>PersonPhoneNumber. </summary>
		PersonPhoneNumber,
		///<summary>PersonSalutation. </summary>
		PersonSalutation,
		///<summary>PersonTitle. </summary>
		PersonTitle,
		///<summary>PostingAmountSumCurrentMonth. </summary>
		PostingAmountSumCurrentMonth,
		///<summary>PostingAmountSumLastMonth. </summary>
		PostingAmountSumLastMonth,
		///<summary>ProductTerminationDate. </summary>
		ProductTerminationDate,
		///<summary>ProductTerminationDemandDate. </summary>
		ProductTerminationDemandDate,
		///<summary>ProductTerminationProductID. </summary>
		ProductTerminationProductID,
		///<summary>ProductTerminationTypeName. </summary>
		ProductTerminationTypeName,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractHistory.</summary>
	public enum ContractHistoryFieldIndex
	{
		///<summary>ContractHistoryID. </summary>
		ContractHistoryID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>LastCustomer. </summary>
		LastCustomer,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractLink.</summary>
	public enum ContractLinkFieldIndex
	{
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>SourceContractID. </summary>
		SourceContractID,
		///<summary>TargetContractID. </summary>
		TargetContractID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractState.</summary>
	public enum ContractStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractTermination.</summary>
	public enum ContractTerminationFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ContractTerminationID. </summary>
		ContractTerminationID,
		///<summary>ContractTerminationTypeID. </summary>
		ContractTerminationTypeID,
		///<summary>Description. </summary>
		Description,
		///<summary>EmployeeID. </summary>
		EmployeeID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TerminationDate. </summary>
		TerminationDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractTerminationType.</summary>
	public enum ContractTerminationTypeFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractTerminationTypeID. </summary>
		ContractTerminationTypeID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractToPaymentMethod.</summary>
	public enum ContractToPaymentMethodFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractToProvider.</summary>
	public enum ContractToProviderFieldIndex
	{
		///<summary>AcceptTermsAndConditions. </summary>
		AcceptTermsAndConditions,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ContractToProviderID. </summary>
		ContractToProviderID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>EncryptedPassword. </summary>
		EncryptedPassword,
		///<summary>HasSubscription. </summary>
		HasSubscription,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MobilityProviderID. </summary>
		MobilityProviderID,
		///<summary>OtherProviderReference. </summary>
		OtherProviderReference,
		///<summary>ProviderReference. </summary>
		ProviderReference,
		///<summary>State. </summary>
		State,
		///<summary>SubscriptionReference. </summary>
		SubscriptionReference,
		///<summary>SubscriptionValidUntil. </summary>
		SubscriptionValidUntil,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractType.</summary>
	public enum ContractTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ContractWorkItem.</summary>
	public enum ContractWorkItemFieldIndex
	{
		///<summary>Assigned. Inherited from WorkItem</summary>
		Assigned,
		///<summary>Assignee. Inherited from WorkItem</summary>
		Assignee,
		///<summary>Created. Inherited from WorkItem</summary>
		Created,
		///<summary>CreatedBy. Inherited from WorkItem</summary>
		CreatedBy,
		///<summary>Executed. Inherited from WorkItem</summary>
		Executed,
		///<summary>LastModified. Inherited from WorkItem</summary>
		LastModified,
		///<summary>LastUser. Inherited from WorkItem</summary>
		LastUser,
		///<summary>Memo. Inherited from WorkItem</summary>
		Memo,
		///<summary>Source. Inherited from WorkItem</summary>
		Source,
		///<summary>State. Inherited from WorkItem</summary>
		State,
		///<summary>Title. Inherited from WorkItem</summary>
		Title,
		///<summary>TransactionCounter. Inherited from WorkItem</summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. Inherited from WorkItem</summary>
		TypeDiscriminator,
		///<summary>WorkItemID. Inherited from WorkItem</summary>
		WorkItemID,
		///<summary>WorkItemSubjectID. Inherited from WorkItem</summary>
		WorkItemSubjectID,
		///<summary>ContractID. </summary>
		ContractID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Coordinate.</summary>
	public enum CoordinateFieldIndex
	{
		///<summary>CoordinateID. </summary>
		CoordinateID,
		///<summary>Easting. </summary>
		Easting,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Northing. </summary>
		Northing,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ZoneName. </summary>
		ZoneName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Country.</summary>
	public enum CountryFieldIndex
	{
		///<summary>Code. </summary>
		Code,
		///<summary>CountryID. </summary>
		CountryID,
		///<summary>DialingPrefix. </summary>
		DialingPrefix,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditCardAuthorization.</summary>
	public enum CreditCardAuthorizationFieldIndex
	{
		///<summary>AccountState. </summary>
		AccountState,
		///<summary>ACI. </summary>
		ACI,
		///<summary>Amount. </summary>
		Amount,
		///<summary>AmountRefunded. </summary>
		AmountRefunded,
		///<summary>AuthorizationType. </summary>
		AuthorizationType,
		///<summary>BankNetData. </summary>
		BankNetData,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardLevelResult. </summary>
		CardLevelResult,
		///<summary>CardSequenceNumber. </summary>
		CardSequenceNumber,
		///<summary>CardType. </summary>
		CardType,
		///<summary>CertificationData. </summary>
		CertificationData,
		///<summary>CompletionDateTime. </summary>
		CompletionDateTime,
		///<summary>CompletionType. </summary>
		CompletionType,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>CreditCardAuthorizationID. </summary>
		CreditCardAuthorizationID,
		///<summary>CreditCardTerminalID. </summary>
		CreditCardTerminalID,
		///<summary>DeviceTypeIndicator. </summary>
		DeviceTypeIndicator,
		///<summary>EMVData. </summary>
		EMVData,
		///<summary>EncryptedData. </summary>
		EncryptedData,
		///<summary>ExternalAuthID. </summary>
		ExternalAuthID,
		///<summary>ExternalCardHash. </summary>
		ExternalCardHash,
		///<summary>ExternalCardReference. </summary>
		ExternalCardReference,
		///<summary>IsArqcRequested. </summary>
		IsArqcRequested,
		///<summary>LastEmvDataUpdate. </summary>
		LastEmvDataUpdate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LocalDateTime. </summary>
		LocalDateTime,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>PurseAmount. </summary>
		PurseAmount,
		///<summary>ResponseCode. </summary>
		ResponseCode,
		///<summary>RetryCountdown. </summary>
		RetryCountdown,
		///<summary>RiskFlags. </summary>
		RiskFlags,
		///<summary>Status. </summary>
		Status,
		///<summary>SystemTraceAuditNumber. </summary>
		SystemTraceAuditNumber,
		///<summary>Token. </summary>
		Token,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionDateTime. </summary>
		TransactionDateTime,
		///<summary>TransactionIdentifier. </summary>
		TransactionIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditCardAuthorizationLog.</summary>
	public enum CreditCardAuthorizationLogFieldIndex
	{
		///<summary>CreditCardAuthorizationID. </summary>
		CreditCardAuthorizationID,
		///<summary>CreditCardAuthorizationLogID. </summary>
		CreditCardAuthorizationLogID,
		///<summary>InsertTime. </summary>
		InsertTime,
		///<summary>Request. </summary>
		Request,
		///<summary>Response. </summary>
		Response,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditCardMerchant.</summary>
	public enum CreditCardMerchantFieldIndex
	{
		///<summary>CreditCardMerchantID. </summary>
		CreditCardMerchantID,
		///<summary>ExternalMerchantID. </summary>
		ExternalMerchantID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TokenType. </summary>
		TokenType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditCardNetwork.</summary>
	public enum CreditCardNetworkFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CreditCardTerminal.</summary>
	public enum CreditCardTerminalFieldIndex
	{
		///<summary>CreditCardMerchantID. </summary>
		CreditCardMerchantID,
		///<summary>CreditCardTerminalID. </summary>
		CreditCardTerminalID,
		///<summary>DataWireID. </summary>
		DataWireID,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>ExternalTerminalID. </summary>
		ExternalTerminalID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoCertificate.</summary>
	public enum CryptoCertificateFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>Description. </summary>
		Description,
		///<summary>FileName. </summary>
		FileName,
		///<summary>Format. </summary>
		Format,
		///<summary>IssuerName. </summary>
		IssuerName,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>ParentID. </summary>
		ParentID,
		///<summary>Purpose. </summary>
		Purpose,
		///<summary>QArchiveID. </summary>
		QArchiveID,
		///<summary>RawData. </summary>
		RawData,
		///<summary>Signature. </summary>
		Signature,
		///<summary>SubjectName. </summary>
		SubjectName,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoContent.</summary>
	public enum CryptoContentFieldIndex
	{
		///<summary>ContentID. </summary>
		ContentID,
		///<summary>CryptogramArchiveID. </summary>
		CryptogramArchiveID,
		///<summary>CryptogramFilename. </summary>
		CryptogramFilename,
		///<summary>CryptogramResourceID. </summary>
		CryptogramResourceID,
		///<summary>ErrorType. </summary>
		ErrorType,
		///<summary>KeyLoadCounter. </summary>
		KeyLoadCounter,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LoadStatus. </summary>
		LoadStatus,
		///<summary>ShortSamNumber. </summary>
		ShortSamNumber,
		///<summary>SignCertificateFilename. </summary>
		SignCertificateFilename,
		///<summary>SignCertificateResourceID. </summary>
		SignCertificateResourceID,
		///<summary>SubCertificateFilename. </summary>
		SubCertificateFilename,
		///<summary>SubCertificateResourceID. </summary>
		SubCertificateResourceID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoCryptogramArchive.</summary>
	public enum CryptoCryptogramArchiveFieldIndex
	{
		///<summary>ArchiveDate. </summary>
		ArchiveDate,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CryptogramArchiveID. </summary>
		CryptogramArchiveID,
		///<summary>CryptogramArchiveName. </summary>
		CryptogramArchiveName,
		///<summary>DataStatus. </summary>
		DataStatus,
		///<summary>Description. </summary>
		Description,
		///<summary>IsArchived. </summary>
		IsArchived,
		///<summary>IsReleased. </summary>
		IsReleased,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ReleaseDate. </summary>
		ReleaseDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptogramCertificate.</summary>
	public enum CryptogramCertificateFieldIndex
	{
		///<summary>CAR. </summary>
		CAR,
		///<summary>CHR. </summary>
		CHR,
		///<summary>ID. </summary>
		ID,
		///<summary>KeyType. </summary>
		KeyType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>MessageID. </summary>
		MessageID,
		///<summary>RawData. </summary>
		RawData,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptogramCertificateType.</summary>
	public enum CryptogramCertificateTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptogramErrorType.</summary>
	public enum CryptogramErrorTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptogramLoadStatus.</summary>
	public enum CryptogramLoadStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoOperatorActivationkey.</summary>
	public enum CryptoOperatorActivationkeyFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>KeyInfo. </summary>
		KeyInfo,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OperatorActivationKeyID. </summary>
		OperatorActivationKeyID,
		///<summary>SamOperatorID. </summary>
		SamOperatorID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoQArchive.</summary>
	public enum CryptoQArchiveFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DataStatus. </summary>
		DataStatus,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>QArchiveID. </summary>
		QArchiveID,
		///<summary>ReleaseCounter. </summary>
		ReleaseCounter,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoResource.</summary>
	public enum CryptoResourceFieldIndex
	{
		///<summary>BinaryData. </summary>
		BinaryData,
		///<summary>DataType. </summary>
		DataType,
		///<summary>HashValue. </summary>
		HashValue,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoResourceData.</summary>
	public enum CryptoResourceDataFieldIndex
	{
		///<summary>CryptoResourceDataID. </summary>
		CryptoResourceDataID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUpdate. </summary>
		LastUpdate,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MetaData. </summary>
		MetaData,
		///<summary>RawData. </summary>
		RawData,
		///<summary>ResourceStatus. </summary>
		ResourceStatus,
		///<summary>ResourceType. </summary>
		ResourceType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoSamSignCertificate.</summary>
	public enum CryptoSamSignCertificateFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SamID. </summary>
		SamID,
		///<summary>SamSignCertificateID. </summary>
		SamSignCertificateID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CryptoUicCertificate.</summary>
	public enum CryptoUicCertificateFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>IsTrusted. </summary>
		IsTrusted,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RicsCode. </summary>
		RicsCode,
		///<summary>SignKeyID. </summary>
		SignKeyID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UicCertificateID. </summary>
		UicCertificateID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomAttribute.</summary>
	public enum CustomAttributeFieldIndex
	{
		///<summary>CustomAttributeClassID. </summary>
		CustomAttributeClassID,
		///<summary>CustomAttributeID. </summary>
		CustomAttributeID,
		///<summary>CustomAttributeName. </summary>
		CustomAttributeName,
		///<summary>Description. </summary>
		Description,
		///<summary>IsMultiValue. </summary>
		IsMultiValue,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomAttributeValue.</summary>
	public enum CustomAttributeValueFieldIndex
	{
		///<summary>CustomAttributeID. </summary>
		CustomAttributeID,
		///<summary>CustomAttributeValueID. </summary>
		CustomAttributeValueID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomAttributeValueToPerson.</summary>
	public enum CustomAttributeValueToPersonFieldIndex
	{
		///<summary>CustomAttributeValueID. </summary>
		CustomAttributeValueID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerAccount.</summary>
	public enum CustomerAccountFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>CustomerAccountRoleID. </summary>
		CustomerAccountRoleID,
		///<summary>CustomerLanguageID. </summary>
		CustomerLanguageID,
		///<summary>IsPasswordTemporary. </summary>
		IsPasswordTemporary,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LockoutUntil. </summary>
		LockoutUntil,
		///<summary>Password. </summary>
		Password,
		///<summary>PasswordChanged. </summary>
		PasswordChanged,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>PhonePassword. </summary>
		PhonePassword,
		///<summary>State. </summary>
		State,
		///<summary>TotalLoginAttempts. </summary>
		TotalLoginAttempts,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserName. </summary>
		UserName,
		///<summary>VerificationToken. </summary>
		VerificationToken,
		///<summary>VerificationTokenExpiryTime. </summary>
		VerificationTokenExpiryTime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerAccountNotification.</summary>
	public enum CustomerAccountNotificationFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>CustomerAccountNotificationID. </summary>
		CustomerAccountNotificationID,
		///<summary>DeviceTokenID. </summary>
		DeviceTokenID,
		///<summary>Enabled. </summary>
		Enabled,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageTypeID. </summary>
		MessageTypeID,
		///<summary>SendTo. </summary>
		SendTo,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerAccountRole.</summary>
	public enum CustomerAccountRoleFieldIndex
	{
		///<summary>CustomerAccountRoleID. </summary>
		CustomerAccountRoleID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerAccountState.</summary>
	public enum CustomerAccountStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerAccountToVerificationAttemptType.</summary>
	public enum CustomerAccountToVerificationAttemptTypeFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TotalAttempts. </summary>
		TotalAttempts,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VerificationAttemptType. </summary>
		VerificationAttemptType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomerLanguage.</summary>
	public enum CustomerLanguageFieldIndex
	{
		///<summary>CustomerLanguageID. </summary>
		CustomerLanguageID,
		///<summary>LanguageName. </summary>
		LanguageName,
		///<summary>LanguageSymbol. </summary>
		LanguageSymbol,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceCommunicationHistory.</summary>
	public enum DeviceCommunicationHistoryFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceCommunicationHistoryID. </summary>
		DeviceCommunicationHistoryID,
		///<summary>DeviceReaderID. </summary>
		DeviceReaderID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RequestDate. </summary>
		RequestDate,
		///<summary>RequestDescription. </summary>
		RequestDescription,
		///<summary>RequestType. </summary>
		RequestType,
		///<summary>ResponseDescription. </summary>
		ResponseDescription,
		///<summary>ResponseType. </summary>
		ResponseType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReader.</summary>
	public enum DeviceReaderFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DeviceNo. </summary>
		DeviceNo,
		///<summary>DeviceReaderID. </summary>
		DeviceReaderID,
		///<summary>DeviceReaderNumber. </summary>
		DeviceReaderNumber,
		///<summary>DeviceRole. </summary>
		DeviceRole,
		///<summary>DeviceStatus. </summary>
		DeviceStatus,
		///<summary>KEK. </summary>
		KEK,
		///<summary>LastConnectionDate. </summary>
		LastConnectionDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReaderCryptoResource.</summary>
	public enum DeviceReaderCryptoResourceFieldIndex
	{
		///<summary>CryptoResourceDataID. </summary>
		CryptoResourceDataID,
		///<summary>DeviceReaderCryptoResourceID. </summary>
		DeviceReaderCryptoResourceID,
		///<summary>DeviceReaderID. </summary>
		DeviceReaderID,
		///<summary>DeviceResourceStatus. </summary>
		DeviceResourceStatus,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUpdate. </summary>
		LastUpdate,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReaderJob.</summary>
	public enum DeviceReaderJobFieldIndex
	{
		///<summary>DeviceReaderJobID. </summary>
		DeviceReaderJobID,
		///<summary>DeviceRole. </summary>
		DeviceRole,
		///<summary>JobParameter. </summary>
		JobParameter,
		///<summary>JobPriority. </summary>
		JobPriority,
		///<summary>JobStatus. </summary>
		JobStatus,
		///<summary>JobType. </summary>
		JobType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TargetReaderID. </summary>
		TargetReaderID,
		///<summary>Timeout. </summary>
		Timeout,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>WorkerReaderID. </summary>
		WorkerReaderID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReaderKey.</summary>
	public enum DeviceReaderKeyFieldIndex
	{
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>DataSignatureKey. </summary>
		DataSignatureKey,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceReaderID. </summary>
		DeviceReaderID,
		///<summary>DeviceReaderKeyID. </summary>
		DeviceReaderKeyID,
		///<summary>InstallationDate. </summary>
		InstallationDate,
		///<summary>KeyBlock. </summary>
		KeyBlock,
		///<summary>KeyIndex. </summary>
		KeyIndex,
		///<summary>KeyStatus. </summary>
		KeyStatus,
		///<summary>KeyType. </summary>
		KeyType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReaderKeyToCert.</summary>
	public enum DeviceReaderKeyToCertFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>DeviceReaderKeyID. </summary>
		DeviceReaderKeyID,
		///<summary>DeviceReaderKeyToCertificateID. </summary>
		DeviceReaderKeyToCertificateID,
		///<summary>Label. </summary>
		Label,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceReaderToCert.</summary>
	public enum DeviceReaderToCertFieldIndex
	{
		///<summary>CertificateID. </summary>
		CertificateID,
		///<summary>CertificateStatus. </summary>
		CertificateStatus,
		///<summary>DeviceReaderID. </summary>
		DeviceReaderID,
		///<summary>DeviceReaderToCertificateID. </summary>
		DeviceReaderToCertificateID,
		///<summary>Label. </summary>
		Label,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceToken.</summary>
	public enum DeviceTokenFieldIndex
	{
		///<summary>DeviceToken. </summary>
		DeviceToken,
		///<summary>DeviceTokenID. </summary>
		DeviceTokenID,
		///<summary>DeviceTokenState. </summary>
		DeviceTokenState,
		///<summary>ExpirationDate. </summary>
		ExpirationDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceTokenState.</summary>
	public enum DeviceTokenStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DocumentHistory.</summary>
	public enum DocumentHistoryFieldIndex
	{
		///<summary>DocumentContent. </summary>
		DocumentContent,
		///<summary>DocumentHistoryID. </summary>
		DocumentHistoryID,
		///<summary>DocumentID. </summary>
		DocumentID,
		///<summary>DocumentName. </summary>
		DocumentName,
		///<summary>DocumentPath. </summary>
		DocumentPath,
		///<summary>DocumentType. </summary>
		DocumentType,
		///<summary>DocumentUrl. </summary>
		DocumentUrl,
		///<summary>InstanceCaseNumber. </summary>
		InstanceCaseNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ProviderType. </summary>
		ProviderType,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TStamp. </summary>
		TStamp,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DocumentType.</summary>
	public enum DocumentTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DormancyNotification.</summary>
	public enum DormancyNotificationFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>DormancyNotificationID. </summary>
		DormancyNotificationID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUsed. </summary>
		LastUsed,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Months. </summary>
		Months,
		///<summary>NotificationDate. </summary>
		NotificationDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningAction.</summary>
	public enum DunningActionFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningLevelConfiguration.</summary>
	public enum DunningLevelConfigurationFieldIndex
	{
		///<summary>Action. </summary>
		Action,
		///<summary>AdditionalToleranceDays. </summary>
		AdditionalToleranceDays,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DunningLevelID. </summary>
		DunningLevelID,
		///<summary>FormID. </summary>
		FormID,
		///<summary>GraceDays. </summary>
		GraceDays,
		///<summary>IsFirstLevel. </summary>
		IsFirstLevel,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>NextLevelID. </summary>
		NextLevelID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningLevelPostingKey.</summary>
	public enum DunningLevelPostingKeyFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>DunningLevelID. </summary>
		DunningLevelID,
		///<summary>DunningLevelPostingKeyID. </summary>
		DunningLevelPostingKeyID,
		///<summary>InitalDescription. </summary>
		InitalDescription,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PostingKeyID. </summary>
		PostingKeyID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningProcess.</summary>
	public enum DunningProcessFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>Description. </summary>
		Description,
		///<summary>DunningProcessID. </summary>
		DunningProcessID,
		///<summary>ExternalID. </summary>
		ExternalID,
		///<summary>IsClosed. </summary>
		IsClosed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUpdated. </summary>
		LastUpdated,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ProcessNumber. </summary>
		ProcessNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningToInvoice.</summary>
	public enum DunningToInvoiceFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DunningLevelID. </summary>
		DunningLevelID,
		///<summary>DunningProcessID. </summary>
		DunningProcessID,
		///<summary>DunningToInvoiceID. </summary>
		DunningToInvoiceID,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactioCcounter. </summary>
		TransactioCcounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningToPosting.</summary>
	public enum DunningToPostingFieldIndex
	{
		///<summary>DunningProcessID. </summary>
		DunningProcessID,
		///<summary>PostingID. </summary>
		PostingID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DunningToProduct.</summary>
	public enum DunningToProductFieldIndex
	{
		///<summary>DunningProcessID. </summary>
		DunningProcessID,
		///<summary>ProductID. </summary>
		ProductID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmailEvent.</summary>
	public enum EmailEventFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>EventName. </summary>
		EventName,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>IsConfigurable. </summary>
		IsConfigurable,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>SendToAnonymousCustomerAccounts. </summary>
		SendToAnonymousCustomerAccounts,
		///<summary>SendToCardHolder. </summary>
		SendToCardHolder,
		///<summary>SendToExecutiveCustomerAccount. </summary>
		SendToExecutiveCustomerAccount,
		///<summary>SendToLoadOnlyCustomerAccounts. </summary>
		SendToLoadOnlyCustomerAccounts,
		///<summary>SendToPrimaryCustomerAccount. </summary>
		SendToPrimaryCustomerAccount,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFormSchemaName. </summary>
		ValidFormSchemaName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmailEventResendLock.</summary>
	public enum EmailEventResendLockFieldIndex
	{
		///<summary>AutoloadSettingID. </summary>
		AutoloadSettingID,
		///<summary>BookingItemID. </summary>
		BookingItemID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>EmailEventResendLockID. </summary>
		EmailEventResendLockID,
		///<summary>EventType. </summary>
		EventType,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>NotificationDate. </summary>
		NotificationDate,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>ProductID. </summary>
		ProductID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EmailMessage.</summary>
	public enum EmailMessageFieldIndex
	{
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>FormID. </summary>
		FormID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>Subject. </summary>
		Subject,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Entitlement.</summary>
	public enum EntitlementFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Document. </summary>
		Document,
		///<summary>EntitlementID. </summary>
		EntitlementID,
		///<summary>EntitlementTypeID. </summary>
		EntitlementTypeID,
		///<summary>ExternalID. </summary>
		ExternalID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntitlementToProduct.</summary>
	public enum EntitlementToProductFieldIndex
	{
		///<summary>EntitlementID. </summary>
		EntitlementID,
		///<summary>ProductID. </summary>
		ProductID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntitlementType.</summary>
	public enum EntitlementTypeFieldIndex
	{
		///<summary>ApplicationScope. </summary>
		ApplicationScope,
		///<summary>Description. </summary>
		Description,
		///<summary>EntitlementTypeID. </summary>
		EntitlementTypeID,
		///<summary>IsExternalIDEditable. </summary>
		IsExternalIDEditable,
		///<summary>IsValidToEditable. </summary>
		IsValidToEditable,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EventSetting.</summary>
	public enum EventSettingFieldIndex
	{
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>IsConfigurable. </summary>
		IsConfigurable,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageTypeID. </summary>
		MessageTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareChangeCause.</summary>
	public enum FareChangeCauseFieldIndex
	{
		///<summary>ChangeCauseName. </summary>
		ChangeCauseName,
		///<summary>ChangeCauseNumber. </summary>
		ChangeCauseNumber,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>FareChangeCauseID. </summary>
		FareChangeCauseID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionBehaviour.</summary>
	public enum FareEvasionBehaviourFieldIndex
	{
		///<summary>BehaviourName. </summary>
		BehaviourName,
		///<summary>BehaviourNumber. </summary>
		BehaviourNumber,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>FareEvasionBehaviourID. </summary>
		FareEvasionBehaviourID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionIncident.</summary>
	public enum FareEvasionIncidentFieldIndex
	{
		///<summary>AppealDate. </summary>
		AppealDate,
		///<summary>BinaryDataID. </summary>
		BinaryDataID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CompletedDateTime. </summary>
		CompletedDateTime,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>DirectionNumber. </summary>
		DirectionNumber,
		///<summary>Endstation. </summary>
		Endstation,
		///<summary>ExternalNote. </summary>
		ExternalNote,
		///<summary>FareChangeCauseID. </summary>
		FareChangeCauseID,
		///<summary>FareEvasionBehaviourID. </summary>
		FareEvasionBehaviourID,
		///<summary>FareEvasionCategoryID. </summary>
		FareEvasionCategoryID,
		///<summary>FareEvasionIncidentID. </summary>
		FareEvasionIncidentID,
		///<summary>FareEvasionReasonID. </summary>
		FareEvasionReasonID,
		///<summary>GuardianID. </summary>
		GuardianID,
		///<summary>IdentificationNumber. </summary>
		IdentificationNumber,
		///<summary>IdentificationTypeID. </summary>
		IdentificationTypeID,
		///<summary>IncidentNumber. </summary>
		IncidentNumber,
		///<summary>InsertionTime. </summary>
		InsertionTime,
		///<summary>InsertionUser. </summary>
		InsertionUser,
		///<summary>InspectionReportID. </summary>
		InspectionReportID,
		///<summary>InspectionStopNumber. </summary>
		InspectionStopNumber,
		///<summary>InspectionTime. </summary>
		InspectionTime,
		///<summary>IsAddressPoliceChecked. </summary>
		IsAddressPoliceChecked,
		///<summary>IsAppealed. </summary>
		IsAppealed,
		///<summary>IsBlackened. </summary>
		IsBlackened,
		///<summary>IsManuallyInserted. </summary>
		IsManuallyInserted,
		///<summary>IsPayed. </summary>
		IsPayed,
		///<summary>IsPenaltyDemanded. </summary>
		IsPenaltyDemanded,
		///<summary>IsTicketWithdrawn. </summary>
		IsTicketWithdrawn,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LineName. </summary>
		LineName,
		///<summary>Notes. </summary>
		Notes,
		///<summary>PaymentDueDate. </summary>
		PaymentDueDate,
		///<summary>PenaltyDemandDate. </summary>
		PenaltyDemandDate,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>PrincipalClaim. </summary>
		PrincipalClaim,
		///<summary>ReceiptPadNumber. </summary>
		ReceiptPadNumber,
		///<summary>RecurringLinkID. </summary>
		RecurringLinkID,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusChangeDate. </summary>
		StatusChangeDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransportCompanyID. </summary>
		TransportCompanyID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionInspection.</summary>
	public enum FareEvasionInspectionFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>FareEvasionInspectionID. </summary>
		FareEvasionInspectionID,
		///<summary>InspectionName. </summary>
		InspectionName,
		///<summary>InspectionNumber. </summary>
		InspectionNumber,
		///<summary>IsControl. </summary>
		IsControl,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FareEvasionState.</summary>
	public enum FareEvasionStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FarePaymentError.</summary>
	public enum FarePaymentErrorFieldIndex
	{
		///<summary>Error. </summary>
		Error,
		///<summary>FarePaymentErrorID. </summary>
		FarePaymentErrorID,
		///<summary>FarePaymentRequest. </summary>
		FarePaymentRequest,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FarePaymentResultType.</summary>
	public enum FarePaymentResultTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Fastcard.</summary>
	public enum FastcardFieldIndex
	{
		///<summary>ActiveProducts. </summary>
		ActiveProducts,
		///<summary>BlockingComment. </summary>
		BlockingComment,
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>CardholderContractID. </summary>
		CardholderContractID,
		///<summary>CardholderContractNumber. </summary>
		CardholderContractNumber,
		///<summary>CardholderDateOfBirth. </summary>
		CardholderDateOfBirth,
		///<summary>CardholderEmail. </summary>
		CardholderEmail,
		///<summary>CardholderFirstName. </summary>
		CardholderFirstName,
		///<summary>CardholderFrameOrganizationID. </summary>
		CardholderFrameOrganizationID,
		///<summary>CardholderGroupExpiry. </summary>
		CardholderGroupExpiry,
		///<summary>CardholderID. </summary>
		CardholderID,
		///<summary>CardholderLastName. </summary>
		CardholderLastName,
		///<summary>CardholderSchoolYearID. </summary>
		CardholderSchoolYearID,
		///<summary>CardholderUserGroup. </summary>
		CardholderUserGroup,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardPhysicalDetailDescription. </summary>
		CardPhysicalDetailDescription,
		///<summary>CardPhysicalDetailID. </summary>
		CardPhysicalDetailID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>DeviceMappingNumber. </summary>
		DeviceMappingNumber,
		///<summary>Expiration. </summary>
		Expiration,
		///<summary>InventoryState. </summary>
		InventoryState,
		///<summary>IsBlockAllowed. </summary>
		IsBlockAllowed,
		///<summary>IsShared. </summary>
		IsShared,
		///<summary>IsUnblockAllowed. </summary>
		IsUnblockAllowed,
		///<summary>NumberOfPrints. </summary>
		NumberOfPrints,
		///<summary>OrganizationalIdentifier. </summary>
		OrganizationalIdentifier,
		///<summary>OrganizationNumber. </summary>
		OrganizationNumber,
		///<summary>PrintedDate. </summary>
		PrintedDate,
		///<summary>PrintedNumber. </summary>
		PrintedNumber,
		///<summary>ReplacedCardPrintedNumber. </summary>
		ReplacedCardPrintedNumber,
		///<summary>SequentialNumber. </summary>
		SequentialNumber,
		///<summary>SerialNumber. </summary>
		SerialNumber,
		///<summary>State. </summary>
		State,
		///<summary>UserGroupID. </summary>
		UserGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterCategory.</summary>
	public enum FilterCategoryFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayOrder. </summary>
		DisplayOrder,
		///<summary>FilterCategoryID. </summary>
		FilterCategoryID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterElement.</summary>
	public enum FilterElementFieldIndex
	{
		///<summary>CrystalReportName. </summary>
		CrystalReportName,
		///<summary>Description. </summary>
		Description,
		///<summary>EditMask. </summary>
		EditMask,
		///<summary>FilterElementID. </summary>
		FilterElementID,
		///<summary>FilterTypeID. </summary>
		FilterTypeID,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MaxValue. </summary>
		MaxValue,
		///<summary>MinValue. </summary>
		MinValue,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UseFlexValues. </summary>
		UseFlexValues,
		///<summary>UseMixedValues. </summary>
		UseMixedValues,
		///<summary>UseMultipleValues. </summary>
		UseMultipleValues,
		///<summary>UseRangeValues. </summary>
		UseRangeValues,
		///<summary>ValueListClientFilter. </summary>
		ValueListClientFilter,
		///<summary>ValueListColumn. </summary>
		ValueListColumn,
		///<summary>ValueListDataSourceID. </summary>
		ValueListDataSourceID,
		///<summary>ValueListShown. </summary>
		ValueListShown,
		///<summary>ValueListTable. </summary>
		ValueListTable,
		///<summary>ValueListWhere. </summary>
		ValueListWhere,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterList.</summary>
	public enum FilterListFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayName. </summary>
		DisplayName,
		///<summary>FilterListID. </summary>
		FilterListID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterListElement.</summary>
	public enum FilterListElementFieldIndex
	{
		///<summary>DisplayName. </summary>
		DisplayName,
		///<summary>DisplayOrder. </summary>
		DisplayOrder,
		///<summary>FilterCategoryID. </summary>
		FilterCategoryID,
		///<summary>FilterElementID. </summary>
		FilterElementID,
		///<summary>FilterListElementID. </summary>
		FilterListElementID,
		///<summary>FilterListID. </summary>
		FilterListID,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>IsMandatory. </summary>
		IsMandatory,
		///<summary>IsVisible. </summary>
		IsVisible,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterType.</summary>
	public enum FilterTypeFieldIndex
	{
		///<summary>AllowFlexValue. </summary>
		AllowFlexValue,
		///<summary>AllowMixedValues. </summary>
		AllowMixedValues,
		///<summary>AllowMultipleValues. </summary>
		AllowMultipleValues,
		///<summary>AllowRangeValues. </summary>
		AllowRangeValues,
		///<summary>Description. </summary>
		Description,
		///<summary>FilterTypeID. </summary>
		FilterTypeID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterValue.</summary>
	public enum FilterValueFieldIndex
	{
		///<summary>Content. </summary>
		Content,
		///<summary>FilterListElementID. </summary>
		FilterListElementID,
		///<summary>FilterValueID. </summary>
		FilterValueID,
		///<summary>FilterValueSetID. </summary>
		FilterValueSetID,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Ranger. </summary>
		Ranger,
		///<summary>Separator. </summary>
		Separator,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FilterValueSet.</summary>
	public enum FilterValueSetFieldIndex
	{
		///<summary>Changed. </summary>
		Changed,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>FilterListID. </summary>
		FilterListID,
		///<summary>FilterValueSetID. </summary>
		FilterValueSetID,
		///<summary>IsProtected. </summary>
		IsProtected,
		///<summary>IsStandard. </summary>
		IsStandard,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>OwnerID. </summary>
		OwnerID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserID. </summary>
		UserID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FlexValue.</summary>
	public enum FlexValueFieldIndex
	{
		///<summary>Content. </summary>
		Content,
		///<summary>DisplayOrder. </summary>
		DisplayOrder,
		///<summary>FlexValueID. </summary>
		FlexValueID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Form.</summary>
	public enum FormFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Copies. </summary>
		Copies,
		///<summary>Description. </summary>
		Description,
		///<summary>DocumentType. </summary>
		DocumentType,
		///<summary>FormID. </summary>
		FormID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Layout. </summary>
		Layout,
		///<summary>Name. </summary>
		Name,
		///<summary>PostingScope. </summary>
		PostingScope,
		///<summary>PreviewImage. </summary>
		PreviewImage,
		///<summary>SchemaName. </summary>
		SchemaName,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FormTemplate.</summary>
	public enum FormTemplateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>FormTemplateID. </summary>
		FormTemplateID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>PreviewImage. </summary>
		PreviewImage,
		///<summary>Template. </summary>
		Template,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Frequency.</summary>
	public enum FrequencyFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Gender.</summary>
	public enum GenderFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IdentificationType.</summary>
	public enum IdentificationTypeFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>IdentificationTypeID. </summary>
		IdentificationTypeID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TypeName. </summary>
		TypeName,
		///<summary>TypeNumber. </summary>
		TypeNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InspectionCriterion.</summary>
	public enum InspectionCriterionFieldIndex
	{
		///<summary>Criterion. </summary>
		Criterion,
		///<summary>CriterionComment. </summary>
		CriterionComment,
		///<summary>InspectionCriterionID. </summary>
		InspectionCriterionID,
		///<summary>InspectionReportID. </summary>
		InspectionReportID,
		///<summary>InspectionResult. </summary>
		InspectionResult,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InspectionReport.</summary>
	public enum InspectionReportFieldIndex
	{
		///<summary>Direction. </summary>
		Direction,
		///<summary>DriverNumber. </summary>
		DriverNumber,
		///<summary>FareEvasionInspectionID. </summary>
		FareEvasionInspectionID,
		///<summary>InspectionComment. </summary>
		InspectionComment,
		///<summary>InspectionCount. </summary>
		InspectionCount,
		///<summary>InspectionEnd. </summary>
		InspectionEnd,
		///<summary>InspectionReportID. </summary>
		InspectionReportID,
		///<summary>InspectionStart. </summary>
		InspectionStart,
		///<summary>InspectorNumber. </summary>
		InspectorNumber,
		///<summary>IsProcessed. </summary>
		IsProcessed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Line. </summary>
		Line,
		///<summary>LocationNumber. </summary>
		LocationNumber,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>RawData. </summary>
		RawData,
		///<summary>Report. </summary>
		Report,
		///<summary>Route. </summary>
		Route,
		///<summary>SupervisorComment. </summary>
		SupervisorComment,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VehicleNumber. </summary>
		VehicleNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InspectionResult.</summary>
	public enum InspectionResultFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InventoryState.</summary>
	public enum InventoryStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Invoice.</summary>
	public enum InvoiceFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>Description. </summary>
		Description,
		///<summary>DueDate. </summary>
		DueDate,
		///<summary>FromDate. </summary>
		FromDate,
		///<summary>GeneratedBy. </summary>
		GeneratedBy,
		///<summary>InvoiceEntryAmount. </summary>
		InvoiceEntryAmount,
		///<summary>InvoiceEntryCount. </summary>
		InvoiceEntryCount,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>InvoiceNumber. </summary>
		InvoiceNumber,
		///<summary>InvoiceType. </summary>
		InvoiceType,
		///<summary>InvoicingID. </summary>
		InvoicingID,
		///<summary>IsPaid. </summary>
		IsPaid,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastPrintDate. </summary>
		LastPrintDate,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SchoolYearID. </summary>
		SchoolYearID,
		///<summary>ToDate. </summary>
		ToDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UsedPaymentOptionID. </summary>
		UsedPaymentOptionID,
		///<summary>UserID. </summary>
		UserID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InvoiceEntry.</summary>
	public enum InvoiceEntryFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>InvoiceEntryID. </summary>
		InvoiceEntryID,
		///<summary>InvoiceEntryNumber. </summary>
		InvoiceEntryNumber,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PostingCount. </summary>
		PostingCount,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InvoiceType.</summary>
	public enum InvoiceTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Invoicing.</summary>
	public enum InvoicingFieldIndex
	{
		///<summary>AccountingDueDate. </summary>
		AccountingDueDate,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CompletionTime. </summary>
		CompletionTime,
		///<summary>FormIds. </summary>
		FormIds,
		///<summary>InvoicesCount. </summary>
		InvoicesCount,
		///<summary>InvoiceTotalAmount. </summary>
		InvoiceTotalAmount,
		///<summary>InvoicingFilterType. </summary>
		InvoicingFilterType,
		///<summary>InvoicingID. </summary>
		InvoicingID,
		///<summary>InvoicingType. </summary>
		InvoicingType,
		///<summary>IsPseudoInvoicing. </summary>
		IsPseudoInvoicing,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RawInvoicingResultData. </summary>
		RawInvoicingResultData,
		///<summary>RequestTime. </summary>
		RequestTime,
		///<summary>SepaFilePath. </summary>
		SepaFilePath,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserID. </summary>
		UserID,
		///<summary>ValueDateTime. </summary>
		ValueDateTime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InvoicingFilterType.</summary>
	public enum InvoicingFilterTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InvoicingType.</summary>
	public enum InvoicingTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Job.</summary>
	public enum JobFieldIndex
	{
		///<summary>BulkImportID. </summary>
		BulkImportID,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreateTime. </summary>
		CreateTime,
		///<summary>Data. </summary>
		Data,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>ExceptionMessage. </summary>
		ExceptionMessage,
		///<summary>HasInputFile. </summary>
		HasInputFile,
		///<summary>HasOutputFile. </summary>
		HasOutputFile,
		///<summary>InvoicingID. </summary>
		InvoicingID,
		///<summary>JobID. </summary>
		JobID,
		///<summary>JobState. </summary>
		JobState,
		///<summary>JobType. </summary>
		JobType,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageID. </summary>
		MessageID,
		///<summary>OrderDetailID. </summary>
		OrderDetailID,
		///<summary>OutputMessages. </summary>
		OutputMessages,
		///<summary>PrinterID. </summary>
		PrinterID,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobBulkImport.</summary>
	public enum JobBulkImportFieldIndex
	{
		///<summary>BulkImportID. </summary>
		BulkImportID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>FareMediaTypeID. </summary>
		FareMediaTypeID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Path. </summary>
		Path,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobCardImport.</summary>
	public enum JobCardImportFieldIndex
	{
		///<summary>BatchNumber. </summary>
		BatchNumber,
		///<summary>CardCount. </summary>
		CardCount,
		///<summary>CardPhysicalDetailID. </summary>
		CardPhysicalDetailID,
		///<summary>FilePath. </summary>
		FilePath,
		///<summary>JobCardImportID. </summary>
		JobCardImportID,
		///<summary>JobID. </summary>
		JobID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>StorageLocationID. </summary>
		StorageLocationID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobFileType.</summary>
	public enum JobFileTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobState.</summary>
	public enum JobStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: JobType.</summary>
	public enum JobTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LookupAddress.</summary>
	public enum LookupAddressFieldIndex
	{
		///<summary>AddressField1. </summary>
		AddressField1,
		///<summary>AddressField2. </summary>
		AddressField2,
		///<summary>City. </summary>
		City,
		///<summary>Country. </summary>
		Country,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LookupAddressID. </summary>
		LookupAddressID,
		///<summary>PostalCode. </summary>
		PostalCode,
		///<summary>Region. </summary>
		Region,
		///<summary>Street. </summary>
		Street,
		///<summary>StreetNumber. </summary>
		StreetNumber,
		///<summary>StreetNumberTo. </summary>
		StreetNumberTo,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaEligibilityRequirement.</summary>
	public enum MediaEligibilityRequirementFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaSalePrecondition.</summary>
	public enum MediaSalePreconditionFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Merchant.</summary>
	public enum MerchantFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>ExternalID. </summary>
		ExternalID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MerchantID. </summary>
		MerchantID,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Message.</summary>
	public enum MessageFieldIndex
	{
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>InternalData. </summary>
		InternalData,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MessageContent. </summary>
		MessageContent,
		///<summary>MessageID. </summary>
		MessageID,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>Recipient. </summary>
		Recipient,
		///<summary>RecipientGroupID. </summary>
		RecipientGroupID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageFormat.</summary>
	public enum MessageFormatFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageType.</summary>
	public enum MessageTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		///<summary>MessageFormat. </summary>
		MessageFormat,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MobilityProvider.</summary>
	public enum MobilityProviderFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>DrivingLicenceRequired. </summary>
		DrivingLicenceRequired,
		///<summary>FixedPriceProvider. </summary>
		FixedPriceProvider,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MobilityProviderID. </summary>
		MobilityProviderID,
		///<summary>MobilityProviderNumber. </summary>
		MobilityProviderNumber,
		///<summary>ProviderName. </summary>
		ProviderName,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NotificationDevice.</summary>
	public enum NotificationDeviceFieldIndex
	{
		///<summary>AuthenticationToken. </summary>
		AuthenticationToken,
		///<summary>DeviceID. </summary>
		DeviceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NotificationDeviceID. </summary>
		NotificationDeviceID,
		///<summary>PushToken. </summary>
		PushToken,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NotificationMessage.</summary>
	public enum NotificationMessageFieldIndex
	{
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>EmailEventID. </summary>
		EmailEventID,
		///<summary>FormID. </summary>
		FormID,
		///<summary>HtmlFormat. </summary>
		HtmlFormat,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Message. </summary>
		Message,
		///<summary>NotificationMessageID. </summary>
		NotificationMessageID,
		///<summary>Subject. </summary>
		Subject,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VisibleFrom. </summary>
		VisibleFrom,
		///<summary>VisibleTo. </summary>
		VisibleTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NotificationMessageOwner.</summary>
	public enum NotificationMessageOwnerFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>IsAcknowledged. </summary>
		IsAcknowledged,
		///<summary>IsDeleted. </summary>
		IsDeleted,
		///<summary>NotificationMessageID. </summary>
		NotificationMessageID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NotificationMessageToCustomer.</summary>
	public enum NotificationMessageToCustomerFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>IsAcknowledged. </summary>
		IsAcknowledged,
		///<summary>IsDeleted. </summary>
		IsDeleted,
		///<summary>NotificationMessageID. </summary>
		NotificationMessageID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NumberGroup.</summary>
	public enum NumberGroupFieldIndex
	{
		///<summary>CurrentValue. </summary>
		CurrentValue,
		///<summary>Description. </summary>
		Description,
		///<summary>Format. </summary>
		Format,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NumberGroupID. </summary>
		NumberGroupID,
		///<summary>ResetAtEndOfMonth. </summary>
		ResetAtEndOfMonth,
		///<summary>ResetAtEndOfYear. </summary>
		ResetAtEndOfYear,
		///<summary>Scope. </summary>
		Scope,
		///<summary>StartValue. </summary>
		StartValue,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NumberGroupRandomized.</summary>
	public enum NumberGroupRandomizedFieldIndex
	{
		///<summary>IsUsed. </summary>
		IsUsed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NumberGroupRandomizedID. </summary>
		NumberGroupRandomizedID,
		///<summary>RandomizedValue. </summary>
		RandomizedValue,
		///<summary>RandomSortNumber. </summary>
		RandomSortNumber,
		///<summary>Scope. </summary>
		Scope,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NumberGroupScope.</summary>
	public enum NumberGroupScopeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Order.</summary>
	public enum OrderFieldIndex
	{
		///<summary>Assignee. </summary>
		Assignee,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>FulfillDate. </summary>
		FulfillDate,
		///<summary>InvoiceAddressID. </summary>
		InvoiceAddressID,
		///<summary>IsClosed. </summary>
		IsClosed,
		///<summary>IsMail. </summary>
		IsMail,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderDate. </summary>
		OrderDate,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>OrderSource. </summary>
		OrderSource,
		///<summary>Ordertype. </summary>
		Ordertype,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>PaymentProviderToken. </summary>
		PaymentProviderToken,
		///<summary>Priority. </summary>
		Priority,
		///<summary>SchoolYearID. </summary>
		SchoolYearID,
		///<summary>ShippingAddressID. </summary>
		ShippingAddressID,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VoucherNumber. </summary>
		VoucherNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderDetail.</summary>
	public enum OrderDetailFieldIndex
	{
		///<summary>CardAssociationType. </summary>
		CardAssociationType,
		///<summary>CardPrintingType. </summary>
		CardPrintingType,
		///<summary>FulfilledQuantity. </summary>
		FulfilledQuantity,
		///<summary>HasSecondaryPrint. </summary>
		HasSecondaryPrint,
		///<summary>IsCardHolderFinanced. </summary>
		IsCardHolderFinanced,
		///<summary>IsProcessed. </summary>
		IsProcessed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderDetailID. </summary>
		OrderDetailID,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>PaymentID. </summary>
		PaymentID,
		///<summary>PaymentIntervalID. </summary>
		PaymentIntervalID,
		///<summary>ProcessingEndDate. </summary>
		ProcessingEndDate,
		///<summary>ProcessingStartDate. </summary>
		ProcessingStartDate,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>ReplacementCardID. </summary>
		ReplacementCardID,
		///<summary>RequiredOrderDetailID. </summary>
		RequiredOrderDetailID,
		///<summary>SchoolyearID. </summary>
		SchoolyearID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderDetailToCard.</summary>
	public enum OrderDetailToCardFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>JobID. </summary>
		JobID,
		///<summary>OrderDetailID. </summary>
		OrderDetailID,
		///<summary>OrderDetailToCardID. </summary>
		OrderDetailToCardID,
		///<summary>ReplacedOrderDetailToCardID. </summary>
		ReplacedOrderDetailToCardID,
		///<summary>Status. </summary>
		Status,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderDetailToCardHolder.</summary>
	public enum OrderDetailToCardHolderFieldIndex
	{
		///<summary>CardHolderID. </summary>
		CardHolderID,
		///<summary>OrderDetailID. </summary>
		OrderDetailID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderHistory.</summary>
	public enum OrderHistoryFieldIndex
	{
		///<summary>LastCustomer. </summary>
		LastCustomer,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Message. </summary>
		Message,
		///<summary>OrderHistoryID. </summary>
		OrderHistoryID,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderProcessingCleanup.</summary>
	public enum OrderProcessingCleanupFieldIndex
	{
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderCleanupID. </summary>
		OrderCleanupID,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>OrderReset. </summary>
		OrderReset,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderState.</summary>
	public enum OrderStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderType.</summary>
	public enum OrderTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderWithTotal.</summary>
	public enum OrderWithTotalFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ExternalOrderNumber. </summary>
		ExternalOrderNumber,
		///<summary>FulfillDate. </summary>
		FulfillDate,
		///<summary>InvoiceAddressID. </summary>
		InvoiceAddressID,
		///<summary>IsMail. </summary>
		IsMail,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderDate. </summary>
		OrderDate,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>OrderSource. </summary>
		OrderSource,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>ShippingAddressID. </summary>
		ShippingAddressID,
		///<summary>State. </summary>
		State,
		///<summary>Total. </summary>
		Total,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VoucherNumber. </summary>
		VoucherNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderWorkItem.</summary>
	public enum OrderWorkItemFieldIndex
	{
		///<summary>Assigned. Inherited from WorkItem</summary>
		Assigned,
		///<summary>Assignee. Inherited from WorkItem</summary>
		Assignee,
		///<summary>Created. Inherited from WorkItem</summary>
		Created,
		///<summary>CreatedBy. Inherited from WorkItem</summary>
		CreatedBy,
		///<summary>Executed. Inherited from WorkItem</summary>
		Executed,
		///<summary>LastModified. Inherited from WorkItem</summary>
		LastModified,
		///<summary>LastUser. Inherited from WorkItem</summary>
		LastUser,
		///<summary>Memo. Inherited from WorkItem</summary>
		Memo,
		///<summary>Source. Inherited from WorkItem</summary>
		Source,
		///<summary>State. Inherited from WorkItem</summary>
		State,
		///<summary>Title. Inherited from WorkItem</summary>
		Title,
		///<summary>TransactionCounter. Inherited from WorkItem</summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. Inherited from WorkItem</summary>
		TypeDiscriminator,
		///<summary>WorkItemID. Inherited from WorkItem</summary>
		WorkItemID,
		///<summary>WorkItemSubjectID. Inherited from WorkItem</summary>
		WorkItemSubjectID,
		///<summary>OrderID. </summary>
		OrderID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Organization.</summary>
	public enum OrganizationFieldIndex
	{
		///<summary>Abbreviation. </summary>
		Abbreviation,
		///<summary>ContactPersonID. </summary>
		ContactPersonID,
		///<summary>Description. </summary>
		Description,
		///<summary>Discount. </summary>
		Discount,
		///<summary>ExternalNumber. </summary>
		ExternalNumber,
		///<summary>ExternalType. </summary>
		ExternalType,
		///<summary>FrameOrganizationID. </summary>
		FrameOrganizationID,
		///<summary>HasProductPool. </summary>
		HasProductPool,
		///<summary>IncludeInExport. </summary>
		IncludeInExport,
		///<summary>IsCardOwner. </summary>
		IsCardOwner,
		///<summary>IsPreTaxEnabled. </summary>
		IsPreTaxEnabled,
		///<summary>IsTrusted. </summary>
		IsTrusted,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>OrdererNumber. </summary>
		OrdererNumber,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>OrganizationNumber. </summary>
		OrganizationNumber,
		///<summary>OrganizationTypeID. </summary>
		OrganizationTypeID,
		///<summary>PrintName. </summary>
		PrintName,
		///<summary>PurchaseOrderNumber. </summary>
		PurchaseOrderNumber,
		///<summary>SubType. </summary>
		SubType,
		///<summary>TaxIDNumber. </summary>
		TaxIDNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrganizationAddress.</summary>
	public enum OrganizationAddressFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>AddressType. </summary>
		AddressType,
		///<summary>Created. </summary>
		Created,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrganizationParticipant.</summary>
	public enum OrganizationParticipantFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>CellPhoneNumber. </summary>
		CellPhoneNumber,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DateOfBirth. </summary>
		DateOfBirth,
		///<summary>Email. </summary>
		Email,
		///<summary>FaxNumber. </summary>
		FaxNumber,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>Gender. </summary>
		Gender,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastName. </summary>
		LastName,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationParticipantID. </summary>
		OrganizationParticipantID,
		///<summary>ParticipantGroupID. </summary>
		ParticipantGroupID,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>PhotographID. </summary>
		PhotographID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrganizationSubtype.</summary>
	public enum OrganizationSubtypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrganizationType.</summary>
	public enum OrganizationTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Literal. </summary>
		Literal,
		///<summary>OrganizationTypeID. </summary>
		OrganizationTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageContent.</summary>
	public enum PageContentFieldIndex
	{
		///<summary>ContentType. </summary>
		ContentType,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PageContentID. </summary>
		PageContentID,
		///<summary>Region. </summary>
		Region,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Parameter.</summary>
	public enum ParameterFieldIndex
	{
		///<summary>DefaultValue. </summary>
		DefaultValue,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MaxValue. </summary>
		MaxValue,
		///<summary>MinValue. </summary>
		MinValue,
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterNumber. </summary>
		ParameterNumber,
		///<summary>ParameterType. </summary>
		ParameterType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Unit. </summary>
		Unit,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterArchive.</summary>
	public enum ParameterArchiveFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>ParameterArchiveID. </summary>
		ParameterArchiveID,
		///<summary>ReleaseCounter. </summary>
		ReleaseCounter,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterArchiveRelease.</summary>
	public enum ParameterArchiveReleaseFieldIndex
	{
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ParameterArchiveID. </summary>
		ParameterArchiveID,
		///<summary>ParameterArchiveReleaseID. </summary>
		ParameterArchiveReleaseID,
		///<summary>ReleaseDateTime. </summary>
		ReleaseDateTime,
		///<summary>ReleaseType. </summary>
		ReleaseType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterArchiveResultView.</summary>
	public enum ParameterArchiveResultViewFieldIndex
	{
		///<summary>DefaultValue. </summary>
		DefaultValue,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>MountingPlateIndex. </summary>
		MountingPlateIndex,
		///<summary>ParameterArchiveID. </summary>
		ParameterArchiveID,
		///<summary>ParameterDescription. </summary>
		ParameterDescription,
		///<summary>ParameterNumber. </summary>
		ParameterNumber,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>UnitCollectionName. </summary>
		UnitCollectionName,
		///<summary>UnitName. </summary>
		UnitName,
		///<summary>UnitNumber. </summary>
		UnitNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterGroup.</summary>
	public enum ParameterGroupFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ParameterGroupID. </summary>
		ParameterGroupID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterGroupToParameter.</summary>
	public enum ParameterGroupToParameterFieldIndex
	{
		///<summary>DefaultValueOverride. </summary>
		DefaultValueOverride,
		///<summary>ParameterGroupID. </summary>
		ParameterGroupID,
		///<summary>ParameterID. </summary>
		ParameterID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterType.</summary>
	public enum ParameterTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParameterValue.</summary>
	public enum ParameterValueFieldIndex
	{
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MountingPlateIndex. </summary>
		MountingPlateIndex,
		///<summary>ParameterArchiveID. </summary>
		ParameterArchiveID,
		///<summary>ParameterID. </summary>
		ParameterID,
		///<summary>ParameterValue. </summary>
		ParameterValue,
		///<summary>ParameterValueID. </summary>
		ParameterValueID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UnitCollectionID. </summary>
		UnitCollectionID,
		///<summary>UnitID. </summary>
		UnitID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParaTransAttribute.</summary>
	public enum ParaTransAttributeFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>ExternalTripID. </summary>
		ExternalTripID,
		///<summary>LastModifier. </summary>
		LastModifier,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ParaTransAttributeID. </summary>
		ParaTransAttributeID,
		///<summary>ParaTransAttributeTypeID. </summary>
		ParaTransAttributeTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParaTransAttributeType.</summary>
	public enum ParaTransAttributeTypeFieldIndex
	{
		///<summary>Cost. </summary>
		Cost,
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParaTransJournal.</summary>
	public enum ParaTransJournalFieldIndex
	{
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>ExternalRiderID. </summary>
		ExternalRiderID,
		///<summary>ExternalTripID. </summary>
		ExternalTripID,
		///<summary>Faremeter. </summary>
		Faremeter,
		///<summary>FareTransactionID. </summary>
		FareTransactionID,
		///<summary>LastModifier. </summary>
		LastModifier,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>Odometer. </summary>
		Odometer,
		///<summary>OperatorCompanyID. </summary>
		OperatorCompanyID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>ParaTransJournalID. </summary>
		ParaTransJournalID,
		///<summary>ParaTransJournalType. </summary>
		ParaTransJournalType,
		///<summary>ParaTransPaymentType. </summary>
		ParaTransPaymentType,
		///<summary>SharedRideID. </summary>
		SharedRideID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VehicleNumber. </summary>
		VehicleNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParaTransJournalType.</summary>
	public enum ParaTransJournalTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParaTransPaymentType.</summary>
	public enum ParaTransPaymentTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ParticipantGroup.</summary>
	public enum ParticipantGroupFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>GroupDescription. </summary>
		GroupDescription,
		///<summary>GroupName. </summary>
		GroupName,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ParticipantGroupID. </summary>
		ParticipantGroupID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PassExpiryNotification.</summary>
	public enum PassExpiryNotificationFieldIndex
	{
		///<summary>Days. </summary>
		Days,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NotificationDate. </summary>
		NotificationDate,
		///<summary>PassExpiryNotificationID. </summary>
		PassExpiryNotificationID,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Payment.</summary>
	public enum PaymentFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>Description. </summary>
		Description,
		///<summary>ExecutionResult. </summary>
		ExecutionResult,
		///<summary>ExecutionTime. </summary>
		ExecutionTime,
		///<summary>FullResult. </summary>
		FullResult,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentID. </summary>
		PaymentID,
		///<summary>ReceiptNumber. </summary>
		ReceiptNumber,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionOrderNumber. </summary>
		TransactionOrderNumber,
		///<summary>UsedGUID. </summary>
		UsedGUID,
		///<summary>UsedToken. </summary>
		UsedToken,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentJournal.</summary>
	public enum PaymentJournalFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CashReceived. </summary>
		CashReceived,
		///<summary>CashReturn. </summary>
		CashReturn,
		///<summary>CheckBranchRouting. </summary>
		CheckBranchRouting,
		///<summary>CheckCustomerIdentifier. </summary>
		CheckCustomerIdentifier,
		///<summary>CheckingAccount. </summary>
		CheckingAccount,
		///<summary>CheckNumber. </summary>
		CheckNumber,
		///<summary>Code. </summary>
		Code,
		///<summary>Confirmed. </summary>
		Confirmed,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreditCardNetwork. </summary>
		CreditCardNetwork,
		///<summary>EncryptedData. </summary>
		EncryptedData,
		///<summary>EncryptedDataType. </summary>
		EncryptedDataType,
		///<summary>ExecutionResult. </summary>
		ExecutionResult,
		///<summary>ExecutionTime. </summary>
		ExecutionTime,
		///<summary>IsPreTax. </summary>
		IsPreTax,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MaskedCheckingAccount. </summary>
		MaskedCheckingAccount,
		///<summary>MaskedPan. </summary>
		MaskedPan,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>PaymentType. </summary>
		PaymentType,
		///<summary>PayPalAuthorizationID. </summary>
		PayPalAuthorizationID,
		///<summary>PayPalCaptureID. </summary>
		PayPalCaptureID,
		///<summary>PayPalCaptureRequestID. </summary>
		PayPalCaptureRequestID,
		///<summary>PaypalCaptureToken. </summary>
		PaypalCaptureToken,
		///<summary>PayPalRequestID. </summary>
		PayPalRequestID,
		///<summary>PayPalRequestToken. </summary>
		PayPalRequestToken,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>ProviderReference. </summary>
		ProviderReference,
		///<summary>ProviderReferenceExternal. </summary>
		ProviderReferenceExternal,
		///<summary>PurchaseOrderNumber. </summary>
		PurchaseOrderNumber,
		///<summary>ReconciliationState. </summary>
		ReconciliationState,
		///<summary>ReferenceNumber. </summary>
		ReferenceNumber,
		///<summary>SaleID. </summary>
		SaleID,
		///<summary>State. </summary>
		State,
		///<summary>Token. </summary>
		Token,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>WalletTransactionID. </summary>
		WalletTransactionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentJournalToComponent.</summary>
	public enum PaymentJournalToComponentFieldIndex
	{
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>PaymentjournalID. </summary>
		PaymentjournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentMethod.</summary>
	public enum PaymentMethodFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentModality.</summary>
	public enum PaymentModalityFieldIndex
	{
		///<summary>DevicePaymentMethod. </summary>
		DevicePaymentMethod,
		///<summary>Frequency. </summary>
		Frequency,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentModalityID. </summary>
		PaymentModalityID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentOption.</summary>
	public enum PaymentOptionFieldIndex
	{
		///<summary>BankConnectionDataID. </summary>
		BankConnectionDataID,
		///<summary>BankingServiceToken. </summary>
		BankingServiceToken,
		///<summary>BlockingComment. </summary>
		BlockingComment,
		///<summary>BlockingDate. </summary>
		BlockingDate,
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreditCardNetwork. </summary>
		CreditCardNetwork,
		///<summary>Description. </summary>
		Description,
		///<summary>Expiry. </summary>
		Expiry,
		///<summary>IsBlocked. </summary>
		IsBlocked,
		///<summary>IsRetired. </summary>
		IsRetired,
		///<summary>IsTemporary. </summary>
		IsTemporary,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>PaymentMethod. </summary>
		PaymentMethod,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>Priority. </summary>
		Priority,
		///<summary>ProspectivePaymentOptionID. </summary>
		ProspectivePaymentOptionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentProvider.</summary>
	public enum PaymentProviderFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentProviderAccount.</summary>
	public enum PaymentProviderAccountFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentProviderAccountID. </summary>
		PaymentProviderAccountID,
		///<summary>PaymentProviderReference. </summary>
		PaymentProviderReference,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentRecognition.</summary>
	public enum PaymentRecognitionFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentRecognitionID. </summary>
		PaymentRecognitionID,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentReconciliation.</summary>
	public enum PaymentReconciliationFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>PaymentReconciliationID. </summary>
		PaymentReconciliationID,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Reconciled. </summary>
		Reconciled,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentReconciliationState.</summary>
	public enum PaymentReconciliationStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentState.</summary>
	public enum PaymentStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PendingOrder.</summary>
	public enum PendingOrderFieldIndex
	{
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>IsProcessed. </summary>
		IsProcessed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrderComplete. </summary>
		OrderComplete,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>PaymentSuccess. </summary>
		PaymentSuccess,
		///<summary>PendingOrderID. </summary>
		PendingOrderID,
		///<summary>RetryCount. </summary>
		RetryCount,
		///<summary>SessionID. </summary>
		SessionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Performance.</summary>
	public enum PerformanceFieldIndex
	{
		///<summary>Aggregation. </summary>
		Aggregation,
		///<summary>Description. </summary>
		Description,
		///<summary>Duration. </summary>
		Duration,
		///<summary>EventTime. </summary>
		EventTime,
		///<summary>IndicatorID. </summary>
		IndicatorID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>PerformanceID. </summary>
		PerformanceID,
		///<summary>Quantity. </summary>
		Quantity,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PerformanceAggregation.</summary>
	public enum PerformanceAggregationFieldIndex
	{
		///<summary>AggregationFrom. </summary>
		AggregationFrom,
		///<summary>AggregationFunction. </summary>
		AggregationFunction,
		///<summary>AggregationID. </summary>
		AggregationID,
		///<summary>AggregationTo. </summary>
		AggregationTo,
		///<summary>AggregationValue. </summary>
		AggregationValue,
		///<summary>Description. </summary>
		Description,
		///<summary>IndicatorID. </summary>
		IndicatorID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PerformanceComponent.</summary>
	public enum PerformanceComponentFieldIndex
	{
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PerformanceIndicator.</summary>
	public enum PerformanceIndicatorFieldIndex
	{
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>IndicatorID. </summary>
		IndicatorID,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Person.</summary>
	public enum PersonFieldIndex
	{
		///<summary>AcceptTermsAndConditions. </summary>
		AcceptTermsAndConditions,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>Authenticated. </summary>
		Authenticated,
		///<summary>CellPhoneNumber. </summary>
		CellPhoneNumber,
		///<summary>DataProtectionNoticeAccepted. </summary>
		DataProtectionNoticeAccepted,
		///<summary>DateOfBirth. </summary>
		DateOfBirth,
		///<summary>Email. </summary>
		Email,
		///<summary>FaxNumber. </summary>
		FaxNumber,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>Gender. </summary>
		Gender,
		///<summary>HasDrivingLicense. </summary>
		HasDrivingLicense,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>IsCellPhoneNumberConfirmed. </summary>
		IsCellPhoneNumberConfirmed,
		///<summary>IsEmailConfirmed. </summary>
		IsEmailConfirmed,
		///<summary>IsEmailEnabled. </summary>
		IsEmailEnabled,
		///<summary>IsNewsletterAccepted. </summary>
		IsNewsletterAccepted,
		///<summary>JobTitle. </summary>
		JobTitle,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastName. </summary>
		LastName,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MiddleName. </summary>
		MiddleName,
		///<summary>OrganizationalIdentifier. </summary>
		OrganizationalIdentifier,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>Salutation. </summary>
		Salutation,
		///<summary>Title. </summary>
		Title,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. </summary>
		TypeDiscriminator,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PersonAddress.</summary>
	public enum PersonAddressFieldIndex
	{
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>AddressType. </summary>
		AddressType,
		///<summary>Created. </summary>
		Created,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Photograph.</summary>
	public enum PhotographFieldIndex
	{
		///<summary>Caption. </summary>
		Caption,
		///<summary>Contractid. </summary>
		Contractid,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>PhotoFile. </summary>
		PhotoFile,
		///<summary>PhotographID. </summary>
		PhotographID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PickupLocation.</summary>
	public enum PickupLocationFieldIndex
	{
		///<summary>AddressField1. </summary>
		AddressField1,
		///<summary>AddressField2. </summary>
		AddressField2,
		///<summary>City. </summary>
		City,
		///<summary>ContactPerson. </summary>
		ContactPerson,
		///<summary>Coordinates. </summary>
		Coordinates,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationName. </summary>
		OrganizationName,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>PickupLocationID. </summary>
		PickupLocationID,
		///<summary>PostalCode. </summary>
		PostalCode,
		///<summary>Region. </summary>
		Region,
		///<summary>Street. </summary>
		Street,
		///<summary>StreetNumber. </summary>
		StreetNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PickupLocationToTicket.</summary>
	public enum PickupLocationToTicketFieldIndex
	{
		///<summary>InternalTicketNumber. </summary>
		InternalTicketNumber,
		///<summary>PickupLocationID. </summary>
		PickupLocationID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posting.</summary>
	public enum PostingFieldIndex
	{
		///<summary>AccountSettlementId. </summary>
		AccountSettlementId,
		///<summary>CancelPostingID. </summary>
		CancelPostingID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Description. </summary>
		Description,
		///<summary>InvoiceEntryID. </summary>
		InvoiceEntryID,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PostingAmount. </summary>
		PostingAmount,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingID. </summary>
		PostingID,
		///<summary>PostingKeyID. </summary>
		PostingKeyID,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ReceiptNumber. </summary>
		ReceiptNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserID. </summary>
		UserID,
		///<summary>ValueDateTime. </summary>
		ValueDateTime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PostingKey.</summary>
	public enum PostingKeyFieldIndex
	{
		///<summary>CancelKey. </summary>
		CancelKey,
		///<summary>Category. </summary>
		Category,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>IsBillingRelevant. </summary>
		IsBillingRelevant,
		///<summary>IsCredit. </summary>
		IsCredit,
		///<summary>IsManuallyBookable. </summary>
		IsManuallyBookable,
		///<summary>IsVisibleInAccount. </summary>
		IsVisibleInAccount,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Liability. </summary>
		Liability,
		///<summary>PostingKey. </summary>
		PostingKey,
		///<summary>PostingKeyID. </summary>
		PostingKeyID,
		///<summary>PostingKeyTypeID. </summary>
		PostingKeyTypeID,
		///<summary>PostingScope. </summary>
		PostingScope,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PostingKeyCategory.</summary>
	public enum PostingKeyCategoryFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PostingScope.</summary>
	public enum PostingScopeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceAdjustment.</summary>
	public enum PriceAdjustmentFieldIndex
	{
		///<summary>IsPercentage. </summary>
		IsPercentage,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>PriceAdjustment. </summary>
		PriceAdjustment,
		///<summary>PriceAdjustmentID. </summary>
		PriceAdjustmentID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceAdjustmentTemplate.</summary>
	public enum PriceAdjustmentTemplateFieldIndex
	{
		///<summary>IsPercentage. </summary>
		IsPercentage,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationTypeID. </summary>
		OrganizationTypeID,
		///<summary>PriceAdjustment. </summary>
		PriceAdjustment,
		///<summary>PriceAdjustmentTemplateID. </summary>
		PriceAdjustmentTemplateID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Printer.</summary>
	public enum PrinterFieldIndex
	{
		///<summary>Capacity. </summary>
		Capacity,
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayName. </summary>
		DisplayName,
		///<summary>GUID. </summary>
		GUID,
		///<summary>IsDisabled. </summary>
		IsDisabled,
		///<summary>IsPaused. </summary>
		IsPaused,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastResponse. </summary>
		LastResponse,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ManufacturerDesignation. </summary>
		ManufacturerDesignation,
		///<summary>Name. </summary>
		Name,
		///<summary>PrinterID. </summary>
		PrinterID,
		///<summary>PrinterState. </summary>
		PrinterState,
		///<summary>PrinterType. </summary>
		PrinterType,
		///<summary>RegistrationTime. </summary>
		RegistrationTime,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PrinterState.</summary>
	public enum PrinterStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PrinterToUnit.</summary>
	public enum PrinterToUnitFieldIndex
	{
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>PrinterID. </summary>
		PrinterID,
		///<summary>UnitID. </summary>
		UnitID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PrinterType.</summary>
	public enum PrinterTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Priority.</summary>
	public enum PriorityFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Product.</summary>
	public enum ProductFieldIndex
	{
		///<summary>BlockingDate. </summary>
		BlockingDate,
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>BlockingReasonEnum. </summary>
		BlockingReasonEnum,
		///<summary>CancellationDate. </summary>
		CancellationDate,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Distance. </summary>
		Distance,
		///<summary>Expiry. </summary>
		Expiry,
		///<summary>ExportState. </summary>
		ExportState,
		///<summary>FillLevel. </summary>
		FillLevel,
		///<summary>GroupSize. </summary>
		GroupSize,
		///<summary>InstanceNumber. </summary>
		InstanceNumber,
		///<summary>IsBlocked. </summary>
		IsBlocked,
		///<summary>IsCanceled. </summary>
		IsCanceled,
		///<summary>IsCleared. </summary>
		IsCleared,
		///<summary>IsLoaded. </summary>
		IsLoaded,
		///<summary>IsPaid. </summary>
		IsPaid,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MinimumTerm. </summary>
		MinimumTerm,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>PointOfSaleID. </summary>
		PointOfSaleID,
		///<summary>Price. </summary>
		Price,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ReferencedProductID. </summary>
		ReferencedProductID,
		///<summary>RefundID. </summary>
		RefundID,
		///<summary>RelationFrom. </summary>
		RelationFrom,
		///<summary>RelationTo. </summary>
		RelationTo,
		///<summary>RemainingValue. </summary>
		RemainingValue,
		///<summary>SubValidFrom. </summary>
		SubValidFrom,
		///<summary>SubValidTo. </summary>
		SubValidTo,
		///<summary>TaxLocationcode. </summary>
		TaxLocationcode,
		///<summary>TaxTicketID. </summary>
		TaxTicketID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketType. </summary>
		TicketType,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TripSerialNumber. </summary>
		TripSerialNumber,
		///<summary>ValidateWhenSelling. </summary>
		ValidateWhenSelling,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>VdvEntitlementData. </summary>
		VdvEntitlementData,
		///<summary>VdvEntNumber. </summary>
		VdvEntNumber,
		///<summary>VoucherID. </summary>
		VoucherID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductRelation.</summary>
	public enum ProductRelationFieldIndex
	{
		///<summary>Eav. </summary>
		Eav,
		///<summary>FareMatrixEntryPriority. </summary>
		FareMatrixEntryPriority,
		///<summary>FareStage. </summary>
		FareStage,
		///<summary>FromAddressID. </summary>
		FromAddressID,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ProductRelationID. </summary>
		ProductRelationID,
		///<summary>RelationFrom. </summary>
		RelationFrom,
		///<summary>RelationNumber. </summary>
		RelationNumber,
		///<summary>RelationTo. </summary>
		RelationTo,
		///<summary>RelationType. </summary>
		RelationType,
		///<summary>RelationVia. </summary>
		RelationVia,
		///<summary>StopNumber. </summary>
		StopNumber,
		///<summary>ToAddressID. </summary>
		ToAddressID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductSubsidy.</summary>
	public enum ProductSubsidyFieldIndex
	{
		///<summary>IsPercentage. </summary>
		IsPercentage,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>ProductSubsidyID. </summary>
		ProductSubsidyID,
		///<summary>Subsidy. </summary>
		Subsidy,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductTermination.</summary>
	public enum ProductTerminationFieldIndex
	{
		///<summary>BankingCost. </summary>
		BankingCost,
		///<summary>CapReached. </summary>
		CapReached,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DemandDate. </summary>
		DemandDate,
		///<summary>Description. </summary>
		Description,
		///<summary>DifferenceCost. </summary>
		DifferenceCost,
		///<summary>EmployeeID. </summary>
		EmployeeID,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MonthinUse. </summary>
		MonthinUse,
		///<summary>ProcessingCost. </summary>
		ProcessingCost,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ProductTerminationID. </summary>
		ProductTerminationID,
		///<summary>ProductTerminationTypeID. </summary>
		ProductTerminationTypeID,
		///<summary>ReferencedTicketID. </summary>
		ReferencedTicketID,
		///<summary>TerminationDate. </summary>
		TerminationDate,
		///<summary>TerminationStatus. </summary>
		TerminationStatus,
		///<summary>TotalCost. </summary>
		TotalCost,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductTerminationType.</summary>
	public enum ProductTerminationTypeFieldIndex
	{
		///<summary>AddDifferenceCost. </summary>
		AddDifferenceCost,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>DunningCost. </summary>
		DunningCost,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>PaymentDueDays. </summary>
		PaymentDueDays,
		///<summary>ProcessingCost. </summary>
		ProcessingCost,
		///<summary>ProductTerminationTypeID. </summary>
		ProductTerminationTypeID,
		///<summary>TaxCost. </summary>
		TaxCost,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductType.</summary>
	public enum ProductTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PromoCode.</summary>
	public enum PromoCodeFieldIndex
	{
		///<summary>BeginDate. </summary>
		BeginDate,
		///<summary>CampaignDescription. </summary>
		CampaignDescription,
		///<summary>CreationDate. </summary>
		CreationDate,
		///<summary>CustomerID. </summary>
		CustomerID,
		///<summary>Destination. </summary>
		Destination,
		///<summary>Discount. </summary>
		Discount,
		///<summary>DiscountType. </summary>
		DiscountType,
		///<summary>EndDate. </summary>
		EndDate,
		///<summary>LastGetTime. </summary>
		LastGetTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Origin. </summary>
		Origin,
		///<summary>PromoCodeID. </summary>
		PromoCodeID,
		///<summary>PromoCodeNumber. </summary>
		PromoCodeNumber,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>RedemptionDate. </summary>
		RedemptionDate,
		///<summary>Status. </summary>
		Status,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PromoCodeStatus.</summary>
	public enum PromoCodeStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProviderAccountState.</summary>
	public enum ProviderAccountStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProvisioningState.</summary>
	public enum ProvisioningStateFieldIndex
	{
		///<summary>AccountHash. </summary>
		AccountHash,
		///<summary>CorrelationID. </summary>
		CorrelationID,
		///<summary>LastModified. </summary>
		LastModified,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RecipientGroup.</summary>
	public enum RecipientGroupFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RecipientGroupID. </summary>
		RecipientGroupID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RecipientGroupMember.</summary>
	public enum RecipientGroupMemberFieldIndex
	{
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>RecipientGroupID. </summary>
		RecipientGroupID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Refund.</summary>
	public enum RefundFieldIndex
	{
		///<summary>Comment. </summary>
		Comment,
		///<summary>IsRelevantForPosting. </summary>
		IsRelevantForPosting,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Reason. </summary>
		Reason,
		///<summary>RefundID. </summary>
		RefundID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RefundReason.</summary>
	public enum RefundReasonFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RentalState.</summary>
	public enum RentalStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RentalType.</summary>
	public enum RentalTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Report.</summary>
	public enum ReportFieldIndex
	{
		///<summary>CategoryID. </summary>
		CategoryID,
		///<summary>DataFileID. </summary>
		DataFileID,
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayOrder. </summary>
		DisplayOrder,
		///<summary>FilterListID. </summary>
		FilterListID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>ReportID. </summary>
		ReportID,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportCategory.</summary>
	public enum ReportCategoryFieldIndex
	{
		///<summary>CategoryFilterListID. </summary>
		CategoryFilterListID,
		///<summary>Description. </summary>
		Description,
		///<summary>DisplayOrder. </summary>
		DisplayOrder,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>MasterCategoryID. </summary>
		MasterCategoryID,
		///<summary>Name. </summary>
		Name,
		///<summary>ReportCategoryID. </summary>
		ReportCategoryID,
		///<summary>ResourceID. </summary>
		ResourceID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportDataFile.</summary>
	public enum ReportDataFileFieldIndex
	{
		///<summary>DataVersion. </summary>
		DataVersion,
		///<summary>Description. </summary>
		Description,
		///<summary>FileName. </summary>
		FileName,
		///<summary>FileTypes. </summary>
		FileTypes,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ReportDataFileID. </summary>
		ReportDataFileID,
		///<summary>ReportID. </summary>
		ReportID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportJob.</summary>
	public enum ReportJobFieldIndex
	{
		///<summary>Active. </summary>
		Active,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CreationTime. </summary>
		CreationTime,
		///<summary>Description. </summary>
		Description,
		///<summary>EmailRecipient. </summary>
		EmailRecipient,
		///<summary>EnableNotification. </summary>
		EnableNotification,
		///<summary>FileType. </summary>
		FileType,
		///<summary>FilterValueSetID. </summary>
		FilterValueSetID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastRun. </summary>
		LastRun,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>NextRun. </summary>
		NextRun,
		///<summary>ReportID. </summary>
		ReportID,
		///<summary>ReportJobID. </summary>
		ReportJobID,
		///<summary>ScheduleRfc5545. </summary>
		ScheduleRfc5545,
		///<summary>State. </summary>
		State,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserID. </summary>
		UserID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportJobResult.</summary>
	public enum ReportJobResultFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>FileName. </summary>
		FileName,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ReportID. </summary>
		ReportID,
		///<summary>ReportJobID. </summary>
		ReportJobID,
		///<summary>ReportJobResultID. </summary>
		ReportJobResultID,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>Success. </summary>
		Success,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportToClient.</summary>
	public enum ReportToClientFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ConfigurationID. </summary>
		ConfigurationID,
		///<summary>ReportID. </summary>
		ReportID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevenueRecognition.</summary>
	public enum RevenueRecognitionFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>AppliedPassTicketID. </summary>
		AppliedPassTicketID,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>BoardingGuid. </summary>
		BoardingGuid,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>IncludeInSettlement. </summary>
		IncludeInSettlement,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RevenueSettlement.</summary>
	public enum RevenueSettlementFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>BaseFare. </summary>
		BaseFare,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>Premium. </summary>
		Premium,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		///<summary>SettlementDistributionPolicy. </summary>
		SettlementDistributionPolicy,
		///<summary>SettlementType. </summary>
		SettlementType,
		///<summary>TapsCount. </summary>
		TapsCount,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleViolation.</summary>
	public enum RuleViolationFieldIndex
	{
		///<summary>AggregationFrom. </summary>
		AggregationFrom,
		///<summary>AggregationTo. </summary>
		AggregationTo,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ExecutionTime. </summary>
		ExecutionTime,
		///<summary>IsResolved. </summary>
		IsResolved,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Message. </summary>
		Message,
		///<summary>PaymentOptionID. </summary>
		PaymentOptionID,
		///<summary>PaymentToken. </summary>
		PaymentToken,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>RuleViolationID. </summary>
		RuleViolationID,
		///<summary>RuleViolationProvider. </summary>
		RuleViolationProvider,
		///<summary>RuleViolationSourceType. </summary>
		RuleViolationSourceType,
		///<summary>RuleViolationType. </summary>
		RuleViolationType,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>ViolationValue. </summary>
		ViolationValue,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleViolationProvider.</summary>
	public enum RuleViolationProviderFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleViolationSourceType.</summary>
	public enum RuleViolationSourceTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleViolationToTransaction.</summary>
	public enum RuleViolationToTransactionFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PaymentJournalID. </summary>
		PaymentJournalID,
		///<summary>RuleViolationID. </summary>
		RuleViolationID,
		///<summary>RuleViolationToTransactionID. </summary>
		RuleViolationToTransactionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RuleViolationType.</summary>
	public enum RuleViolationTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Sale.</summary>
	public enum SaleFieldIndex
	{
		///<summary>CancellationReferenceID. </summary>
		CancellationReferenceID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>CreditCardAuthorizationID. </summary>
		CreditCardAuthorizationID,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>ExternalOrderNumber. </summary>
		ExternalOrderNumber,
		///<summary>IsClosed. </summary>
		IsClosed,
		///<summary>IsOrganizational. </summary>
		IsOrganizational,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LocationNumber. </summary>
		LocationNumber,
		///<summary>MerchantName. </summary>
		MerchantName,
		///<summary>MerchantNumber. </summary>
		MerchantNumber,
		///<summary>MerchantRetailer. </summary>
		MerchantRetailer,
		///<summary>NetworkReference. </summary>
		NetworkReference,
		///<summary>Notes. </summary>
		Notes,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>ReceiptReference. </summary>
		ReceiptReference,
		///<summary>RefundReferenceID. </summary>
		RefundReferenceID,
		///<summary>SaleDate. </summary>
		SaleDate,
		///<summary>SaleID. </summary>
		SaleID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>SalesPersonEmail. </summary>
		SalesPersonEmail,
		///<summary>SalesPersonNumber. </summary>
		SalesPersonNumber,
		///<summary>SaleTransactionID. </summary>
		SaleTransactionID,
		///<summary>SaleType. </summary>
		SaleType,
		///<summary>SecondaryContractID. </summary>
		SecondaryContractID,
		///<summary>ShiftInventoryID. </summary>
		ShiftInventoryID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SalesChannelToPaymentMethod.</summary>
	public enum SalesChannelToPaymentMethodFieldIndex
	{
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>Usage. </summary>
		Usage,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SalesRevenue.</summary>
	public enum SalesRevenueFieldIndex
	{
		///<summary>Amount. </summary>
		Amount,
		///<summary>CloseoutPeriodID. </summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber. </summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber. </summary>
		DebitAccountNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PostingDate. </summary>
		PostingDate,
		///<summary>PostingReference. </summary>
		PostingReference,
		///<summary>SalesRevenueID. </summary>
		SalesRevenueID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SaleToComponent.</summary>
	public enum SaleToComponentFieldIndex
	{
		///<summary>ComponentID. </summary>
		ComponentID,
		///<summary>SaleID. </summary>
		SaleID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SaleType.</summary>
	public enum SaleTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SamModule.</summary>
	public enum SamModuleFieldIndex
	{
		///<summary>BlockingReason. </summary>
		BlockingReason,
		///<summary>Counter. </summary>
		Counter,
		///<summary>DeviceID. </summary>
		DeviceID,
		///<summary>Info. </summary>
		Info,
		///<summary>IsBlocked. </summary>
		IsBlocked,
		///<summary>LastData. </summary>
		LastData,
		///<summary>LossDate. </summary>
		LossDate,
		///<summary>LossDetectionDate. </summary>
		LossDetectionDate,
		///<summary>SamID. </summary>
		SamID,
		///<summary>SamNumber. </summary>
		SamNumber,
		///<summary>Slot. </summary>
		Slot,
		///<summary>State. </summary>
		State,
		///<summary>Type. </summary>
		Type,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTil. </summary>
		ValidTil,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SamModuleStatus.</summary>
	public enum SamModuleStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SchoolYear.</summary>
	public enum SchoolYearFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>EndDate. </summary>
		EndDate,
		///<summary>IsActive. </summary>
		IsActive,
		///<summary>IsCurrentSchoolYear. </summary>
		IsCurrentSchoolYear,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NumberOfAccountingMonths. </summary>
		NumberOfAccountingMonths,
		///<summary>NumberOfMonths. </summary>
		NumberOfMonths,
		///<summary>PreviousSchoolYearID. </summary>
		PreviousSchoolYearID,
		///<summary>SchoolYearID. </summary>
		SchoolYearID,
		///<summary>SchoolYearName. </summary>
		SchoolYearName,
		///<summary>StartDate. </summary>
		StartDate,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SecurityQuestion.</summary>
	public enum SecurityQuestionFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SecurityQuestionID. </summary>
		SecurityQuestionID,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SecurityResponse.</summary>
	public enum SecurityResponseFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Response. </summary>
		Response,
		///<summary>SecurityQuestionID. </summary>
		SecurityQuestionID,
		///<summary>SecurityResponseID. </summary>
		SecurityResponseID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SepaFrequencyType.</summary>
	public enum SepaFrequencyTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SepaOwnerType.</summary>
	public enum SepaOwnerTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServerResponseTimeData.</summary>
	public enum ServerResponseTimeDataFieldIndex
	{
		///<summary>AggregationType. </summary>
		AggregationType,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>Duration. </summary>
		Duration,
		///<summary>EventTime. </summary>
		EventTime,
		///<summary>HostName. </summary>
		HostName,
		///<summary>IndicatorID. </summary>
		IndicatorID,
		///<summary>IsAggregated. </summary>
		IsAggregated,
		///<summary>NumberAboveThreshold. </summary>
		NumberAboveThreshold,
		///<summary>NumberOfAuthorizedTx. </summary>
		NumberOfAuthorizedTx,
		///<summary>PerformanceID. </summary>
		PerformanceID,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>ShiftBegin. </summary>
		ShiftBegin,
		///<summary>TotalNumber. </summary>
		TotalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServerResponseTimeDataAggregation.</summary>
	public enum ServerResponseTimeDataAggregationFieldIndex
	{
		///<summary>AggregationFrom. </summary>
		AggregationFrom,
		///<summary>AggregationID. </summary>
		AggregationID,
		///<summary>AggregationTo. </summary>
		AggregationTo,
		///<summary>AggregationType. </summary>
		AggregationType,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>Duration. </summary>
		Duration,
		///<summary>EventTime. </summary>
		EventTime,
		///<summary>Hostname. </summary>
		Hostname,
		///<summary>IndicatorID. </summary>
		IndicatorID,
		///<summary>NumberAboveThreshold. </summary>
		NumberAboveThreshold,
		///<summary>NumberOfAuthorizedTx. </summary>
		NumberOfAuthorizedTx,
		///<summary>PerformanceID. </summary>
		PerformanceID,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>ShiftBegin. </summary>
		ShiftBegin,
		///<summary>TotalNumber. </summary>
		TotalNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettledRevenue.</summary>
	public enum SettledRevenueFieldIndex
	{
		///<summary>RevenueRecognitionID. </summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID. </summary>
		RevenueSettlementID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementAccount.</summary>
	public enum SettlementAccountFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>SettlementAccountID. </summary>
		SettlementAccountID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementCalendar.</summary>
	public enum SettlementCalendarFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>SettlementCalendarID. </summary>
		SettlementCalendarID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementDistributionPolicy.</summary>
	public enum SettlementDistributionPolicyFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementHoliday.</summary>
	public enum SettlementHolidayFieldIndex
	{
		///<summary>Holiday. </summary>
		Holiday,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>Recurrence. </summary>
		Recurrence,
		///<summary>SettlementCalendarID. </summary>
		SettlementCalendarID,
		///<summary>SettlementHolidayID. </summary>
		SettlementHolidayID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementOperation.</summary>
	public enum SettlementOperationFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementQuerySetting.</summary>
	public enum SettlementQuerySettingFieldIndex
	{
		///<summary>AgencyOwningVehicle. </summary>
		AgencyOwningVehicle,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Line. </summary>
		Line,
		///<summary>Name. </summary>
		Name,
		///<summary>Operation. </summary>
		Operation,
		///<summary>SalesChannel. </summary>
		SalesChannel,
		///<summary>Settlementcalendarid. </summary>
		Settlementcalendarid,
		///<summary>SettlementQuerySettingID. </summary>
		SettlementQuerySettingID,
		///<summary>SettlementSetupID. </summary>
		SettlementSetupID,
		///<summary>StaticValue. </summary>
		StaticValue,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionTypeID. </summary>
		TransactionTypeID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementQuerySettingToTicket.</summary>
	public enum SettlementQuerySettingToTicketFieldIndex
	{
		///<summary>SettlementQuerySettingID. </summary>
		SettlementQuerySettingID,
		///<summary>TicketID. </summary>
		TicketID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementQueryValue.</summary>
	public enum SettlementQueryValueFieldIndex
	{
		///<summary>Calculated. </summary>
		Calculated,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SettlementQuerySettingID. </summary>
		SettlementQuerySettingID,
		///<summary>SettlementQueryValueID. </summary>
		SettlementQueryValueID,
		///<summary>SettlementRunValueID. </summary>
		SettlementRunValueID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementReleaseState.</summary>
	public enum SettlementReleaseStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementResult.</summary>
	public enum SettlementResultFieldIndex
	{
		///<summary>BadSum. </summary>
		BadSum,
		///<summary>BadSumTaxed. </summary>
		BadSumTaxed,
		///<summary>CardIssuerID. </summary>
		CardIssuerID,
		///<summary>ClearingResultLevel. </summary>
		ClearingResultLevel,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>ContractorID. </summary>
		ContractorID,
		///<summary>CorrectedSum. </summary>
		CorrectedSum,
		///<summary>CorrectedSumTaxed. </summary>
		CorrectedSumTaxed,
		///<summary>CustomerGroupID. </summary>
		CustomerGroupID,
		///<summary>DebtorID. </summary>
		DebtorID,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>DevicePaymentMethodID. </summary>
		DevicePaymentMethodID,
		///<summary>GoodSum. </summary>
		GoodSum,
		///<summary>GoodSumTaxed. </summary>
		GoodSumTaxed,
		///<summary>InvalidSum. </summary>
		InvalidSum,
		///<summary>IsCancellation. </summary>
		IsCancellation,
		///<summary>LineGroupID. </summary>
		LineGroupID,
		///<summary>LineID. </summary>
		LineID,
		///<summary>PtomUnit. </summary>
		PtomUnit,
		///<summary>QuarantinedSum. </summary>
		QuarantinedSum,
		///<summary>QuarantinedSumTaxed. </summary>
		QuarantinedSumTaxed,
		///<summary>ResponsibleClientID. </summary>
		ResponsibleClientID,
		///<summary>SettlementID. </summary>
		SettlementID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketType. </summary>
		TicketType,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		///<summary>TransactionTypeID. </summary>
		TransactionTypeID,
		///<summary>TripCode. </summary>
		TripCode,
		///<summary>ValidationSum. </summary>
		ValidationSum,
		///<summary>ValidationSumTaxed. </summary>
		ValidationSumTaxed,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementRun.</summary>
	public enum SettlementRunFieldIndex
	{
		///<summary>BeginDate. </summary>
		BeginDate,
		///<summary>EndDate. </summary>
		EndDate,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SettlementRunID. </summary>
		SettlementRunID,
		///<summary>SettlementSetupID. </summary>
		SettlementSetupID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementRunValue.</summary>
	public enum SettlementRunValueFieldIndex
	{
		///<summary>Calculated. </summary>
		Calculated,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SettlementRunID. </summary>
		SettlementRunID,
		///<summary>SettlementRunValueID. </summary>
		SettlementRunValueID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementRunValueToVarioSettlement.</summary>
	public enum SettlementRunValueToVarioSettlementFieldIndex
	{
		///<summary>SettlementRunValueID. </summary>
		SettlementRunValueID,
		///<summary>VarioSettlementID. </summary>
		VarioSettlementID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementSetup.</summary>
	public enum SettlementSetupFieldIndex
	{
		///<summary>FormulaString. </summary>
		FormulaString,
		///<summary>FromAccountID. </summary>
		FromAccountID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>ReleaseState. </summary>
		ReleaseState,
		///<summary>ScheduleRFC5545. </summary>
		ScheduleRFC5545,
		///<summary>SettlementSetupID. </summary>
		SettlementSetupID,
		///<summary>ToAccountID. </summary>
		ToAccountID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidUntil. </summary>
		ValidUntil,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SettlementType.</summary>
	public enum SettlementTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Source.</summary>
	public enum SourceFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StatementOfAccount.</summary>
	public enum StatementOfAccountFieldIndex
	{
		///<summary>BookingDate. </summary>
		BookingDate,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>ImportedFilename. </summary>
		ImportedFilename,
		///<summary>ImportFiledate. </summary>
		ImportFiledate,
		///<summary>InvoiceID. </summary>
		InvoiceID,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ManuallyBooked. </summary>
		ManuallyBooked,
		///<summary>ReasonOfPayment. </summary>
		ReasonOfPayment,
		///<summary>ReturnDebitDate. </summary>
		ReturnDebitDate,
		///<summary>ReturnDebitEndToEndID. </summary>
		ReturnDebitEndToEndID,
		///<summary>ReturnDebitReasonID. </summary>
		ReturnDebitReasonID,
		///<summary>Statementofaccountid. </summary>
		Statementofaccountid,
		///<summary>TotalAmount. </summary>
		TotalAmount,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TypeDiskriminator. </summary>
		TypeDiskriminator,
		///<summary>ValueDate. </summary>
		ValueDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StateOfCountry.</summary>
	public enum StateOfCountryFieldIndex
	{
		///<summary>CountryID. </summary>
		CountryID,
		///<summary>IsoCode. </summary>
		IsoCode,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>OrderNumber. </summary>
		OrderNumber,
		///<summary>ShortName. </summary>
		ShortName,
		///<summary>StateOfCountryID. </summary>
		StateOfCountryID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StockTransfer.</summary>
	public enum StockTransferFieldIndex
	{
		///<summary>InvoiceNumber. </summary>
		InvoiceNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NewStorageLocationID. </summary>
		NewStorageLocationID,
		///<summary>OldStorageLocationID. </summary>
		OldStorageLocationID,
		///<summary>StockTransferID. </summary>
		StockTransferID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransferTime. </summary>
		TransferTime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StorageLocation.</summary>
	public enum StorageLocationFieldIndex
	{
		///<summary>Abbreviation. </summary>
		Abbreviation,
		///<summary>AddressID. </summary>
		AddressID,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>StorageLocationID. </summary>
		StorageLocationID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SubsidyLimit.</summary>
	public enum SubsidyLimitFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>LimitAmount. </summary>
		LimitAmount,
		///<summary>OrganizationID. </summary>
		OrganizationID,
		///<summary>SubsidyLimitID. </summary>
		SubsidyLimitID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SubsidyTransaction.</summary>
	public enum SubsidyTransactionFieldIndex
	{
		///<summary>FullPrice. </summary>
		FullPrice,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ReducedPrice. </summary>
		ReducedPrice,
		///<summary>SubsidyTransactionID. </summary>
		SubsidyTransactionID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionDate. </summary>
		TransactionDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Survey.</summary>
	public enum SurveyFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SurveyID. </summary>
		SurveyID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyAnswer.</summary>
	public enum SurveyAnswerFieldIndex
	{
		///<summary>FreeTextAnswer. </summary>
		FreeTextAnswer,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SurveyChoiceID. </summary>
		SurveyChoiceID,
		///<summary>SurveyQuestionID. </summary>
		SurveyQuestionID,
		///<summary>SurveyResponseID. </summary>
		SurveyResponseID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyChoice.</summary>
	public enum SurveyChoiceFieldIndex
	{
		///<summary>FollowUpQuestionID. </summary>
		FollowUpQuestionID,
		///<summary>HasFreeText. </summary>
		HasFreeText,
		///<summary>IsRetired. </summary>
		IsRetired,
		///<summary>Language. </summary>
		Language,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>SurveyChoiceID. </summary>
		SurveyChoiceID,
		///<summary>SurveyQuestionID. </summary>
		SurveyQuestionID,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyDescription.</summary>
	public enum SurveyDescriptionFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>Language. </summary>
		Language,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>SurveyID. </summary>
		SurveyID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyQuestion.</summary>
	public enum SurveyQuestionFieldIndex
	{
		///<summary>HasMultipleAnswers. </summary>
		HasMultipleAnswers,
		///<summary>IsRetired. </summary>
		IsRetired,
		///<summary>Language. </summary>
		Language,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Rank. </summary>
		Rank,
		///<summary>SurveyID. </summary>
		SurveyID,
		///<summary>SurveyQuestionID. </summary>
		SurveyQuestionID,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyResponse.</summary>
	public enum SurveyResponseFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>PersonID. </summary>
		PersonID,
		///<summary>SurveyID. </summary>
		SurveyID,
		///<summary>SurveyResponseID. </summary>
		SurveyResponseID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Tenant.</summary>
	public enum TenantFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>State. </summary>
		State,
		///<summary>TenantID. </summary>
		TenantID,
		///<summary>TenantName. </summary>
		TenantName,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TenantHistory.</summary>
	public enum TenantHistoryFieldIndex
	{
		///<summary>LogCreationDate. </summary>
		LogCreationDate,
		///<summary>Message. </summary>
		Message,
		///<summary>TenantHistoryID. </summary>
		TenantHistoryID,
		///<summary>TenantID. </summary>
		TenantID,
		///<summary>TenantPersonID. </summary>
		TenantPersonID,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TenantPerson.</summary>
	public enum TenantPersonFieldIndex
	{
		///<summary>CellPhoneNumber. </summary>
		CellPhoneNumber,
		///<summary>Email. </summary>
		Email,
		///<summary>FaxNumber. </summary>
		FaxNumber,
		///<summary>FirstName. </summary>
		FirstName,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastName. </summary>
		LastName,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Password. </summary>
		Password,
		///<summary>TenantID. </summary>
		TenantID,
		///<summary>TenantPersonID. </summary>
		TenantPersonID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TenantState.</summary>
	public enum TenantStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminationStatus.</summary>
	public enum TerminationStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketAssignment.</summary>
	public enum TicketAssignmentFieldIndex
	{
		///<summary>AssignmentName. </summary>
		AssignmentName,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>DataRowCreationDate. </summary>
		DataRowCreationDate,
		///<summary>Destination. </summary>
		Destination,
		///<summary>EntitlementID. </summary>
		EntitlementID,
		///<summary>InstitutionID. </summary>
		InstitutionID,
		///<summary>IsAutoRenewEnabled. </summary>
		IsAutoRenewEnabled,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastTransaction. </summary>
		LastTransaction,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Origin. </summary>
		Origin,
		///<summary>PeriodicAllowance. </summary>
		PeriodicAllowance,
		///<summary>PeriodicTypeID. </summary>
		PeriodicTypeID,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>Status. </summary>
		Status,
		///<summary>TicketAssignmentId. </summary>
		TicketAssignmentId,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketsRemaining. </summary>
		TicketsRemaining,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketAssignmentStatus.</summary>
	public enum TicketAssignmentStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketSerialNumber.</summary>
	public enum TicketSerialNumberFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>SerialNumber. </summary>
		SerialNumber,
		///<summary>SoldBy. </summary>
		SoldBy,
		///<summary>SoldDate. </summary>
		SoldDate,
		///<summary>Status. </summary>
		Status,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketSerialnumberID. </summary>
		TicketSerialnumberID,
		///<summary>TicketTypeID. </summary>
		TicketTypeID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TicketSerialNumberStatus.</summary>
	public enum TicketSerialNumberStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionJournal.</summary>
	public enum TransactionJournalFieldIndex
	{
		///<summary>AlightDuration. </summary>
		AlightDuration,
		///<summary>AppliedPassProductID. </summary>
		AppliedPassProductID,
		///<summary>AppliedPassTicketID. </summary>
		AppliedPassTicketID,
		///<summary>BlockNumber. </summary>
		BlockNumber,
		///<summary>BoardingGuid. </summary>
		BoardingGuid,
		///<summary>BoardingTime. </summary>
		BoardingTime,
		///<summary>CancellationReference. </summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid. </summary>
		CancellationReferenceGuid,
		///<summary>CardHolderID. </summary>
		CardHolderID,
		///<summary>CheckOutTime. </summary>
		CheckOutTime,
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CompletionState. </summary>
		CompletionState,
		///<summary>CorrectedLine. </summary>
		CorrectedLine,
		///<summary>CostCenterKey. </summary>
		CostCenterKey,
		///<summary>CourseNumber. </summary>
		CourseNumber,
		///<summary>CreditCardAuthorizationID. </summary>
		CreditCardAuthorizationID,
		///<summary>CustomerGroup. </summary>
		CustomerGroup,
		///<summary>DebtorNumber. </summary>
		DebtorNumber,
		///<summary>DestinationZone. </summary>
		DestinationZone,
		///<summary>DeviceBookingNumber. </summary>
		DeviceBookingNumber,
		///<summary>DeviceNumber. </summary>
		DeviceNumber,
		///<summary>DeviceTime. </summary>
		DeviceTime,
		///<summary>Direction. </summary>
		Direction,
		///<summary>Distance. </summary>
		Distance,
		///<summary>Duration. </summary>
		Duration,
		///<summary>ExternalTicketName. </summary>
		ExternalTicketName,
		///<summary>ExternalTransactionIdentifier. </summary>
		ExternalTransactionIdentifier,
		///<summary>FareAmount. </summary>
		FareAmount,
		///<summary>FareMediaID. </summary>
		FareMediaID,
		///<summary>FareMediaType. </summary>
		FareMediaType,
		///<summary>FareScaleName. </summary>
		FareScaleName,
		///<summary>FillLevel. </summary>
		FillLevel,
		///<summary>GeolocationLatitude. </summary>
		GeolocationLatitude,
		///<summary>GeolocationLongitude. </summary>
		GeolocationLongitude,
		///<summary>GroupSize. </summary>
		GroupSize,
		///<summary>InsertTime. </summary>
		InsertTime,
		///<summary>JourneyStartTime. </summary>
		JourneyStartTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Line. </summary>
		Line,
		///<summary>MountingPlateNumber. </summary>
		MountingPlateNumber,
		///<summary>Notes. </summary>
		Notes,
		///<summary>NoTrip. </summary>
		NoTrip,
		///<summary>OperatorID. </summary>
		OperatorID,
		///<summary>OriginalDeviceResultType. </summary>
		OriginalDeviceResultType,
		///<summary>OriginalOnlineResultType. </summary>
		OriginalOnlineResultType,
		///<summary>OriginalSingleFare. </summary>
		OriginalSingleFare,
		///<summary>ProductID. </summary>
		ProductID,
		///<summary>ProductID2. </summary>
		ProductID2,
		///<summary>Properties. </summary>
		Properties,
		///<summary>PurseBalance. </summary>
		PurseBalance,
		///<summary>PurseBalance2. </summary>
		PurseBalance2,
		///<summary>PurseCredit. </summary>
		PurseCredit,
		///<summary>PurseCredit2. </summary>
		PurseCredit2,
		///<summary>ResultSource. </summary>
		ResultSource,
		///<summary>ResultType. </summary>
		ResultType,
		///<summary>RoundTripTime. </summary>
		RoundTripTime,
		///<summary>RouteCode. </summary>
		RouteCode,
		///<summary>RouteNumber. </summary>
		RouteNumber,
		///<summary>SaleID. </summary>
		SaleID,
		///<summary>SalesChannelID. </summary>
		SalesChannelID,
		///<summary>ShiftBegin. </summary>
		ShiftBegin,
		///<summary>SmartTapCounter. </summary>
		SmartTapCounter,
		///<summary>SmartTapID. </summary>
		SmartTapID,
		///<summary>StopCode. </summary>
		StopCode,
		///<summary>StopNumber. </summary>
		StopNumber,
		///<summary>TariffDate. </summary>
		TariffDate,
		///<summary>TariffVersion. </summary>
		TariffVersion,
		///<summary>TaxAmount. </summary>
		TaxAmount,
		///<summary>TaxLocationCode. </summary>
		TaxLocationCode,
		///<summary>TaxRate. </summary>
		TaxRate,
		///<summary>TaxTicketID. </summary>
		TaxTicketID,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TokenCardID. </summary>
		TokenCardID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		///<summary>TransactionNumber. </summary>
		TransactionNumber,
		///<summary>TransactionTime. </summary>
		TransactionTime,
		///<summary>TransactionType. </summary>
		TransactionType,
		///<summary>Transferredfromcardid. </summary>
		Transferredfromcardid,
		///<summary>TransitAccountID. </summary>
		TransitAccountID,
		///<summary>TransportState. </summary>
		TransportState,
		///<summary>TripCounter. </summary>
		TripCounter,
		///<summary>TripSerialNumber. </summary>
		TripSerialNumber,
		///<summary>TripTicketInternalNumber. </summary>
		TripTicketInternalNumber,
		///<summary>TripValue. </summary>
		TripValue,
		///<summary>TripValue2. </summary>
		TripValue2,
		///<summary>TripValueFloor. </summary>
		TripValueFloor,
		///<summary>TripValueFloor2. </summary>
		TripValueFloor2,
		///<summary>ValidFrom. </summary>
		ValidFrom,
		///<summary>ValidTo. </summary>
		ValidTo,
		///<summary>VanpoolGroupID. </summary>
		VanpoolGroupID,
		///<summary>VehicleNumber. </summary>
		VehicleNumber,
		///<summary>WhitelistVersion. </summary>
		WhitelistVersion,
		///<summary>WhitelistVersionCreated. </summary>
		WhitelistVersionCreated,
		///<summary>Zone. </summary>
		Zone,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionJournalCorrection.</summary>
	public enum TransactionJournalCorrectionFieldIndex
	{
		///<summary>BlockNumber. </summary>
		BlockNumber,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RouteNumber. </summary>
		RouteNumber,
		///<summary>SpxTransID. </summary>
		SpxTransID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransactionJournalCorrectionID. </summary>
		TransactionJournalCorrectionID,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionJourneyHistory.</summary>
	public enum TransactionJourneyHistoryFieldIndex
	{
		///<summary>CardBalance. </summary>
		CardBalance,
		///<summary>CardID. </summary>
		CardID,
		///<summary>CardReferenceTransactionNumber. </summary>
		CardReferenceTransactionNumber,
		///<summary>CardTransactionNumber. </summary>
		CardTransactionNumber,
		///<summary>FromStopName. </summary>
		FromStopName,
		///<summary>FromStopNumber. </summary>
		FromStopNumber,
		///<summary>JourneyLegCounter. </summary>
		JourneyLegCounter,
		///<summary>Price. </summary>
		Price,
		///<summary>TicketName. </summary>
		TicketName,
		///<summary>TicketNumber. </summary>
		TicketNumber,
		///<summary>ToStopName. </summary>
		ToStopName,
		///<summary>ToStopNumber. </summary>
		ToStopNumber,
		///<summary>TransactionID. </summary>
		TransactionID,
		///<summary>TripDateTime. </summary>
		TripDateTime,
		///<summary>TripSerialNumber. </summary>
		TripSerialNumber,
		///<summary>TypeID. </summary>
		TypeID,
		///<summary>TypeName. </summary>
		TypeName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionToInspection.</summary>
	public enum TransactionToInspectionFieldIndex
	{
		///<summary>InspectionReportID. </summary>
		InspectionReportID,
		///<summary>TransactionJournalID. </summary>
		TransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransactionType.</summary>
	public enum TransactionTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TransportCompany.</summary>
	public enum TransportCompanyFieldIndex
	{
		///<summary>ClientID. </summary>
		ClientID,
		///<summary>CompanyName. </summary>
		CompanyName,
		///<summary>CompanyNumber. </summary>
		CompanyNumber,
		///<summary>Description. </summary>
		Description,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TransportCompanyID. </summary>
		TransportCompanyID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TriggerType.</summary>
	public enum TriggerTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UnitCollection.</summary>
	public enum UnitCollectionFieldIndex
	{
		///<summary>Abbreviation. </summary>
		Abbreviation,
		///<summary>Description. </summary>
		Description,
		///<summary>DeviceClassID. </summary>
		DeviceClassID,
		///<summary>IsDefaultForDeviceClass. </summary>
		IsDefaultForDeviceClass,
		///<summary>IsGlobal. </summary>
		IsGlobal,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>ParameterArchiveID. </summary>
		ParameterArchiveID,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>UnitCollectionID. </summary>
		UnitCollectionID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UnitCollectionToUnit.</summary>
	public enum UnitCollectionToUnitFieldIndex
	{
		///<summary>UnitCollectionID. </summary>
		UnitCollectionID,
		///<summary>UnitID. </summary>
		UnitID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationResult.</summary>
	public enum ValidationResultFieldIndex
	{
		///<summary>Checked. </summary>
		Checked,
		///<summary>Details. </summary>
		Details,
		///<summary>Info. </summary>
		Info,
		///<summary>IsValid. </summary>
		IsValid,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>OriginalValidationResultID. </summary>
		OriginalValidationResultID,
		///<summary>Resolved. </summary>
		Resolved,
		///<summary>ResolvedComment. </summary>
		ResolvedComment,
		///<summary>ResolvedType. </summary>
		ResolvedType,
		///<summary>ResolvedUser. </summary>
		ResolvedUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidationResultID. </summary>
		ValidationResultID,
		///<summary>ValidationRuleID. </summary>
		ValidationRuleID,
		///<summary>ValidationRunRuleID. </summary>
		ValidationRunRuleID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationResultResolveType.</summary>
	public enum ValidationResultResolveTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationRule.</summary>
	public enum ValidationRuleFieldIndex
	{
		///<summary>IsAutoResolvedAllowed. </summary>
		IsAutoResolvedAllowed,
		///<summary>IsCritical. </summary>
		IsCritical,
		///<summary>IsEnabled. </summary>
		IsEnabled,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>ValidationRuleID. </summary>
		ValidationRuleID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationRun.</summary>
	public enum ValidationRunFieldIndex
	{
		///<summary>CreateDate. </summary>
		CreateDate,
		///<summary>PeriodDndDate. </summary>
		PeriodDndDate,
		///<summary>PeriodStartDate. </summary>
		PeriodStartDate,
		///<summary>ValidationRunID. </summary>
		ValidationRunID,
		///<summary>ValidationState. </summary>
		ValidationState,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationRunRule.</summary>
	public enum ValidationRunRuleFieldIndex
	{
		///<summary>EndDateTime. </summary>
		EndDateTime,
		///<summary>StartDateTime. </summary>
		StartDateTime,
		///<summary>Status. </summary>
		Status,
		///<summary>ValidationRuleID. </summary>
		ValidationRuleID,
		///<summary>ValidationRunID. </summary>
		ValidationRunID,
		///<summary>ValidationRunRuleID. </summary>
		ValidationRunRuleID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationStatus.</summary>
	public enum ValidationStatusFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ValidationValue.</summary>
	public enum ValidationValueFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Name. </summary>
		Name,
		///<summary>TransactioCcounter. </summary>
		TransactioCcounter,
		///<summary>ValidationResultID. </summary>
		ValidationResultID,
		///<summary>ValidationValueID. </summary>
		ValidationValueID,
		///<summary>Value. </summary>
		Value,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VdvTransaction.</summary>
	public enum VdvTransactionFieldIndex
	{
		///<summary>BerTariffBlock. </summary>
		BerTariffBlock,
		///<summary>BerValidBegin. </summary>
		BerValidBegin,
		///<summary>BerValidEnd. </summary>
		BerValidEnd,
		///<summary>CardID. </summary>
		CardID,
		///<summary>Name. </summary>
		Name,
		///<summary>Price. </summary>
		Price,
		///<summary>SalesChannel. </summary>
		SalesChannel,
		///<summary>TripDateTime. </summary>
		TripDateTime,
		///<summary>TxData. </summary>
		TxData,
		///<summary>TxTyp. </summary>
		TxTyp,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VerificationAttempt.</summary>
	public enum VerificationAttemptFieldIndex
	{
		///<summary>CustomerAccountID. </summary>
		CustomerAccountID,
		///<summary>IsLockoutForced. </summary>
		IsLockoutForced,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VerificationAttemptID. </summary>
		VerificationAttemptID,
		///<summary>VerificationTime. </summary>
		VerificationTime,
		///<summary>VerificationType. </summary>
		VerificationType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VerificationAttemptType.</summary>
	public enum VerificationAttemptTypeFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VirtualCard.</summary>
	public enum VirtualCardFieldIndex
	{
		///<summary>CardID. </summary>
		CardID,
		///<summary>DeviceType. </summary>
		DeviceType,
		///<summary>ExternalAppID. </summary>
		ExternalAppID,
		///<summary>ExternalDeviceID. </summary>
		ExternalDeviceID,
		///<summary>ExternalUserID. </summary>
		ExternalUserID,
		///<summary>ExternalWalletID. </summary>
		ExternalWalletID,
		///<summary>HasLinkedDevice. </summary>
		HasLinkedDevice,
		///<summary>HasUsers. </summary>
		HasUsers,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>NotificationDeviceID. </summary>
		NotificationDeviceID,
		///<summary>Synchronized. </summary>
		Synchronized,
		///<summary>TapCounter. </summary>
		TapCounter,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VirtualCardID. </summary>
		VirtualCardID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Voucher.</summary>
	public enum VoucherFieldIndex
	{
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreatedOn. </summary>
		CreatedOn,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>RedeemableFrom. </summary>
		RedeemableFrom,
		///<summary>RedeemableTo. </summary>
		RedeemableTo,
		///<summary>RedeemedBy. </summary>
		RedeemedBy,
		///<summary>RedeemedForCardID. </summary>
		RedeemedForCardID,
		///<summary>RedeemedOn. </summary>
		RedeemedOn,
		///<summary>TicketID. </summary>
		TicketID,
		///<summary>TicketInternalNumber. </summary>
		TicketInternalNumber,
		///<summary>TicketValidFrom. </summary>
		TicketValidFrom,
		///<summary>TicketValidTo. </summary>
		TicketValidTo,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>VoucherCode. </summary>
		VoucherCode,
		///<summary>VoucherID. </summary>
		VoucherID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Whitelist.</summary>
	public enum WhitelistFieldIndex
	{
		///<summary>ByteRepresentation. </summary>
		ByteRepresentation,
		///<summary>CardID. </summary>
		CardID,
		///<summary>FareMediaID. </summary>
		FareMediaID,
		///<summary>FareMediaType. </summary>
		FareMediaType,
		///<summary>InitiatorTransactionTime. </summary>
		InitiatorTransactionTime,
		///<summary>InsertTime. </summary>
		InsertTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>WhitelistID. </summary>
		WhitelistID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WhitelistJournal.</summary>
	public enum WhitelistJournalFieldIndex
	{
		///<summary>ByteRepresentation. </summary>
		ByteRepresentation,
		///<summary>CardID. </summary>
		CardID,
		///<summary>FareMediaID. </summary>
		FareMediaID,
		///<summary>FareMediaType. </summary>
		FareMediaType,
		///<summary>InitiatorTransactionTime. </summary>
		InitiatorTransactionTime,
		///<summary>InsertTime. </summary>
		InsertTime,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>WhitelistJournalID. </summary>
		WhitelistJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItem.</summary>
	public enum WorkItemFieldIndex
	{
		///<summary>Assigned. </summary>
		Assigned,
		///<summary>Assignee. </summary>
		Assignee,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Executed. </summary>
		Executed,
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Memo. </summary>
		Memo,
		///<summary>Source. </summary>
		Source,
		///<summary>State. </summary>
		State,
		///<summary>Title. </summary>
		Title,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>TypeDiscriminator. </summary>
		TypeDiscriminator,
		///<summary>WorkItemID. </summary>
		WorkItemID,
		///<summary>WorkItemSubjectID. </summary>
		WorkItemSubjectID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItemCategory.</summary>
	public enum WorkItemCategoryFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItemHistory.</summary>
	public enum WorkItemHistoryFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>WorkItemHistoryID. </summary>
		WorkItemHistoryID,
		///<summary>WorkItemID. </summary>
		WorkItemID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItemState.</summary>
	public enum WorkItemStateFieldIndex
	{
		///<summary>Description. </summary>
		Description,
		///<summary>EnumerationValue. </summary>
		EnumerationValue,
		///<summary>Literal. </summary>
		Literal,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItemSubject.</summary>
	public enum WorkItemSubjectFieldIndex
	{
		///<summary>LastModified. </summary>
		LastModified,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Text. </summary>
		Text,
		///<summary>TransactionCounter. </summary>
		TransactionCounter,
		///<summary>WorkItemSubjectID. </summary>
		WorkItemSubjectID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WorkItemView.</summary>
	public enum WorkItemViewFieldIndex
	{
		///<summary>Assigned. </summary>
		Assigned,
		///<summary>Assignee. </summary>
		Assignee,
		///<summary>CardID. </summary>
		CardID,
		///<summary>ContractID. </summary>
		ContractID,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Executed. </summary>
		Executed,
		///<summary>ExternalOrderNumber. </summary>
		ExternalOrderNumber,
		///<summary>IdentificationNumber. </summary>
		IdentificationNumber,
		///<summary>LastUser. </summary>
		LastUser,
		///<summary>Memo. </summary>
		Memo,
		///<summary>OrderID. </summary>
		OrderID,
		///<summary>Source. </summary>
		Source,
		///<summary>State. </summary>
		State,
		///<summary>Title. </summary>
		Title,
		///<summary>TypeDiscriminator. </summary>
		TypeDiscriminator,
		///<summary>WorkItemID. </summary>
		WorkItemID,
		///<summary>WorkItemSubjectID. </summary>
		WorkItemSubjectID,
		/// <summary></summary>
		AmountOfFields
	}

	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccCancelRecTapCmlSettled.</summary>
	public enum AccCancelRecTapCmlSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccCancelRecTapTabSettled.</summary>
	public enum AccCancelRecTapTabSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccountSearch.</summary>
	public enum AccountSearchFieldIndex
	{
		///<summary>PersonID</summary>
		PersonID,
		///<summary>ContractID</summary>
		ContractID,
		///<summary>UserName</summary>
		UserName,
		///<summary>PhonePIN</summary>
		PhonePIN,
		///<summary>AccountState</summary>
		AccountState,
		///<summary>ContractNumber</summary>
		ContractNumber,
		///<summary>AuthenticationToken</summary>
		AuthenticationToken,
		///<summary>LastName</summary>
		LastName,
		///<summary>FirstName</summary>
		FirstName,
		///<summary>DateOfBirth</summary>
		DateOfBirth,
		///<summary>Email</summary>
		Email,
		///<summary>PhoneNumber</summary>
		PhoneNumber,
		///<summary>PersonType</summary>
		PersonType,
		///<summary>OrganizationName</summary>
		OrganizationName,
		///<summary>Addressee</summary>
		Addressee,
		///<summary>Street</summary>
		Street,
		///<summary>StreetNumber</summary>
		StreetNumber,
		///<summary>AddressField1</summary>
		AddressField1,
		///<summary>AddressField2</summary>
		AddressField2,
		///<summary>City</summary>
		City,
		///<summary>PostalCode</summary>
		PostalCode,
		///<summary>Region</summary>
		Region,
		///<summary>Country</summary>
		Country,
		///<summary>AddressType</summary>
		AddressType,
		///<summary>CardPrintedNumber</summary>
		CardPrintedNumber,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccRecLoadUseCmlSettled.</summary>
	public enum AccRecLoadUseCmlSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccRecLoadUseTabSettled.</summary>
	public enum AccRecLoadUseTabSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccRecognizedTapCmlSettled.</summary>
	public enum AccRecognizedTapCmlSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : AccRecognizedTapTabSettled.</summary>
	public enum AccRecognizedTapTabSettledFieldIndex
	{
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>SettlementType</summary>
		SettlementType,
		///<summary>Amount</summary>
		Amount,
		///<summary>Premium</summary>
		Premium,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>IncludeInSettlement</summary>
		IncludeInSettlement,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>CancellationReferenceGUID</summary>
		CancellationReferenceGUID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>CostCenterKey</summary>
		CostCenterKey,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : ChangeVoucher.</summary>
	public enum ChangeVoucherFieldIndex
	{
		///<summary>ChangeVoucherID</summary>
		ChangeVoucherID,
		///<summary>Debtor</summary>
		Debtor,
		///<summary>DebtorName</summary>
		DebtorName,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>IssueDate</summary>
		IssueDate,
		///<summary>LocationNumber</summary>
		LocationNumber,
		///<summary>Location</summary>
		Location,
		///<summary>Reference</summary>
		Reference,
		///<summary>IsRedeemed</summary>
		IsRedeemed,
		///<summary>RedeemDate</summary>
		RedeemDate,
		///<summary>RedeemBy</summary>
		RedeemBy,
		///<summary>RedeemedOnDevice</summary>
		RedeemedOnDevice,
		///<summary>SalesTransactionID</summary>
		SalesTransactionID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>FareAmount</summary>
		FareAmount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : DebtorAccountBalance.</summary>
	public enum DebtorAccountBalanceFieldIndex
	{
		///<summary>AccountID</summary>
		AccountID,
		///<summary>AccountNumber</summary>
		AccountNumber,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>FirstName</summary>
		FirstName,
		///<summary>LastName</summary>
		LastName,
		///<summary>AccountBalance</summary>
		AccountBalance,
		///<summary>Period</summary>
		Period,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : DebtorPostings.</summary>
	public enum DebtorPostingsFieldIndex
	{
		///<summary>AccountID</summary>
		AccountID,
		///<summary>AccountNumber</summary>
		AccountNumber,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>PostingTime</summary>
		PostingTime,
		///<summary>ValueDate</summary>
		ValueDate,
		///<summary>Amount</summary>
		Amount,
		///<summary>PostingText</summary>
		PostingText,
		///<summary>ReceiptNumber</summary>
		ReceiptNumber,
		///<summary>PostingCodeNumber</summary>
		PostingCodeNumber,
		///<summary>PostingCodeText</summary>
		PostingCodeText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : FilteredAccountPostingKey.</summary>
	public enum FilteredAccountPostingKeyFieldIndex
	{
		///<summary>EntryTypeNumber</summary>
		EntryTypeNumber,
		///<summary>EntryTypeName</summary>
		EntryTypeName,
		///<summary>IsManual</summary>
		IsManual,
		///<summary>IsExport</summary>
		IsExport,
		///<summary>Sign</summary>
		Sign,
		///<summary>IsCancelation</summary>
		IsCancelation,
		///<summary>IsDebitEntry</summary>
		IsDebitEntry,
		///<summary>EntryRangeID</summary>
		EntryRangeID,
		///<summary>TaxRate</summary>
		TaxRate,
		///<summary>IsBillShown</summary>
		IsBillShown,
		///<summary>IsBillClear</summary>
		IsBillClear,
		///<summary>IsReturnDebit</summary>
		IsReturnDebit,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : FrameworkOrderDetail.</summary>
	public enum FrameworkOrderDetailFieldIndex
	{
		///<summary>OrderID</summary>
		OrderID,
		///<summary>OrderNumber</summary>
		OrderNumber,
		///<summary>FrameworkName</summary>
		FrameworkName,
		///<summary>FrameworkOrganizationNumber</summary>
		FrameworkOrganizationNumber,
		///<summary>FrameworkOrgExternalNumber</summary>
		FrameworkOrgExternalNumber,
		///<summary>FrameworkAddressee</summary>
		FrameworkAddressee,
		///<summary>FrameworkAddressField1</summary>
		FrameworkAddressField1,
		///<summary>FrameworkAddressField2</summary>
		FrameworkAddressField2,
		///<summary>FrameworkCity</summary>
		FrameworkCity,
		///<summary>FrameworkPostalCode</summary>
		FrameworkPostalCode,
		///<summary>FrameworkCountry</summary>
		FrameworkCountry,
		///<summary>FrameworkRegion</summary>
		FrameworkRegion,
		///<summary>FrameworkStreet</summary>
		FrameworkStreet,
		///<summary>FrameworkStreetNumber</summary>
		FrameworkStreetNumber,
		///<summary>SchoolName</summary>
		SchoolName,
		///<summary>SchoolOrganizationNumber</summary>
		SchoolOrganizationNumber,
		///<summary>SchoolAddressee</summary>
		SchoolAddressee,
		///<summary>SchoolAddressField1</summary>
		SchoolAddressField1,
		///<summary>SchoolAddressField2</summary>
		SchoolAddressField2,
		///<summary>SchoolCity</summary>
		SchoolCity,
		///<summary>SchoolPostalCode</summary>
		SchoolPostalCode,
		///<summary>SchoolCountry</summary>
		SchoolCountry,
		///<summary>SchoolStreet</summary>
		SchoolStreet,
		///<summary>SchoolStreetNumber</summary>
		SchoolStreetNumber,
		///<summary>SchoolRegion</summary>
		SchoolRegion,
		///<summary>OrganizationalIdentifier</summary>
		OrganizationalIdentifier,
		///<summary>FirstName</summary>
		FirstName,
		///<summary>LastName</summary>
		LastName,
		///<summary>DateOfBirth</summary>
		DateOfBirth,
		///<summary>PrintedNumber</summary>
		PrintedNumber,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		///<summary>RelationFrom</summary>
		RelationFrom,
		///<summary>RelationTo</summary>
		RelationTo,
		///<summary>RelationPriority</summary>
		RelationPriority,
		///<summary>SchoolYearName</summary>
		SchoolYearName,
		///<summary>FrameworkContractID</summary>
		FrameworkContractID,
		///<summary>SchoolContractID</summary>
		SchoolContractID,
		///<summary>PersonID</summary>
		PersonID,
		///<summary>ProductRelationID</summary>
		ProductRelationID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>OrderDetailID</summary>
		OrderDetailID,
		///<summary>RelationFareStage</summary>
		RelationFareStage,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : InvoiceDetail.</summary>
	public enum InvoiceDetailFieldIndex
	{
		///<summary>SchoolYearID</summary>
		SchoolYearID,
		///<summary>ContractID</summary>
		ContractID,
		///<summary>OrganizationName</summary>
		OrganizationName,
		///<summary>OrganizationNumber</summary>
		OrganizationNumber,
		///<summary>InvoiceEntryAmount</summary>
		InvoiceEntryAmount,
		///<summary>CreationDate</summary>
		CreationDate,
		///<summary>FromDate</summary>
		FromDate,
		///<summary>InvoiceEntryCount</summary>
		InvoiceEntryCount,
		///<summary>InvoiceID</summary>
		InvoiceID,
		///<summary>InvoiceNumber</summary>
		InvoiceNumber,
		///<summary>InvoiceType</summary>
		InvoiceType,
		///<summary>ToDate</summary>
		ToDate,
		///<summary>ActiveCardCount</summary>
		ActiveCardCount,
		///<summary>BlockedCardCount</summary>
		BlockedCardCount,
		///<summary>ReplacedCardCount</summary>
		ReplacedCardCount,
		///<summary>PrintedCardCount</summary>
		PrintedCardCount,
		///<summary>NotPrintedCardCount</summary>
		NotPrintedCardCount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : PassRevenueRecognition.</summary>
	public enum PassRevenueRecognitionFieldIndex
	{
		///<summary>PassTicketID</summary>
		PassTicketID,
		///<summary>PassTransitAccountID</summary>
		PassTransitAccountID,
		///<summary>PassRevenueSettlementID</summary>
		PassRevenueSettlementID,
		///<summary>TapRevenueSettlementID</summary>
		TapRevenueSettlementID,
		///<summary>TapRevenueRecognitionID</summary>
		TapRevenueRecognitionID,
		///<summary>TapRevenueRecognitionAmount</summary>
		TapRevenueRecognitionAmount,
		///<summary>TapTransactionJournalID</summary>
		TapTransactionJournalID,
		///<summary>TapTransactionType</summary>
		TapTransactionType,
		///<summary>TapBoardingGuid</summary>
		TapBoardingGuid,
		///<summary>TapPurseCredit</summary>
		TapPurseCredit,
		///<summary>TapOriginalSingleFare</summary>
		TapOriginalSingleFare,
		///<summary>TapBaseFare</summary>
		TapBaseFare,
		///<summary>TapOperatorID</summary>
		TapOperatorID,
		///<summary>TapTicketNumber</summary>
		TapTicketNumber,
		///<summary>TapAppliedPassTicketID</summary>
		TapAppliedPassTicketID,
		///<summary>TapDeviceTime</summary>
		TapDeviceTime,
		///<summary>TapTicketID</summary>
		TapTicketID,
		///<summary>TapCancellationReferenceGuid</summary>
		TapCancellationReferenceGuid,
		///<summary>TapCancellationReference</summary>
		TapCancellationReference,
		///<summary>PassAmount</summary>
		PassAmount,
		///<summary>PassValidFrom</summary>
		PassValidFrom,
		///<summary>PassValidTo</summary>
		PassValidTo,
		///<summary>PassTicketInternalNumber</summary>
		PassTicketInternalNumber,
		///<summary>PassCloseoutPeriodID</summary>
		PassCloseoutPeriodID,
		///<summary>TapCloseoutPeriodID</summary>
		TapCloseoutPeriodID,
		///<summary>PassRevenueRecognitionID</summary>
		PassRevenueRecognitionID,
		///<summary>PassTransactionJournalID</summary>
		PassTransactionJournalID,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : PaymentJournalDetail.</summary>
	public enum PaymentJournalDetailFieldIndex
	{
		///<summary>PaymentJournalID</summary>
		PaymentJournalID,
		///<summary>PaymentJournalTransactionJournalID</summary>
		PaymentJournalTransactionJournalID,
		///<summary>PaymentJournalSaleID</summary>
		PaymentJournalSaleID,
		///<summary>PaymentType</summary>
		PaymentType,
		///<summary>PaymentJournalCreditcardNetwork</summary>
		PaymentJournalCreditcardNetwork,
		///<summary>Amount</summary>
		Amount,
		///<summary>Code</summary>
		Code,
		///<summary>Confirmed</summary>
		Confirmed,
		///<summary>ExecutionTime</summary>
		ExecutionTime,
		///<summary>ExecutionResult</summary>
		ExecutionResult,
		///<summary>State</summary>
		State,
		///<summary>Token</summary>
		Token,
		///<summary>IsPretax</summary>
		IsPretax,
		///<summary>ReferenceNumber</summary>
		ReferenceNumber,
		///<summary>PaymentOptionID</summary>
		PaymentOptionID,
		///<summary>MaskedPan</summary>
		MaskedPan,
		///<summary>ProviderReference</summary>
		ProviderReference,
		///<summary>ReconciliationState</summary>
		ReconciliationState,
		///<summary>PaymentOptionContractID</summary>
		PaymentOptionContractID,
		///<summary>PaymentMethod</summary>
		PaymentMethod,
		///<summary>PaymentOptionDescription</summary>
		PaymentOptionDescription,
		///<summary>BankingServiceToken</summary>
		BankingServiceToken,
		///<summary>PaymentOptionExpiry</summary>
		PaymentOptionExpiry,
		///<summary>PaymentOptionCreditcardNetwork</summary>
		PaymentOptionCreditcardNetwork,
		///<summary>PaymentOptionPriority</summary>
		PaymentOptionPriority,
		///<summary>PaymentOptionName</summary>
		PaymentOptionName,
		///<summary>IsTemporary</summary>
		IsTemporary,
		///<summary>PaymentOptionIsRetired</summary>
		PaymentOptionIsRetired,
		///<summary>BankConnectionDataID</summary>
		BankConnectionDataID,
		///<summary>ProspectivePaymentOptionID</summary>
		ProspectivePaymentOptionID,
		///<summary>PaymentOptionIsBlocked</summary>
		PaymentOptionIsBlocked,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransactionID</summary>
		TransactionID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>BoardingTime</summary>
		BoardingTime,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>TransactionJournalClientID</summary>
		TransactionJournalClientID,
		///<summary>Line</summary>
		Line,
		///<summary>CourseNumber</summary>
		CourseNumber,
		///<summary>RouteNumber</summary>
		RouteNumber,
		///<summary>RouteCode</summary>
		RouteCode,
		///<summary>Direction</summary>
		Direction,
		///<summary>Zone</summary>
		Zone,
		///<summary>TransactionJournalSalesChannel</summary>
		TransactionJournalSalesChannel,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>VehicleNumber</summary>
		VehicleNumber,
		///<summary>MountingPlateNumber</summary>
		MountingPlateNumber,
		///<summary>DebtorNumber</summary>
		DebtorNumber,
		///<summary>GeoLocationLongitude</summary>
		GeoLocationLongitude,
		///<summary>GeoLocationLatitude</summary>
		GeoLocationLatitude,
		///<summary>StopNumber</summary>
		StopNumber,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>FareMediaID</summary>
		FareMediaID,
		///<summary>FareMediaType</summary>
		FareMediaType,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>TransactionNumber</summary>
		TransactionNumber,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>FillLevel</summary>
		FillLevel,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>GroupSize</summary>
		GroupSize,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		///<summary>Duration</summary>
		Duration,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>WhitelistVersion</summary>
		WhitelistVersion,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid</summary>
		CancellationReferenceGuid,
		///<summary>WhitelistVersionCreated</summary>
		WhitelistVersionCreated,
		///<summary>Properties</summary>
		Properties,
		///<summary>TransactionJournalSaleID</summary>
		TransactionJournalSaleID,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>ShiftBegin</summary>
		ShiftBegin,
		///<summary>CompletionState</summary>
		CompletionState,
		///<summary>TransportState</summary>
		TransportState,
		///<summary>ProductID2</summary>
		ProductID2,
		///<summary>PurseBalance2</summary>
		PurseBalance2,
		///<summary>PurseCredit2</summary>
		PurseCredit2,
		///<summary>CreditCardAuthorizationID</summary>
		CreditCardAuthorizationID,
		///<summary>TripCounter</summary>
		TripCounter,
		///<summary>InsertTime</summary>
		InsertTime,
		///<summary>SaleTransactionID</summary>
		SaleTransactionID,
		///<summary>SaleType</summary>
		SaleType,
		///<summary>SaleDate</summary>
		SaleDate,
		///<summary>SaleClientID</summary>
		SaleClientID,
		///<summary>SaleSalesChannel</summary>
		SaleSalesChannel,
		///<summary>LocationNumber</summary>
		LocationNumber,
		///<summary>SaleDeviceNumber</summary>
		SaleDeviceNumber,
		///<summary>SalesPersonNumber</summary>
		SalesPersonNumber,
		///<summary>MerchantNumber</summary>
		MerchantNumber,
		///<summary>ReceiptReference</summary>
		ReceiptReference,
		///<summary>NetworkReference</summary>
		NetworkReference,
		///<summary>CancellationReferenceID</summary>
		CancellationReferenceID,
		///<summary>IsClosed</summary>
		IsClosed,
		///<summary>OrderID</summary>
		OrderID,
		///<summary>ExternalOrderNumber</summary>
		ExternalOrderNumber,
		///<summary>SaleContractID</summary>
		SaleContractID,
		///<summary>RefundReferenceID</summary>
		RefundReferenceID,
		///<summary>IsOrganizational</summary>
		IsOrganizational,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : RecognizedLoadAndUse.</summary>
	public enum RecognizedLoadAndUseFieldIndex
	{
		///<summary>Amount</summary>
		Amount,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>Premium</summary>
		Premium,
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid</summary>
		CancellationReferenceGuid,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : RecognizedPayment.</summary>
	public enum RecognizedPaymentFieldIndex
	{
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>MerchantNumber</summary>
		MerchantNumber,
		///<summary>SaleType</summary>
		SaleType,
		///<summary>IsOrganizational</summary>
		IsOrganizational,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>PaymentType</summary>
		PaymentType,
		///<summary>PaymentRecognitionID</summary>
		PaymentRecognitionID,
		///<summary>PaymentJournalID</summary>
		PaymentJournalID,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>Amount</summary>
		Amount,
		///<summary>LastUser</summary>
		LastUser,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>TransactionCounter</summary>
		TransactionCounter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : RecognizedTap.</summary>
	public enum RecognizedTapFieldIndex
	{
		///<summary>Amount</summary>
		Amount,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>Premium</summary>
		Premium,
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid</summary>
		CancellationReferenceGuid,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>CancelledTap</summary>
		CancelledTap,
		///<summary>BoardingGuid</summary>
		BoardingGuid,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : RecognizedTapCancellation.</summary>
	public enum RecognizedTapCancellationFieldIndex
	{
		///<summary>Amount</summary>
		Amount,
		///<summary>BaseFare</summary>
		BaseFare,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>LineGroupID</summary>
		LineGroupID,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>Premium</summary>
		Premium,
		///<summary>RevenueRecognitionID</summary>
		RevenueRecognitionID,
		///<summary>RevenueSettlementID</summary>
		RevenueSettlementID,
		///<summary>TicketNumber</summary>
		TicketNumber,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>PeriodFrom</summary>
		PeriodFrom,
		///<summary>PeriodTo</summary>
		PeriodTo,
		///<summary>CloseoutType</summary>
		CloseoutType,
		///<summary>State</summary>
		State,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid</summary>
		CancellationReferenceGuid,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>Line</summary>
		Line,
		///<summary>TransactionOperatorID</summary>
		TransactionOperatorID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>LastModified</summary>
		LastModified,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : ReconciledPayment.</summary>
	public enum ReconciledPaymentFieldIndex
	{
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>MerchantNumber</summary>
		MerchantNumber,
		///<summary>SaleType</summary>
		SaleType,
		///<summary>PaymentType</summary>
		PaymentType,
		///<summary>PaymentReconciliationID</summary>
		PaymentReconciliationID,
		///<summary>PaymentJournalID</summary>
		PaymentJournalID,
		///<summary>PostingDate</summary>
		PostingDate,
		///<summary>PostingReference</summary>
		PostingReference,
		///<summary>CreditAccountNumber</summary>
		CreditAccountNumber,
		///<summary>DebitAccountNumber</summary>
		DebitAccountNumber,
		///<summary>Amount</summary>
		Amount,
		///<summary>LastUser</summary>
		LastUser,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>TransactionCounter</summary>
		TransactionCounter,
		///<summary>CloseoutPeriodID</summary>
		CloseoutPeriodID,
		///<summary>Reconciled</summary>
		Reconciled,
		///<summary>Clientid</summary>
		Clientid,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : RuleViolationDetail.</summary>
	public enum RuleViolationDetailFieldIndex
	{
		///<summary>RuleViolationID</summary>
		RuleViolationID,
		///<summary>RuleViolationType</summary>
		RuleViolationType,
		///<summary>ViolationValue</summary>
		ViolationValue,
		///<summary>Message</summary>
		Message,
		///<summary>ExecutionTime</summary>
		ExecutionTime,
		///<summary>RuleViolationText</summary>
		RuleViolationText,
		///<summary>RuleViolationProvider</summary>
		RuleViolationProvider,
		///<summary>IsResolved</summary>
		IsResolved,
		///<summary>AggregationFrom</summary>
		AggregationFrom,
		///<summary>AggregationTo</summary>
		AggregationTo,
		///<summary>RuleViolationSourceType</summary>
		RuleViolationSourceType,
		///<summary>CardID</summary>
		CardID,
		///<summary>SerialNumber</summary>
		SerialNumber,
		///<summary>PrintedNumber</summary>
		PrintedNumber,
		///<summary>CardState</summary>
		CardState,
		///<summary>CardLastUsed</summary>
		CardLastUsed,
		///<summary>CardExpiration</summary>
		CardExpiration,
		///<summary>CardIsBlocked</summary>
		CardIsBlocked,
		///<summary>CardBlockingReason</summary>
		CardBlockingReason,
		///<summary>CardBlockingComment</summary>
		CardBlockingComment,
		///<summary>CardType</summary>
		CardType,
		///<summary>FareMediaID</summary>
		FareMediaID,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TicketType</summary>
		TicketType,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		///<summary>ProductIsCanceled</summary>
		ProductIsCanceled,
		///<summary>Price</summary>
		Price,
		///<summary>ProductIsBlocked</summary>
		ProductIsBlocked,
		///<summary>ProductBlockingReason</summary>
		ProductBlockingReason,
		///<summary>ProductBlockingDate</summary>
		ProductBlockingDate,
		///<summary>ProductCancellationDate</summary>
		ProductCancellationDate,
		///<summary>PaymentOptionID</summary>
		PaymentOptionID,
		///<summary>ContractID</summary>
		ContractID,
		///<summary>PaymentMethod</summary>
		PaymentMethod,
		///<summary>PaymentOptionDescription</summary>
		PaymentOptionDescription,
		///<summary>BankingServiceToken</summary>
		BankingServiceToken,
		///<summary>PaymentOptionExpiry</summary>
		PaymentOptionExpiry,
		///<summary>PaymentOptionCreditCardNetwork</summary>
		PaymentOptionCreditCardNetwork,
		///<summary>PaymentOptionPriority</summary>
		PaymentOptionPriority,
		///<summary>PaymentOptionName</summary>
		PaymentOptionName,
		///<summary>IsTemporary</summary>
		IsTemporary,
		///<summary>PaymentOptionIsRetired</summary>
		PaymentOptionIsRetired,
		///<summary>BankConnectionDataID</summary>
		BankConnectionDataID,
		///<summary>ProspectivePaymentOptionID</summary>
		ProspectivePaymentOptionID,
		///<summary>PaymentOptionIsBlocked</summary>
		PaymentOptionIsBlocked,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : SaleHistory.</summary>
	public enum SaleHistoryFieldIndex
	{
		///<summary>SaleID</summary>
		SaleID,
		///<summary>SaleTransactionID</summary>
		SaleTransactionID,
		///<summary>ReceiptReference</summary>
		ReceiptReference,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>SaleType</summary>
		SaleType,
		///<summary>LocationNumber</summary>
		LocationNumber,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>SalesPersonNumber</summary>
		SalesPersonNumber,
		///<summary>MerchantNumber</summary>
		MerchantNumber,
		///<summary>NetworkReference</summary>
		NetworkReference,
		///<summary>SaleDate</summary>
		SaleDate,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>AccountID</summary>
		AccountID,
		///<summary>SecondaryAccountID</summary>
		SecondaryAccountID,
		///<summary>ExternalOrderNumber</summary>
		ExternalOrderNumber,
		///<summary>OrderID</summary>
		OrderID,
		///<summary>Notes</summary>
		Notes,
		///<summary>State</summary>
		State,
		///<summary>OrderNumber</summary>
		OrderNumber,
		///<summary>IsCanceled</summary>
		IsCanceled,
		///<summary>Amount</summary>
		Amount,
		///<summary>LastCustomer</summary>
		LastCustomer,
		///<summary>SalesPersonEmail</summary>
		SalesPersonEmail,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : SaleHistoryDetail.</summary>
	public enum SaleHistoryDetailFieldIndex
	{
		///<summary>SaleID</summary>
		SaleID,
		///<summary>SaleTransactionID</summary>
		SaleTransactionID,
		///<summary>ReceiptReference</summary>
		ReceiptReference,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>SaleType</summary>
		SaleType,
		///<summary>LocationNumber</summary>
		LocationNumber,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>SalesPersonNumber</summary>
		SalesPersonNumber,
		///<summary>MerchantNumber</summary>
		MerchantNumber,
		///<summary>NetworkReference</summary>
		NetworkReference,
		///<summary>SaleDate</summary>
		SaleDate,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>AccountID</summary>
		AccountID,
		///<summary>SecondaryAccountID</summary>
		SecondaryAccountID,
		///<summary>ExternalOrderNumber</summary>
		ExternalOrderNumber,
		///<summary>OrderID</summary>
		OrderID,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>InternalNumber</summary>
		InternalNumber,
		///<summary>Price</summary>
		Price,
		///<summary>State</summary>
		State,
		///<summary>OrderNumber</summary>
		OrderNumber,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>Quantity</summary>
		Quantity,
		///<summary>FulfilledQuantity</summary>
		FulfilledQuantity,
		///<summary>OrderDetailID</summary>
		OrderDetailID,
		///<summary>ParentOrderDetailID</summary>
		ParentOrderDetailID,
		///<summary>VanpoolGroupID</summary>
		VanpoolGroupID,
		///<summary>Notes</summary>
		Notes,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : StudentCardPrint.</summary>
	public enum StudentCardPrintFieldIndex
	{
		///<summary>CardHolderID</summary>
		CardHolderID,
		///<summary>SchoolYearName</summary>
		SchoolYearName,
		///<summary>SchoolYearID</summary>
		SchoolYearID,
		///<summary>CardHolderFirstName</summary>
		CardHolderFirstName,
		///<summary>CardHolderLastName</summary>
		CardHolderLastName,
		///<summary>CardHolderDateOfBirth</summary>
		CardHolderDateOfBirth,
		///<summary>CardHolderGender</summary>
		CardHolderGender,
		///<summary>SchoolPrintName</summary>
		SchoolPrintName,
		///<summary>SchoolNumber</summary>
		SchoolNumber,
		///<summary>OrdererNumber</summary>
		OrdererNumber,
		///<summary>OrdererPrintName</summary>
		OrdererPrintName,
		///<summary>OrdererOrganizationID</summary>
		OrdererOrganizationID,
		///<summary>NumberOfCardPrints</summary>
		NumberOfCardPrints,
		///<summary>CardID</summary>
		CardID,
		///<summary>CardState</summary>
		CardState,
		///<summary>CardExpiration</summary>
		CardExpiration,
		///<summary>CardSerialNumber</summary>
		CardSerialNumber,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>TariffID</summary>
		TariffID,
		///<summary>CardPrintedNumber</summary>
		CardPrintedNumber,
		///<summary>StartStreet</summary>
		StartStreet,
		///<summary>StartStreetNumber</summary>
		StartStreetNumber,
		///<summary>StopStreet</summary>
		StopStreet,
		///<summary>StopStreetNumber</summary>
		StopStreetNumber,
		///<summary>RelationFrom</summary>
		RelationFrom,
		///<summary>RelationTo</summary>
		RelationTo,
		///<summary>FareMatrixEntryPriority</summary>
		FareMatrixEntryPriority,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : SurveyReply.</summary>
	public enum SurveyReplyFieldIndex
	{
		///<summary>PersonID</summary>
		PersonID,
		///<summary>PersonFirstName</summary>
		PersonFirstName,
		///<summary>PersonLastName</summary>
		PersonLastName,
		///<summary>SurveyID</summary>
		SurveyID,
		///<summary>SurveyName</summary>
		SurveyName,
		///<summary>SurveyDescription</summary>
		SurveyDescription,
		///<summary>QuestionID</summary>
		QuestionID,
		///<summary>QuestionLanguage</summary>
		QuestionLanguage,
		///<summary>QuestionText</summary>
		QuestionText,
		///<summary>ChoiceID</summary>
		ChoiceID,
		///<summary>ChoiceText</summary>
		ChoiceText,
		///<summary>ChoiceFollowUpQuestionID</summary>
		ChoiceFollowUpQuestionID,
		///<summary>ChoiceFreeText</summary>
		ChoiceFreeText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : TransactionHistory.</summary>
	public enum TransactionHistoryFieldIndex
	{
		///<summary>CardID</summary>
		CardID,
		///<summary>CardTransactionNo</summary>
		CardTransactionNo,
		///<summary>CardBalance</summary>
		CardBalance,
		///<summary>CardCredit</summary>
		CardCredit,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>DeviceBookingState</summary>
		DeviceBookingState,
		///<summary>Distance</summary>
		Distance,
		///<summary>ExternalTicketName</summary>
		ExternalTicketName,
		///<summary>FareStage</summary>
		FareStage,
		///<summary>Factor</summary>
		Factor,
		///<summary>FromTariffZone</summary>
		FromTariffZone,
		///<summary>LineName</summary>
		LineName,
		///<summary>PayCardNumber</summary>
		PayCardNumber,
		///<summary>Price</summary>
		Price,
		///<summary>RouteNumber</summary>
		RouteNumber,
		///<summary>TransactionID</summary>
		TransactionID,
		///<summary>TripDateTime</summary>
		TripDateTime,
		///<summary>ImportDateTime</summary>
		ImportDateTime,
		///<summary>TypeID</summary>
		TypeID,
		///<summary>TypeName</summary>
		TypeName,
		///<summary>ValueOfRide</summary>
		ValueOfRide,
		///<summary>FromStopNumber</summary>
		FromStopNumber,
		///<summary>StopCode</summary>
		StopCode,
		///<summary>Direction</summary>
		Direction,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>LocationNumber</summary>
		LocationNumber,
		///<summary>StopName</summary>
		StopName,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidUntil</summary>
		ValidUntil,
		///<summary>DevicePaymentMethod</summary>
		DevicePaymentMethod,
		///<summary>DebtorNumber</summary>
		DebtorNumber,
		///<summary>RemainingRides</summary>
		RemainingRides,
		///<summary>OrganizationID</summary>
		OrganizationID,
		///<summary>CompanyName</summary>
		CompanyName,
		///<summary>SalesChannel</summary>
		SalesChannel,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : TransactionJournalDetail.</summary>
	public enum TransactionJournalDetailFieldIndex
	{
		///<summary>TransactionJournalID</summary>
		TransactionJournalID,
		///<summary>TransactionID</summary>
		TransactionID,
		///<summary>DeviceTime</summary>
		DeviceTime,
		///<summary>BoardingTime</summary>
		BoardingTime,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>Line</summary>
		Line,
		///<summary>CourseNumber</summary>
		CourseNumber,
		///<summary>RouteNumber</summary>
		RouteNumber,
		///<summary>RouteCode</summary>
		RouteCode,
		///<summary>Direction</summary>
		Direction,
		///<summary>Zone</summary>
		Zone,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>VehicleNumber</summary>
		VehicleNumber,
		///<summary>MountingPlateNumber</summary>
		MountingPlateNumber,
		///<summary>DebtorNumber</summary>
		DebtorNumber,
		///<summary>GeoLocationLongitude</summary>
		GeoLocationLongitude,
		///<summary>GeoLocationLatitude</summary>
		GeoLocationLatitude,
		///<summary>StopNumber</summary>
		StopNumber,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>PrintedNumber</summary>
		PrintedNumber,
		///<summary>SerialNumber</summary>
		SerialNumber,
		///<summary>FareMediaID</summary>
		FareMediaID,
		///<summary>FareMediaType</summary>
		FareMediaType,
		///<summary>ProductID</summary>
		ProductID,
		///<summary>TicketID</summary>
		TicketID,
		///<summary>FareAmount</summary>
		FareAmount,
		///<summary>CustomerGroup</summary>
		CustomerGroup,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>TransactionNumber</summary>
		TransactionNumber,
		///<summary>ResultType</summary>
		ResultType,
		///<summary>FillLevel</summary>
		FillLevel,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>GroupSize</summary>
		GroupSize,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		///<summary>Duration</summary>
		Duration,
		///<summary>TariffDate</summary>
		TariffDate,
		///<summary>TariffVersion</summary>
		TariffVersion,
		///<summary>TicketInternalNumber</summary>
		TicketInternalNumber,
		///<summary>WhitelistVersion</summary>
		WhitelistVersion,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>CancellationReferenceGuid</summary>
		CancellationReferenceGuid,
		///<summary>LastUser</summary>
		LastUser,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>TransactionCounter</summary>
		TransactionCounter,
		///<summary>WhitelistVersionCreated</summary>
		WhitelistVersionCreated,
		///<summary>Properties</summary>
		Properties,
		///<summary>SaleID</summary>
		SaleID,
		///<summary>TripTicketInternalNumber</summary>
		TripTicketInternalNumber,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>ShiftBegin</summary>
		ShiftBegin,
		///<summary>CompletionState</summary>
		CompletionState,
		///<summary>TransportState</summary>
		TransportState,
		///<summary>ProductID2</summary>
		ProductID2,
		///<summary>PurseBalance2</summary>
		PurseBalance2,
		///<summary>PurseCredit2</summary>
		PurseCredit2,
		///<summary>CreditCardAuthorizationID</summary>
		CreditCardAuthorizationID,
		///<summary>TripCounter</summary>
		TripCounter,
		///<summary>InsertTime</summary>
		InsertTime,
		///<summary>IsVoiceEnabled</summary>
		IsVoiceEnabled,
		///<summary>IsTraining</summary>
		IsTraining,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : TransactionJournalHistory.</summary>
	public enum TransactionJournalHistoryFieldIndex
	{
		///<summary>TransactionID</summary>
		TransactionID,
		///<summary>OperatorID</summary>
		OperatorID,
		///<summary>SalesChannelID</summary>
		SalesChannelID,
		///<summary>TransactionType</summary>
		TransactionType,
		///<summary>Timestamp</summary>
		Timestamp,
		///<summary>TicketExternalNumber</summary>
		TicketExternalNumber,
		///<summary>Result</summary>
		Result,
		///<summary>PurseCredit</summary>
		PurseCredit,
		///<summary>PurseBalance</summary>
		PurseBalance,
		///<summary>PurseCreditPreTax</summary>
		PurseCreditPreTax,
		///<summary>PurseBalancePreTax</summary>
		PurseBalancePreTax,
		///<summary>Price</summary>
		Price,
		///<summary>ValidFrom</summary>
		ValidFrom,
		///<summary>ValidTo</summary>
		ValidTo,
		///<summary>NumberOfPersons</summary>
		NumberOfPersons,
		///<summary>ExternalTransactionIdentifier</summary>
		ExternalTransactionIdentifier,
		///<summary>StopNumber</summary>
		StopNumber,
		///<summary>StopCode</summary>
		StopCode,
		///<summary>StopName</summary>
		StopName,
		///<summary>LineNumber</summary>
		LineNumber,
		///<summary>LineCode</summary>
		LineCode,
		///<summary>LineName</summary>
		LineName,
		///<summary>VehicleNumber</summary>
		VehicleNumber,
		///<summary>DeviceNumber</summary>
		DeviceNumber,
		///<summary>DeviceName</summary>
		DeviceName,
		///<summary>DeviceDescription</summary>
		DeviceDescription,
		///<summary>AppliedCapping</summary>
		AppliedCapping,
		///<summary>TicketName</summary>
		TicketName,
		///<summary>InsertTime</summary>
		InsertTime,
		///<summary>TransitAccountID</summary>
		TransitAccountID,
		///<summary>CancellationReference</summary>
		CancellationReference,
		///<summary>HasCancellationReference</summary>
		HasCancellationReference,
		///<summary>TokenCardSerialNumber</summary>
		TokenCardSerialNumber,
		///<summary>SalesPersonNumber</summary>
		SalesPersonNumber,
		///<summary>ReceiptReference</summary>
		ReceiptReference,
		///<summary>Notes</summary>
		Notes,
		///<summary>TransferredFromCardID</summary>
		TransferredFromCardID,
		///<summary>FareScaleName</summary>
		FareScaleName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : UserClaim.</summary>
	public enum UserClaimFieldIndex
	{
		///<summary>ClientID</summary>
		ClientID,
		///<summary>UserShortName</summary>
		UserShortName,
		///<summary>ResourceID</summary>
		ResourceID,
		///<summary>URI</summary>
		URI,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : UserDebtorNumber.</summary>
	public enum UserDebtorNumberFieldIndex
	{
		///<summary>ClientID</summary>
		ClientID,
		///<summary>UserShortName</summary>
		UserShortName,
		///<summary>CompanyName</summary>
		CompanyName,
		///<summary>DebtorNumber</summary>
		DebtorNumber,
		///<summary>TrainingLevel</summary>
		TrainingLevel,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : UserRight.</summary>
	public enum UserRightFieldIndex
	{
		///<summary>ClientID</summary>
		ClientID,
		///<summary>UserShortName</summary>
		UserShortName,
		///<summary>Companyname</summary>
		Companyname,
		///<summary>ResourceID</summary>
		ResourceID,
		///<summary>LevelOfRight</summary>
		LevelOfRight,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : ValidatedShifts.</summary>
	public enum ValidatedShiftsFieldIndex
	{
		///<summary>ShiftId</summary>
		ShiftId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access Typed View EntityFields in the IEntityFields collection for the typed view : WorkItemOverview.</summary>
	public enum WorkItemOverviewFieldIndex
	{
		///<summary>WorkItemID</summary>
		WorkItemID,
		///<summary>CardID</summary>
		CardID,
		///<summary>FareMediaID</summary>
		FareMediaID,
		///<summary>PrintedNumber</summary>
		PrintedNumber,
		///<summary>OrderID</summary>
		OrderID,
		///<summary>ContractID</summary>
		ContractID,
		///<summary>ContractNumber</summary>
		ContractNumber,
		///<summary>OrganizationID</summary>
		OrganizationID,
		///<summary>WorkItemSubjectID</summary>
		WorkItemSubjectID,
		///<summary>TypeDiscriminator</summary>
		TypeDiscriminator,
		///<summary>Title</summary>
		Title,
		///<summary>Memo</summary>
		Memo,
		///<summary>Source</summary>
		Source,
		///<summary>State</summary>
		State,
		///<summary>Assignee</summary>
		Assignee,
		///<summary>CreatedBy</summary>
		CreatedBy,
		///<summary>Created</summary>
		Created,
		///<summary>Assigned</summary>
		Assigned,
		///<summary>Executed</summary>
		Executed,
		///<summary>LastModified</summary>
		LastModified,
		///<summary>ClientID</summary>
		ClientID,
		///<summary>ClientName</summary>
		ClientName,
		///<summary>OrderNumber</summary>
		OrderNumber,
		/// <summary></summary>
		AmountOfFields
	}


	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>AbortedTransaction</summary>
		AbortedTransactionEntity,
		///<summary>AccountBalance</summary>
		AccountBalanceEntity,
		///<summary>AccountEntry</summary>
		AccountEntryEntity,
		///<summary>AccountEntryNumber</summary>
		AccountEntryNumberEntity,
		///<summary>AccountPostingKey</summary>
		AccountPostingKeyEntity,
		///<summary>Application</summary>
		ApplicationEntity,
		///<summary>ApplicationVersion</summary>
		ApplicationVersionEntity,
		///<summary>Apportionment</summary>
		ApportionmentEntity,
		///<summary>ApportionmentResult</summary>
		ApportionmentResultEntity,
		///<summary>AreaList</summary>
		AreaListEntity,
		///<summary>AreaListElement</summary>
		AreaListElementEntity,
		///<summary>AreaListElementGroup</summary>
		AreaListElementGroupEntity,
		///<summary>AreaType</summary>
		AreaTypeEntity,
		///<summary>Attribute</summary>
		AttributeEntity,
		///<summary>AttributeBitmapLayoutObject</summary>
		AttributeBitmapLayoutObjectEntity,
		///<summary>AttributeTextLayoutObject</summary>
		AttributeTextLayoutObjectEntity,
		///<summary>AttributeValue</summary>
		AttributeValueEntity,
		///<summary>AttributeValueList</summary>
		AttributeValueListEntity,
		///<summary>Automat</summary>
		AutomatEntity,
		///<summary>Bank</summary>
		BankEntity,
		///<summary>BinaryData</summary>
		BinaryDataEntity,
		///<summary>BlockingReason</summary>
		BlockingReasonEntity,
		///<summary>BusinessRule</summary>
		BusinessRuleEntity,
		///<summary>BusinessRuleClause</summary>
		BusinessRuleClauseEntity,
		///<summary>BusinessRuleCondition</summary>
		BusinessRuleConditionEntity,
		///<summary>BusinessRuleResult</summary>
		BusinessRuleResultEntity,
		///<summary>BusinessRuleType</summary>
		BusinessRuleTypeEntity,
		///<summary>BusinessRuleTypeToVariable</summary>
		BusinessRuleTypeToVariableEntity,
		///<summary>BusinessRuleVariable</summary>
		BusinessRuleVariableEntity,
		///<summary>Calendar</summary>
		CalendarEntity,
		///<summary>CalendarEntry</summary>
		CalendarEntryEntity,
		///<summary>CardActionAttribute</summary>
		CardActionAttributeEntity,
		///<summary>CardActionRequest</summary>
		CardActionRequestEntity,
		///<summary>CardActionRequestType</summary>
		CardActionRequestTypeEntity,
		///<summary>CardChipType</summary>
		CardChipTypeEntity,
		///<summary>CardState</summary>
		CardStateEntity,
		///<summary>CardTicket</summary>
		CardTicketEntity,
		///<summary>CardTicketToTicket</summary>
		CardTicketToTicketEntity,
		///<summary>CashService</summary>
		CashServiceEntity,
		///<summary>CashServiceBalance</summary>
		CashServiceBalanceEntity,
		///<summary>CashServiceData</summary>
		CashServiceDataEntity,
		///<summary>CashUnit</summary>
		CashUnitEntity,
		///<summary>Choice</summary>
		ChoiceEntity,
		///<summary>Clearing</summary>
		ClearingEntity,
		///<summary>ClearingClassification</summary>
		ClearingClassificationEntity,
		///<summary>ClearingDetail</summary>
		ClearingDetailEntity,
		///<summary>ClearingResult</summary>
		ClearingResultEntity,
		///<summary>ClearingResultLevel</summary>
		ClearingResultLevelEntity,
		///<summary>ClearingState</summary>
		ClearingStateEntity,
		///<summary>ClearingSum</summary>
		ClearingSumEntity,
		///<summary>ClearingTransaction</summary>
		ClearingTransactionEntity,
		///<summary>Client</summary>
		ClientEntity,
		///<summary>ClientAdaptedLayoutObject</summary>
		ClientAdaptedLayoutObjectEntity,
		///<summary>Component</summary>
		ComponentEntity,
		///<summary>ComponentClearing</summary>
		ComponentClearingEntity,
		///<summary>ComponentFilling</summary>
		ComponentFillingEntity,
		///<summary>ComponentState</summary>
		ComponentStateEntity,
		///<summary>ComponentType</summary>
		ComponentTypeEntity,
		///<summary>ConditionalSubPageLayoutObject</summary>
		ConditionalSubPageLayoutObjectEntity,
		///<summary>CreditScreening</summary>
		CreditScreeningEntity,
		///<summary>DayType</summary>
		DayTypeEntity,
		///<summary>Debtor</summary>
		DebtorEntity,
		///<summary>DebtorCard</summary>
		DebtorCardEntity,
		///<summary>DebtorCardHistory</summary>
		DebtorCardHistoryEntity,
		///<summary>DefaultPin</summary>
		DefaultPinEntity,
		///<summary>Depot</summary>
		DepotEntity,
		///<summary>Device</summary>
		DeviceEntity,
		///<summary>DeviceBookingState</summary>
		DeviceBookingStateEntity,
		///<summary>DeviceClass</summary>
		DeviceClassEntity,
		///<summary>DevicePaymentMethod</summary>
		DevicePaymentMethodEntity,
		///<summary>Direction</summary>
		DirectionEntity,
		///<summary>DunningLevel</summary>
		DunningLevelEntity,
		///<summary>Eticket</summary>
		EticketEntity,
		///<summary>ExportInfo</summary>
		ExportInfoEntity,
		///<summary>ExternalCard</summary>
		ExternalCardEntity,
		///<summary>ExternalData</summary>
		ExternalDataEntity,
		///<summary>ExternalEffort</summary>
		ExternalEffortEntity,
		///<summary>ExternalPacket</summary>
		ExternalPacketEntity,
		///<summary>ExternalPacketEffort</summary>
		ExternalPacketEffortEntity,
		///<summary>ExternalType</summary>
		ExternalTypeEntity,
		///<summary>FareEvasionCategory</summary>
		FareEvasionCategoryEntity,
		///<summary>FareEvasionReason</summary>
		FareEvasionReasonEntity,
		///<summary>FareEvasionReasonToCategory</summary>
		FareEvasionReasonToCategoryEntity,
		///<summary>FareMatrix</summary>
		FareMatrixEntity,
		///<summary>FareMatrixEntry</summary>
		FareMatrixEntryEntity,
		///<summary>FareStage</summary>
		FareStageEntity,
		///<summary>FareStageAlias</summary>
		FareStageAliasEntity,
		///<summary>FareStageHierarchieLevel</summary>
		FareStageHierarchieLevelEntity,
		///<summary>FareStageList</summary>
		FareStageListEntity,
		///<summary>FareStageType</summary>
		FareStageTypeEntity,
		///<summary>FareTable</summary>
		FareTableEntity,
		///<summary>FareTableEntry</summary>
		FareTableEntryEntity,
		///<summary>FixedBitmapLayoutObject</summary>
		FixedBitmapLayoutObjectEntity,
		///<summary>FixedTextLayoutObject</summary>
		FixedTextLayoutObjectEntity,
		///<summary>GuiDef</summary>
		GuiDefEntity,
		///<summary>KeyAttributeTransfrom</summary>
		KeyAttributeTransfromEntity,
		///<summary>KeySelectionMode</summary>
		KeySelectionModeEntity,
		///<summary>KVVSubscription</summary>
		KVVSubscriptionEntity,
		///<summary>Language</summary>
		LanguageEntity,
		///<summary>Layout</summary>
		LayoutEntity,
		///<summary>Line</summary>
		LineEntity,
		///<summary>LineGroup</summary>
		LineGroupEntity,
		///<summary>LineGroupToLineGroup</summary>
		LineGroupToLineGroupEntity,
		///<summary>LineToLineGroup</summary>
		LineToLineGroupEntity,
		///<summary>ListLayoutObject</summary>
		ListLayoutObjectEntity,
		///<summary>ListLayoutType</summary>
		ListLayoutTypeEntity,
		///<summary>Logo</summary>
		LogoEntity,
		///<summary>Net</summary>
		NetEntity,
		///<summary>NumberRange</summary>
		NumberRangeEntity,
		///<summary>Operator</summary>
		OperatorEntity,
		///<summary>OutputDevice</summary>
		OutputDeviceEntity,
		///<summary>PageContentGroup</summary>
		PageContentGroupEntity,
		///<summary>PageContentGroupToContent</summary>
		PageContentGroupToContentEntity,
		///<summary>Panel</summary>
		PanelEntity,
		///<summary>ParameterAttributeValue</summary>
		ParameterAttributeValueEntity,
		///<summary>ParameterFareStage</summary>
		ParameterFareStageEntity,
		///<summary>ParameterTariff</summary>
		ParameterTariffEntity,
		///<summary>ParameterTicket</summary>
		ParameterTicketEntity,
		///<summary>PaymentDetail</summary>
		PaymentDetailEntity,
		///<summary>PaymentInterval</summary>
		PaymentIntervalEntity,
		///<summary>PointOfSale</summary>
		PointOfSaleEntity,
		///<summary>PredefinedKey</summary>
		PredefinedKeyEntity,
		///<summary>PriceType</summary>
		PriceTypeEntity,
		///<summary>PrimalKey</summary>
		PrimalKeyEntity,
		///<summary>PrintText</summary>
		PrintTextEntity,
		///<summary>ProductOnCard</summary>
		ProductOnCardEntity,
		///<summary>Protocol</summary>
		ProtocolEntity,
		///<summary>ProtocolAction</summary>
		ProtocolActionEntity,
		///<summary>ProtocolFunction</summary>
		ProtocolFunctionEntity,
		///<summary>ProtocolFunctionGroup</summary>
		ProtocolFunctionGroupEntity,
		///<summary>ProtocolMessage</summary>
		ProtocolMessageEntity,
		///<summary>Qualification</summary>
		QualificationEntity,
		///<summary>Responsibility</summary>
		ResponsibilityEntity,
		///<summary>RevenueTransactionType</summary>
		RevenueTransactionTypeEntity,
		///<summary>RmPayment</summary>
		RmPaymentEntity,
		///<summary>Route</summary>
		RouteEntity,
		///<summary>RouteName</summary>
		RouteNameEntity,
		///<summary>RuleCapping</summary>
		RuleCappingEntity,
		///<summary>RuleCappingToFtEntry</summary>
		RuleCappingToFtEntryEntity,
		///<summary>RuleCappingToTicket</summary>
		RuleCappingToTicketEntity,
		///<summary>RulePeriod</summary>
		RulePeriodEntity,
		///<summary>RuleType</summary>
		RuleTypeEntity,
		///<summary>Salutation</summary>
		SalutationEntity,
		///<summary>ServiceAllocation</summary>
		ServiceAllocationEntity,
		///<summary>ServiceIdToCard</summary>
		ServiceIdToCardEntity,
		///<summary>ServicePercentage</summary>
		ServicePercentageEntity,
		///<summary>Shift</summary>
		ShiftEntity,
		///<summary>ShiftInventory</summary>
		ShiftInventoryEntity,
		///<summary>ShiftState</summary>
		ShiftStateEntity,
		///<summary>ShortDistance</summary>
		ShortDistanceEntity,
		///<summary>SpecialReceipt</summary>
		SpecialReceiptEntity,
		///<summary>Stop</summary>
		StopEntity,
		///<summary>SystemField</summary>
		SystemFieldEntity,
		///<summary>SystemFieldBarcodeLayoutObject</summary>
		SystemFieldBarcodeLayoutObjectEntity,
		///<summary>SystemFieldDynamicGraphicLayoutObject</summary>
		SystemFieldDynamicGraphicLayoutObjectEntity,
		///<summary>SystemFieldTextLayoutObject</summary>
		SystemFieldTextLayoutObjectEntity,
		///<summary>SystemText</summary>
		SystemTextEntity,
		///<summary>Tariff</summary>
		TariffEntity,
		///<summary>TariffParameter</summary>
		TariffParameterEntity,
		///<summary>TariffRelease</summary>
		TariffReleaseEntity,
		///<summary>TemporalType</summary>
		TemporalTypeEntity,
		///<summary>Ticket</summary>
		TicketEntity,
		///<summary>TicketCancellationType</summary>
		TicketCancellationTypeEntity,
		///<summary>TicketCategory</summary>
		TicketCategoryEntity,
		///<summary>TicketDayType</summary>
		TicketDayTypeEntity,
		///<summary>TicketDeviceClass</summary>
		TicketDeviceClassEntity,
		///<summary>TicketDeviceClassOutputDevice</summary>
		TicketDeviceClassOutputDeviceEntity,
		///<summary>TicketDeviceClassPaymentMethod</summary>
		TicketDeviceClassPaymentMethodEntity,
		///<summary>TicketDevicePaymentMethod</summary>
		TicketDevicePaymentMethodEntity,
		///<summary>TicketGroup</summary>
		TicketGroupEntity,
		///<summary>TicketOrganization</summary>
		TicketOrganizationEntity,
		///<summary>TicketOutputdevice</summary>
		TicketOutputdeviceEntity,
		///<summary>TicketPaymentInterval</summary>
		TicketPaymentIntervalEntity,
		///<summary>TicketPhysicalCardType</summary>
		TicketPhysicalCardTypeEntity,
		///<summary>TicketSelectionMode</summary>
		TicketSelectionModeEntity,
		///<summary>TicketServicesPermitted</summary>
		TicketServicesPermittedEntity,
		///<summary>TicketToGroup</summary>
		TicketToGroupEntity,
		///<summary>TicketType</summary>
		TicketTypeEntity,
		///<summary>TicketVendingClient</summary>
		TicketVendingClientEntity,
		///<summary>Transaction</summary>
		TransactionEntity,
		///<summary>TransactionAdditional</summary>
		TransactionAdditionalEntity,
		///<summary>TransactionBackup</summary>
		TransactionBackupEntity,
		///<summary>TransactionExtension</summary>
		TransactionExtensionEntity,
		///<summary>Translation</summary>
		TranslationEntity,
		///<summary>TypeOfCard</summary>
		TypeOfCardEntity,
		///<summary>TypeOfUnit</summary>
		TypeOfUnitEntity,
		///<summary>Unit</summary>
		UnitEntity,
		///<summary>UserConfig</summary>
		UserConfigEntity,
		///<summary>UserGroup</summary>
		UserGroupEntity,
		///<summary>UserGroupRight</summary>
		UserGroupRightEntity,
		///<summary>UserIsMember</summary>
		UserIsMemberEntity,
		///<summary>UserKey</summary>
		UserKeyEntity,
		///<summary>UserLanguage</summary>
		UserLanguageEntity,
		///<summary>UserList</summary>
		UserListEntity,
		///<summary>UserResource</summary>
		UserResourceEntity,
		///<summary>UserStatus</summary>
		UserStatusEntity,
		///<summary>UserToWorkstation</summary>
		UserToWorkstationEntity,
		///<summary>ValidationTransaction</summary>
		ValidationTransactionEntity,
		///<summary>VarioAddress</summary>
		VarioAddressEntity,
		///<summary>VarioSettlement</summary>
		VarioSettlementEntity,
		///<summary>VarioTypeOfSettlement</summary>
		VarioTypeOfSettlementEntity,
		///<summary>VdvKeyInfo</summary>
		VdvKeyInfoEntity,
		///<summary>VdvKeySet</summary>
		VdvKeySetEntity,
		///<summary>VdvLayout</summary>
		VdvLayoutEntity,
		///<summary>VdvLayoutObject</summary>
		VdvLayoutObjectEntity,
		///<summary>VdvLoadKeyMessage</summary>
		VdvLoadKeyMessageEntity,
		///<summary>VdvProduct</summary>
		VdvProductEntity,
		///<summary>VdvSamStatus</summary>
		VdvSamStatusEntity,
		///<summary>VdvTag</summary>
		VdvTagEntity,
		///<summary>VdvTerminal</summary>
		VdvTerminalEntity,
		///<summary>VdvType</summary>
		VdvTypeEntity,
		///<summary>Workstation</summary>
		WorkstationEntity,
		///<summary>AccountingCancelRecTapCmlSettled</summary>
		AccountingCancelRecTapCmlSettledEntity,
		///<summary>AccountingCancelRecTapTabSettled</summary>
		AccountingCancelRecTapTabSettledEntity,
		///<summary>AccountingMethod</summary>
		AccountingMethodEntity,
		///<summary>AccountingMode</summary>
		AccountingModeEntity,
		///<summary>AccountingRecLoadUseCmlSettled</summary>
		AccountingRecLoadUseCmlSettledEntity,
		///<summary>AccountingRecLoadUseTabSettled</summary>
		AccountingRecLoadUseTabSettledEntity,
		///<summary>AccountingRecognizedLoadAndUse</summary>
		AccountingRecognizedLoadAndUseEntity,
		///<summary>AccountingRecognizedPayment</summary>
		AccountingRecognizedPaymentEntity,
		///<summary>AccountingRecognizedTap</summary>
		AccountingRecognizedTapEntity,
		///<summary>AccountingRecognizedTapCancellation</summary>
		AccountingRecognizedTapCancellationEntity,
		///<summary>AccountingRecognizedTapCmlSettled</summary>
		AccountingRecognizedTapCmlSettledEntity,
		///<summary>AccountingRecognizedTapTabSettled</summary>
		AccountingRecognizedTapTabSettledEntity,
		///<summary>AccountingReconciledPayment</summary>
		AccountingReconciledPaymentEntity,
		///<summary>AccountMessageSetting</summary>
		AccountMessageSettingEntity,
		///<summary>AdditionalData</summary>
		AdditionalDataEntity,
		///<summary>Address</summary>
		AddressEntity,
		///<summary>AddressBookEntry</summary>
		AddressBookEntryEntity,
		///<summary>AddressBookEntryType</summary>
		AddressBookEntryTypeEntity,
		///<summary>AddressType</summary>
		AddressTypeEntity,
		///<summary>AggregationFunction</summary>
		AggregationFunctionEntity,
		///<summary>AggregationType</summary>
		AggregationTypeEntity,
		///<summary>Apportion</summary>
		ApportionEntity,
		///<summary>ApportionPassRevenueRecognition</summary>
		ApportionPassRevenueRecognitionEntity,
		///<summary>ArchiveState</summary>
		ArchiveStateEntity,
		///<summary>AttributeToMobilityProvider</summary>
		AttributeToMobilityProviderEntity,
		///<summary>AuditRegister</summary>
		AuditRegisterEntity,
		///<summary>AuditRegisterValue</summary>
		AuditRegisterValueEntity,
		///<summary>AuditRegisterValueType</summary>
		AuditRegisterValueTypeEntity,
		///<summary>AutoloadSetting</summary>
		AutoloadSettingEntity,
		///<summary>AutoloadToPaymentOption</summary>
		AutoloadToPaymentOptionEntity,
		///<summary>BadCard</summary>
		BadCardEntity,
		///<summary>BadCheck</summary>
		BadCheckEntity,
		///<summary>BankConnectionData</summary>
		BankConnectionDataEntity,
		///<summary>BankStatement</summary>
		BankStatementEntity,
		///<summary>BankStatementRecordType</summary>
		BankStatementRecordTypeEntity,
		///<summary>BankStatementState</summary>
		BankStatementStateEntity,
		///<summary>BankStatementVerification</summary>
		BankStatementVerificationEntity,
		///<summary>Booking</summary>
		BookingEntity,
		///<summary>BookingItem</summary>
		BookingItemEntity,
		///<summary>BookingItemPayment</summary>
		BookingItemPaymentEntity,
		///<summary>BookingItemPaymentType</summary>
		BookingItemPaymentTypeEntity,
		///<summary>BookingType</summary>
		BookingTypeEntity,
		///<summary>CappingJournal</summary>
		CappingJournalEntity,
		///<summary>CappingState</summary>
		CappingStateEntity,
		///<summary>Card</summary>
		CardEntity,
		///<summary>CardBodyType</summary>
		CardBodyTypeEntity,
		///<summary>CardDormancy</summary>
		CardDormancyEntity,
		///<summary>CardEvent</summary>
		CardEventEntity,
		///<summary>CardEventType</summary>
		CardEventTypeEntity,
		///<summary>CardFulfillmentStatus</summary>
		CardFulfillmentStatusEntity,
		///<summary>CardHolder</summary>
		CardHolderEntity,
		///<summary>CardHolderOrganization</summary>
		CardHolderOrganizationEntity,
		///<summary>CardKey</summary>
		CardKeyEntity,
		///<summary>CardLiability</summary>
		CardLiabilityEntity,
		///<summary>CardLink</summary>
		CardLinkEntity,
		///<summary>CardOrderDetail</summary>
		CardOrderDetailEntity,
		///<summary>CardPhysicalDetail</summary>
		CardPhysicalDetailEntity,
		///<summary>CardPrintingType</summary>
		CardPrintingTypeEntity,
		///<summary>CardStockTransfer</summary>
		CardStockTransferEntity,
		///<summary>CardToContract</summary>
		CardToContractEntity,
		///<summary>CardToRuleViolation</summary>
		CardToRuleViolationEntity,
		///<summary>CardWorkItem</summary>
		CardWorkItemEntity,
		///<summary>Certificate</summary>
		CertificateEntity,
		///<summary>CertificateFormat</summary>
		CertificateFormatEntity,
		///<summary>CertificatePurpose</summary>
		CertificatePurposeEntity,
		///<summary>CertificateType</summary>
		CertificateTypeEntity,
		///<summary>Claim</summary>
		ClaimEntity,
		///<summary>ClientFilterType</summary>
		ClientFilterTypeEntity,
		///<summary>CloseoutPeriod</summary>
		CloseoutPeriodEntity,
		///<summary>CloseoutState</summary>
		CloseoutStateEntity,
		///<summary>CloseoutType</summary>
		CloseoutTypeEntity,
		///<summary>Comment</summary>
		CommentEntity,
		///<summary>CommentPriority</summary>
		CommentPriorityEntity,
		///<summary>CommentType</summary>
		CommentTypeEntity,
		///<summary>Configuration</summary>
		ConfigurationEntity,
		///<summary>ConfigurationDataType</summary>
		ConfigurationDataTypeEntity,
		///<summary>ConfigurationDefinition</summary>
		ConfigurationDefinitionEntity,
		///<summary>ConfigurationOverwrite</summary>
		ConfigurationOverwriteEntity,
		///<summary>ContentItem</summary>
		ContentItemEntity,
		///<summary>ContentType</summary>
		ContentTypeEntity,
		///<summary>Contract</summary>
		ContractEntity,
		///<summary>ContractAddress</summary>
		ContractAddressEntity,
		///<summary>ContractDetails</summary>
		ContractDetailsEntity,
		///<summary>ContractHistory</summary>
		ContractHistoryEntity,
		///<summary>ContractLink</summary>
		ContractLinkEntity,
		///<summary>ContractState</summary>
		ContractStateEntity,
		///<summary>ContractTermination</summary>
		ContractTerminationEntity,
		///<summary>ContractTerminationType</summary>
		ContractTerminationTypeEntity,
		///<summary>ContractToPaymentMethod</summary>
		ContractToPaymentMethodEntity,
		///<summary>ContractToProvider</summary>
		ContractToProviderEntity,
		///<summary>ContractType</summary>
		ContractTypeEntity,
		///<summary>ContractWorkItem</summary>
		ContractWorkItemEntity,
		///<summary>Coordinate</summary>
		CoordinateEntity,
		///<summary>Country</summary>
		CountryEntity,
		///<summary>CreditCardAuthorization</summary>
		CreditCardAuthorizationEntity,
		///<summary>CreditCardAuthorizationLog</summary>
		CreditCardAuthorizationLogEntity,
		///<summary>CreditCardMerchant</summary>
		CreditCardMerchantEntity,
		///<summary>CreditCardNetwork</summary>
		CreditCardNetworkEntity,
		///<summary>CreditCardTerminal</summary>
		CreditCardTerminalEntity,
		///<summary>CryptoCertificate</summary>
		CryptoCertificateEntity,
		///<summary>CryptoContent</summary>
		CryptoContentEntity,
		///<summary>CryptoCryptogramArchive</summary>
		CryptoCryptogramArchiveEntity,
		///<summary>CryptogramCertificate</summary>
		CryptogramCertificateEntity,
		///<summary>CryptogramCertificateType</summary>
		CryptogramCertificateTypeEntity,
		///<summary>CryptogramErrorType</summary>
		CryptogramErrorTypeEntity,
		///<summary>CryptogramLoadStatus</summary>
		CryptogramLoadStatusEntity,
		///<summary>CryptoOperatorActivationkey</summary>
		CryptoOperatorActivationkeyEntity,
		///<summary>CryptoQArchive</summary>
		CryptoQArchiveEntity,
		///<summary>CryptoResource</summary>
		CryptoResourceEntity,
		///<summary>CryptoResourceData</summary>
		CryptoResourceDataEntity,
		///<summary>CryptoSamSignCertificate</summary>
		CryptoSamSignCertificateEntity,
		///<summary>CryptoUicCertificate</summary>
		CryptoUicCertificateEntity,
		///<summary>CustomAttribute</summary>
		CustomAttributeEntity,
		///<summary>CustomAttributeValue</summary>
		CustomAttributeValueEntity,
		///<summary>CustomAttributeValueToPerson</summary>
		CustomAttributeValueToPersonEntity,
		///<summary>CustomerAccount</summary>
		CustomerAccountEntity,
		///<summary>CustomerAccountNotification</summary>
		CustomerAccountNotificationEntity,
		///<summary>CustomerAccountRole</summary>
		CustomerAccountRoleEntity,
		///<summary>CustomerAccountState</summary>
		CustomerAccountStateEntity,
		///<summary>CustomerAccountToVerificationAttemptType</summary>
		CustomerAccountToVerificationAttemptTypeEntity,
		///<summary>CustomerLanguage</summary>
		CustomerLanguageEntity,
		///<summary>DeviceCommunicationHistory</summary>
		DeviceCommunicationHistoryEntity,
		///<summary>DeviceReader</summary>
		DeviceReaderEntity,
		///<summary>DeviceReaderCryptoResource</summary>
		DeviceReaderCryptoResourceEntity,
		///<summary>DeviceReaderJob</summary>
		DeviceReaderJobEntity,
		///<summary>DeviceReaderKey</summary>
		DeviceReaderKeyEntity,
		///<summary>DeviceReaderKeyToCert</summary>
		DeviceReaderKeyToCertEntity,
		///<summary>DeviceReaderToCert</summary>
		DeviceReaderToCertEntity,
		///<summary>DeviceToken</summary>
		DeviceTokenEntity,
		///<summary>DeviceTokenState</summary>
		DeviceTokenStateEntity,
		///<summary>DocumentHistory</summary>
		DocumentHistoryEntity,
		///<summary>DocumentType</summary>
		DocumentTypeEntity,
		///<summary>DormancyNotification</summary>
		DormancyNotificationEntity,
		///<summary>DunningAction</summary>
		DunningActionEntity,
		///<summary>DunningLevelConfiguration</summary>
		DunningLevelConfigurationEntity,
		///<summary>DunningLevelPostingKey</summary>
		DunningLevelPostingKeyEntity,
		///<summary>DunningProcess</summary>
		DunningProcessEntity,
		///<summary>DunningToInvoice</summary>
		DunningToInvoiceEntity,
		///<summary>DunningToPosting</summary>
		DunningToPostingEntity,
		///<summary>DunningToProduct</summary>
		DunningToProductEntity,
		///<summary>EmailEvent</summary>
		EmailEventEntity,
		///<summary>EmailEventResendLock</summary>
		EmailEventResendLockEntity,
		///<summary>EmailMessage</summary>
		EmailMessageEntity,
		///<summary>Entitlement</summary>
		EntitlementEntity,
		///<summary>EntitlementToProduct</summary>
		EntitlementToProductEntity,
		///<summary>EntitlementType</summary>
		EntitlementTypeEntity,
		///<summary>EventSetting</summary>
		EventSettingEntity,
		///<summary>FareChangeCause</summary>
		FareChangeCauseEntity,
		///<summary>FareEvasionBehaviour</summary>
		FareEvasionBehaviourEntity,
		///<summary>FareEvasionIncident</summary>
		FareEvasionIncidentEntity,
		///<summary>FareEvasionInspection</summary>
		FareEvasionInspectionEntity,
		///<summary>FareEvasionState</summary>
		FareEvasionStateEntity,
		///<summary>FarePaymentError</summary>
		FarePaymentErrorEntity,
		///<summary>FarePaymentResultType</summary>
		FarePaymentResultTypeEntity,
		///<summary>Fastcard</summary>
		FastcardEntity,
		///<summary>FilterCategory</summary>
		FilterCategoryEntity,
		///<summary>FilterElement</summary>
		FilterElementEntity,
		///<summary>FilterList</summary>
		FilterListEntity,
		///<summary>FilterListElement</summary>
		FilterListElementEntity,
		///<summary>FilterType</summary>
		FilterTypeEntity,
		///<summary>FilterValue</summary>
		FilterValueEntity,
		///<summary>FilterValueSet</summary>
		FilterValueSetEntity,
		///<summary>FlexValue</summary>
		FlexValueEntity,
		///<summary>Form</summary>
		FormEntity,
		///<summary>FormTemplate</summary>
		FormTemplateEntity,
		///<summary>Frequency</summary>
		FrequencyEntity,
		///<summary>Gender</summary>
		GenderEntity,
		///<summary>IdentificationType</summary>
		IdentificationTypeEntity,
		///<summary>InspectionCriterion</summary>
		InspectionCriterionEntity,
		///<summary>InspectionReport</summary>
		InspectionReportEntity,
		///<summary>InspectionResult</summary>
		InspectionResultEntity,
		///<summary>InventoryState</summary>
		InventoryStateEntity,
		///<summary>Invoice</summary>
		InvoiceEntity,
		///<summary>InvoiceEntry</summary>
		InvoiceEntryEntity,
		///<summary>InvoiceType</summary>
		InvoiceTypeEntity,
		///<summary>Invoicing</summary>
		InvoicingEntity,
		///<summary>InvoicingFilterType</summary>
		InvoicingFilterTypeEntity,
		///<summary>InvoicingType</summary>
		InvoicingTypeEntity,
		///<summary>Job</summary>
		JobEntity,
		///<summary>JobBulkImport</summary>
		JobBulkImportEntity,
		///<summary>JobCardImport</summary>
		JobCardImportEntity,
		///<summary>JobFileType</summary>
		JobFileTypeEntity,
		///<summary>JobState</summary>
		JobStateEntity,
		///<summary>JobType</summary>
		JobTypeEntity,
		///<summary>LookupAddress</summary>
		LookupAddressEntity,
		///<summary>MediaEligibilityRequirement</summary>
		MediaEligibilityRequirementEntity,
		///<summary>MediaSalePrecondition</summary>
		MediaSalePreconditionEntity,
		///<summary>Merchant</summary>
		MerchantEntity,
		///<summary>Message</summary>
		MessageEntity,
		///<summary>MessageFormat</summary>
		MessageFormatEntity,
		///<summary>MessageType</summary>
		MessageTypeEntity,
		///<summary>MobilityProvider</summary>
		MobilityProviderEntity,
		///<summary>NotificationDevice</summary>
		NotificationDeviceEntity,
		///<summary>NotificationMessage</summary>
		NotificationMessageEntity,
		///<summary>NotificationMessageOwner</summary>
		NotificationMessageOwnerEntity,
		///<summary>NotificationMessageToCustomer</summary>
		NotificationMessageToCustomerEntity,
		///<summary>NumberGroup</summary>
		NumberGroupEntity,
		///<summary>NumberGroupRandomized</summary>
		NumberGroupRandomizedEntity,
		///<summary>NumberGroupScope</summary>
		NumberGroupScopeEntity,
		///<summary>Order</summary>
		OrderEntity,
		///<summary>OrderDetail</summary>
		OrderDetailEntity,
		///<summary>OrderDetailToCard</summary>
		OrderDetailToCardEntity,
		///<summary>OrderDetailToCardHolder</summary>
		OrderDetailToCardHolderEntity,
		///<summary>OrderHistory</summary>
		OrderHistoryEntity,
		///<summary>OrderProcessingCleanup</summary>
		OrderProcessingCleanupEntity,
		///<summary>OrderState</summary>
		OrderStateEntity,
		///<summary>OrderType</summary>
		OrderTypeEntity,
		///<summary>OrderWithTotal</summary>
		OrderWithTotalEntity,
		///<summary>OrderWorkItem</summary>
		OrderWorkItemEntity,
		///<summary>Organization</summary>
		OrganizationEntity,
		///<summary>OrganizationAddress</summary>
		OrganizationAddressEntity,
		///<summary>OrganizationParticipant</summary>
		OrganizationParticipantEntity,
		///<summary>OrganizationSubtype</summary>
		OrganizationSubtypeEntity,
		///<summary>OrganizationType</summary>
		OrganizationTypeEntity,
		///<summary>PageContent</summary>
		PageContentEntity,
		///<summary>Parameter</summary>
		ParameterEntity,
		///<summary>ParameterArchive</summary>
		ParameterArchiveEntity,
		///<summary>ParameterArchiveRelease</summary>
		ParameterArchiveReleaseEntity,
		///<summary>ParameterArchiveResultView</summary>
		ParameterArchiveResultViewEntity,
		///<summary>ParameterGroup</summary>
		ParameterGroupEntity,
		///<summary>ParameterGroupToParameter</summary>
		ParameterGroupToParameterEntity,
		///<summary>ParameterType</summary>
		ParameterTypeEntity,
		///<summary>ParameterValue</summary>
		ParameterValueEntity,
		///<summary>ParaTransAttribute</summary>
		ParaTransAttributeEntity,
		///<summary>ParaTransAttributeType</summary>
		ParaTransAttributeTypeEntity,
		///<summary>ParaTransJournal</summary>
		ParaTransJournalEntity,
		///<summary>ParaTransJournalType</summary>
		ParaTransJournalTypeEntity,
		///<summary>ParaTransPaymentType</summary>
		ParaTransPaymentTypeEntity,
		///<summary>ParticipantGroup</summary>
		ParticipantGroupEntity,
		///<summary>PassExpiryNotification</summary>
		PassExpiryNotificationEntity,
		///<summary>Payment</summary>
		PaymentEntity,
		///<summary>PaymentJournal</summary>
		PaymentJournalEntity,
		///<summary>PaymentJournalToComponent</summary>
		PaymentJournalToComponentEntity,
		///<summary>PaymentMethod</summary>
		PaymentMethodEntity,
		///<summary>PaymentModality</summary>
		PaymentModalityEntity,
		///<summary>PaymentOption</summary>
		PaymentOptionEntity,
		///<summary>PaymentProvider</summary>
		PaymentProviderEntity,
		///<summary>PaymentProviderAccount</summary>
		PaymentProviderAccountEntity,
		///<summary>PaymentRecognition</summary>
		PaymentRecognitionEntity,
		///<summary>PaymentReconciliation</summary>
		PaymentReconciliationEntity,
		///<summary>PaymentReconciliationState</summary>
		PaymentReconciliationStateEntity,
		///<summary>PaymentState</summary>
		PaymentStateEntity,
		///<summary>PendingOrder</summary>
		PendingOrderEntity,
		///<summary>Performance</summary>
		PerformanceEntity,
		///<summary>PerformanceAggregation</summary>
		PerformanceAggregationEntity,
		///<summary>PerformanceComponent</summary>
		PerformanceComponentEntity,
		///<summary>PerformanceIndicator</summary>
		PerformanceIndicatorEntity,
		///<summary>Person</summary>
		PersonEntity,
		///<summary>PersonAddress</summary>
		PersonAddressEntity,
		///<summary>Photograph</summary>
		PhotographEntity,
		///<summary>PickupLocation</summary>
		PickupLocationEntity,
		///<summary>PickupLocationToTicket</summary>
		PickupLocationToTicketEntity,
		///<summary>Posting</summary>
		PostingEntity,
		///<summary>PostingKey</summary>
		PostingKeyEntity,
		///<summary>PostingKeyCategory</summary>
		PostingKeyCategoryEntity,
		///<summary>PostingScope</summary>
		PostingScopeEntity,
		///<summary>PriceAdjustment</summary>
		PriceAdjustmentEntity,
		///<summary>PriceAdjustmentTemplate</summary>
		PriceAdjustmentTemplateEntity,
		///<summary>Printer</summary>
		PrinterEntity,
		///<summary>PrinterState</summary>
		PrinterStateEntity,
		///<summary>PrinterToUnit</summary>
		PrinterToUnitEntity,
		///<summary>PrinterType</summary>
		PrinterTypeEntity,
		///<summary>Priority</summary>
		PriorityEntity,
		///<summary>Product</summary>
		ProductEntity,
		///<summary>ProductRelation</summary>
		ProductRelationEntity,
		///<summary>ProductSubsidy</summary>
		ProductSubsidyEntity,
		///<summary>ProductTermination</summary>
		ProductTerminationEntity,
		///<summary>ProductTerminationType</summary>
		ProductTerminationTypeEntity,
		///<summary>ProductType</summary>
		ProductTypeEntity,
		///<summary>PromoCode</summary>
		PromoCodeEntity,
		///<summary>PromoCodeStatus</summary>
		PromoCodeStatusEntity,
		///<summary>ProviderAccountState</summary>
		ProviderAccountStateEntity,
		///<summary>ProvisioningState</summary>
		ProvisioningStateEntity,
		///<summary>RecipientGroup</summary>
		RecipientGroupEntity,
		///<summary>RecipientGroupMember</summary>
		RecipientGroupMemberEntity,
		///<summary>Refund</summary>
		RefundEntity,
		///<summary>RefundReason</summary>
		RefundReasonEntity,
		///<summary>RentalState</summary>
		RentalStateEntity,
		///<summary>RentalType</summary>
		RentalTypeEntity,
		///<summary>Report</summary>
		ReportEntity,
		///<summary>ReportCategory</summary>
		ReportCategoryEntity,
		///<summary>ReportDataFile</summary>
		ReportDataFileEntity,
		///<summary>ReportJob</summary>
		ReportJobEntity,
		///<summary>ReportJobResult</summary>
		ReportJobResultEntity,
		///<summary>ReportToClient</summary>
		ReportToClientEntity,
		///<summary>RevenueRecognition</summary>
		RevenueRecognitionEntity,
		///<summary>RevenueSettlement</summary>
		RevenueSettlementEntity,
		///<summary>RuleViolation</summary>
		RuleViolationEntity,
		///<summary>RuleViolationProvider</summary>
		RuleViolationProviderEntity,
		///<summary>RuleViolationSourceType</summary>
		RuleViolationSourceTypeEntity,
		///<summary>RuleViolationToTransaction</summary>
		RuleViolationToTransactionEntity,
		///<summary>RuleViolationType</summary>
		RuleViolationTypeEntity,
		///<summary>Sale</summary>
		SaleEntity,
		///<summary>SalesChannelToPaymentMethod</summary>
		SalesChannelToPaymentMethodEntity,
		///<summary>SalesRevenue</summary>
		SalesRevenueEntity,
		///<summary>SaleToComponent</summary>
		SaleToComponentEntity,
		///<summary>SaleType</summary>
		SaleTypeEntity,
		///<summary>SamModule</summary>
		SamModuleEntity,
		///<summary>SamModuleStatus</summary>
		SamModuleStatusEntity,
		///<summary>SchoolYear</summary>
		SchoolYearEntity,
		///<summary>SecurityQuestion</summary>
		SecurityQuestionEntity,
		///<summary>SecurityResponse</summary>
		SecurityResponseEntity,
		///<summary>SepaFrequencyType</summary>
		SepaFrequencyTypeEntity,
		///<summary>SepaOwnerType</summary>
		SepaOwnerTypeEntity,
		///<summary>ServerResponseTimeData</summary>
		ServerResponseTimeDataEntity,
		///<summary>ServerResponseTimeDataAggregation</summary>
		ServerResponseTimeDataAggregationEntity,
		///<summary>SettledRevenue</summary>
		SettledRevenueEntity,
		///<summary>SettlementAccount</summary>
		SettlementAccountEntity,
		///<summary>SettlementCalendar</summary>
		SettlementCalendarEntity,
		///<summary>SettlementDistributionPolicy</summary>
		SettlementDistributionPolicyEntity,
		///<summary>SettlementHoliday</summary>
		SettlementHolidayEntity,
		///<summary>SettlementOperation</summary>
		SettlementOperationEntity,
		///<summary>SettlementQuerySetting</summary>
		SettlementQuerySettingEntity,
		///<summary>SettlementQuerySettingToTicket</summary>
		SettlementQuerySettingToTicketEntity,
		///<summary>SettlementQueryValue</summary>
		SettlementQueryValueEntity,
		///<summary>SettlementReleaseState</summary>
		SettlementReleaseStateEntity,
		///<summary>SettlementResult</summary>
		SettlementResultEntity,
		///<summary>SettlementRun</summary>
		SettlementRunEntity,
		///<summary>SettlementRunValue</summary>
		SettlementRunValueEntity,
		///<summary>SettlementRunValueToVarioSettlement</summary>
		SettlementRunValueToVarioSettlementEntity,
		///<summary>SettlementSetup</summary>
		SettlementSetupEntity,
		///<summary>SettlementType</summary>
		SettlementTypeEntity,
		///<summary>Source</summary>
		SourceEntity,
		///<summary>StatementOfAccount</summary>
		StatementOfAccountEntity,
		///<summary>StateOfCountry</summary>
		StateOfCountryEntity,
		///<summary>StockTransfer</summary>
		StockTransferEntity,
		///<summary>StorageLocation</summary>
		StorageLocationEntity,
		///<summary>SubsidyLimit</summary>
		SubsidyLimitEntity,
		///<summary>SubsidyTransaction</summary>
		SubsidyTransactionEntity,
		///<summary>Survey</summary>
		SurveyEntity,
		///<summary>SurveyAnswer</summary>
		SurveyAnswerEntity,
		///<summary>SurveyChoice</summary>
		SurveyChoiceEntity,
		///<summary>SurveyDescription</summary>
		SurveyDescriptionEntity,
		///<summary>SurveyQuestion</summary>
		SurveyQuestionEntity,
		///<summary>SurveyResponse</summary>
		SurveyResponseEntity,
		///<summary>Tenant</summary>
		TenantEntity,
		///<summary>TenantHistory</summary>
		TenantHistoryEntity,
		///<summary>TenantPerson</summary>
		TenantPersonEntity,
		///<summary>TenantState</summary>
		TenantStateEntity,
		///<summary>TerminationStatus</summary>
		TerminationStatusEntity,
		///<summary>TicketAssignment</summary>
		TicketAssignmentEntity,
		///<summary>TicketAssignmentStatus</summary>
		TicketAssignmentStatusEntity,
		///<summary>TicketSerialNumber</summary>
		TicketSerialNumberEntity,
		///<summary>TicketSerialNumberStatus</summary>
		TicketSerialNumberStatusEntity,
		///<summary>TransactionJournal</summary>
		TransactionJournalEntity,
		///<summary>TransactionJournalCorrection</summary>
		TransactionJournalCorrectionEntity,
		///<summary>TransactionJourneyHistory</summary>
		TransactionJourneyHistoryEntity,
		///<summary>TransactionToInspection</summary>
		TransactionToInspectionEntity,
		///<summary>TransactionType</summary>
		TransactionTypeEntity,
		///<summary>TransportCompany</summary>
		TransportCompanyEntity,
		///<summary>TriggerType</summary>
		TriggerTypeEntity,
		///<summary>UnitCollection</summary>
		UnitCollectionEntity,
		///<summary>UnitCollectionToUnit</summary>
		UnitCollectionToUnitEntity,
		///<summary>ValidationResult</summary>
		ValidationResultEntity,
		///<summary>ValidationResultResolveType</summary>
		ValidationResultResolveTypeEntity,
		///<summary>ValidationRule</summary>
		ValidationRuleEntity,
		///<summary>ValidationRun</summary>
		ValidationRunEntity,
		///<summary>ValidationRunRule</summary>
		ValidationRunRuleEntity,
		///<summary>ValidationStatus</summary>
		ValidationStatusEntity,
		///<summary>ValidationValue</summary>
		ValidationValueEntity,
		///<summary>VdvTransaction</summary>
		VdvTransactionEntity,
		///<summary>VerificationAttempt</summary>
		VerificationAttemptEntity,
		///<summary>VerificationAttemptType</summary>
		VerificationAttemptTypeEntity,
		///<summary>VirtualCard</summary>
		VirtualCardEntity,
		///<summary>Voucher</summary>
		VoucherEntity,
		///<summary>Whitelist</summary>
		WhitelistEntity,
		///<summary>WhitelistJournal</summary>
		WhitelistJournalEntity,
		///<summary>WorkItem</summary>
		WorkItemEntity,
		///<summary>WorkItemCategory</summary>
		WorkItemCategoryEntity,
		///<summary>WorkItemHistory</summary>
		WorkItemHistoryEntity,
		///<summary>WorkItemState</summary>
		WorkItemStateEntity,
		///<summary>WorkItemSubject</summary>
		WorkItemSubjectEntity,
		///<summary>WorkItemView</summary>
		WorkItemViewEntity
	}

	/// <summary>Enum definition for all the typed view types defined in this namespace. Used by the entityfields factory.</summary>
	public enum TypedViewType
	{
		///<summary>AccCancelRecTapCmlSettled</summary>
		AccCancelRecTapCmlSettledTypedView,
		///<summary>AccCancelRecTapTabSettled</summary>
		AccCancelRecTapTabSettledTypedView,
		///<summary>AccountSearch</summary>
		AccountSearchTypedView,
		///<summary>AccRecLoadUseCmlSettled</summary>
		AccRecLoadUseCmlSettledTypedView,
		///<summary>AccRecLoadUseTabSettled</summary>
		AccRecLoadUseTabSettledTypedView,
		///<summary>AccRecognizedTapCmlSettled</summary>
		AccRecognizedTapCmlSettledTypedView,
		///<summary>AccRecognizedTapTabSettled</summary>
		AccRecognizedTapTabSettledTypedView,
		///<summary>ChangeVoucher</summary>
		ChangeVoucherTypedView,
		///<summary>DebtorAccountBalance</summary>
		DebtorAccountBalanceTypedView,
		///<summary>DebtorPostings</summary>
		DebtorPostingsTypedView,
		///<summary>FilteredAccountPostingKey</summary>
		FilteredAccountPostingKeyTypedView,
		///<summary>FrameworkOrderDetail</summary>
		FrameworkOrderDetailTypedView,
		///<summary>InvoiceDetail</summary>
		InvoiceDetailTypedView,
		///<summary>PassRevenueRecognition</summary>
		PassRevenueRecognitionTypedView,
		///<summary>PaymentJournalDetail</summary>
		PaymentJournalDetailTypedView,
		///<summary>RecognizedLoadAndUse</summary>
		RecognizedLoadAndUseTypedView,
		///<summary>RecognizedPayment</summary>
		RecognizedPaymentTypedView,
		///<summary>RecognizedTap</summary>
		RecognizedTapTypedView,
		///<summary>RecognizedTapCancellation</summary>
		RecognizedTapCancellationTypedView,
		///<summary>ReconciledPayment</summary>
		ReconciledPaymentTypedView,
		///<summary>RuleViolationDetail</summary>
		RuleViolationDetailTypedView,
		///<summary>SaleHistory</summary>
		SaleHistoryTypedView,
		///<summary>SaleHistoryDetail</summary>
		SaleHistoryDetailTypedView,
		///<summary>StudentCardPrint</summary>
		StudentCardPrintTypedView,
		///<summary>SurveyReply</summary>
		SurveyReplyTypedView,
		///<summary>TransactionHistory</summary>
		TransactionHistoryTypedView,
		///<summary>TransactionJournalDetail</summary>
		TransactionJournalDetailTypedView,
		///<summary>TransactionJournalHistory</summary>
		TransactionJournalHistoryTypedView,
		///<summary>UserClaim</summary>
		UserClaimTypedView,
		///<summary>UserDebtorNumber</summary>
		UserDebtorNumberTypedView,
		///<summary>UserRight</summary>
		UserRightTypedView,
		///<summary>ValidatedShifts</summary>
		ValidatedShiftsTypedView,
		///<summary>WorkItemOverview</summary>
		WorkItemOverviewTypedView
	}

	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

