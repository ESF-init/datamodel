﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'InvoiceDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class InvoiceDetailTypedView : TypedViewBase<InvoiceDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnSchoolYearID;
		private DataColumn _columnContractID;
		private DataColumn _columnOrganizationName;
		private DataColumn _columnOrganizationNumber;
		private DataColumn _columnInvoiceEntryAmount;
		private DataColumn _columnCreationDate;
		private DataColumn _columnFromDate;
		private DataColumn _columnInvoiceEntryCount;
		private DataColumn _columnInvoiceID;
		private DataColumn _columnInvoiceNumber;
		private DataColumn _columnInvoiceType;
		private DataColumn _columnToDate;
		private DataColumn _columnActiveCardCount;
		private DataColumn _columnBlockedCardCount;
		private DataColumn _columnReplacedCardCount;
		private DataColumn _columnPrintedCardCount;
		private DataColumn _columnNotPrintedCardCount;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 17;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static InvoiceDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public InvoiceDetailTypedView():base("InvoiceDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InvoiceDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.InvoiceDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.InvoiceDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new InvoiceDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InvoiceEntryAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreationDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FromDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InvoiceEntryCount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InvoiceID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InvoiceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InvoiceType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ToDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ActiveCardCount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BlockedCardCount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReplacedCardCount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PrintedCardCount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NotPrintedCardCount", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "InvoiceDetail";
			_columnSchoolYearID = GeneralUtils.CreateTypedDataTableColumn("SchoolYearID", @"SchoolYearID", typeof(System.Int64), this.Columns);
			_columnContractID = GeneralUtils.CreateTypedDataTableColumn("ContractID", @"ContractID", typeof(System.Int64), this.Columns);
			_columnOrganizationName = GeneralUtils.CreateTypedDataTableColumn("OrganizationName", @"OrganizationName", typeof(System.String), this.Columns);
			_columnOrganizationNumber = GeneralUtils.CreateTypedDataTableColumn("OrganizationNumber", @"OrganizationNumber", typeof(System.String), this.Columns);
			_columnInvoiceEntryAmount = GeneralUtils.CreateTypedDataTableColumn("InvoiceEntryAmount", @"InvoiceEntryAmount", typeof(System.Int64), this.Columns);
			_columnCreationDate = GeneralUtils.CreateTypedDataTableColumn("CreationDate", @"CreationDate", typeof(System.DateTime), this.Columns);
			_columnFromDate = GeneralUtils.CreateTypedDataTableColumn("FromDate", @"FromDate", typeof(System.DateTime), this.Columns);
			_columnInvoiceEntryCount = GeneralUtils.CreateTypedDataTableColumn("InvoiceEntryCount", @"InvoiceEntryCount", typeof(System.Int64), this.Columns);
			_columnInvoiceID = GeneralUtils.CreateTypedDataTableColumn("InvoiceID", @"InvoiceID", typeof(System.Int64), this.Columns);
			_columnInvoiceNumber = GeneralUtils.CreateTypedDataTableColumn("InvoiceNumber", @"InvoiceNumber", typeof(System.String), this.Columns);
			_columnInvoiceType = GeneralUtils.CreateTypedDataTableColumn("InvoiceType", @"InvoiceType", typeof(VarioSL.Entities.Enumerations.InvoiceType), this.Columns);
			_columnToDate = GeneralUtils.CreateTypedDataTableColumn("ToDate", @"ToDate", typeof(System.DateTime), this.Columns);
			_columnActiveCardCount = GeneralUtils.CreateTypedDataTableColumn("ActiveCardCount", @"ActiveCardCount", typeof(System.Decimal), this.Columns);
			_columnBlockedCardCount = GeneralUtils.CreateTypedDataTableColumn("BlockedCardCount", @"BlockedCardCount", typeof(System.Decimal), this.Columns);
			_columnReplacedCardCount = GeneralUtils.CreateTypedDataTableColumn("ReplacedCardCount", @"ReplacedCardCount", typeof(System.Decimal), this.Columns);
			_columnPrintedCardCount = GeneralUtils.CreateTypedDataTableColumn("PrintedCardCount", @"PrintedCardCount", typeof(System.Decimal), this.Columns);
			_columnNotPrintedCardCount = GeneralUtils.CreateTypedDataTableColumn("NotPrintedCardCount", @"NotPrintedCardCount", typeof(System.Decimal), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnSchoolYearID = this.Columns["SchoolYearID"];
			_columnContractID = this.Columns["ContractID"];
			_columnOrganizationName = this.Columns["OrganizationName"];
			_columnOrganizationNumber = this.Columns["OrganizationNumber"];
			_columnInvoiceEntryAmount = this.Columns["InvoiceEntryAmount"];
			_columnCreationDate = this.Columns["CreationDate"];
			_columnFromDate = this.Columns["FromDate"];
			_columnInvoiceEntryCount = this.Columns["InvoiceEntryCount"];
			_columnInvoiceID = this.Columns["InvoiceID"];
			_columnInvoiceNumber = this.Columns["InvoiceNumber"];
			_columnInvoiceType = this.Columns["InvoiceType"];
			_columnToDate = this.Columns["ToDate"];
			_columnActiveCardCount = this.Columns["ActiveCardCount"];
			_columnBlockedCardCount = this.Columns["BlockedCardCount"];
			_columnReplacedCardCount = this.Columns["ReplacedCardCount"];
			_columnPrintedCardCount = this.Columns["PrintedCardCount"];
			_columnNotPrintedCardCount = this.Columns["NotPrintedCardCount"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			InvoiceDetailTypedView cloneToReturn = ((InvoiceDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new InvoiceDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return InvoiceDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return InvoiceDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolYearID'</summary>
		internal DataColumn SchoolYearIDColumn 
		{
			get { return _columnSchoolYearID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractID'</summary>
		internal DataColumn ContractIDColumn 
		{
			get { return _columnContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationName'</summary>
		internal DataColumn OrganizationNameColumn 
		{
			get { return _columnOrganizationName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationNumber'</summary>
		internal DataColumn OrganizationNumberColumn 
		{
			get { return _columnOrganizationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InvoiceEntryAmount'</summary>
		internal DataColumn InvoiceEntryAmountColumn 
		{
			get { return _columnInvoiceEntryAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreationDate'</summary>
		internal DataColumn CreationDateColumn 
		{
			get { return _columnCreationDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FromDate'</summary>
		internal DataColumn FromDateColumn 
		{
			get { return _columnFromDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InvoiceEntryCount'</summary>
		internal DataColumn InvoiceEntryCountColumn 
		{
			get { return _columnInvoiceEntryCount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InvoiceID'</summary>
		internal DataColumn InvoiceIDColumn 
		{
			get { return _columnInvoiceID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InvoiceNumber'</summary>
		internal DataColumn InvoiceNumberColumn 
		{
			get { return _columnInvoiceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InvoiceType'</summary>
		internal DataColumn InvoiceTypeColumn 
		{
			get { return _columnInvoiceType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ToDate'</summary>
		internal DataColumn ToDateColumn 
		{
			get { return _columnToDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ActiveCardCount'</summary>
		internal DataColumn ActiveCardCountColumn 
		{
			get { return _columnActiveCardCount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BlockedCardCount'</summary>
		internal DataColumn BlockedCardCountColumn 
		{
			get { return _columnBlockedCardCount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReplacedCardCount'</summary>
		internal DataColumn ReplacedCardCountColumn 
		{
			get { return _columnReplacedCardCount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PrintedCardCount'</summary>
		internal DataColumn PrintedCardCountColumn 
		{
			get { return _columnPrintedCardCount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NotPrintedCardCount'</summary>
		internal DataColumn NotPrintedCardCountColumn 
		{
			get { return _columnNotPrintedCardCount; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable InvoiceDetail</summary>
	public partial class InvoiceDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private InvoiceDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal InvoiceDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((InvoiceDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field SchoolYearID</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."SCHOOLYEARID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SchoolYearID
		{
			get { return IsSchoolYearIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SchoolYearIDColumn]; }
			set { this[_parent.SchoolYearIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolYearID is NULL, false otherwise.</summary>
		public bool IsSchoolYearIDNull() 
		{
			return IsNull(_parent.SchoolYearIDColumn);
		}

		/// <summary>Sets the TypedView field SchoolYearID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolYearIDNull() 
		{
			this[_parent.SchoolYearIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractID</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."CONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ContractID
		{
			get { return IsContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ContractIDColumn]; }
			set { this[_parent.ContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractID is NULL, false otherwise.</summary>
		public bool IsContractIDNull() 
		{
			return IsNull(_parent.ContractIDColumn);
		}

		/// <summary>Sets the TypedView field ContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractIDNull() 
		{
			this[_parent.ContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationName</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."ORGANIZATIONNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrganizationName
		{
			get { return IsOrganizationNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationNameColumn]; }
			set { this[_parent.OrganizationNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationName is NULL, false otherwise.</summary>
		public bool IsOrganizationNameNull() 
		{
			return IsNull(_parent.OrganizationNameColumn);
		}

		/// <summary>Sets the TypedView field OrganizationName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationNameNull() 
		{
			this[_parent.OrganizationNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationNumber</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."ORGANIZATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String OrganizationNumber
		{
			get { return IsOrganizationNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationNumberColumn]; }
			set { this[_parent.OrganizationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationNumber is NULL, false otherwise.</summary>
		public bool IsOrganizationNumberNull() 
		{
			return IsNull(_parent.OrganizationNumberColumn);
		}

		/// <summary>Sets the TypedView field OrganizationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationNumberNull() 
		{
			this[_parent.OrganizationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InvoiceEntryAmount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."INVOICEENTRYAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 InvoiceEntryAmount
		{
			get { return IsInvoiceEntryAmountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.InvoiceEntryAmountColumn]; }
			set { this[_parent.InvoiceEntryAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InvoiceEntryAmount is NULL, false otherwise.</summary>
		public bool IsInvoiceEntryAmountNull() 
		{
			return IsNull(_parent.InvoiceEntryAmountColumn);
		}

		/// <summary>Sets the TypedView field InvoiceEntryAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInvoiceEntryAmountNull() 
		{
			this[_parent.InvoiceEntryAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreationDate</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."CREATIONDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CreationDate
		{
			get { return IsCreationDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CreationDateColumn]; }
			set { this[_parent.CreationDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreationDate is NULL, false otherwise.</summary>
		public bool IsCreationDateNull() 
		{
			return IsNull(_parent.CreationDateColumn);
		}

		/// <summary>Sets the TypedView field CreationDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreationDateNull() 
		{
			this[_parent.CreationDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FromDate</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."FROMDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime FromDate
		{
			get { return IsFromDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.FromDateColumn]; }
			set { this[_parent.FromDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FromDate is NULL, false otherwise.</summary>
		public bool IsFromDateNull() 
		{
			return IsNull(_parent.FromDateColumn);
		}

		/// <summary>Sets the TypedView field FromDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFromDateNull() 
		{
			this[_parent.FromDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InvoiceEntryCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."INVOICEENTRYCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 InvoiceEntryCount
		{
			get { return IsInvoiceEntryCountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.InvoiceEntryCountColumn]; }
			set { this[_parent.InvoiceEntryCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InvoiceEntryCount is NULL, false otherwise.</summary>
		public bool IsInvoiceEntryCountNull() 
		{
			return IsNull(_parent.InvoiceEntryCountColumn);
		}

		/// <summary>Sets the TypedView field InvoiceEntryCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInvoiceEntryCountNull() 
		{
			this[_parent.InvoiceEntryCountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InvoiceID</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."INVOICEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 InvoiceID
		{
			get { return IsInvoiceIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.InvoiceIDColumn]; }
			set { this[_parent.InvoiceIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InvoiceID is NULL, false otherwise.</summary>
		public bool IsInvoiceIDNull() 
		{
			return IsNull(_parent.InvoiceIDColumn);
		}

		/// <summary>Sets the TypedView field InvoiceID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInvoiceIDNull() 
		{
			this[_parent.InvoiceIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InvoiceNumber</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."INVOICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String InvoiceNumber
		{
			get { return IsInvoiceNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.InvoiceNumberColumn]; }
			set { this[_parent.InvoiceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InvoiceNumber is NULL, false otherwise.</summary>
		public bool IsInvoiceNumberNull() 
		{
			return IsNull(_parent.InvoiceNumberColumn);
		}

		/// <summary>Sets the TypedView field InvoiceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInvoiceNumberNull() 
		{
			this[_parent.InvoiceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InvoiceType</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."INVOICETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.InvoiceType InvoiceType
		{
			get { return IsInvoiceTypeNull() ? (VarioSL.Entities.Enumerations.InvoiceType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.InvoiceType)) : (VarioSL.Entities.Enumerations.InvoiceType)this[_parent.InvoiceTypeColumn]; }
			set { this[_parent.InvoiceTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InvoiceType is NULL, false otherwise.</summary>
		public bool IsInvoiceTypeNull() 
		{
			return IsNull(_parent.InvoiceTypeColumn);
		}

		/// <summary>Sets the TypedView field InvoiceType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInvoiceTypeNull() 
		{
			this[_parent.InvoiceTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ToDate</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."TODATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ToDate
		{
			get { return IsToDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ToDateColumn]; }
			set { this[_parent.ToDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ToDate is NULL, false otherwise.</summary>
		public bool IsToDateNull() 
		{
			return IsNull(_parent.ToDateColumn);
		}

		/// <summary>Sets the TypedView field ToDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetToDateNull() 
		{
			this[_parent.ToDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ActiveCardCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."ACTIVECARDCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal ActiveCardCount
		{
			get { return IsActiveCardCountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.ActiveCardCountColumn]; }
			set { this[_parent.ActiveCardCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ActiveCardCount is NULL, false otherwise.</summary>
		public bool IsActiveCardCountNull() 
		{
			return IsNull(_parent.ActiveCardCountColumn);
		}

		/// <summary>Sets the TypedView field ActiveCardCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetActiveCardCountNull() 
		{
			this[_parent.ActiveCardCountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BlockedCardCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."BLOCKEDCARDCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal BlockedCardCount
		{
			get { return IsBlockedCardCountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.BlockedCardCountColumn]; }
			set { this[_parent.BlockedCardCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BlockedCardCount is NULL, false otherwise.</summary>
		public bool IsBlockedCardCountNull() 
		{
			return IsNull(_parent.BlockedCardCountColumn);
		}

		/// <summary>Sets the TypedView field BlockedCardCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBlockedCardCountNull() 
		{
			this[_parent.BlockedCardCountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReplacedCardCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."REPLACEDCARDCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal ReplacedCardCount
		{
			get { return IsReplacedCardCountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.ReplacedCardCountColumn]; }
			set { this[_parent.ReplacedCardCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReplacedCardCount is NULL, false otherwise.</summary>
		public bool IsReplacedCardCountNull() 
		{
			return IsNull(_parent.ReplacedCardCountColumn);
		}

		/// <summary>Sets the TypedView field ReplacedCardCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReplacedCardCountNull() 
		{
			this[_parent.ReplacedCardCountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PrintedCardCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."PRINTEDCARDCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal PrintedCardCount
		{
			get { return IsPrintedCardCountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.PrintedCardCountColumn]; }
			set { this[_parent.PrintedCardCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PrintedCardCount is NULL, false otherwise.</summary>
		public bool IsPrintedCardCountNull() 
		{
			return IsNull(_parent.PrintedCardCountColumn);
		}

		/// <summary>Sets the TypedView field PrintedCardCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPrintedCardCountNull() 
		{
			this[_parent.PrintedCardCountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NotPrintedCardCount</summary>
		/// <remarks>Mapped on view field: "SL_INVOICE_VIEW"."NOTPRINTEDCARDCOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal NotPrintedCardCount
		{
			get { return IsNotPrintedCardCountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.NotPrintedCardCountColumn]; }
			set { this[_parent.NotPrintedCardCountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NotPrintedCardCount is NULL, false otherwise.</summary>
		public bool IsNotPrintedCardCountNull() 
		{
			return IsNull(_parent.NotPrintedCardCountColumn);
		}

		/// <summary>Sets the TypedView field NotPrintedCardCount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNotPrintedCardCountNull() 
		{
			this[_parent.NotPrintedCardCountColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
