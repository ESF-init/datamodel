﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'AccRecognizedTapCmlSettled'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class AccRecognizedTapCmlSettledTypedView : TypedViewBase<AccRecognizedTapCmlSettledRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnRevenueRecognitionID;
		private DataColumn _columnRevenueSettlementID;
		private DataColumn _columnSettlementType;
		private DataColumn _columnAmount;
		private DataColumn _columnPremium;
		private DataColumn _columnBaseFare;
		private DataColumn _columnTicketNumber;
		private DataColumn _columnCustomerGroup;
		private DataColumn _columnLineGroupID;
		private DataColumn _columnOperatorID;
		private DataColumn _columnCreditAccountNumber;
		private DataColumn _columnDebitAccountNumber;
		private DataColumn _columnPostingDate;
		private DataColumn _columnPostingReference;
		private DataColumn _columnIncludeInSettlement;
		private DataColumn _columnCloseoutPeriodID;
		private DataColumn _columnPeriodFrom;
		private DataColumn _columnPeriodTo;
		private DataColumn _columnCloseoutType;
		private DataColumn _columnState;
		private DataColumn _columnTransactionJournalID;
		private DataColumn _columnTransitAccountID;
		private DataColumn _columnDeviceTime;
		private DataColumn _columnFareAmount;
		private DataColumn _columnLine;
		private DataColumn _columnTicketInternalNumber;
		private DataColumn _columnTicketID;
		private DataColumn _columnTransactionType;
		private DataColumn _columnClientID;
		private DataColumn _columnTransactionOperatorID;
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnResultType;
		private DataColumn _columnCancellationReferenceGUID;
		private DataColumn _columnCancellationReference;
		private DataColumn _columnProductID;
		private DataColumn _columnPurseBalance;
		private DataColumn _columnPurseCredit;
		private DataColumn _columnTariffDate;
		private DataColumn _columnTariffVersion;
		private DataColumn _columnTripTicketInternalNumber;
		private DataColumn _columnCostCenterKey;
		private DataColumn _columnLastModified;
		private DataColumn _columnCancelledTap;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 43;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static AccRecognizedTapCmlSettledTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AccRecognizedTapCmlSettledTypedView():base("AccRecognizedTapCmlSettled")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccRecognizedTapCmlSettledTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.AccRecognizedTapCmlSettledTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.AccRecognizedTapCmlSettledTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new AccRecognizedTapCmlSettledRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RevenueSettlementID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SettlementType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Premium", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BaseFare", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LineGroupID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IncludeInSettlement", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PeriodFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PeriodTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CloseoutType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionOperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReferenceGUID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CostCenterKey", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancelledTap", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "AccRecognizedTapCmlSettled";
			_columnRevenueRecognitionID = GeneralUtils.CreateTypedDataTableColumn("RevenueRecognitionID", @"RevenueRecognitionID", typeof(System.Int64), this.Columns);
			_columnRevenueSettlementID = GeneralUtils.CreateTypedDataTableColumn("RevenueSettlementID", @"RevenueSettlementID", typeof(System.Int64), this.Columns);
			_columnSettlementType = GeneralUtils.CreateTypedDataTableColumn("SettlementType", @"SettlementType", typeof(System.Int32), this.Columns);
			_columnAmount = GeneralUtils.CreateTypedDataTableColumn("Amount", @"Amount", typeof(System.Int64), this.Columns);
			_columnPremium = GeneralUtils.CreateTypedDataTableColumn("Premium", @"Premium", typeof(System.Int64), this.Columns);
			_columnBaseFare = GeneralUtils.CreateTypedDataTableColumn("BaseFare", @"BaseFare", typeof(System.Int64), this.Columns);
			_columnTicketNumber = GeneralUtils.CreateTypedDataTableColumn("TicketNumber", @"TicketNumber", typeof(System.Int32), this.Columns);
			_columnCustomerGroup = GeneralUtils.CreateTypedDataTableColumn("CustomerGroup", @"CustomerGroup", typeof(System.Int32), this.Columns);
			_columnLineGroupID = GeneralUtils.CreateTypedDataTableColumn("LineGroupID", @"LineGroupID", typeof(System.Int64), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int32), this.Columns);
			_columnCreditAccountNumber = GeneralUtils.CreateTypedDataTableColumn("CreditAccountNumber", @"CreditAccountNumber", typeof(System.String), this.Columns);
			_columnDebitAccountNumber = GeneralUtils.CreateTypedDataTableColumn("DebitAccountNumber", @"DebitAccountNumber", typeof(System.String), this.Columns);
			_columnPostingDate = GeneralUtils.CreateTypedDataTableColumn("PostingDate", @"PostingDate", typeof(System.DateTime), this.Columns);
			_columnPostingReference = GeneralUtils.CreateTypedDataTableColumn("PostingReference", @"PostingReference", typeof(System.String), this.Columns);
			_columnIncludeInSettlement = GeneralUtils.CreateTypedDataTableColumn("IncludeInSettlement", @"IncludeInSettlement", typeof(System.Boolean), this.Columns);
			_columnCloseoutPeriodID = GeneralUtils.CreateTypedDataTableColumn("CloseoutPeriodID", @"CloseoutPeriodID", typeof(System.Int64), this.Columns);
			_columnPeriodFrom = GeneralUtils.CreateTypedDataTableColumn("PeriodFrom", @"PeriodFrom", typeof(System.DateTime), this.Columns);
			_columnPeriodTo = GeneralUtils.CreateTypedDataTableColumn("PeriodTo", @"PeriodTo", typeof(System.DateTime), this.Columns);
			_columnCloseoutType = GeneralUtils.CreateTypedDataTableColumn("CloseoutType", @"CloseoutType", typeof(System.Int32), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(System.Int32), this.Columns);
			_columnTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalID", @"TransactionJournalID", typeof(System.Int64), this.Columns);
			_columnTransitAccountID = GeneralUtils.CreateTypedDataTableColumn("TransitAccountID", @"TransitAccountID", typeof(System.Int64), this.Columns);
			_columnDeviceTime = GeneralUtils.CreateTypedDataTableColumn("DeviceTime", @"DeviceTime", typeof(System.DateTime), this.Columns);
			_columnFareAmount = GeneralUtils.CreateTypedDataTableColumn("FareAmount", @"FareAmount", typeof(System.Int32), this.Columns);
			_columnLine = GeneralUtils.CreateTypedDataTableColumn("Line", @"Line", typeof(System.String), this.Columns);
			_columnTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TicketInternalNumber", @"TicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnTransactionType = GeneralUtils.CreateTypedDataTableColumn("TransactionType", @"TransactionType", typeof(System.Int32), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnTransactionOperatorID = GeneralUtils.CreateTypedDataTableColumn("TransactionOperatorID", @"TransactionOperatorID", typeof(System.Int64), this.Columns);
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnResultType = GeneralUtils.CreateTypedDataTableColumn("ResultType", @"ResultType", typeof(System.Int32), this.Columns);
			_columnCancellationReferenceGUID = GeneralUtils.CreateTypedDataTableColumn("CancellationReferenceGUID", @"CancellationReferenceGUID", typeof(System.String), this.Columns);
			_columnCancellationReference = GeneralUtils.CreateTypedDataTableColumn("CancellationReference", @"CancellationReference", typeof(System.Int64), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnPurseBalance = GeneralUtils.CreateTypedDataTableColumn("PurseBalance", @"PurseBalance", typeof(System.Int32), this.Columns);
			_columnPurseCredit = GeneralUtils.CreateTypedDataTableColumn("PurseCredit", @"PurseCredit", typeof(System.Int32), this.Columns);
			_columnTariffDate = GeneralUtils.CreateTypedDataTableColumn("TariffDate", @"TariffDate", typeof(System.DateTime), this.Columns);
			_columnTariffVersion = GeneralUtils.CreateTypedDataTableColumn("TariffVersion", @"TariffVersion", typeof(System.Int32), this.Columns);
			_columnTripTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TripTicketInternalNumber", @"TripTicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnCostCenterKey = GeneralUtils.CreateTypedDataTableColumn("CostCenterKey", @"CostCenterKey", typeof(System.String), this.Columns);
			_columnLastModified = GeneralUtils.CreateTypedDataTableColumn("LastModified", @"LastModified", typeof(System.DateTime), this.Columns);
			_columnCancelledTap = GeneralUtils.CreateTypedDataTableColumn("CancelledTap", @"CancelledTap", typeof(System.Int32), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnRevenueRecognitionID = this.Columns["RevenueRecognitionID"];
			_columnRevenueSettlementID = this.Columns["RevenueSettlementID"];
			_columnSettlementType = this.Columns["SettlementType"];
			_columnAmount = this.Columns["Amount"];
			_columnPremium = this.Columns["Premium"];
			_columnBaseFare = this.Columns["BaseFare"];
			_columnTicketNumber = this.Columns["TicketNumber"];
			_columnCustomerGroup = this.Columns["CustomerGroup"];
			_columnLineGroupID = this.Columns["LineGroupID"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnCreditAccountNumber = this.Columns["CreditAccountNumber"];
			_columnDebitAccountNumber = this.Columns["DebitAccountNumber"];
			_columnPostingDate = this.Columns["PostingDate"];
			_columnPostingReference = this.Columns["PostingReference"];
			_columnIncludeInSettlement = this.Columns["IncludeInSettlement"];
			_columnCloseoutPeriodID = this.Columns["CloseoutPeriodID"];
			_columnPeriodFrom = this.Columns["PeriodFrom"];
			_columnPeriodTo = this.Columns["PeriodTo"];
			_columnCloseoutType = this.Columns["CloseoutType"];
			_columnState = this.Columns["State"];
			_columnTransactionJournalID = this.Columns["TransactionJournalID"];
			_columnTransitAccountID = this.Columns["TransitAccountID"];
			_columnDeviceTime = this.Columns["DeviceTime"];
			_columnFareAmount = this.Columns["FareAmount"];
			_columnLine = this.Columns["Line"];
			_columnTicketInternalNumber = this.Columns["TicketInternalNumber"];
			_columnTicketID = this.Columns["TicketID"];
			_columnTransactionType = this.Columns["TransactionType"];
			_columnClientID = this.Columns["ClientID"];
			_columnTransactionOperatorID = this.Columns["TransactionOperatorID"];
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnResultType = this.Columns["ResultType"];
			_columnCancellationReferenceGUID = this.Columns["CancellationReferenceGUID"];
			_columnCancellationReference = this.Columns["CancellationReference"];
			_columnProductID = this.Columns["ProductID"];
			_columnPurseBalance = this.Columns["PurseBalance"];
			_columnPurseCredit = this.Columns["PurseCredit"];
			_columnTariffDate = this.Columns["TariffDate"];
			_columnTariffVersion = this.Columns["TariffVersion"];
			_columnTripTicketInternalNumber = this.Columns["TripTicketInternalNumber"];
			_columnCostCenterKey = this.Columns["CostCenterKey"];
			_columnLastModified = this.Columns["LastModified"];
			_columnCancelledTap = this.Columns["CancelledTap"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			AccRecognizedTapCmlSettledTypedView cloneToReturn = ((AccRecognizedTapCmlSettledTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new AccRecognizedTapCmlSettledTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return AccRecognizedTapCmlSettledTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return AccRecognizedTapCmlSettledTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'RevenueRecognitionID'</summary>
		internal DataColumn RevenueRecognitionIDColumn 
		{
			get { return _columnRevenueRecognitionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RevenueSettlementID'</summary>
		internal DataColumn RevenueSettlementIDColumn 
		{
			get { return _columnRevenueSettlementID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SettlementType'</summary>
		internal DataColumn SettlementTypeColumn 
		{
			get { return _columnSettlementType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Amount'</summary>
		internal DataColumn AmountColumn 
		{
			get { return _columnAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Premium'</summary>
		internal DataColumn PremiumColumn 
		{
			get { return _columnPremium; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BaseFare'</summary>
		internal DataColumn BaseFareColumn 
		{
			get { return _columnBaseFare; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketNumber'</summary>
		internal DataColumn TicketNumberColumn 
		{
			get { return _columnTicketNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CustomerGroup'</summary>
		internal DataColumn CustomerGroupColumn 
		{
			get { return _columnCustomerGroup; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LineGroupID'</summary>
		internal DataColumn LineGroupIDColumn 
		{
			get { return _columnLineGroupID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreditAccountNumber'</summary>
		internal DataColumn CreditAccountNumberColumn 
		{
			get { return _columnCreditAccountNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebitAccountNumber'</summary>
		internal DataColumn DebitAccountNumberColumn 
		{
			get { return _columnDebitAccountNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingDate'</summary>
		internal DataColumn PostingDateColumn 
		{
			get { return _columnPostingDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingReference'</summary>
		internal DataColumn PostingReferenceColumn 
		{
			get { return _columnPostingReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IncludeInSettlement'</summary>
		internal DataColumn IncludeInSettlementColumn 
		{
			get { return _columnIncludeInSettlement; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CloseoutPeriodID'</summary>
		internal DataColumn CloseoutPeriodIDColumn 
		{
			get { return _columnCloseoutPeriodID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PeriodFrom'</summary>
		internal DataColumn PeriodFromColumn 
		{
			get { return _columnPeriodFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PeriodTo'</summary>
		internal DataColumn PeriodToColumn 
		{
			get { return _columnPeriodTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CloseoutType'</summary>
		internal DataColumn CloseoutTypeColumn 
		{
			get { return _columnCloseoutType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalID'</summary>
		internal DataColumn TransactionJournalIDColumn 
		{
			get { return _columnTransactionJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransitAccountID'</summary>
		internal DataColumn TransitAccountIDColumn 
		{
			get { return _columnTransitAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceTime'</summary>
		internal DataColumn DeviceTimeColumn 
		{
			get { return _columnDeviceTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareAmount'</summary>
		internal DataColumn FareAmountColumn 
		{
			get { return _columnFareAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Line'</summary>
		internal DataColumn LineColumn 
		{
			get { return _columnLine; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketInternalNumber'</summary>
		internal DataColumn TicketInternalNumberColumn 
		{
			get { return _columnTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionType'</summary>
		internal DataColumn TransactionTypeColumn 
		{
			get { return _columnTransactionType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionOperatorID'</summary>
		internal DataColumn TransactionOperatorIDColumn 
		{
			get { return _columnTransactionOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ResultType'</summary>
		internal DataColumn ResultTypeColumn 
		{
			get { return _columnResultType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReferenceGUID'</summary>
		internal DataColumn CancellationReferenceGUIDColumn 
		{
			get { return _columnCancellationReferenceGUID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReference'</summary>
		internal DataColumn CancellationReferenceColumn 
		{
			get { return _columnCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance'</summary>
		internal DataColumn PurseBalanceColumn 
		{
			get { return _columnPurseBalance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit'</summary>
		internal DataColumn PurseCreditColumn 
		{
			get { return _columnPurseCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffDate'</summary>
		internal DataColumn TariffDateColumn 
		{
			get { return _columnTariffDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffVersion'</summary>
		internal DataColumn TariffVersionColumn 
		{
			get { return _columnTariffVersion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripTicketInternalNumber'</summary>
		internal DataColumn TripTicketInternalNumberColumn 
		{
			get { return _columnTripTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CostCenterKey'</summary>
		internal DataColumn CostCenterKeyColumn 
		{
			get { return _columnCostCenterKey; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastModified'</summary>
		internal DataColumn LastModifiedColumn 
		{
			get { return _columnLastModified; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancelledTap'</summary>
		internal DataColumn CancelledTapColumn 
		{
			get { return _columnCancelledTap; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable AccRecognizedTapCmlSettled</summary>
	public partial class AccRecognizedTapCmlSettledRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private AccRecognizedTapCmlSettledTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal AccRecognizedTapCmlSettledRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((AccRecognizedTapCmlSettledTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field RevenueRecognitionID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."REVENUERECOGNITIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RevenueRecognitionID
		{
			get { return IsRevenueRecognitionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RevenueRecognitionIDColumn]; }
			set { this[_parent.RevenueRecognitionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RevenueRecognitionID is NULL, false otherwise.</summary>
		public bool IsRevenueRecognitionIDNull() 
		{
			return IsNull(_parent.RevenueRecognitionIDColumn);
		}

		/// <summary>Sets the TypedView field RevenueRecognitionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRevenueRecognitionIDNull() 
		{
			this[_parent.RevenueRecognitionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RevenueSettlementID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."REVENUESETTLEMENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RevenueSettlementID
		{
			get { return IsRevenueSettlementIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RevenueSettlementIDColumn]; }
			set { this[_parent.RevenueSettlementIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RevenueSettlementID is NULL, false otherwise.</summary>
		public bool IsRevenueSettlementIDNull() 
		{
			return IsNull(_parent.RevenueSettlementIDColumn);
		}

		/// <summary>Sets the TypedView field RevenueSettlementID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRevenueSettlementIDNull() 
		{
			this[_parent.RevenueSettlementIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SettlementType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."SETTLEMENTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SettlementType
		{
			get { return IsSettlementTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SettlementTypeColumn]; }
			set { this[_parent.SettlementTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SettlementType is NULL, false otherwise.</summary>
		public bool IsSettlementTypeNull() 
		{
			return IsNull(_parent.SettlementTypeColumn);
		}

		/// <summary>Sets the TypedView field SettlementType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSettlementTypeNull() 
		{
			this[_parent.SettlementTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Amount</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."AMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Amount
		{
			get { return IsAmountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AmountColumn]; }
			set { this[_parent.AmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Amount is NULL, false otherwise.</summary>
		public bool IsAmountNull() 
		{
			return IsNull(_parent.AmountColumn);
		}

		/// <summary>Sets the TypedView field Amount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAmountNull() 
		{
			this[_parent.AmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Premium</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PREMIUM"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Premium
		{
			get { return IsPremiumNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PremiumColumn]; }
			set { this[_parent.PremiumColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Premium is NULL, false otherwise.</summary>
		public bool IsPremiumNull() 
		{
			return IsNull(_parent.PremiumColumn);
		}

		/// <summary>Sets the TypedView field Premium to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPremiumNull() 
		{
			this[_parent.PremiumColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BaseFare</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."BASEFARE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 BaseFare
		{
			get { return IsBaseFareNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.BaseFareColumn]; }
			set { this[_parent.BaseFareColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BaseFare is NULL, false otherwise.</summary>
		public bool IsBaseFareNull() 
		{
			return IsNull(_parent.BaseFareColumn);
		}

		/// <summary>Sets the TypedView field BaseFare to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBaseFareNull() 
		{
			this[_parent.BaseFareColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TICKETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TicketNumber
		{
			get { return IsTicketNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TicketNumberColumn]; }
			set { this[_parent.TicketNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketNumber is NULL, false otherwise.</summary>
		public bool IsTicketNumberNull() 
		{
			return IsNull(_parent.TicketNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNumberNull() 
		{
			this[_parent.TicketNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CustomerGroup</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CUSTOMERGROUP"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CustomerGroup
		{
			get { return IsCustomerGroupNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CustomerGroupColumn]; }
			set { this[_parent.CustomerGroupColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CustomerGroup is NULL, false otherwise.</summary>
		public bool IsCustomerGroupNull() 
		{
			return IsNull(_parent.CustomerGroupColumn);
		}

		/// <summary>Sets the TypedView field CustomerGroup to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCustomerGroupNull() 
		{
			this[_parent.CustomerGroupColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LineGroupID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."LINEGROUPID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 LineGroupID
		{
			get { return IsLineGroupIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.LineGroupIDColumn]; }
			set { this[_parent.LineGroupIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LineGroupID is NULL, false otherwise.</summary>
		public bool IsLineGroupIDNull() 
		{
			return IsNull(_parent.LineGroupIDColumn);
		}

		/// <summary>Sets the TypedView field LineGroupID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineGroupIDNull() 
		{
			this[_parent.LineGroupIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreditAccountNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CREDITACCOUNTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CreditAccountNumber
		{
			get { return IsCreditAccountNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CreditAccountNumberColumn]; }
			set { this[_parent.CreditAccountNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreditAccountNumber is NULL, false otherwise.</summary>
		public bool IsCreditAccountNumberNull() 
		{
			return IsNull(_parent.CreditAccountNumberColumn);
		}

		/// <summary>Sets the TypedView field CreditAccountNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreditAccountNumberNull() 
		{
			this[_parent.CreditAccountNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebitAccountNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."DEBITACCOUNTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String DebitAccountNumber
		{
			get { return IsDebitAccountNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.DebitAccountNumberColumn]; }
			set { this[_parent.DebitAccountNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebitAccountNumber is NULL, false otherwise.</summary>
		public bool IsDebitAccountNumberNull() 
		{
			return IsNull(_parent.DebitAccountNumberColumn);
		}

		/// <summary>Sets the TypedView field DebitAccountNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebitAccountNumberNull() 
		{
			this[_parent.DebitAccountNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingDate</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."POSTINGDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PostingDate
		{
			get { return IsPostingDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PostingDateColumn]; }
			set { this[_parent.PostingDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingDate is NULL, false otherwise.</summary>
		public bool IsPostingDateNull() 
		{
			return IsNull(_parent.PostingDateColumn);
		}

		/// <summary>Sets the TypedView field PostingDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingDateNull() 
		{
			this[_parent.PostingDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingReference</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."POSTINGREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String PostingReference
		{
			get { return IsPostingReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PostingReferenceColumn]; }
			set { this[_parent.PostingReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingReference is NULL, false otherwise.</summary>
		public bool IsPostingReferenceNull() 
		{
			return IsNull(_parent.PostingReferenceColumn);
		}

		/// <summary>Sets the TypedView field PostingReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingReferenceNull() 
		{
			this[_parent.PostingReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IncludeInSettlement</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."INCLUDEINSETTLEMENT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IncludeInSettlement
		{
			get { return IsIncludeInSettlementNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IncludeInSettlementColumn]; }
			set { this[_parent.IncludeInSettlementColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IncludeInSettlement is NULL, false otherwise.</summary>
		public bool IsIncludeInSettlementNull() 
		{
			return IsNull(_parent.IncludeInSettlementColumn);
		}

		/// <summary>Sets the TypedView field IncludeInSettlement to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIncludeInSettlementNull() 
		{
			this[_parent.IncludeInSettlementColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CloseoutPeriodID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CLOSEOUTPERIODID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CloseoutPeriodID
		{
			get { return IsCloseoutPeriodIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CloseoutPeriodIDColumn]; }
			set { this[_parent.CloseoutPeriodIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CloseoutPeriodID is NULL, false otherwise.</summary>
		public bool IsCloseoutPeriodIDNull() 
		{
			return IsNull(_parent.CloseoutPeriodIDColumn);
		}

		/// <summary>Sets the TypedView field CloseoutPeriodID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCloseoutPeriodIDNull() 
		{
			this[_parent.CloseoutPeriodIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PeriodFrom</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PERIODFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PeriodFrom
		{
			get { return IsPeriodFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PeriodFromColumn]; }
			set { this[_parent.PeriodFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PeriodFrom is NULL, false otherwise.</summary>
		public bool IsPeriodFromNull() 
		{
			return IsNull(_parent.PeriodFromColumn);
		}

		/// <summary>Sets the TypedView field PeriodFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPeriodFromNull() 
		{
			this[_parent.PeriodFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PeriodTo</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PERIODTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PeriodTo
		{
			get { return IsPeriodToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PeriodToColumn]; }
			set { this[_parent.PeriodToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PeriodTo is NULL, false otherwise.</summary>
		public bool IsPeriodToNull() 
		{
			return IsNull(_parent.PeriodToColumn);
		}

		/// <summary>Sets the TypedView field PeriodTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPeriodToNull() 
		{
			this[_parent.PeriodToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CloseoutType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CLOSEOUTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CloseoutType
		{
			get { return IsCloseoutTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CloseoutTypeColumn]; }
			set { this[_parent.CloseoutTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CloseoutType is NULL, false otherwise.</summary>
		public bool IsCloseoutTypeNull() 
		{
			return IsNull(_parent.CloseoutTypeColumn);
		}

		/// <summary>Sets the TypedView field CloseoutType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCloseoutTypeNull() 
		{
			this[_parent.CloseoutTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 State
		{
			get { return IsStateNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TRANSACTIONJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransactionJournalID
		{
			get { return IsTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalIDColumn]; }
			set { this[_parent.TransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalID is NULL, false otherwise.</summary>
		public bool IsTransactionJournalIDNull() 
		{
			return IsNull(_parent.TransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalIDNull() 
		{
			this[_parent.TransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransitAccountID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TRANSITACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransitAccountID
		{
			get { return IsTransitAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransitAccountIDColumn]; }
			set { this[_parent.TransitAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransitAccountID is NULL, false otherwise.</summary>
		public bool IsTransitAccountIDNull() 
		{
			return IsNull(_parent.TransitAccountIDColumn);
		}

		/// <summary>Sets the TypedView field TransitAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransitAccountIDNull() 
		{
			this[_parent.TransitAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceTime</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."DEVICETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime DeviceTime
		{
			get { return IsDeviceTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.DeviceTimeColumn]; }
			set { this[_parent.DeviceTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceTime is NULL, false otherwise.</summary>
		public bool IsDeviceTimeNull() 
		{
			return IsNull(_parent.DeviceTimeColumn);
		}

		/// <summary>Sets the TypedView field DeviceTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceTimeNull() 
		{
			this[_parent.DeviceTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareAmount</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."FAREAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FareAmount
		{
			get { return IsFareAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FareAmountColumn]; }
			set { this[_parent.FareAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareAmount is NULL, false otherwise.</summary>
		public bool IsFareAmountNull() 
		{
			return IsNull(_parent.FareAmountColumn);
		}

		/// <summary>Sets the TypedView field FareAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareAmountNull() 
		{
			this[_parent.FareAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Line</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."LINE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String Line
		{
			get { return IsLineNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineColumn]; }
			set { this[_parent.LineColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Line is NULL, false otherwise.</summary>
		public bool IsLineNull() 
		{
			return IsNull(_parent.LineColumn);
		}

		/// <summary>Sets the TypedView field Line to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNull() 
		{
			this[_parent.LineColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TicketInternalNumber
		{
			get { return IsTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TicketInternalNumberColumn]; }
			set { this[_parent.TicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTicketInternalNumberNull() 
		{
			return IsNull(_parent.TicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketInternalNumberNull() 
		{
			this[_parent.TicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TRANSACTIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TransactionType
		{
			get { return IsTransactionTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TransactionTypeColumn]; }
			set { this[_parent.TransactionTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionType is NULL, false otherwise.</summary>
		public bool IsTransactionTypeNull() 
		{
			return IsNull(_parent.TransactionTypeColumn);
		}

		/// <summary>Sets the TypedView field TransactionType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionTypeNull() 
		{
			this[_parent.TransactionTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionOperatorID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TRANSACTIONOPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TransactionOperatorID
		{
			get { return IsTransactionOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionOperatorIDColumn]; }
			set { this[_parent.TransactionOperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionOperatorID is NULL, false otherwise.</summary>
		public bool IsTransactionOperatorIDNull() 
		{
			return IsNull(_parent.TransactionOperatorIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionOperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionOperatorIDNull() 
		{
			this[_parent.TransactionOperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ResultType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."RESULTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 ResultType
		{
			get { return IsResultTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.ResultTypeColumn]; }
			set { this[_parent.ResultTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ResultType is NULL, false otherwise.</summary>
		public bool IsResultTypeNull() 
		{
			return IsNull(_parent.ResultTypeColumn);
		}

		/// <summary>Sets the TypedView field ResultType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetResultTypeNull() 
		{
			this[_parent.ResultTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReferenceGUID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CANCELLATIONREFERENCEGUID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CancellationReferenceGUID
		{
			get { return IsCancellationReferenceGUIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CancellationReferenceGUIDColumn]; }
			set { this[_parent.CancellationReferenceGUIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReferenceGUID is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceGUIDNull() 
		{
			return IsNull(_parent.CancellationReferenceGUIDColumn);
		}

		/// <summary>Sets the TypedView field CancellationReferenceGUID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceGUIDNull() 
		{
			this[_parent.CancellationReferenceGUIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReference</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CancellationReference
		{
			get { return IsCancellationReferenceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CancellationReferenceColumn]; }
			set { this[_parent.CancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReference is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceNull() 
		{
			return IsNull(_parent.CancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field CancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceNull() 
		{
			this[_parent.CancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PURSEBALANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance
		{
			get { return IsPurseBalanceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalanceColumn]; }
			set { this[_parent.PurseBalanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance is NULL, false otherwise.</summary>
		public bool IsPurseBalanceNull() 
		{
			return IsNull(_parent.PurseBalanceColumn);
		}

		/// <summary>Sets the TypedView field PurseBalance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalanceNull() 
		{
			this[_parent.PurseBalanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."PURSECREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit
		{
			get { return IsPurseCreditNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCreditColumn]; }
			set { this[_parent.PurseCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit is NULL, false otherwise.</summary>
		public bool IsPurseCreditNull() 
		{
			return IsNull(_parent.PurseCreditColumn);
		}

		/// <summary>Sets the TypedView field PurseCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCreditNull() 
		{
			this[_parent.PurseCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffDate</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TARIFFDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime TariffDate
		{
			get { return IsTariffDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TariffDateColumn]; }
			set { this[_parent.TariffDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffDate is NULL, false otherwise.</summary>
		public bool IsTariffDateNull() 
		{
			return IsNull(_parent.TariffDateColumn);
		}

		/// <summary>Sets the TypedView field TariffDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffDateNull() 
		{
			this[_parent.TariffDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffVersion</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TARIFFVERSION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TariffVersion
		{
			get { return IsTariffVersionNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TariffVersionColumn]; }
			set { this[_parent.TariffVersionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffVersion is NULL, false otherwise.</summary>
		public bool IsTariffVersionNull() 
		{
			return IsNull(_parent.TariffVersionColumn);
		}

		/// <summary>Sets the TypedView field TariffVersion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffVersionNull() 
		{
			this[_parent.TariffVersionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripTicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."TRIPTICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TripTicketInternalNumber
		{
			get { return IsTripTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TripTicketInternalNumberColumn]; }
			set { this[_parent.TripTicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripTicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTripTicketInternalNumberNull() 
		{
			return IsNull(_parent.TripTicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TripTicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripTicketInternalNumberNull() 
		{
			this[_parent.TripTicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CostCenterKey</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."COSTCENTERKEY"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String CostCenterKey
		{
			get { return IsCostCenterKeyNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CostCenterKeyColumn]; }
			set { this[_parent.CostCenterKeyColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CostCenterKey is NULL, false otherwise.</summary>
		public bool IsCostCenterKeyNull() 
		{
			return IsNull(_parent.CostCenterKeyColumn);
		}

		/// <summary>Sets the TypedView field CostCenterKey to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCostCenterKeyNull() 
		{
			this[_parent.CostCenterKeyColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastModified</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."LASTMODIFIED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastModified
		{
			get { return IsLastModifiedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastModifiedColumn]; }
			set { this[_parent.LastModifiedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastModified is NULL, false otherwise.</summary>
		public bool IsLastModifiedNull() 
		{
			return IsNull(_parent.LastModifiedColumn);
		}

		/// <summary>Sets the TypedView field LastModified to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastModifiedNull() 
		{
			this[_parent.LastModifiedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancelledTap</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZED_TAP_CML_SETTLED"."CANCELLEDTAP"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CancelledTap
		{
			get { return IsCancelledTapNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CancelledTapColumn]; }
			set { this[_parent.CancelledTapColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancelledTap is NULL, false otherwise.</summary>
		public bool IsCancelledTapNull() 
		{
			return IsNull(_parent.CancelledTapColumn);
		}

		/// <summary>Sets the TypedView field CancelledTap to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancelledTapNull() 
		{
			this[_parent.CancelledTapColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
