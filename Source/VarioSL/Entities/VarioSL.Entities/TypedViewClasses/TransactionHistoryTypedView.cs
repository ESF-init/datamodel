﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Typed datatable for the view 'TransactionHistory'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class TransactionHistoryTypedView : TypedViewBase<TransactionHistoryRow>, ITypedView
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private DataColumn _columnCardID;
		private DataColumn _columnCardTransactionNo;
		private DataColumn _columnCardBalance;
		private DataColumn _columnCardCredit;
		private DataColumn _columnClientID;
		private DataColumn _columnDeviceBookingState;
		private DataColumn _columnDistance;
		private DataColumn _columnExternalTicketName;
		private DataColumn _columnFareStage;
		private DataColumn _columnFactor;
		private DataColumn _columnFromTariffZone;
		private DataColumn _columnLineName;
		private DataColumn _columnPayCardNumber;
		private DataColumn _columnPrice;
		private DataColumn _columnRouteNumber;
		private DataColumn _columnTransactionID;
		private DataColumn _columnTripDateTime;
		private DataColumn _columnImportDateTime;
		private DataColumn _columnTypeID;
		private DataColumn _columnTypeName;
		private DataColumn _columnValueOfRide;
		private DataColumn _columnFromStopNumber;
		private DataColumn _columnStopCode;
		private DataColumn _columnDirection;
		private DataColumn _columnTicketName;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnLocationNumber;
		private DataColumn _columnStopName;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidUntil;
		private DataColumn _columnDevicePaymentMethod;
		private DataColumn _columnDebtorNumber;
		private DataColumn _columnRemainingRides;
		private DataColumn _columnOrganizationID;
		private DataColumn _columnCompanyName;
		private DataColumn _columnSalesChannel;
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 36;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static TransactionHistoryTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionHistoryTypedView():base("TransactionHistory")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionHistoryTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
			// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields
			// be sure to call fieldsInResultset.Expand(number of new fields) first. 
			// __LLBLGENPRO_USER_CODE_REGION_END
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionHistoryTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionHistoryTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new TransactionHistoryRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardTransactionNo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardBalance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceBookingState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Distance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExternalTicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareStage", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Factor", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FromTariffZone", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PayCardNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RouteNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripDateTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ImportDateTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TypeID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TypeName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValueOfRide", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FromStopNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidUntil", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DevicePaymentMethod", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RemainingRides", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CompanyName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannel", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "TransactionHistory";
			_columnCardID = GeneralUtils.CreateTypedDataTableColumn("CardID", @"CardID", typeof(System.Int64), this.Columns);
			_columnCardTransactionNo = GeneralUtils.CreateTypedDataTableColumn("CardTransactionNo", @"CardTransactionNo", typeof(System.Int64), this.Columns);
			_columnCardBalance = GeneralUtils.CreateTypedDataTableColumn("CardBalance", @"CardBalance", typeof(System.Int64), this.Columns);
			_columnCardCredit = GeneralUtils.CreateTypedDataTableColumn("CardCredit", @"CardCredit", typeof(System.Int64), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int32), this.Columns);
			_columnDeviceBookingState = GeneralUtils.CreateTypedDataTableColumn("DeviceBookingState", @"DeviceBookingState", typeof(System.Int16), this.Columns);
			_columnDistance = GeneralUtils.CreateTypedDataTableColumn("Distance", @"Distance", typeof(System.Int64), this.Columns);
			_columnExternalTicketName = GeneralUtils.CreateTypedDataTableColumn("ExternalTicketName", @"ExternalTicketName", typeof(System.String), this.Columns);
			_columnFareStage = GeneralUtils.CreateTypedDataTableColumn("FareStage", @"FareStage", typeof(System.Int64), this.Columns);
			_columnFactor = GeneralUtils.CreateTypedDataTableColumn("Factor", @"Factor", typeof(System.Int64), this.Columns);
			_columnFromTariffZone = GeneralUtils.CreateTypedDataTableColumn("FromTariffZone", @"FromTariffZone", typeof(System.Int64), this.Columns);
			_columnLineName = GeneralUtils.CreateTypedDataTableColumn("LineName", @"LineName", typeof(System.String), this.Columns);
			_columnPayCardNumber = GeneralUtils.CreateTypedDataTableColumn("PayCardNumber", @"PayCardNumber", typeof(System.String), this.Columns);
			_columnPrice = GeneralUtils.CreateTypedDataTableColumn("Price", @"Price", typeof(System.Int64), this.Columns);
			_columnRouteNumber = GeneralUtils.CreateTypedDataTableColumn("RouteNumber", @"RouteNumber", typeof(System.Int64), this.Columns);
			_columnTransactionID = GeneralUtils.CreateTypedDataTableColumn("TransactionID", @"TransactionID", typeof(System.Int64), this.Columns);
			_columnTripDateTime = GeneralUtils.CreateTypedDataTableColumn("TripDateTime", @"TripDateTime", typeof(System.DateTime), this.Columns);
			_columnImportDateTime = GeneralUtils.CreateTypedDataTableColumn("ImportDateTime", @"ImportDateTime", typeof(System.DateTime), this.Columns);
			_columnTypeID = GeneralUtils.CreateTypedDataTableColumn("TypeID", @"TypeID", typeof(System.Int64), this.Columns);
			_columnTypeName = GeneralUtils.CreateTypedDataTableColumn("TypeName", @"TypeName", typeof(System.String), this.Columns);
			_columnValueOfRide = GeneralUtils.CreateTypedDataTableColumn("ValueOfRide", @"ValueOfRide", typeof(System.Int64), this.Columns);
			_columnFromStopNumber = GeneralUtils.CreateTypedDataTableColumn("FromStopNumber", @"FromStopNumber", typeof(System.Int64), this.Columns);
			_columnStopCode = GeneralUtils.CreateTypedDataTableColumn("StopCode", @"StopCode", typeof(System.String), this.Columns);
			_columnDirection = GeneralUtils.CreateTypedDataTableColumn("Direction", @"Direction", typeof(System.Int16), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnLocationNumber = GeneralUtils.CreateTypedDataTableColumn("LocationNumber", @"LocationNumber", typeof(System.Int64), this.Columns);
			_columnStopName = GeneralUtils.CreateTypedDataTableColumn("StopName", @"StopName", typeof(System.String), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidUntil = GeneralUtils.CreateTypedDataTableColumn("ValidUntil", @"ValidUntil", typeof(System.DateTime), this.Columns);
			_columnDevicePaymentMethod = GeneralUtils.CreateTypedDataTableColumn("DevicePaymentMethod", @"DevicePaymentMethod", typeof(System.Int16), this.Columns);
			_columnDebtorNumber = GeneralUtils.CreateTypedDataTableColumn("DebtorNumber", @"DebtorNumber", typeof(System.Int64), this.Columns);
			_columnRemainingRides = GeneralUtils.CreateTypedDataTableColumn("RemainingRides", @"RemainingRides", typeof(System.Int32), this.Columns);
			_columnOrganizationID = GeneralUtils.CreateTypedDataTableColumn("OrganizationID", @"OrganizationID", typeof(System.Int64), this.Columns);
			_columnCompanyName = GeneralUtils.CreateTypedDataTableColumn("CompanyName", @"CompanyName", typeof(System.String), this.Columns);
			_columnSalesChannel = GeneralUtils.CreateTypedDataTableColumn("SalesChannel", @"SalesChannel", typeof(System.String), this.Columns);
			// __LLBLGENPRO_USER_CODE_REGION_START InitClass
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnCardID = this.Columns["CardID"];
			_columnCardTransactionNo = this.Columns["CardTransactionNo"];
			_columnCardBalance = this.Columns["CardBalance"];
			_columnCardCredit = this.Columns["CardCredit"];
			_columnClientID = this.Columns["ClientID"];
			_columnDeviceBookingState = this.Columns["DeviceBookingState"];
			_columnDistance = this.Columns["Distance"];
			_columnExternalTicketName = this.Columns["ExternalTicketName"];
			_columnFareStage = this.Columns["FareStage"];
			_columnFactor = this.Columns["Factor"];
			_columnFromTariffZone = this.Columns["FromTariffZone"];
			_columnLineName = this.Columns["LineName"];
			_columnPayCardNumber = this.Columns["PayCardNumber"];
			_columnPrice = this.Columns["Price"];
			_columnRouteNumber = this.Columns["RouteNumber"];
			_columnTransactionID = this.Columns["TransactionID"];
			_columnTripDateTime = this.Columns["TripDateTime"];
			_columnImportDateTime = this.Columns["ImportDateTime"];
			_columnTypeID = this.Columns["TypeID"];
			_columnTypeName = this.Columns["TypeName"];
			_columnValueOfRide = this.Columns["ValueOfRide"];
			_columnFromStopNumber = this.Columns["FromStopNumber"];
			_columnStopCode = this.Columns["StopCode"];
			_columnDirection = this.Columns["Direction"];
			_columnTicketName = this.Columns["TicketName"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnLocationNumber = this.Columns["LocationNumber"];
			_columnStopName = this.Columns["StopName"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidUntil = this.Columns["ValidUntil"];
			_columnDevicePaymentMethod = this.Columns["DevicePaymentMethod"];
			_columnDebtorNumber = this.Columns["DebtorNumber"];
			_columnRemainingRides = this.Columns["RemainingRides"];
			_columnOrganizationID = this.Columns["OrganizationID"];
			_columnCompanyName = this.Columns["CompanyName"];
			_columnSalesChannel = this.Columns["SalesChannel"];
			// __LLBLGENPRO_USER_CODE_REGION_START InitMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			TransactionHistoryTypedView cloneToReturn = ((TransactionHistoryTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new TransactionHistoryTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return TransactionHistoryTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return TransactionHistoryTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'CardID'</summary>
		internal DataColumn CardIDColumn 
		{
			get { return _columnCardID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardTransactionNo'</summary>
		internal DataColumn CardTransactionNoColumn 
		{
			get { return _columnCardTransactionNo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardBalance'</summary>
		internal DataColumn CardBalanceColumn 
		{
			get { return _columnCardBalance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardCredit'</summary>
		internal DataColumn CardCreditColumn 
		{
			get { return _columnCardCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceBookingState'</summary>
		internal DataColumn DeviceBookingStateColumn 
		{
			get { return _columnDeviceBookingState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Distance'</summary>
		internal DataColumn DistanceColumn 
		{
			get { return _columnDistance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExternalTicketName'</summary>
		internal DataColumn ExternalTicketNameColumn 
		{
			get { return _columnExternalTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareStage'</summary>
		internal DataColumn FareStageColumn 
		{
			get { return _columnFareStage; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Factor'</summary>
		internal DataColumn FactorColumn 
		{
			get { return _columnFactor; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FromTariffZone'</summary>
		internal DataColumn FromTariffZoneColumn 
		{
			get { return _columnFromTariffZone; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LineName'</summary>
		internal DataColumn LineNameColumn 
		{
			get { return _columnLineName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PayCardNumber'</summary>
		internal DataColumn PayCardNumberColumn 
		{
			get { return _columnPayCardNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Price'</summary>
		internal DataColumn PriceColumn 
		{
			get { return _columnPrice; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RouteNumber'</summary>
		internal DataColumn RouteNumberColumn 
		{
			get { return _columnRouteNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionID'</summary>
		internal DataColumn TransactionIDColumn 
		{
			get { return _columnTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripDateTime'</summary>
		internal DataColumn TripDateTimeColumn 
		{
			get { return _columnTripDateTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ImportDateTime'</summary>
		internal DataColumn ImportDateTimeColumn 
		{
			get { return _columnImportDateTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TypeID'</summary>
		internal DataColumn TypeIDColumn 
		{
			get { return _columnTypeID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TypeName'</summary>
		internal DataColumn TypeNameColumn 
		{
			get { return _columnTypeName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValueOfRide'</summary>
		internal DataColumn ValueOfRideColumn 
		{
			get { return _columnValueOfRide; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FromStopNumber'</summary>
		internal DataColumn FromStopNumberColumn 
		{
			get { return _columnFromStopNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopCode'</summary>
		internal DataColumn StopCodeColumn 
		{
			get { return _columnStopCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Direction'</summary>
		internal DataColumn DirectionColumn 
		{
			get { return _columnDirection; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LocationNumber'</summary>
		internal DataColumn LocationNumberColumn 
		{
			get { return _columnLocationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopName'</summary>
		internal DataColumn StopNameColumn 
		{
			get { return _columnStopName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidUntil'</summary>
		internal DataColumn ValidUntilColumn 
		{
			get { return _columnValidUntil; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DevicePaymentMethod'</summary>
		internal DataColumn DevicePaymentMethodColumn 
		{
			get { return _columnDevicePaymentMethod; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebtorNumber'</summary>
		internal DataColumn DebtorNumberColumn 
		{
			get { return _columnDebtorNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RemainingRides'</summary>
		internal DataColumn RemainingRidesColumn 
		{
			get { return _columnRemainingRides; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationID'</summary>
		internal DataColumn OrganizationIDColumn 
		{
			get { return _columnOrganizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CompanyName'</summary>
		internal DataColumn CompanyNameColumn 
		{
			get { return _columnCompanyName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannel'</summary>
		internal DataColumn SalesChannelColumn 
		{
			get { return _columnSalesChannel; }
		}
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties
		// __LLBLGENPRO_USER_CODE_REGION_END
 		#endregion
		
		#region Custom Typed View code
		// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode
		// __LLBLGENPRO_USER_CODE_REGION_END																																																																																														
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable TransactionHistory</summary>
	public partial class TransactionHistoryRow : DataRow
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private TransactionHistoryTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal TransactionHistoryRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((TransactionHistoryTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field CardID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."CARDID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 CardID
		{
			get { return IsCardIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardIDColumn]; }
			set { this[_parent.CardIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardID is NULL, false otherwise.</summary>
		public bool IsCardIDNull() 
		{
			return IsNull(_parent.CardIDColumn);
		}

		/// <summary>Sets the TypedView field CardID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardIDNull() 
		{
			this[_parent.CardIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardTransactionNo</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."CARDTRANSACTIONNO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 CardTransactionNo
		{
			get { return IsCardTransactionNoNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardTransactionNoColumn]; }
			set { this[_parent.CardTransactionNoColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardTransactionNo is NULL, false otherwise.</summary>
		public bool IsCardTransactionNoNull() 
		{
			return IsNull(_parent.CardTransactionNoColumn);
		}

		/// <summary>Sets the TypedView field CardTransactionNo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardTransactionNoNull() 
		{
			this[_parent.CardTransactionNoColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardBalance</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."CARDBALANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 CardBalance
		{
			get { return IsCardBalanceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardBalanceColumn]; }
			set { this[_parent.CardBalanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardBalance is NULL, false otherwise.</summary>
		public bool IsCardBalanceNull() 
		{
			return IsNull(_parent.CardBalanceColumn);
		}

		/// <summary>Sets the TypedView field CardBalance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardBalanceNull() 
		{
			this[_parent.CardBalanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardCredit</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."CARDCREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 CardCredit
		{
			get { return IsCardCreditNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardCreditColumn]; }
			set { this[_parent.CardCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardCredit is NULL, false otherwise.</summary>
		public bool IsCardCreditNull() 
		{
			return IsNull(_parent.CardCreditColumn);
		}

		/// <summary>Sets the TypedView field CardCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardCreditNull() 
		{
			this[_parent.CardCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 5, 0, 0</remarks>
		public System.Int32 ClientID
		{
			get { return IsClientIDNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceBookingState</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DEVICEBOOKINGSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 4, 0, 0</remarks>
		public System.Int16 DeviceBookingState
		{
			get { return IsDeviceBookingStateNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.DeviceBookingStateColumn]; }
			set { this[_parent.DeviceBookingStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceBookingState is NULL, false otherwise.</summary>
		public bool IsDeviceBookingStateNull() 
		{
			return IsNull(_parent.DeviceBookingStateColumn);
		}

		/// <summary>Sets the TypedView field DeviceBookingState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceBookingStateNull() 
		{
			this[_parent.DeviceBookingStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Distance</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DISTANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 Distance
		{
			get { return IsDistanceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.DistanceColumn]; }
			set { this[_parent.DistanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Distance is NULL, false otherwise.</summary>
		public bool IsDistanceNull() 
		{
			return IsNull(_parent.DistanceColumn);
		}

		/// <summary>Sets the TypedView field Distance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDistanceNull() 
		{
			this[_parent.DistanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExternalTicketName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."EXTTIKNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 10</remarks>
		public System.String ExternalTicketName
		{
			get { return IsExternalTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExternalTicketNameColumn]; }
			set { this[_parent.ExternalTicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExternalTicketName is NULL, false otherwise.</summary>
		public bool IsExternalTicketNameNull() 
		{
			return IsNull(_parent.ExternalTicketNameColumn);
		}

		/// <summary>Sets the TypedView field ExternalTicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExternalTicketNameNull() 
		{
			this[_parent.ExternalTicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareStage</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."FARESTAGE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 FareStage
		{
			get { return IsFareStageNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FareStageColumn]; }
			set { this[_parent.FareStageColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareStage is NULL, false otherwise.</summary>
		public bool IsFareStageNull() 
		{
			return IsNull(_parent.FareStageColumn);
		}

		/// <summary>Sets the TypedView field FareStage to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareStageNull() 
		{
			this[_parent.FareStageColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Factor</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."FACTOR"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 Factor
		{
			get { return IsFactorNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FactorColumn]; }
			set { this[_parent.FactorColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Factor is NULL, false otherwise.</summary>
		public bool IsFactorNull() 
		{
			return IsNull(_parent.FactorColumn);
		}

		/// <summary>Sets the TypedView field Factor to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFactorNull() 
		{
			this[_parent.FactorColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FromTariffZone</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."FROMTARIFFZONE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 FromTariffZone
		{
			get { return IsFromTariffZoneNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FromTariffZoneColumn]; }
			set { this[_parent.FromTariffZoneColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FromTariffZone is NULL, false otherwise.</summary>
		public bool IsFromTariffZoneNull() 
		{
			return IsNull(_parent.FromTariffZoneColumn);
		}

		/// <summary>Sets the TypedView field FromTariffZone to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFromTariffZoneNull() 
		{
			this[_parent.FromTariffZoneColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LineName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."LINENAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 10</remarks>
		public System.String LineName
		{
			get { return IsLineNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineNameColumn]; }
			set { this[_parent.LineNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LineName is NULL, false otherwise.</summary>
		public bool IsLineNameNull() 
		{
			return IsNull(_parent.LineNameColumn);
		}

		/// <summary>Sets the TypedView field LineName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNameNull() 
		{
			this[_parent.LineNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PayCardNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."PAYCARDNO"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PayCardNumber
		{
			get { return IsPayCardNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PayCardNumberColumn]; }
			set { this[_parent.PayCardNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PayCardNumber is NULL, false otherwise.</summary>
		public bool IsPayCardNumberNull() 
		{
			return IsNull(_parent.PayCardNumberColumn);
		}

		/// <summary>Sets the TypedView field PayCardNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPayCardNumberNull() 
		{
			this[_parent.PayCardNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Price</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."PRICE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 Price
		{
			get { return IsPriceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PriceColumn]; }
			set { this[_parent.PriceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Price is NULL, false otherwise.</summary>
		public bool IsPriceNull() 
		{
			return IsNull(_parent.PriceColumn);
		}

		/// <summary>Sets the TypedView field Price to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPriceNull() 
		{
			this[_parent.PriceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RouteNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."ROUTENO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 RouteNumber
		{
			get { return IsRouteNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RouteNumberColumn]; }
			set { this[_parent.RouteNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RouteNumber is NULL, false otherwise.</summary>
		public bool IsRouteNumberNull() 
		{
			return IsNull(_parent.RouteNumberColumn);
		}

		/// <summary>Sets the TypedView field RouteNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRouteNumberNull() 
		{
			this[_parent.RouteNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."TRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TransactionID
		{
			get { return IsTransactionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionIDColumn]; }
			set { this[_parent.TransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionID is NULL, false otherwise.</summary>
		public bool IsTransactionIDNull() 
		{
			return IsNull(_parent.TransactionIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionIDNull() 
		{
			this[_parent.TransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripDateTime</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."TRIPDATETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime TripDateTime
		{
			get { return IsTripDateTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TripDateTimeColumn]; }
			set { this[_parent.TripDateTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripDateTime is NULL, false otherwise.</summary>
		public bool IsTripDateTimeNull() 
		{
			return IsNull(_parent.TripDateTimeColumn);
		}

		/// <summary>Sets the TypedView field TripDateTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripDateTimeNull() 
		{
			this[_parent.TripDateTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ImportDateTime</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."IMPORTDATETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ImportDateTime
		{
			get { return IsImportDateTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ImportDateTimeColumn]; }
			set { this[_parent.ImportDateTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ImportDateTime is NULL, false otherwise.</summary>
		public bool IsImportDateTimeNull() 
		{
			return IsNull(_parent.ImportDateTimeColumn);
		}

		/// <summary>Sets the TypedView field ImportDateTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetImportDateTimeNull() 
		{
			this[_parent.ImportDateTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TypeID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."TYPEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TypeID
		{
			get { return IsTypeIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TypeIDColumn]; }
			set { this[_parent.TypeIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TypeID is NULL, false otherwise.</summary>
		public bool IsTypeIDNull() 
		{
			return IsNull(_parent.TypeIDColumn);
		}

		/// <summary>Sets the TypedView field TypeID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTypeIDNull() 
		{
			this[_parent.TypeIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TypeName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."TYPENAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String TypeName
		{
			get { return IsTypeNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TypeNameColumn]; }
			set { this[_parent.TypeNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TypeName is NULL, false otherwise.</summary>
		public bool IsTypeNameNull() 
		{
			return IsNull(_parent.TypeNameColumn);
		}

		/// <summary>Sets the TypedView field TypeName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTypeNameNull() 
		{
			this[_parent.TypeNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValueOfRide</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."VALUEOFRIDE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 ValueOfRide
		{
			get { return IsValueOfRideNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ValueOfRideColumn]; }
			set { this[_parent.ValueOfRideColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValueOfRide is NULL, false otherwise.</summary>
		public bool IsValueOfRideNull() 
		{
			return IsNull(_parent.ValueOfRideColumn);
		}

		/// <summary>Sets the TypedView field ValueOfRide to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValueOfRideNull() 
		{
			this[_parent.ValueOfRideColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FromStopNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."FROMSTOPNO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 FromStopNumber
		{
			get { return IsFromStopNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FromStopNumberColumn]; }
			set { this[_parent.FromStopNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FromStopNumber is NULL, false otherwise.</summary>
		public bool IsFromStopNumberNull() 
		{
			return IsNull(_parent.FromStopNumberColumn);
		}

		/// <summary>Sets the TypedView field FromStopNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFromStopNumberNull() 
		{
			this[_parent.FromStopNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopCode</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."STOPCODE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String StopCode
		{
			get { return IsStopCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopCodeColumn]; }
			set { this[_parent.StopCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopCode is NULL, false otherwise.</summary>
		public bool IsStopCodeNull() 
		{
			return IsNull(_parent.StopCodeColumn);
		}

		/// <summary>Sets the TypedView field StopCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopCodeNull() 
		{
			this[_parent.StopCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Direction</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DIRECTION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 3, 0, 0</remarks>
		public System.Int16 Direction
		{
			get { return IsDirectionNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.DirectionColumn]; }
			set { this[_parent.DirectionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Direction is NULL, false otherwise.</summary>
		public bool IsDirectionNull() 
		{
			return IsNull(_parent.DirectionColumn);
		}

		/// <summary>Sets the TypedView field Direction to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDirectionNull() 
		{
			this[_parent.DirectionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DEVICENO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 8, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LocationNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."LOCATIONNO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 LocationNumber
		{
			get { return IsLocationNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.LocationNumberColumn]; }
			set { this[_parent.LocationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LocationNumber is NULL, false otherwise.</summary>
		public bool IsLocationNumberNull() 
		{
			return IsNull(_parent.LocationNumberColumn);
		}

		/// <summary>Sets the TypedView field LocationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNumberNull() 
		{
			this[_parent.LocationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."STOPNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String StopName
		{
			get { return IsStopNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopNameColumn]; }
			set { this[_parent.StopNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopName is NULL, false otherwise.</summary>
		public bool IsStopNameNull() 
		{
			return IsNull(_parent.StopNameColumn);
		}

		/// <summary>Sets the TypedView field StopName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopNameNull() 
		{
			this[_parent.StopNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidUntil</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."VALIDUNTIL"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidUntil
		{
			get { return IsValidUntilNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidUntilColumn]; }
			set { this[_parent.ValidUntilColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidUntil is NULL, false otherwise.</summary>
		public bool IsValidUntilNull() 
		{
			return IsNull(_parent.ValidUntilColumn);
		}

		/// <summary>Sets the TypedView field ValidUntil to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidUntilNull() 
		{
			this[_parent.ValidUntilColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DevicePaymentMethod</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DEVICEPAYMENTMETHOD"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 3, 0, 0</remarks>
		public System.Int16 DevicePaymentMethod
		{
			get { return IsDevicePaymentMethodNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.DevicePaymentMethodColumn]; }
			set { this[_parent.DevicePaymentMethodColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DevicePaymentMethod is NULL, false otherwise.</summary>
		public bool IsDevicePaymentMethodNull() 
		{
			return IsNull(_parent.DevicePaymentMethodColumn);
		}

		/// <summary>Sets the TypedView field DevicePaymentMethod to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDevicePaymentMethodNull() 
		{
			this[_parent.DevicePaymentMethodColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebtorNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."DEBTORNO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 DebtorNumber
		{
			get { return IsDebtorNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.DebtorNumberColumn]; }
			set { this[_parent.DebtorNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebtorNumber is NULL, false otherwise.</summary>
		public bool IsDebtorNumberNull() 
		{
			return IsNull(_parent.DebtorNumberColumn);
		}

		/// <summary>Sets the TypedView field DebtorNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebtorNumberNull() 
		{
			this[_parent.DebtorNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RemainingRides</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."REMAININGRIDES"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 5, 0, 0</remarks>
		public System.Int32 RemainingRides
		{
			get { return IsRemainingRidesNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.RemainingRidesColumn]; }
			set { this[_parent.RemainingRidesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RemainingRides is NULL, false otherwise.</summary>
		public bool IsRemainingRidesNull() 
		{
			return IsNull(_parent.RemainingRidesColumn);
		}

		/// <summary>Sets the TypedView field RemainingRides to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRemainingRidesNull() 
		{
			this[_parent.RemainingRidesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."ORGANIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrganizationID
		{
			get { return IsOrganizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrganizationIDColumn]; }
			set { this[_parent.OrganizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationID is NULL, false otherwise.</summary>
		public bool IsOrganizationIDNull() 
		{
			return IsNull(_parent.OrganizationIDColumn);
		}

		/// <summary>Sets the TypedView field OrganizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationIDNull() 
		{
			this[_parent.OrganizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CompanyName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."COMPANYNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String CompanyName
		{
			get { return IsCompanyNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CompanyNameColumn]; }
			set { this[_parent.CompanyNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CompanyName is NULL, false otherwise.</summary>
		public bool IsCompanyNameNull() 
		{
			return IsNull(_parent.CompanyNameColumn);
		}

		/// <summary>Sets the TypedView field CompanyName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCompanyNameNull() 
		{
			this[_parent.CompanyNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannel</summary>
		/// <remarks>Mapped on view field: "SL_TRANSACTIONHISTORY"."SALESCHANNEL"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String SalesChannel
		{
			get { return IsSalesChannelNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SalesChannelColumn]; }
			set { this[_parent.SalesChannelColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannel is NULL, false otherwise.</summary>
		public bool IsSalesChannelNull() 
		{
			return IsNull(_parent.SalesChannelColumn);
		}

		/// <summary>Sets the TypedView field SalesChannel to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelNull() 
		{
			this[_parent.SalesChannelColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
		// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode
		// __LLBLGENPRO_USER_CODE_REGION_END																																																																																														
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
