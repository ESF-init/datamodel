﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'DebtorPostings'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class DebtorPostingsTypedView : TypedViewBase<DebtorPostingsRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnAccountID;
		private DataColumn _columnAccountNumber;
		private DataColumn _columnClientID;
		private DataColumn _columnPostingTime;
		private DataColumn _columnValueDate;
		private DataColumn _columnAmount;
		private DataColumn _columnPostingText;
		private DataColumn _columnReceiptNumber;
		private DataColumn _columnPostingCodeNumber;
		private DataColumn _columnPostingCodeText;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 10;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static DebtorPostingsTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public DebtorPostingsTypedView():base("DebtorPostings")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DebtorPostingsTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.DebtorPostingsTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.DebtorPostingsTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new DebtorPostingsRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValueDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReceiptNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingCodeNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingCodeText", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "DebtorPostings";
			_columnAccountID = GeneralUtils.CreateTypedDataTableColumn("AccountID", @"AccountID", typeof(System.Int64), this.Columns);
			_columnAccountNumber = GeneralUtils.CreateTypedDataTableColumn("AccountNumber", @"AccountNumber", typeof(System.Int64), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnPostingTime = GeneralUtils.CreateTypedDataTableColumn("PostingTime", @"PostingTime", typeof(System.DateTime), this.Columns);
			_columnValueDate = GeneralUtils.CreateTypedDataTableColumn("ValueDate", @"ValueDate", typeof(System.DateTime), this.Columns);
			_columnAmount = GeneralUtils.CreateTypedDataTableColumn("Amount", @"Amount", typeof(System.Int32), this.Columns);
			_columnPostingText = GeneralUtils.CreateTypedDataTableColumn("PostingText", @"PostingText", typeof(System.String), this.Columns);
			_columnReceiptNumber = GeneralUtils.CreateTypedDataTableColumn("ReceiptNumber", @"ReceiptNumber", typeof(System.String), this.Columns);
			_columnPostingCodeNumber = GeneralUtils.CreateTypedDataTableColumn("PostingCodeNumber", @"PostingCodeNumber", typeof(System.Int64), this.Columns);
			_columnPostingCodeText = GeneralUtils.CreateTypedDataTableColumn("PostingCodeText", @"PostingCodeText", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnAccountID = this.Columns["AccountID"];
			_columnAccountNumber = this.Columns["AccountNumber"];
			_columnClientID = this.Columns["ClientID"];
			_columnPostingTime = this.Columns["PostingTime"];
			_columnValueDate = this.Columns["ValueDate"];
			_columnAmount = this.Columns["Amount"];
			_columnPostingText = this.Columns["PostingText"];
			_columnReceiptNumber = this.Columns["ReceiptNumber"];
			_columnPostingCodeNumber = this.Columns["PostingCodeNumber"];
			_columnPostingCodeText = this.Columns["PostingCodeText"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			DebtorPostingsTypedView cloneToReturn = ((DebtorPostingsTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new DebtorPostingsTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return DebtorPostingsTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return DebtorPostingsTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'AccountID'</summary>
		internal DataColumn AccountIDColumn 
		{
			get { return _columnAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AccountNumber'</summary>
		internal DataColumn AccountNumberColumn 
		{
			get { return _columnAccountNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingTime'</summary>
		internal DataColumn PostingTimeColumn 
		{
			get { return _columnPostingTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValueDate'</summary>
		internal DataColumn ValueDateColumn 
		{
			get { return _columnValueDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Amount'</summary>
		internal DataColumn AmountColumn 
		{
			get { return _columnAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingText'</summary>
		internal DataColumn PostingTextColumn 
		{
			get { return _columnPostingText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReceiptNumber'</summary>
		internal DataColumn ReceiptNumberColumn 
		{
			get { return _columnReceiptNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingCodeNumber'</summary>
		internal DataColumn PostingCodeNumberColumn 
		{
			get { return _columnPostingCodeNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingCodeText'</summary>
		internal DataColumn PostingCodeTextColumn 
		{
			get { return _columnPostingCodeText; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable DebtorPostings</summary>
	public partial class DebtorPostingsRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DebtorPostingsTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal DebtorPostingsRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((DebtorPostingsTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field AccountID</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."ACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 AccountID
		{
			get { return IsAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AccountIDColumn]; }
			set { this[_parent.AccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountID is NULL, false otherwise.</summary>
		public bool IsAccountIDNull() 
		{
			return IsNull(_parent.AccountIDColumn);
		}

		/// <summary>Sets the TypedView field AccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountIDNull() 
		{
			this[_parent.AccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AccountNumber</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."ACCOUNTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 AccountNumber
		{
			get { return IsAccountNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AccountNumberColumn]; }
			set { this[_parent.AccountNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountNumber is NULL, false otherwise.</summary>
		public bool IsAccountNumberNull() 
		{
			return IsNull(_parent.AccountNumberColumn);
		}

		/// <summary>Sets the TypedView field AccountNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountNumberNull() 
		{
			this[_parent.AccountNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingTime</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."POSTINGTIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PostingTime
		{
			get { return IsPostingTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PostingTimeColumn]; }
			set { this[_parent.PostingTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingTime is NULL, false otherwise.</summary>
		public bool IsPostingTimeNull() 
		{
			return IsNull(_parent.PostingTimeColumn);
		}

		/// <summary>Sets the TypedView field PostingTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingTimeNull() 
		{
			this[_parent.PostingTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValueDate</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."VALUEDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValueDate
		{
			get { return IsValueDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValueDateColumn]; }
			set { this[_parent.ValueDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValueDate is NULL, false otherwise.</summary>
		public bool IsValueDateNull() 
		{
			return IsNull(_parent.ValueDateColumn);
		}

		/// <summary>Sets the TypedView field ValueDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValueDateNull() 
		{
			this[_parent.ValueDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Amount</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."AMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Amount
		{
			get { return IsAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.AmountColumn]; }
			set { this[_parent.AmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Amount is NULL, false otherwise.</summary>
		public bool IsAmountNull() 
		{
			return IsNull(_parent.AmountColumn);
		}

		/// <summary>Sets the TypedView field Amount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAmountNull() 
		{
			this[_parent.AmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingText</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."POSTINGTEXT"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 500</remarks>
		public System.String PostingText
		{
			get { return IsPostingTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PostingTextColumn]; }
			set { this[_parent.PostingTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingText is NULL, false otherwise.</summary>
		public bool IsPostingTextNull() 
		{
			return IsNull(_parent.PostingTextColumn);
		}

		/// <summary>Sets the TypedView field PostingText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingTextNull() 
		{
			this[_parent.PostingTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReceiptNumber</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."RECEIPTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 12</remarks>
		public System.String ReceiptNumber
		{
			get { return IsReceiptNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReceiptNumberColumn]; }
			set { this[_parent.ReceiptNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReceiptNumber is NULL, false otherwise.</summary>
		public bool IsReceiptNumberNull() 
		{
			return IsNull(_parent.ReceiptNumberColumn);
		}

		/// <summary>Sets the TypedView field ReceiptNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReceiptNumberNull() 
		{
			this[_parent.ReceiptNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingCodeNumber</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."POSTINGCODENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 PostingCodeNumber
		{
			get { return IsPostingCodeNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PostingCodeNumberColumn]; }
			set { this[_parent.PostingCodeNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingCodeNumber is NULL, false otherwise.</summary>
		public bool IsPostingCodeNumberNull() 
		{
			return IsNull(_parent.PostingCodeNumberColumn);
		}

		/// <summary>Sets the TypedView field PostingCodeNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingCodeNumberNull() 
		{
			this[_parent.PostingCodeNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingCodeText</summary>
		/// <remarks>Mapped on view field: "SL_DEBTORPOSTINGS"."POSTINGCODETEXT"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String PostingCodeText
		{
			get { return IsPostingCodeTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PostingCodeTextColumn]; }
			set { this[_parent.PostingCodeTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingCodeText is NULL, false otherwise.</summary>
		public bool IsPostingCodeTextNull() 
		{
			return IsNull(_parent.PostingCodeTextColumn);
		}

		/// <summary>Sets the TypedView field PostingCodeText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingCodeTextNull() 
		{
			this[_parent.PostingCodeTextColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
