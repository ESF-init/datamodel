﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'TransactionJournalHistory'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class TransactionJournalHistoryTypedView : TypedViewBase<TransactionJournalHistoryRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnTransactionID;
		private DataColumn _columnOperatorID;
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnTransactionType;
		private DataColumn _columnTimestamp;
		private DataColumn _columnTicketExternalNumber;
		private DataColumn _columnResult;
		private DataColumn _columnPurseCredit;
		private DataColumn _columnPurseBalance;
		private DataColumn _columnPurseCreditPreTax;
		private DataColumn _columnPurseBalancePreTax;
		private DataColumn _columnPrice;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnNumberOfPersons;
		private DataColumn _columnExternalTransactionIdentifier;
		private DataColumn _columnStopNumber;
		private DataColumn _columnStopCode;
		private DataColumn _columnStopName;
		private DataColumn _columnLineNumber;
		private DataColumn _columnLineCode;
		private DataColumn _columnLineName;
		private DataColumn _columnVehicleNumber;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnDeviceName;
		private DataColumn _columnDeviceDescription;
		private DataColumn _columnAppliedCapping;
		private DataColumn _columnTicketName;
		private DataColumn _columnInsertTime;
		private DataColumn _columnTransitAccountID;
		private DataColumn _columnCancellationReference;
		private DataColumn _columnHasCancellationReference;
		private DataColumn _columnTokenCardSerialNumber;
		private DataColumn _columnSalesPersonNumber;
		private DataColumn _columnReceiptReference;
		private DataColumn _columnNotes;
		private DataColumn _columnTransferredFromCardID;
		private DataColumn _columnFareScaleName;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 38;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static TransactionJournalHistoryTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionJournalHistoryTypedView():base("TransactionJournalHistory")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionJournalHistoryTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionJournalHistoryTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionJournalHistoryTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new TransactionJournalHistoryRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Timestamp", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketExternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Result", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCreditPreTax", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalancePreTax", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NumberOfPersons", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExternalTransactionIdentifier", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LineNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LineCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LineName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceDescription", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AppliedCapping", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InsertTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("HasCancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TokenCardSerialNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesPersonNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReceiptReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransferredFromCardID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareScaleName", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "TransactionJournalHistory";
			_columnTransactionID = GeneralUtils.CreateTypedDataTableColumn("TransactionID", @"TransactionID", typeof(System.String), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int64), this.Columns);
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnTransactionType = GeneralUtils.CreateTypedDataTableColumn("TransactionType", @"TransactionType", typeof(VarioSL.Entities.Enumerations.TransactionType), this.Columns);
			_columnTimestamp = GeneralUtils.CreateTypedDataTableColumn("Timestamp", @"Timestamp", typeof(System.DateTime), this.Columns);
			_columnTicketExternalNumber = GeneralUtils.CreateTypedDataTableColumn("TicketExternalNumber", @"TicketExternalNumber", typeof(System.Int32), this.Columns);
			_columnResult = GeneralUtils.CreateTypedDataTableColumn("Result", @"Result", typeof(VarioSL.Entities.Enumerations.FarePaymentResultType), this.Columns);
			_columnPurseCredit = GeneralUtils.CreateTypedDataTableColumn("PurseCredit", @"PurseCredit", typeof(System.Int32), this.Columns);
			_columnPurseBalance = GeneralUtils.CreateTypedDataTableColumn("PurseBalance", @"PurseBalance", typeof(System.Int32), this.Columns);
			_columnPurseCreditPreTax = GeneralUtils.CreateTypedDataTableColumn("PurseCreditPreTax", @"PurseCreditPreTax", typeof(System.Int32), this.Columns);
			_columnPurseBalancePreTax = GeneralUtils.CreateTypedDataTableColumn("PurseBalancePreTax", @"PurseBalancePreTax", typeof(System.Int32), this.Columns);
			_columnPrice = GeneralUtils.CreateTypedDataTableColumn("Price", @"Price", typeof(System.Int32), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnNumberOfPersons = GeneralUtils.CreateTypedDataTableColumn("NumberOfPersons", @"NumberOfPersons", typeof(System.Int32), this.Columns);
			_columnExternalTransactionIdentifier = GeneralUtils.CreateTypedDataTableColumn("ExternalTransactionIdentifier", @"ExternalTransactionIdentifier", typeof(System.String), this.Columns);
			_columnStopNumber = GeneralUtils.CreateTypedDataTableColumn("StopNumber", @"StopNumber", typeof(System.Int64), this.Columns);
			_columnStopCode = GeneralUtils.CreateTypedDataTableColumn("StopCode", @"StopCode", typeof(System.String), this.Columns);
			_columnStopName = GeneralUtils.CreateTypedDataTableColumn("StopName", @"StopName", typeof(System.String), this.Columns);
			_columnLineNumber = GeneralUtils.CreateTypedDataTableColumn("LineNumber", @"LineNumber", typeof(System.Int64), this.Columns);
			_columnLineCode = GeneralUtils.CreateTypedDataTableColumn("LineCode", @"LineCode", typeof(System.String), this.Columns);
			_columnLineName = GeneralUtils.CreateTypedDataTableColumn("LineName", @"LineName", typeof(System.String), this.Columns);
			_columnVehicleNumber = GeneralUtils.CreateTypedDataTableColumn("VehicleNumber", @"VehicleNumber", typeof(System.Int32), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnDeviceName = GeneralUtils.CreateTypedDataTableColumn("DeviceName", @"DeviceName", typeof(System.String), this.Columns);
			_columnDeviceDescription = GeneralUtils.CreateTypedDataTableColumn("DeviceDescription", @"DeviceDescription", typeof(System.String), this.Columns);
			_columnAppliedCapping = GeneralUtils.CreateTypedDataTableColumn("AppliedCapping", @"AppliedCapping", typeof(System.String), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnInsertTime = GeneralUtils.CreateTypedDataTableColumn("InsertTime", @"InsertTime", typeof(System.DateTime), this.Columns);
			_columnTransitAccountID = GeneralUtils.CreateTypedDataTableColumn("TransitAccountID", @"TransitAccountID", typeof(System.Int64), this.Columns);
			_columnCancellationReference = GeneralUtils.CreateTypedDataTableColumn("CancellationReference", @"CancellationReference", typeof(System.String), this.Columns);
			_columnHasCancellationReference = GeneralUtils.CreateTypedDataTableColumn("HasCancellationReference", @"HasCancellationReference", typeof(System.Boolean), this.Columns);
			_columnTokenCardSerialNumber = GeneralUtils.CreateTypedDataTableColumn("TokenCardSerialNumber", @"TokenCardSerialNumber", typeof(System.String), this.Columns);
			_columnSalesPersonNumber = GeneralUtils.CreateTypedDataTableColumn("SalesPersonNumber", @"SalesPersonNumber", typeof(System.Int32), this.Columns);
			_columnReceiptReference = GeneralUtils.CreateTypedDataTableColumn("ReceiptReference", @"ReceiptReference", typeof(System.String), this.Columns);
			_columnNotes = GeneralUtils.CreateTypedDataTableColumn("Notes", @"Notes", typeof(System.String), this.Columns);
			_columnTransferredFromCardID = GeneralUtils.CreateTypedDataTableColumn("TransferredFromCardID", @"TransferredFromCardID", typeof(System.Int64), this.Columns);
			_columnFareScaleName = GeneralUtils.CreateTypedDataTableColumn("FareScaleName", @"FareScaleName", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnTransactionID = this.Columns["TransactionID"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnTransactionType = this.Columns["TransactionType"];
			_columnTimestamp = this.Columns["Timestamp"];
			_columnTicketExternalNumber = this.Columns["TicketExternalNumber"];
			_columnResult = this.Columns["Result"];
			_columnPurseCredit = this.Columns["PurseCredit"];
			_columnPurseBalance = this.Columns["PurseBalance"];
			_columnPurseCreditPreTax = this.Columns["PurseCreditPreTax"];
			_columnPurseBalancePreTax = this.Columns["PurseBalancePreTax"];
			_columnPrice = this.Columns["Price"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnNumberOfPersons = this.Columns["NumberOfPersons"];
			_columnExternalTransactionIdentifier = this.Columns["ExternalTransactionIdentifier"];
			_columnStopNumber = this.Columns["StopNumber"];
			_columnStopCode = this.Columns["StopCode"];
			_columnStopName = this.Columns["StopName"];
			_columnLineNumber = this.Columns["LineNumber"];
			_columnLineCode = this.Columns["LineCode"];
			_columnLineName = this.Columns["LineName"];
			_columnVehicleNumber = this.Columns["VehicleNumber"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnDeviceName = this.Columns["DeviceName"];
			_columnDeviceDescription = this.Columns["DeviceDescription"];
			_columnAppliedCapping = this.Columns["AppliedCapping"];
			_columnTicketName = this.Columns["TicketName"];
			_columnInsertTime = this.Columns["InsertTime"];
			_columnTransitAccountID = this.Columns["TransitAccountID"];
			_columnCancellationReference = this.Columns["CancellationReference"];
			_columnHasCancellationReference = this.Columns["HasCancellationReference"];
			_columnTokenCardSerialNumber = this.Columns["TokenCardSerialNumber"];
			_columnSalesPersonNumber = this.Columns["SalesPersonNumber"];
			_columnReceiptReference = this.Columns["ReceiptReference"];
			_columnNotes = this.Columns["Notes"];
			_columnTransferredFromCardID = this.Columns["TransferredFromCardID"];
			_columnFareScaleName = this.Columns["FareScaleName"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			TransactionJournalHistoryTypedView cloneToReturn = ((TransactionJournalHistoryTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new TransactionJournalHistoryTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return TransactionJournalHistoryTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return TransactionJournalHistoryTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionID'</summary>
		internal DataColumn TransactionIDColumn 
		{
			get { return _columnTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionType'</summary>
		internal DataColumn TransactionTypeColumn 
		{
			get { return _columnTransactionType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Timestamp'</summary>
		internal DataColumn TimestampColumn 
		{
			get { return _columnTimestamp; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketExternalNumber'</summary>
		internal DataColumn TicketExternalNumberColumn 
		{
			get { return _columnTicketExternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Result'</summary>
		internal DataColumn ResultColumn 
		{
			get { return _columnResult; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit'</summary>
		internal DataColumn PurseCreditColumn 
		{
			get { return _columnPurseCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance'</summary>
		internal DataColumn PurseBalanceColumn 
		{
			get { return _columnPurseBalance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCreditPreTax'</summary>
		internal DataColumn PurseCreditPreTaxColumn 
		{
			get { return _columnPurseCreditPreTax; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalancePreTax'</summary>
		internal DataColumn PurseBalancePreTaxColumn 
		{
			get { return _columnPurseBalancePreTax; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Price'</summary>
		internal DataColumn PriceColumn 
		{
			get { return _columnPrice; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NumberOfPersons'</summary>
		internal DataColumn NumberOfPersonsColumn 
		{
			get { return _columnNumberOfPersons; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExternalTransactionIdentifier'</summary>
		internal DataColumn ExternalTransactionIdentifierColumn 
		{
			get { return _columnExternalTransactionIdentifier; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopNumber'</summary>
		internal DataColumn StopNumberColumn 
		{
			get { return _columnStopNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopCode'</summary>
		internal DataColumn StopCodeColumn 
		{
			get { return _columnStopCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopName'</summary>
		internal DataColumn StopNameColumn 
		{
			get { return _columnStopName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LineNumber'</summary>
		internal DataColumn LineNumberColumn 
		{
			get { return _columnLineNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LineCode'</summary>
		internal DataColumn LineCodeColumn 
		{
			get { return _columnLineCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LineName'</summary>
		internal DataColumn LineNameColumn 
		{
			get { return _columnLineName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'VehicleNumber'</summary>
		internal DataColumn VehicleNumberColumn 
		{
			get { return _columnVehicleNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceName'</summary>
		internal DataColumn DeviceNameColumn 
		{
			get { return _columnDeviceName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceDescription'</summary>
		internal DataColumn DeviceDescriptionColumn 
		{
			get { return _columnDeviceDescription; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AppliedCapping'</summary>
		internal DataColumn AppliedCappingColumn 
		{
			get { return _columnAppliedCapping; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InsertTime'</summary>
		internal DataColumn InsertTimeColumn 
		{
			get { return _columnInsertTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransitAccountID'</summary>
		internal DataColumn TransitAccountIDColumn 
		{
			get { return _columnTransitAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReference'</summary>
		internal DataColumn CancellationReferenceColumn 
		{
			get { return _columnCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'HasCancellationReference'</summary>
		internal DataColumn HasCancellationReferenceColumn 
		{
			get { return _columnHasCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TokenCardSerialNumber'</summary>
		internal DataColumn TokenCardSerialNumberColumn 
		{
			get { return _columnTokenCardSerialNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesPersonNumber'</summary>
		internal DataColumn SalesPersonNumberColumn 
		{
			get { return _columnSalesPersonNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReceiptReference'</summary>
		internal DataColumn ReceiptReferenceColumn 
		{
			get { return _columnReceiptReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Notes'</summary>
		internal DataColumn NotesColumn 
		{
			get { return _columnNotes; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransferredFromCardID'</summary>
		internal DataColumn TransferredFromCardIDColumn 
		{
			get { return _columnTransferredFromCardID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareScaleName'</summary>
		internal DataColumn FareScaleNameColumn 
		{
			get { return _columnFareScaleName; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable TransactionJournalHistory</summary>
	public partial class TransactionJournalHistoryRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private TransactionJournalHistoryTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal TransactionJournalHistoryRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((TransactionJournalHistoryTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field TransactionID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String TransactionID
		{
			get { return IsTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TransactionIDColumn]; }
			set { this[_parent.TransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionID is NULL, false otherwise.</summary>
		public bool IsTransactionIDNull() 
		{
			return IsNull(_parent.TransactionIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionIDNull() 
		{
			this[_parent.TransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionType</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TRANSACTIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionType TransactionType
		{
			get { return IsTransactionTypeNull() ? (VarioSL.Entities.Enumerations.TransactionType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionType)) : (VarioSL.Entities.Enumerations.TransactionType)this[_parent.TransactionTypeColumn]; }
			set { this[_parent.TransactionTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionType is NULL, false otherwise.</summary>
		public bool IsTransactionTypeNull() 
		{
			return IsNull(_parent.TransactionTypeColumn);
		}

		/// <summary>Sets the TypedView field TransactionType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionTypeNull() 
		{
			this[_parent.TransactionTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Timestamp</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TIMESTAMP"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime Timestamp
		{
			get { return IsTimestampNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TimestampColumn]; }
			set { this[_parent.TimestampColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Timestamp is NULL, false otherwise.</summary>
		public bool IsTimestampNull() 
		{
			return IsNull(_parent.TimestampColumn);
		}

		/// <summary>Sets the TypedView field Timestamp to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTimestampNull() 
		{
			this[_parent.TimestampColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketExternalNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TICKETEXTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TicketExternalNumber
		{
			get { return IsTicketExternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TicketExternalNumberColumn]; }
			set { this[_parent.TicketExternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketExternalNumber is NULL, false otherwise.</summary>
		public bool IsTicketExternalNumberNull() 
		{
			return IsNull(_parent.TicketExternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketExternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketExternalNumberNull() 
		{
			this[_parent.TicketExternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Result</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."RESULT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.FarePaymentResultType Result
		{
			get { return IsResultNull() ? (VarioSL.Entities.Enumerations.FarePaymentResultType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.FarePaymentResultType)) : (VarioSL.Entities.Enumerations.FarePaymentResultType)this[_parent.ResultColumn]; }
			set { this[_parent.ResultColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Result is NULL, false otherwise.</summary>
		public bool IsResultNull() 
		{
			return IsNull(_parent.ResultColumn);
		}

		/// <summary>Sets the TypedView field Result to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetResultNull() 
		{
			this[_parent.ResultColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."PURSECREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit
		{
			get { return IsPurseCreditNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCreditColumn]; }
			set { this[_parent.PurseCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit is NULL, false otherwise.</summary>
		public bool IsPurseCreditNull() 
		{
			return IsNull(_parent.PurseCreditColumn);
		}

		/// <summary>Sets the TypedView field PurseCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCreditNull() 
		{
			this[_parent.PurseCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."PURSEBALANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance
		{
			get { return IsPurseBalanceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalanceColumn]; }
			set { this[_parent.PurseBalanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance is NULL, false otherwise.</summary>
		public bool IsPurseBalanceNull() 
		{
			return IsNull(_parent.PurseBalanceColumn);
		}

		/// <summary>Sets the TypedView field PurseBalance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalanceNull() 
		{
			this[_parent.PurseBalanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCreditPreTax</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."PURSECREDITPRETAX"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCreditPreTax
		{
			get { return IsPurseCreditPreTaxNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCreditPreTaxColumn]; }
			set { this[_parent.PurseCreditPreTaxColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCreditPreTax is NULL, false otherwise.</summary>
		public bool IsPurseCreditPreTaxNull() 
		{
			return IsNull(_parent.PurseCreditPreTaxColumn);
		}

		/// <summary>Sets the TypedView field PurseCreditPreTax to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCreditPreTaxNull() 
		{
			this[_parent.PurseCreditPreTaxColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalancePreTax</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."PURSEBALANCEPRETAX"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalancePreTax
		{
			get { return IsPurseBalancePreTaxNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalancePreTaxColumn]; }
			set { this[_parent.PurseBalancePreTaxColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalancePreTax is NULL, false otherwise.</summary>
		public bool IsPurseBalancePreTaxNull() 
		{
			return IsNull(_parent.PurseBalancePreTaxColumn);
		}

		/// <summary>Sets the TypedView field PurseBalancePreTax to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalancePreTaxNull() 
		{
			this[_parent.PurseBalancePreTaxColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Price</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."PRICE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Price
		{
			get { return IsPriceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PriceColumn]; }
			set { this[_parent.PriceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Price is NULL, false otherwise.</summary>
		public bool IsPriceNull() 
		{
			return IsNull(_parent.PriceColumn);
		}

		/// <summary>Sets the TypedView field Price to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPriceNull() 
		{
			this[_parent.PriceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NumberOfPersons</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."NUMBEROFPERSONS"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 NumberOfPersons
		{
			get { return IsNumberOfPersonsNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.NumberOfPersonsColumn]; }
			set { this[_parent.NumberOfPersonsColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NumberOfPersons is NULL, false otherwise.</summary>
		public bool IsNumberOfPersonsNull() 
		{
			return IsNull(_parent.NumberOfPersonsColumn);
		}

		/// <summary>Sets the TypedView field NumberOfPersons to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNumberOfPersonsNull() 
		{
			this[_parent.NumberOfPersonsColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExternalTransactionIdentifier</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."EXTERNALTRANSACTIONIDENTIFIER"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 64</remarks>
		public System.String ExternalTransactionIdentifier
		{
			get { return IsExternalTransactionIdentifierNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExternalTransactionIdentifierColumn]; }
			set { this[_parent.ExternalTransactionIdentifierColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExternalTransactionIdentifier is NULL, false otherwise.</summary>
		public bool IsExternalTransactionIdentifierNull() 
		{
			return IsNull(_parent.ExternalTransactionIdentifierColumn);
		}

		/// <summary>Sets the TypedView field ExternalTransactionIdentifier to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExternalTransactionIdentifierNull() 
		{
			this[_parent.ExternalTransactionIdentifierColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."STOPNO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 StopNumber
		{
			get { return IsStopNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.StopNumberColumn]; }
			set { this[_parent.StopNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopNumber is NULL, false otherwise.</summary>
		public bool IsStopNumberNull() 
		{
			return IsNull(_parent.StopNumberColumn);
		}

		/// <summary>Sets the TypedView field StopNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopNumberNull() 
		{
			this[_parent.StopNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopCode</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."STOPCODE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String StopCode
		{
			get { return IsStopCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopCodeColumn]; }
			set { this[_parent.StopCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopCode is NULL, false otherwise.</summary>
		public bool IsStopCodeNull() 
		{
			return IsNull(_parent.StopCodeColumn);
		}

		/// <summary>Sets the TypedView field StopCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopCodeNull() 
		{
			this[_parent.StopCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."STOPNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String StopName
		{
			get { return IsStopNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopNameColumn]; }
			set { this[_parent.StopNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopName is NULL, false otherwise.</summary>
		public bool IsStopNameNull() 
		{
			return IsNull(_parent.StopNameColumn);
		}

		/// <summary>Sets the TypedView field StopName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopNameNull() 
		{
			this[_parent.StopNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LineNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."LINENO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 LineNumber
		{
			get { return IsLineNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.LineNumberColumn]; }
			set { this[_parent.LineNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LineNumber is NULL, false otherwise.</summary>
		public bool IsLineNumberNull() 
		{
			return IsNull(_parent.LineNumberColumn);
		}

		/// <summary>Sets the TypedView field LineNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNumberNull() 
		{
			this[_parent.LineNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LineCode</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."LINECODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LineCode
		{
			get { return IsLineCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineCodeColumn]; }
			set { this[_parent.LineCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LineCode is NULL, false otherwise.</summary>
		public bool IsLineCodeNull() 
		{
			return IsNull(_parent.LineCodeColumn);
		}

		/// <summary>Sets the TypedView field LineCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineCodeNull() 
		{
			this[_parent.LineCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LineName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."LINENAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String LineName
		{
			get { return IsLineNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineNameColumn]; }
			set { this[_parent.LineNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LineName is NULL, false otherwise.</summary>
		public bool IsLineNameNull() 
		{
			return IsNull(_parent.LineNameColumn);
		}

		/// <summary>Sets the TypedView field LineName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNameNull() 
		{
			this[_parent.LineNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field VehicleNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."VEHICLENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 VehicleNumber
		{
			get { return IsVehicleNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.VehicleNumberColumn]; }
			set { this[_parent.VehicleNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field VehicleNumber is NULL, false otherwise.</summary>
		public bool IsVehicleNumberNull() 
		{
			return IsNull(_parent.VehicleNumberColumn);
		}

		/// <summary>Sets the TypedView field VehicleNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetVehicleNumberNull() 
		{
			this[_parent.VehicleNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."DEVICENAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String DeviceName
		{
			get { return IsDeviceNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.DeviceNameColumn]; }
			set { this[_parent.DeviceNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceName is NULL, false otherwise.</summary>
		public bool IsDeviceNameNull() 
		{
			return IsNull(_parent.DeviceNameColumn);
		}

		/// <summary>Sets the TypedView field DeviceName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNameNull() 
		{
			this[_parent.DeviceNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceDescription</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."DEVICEDESCRIPTION"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 512</remarks>
		public System.String DeviceDescription
		{
			get { return IsDeviceDescriptionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.DeviceDescriptionColumn]; }
			set { this[_parent.DeviceDescriptionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceDescription is NULL, false otherwise.</summary>
		public bool IsDeviceDescriptionNull() 
		{
			return IsNull(_parent.DeviceDescriptionColumn);
		}

		/// <summary>Sets the TypedView field DeviceDescription to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceDescriptionNull() 
		{
			this[_parent.DeviceDescriptionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AppliedCapping</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."APPLIEDCAPPING"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String AppliedCapping
		{
			get { return IsAppliedCappingNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AppliedCappingColumn]; }
			set { this[_parent.AppliedCappingColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AppliedCapping is NULL, false otherwise.</summary>
		public bool IsAppliedCappingNull() 
		{
			return IsNull(_parent.AppliedCappingColumn);
		}

		/// <summary>Sets the TypedView field AppliedCapping to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAppliedCappingNull() 
		{
			this[_parent.AppliedCappingColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InsertTime</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."INSERTTIME"<br/>
		/// View field characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0</remarks>
		public System.DateTime InsertTime
		{
			get { return IsInsertTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.InsertTimeColumn]; }
			set { this[_parent.InsertTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InsertTime is NULL, false otherwise.</summary>
		public bool IsInsertTimeNull() 
		{
			return IsNull(_parent.InsertTimeColumn);
		}

		/// <summary>Sets the TypedView field InsertTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInsertTimeNull() 
		{
			this[_parent.InsertTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransitAccountID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TRANSITACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransitAccountID
		{
			get { return IsTransitAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransitAccountIDColumn]; }
			set { this[_parent.TransitAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransitAccountID is NULL, false otherwise.</summary>
		public bool IsTransitAccountIDNull() 
		{
			return IsNull(_parent.TransitAccountIDColumn);
		}

		/// <summary>Sets the TypedView field TransitAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransitAccountIDNull() 
		{
			this[_parent.TransitAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReference</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."CANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CancellationReference
		{
			get { return IsCancellationReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CancellationReferenceColumn]; }
			set { this[_parent.CancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReference is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceNull() 
		{
			return IsNull(_parent.CancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field CancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceNull() 
		{
			this[_parent.CancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field HasCancellationReference</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."HASCANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Boolean HasCancellationReference
		{
			get { return IsHasCancellationReferenceNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.HasCancellationReferenceColumn]; }
			set { this[_parent.HasCancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field HasCancellationReference is NULL, false otherwise.</summary>
		public bool IsHasCancellationReferenceNull() 
		{
			return IsNull(_parent.HasCancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field HasCancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetHasCancellationReferenceNull() 
		{
			this[_parent.HasCancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TokenCardSerialNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TOKENCARDSERIALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String TokenCardSerialNumber
		{
			get { return IsTokenCardSerialNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TokenCardSerialNumberColumn]; }
			set { this[_parent.TokenCardSerialNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TokenCardSerialNumber is NULL, false otherwise.</summary>
		public bool IsTokenCardSerialNumberNull() 
		{
			return IsNull(_parent.TokenCardSerialNumberColumn);
		}

		/// <summary>Sets the TypedView field TokenCardSerialNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTokenCardSerialNumberNull() 
		{
			this[_parent.TokenCardSerialNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesPersonNumber</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."SALESPERSONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SalesPersonNumber
		{
			get { return IsSalesPersonNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SalesPersonNumberColumn]; }
			set { this[_parent.SalesPersonNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesPersonNumber is NULL, false otherwise.</summary>
		public bool IsSalesPersonNumberNull() 
		{
			return IsNull(_parent.SalesPersonNumberColumn);
		}

		/// <summary>Sets the TypedView field SalesPersonNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesPersonNumberNull() 
		{
			this[_parent.SalesPersonNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReceiptReference</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."RECEIPTREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String ReceiptReference
		{
			get { return IsReceiptReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReceiptReferenceColumn]; }
			set { this[_parent.ReceiptReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReceiptReference is NULL, false otherwise.</summary>
		public bool IsReceiptReferenceNull() 
		{
			return IsNull(_parent.ReceiptReferenceColumn);
		}

		/// <summary>Sets the TypedView field ReceiptReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReceiptReferenceNull() 
		{
			this[_parent.ReceiptReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Notes</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."NOTES"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255</remarks>
		public System.String Notes
		{
			get { return IsNotesNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NotesColumn]; }
			set { this[_parent.NotesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Notes is NULL, false otherwise.</summary>
		public bool IsNotesNull() 
		{
			return IsNull(_parent.NotesColumn);
		}

		/// <summary>Sets the TypedView field Notes to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNotesNull() 
		{
			this[_parent.NotesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransferredFromCardID</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."TRANSFERREDFROMCARDID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransferredFromCardID
		{
			get { return IsTransferredFromCardIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransferredFromCardIDColumn]; }
			set { this[_parent.TransferredFromCardIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransferredFromCardID is NULL, false otherwise.</summary>
		public bool IsTransferredFromCardIDNull() 
		{
			return IsNull(_parent.TransferredFromCardIDColumn);
		}

		/// <summary>Sets the TypedView field TransferredFromCardID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransferredFromCardIDNull() 
		{
			this[_parent.TransferredFromCardIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareScaleName</summary>
		/// <remarks>Mapped on view field: "SL_TRANSJOURNHISTWITHCANCELLED"."FARESCALENAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String FareScaleName
		{
			get { return IsFareScaleNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareScaleNameColumn]; }
			set { this[_parent.FareScaleNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareScaleName is NULL, false otherwise.</summary>
		public bool IsFareScaleNameNull() 
		{
			return IsNull(_parent.FareScaleNameColumn);
		}

		/// <summary>Sets the TypedView field FareScaleName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareScaleNameNull() 
		{
			this[_parent.FareScaleNameColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
