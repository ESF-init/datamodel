﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'ChangeVoucher'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class ChangeVoucherTypedView : TypedViewBase<ChangeVoucherRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnChangeVoucherID;
		private DataColumn _columnDebtor;
		private DataColumn _columnDebtorName;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnIssueDate;
		private DataColumn _columnLocationNumber;
		private DataColumn _columnLocation;
		private DataColumn _columnReference;
		private DataColumn _columnIsRedeemed;
		private DataColumn _columnRedeemDate;
		private DataColumn _columnRedeemBy;
		private DataColumn _columnRedeemedOnDevice;
		private DataColumn _columnSalesTransactionID;
		private DataColumn _columnProductID;
		private DataColumn _columnFareAmount;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 15;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static ChangeVoucherTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ChangeVoucherTypedView():base("ChangeVoucher")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ChangeVoucherTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.ChangeVoucherTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.ChangeVoucherTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new ChangeVoucherRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ChangeVoucherID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Debtor", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebtorName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IssueDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Location", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Reference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsRedeemed", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RedeemDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RedeemBy", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RedeemedOnDevice", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesTransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "ChangeVoucher";
			_columnChangeVoucherID = GeneralUtils.CreateTypedDataTableColumn("ChangeVoucherID", @"ChangeVoucherID", typeof(System.Int64), this.Columns);
			_columnDebtor = GeneralUtils.CreateTypedDataTableColumn("Debtor", @"Debtor", typeof(System.Int64), this.Columns);
			_columnDebtorName = GeneralUtils.CreateTypedDataTableColumn("DebtorName", @"DebtorName", typeof(System.String), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnIssueDate = GeneralUtils.CreateTypedDataTableColumn("IssueDate", @"IssueDate", typeof(System.DateTime), this.Columns);
			_columnLocationNumber = GeneralUtils.CreateTypedDataTableColumn("LocationNumber", @"LocationNumber", typeof(System.Int64), this.Columns);
			_columnLocation = GeneralUtils.CreateTypedDataTableColumn("Location", @"Location", typeof(System.String), this.Columns);
			_columnReference = GeneralUtils.CreateTypedDataTableColumn("Reference", @"Reference", typeof(System.String), this.Columns);
			_columnIsRedeemed = GeneralUtils.CreateTypedDataTableColumn("IsRedeemed", @"IsRedeemed", typeof(System.Boolean), this.Columns);
			_columnRedeemDate = GeneralUtils.CreateTypedDataTableColumn("RedeemDate", @"RedeemDate", typeof(System.DateTime), this.Columns);
			_columnRedeemBy = GeneralUtils.CreateTypedDataTableColumn("RedeemBy", @"RedeemBy", typeof(System.Int32), this.Columns);
			_columnRedeemedOnDevice = GeneralUtils.CreateTypedDataTableColumn("RedeemedOnDevice", @"RedeemedOnDevice", typeof(System.Int32), this.Columns);
			_columnSalesTransactionID = GeneralUtils.CreateTypedDataTableColumn("SalesTransactionID", @"SalesTransactionID", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnFareAmount = GeneralUtils.CreateTypedDataTableColumn("FareAmount", @"FareAmount", typeof(System.Int32), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnChangeVoucherID = this.Columns["ChangeVoucherID"];
			_columnDebtor = this.Columns["Debtor"];
			_columnDebtorName = this.Columns["DebtorName"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnIssueDate = this.Columns["IssueDate"];
			_columnLocationNumber = this.Columns["LocationNumber"];
			_columnLocation = this.Columns["Location"];
			_columnReference = this.Columns["Reference"];
			_columnIsRedeemed = this.Columns["IsRedeemed"];
			_columnRedeemDate = this.Columns["RedeemDate"];
			_columnRedeemBy = this.Columns["RedeemBy"];
			_columnRedeemedOnDevice = this.Columns["RedeemedOnDevice"];
			_columnSalesTransactionID = this.Columns["SalesTransactionID"];
			_columnProductID = this.Columns["ProductID"];
			_columnFareAmount = this.Columns["FareAmount"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			ChangeVoucherTypedView cloneToReturn = ((ChangeVoucherTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new ChangeVoucherTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return ChangeVoucherTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return ChangeVoucherTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'ChangeVoucherID'</summary>
		internal DataColumn ChangeVoucherIDColumn 
		{
			get { return _columnChangeVoucherID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Debtor'</summary>
		internal DataColumn DebtorColumn 
		{
			get { return _columnDebtor; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebtorName'</summary>
		internal DataColumn DebtorNameColumn 
		{
			get { return _columnDebtorName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IssueDate'</summary>
		internal DataColumn IssueDateColumn 
		{
			get { return _columnIssueDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LocationNumber'</summary>
		internal DataColumn LocationNumberColumn 
		{
			get { return _columnLocationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Location'</summary>
		internal DataColumn LocationColumn 
		{
			get { return _columnLocation; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Reference'</summary>
		internal DataColumn ReferenceColumn 
		{
			get { return _columnReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsRedeemed'</summary>
		internal DataColumn IsRedeemedColumn 
		{
			get { return _columnIsRedeemed; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RedeemDate'</summary>
		internal DataColumn RedeemDateColumn 
		{
			get { return _columnRedeemDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RedeemBy'</summary>
		internal DataColumn RedeemByColumn 
		{
			get { return _columnRedeemBy; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RedeemedOnDevice'</summary>
		internal DataColumn RedeemedOnDeviceColumn 
		{
			get { return _columnRedeemedOnDevice; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesTransactionID'</summary>
		internal DataColumn SalesTransactionIDColumn 
		{
			get { return _columnSalesTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareAmount'</summary>
		internal DataColumn FareAmountColumn 
		{
			get { return _columnFareAmount; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable ChangeVoucher</summary>
	public partial class ChangeVoucherRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private ChangeVoucherTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal ChangeVoucherRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((ChangeVoucherTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field ChangeVoucherID</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."CHANGEVOUCHERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ChangeVoucherID
		{
			get { return IsChangeVoucherIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ChangeVoucherIDColumn]; }
			set { this[_parent.ChangeVoucherIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ChangeVoucherID is NULL, false otherwise.</summary>
		public bool IsChangeVoucherIDNull() 
		{
			return IsNull(_parent.ChangeVoucherIDColumn);
		}

		/// <summary>Sets the TypedView field ChangeVoucherID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetChangeVoucherIDNull() 
		{
			this[_parent.ChangeVoucherIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Debtor</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."DEBTOR"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 Debtor
		{
			get { return IsDebtorNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.DebtorColumn]; }
			set { this[_parent.DebtorColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Debtor is NULL, false otherwise.</summary>
		public bool IsDebtorNull() 
		{
			return IsNull(_parent.DebtorColumn);
		}

		/// <summary>Sets the TypedView field Debtor to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebtorNull() 
		{
			this[_parent.DebtorColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebtorName</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."DEBTORNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String DebtorName
		{
			get { return IsDebtorNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.DebtorNameColumn]; }
			set { this[_parent.DebtorNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebtorName is NULL, false otherwise.</summary>
		public bool IsDebtorNameNull() 
		{
			return IsNull(_parent.DebtorNameColumn);
		}

		/// <summary>Sets the TypedView field DebtorName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebtorNameNull() 
		{
			this[_parent.DebtorNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IssueDate</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."ISSUEDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime IssueDate
		{
			get { return IsIssueDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.IssueDateColumn]; }
			set { this[_parent.IssueDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IssueDate is NULL, false otherwise.</summary>
		public bool IsIssueDateNull() 
		{
			return IsNull(_parent.IssueDateColumn);
		}

		/// <summary>Sets the TypedView field IssueDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIssueDateNull() 
		{
			this[_parent.IssueDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LocationNumber</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."LOCATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 LocationNumber
		{
			get { return IsLocationNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.LocationNumberColumn]; }
			set { this[_parent.LocationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LocationNumber is NULL, false otherwise.</summary>
		public bool IsLocationNumberNull() 
		{
			return IsNull(_parent.LocationNumberColumn);
		}

		/// <summary>Sets the TypedView field LocationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNumberNull() 
		{
			this[_parent.LocationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Location</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."LOCATION"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String Location
		{
			get { return IsLocationNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LocationColumn]; }
			set { this[_parent.LocationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Location is NULL, false otherwise.</summary>
		public bool IsLocationNull() 
		{
			return IsNull(_parent.LocationColumn);
		}

		/// <summary>Sets the TypedView field Location to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNull() 
		{
			this[_parent.LocationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Reference</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."REFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String Reference
		{
			get { return IsReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReferenceColumn]; }
			set { this[_parent.ReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Reference is NULL, false otherwise.</summary>
		public bool IsReferenceNull() 
		{
			return IsNull(_parent.ReferenceColumn);
		}

		/// <summary>Sets the TypedView field Reference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReferenceNull() 
		{
			this[_parent.ReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsRedeemed</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."ISREDEEMED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Boolean IsRedeemed
		{
			get { return IsIsRedeemedNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsRedeemedColumn]; }
			set { this[_parent.IsRedeemedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsRedeemed is NULL, false otherwise.</summary>
		public bool IsIsRedeemedNull() 
		{
			return IsNull(_parent.IsRedeemedColumn);
		}

		/// <summary>Sets the TypedView field IsRedeemed to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsRedeemedNull() 
		{
			this[_parent.IsRedeemedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RedeemDate</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."REDEEMDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime RedeemDate
		{
			get { return IsRedeemDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.RedeemDateColumn]; }
			set { this[_parent.RedeemDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RedeemDate is NULL, false otherwise.</summary>
		public bool IsRedeemDateNull() 
		{
			return IsNull(_parent.RedeemDateColumn);
		}

		/// <summary>Sets the TypedView field RedeemDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRedeemDateNull() 
		{
			this[_parent.RedeemDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RedeemBy</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."REDEEMBY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 RedeemBy
		{
			get { return IsRedeemByNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.RedeemByColumn]; }
			set { this[_parent.RedeemByColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RedeemBy is NULL, false otherwise.</summary>
		public bool IsRedeemByNull() 
		{
			return IsNull(_parent.RedeemByColumn);
		}

		/// <summary>Sets the TypedView field RedeemBy to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRedeemByNull() 
		{
			this[_parent.RedeemByColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RedeemedOnDevice</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."REDEEMEDONDEVICE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 RedeemedOnDevice
		{
			get { return IsRedeemedOnDeviceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.RedeemedOnDeviceColumn]; }
			set { this[_parent.RedeemedOnDeviceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RedeemedOnDevice is NULL, false otherwise.</summary>
		public bool IsRedeemedOnDeviceNull() 
		{
			return IsNull(_parent.RedeemedOnDeviceColumn);
		}

		/// <summary>Sets the TypedView field RedeemedOnDevice to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRedeemedOnDeviceNull() 
		{
			this[_parent.RedeemedOnDeviceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesTransactionID</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."SALESTRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String SalesTransactionID
		{
			get { return IsSalesTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SalesTransactionIDColumn]; }
			set { this[_parent.SalesTransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesTransactionID is NULL, false otherwise.</summary>
		public bool IsSalesTransactionIDNull() 
		{
			return IsNull(_parent.SalesTransactionIDColumn);
		}

		/// <summary>Sets the TypedView field SalesTransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesTransactionIDNull() 
		{
			this[_parent.SalesTransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareAmount</summary>
		/// <remarks>Mapped on view field: "SL_CHANGEVOUCHER"."FAREAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FareAmount
		{
			get { return IsFareAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FareAmountColumn]; }
			set { this[_parent.FareAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareAmount is NULL, false otherwise.</summary>
		public bool IsFareAmountNull() 
		{
			return IsNull(_parent.FareAmountColumn);
		}

		/// <summary>Sets the TypedView field FareAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareAmountNull() 
		{
			this[_parent.FareAmountColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
