﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'TransactionJournalDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class TransactionJournalDetailTypedView : TypedViewBase<TransactionJournalDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnTransactionJournalID;
		private DataColumn _columnTransactionID;
		private DataColumn _columnDeviceTime;
		private DataColumn _columnBoardingTime;
		private DataColumn _columnOperatorID;
		private DataColumn _columnClientID;
		private DataColumn _columnLine;
		private DataColumn _columnCourseNumber;
		private DataColumn _columnRouteNumber;
		private DataColumn _columnRouteCode;
		private DataColumn _columnDirection;
		private DataColumn _columnZone;
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnVehicleNumber;
		private DataColumn _columnMountingPlateNumber;
		private DataColumn _columnDebtorNumber;
		private DataColumn _columnGeoLocationLongitude;
		private DataColumn _columnGeoLocationLatitude;
		private DataColumn _columnStopNumber;
		private DataColumn _columnTransitAccountID;
		private DataColumn _columnPrintedNumber;
		private DataColumn _columnSerialNumber;
		private DataColumn _columnFareMediaID;
		private DataColumn _columnFareMediaType;
		private DataColumn _columnProductID;
		private DataColumn _columnTicketID;
		private DataColumn _columnFareAmount;
		private DataColumn _columnCustomerGroup;
		private DataColumn _columnTransactionType;
		private DataColumn _columnTransactionNumber;
		private DataColumn _columnResultType;
		private DataColumn _columnFillLevel;
		private DataColumn _columnPurseBalance;
		private DataColumn _columnPurseCredit;
		private DataColumn _columnGroupSize;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnDuration;
		private DataColumn _columnTariffDate;
		private DataColumn _columnTariffVersion;
		private DataColumn _columnTicketInternalNumber;
		private DataColumn _columnWhitelistVersion;
		private DataColumn _columnCancellationReference;
		private DataColumn _columnCancellationReferenceGuid;
		private DataColumn _columnLastUser;
		private DataColumn _columnLastModified;
		private DataColumn _columnTransactionCounter;
		private DataColumn _columnWhitelistVersionCreated;
		private DataColumn _columnProperties;
		private DataColumn _columnSaleID;
		private DataColumn _columnTripTicketInternalNumber;
		private DataColumn _columnTicketName;
		private DataColumn _columnShiftBegin;
		private DataColumn _columnCompletionState;
		private DataColumn _columnTransportState;
		private DataColumn _columnProductID2;
		private DataColumn _columnPurseBalance2;
		private DataColumn _columnPurseCredit2;
		private DataColumn _columnCreditCardAuthorizationID;
		private DataColumn _columnTripCounter;
		private DataColumn _columnInsertTime;
		private DataColumn _columnIsVoiceEnabled;
		private DataColumn _columnIsTraining;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 64;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static TransactionJournalDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public TransactionJournalDetailTypedView():base("TransactionJournalDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionJournalDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionJournalDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.TransactionJournalDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new TransactionJournalDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BoardingTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CourseNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RouteNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RouteCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Zone", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GeoLocationLongitude", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GeoLocationLatitude", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FillLevel", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GroupSize", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WhitelistVersion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WhitelistVersionCreated", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Properties", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CompletionState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransportState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripCounter", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InsertTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsVoiceEnabled", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsTraining", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "TransactionJournalDetail";
			_columnTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalID", @"TransactionJournalID", typeof(System.Int64), this.Columns);
			_columnTransactionID = GeneralUtils.CreateTypedDataTableColumn("TransactionID", @"TransactionID", typeof(System.String), this.Columns);
			_columnDeviceTime = GeneralUtils.CreateTypedDataTableColumn("DeviceTime", @"DeviceTime", typeof(System.DateTime), this.Columns);
			_columnBoardingTime = GeneralUtils.CreateTypedDataTableColumn("BoardingTime", @"BoardingTime", typeof(System.DateTime), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int64), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnLine = GeneralUtils.CreateTypedDataTableColumn("Line", @"Line", typeof(System.String), this.Columns);
			_columnCourseNumber = GeneralUtils.CreateTypedDataTableColumn("CourseNumber", @"CourseNumber", typeof(System.Int32), this.Columns);
			_columnRouteNumber = GeneralUtils.CreateTypedDataTableColumn("RouteNumber", @"RouteNumber", typeof(System.Int32), this.Columns);
			_columnRouteCode = GeneralUtils.CreateTypedDataTableColumn("RouteCode", @"RouteCode", typeof(System.String), this.Columns);
			_columnDirection = GeneralUtils.CreateTypedDataTableColumn("Direction", @"Direction", typeof(System.Int32), this.Columns);
			_columnZone = GeneralUtils.CreateTypedDataTableColumn("Zone", @"Zone", typeof(System.Int32), this.Columns);
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnVehicleNumber = GeneralUtils.CreateTypedDataTableColumn("VehicleNumber", @"VehicleNumber", typeof(System.Int32), this.Columns);
			_columnMountingPlateNumber = GeneralUtils.CreateTypedDataTableColumn("MountingPlateNumber", @"MountingPlateNumber", typeof(System.Int32), this.Columns);
			_columnDebtorNumber = GeneralUtils.CreateTypedDataTableColumn("DebtorNumber", @"DebtorNumber", typeof(System.Int32), this.Columns);
			_columnGeoLocationLongitude = GeneralUtils.CreateTypedDataTableColumn("GeoLocationLongitude", @"GeoLocationLongitude", typeof(System.Int32), this.Columns);
			_columnGeoLocationLatitude = GeneralUtils.CreateTypedDataTableColumn("GeoLocationLatitude", @"GeoLocationLatitude", typeof(System.Int32), this.Columns);
			_columnStopNumber = GeneralUtils.CreateTypedDataTableColumn("StopNumber", @"StopNumber", typeof(System.Int32), this.Columns);
			_columnTransitAccountID = GeneralUtils.CreateTypedDataTableColumn("TransitAccountID", @"TransitAccountID", typeof(System.Int64), this.Columns);
			_columnPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("PrintedNumber", @"PrintedNumber", typeof(System.String), this.Columns);
			_columnSerialNumber = GeneralUtils.CreateTypedDataTableColumn("SerialNumber", @"SerialNumber", typeof(System.String), this.Columns);
			_columnFareMediaID = GeneralUtils.CreateTypedDataTableColumn("FareMediaID", @"FareMediaID", typeof(System.String), this.Columns);
			_columnFareMediaType = GeneralUtils.CreateTypedDataTableColumn("FareMediaType", @"FareMediaType", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnFareAmount = GeneralUtils.CreateTypedDataTableColumn("FareAmount", @"FareAmount", typeof(System.Int32), this.Columns);
			_columnCustomerGroup = GeneralUtils.CreateTypedDataTableColumn("CustomerGroup", @"CustomerGroup", typeof(System.String), this.Columns);
			_columnTransactionType = GeneralUtils.CreateTypedDataTableColumn("TransactionType", @"TransactionType", typeof(VarioSL.Entities.Enumerations.TransactionType), this.Columns);
			_columnTransactionNumber = GeneralUtils.CreateTypedDataTableColumn("TransactionNumber", @"TransactionNumber", typeof(System.Int32), this.Columns);
			_columnResultType = GeneralUtils.CreateTypedDataTableColumn("ResultType", @"ResultType", typeof(VarioSL.Entities.Enumerations.FarePaymentResultType), this.Columns);
			_columnFillLevel = GeneralUtils.CreateTypedDataTableColumn("FillLevel", @"FillLevel", typeof(System.Int32), this.Columns);
			_columnPurseBalance = GeneralUtils.CreateTypedDataTableColumn("PurseBalance", @"PurseBalance", typeof(System.Int32), this.Columns);
			_columnPurseCredit = GeneralUtils.CreateTypedDataTableColumn("PurseCredit", @"PurseCredit", typeof(System.Int32), this.Columns);
			_columnGroupSize = GeneralUtils.CreateTypedDataTableColumn("GroupSize", @"GroupSize", typeof(System.Int32), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnDuration = GeneralUtils.CreateTypedDataTableColumn("Duration", @"Duration", typeof(System.Int64), this.Columns);
			_columnTariffDate = GeneralUtils.CreateTypedDataTableColumn("TariffDate", @"TariffDate", typeof(System.DateTime), this.Columns);
			_columnTariffVersion = GeneralUtils.CreateTypedDataTableColumn("TariffVersion", @"TariffVersion", typeof(System.Int32), this.Columns);
			_columnTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TicketInternalNumber", @"TicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnWhitelistVersion = GeneralUtils.CreateTypedDataTableColumn("WhitelistVersion", @"WhitelistVersion", typeof(System.Int64), this.Columns);
			_columnCancellationReference = GeneralUtils.CreateTypedDataTableColumn("CancellationReference", @"CancellationReference", typeof(System.Int64), this.Columns);
			_columnCancellationReferenceGuid = GeneralUtils.CreateTypedDataTableColumn("CancellationReferenceGuid", @"CancellationReferenceGuid", typeof(System.String), this.Columns);
			_columnLastUser = GeneralUtils.CreateTypedDataTableColumn("LastUser", @"LastUser", typeof(System.String), this.Columns);
			_columnLastModified = GeneralUtils.CreateTypedDataTableColumn("LastModified", @"LastModified", typeof(System.DateTime), this.Columns);
			_columnTransactionCounter = GeneralUtils.CreateTypedDataTableColumn("TransactionCounter", @"TransactionCounter", typeof(System.Decimal), this.Columns);
			_columnWhitelistVersionCreated = GeneralUtils.CreateTypedDataTableColumn("WhitelistVersionCreated", @"WhitelistVersionCreated", typeof(System.Int64), this.Columns);
			_columnProperties = GeneralUtils.CreateTypedDataTableColumn("Properties", @"Properties", typeof(VarioSL.Entities.Enumerations.TransactionJournalProperties), this.Columns);
			_columnSaleID = GeneralUtils.CreateTypedDataTableColumn("SaleID", @"SaleID", typeof(System.Int64), this.Columns);
			_columnTripTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TripTicketInternalNumber", @"TripTicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnShiftBegin = GeneralUtils.CreateTypedDataTableColumn("ShiftBegin", @"ShiftBegin", typeof(System.DateTime), this.Columns);
			_columnCompletionState = GeneralUtils.CreateTypedDataTableColumn("CompletionState", @"CompletionState", typeof(System.String), this.Columns);
			_columnTransportState = GeneralUtils.CreateTypedDataTableColumn("TransportState", @"TransportState", typeof(System.String), this.Columns);
			_columnProductID2 = GeneralUtils.CreateTypedDataTableColumn("ProductID2", @"ProductID2", typeof(System.Int64), this.Columns);
			_columnPurseBalance2 = GeneralUtils.CreateTypedDataTableColumn("PurseBalance2", @"PurseBalance2", typeof(System.Int32), this.Columns);
			_columnPurseCredit2 = GeneralUtils.CreateTypedDataTableColumn("PurseCredit2", @"PurseCredit2", typeof(System.Int32), this.Columns);
			_columnCreditCardAuthorizationID = GeneralUtils.CreateTypedDataTableColumn("CreditCardAuthorizationID", @"CreditCardAuthorizationID", typeof(System.Int64), this.Columns);
			_columnTripCounter = GeneralUtils.CreateTypedDataTableColumn("TripCounter", @"TripCounter", typeof(System.Int32), this.Columns);
			_columnInsertTime = GeneralUtils.CreateTypedDataTableColumn("InsertTime", @"InsertTime", typeof(System.DateTime), this.Columns);
			_columnIsVoiceEnabled = GeneralUtils.CreateTypedDataTableColumn("IsVoiceEnabled", @"IsVoiceEnabled", typeof(System.Int16), this.Columns);
			_columnIsTraining = GeneralUtils.CreateTypedDataTableColumn("IsTraining", @"IsTraining", typeof(System.Int16), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnTransactionJournalID = this.Columns["TransactionJournalID"];
			_columnTransactionID = this.Columns["TransactionID"];
			_columnDeviceTime = this.Columns["DeviceTime"];
			_columnBoardingTime = this.Columns["BoardingTime"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnClientID = this.Columns["ClientID"];
			_columnLine = this.Columns["Line"];
			_columnCourseNumber = this.Columns["CourseNumber"];
			_columnRouteNumber = this.Columns["RouteNumber"];
			_columnRouteCode = this.Columns["RouteCode"];
			_columnDirection = this.Columns["Direction"];
			_columnZone = this.Columns["Zone"];
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnVehicleNumber = this.Columns["VehicleNumber"];
			_columnMountingPlateNumber = this.Columns["MountingPlateNumber"];
			_columnDebtorNumber = this.Columns["DebtorNumber"];
			_columnGeoLocationLongitude = this.Columns["GeoLocationLongitude"];
			_columnGeoLocationLatitude = this.Columns["GeoLocationLatitude"];
			_columnStopNumber = this.Columns["StopNumber"];
			_columnTransitAccountID = this.Columns["TransitAccountID"];
			_columnPrintedNumber = this.Columns["PrintedNumber"];
			_columnSerialNumber = this.Columns["SerialNumber"];
			_columnFareMediaID = this.Columns["FareMediaID"];
			_columnFareMediaType = this.Columns["FareMediaType"];
			_columnProductID = this.Columns["ProductID"];
			_columnTicketID = this.Columns["TicketID"];
			_columnFareAmount = this.Columns["FareAmount"];
			_columnCustomerGroup = this.Columns["CustomerGroup"];
			_columnTransactionType = this.Columns["TransactionType"];
			_columnTransactionNumber = this.Columns["TransactionNumber"];
			_columnResultType = this.Columns["ResultType"];
			_columnFillLevel = this.Columns["FillLevel"];
			_columnPurseBalance = this.Columns["PurseBalance"];
			_columnPurseCredit = this.Columns["PurseCredit"];
			_columnGroupSize = this.Columns["GroupSize"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnDuration = this.Columns["Duration"];
			_columnTariffDate = this.Columns["TariffDate"];
			_columnTariffVersion = this.Columns["TariffVersion"];
			_columnTicketInternalNumber = this.Columns["TicketInternalNumber"];
			_columnWhitelistVersion = this.Columns["WhitelistVersion"];
			_columnCancellationReference = this.Columns["CancellationReference"];
			_columnCancellationReferenceGuid = this.Columns["CancellationReferenceGuid"];
			_columnLastUser = this.Columns["LastUser"];
			_columnLastModified = this.Columns["LastModified"];
			_columnTransactionCounter = this.Columns["TransactionCounter"];
			_columnWhitelistVersionCreated = this.Columns["WhitelistVersionCreated"];
			_columnProperties = this.Columns["Properties"];
			_columnSaleID = this.Columns["SaleID"];
			_columnTripTicketInternalNumber = this.Columns["TripTicketInternalNumber"];
			_columnTicketName = this.Columns["TicketName"];
			_columnShiftBegin = this.Columns["ShiftBegin"];
			_columnCompletionState = this.Columns["CompletionState"];
			_columnTransportState = this.Columns["TransportState"];
			_columnProductID2 = this.Columns["ProductID2"];
			_columnPurseBalance2 = this.Columns["PurseBalance2"];
			_columnPurseCredit2 = this.Columns["PurseCredit2"];
			_columnCreditCardAuthorizationID = this.Columns["CreditCardAuthorizationID"];
			_columnTripCounter = this.Columns["TripCounter"];
			_columnInsertTime = this.Columns["InsertTime"];
			_columnIsVoiceEnabled = this.Columns["IsVoiceEnabled"];
			_columnIsTraining = this.Columns["IsTraining"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			TransactionJournalDetailTypedView cloneToReturn = ((TransactionJournalDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new TransactionJournalDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return TransactionJournalDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return TransactionJournalDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalID'</summary>
		internal DataColumn TransactionJournalIDColumn 
		{
			get { return _columnTransactionJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionID'</summary>
		internal DataColumn TransactionIDColumn 
		{
			get { return _columnTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceTime'</summary>
		internal DataColumn DeviceTimeColumn 
		{
			get { return _columnDeviceTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BoardingTime'</summary>
		internal DataColumn BoardingTimeColumn 
		{
			get { return _columnBoardingTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Line'</summary>
		internal DataColumn LineColumn 
		{
			get { return _columnLine; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CourseNumber'</summary>
		internal DataColumn CourseNumberColumn 
		{
			get { return _columnCourseNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RouteNumber'</summary>
		internal DataColumn RouteNumberColumn 
		{
			get { return _columnRouteNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RouteCode'</summary>
		internal DataColumn RouteCodeColumn 
		{
			get { return _columnRouteCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Direction'</summary>
		internal DataColumn DirectionColumn 
		{
			get { return _columnDirection; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Zone'</summary>
		internal DataColumn ZoneColumn 
		{
			get { return _columnZone; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'VehicleNumber'</summary>
		internal DataColumn VehicleNumberColumn 
		{
			get { return _columnVehicleNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MountingPlateNumber'</summary>
		internal DataColumn MountingPlateNumberColumn 
		{
			get { return _columnMountingPlateNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebtorNumber'</summary>
		internal DataColumn DebtorNumberColumn 
		{
			get { return _columnDebtorNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GeoLocationLongitude'</summary>
		internal DataColumn GeoLocationLongitudeColumn 
		{
			get { return _columnGeoLocationLongitude; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GeoLocationLatitude'</summary>
		internal DataColumn GeoLocationLatitudeColumn 
		{
			get { return _columnGeoLocationLatitude; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopNumber'</summary>
		internal DataColumn StopNumberColumn 
		{
			get { return _columnStopNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransitAccountID'</summary>
		internal DataColumn TransitAccountIDColumn 
		{
			get { return _columnTransitAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PrintedNumber'</summary>
		internal DataColumn PrintedNumberColumn 
		{
			get { return _columnPrintedNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SerialNumber'</summary>
		internal DataColumn SerialNumberColumn 
		{
			get { return _columnSerialNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaID'</summary>
		internal DataColumn FareMediaIDColumn 
		{
			get { return _columnFareMediaID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaType'</summary>
		internal DataColumn FareMediaTypeColumn 
		{
			get { return _columnFareMediaType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareAmount'</summary>
		internal DataColumn FareAmountColumn 
		{
			get { return _columnFareAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CustomerGroup'</summary>
		internal DataColumn CustomerGroupColumn 
		{
			get { return _columnCustomerGroup; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionType'</summary>
		internal DataColumn TransactionTypeColumn 
		{
			get { return _columnTransactionType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionNumber'</summary>
		internal DataColumn TransactionNumberColumn 
		{
			get { return _columnTransactionNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ResultType'</summary>
		internal DataColumn ResultTypeColumn 
		{
			get { return _columnResultType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FillLevel'</summary>
		internal DataColumn FillLevelColumn 
		{
			get { return _columnFillLevel; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance'</summary>
		internal DataColumn PurseBalanceColumn 
		{
			get { return _columnPurseBalance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit'</summary>
		internal DataColumn PurseCreditColumn 
		{
			get { return _columnPurseCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GroupSize'</summary>
		internal DataColumn GroupSizeColumn 
		{
			get { return _columnGroupSize; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Duration'</summary>
		internal DataColumn DurationColumn 
		{
			get { return _columnDuration; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffDate'</summary>
		internal DataColumn TariffDateColumn 
		{
			get { return _columnTariffDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffVersion'</summary>
		internal DataColumn TariffVersionColumn 
		{
			get { return _columnTariffVersion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketInternalNumber'</summary>
		internal DataColumn TicketInternalNumberColumn 
		{
			get { return _columnTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'WhitelistVersion'</summary>
		internal DataColumn WhitelistVersionColumn 
		{
			get { return _columnWhitelistVersion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReference'</summary>
		internal DataColumn CancellationReferenceColumn 
		{
			get { return _columnCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReferenceGuid'</summary>
		internal DataColumn CancellationReferenceGuidColumn 
		{
			get { return _columnCancellationReferenceGuid; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastUser'</summary>
		internal DataColumn LastUserColumn 
		{
			get { return _columnLastUser; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastModified'</summary>
		internal DataColumn LastModifiedColumn 
		{
			get { return _columnLastModified; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionCounter'</summary>
		internal DataColumn TransactionCounterColumn 
		{
			get { return _columnTransactionCounter; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'WhitelistVersionCreated'</summary>
		internal DataColumn WhitelistVersionCreatedColumn 
		{
			get { return _columnWhitelistVersionCreated; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Properties'</summary>
		internal DataColumn PropertiesColumn 
		{
			get { return _columnProperties; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleID'</summary>
		internal DataColumn SaleIDColumn 
		{
			get { return _columnSaleID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripTicketInternalNumber'</summary>
		internal DataColumn TripTicketInternalNumberColumn 
		{
			get { return _columnTripTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ShiftBegin'</summary>
		internal DataColumn ShiftBeginColumn 
		{
			get { return _columnShiftBegin; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CompletionState'</summary>
		internal DataColumn CompletionStateColumn 
		{
			get { return _columnCompletionState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransportState'</summary>
		internal DataColumn TransportStateColumn 
		{
			get { return _columnTransportState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID2'</summary>
		internal DataColumn ProductID2Column 
		{
			get { return _columnProductID2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance2'</summary>
		internal DataColumn PurseBalance2Column 
		{
			get { return _columnPurseBalance2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit2'</summary>
		internal DataColumn PurseCredit2Column 
		{
			get { return _columnPurseCredit2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreditCardAuthorizationID'</summary>
		internal DataColumn CreditCardAuthorizationIDColumn 
		{
			get { return _columnCreditCardAuthorizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripCounter'</summary>
		internal DataColumn TripCounterColumn 
		{
			get { return _columnTripCounter; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InsertTime'</summary>
		internal DataColumn InsertTimeColumn 
		{
			get { return _columnInsertTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsVoiceEnabled'</summary>
		internal DataColumn IsVoiceEnabledColumn 
		{
			get { return _columnIsVoiceEnabled; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsTraining'</summary>
		internal DataColumn IsTrainingColumn 
		{
			get { return _columnIsTraining; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable TransactionJournalDetail</summary>
	public partial class TransactionJournalDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private TransactionJournalDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal TransactionJournalDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((TransactionJournalDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSACTIONJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransactionJournalID
		{
			get { return IsTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalIDColumn]; }
			set { this[_parent.TransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalID is NULL, false otherwise.</summary>
		public bool IsTransactionJournalIDNull() 
		{
			return IsNull(_parent.TransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalIDNull() 
		{
			this[_parent.TransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String TransactionID
		{
			get { return IsTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TransactionIDColumn]; }
			set { this[_parent.TransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionID is NULL, false otherwise.</summary>
		public bool IsTransactionIDNull() 
		{
			return IsNull(_parent.TransactionIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionIDNull() 
		{
			this[_parent.TransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."DEVICETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime DeviceTime
		{
			get { return IsDeviceTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.DeviceTimeColumn]; }
			set { this[_parent.DeviceTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceTime is NULL, false otherwise.</summary>
		public bool IsDeviceTimeNull() 
		{
			return IsNull(_parent.DeviceTimeColumn);
		}

		/// <summary>Sets the TypedView field DeviceTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceTimeNull() 
		{
			this[_parent.DeviceTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BoardingTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."BOARDINGTIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime BoardingTime
		{
			get { return IsBoardingTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.BoardingTimeColumn]; }
			set { this[_parent.BoardingTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BoardingTime is NULL, false otherwise.</summary>
		public bool IsBoardingTimeNull() 
		{
			return IsNull(_parent.BoardingTimeColumn);
		}

		/// <summary>Sets the TypedView field BoardingTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBoardingTimeNull() 
		{
			this[_parent.BoardingTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Line</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."LINE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String Line
		{
			get { return IsLineNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineColumn]; }
			set { this[_parent.LineColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Line is NULL, false otherwise.</summary>
		public bool IsLineNull() 
		{
			return IsNull(_parent.LineColumn);
		}

		/// <summary>Sets the TypedView field Line to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNull() 
		{
			this[_parent.LineColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CourseNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."COURSENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CourseNumber
		{
			get { return IsCourseNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CourseNumberColumn]; }
			set { this[_parent.CourseNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CourseNumber is NULL, false otherwise.</summary>
		public bool IsCourseNumberNull() 
		{
			return IsNull(_parent.CourseNumberColumn);
		}

		/// <summary>Sets the TypedView field CourseNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCourseNumberNull() 
		{
			this[_parent.CourseNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RouteNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."ROUTENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 RouteNumber
		{
			get { return IsRouteNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.RouteNumberColumn]; }
			set { this[_parent.RouteNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RouteNumber is NULL, false otherwise.</summary>
		public bool IsRouteNumberNull() 
		{
			return IsNull(_parent.RouteNumberColumn);
		}

		/// <summary>Sets the TypedView field RouteNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRouteNumberNull() 
		{
			this[_parent.RouteNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RouteCode</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."ROUTECODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 60</remarks>
		public System.String RouteCode
		{
			get { return IsRouteCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.RouteCodeColumn]; }
			set { this[_parent.RouteCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RouteCode is NULL, false otherwise.</summary>
		public bool IsRouteCodeNull() 
		{
			return IsNull(_parent.RouteCodeColumn);
		}

		/// <summary>Sets the TypedView field RouteCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRouteCodeNull() 
		{
			this[_parent.RouteCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Direction</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."DIRECTION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Direction
		{
			get { return IsDirectionNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DirectionColumn]; }
			set { this[_parent.DirectionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Direction is NULL, false otherwise.</summary>
		public bool IsDirectionNull() 
		{
			return IsNull(_parent.DirectionColumn);
		}

		/// <summary>Sets the TypedView field Direction to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDirectionNull() 
		{
			this[_parent.DirectionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Zone</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."ZONE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Zone
		{
			get { return IsZoneNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.ZoneColumn]; }
			set { this[_parent.ZoneColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Zone is NULL, false otherwise.</summary>
		public bool IsZoneNull() 
		{
			return IsNull(_parent.ZoneColumn);
		}

		/// <summary>Sets the TypedView field Zone to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetZoneNull() 
		{
			this[_parent.ZoneColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field VehicleNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."VEHICLENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 VehicleNumber
		{
			get { return IsVehicleNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.VehicleNumberColumn]; }
			set { this[_parent.VehicleNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field VehicleNumber is NULL, false otherwise.</summary>
		public bool IsVehicleNumberNull() 
		{
			return IsNull(_parent.VehicleNumberColumn);
		}

		/// <summary>Sets the TypedView field VehicleNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetVehicleNumberNull() 
		{
			this[_parent.VehicleNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MountingPlateNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."MOUNTINGPLATENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MountingPlateNumber
		{
			get { return IsMountingPlateNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MountingPlateNumberColumn]; }
			set { this[_parent.MountingPlateNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MountingPlateNumber is NULL, false otherwise.</summary>
		public bool IsMountingPlateNumberNull() 
		{
			return IsNull(_parent.MountingPlateNumberColumn);
		}

		/// <summary>Sets the TypedView field MountingPlateNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMountingPlateNumberNull() 
		{
			this[_parent.MountingPlateNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebtorNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."DEBTORNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DebtorNumber
		{
			get { return IsDebtorNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DebtorNumberColumn]; }
			set { this[_parent.DebtorNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebtorNumber is NULL, false otherwise.</summary>
		public bool IsDebtorNumberNull() 
		{
			return IsNull(_parent.DebtorNumberColumn);
		}

		/// <summary>Sets the TypedView field DebtorNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebtorNumberNull() 
		{
			this[_parent.DebtorNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GeoLocationLongitude</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."GEOLOCATIONLONGITUDE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GeoLocationLongitude
		{
			get { return IsGeoLocationLongitudeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GeoLocationLongitudeColumn]; }
			set { this[_parent.GeoLocationLongitudeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GeoLocationLongitude is NULL, false otherwise.</summary>
		public bool IsGeoLocationLongitudeNull() 
		{
			return IsNull(_parent.GeoLocationLongitudeColumn);
		}

		/// <summary>Sets the TypedView field GeoLocationLongitude to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGeoLocationLongitudeNull() 
		{
			this[_parent.GeoLocationLongitudeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GeoLocationLatitude</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."GEOLOCATIONLATITUDE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GeoLocationLatitude
		{
			get { return IsGeoLocationLatitudeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GeoLocationLatitudeColumn]; }
			set { this[_parent.GeoLocationLatitudeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GeoLocationLatitude is NULL, false otherwise.</summary>
		public bool IsGeoLocationLatitudeNull() 
		{
			return IsNull(_parent.GeoLocationLatitudeColumn);
		}

		/// <summary>Sets the TypedView field GeoLocationLatitude to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGeoLocationLatitudeNull() 
		{
			this[_parent.GeoLocationLatitudeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."STOPNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 StopNumber
		{
			get { return IsStopNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.StopNumberColumn]; }
			set { this[_parent.StopNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopNumber is NULL, false otherwise.</summary>
		public bool IsStopNumberNull() 
		{
			return IsNull(_parent.StopNumberColumn);
		}

		/// <summary>Sets the TypedView field StopNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopNumberNull() 
		{
			this[_parent.StopNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransitAccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSITACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransitAccountID
		{
			get { return IsTransitAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransitAccountIDColumn]; }
			set { this[_parent.TransitAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransitAccountID is NULL, false otherwise.</summary>
		public bool IsTransitAccountIDNull() 
		{
			return IsNull(_parent.TransitAccountIDColumn);
		}

		/// <summary>Sets the TypedView field TransitAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransitAccountIDNull() 
		{
			this[_parent.TransitAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PrintedNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String PrintedNumber
		{
			get { return IsPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PrintedNumberColumn]; }
			set { this[_parent.PrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PrintedNumber is NULL, false otherwise.</summary>
		public bool IsPrintedNumberNull() 
		{
			return IsNull(_parent.PrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field PrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPrintedNumberNull() 
		{
			this[_parent.PrintedNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SerialNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."SERIALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String SerialNumber
		{
			get { return IsSerialNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SerialNumberColumn]; }
			set { this[_parent.SerialNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SerialNumber is NULL, false otherwise.</summary>
		public bool IsSerialNumberNull() 
		{
			return IsNull(_parent.SerialNumberColumn);
		}

		/// <summary>Sets the TypedView field SerialNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSerialNumberNull() 
		{
			this[_parent.SerialNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."FAREMEDIAID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String FareMediaID
		{
			get { return IsFareMediaIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaIDColumn]; }
			set { this[_parent.FareMediaIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaID is NULL, false otherwise.</summary>
		public bool IsFareMediaIDNull() 
		{
			return IsNull(_parent.FareMediaIDColumn);
		}

		/// <summary>Sets the TypedView field FareMediaID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaIDNull() 
		{
			this[_parent.FareMediaIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."FAREMEDIATYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String FareMediaType
		{
			get { return IsFareMediaTypeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaTypeColumn]; }
			set { this[_parent.FareMediaTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaType is NULL, false otherwise.</summary>
		public bool IsFareMediaTypeNull() 
		{
			return IsNull(_parent.FareMediaTypeColumn);
		}

		/// <summary>Sets the TypedView field FareMediaType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaTypeNull() 
		{
			this[_parent.FareMediaTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareAmount</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."FAREAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FareAmount
		{
			get { return IsFareAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FareAmountColumn]; }
			set { this[_parent.FareAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareAmount is NULL, false otherwise.</summary>
		public bool IsFareAmountNull() 
		{
			return IsNull(_parent.FareAmountColumn);
		}

		/// <summary>Sets the TypedView field FareAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareAmountNull() 
		{
			this[_parent.FareAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CustomerGroup</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."CUSTOMERGROUP"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String CustomerGroup
		{
			get { return IsCustomerGroupNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CustomerGroupColumn]; }
			set { this[_parent.CustomerGroupColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CustomerGroup is NULL, false otherwise.</summary>
		public bool IsCustomerGroupNull() 
		{
			return IsNull(_parent.CustomerGroupColumn);
		}

		/// <summary>Sets the TypedView field CustomerGroup to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCustomerGroupNull() 
		{
			this[_parent.CustomerGroupColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSACTIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionType TransactionType
		{
			get { return IsTransactionTypeNull() ? (VarioSL.Entities.Enumerations.TransactionType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionType)) : (VarioSL.Entities.Enumerations.TransactionType)this[_parent.TransactionTypeColumn]; }
			set { this[_parent.TransactionTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionType is NULL, false otherwise.</summary>
		public bool IsTransactionTypeNull() 
		{
			return IsNull(_parent.TransactionTypeColumn);
		}

		/// <summary>Sets the TypedView field TransactionType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionTypeNull() 
		{
			this[_parent.TransactionTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSACTIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TransactionNumber
		{
			get { return IsTransactionNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TransactionNumberColumn]; }
			set { this[_parent.TransactionNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionNumber is NULL, false otherwise.</summary>
		public bool IsTransactionNumberNull() 
		{
			return IsNull(_parent.TransactionNumberColumn);
		}

		/// <summary>Sets the TypedView field TransactionNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionNumberNull() 
		{
			this[_parent.TransactionNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ResultType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."RESULTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.FarePaymentResultType ResultType
		{
			get { return IsResultTypeNull() ? (VarioSL.Entities.Enumerations.FarePaymentResultType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.FarePaymentResultType)) : (VarioSL.Entities.Enumerations.FarePaymentResultType)this[_parent.ResultTypeColumn]; }
			set { this[_parent.ResultTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ResultType is NULL, false otherwise.</summary>
		public bool IsResultTypeNull() 
		{
			return IsNull(_parent.ResultTypeColumn);
		}

		/// <summary>Sets the TypedView field ResultType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetResultTypeNull() 
		{
			this[_parent.ResultTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FillLevel</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."FILLLEVEL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FillLevel
		{
			get { return IsFillLevelNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FillLevelColumn]; }
			set { this[_parent.FillLevelColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FillLevel is NULL, false otherwise.</summary>
		public bool IsFillLevelNull() 
		{
			return IsNull(_parent.FillLevelColumn);
		}

		/// <summary>Sets the TypedView field FillLevel to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFillLevelNull() 
		{
			this[_parent.FillLevelColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PURSEBALANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance
		{
			get { return IsPurseBalanceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalanceColumn]; }
			set { this[_parent.PurseBalanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance is NULL, false otherwise.</summary>
		public bool IsPurseBalanceNull() 
		{
			return IsNull(_parent.PurseBalanceColumn);
		}

		/// <summary>Sets the TypedView field PurseBalance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalanceNull() 
		{
			this[_parent.PurseBalanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PURSECREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit
		{
			get { return IsPurseCreditNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCreditColumn]; }
			set { this[_parent.PurseCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit is NULL, false otherwise.</summary>
		public bool IsPurseCreditNull() 
		{
			return IsNull(_parent.PurseCreditColumn);
		}

		/// <summary>Sets the TypedView field PurseCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCreditNull() 
		{
			this[_parent.PurseCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GroupSize</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."GROUPSIZE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GroupSize
		{
			get { return IsGroupSizeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GroupSizeColumn]; }
			set { this[_parent.GroupSizeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GroupSize is NULL, false otherwise.</summary>
		public bool IsGroupSizeNull() 
		{
			return IsNull(_parent.GroupSizeColumn);
		}

		/// <summary>Sets the TypedView field GroupSize to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGroupSizeNull() 
		{
			this[_parent.GroupSizeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Duration</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."DURATION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Duration
		{
			get { return IsDurationNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.DurationColumn]; }
			set { this[_parent.DurationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Duration is NULL, false otherwise.</summary>
		public bool IsDurationNull() 
		{
			return IsNull(_parent.DurationColumn);
		}

		/// <summary>Sets the TypedView field Duration to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDurationNull() 
		{
			this[_parent.DurationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TARIFFDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime TariffDate
		{
			get { return IsTariffDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TariffDateColumn]; }
			set { this[_parent.TariffDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffDate is NULL, false otherwise.</summary>
		public bool IsTariffDateNull() 
		{
			return IsNull(_parent.TariffDateColumn);
		}

		/// <summary>Sets the TypedView field TariffDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffDateNull() 
		{
			this[_parent.TariffDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffVersion</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TARIFFVERSION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TariffVersion
		{
			get { return IsTariffVersionNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TariffVersionColumn]; }
			set { this[_parent.TariffVersionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffVersion is NULL, false otherwise.</summary>
		public bool IsTariffVersionNull() 
		{
			return IsNull(_parent.TariffVersionColumn);
		}

		/// <summary>Sets the TypedView field TariffVersion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffVersionNull() 
		{
			this[_parent.TariffVersionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TicketInternalNumber
		{
			get { return IsTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TicketInternalNumberColumn]; }
			set { this[_parent.TicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTicketInternalNumberNull() 
		{
			return IsNull(_parent.TicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketInternalNumberNull() 
		{
			this[_parent.TicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field WhitelistVersion</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."WHITELISTVERSION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WhitelistVersion
		{
			get { return IsWhitelistVersionNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WhitelistVersionColumn]; }
			set { this[_parent.WhitelistVersionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WhitelistVersion is NULL, false otherwise.</summary>
		public bool IsWhitelistVersionNull() 
		{
			return IsNull(_parent.WhitelistVersionColumn);
		}

		/// <summary>Sets the TypedView field WhitelistVersion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWhitelistVersionNull() 
		{
			this[_parent.WhitelistVersionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."CANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CancellationReference
		{
			get { return IsCancellationReferenceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CancellationReferenceColumn]; }
			set { this[_parent.CancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReference is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceNull() 
		{
			return IsNull(_parent.CancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field CancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceNull() 
		{
			this[_parent.CancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReferenceGuid</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."CANCELLATIONREFERENCEGUID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CancellationReferenceGuid
		{
			get { return IsCancellationReferenceGuidNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CancellationReferenceGuidColumn]; }
			set { this[_parent.CancellationReferenceGuidColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReferenceGuid is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceGuidNull() 
		{
			return IsNull(_parent.CancellationReferenceGuidColumn);
		}

		/// <summary>Sets the TypedView field CancellationReferenceGuid to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceGuidNull() 
		{
			this[_parent.CancellationReferenceGuidColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastUser</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."LASTUSER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastUser
		{
			get { return IsLastUserNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastUserColumn]; }
			set { this[_parent.LastUserColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastUser is NULL, false otherwise.</summary>
		public bool IsLastUserNull() 
		{
			return IsNull(_parent.LastUserColumn);
		}

		/// <summary>Sets the TypedView field LastUser to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastUserNull() 
		{
			this[_parent.LastUserColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastModified</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."LASTMODIFIED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastModified
		{
			get { return IsLastModifiedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastModifiedColumn]; }
			set { this[_parent.LastModifiedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastModified is NULL, false otherwise.</summary>
		public bool IsLastModifiedNull() 
		{
			return IsNull(_parent.LastModifiedColumn);
		}

		/// <summary>Sets the TypedView field LastModified to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastModifiedNull() 
		{
			this[_parent.LastModifiedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionCounter</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRANSACTIONCOUNTER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal TransactionCounter
		{
			get { return IsTransactionCounterNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.TransactionCounterColumn]; }
			set { this[_parent.TransactionCounterColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionCounter is NULL, false otherwise.</summary>
		public bool IsTransactionCounterNull() 
		{
			return IsNull(_parent.TransactionCounterColumn);
		}

		/// <summary>Sets the TypedView field TransactionCounter to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionCounterNull() 
		{
			this[_parent.TransactionCounterColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field WhitelistVersionCreated</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."WHITELISTVERSIONCREATED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WhitelistVersionCreated
		{
			get { return IsWhitelistVersionCreatedNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WhitelistVersionCreatedColumn]; }
			set { this[_parent.WhitelistVersionCreatedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WhitelistVersionCreated is NULL, false otherwise.</summary>
		public bool IsWhitelistVersionCreatedNull() 
		{
			return IsNull(_parent.WhitelistVersionCreatedColumn);
		}

		/// <summary>Sets the TypedView field WhitelistVersionCreated to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWhitelistVersionCreatedNull() 
		{
			this[_parent.WhitelistVersionCreatedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Properties</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PROPERTIES"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionJournalProperties Properties
		{
			get { return IsPropertiesNull() ? (VarioSL.Entities.Enumerations.TransactionJournalProperties)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionJournalProperties)) : (VarioSL.Entities.Enumerations.TransactionJournalProperties)this[_parent.PropertiesColumn]; }
			set { this[_parent.PropertiesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Properties is NULL, false otherwise.</summary>
		public bool IsPropertiesNull() 
		{
			return IsNull(_parent.PropertiesColumn);
		}

		/// <summary>Sets the TypedView field Properties to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPropertiesNull() 
		{
			this[_parent.PropertiesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."SALEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SaleID
		{
			get { return IsSaleIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SaleIDColumn]; }
			set { this[_parent.SaleIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleID is NULL, false otherwise.</summary>
		public bool IsSaleIDNull() 
		{
			return IsNull(_parent.SaleIDColumn);
		}

		/// <summary>Sets the TypedView field SaleID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleIDNull() 
		{
			this[_parent.SaleIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripTicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRIPTICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TripTicketInternalNumber
		{
			get { return IsTripTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TripTicketInternalNumberColumn]; }
			set { this[_parent.TripTicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripTicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTripTicketInternalNumberNull() 
		{
			return IsNull(_parent.TripTicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TripTicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripTicketInternalNumberNull() 
		{
			this[_parent.TripTicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ShiftBegin</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."SHIFTBEGIN"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ShiftBegin
		{
			get { return IsShiftBeginNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ShiftBeginColumn]; }
			set { this[_parent.ShiftBeginColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ShiftBegin is NULL, false otherwise.</summary>
		public bool IsShiftBeginNull() 
		{
			return IsNull(_parent.ShiftBeginColumn);
		}

		/// <summary>Sets the TypedView field ShiftBegin to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetShiftBeginNull() 
		{
			this[_parent.ShiftBeginColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CompletionState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."COMPLETIONSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String CompletionState
		{
			get { return IsCompletionStateNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CompletionStateColumn]; }
			set { this[_parent.CompletionStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CompletionState is NULL, false otherwise.</summary>
		public bool IsCompletionStateNull() 
		{
			return IsNull(_parent.CompletionStateColumn);
		}

		/// <summary>Sets the TypedView field CompletionState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCompletionStateNull() 
		{
			this[_parent.CompletionStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransportState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."RESPONSESTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String TransportState
		{
			get { return IsTransportStateNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TransportStateColumn]; }
			set { this[_parent.TransportStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransportState is NULL, false otherwise.</summary>
		public bool IsTransportStateNull() 
		{
			return IsNull(_parent.TransportStateColumn);
		}

		/// <summary>Sets the TypedView field TransportState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransportStateNull() 
		{
			this[_parent.TransportStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PRODUCTID2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID2
		{
			get { return IsProductID2Null() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductID2Column]; }
			set { this[_parent.ProductID2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID2 is NULL, false otherwise.</summary>
		public bool IsProductID2Null() 
		{
			return IsNull(_parent.ProductID2Column);
		}

		/// <summary>Sets the TypedView field ProductID2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductID2Null() 
		{
			this[_parent.ProductID2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PURSEBALANCE2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance2
		{
			get { return IsPurseBalance2Null() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalance2Column]; }
			set { this[_parent.PurseBalance2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance2 is NULL, false otherwise.</summary>
		public bool IsPurseBalance2Null() 
		{
			return IsNull(_parent.PurseBalance2Column);
		}

		/// <summary>Sets the TypedView field PurseBalance2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalance2Null() 
		{
			this[_parent.PurseBalance2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."PURSECREDIT2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit2
		{
			get { return IsPurseCredit2Null() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCredit2Column]; }
			set { this[_parent.PurseCredit2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit2 is NULL, false otherwise.</summary>
		public bool IsPurseCredit2Null() 
		{
			return IsNull(_parent.PurseCredit2Column);
		}

		/// <summary>Sets the TypedView field PurseCredit2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCredit2Null() 
		{
			this[_parent.PurseCredit2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreditCardAuthorizationID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."CREDITCARDAUTHORIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CreditCardAuthorizationID
		{
			get { return IsCreditCardAuthorizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CreditCardAuthorizationIDColumn]; }
			set { this[_parent.CreditCardAuthorizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreditCardAuthorizationID is NULL, false otherwise.</summary>
		public bool IsCreditCardAuthorizationIDNull() 
		{
			return IsNull(_parent.CreditCardAuthorizationIDColumn);
		}

		/// <summary>Sets the TypedView field CreditCardAuthorizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreditCardAuthorizationIDNull() 
		{
			this[_parent.CreditCardAuthorizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripCounter</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."TRIPCOUNTER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TripCounter
		{
			get { return IsTripCounterNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TripCounterColumn]; }
			set { this[_parent.TripCounterColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripCounter is NULL, false otherwise.</summary>
		public bool IsTripCounterNull() 
		{
			return IsNull(_parent.TripCounterColumn);
		}

		/// <summary>Sets the TypedView field TripCounter to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripCounterNull() 
		{
			this[_parent.TripCounterColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InsertTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."INSERTTIME"<br/>
		/// View field characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0</remarks>
		public System.DateTime InsertTime
		{
			get { return IsInsertTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.InsertTimeColumn]; }
			set { this[_parent.InsertTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InsertTime is NULL, false otherwise.</summary>
		public bool IsInsertTimeNull() 
		{
			return IsNull(_parent.InsertTimeColumn);
		}

		/// <summary>Sets the TypedView field InsertTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInsertTimeNull() 
		{
			this[_parent.InsertTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsVoiceEnabled</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."ISVOICEENABLED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsVoiceEnabled
		{
			get { return IsIsVoiceEnabledNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsVoiceEnabledColumn]; }
			set { this[_parent.IsVoiceEnabledColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsVoiceEnabled is NULL, false otherwise.</summary>
		public bool IsIsVoiceEnabledNull() 
		{
			return IsNull(_parent.IsVoiceEnabledColumn);
		}

		/// <summary>Sets the TypedView field IsVoiceEnabled to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsVoiceEnabledNull() 
		{
			this[_parent.IsVoiceEnabledColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsTraining</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_TRANSACTIONJOURNAL"."ISTRAINING"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsTraining
		{
			get { return IsIsTrainingNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsTrainingColumn]; }
			set { this[_parent.IsTrainingColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsTraining is NULL, false otherwise.</summary>
		public bool IsIsTrainingNull() 
		{
			return IsNull(_parent.IsTrainingColumn);
		}

		/// <summary>Sets the TypedView field IsTraining to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsTrainingNull() 
		{
			this[_parent.IsTrainingColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
