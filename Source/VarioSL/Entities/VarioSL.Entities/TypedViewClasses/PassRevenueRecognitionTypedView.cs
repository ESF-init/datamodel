﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'PassRevenueRecognition'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class PassRevenueRecognitionTypedView : TypedViewBase<PassRevenueRecognitionRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnPassTicketID;
		private DataColumn _columnPassTransitAccountID;
		private DataColumn _columnPassRevenueSettlementID;
		private DataColumn _columnTapRevenueSettlementID;
		private DataColumn _columnTapRevenueRecognitionID;
		private DataColumn _columnTapRevenueRecognitionAmount;
		private DataColumn _columnTapTransactionJournalID;
		private DataColumn _columnTapTransactionType;
		private DataColumn _columnTapBoardingGuid;
		private DataColumn _columnTapPurseCredit;
		private DataColumn _columnTapOriginalSingleFare;
		private DataColumn _columnTapBaseFare;
		private DataColumn _columnTapOperatorID;
		private DataColumn _columnTapTicketNumber;
		private DataColumn _columnTapAppliedPassTicketID;
		private DataColumn _columnTapDeviceTime;
		private DataColumn _columnTapTicketID;
		private DataColumn _columnTapCancellationReferenceGuid;
		private DataColumn _columnTapCancellationReference;
		private DataColumn _columnPassAmount;
		private DataColumn _columnPassValidFrom;
		private DataColumn _columnPassValidTo;
		private DataColumn _columnPassTicketInternalNumber;
		private DataColumn _columnPassCloseoutPeriodID;
		private DataColumn _columnTapCloseoutPeriodID;
		private DataColumn _columnPassRevenueRecognitionID;
		private DataColumn _columnPassTransactionJournalID;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 27;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static PassRevenueRecognitionTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PassRevenueRecognitionTypedView():base("PassRevenueRecognition")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PassRevenueRecognitionTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.PassRevenueRecognitionTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.PassRevenueRecognitionTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new PassRevenueRecognitionRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassTicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassTransitAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassRevenueSettlementID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapRevenueSettlementID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapRevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapRevenueRecognitionAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapTransactionJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapTransactionType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapBoardingGuid", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapPurseCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapOriginalSingleFare", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapBaseFare", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapOperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapTicketNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapAppliedPassTicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapDeviceTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapTicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapCancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapCancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassCloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TapCloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassRevenueRecognitionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PassTransactionJournalID", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "PassRevenueRecognition";
			_columnPassTicketID = GeneralUtils.CreateTypedDataTableColumn("PassTicketID", @"PassTicketID", typeof(System.Int64), this.Columns);
			_columnPassTransitAccountID = GeneralUtils.CreateTypedDataTableColumn("PassTransitAccountID", @"PassTransitAccountID", typeof(System.Int64), this.Columns);
			_columnPassRevenueSettlementID = GeneralUtils.CreateTypedDataTableColumn("PassRevenueSettlementID", @"PassRevenueSettlementID", typeof(System.Int64), this.Columns);
			_columnTapRevenueSettlementID = GeneralUtils.CreateTypedDataTableColumn("TapRevenueSettlementID", @"TapRevenueSettlementID", typeof(System.Int64), this.Columns);
			_columnTapRevenueRecognitionID = GeneralUtils.CreateTypedDataTableColumn("TapRevenueRecognitionID", @"TapRevenueRecognitionID", typeof(System.Int64), this.Columns);
			_columnTapRevenueRecognitionAmount = GeneralUtils.CreateTypedDataTableColumn("TapRevenueRecognitionAmount", @"TapRevenueRecognitionAmount", typeof(System.Int64), this.Columns);
			_columnTapTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("TapTransactionJournalID", @"TapTransactionJournalID", typeof(System.Int64), this.Columns);
			_columnTapTransactionType = GeneralUtils.CreateTypedDataTableColumn("TapTransactionType", @"TapTransactionType", typeof(System.Int32), this.Columns);
			_columnTapBoardingGuid = GeneralUtils.CreateTypedDataTableColumn("TapBoardingGuid", @"TapBoardingGuid", typeof(System.String), this.Columns);
			_columnTapPurseCredit = GeneralUtils.CreateTypedDataTableColumn("TapPurseCredit", @"TapPurseCredit", typeof(System.Int32), this.Columns);
			_columnTapOriginalSingleFare = GeneralUtils.CreateTypedDataTableColumn("TapOriginalSingleFare", @"TapOriginalSingleFare", typeof(System.Int32), this.Columns);
			_columnTapBaseFare = GeneralUtils.CreateTypedDataTableColumn("TapBaseFare", @"TapBaseFare", typeof(System.Int64), this.Columns);
			_columnTapOperatorID = GeneralUtils.CreateTypedDataTableColumn("TapOperatorID", @"TapOperatorID", typeof(System.Int32), this.Columns);
			_columnTapTicketNumber = GeneralUtils.CreateTypedDataTableColumn("TapTicketNumber", @"TapTicketNumber", typeof(System.Int32), this.Columns);
			_columnTapAppliedPassTicketID = GeneralUtils.CreateTypedDataTableColumn("TapAppliedPassTicketID", @"TapAppliedPassTicketID", typeof(System.Int64), this.Columns);
			_columnTapDeviceTime = GeneralUtils.CreateTypedDataTableColumn("TapDeviceTime", @"TapDeviceTime", typeof(System.DateTime), this.Columns);
			_columnTapTicketID = GeneralUtils.CreateTypedDataTableColumn("TapTicketID", @"TapTicketID", typeof(System.Int64), this.Columns);
			_columnTapCancellationReferenceGuid = GeneralUtils.CreateTypedDataTableColumn("TapCancellationReferenceGuid", @"TapCancellationReferenceGuid", typeof(System.String), this.Columns);
			_columnTapCancellationReference = GeneralUtils.CreateTypedDataTableColumn("TapCancellationReference", @"TapCancellationReference", typeof(System.Int64), this.Columns);
			_columnPassAmount = GeneralUtils.CreateTypedDataTableColumn("PassAmount", @"PassAmount", typeof(System.Int64), this.Columns);
			_columnPassValidFrom = GeneralUtils.CreateTypedDataTableColumn("PassValidFrom", @"PassValidFrom", typeof(System.DateTime), this.Columns);
			_columnPassValidTo = GeneralUtils.CreateTypedDataTableColumn("PassValidTo", @"PassValidTo", typeof(System.DateTime), this.Columns);
			_columnPassTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("PassTicketInternalNumber", @"PassTicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnPassCloseoutPeriodID = GeneralUtils.CreateTypedDataTableColumn("PassCloseoutPeriodID", @"PassCloseoutPeriodID", typeof(System.Int64), this.Columns);
			_columnTapCloseoutPeriodID = GeneralUtils.CreateTypedDataTableColumn("TapCloseoutPeriodID", @"TapCloseoutPeriodID", typeof(System.Int64), this.Columns);
			_columnPassRevenueRecognitionID = GeneralUtils.CreateTypedDataTableColumn("PassRevenueRecognitionID", @"PassRevenueRecognitionID", typeof(System.Int64), this.Columns);
			_columnPassTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("PassTransactionJournalID", @"PassTransactionJournalID", typeof(System.Int64), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnPassTicketID = this.Columns["PassTicketID"];
			_columnPassTransitAccountID = this.Columns["PassTransitAccountID"];
			_columnPassRevenueSettlementID = this.Columns["PassRevenueSettlementID"];
			_columnTapRevenueSettlementID = this.Columns["TapRevenueSettlementID"];
			_columnTapRevenueRecognitionID = this.Columns["TapRevenueRecognitionID"];
			_columnTapRevenueRecognitionAmount = this.Columns["TapRevenueRecognitionAmount"];
			_columnTapTransactionJournalID = this.Columns["TapTransactionJournalID"];
			_columnTapTransactionType = this.Columns["TapTransactionType"];
			_columnTapBoardingGuid = this.Columns["TapBoardingGuid"];
			_columnTapPurseCredit = this.Columns["TapPurseCredit"];
			_columnTapOriginalSingleFare = this.Columns["TapOriginalSingleFare"];
			_columnTapBaseFare = this.Columns["TapBaseFare"];
			_columnTapOperatorID = this.Columns["TapOperatorID"];
			_columnTapTicketNumber = this.Columns["TapTicketNumber"];
			_columnTapAppliedPassTicketID = this.Columns["TapAppliedPassTicketID"];
			_columnTapDeviceTime = this.Columns["TapDeviceTime"];
			_columnTapTicketID = this.Columns["TapTicketID"];
			_columnTapCancellationReferenceGuid = this.Columns["TapCancellationReferenceGuid"];
			_columnTapCancellationReference = this.Columns["TapCancellationReference"];
			_columnPassAmount = this.Columns["PassAmount"];
			_columnPassValidFrom = this.Columns["PassValidFrom"];
			_columnPassValidTo = this.Columns["PassValidTo"];
			_columnPassTicketInternalNumber = this.Columns["PassTicketInternalNumber"];
			_columnPassCloseoutPeriodID = this.Columns["PassCloseoutPeriodID"];
			_columnTapCloseoutPeriodID = this.Columns["TapCloseoutPeriodID"];
			_columnPassRevenueRecognitionID = this.Columns["PassRevenueRecognitionID"];
			_columnPassTransactionJournalID = this.Columns["PassTransactionJournalID"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			PassRevenueRecognitionTypedView cloneToReturn = ((PassRevenueRecognitionTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new PassRevenueRecognitionTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return PassRevenueRecognitionTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return PassRevenueRecognitionTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'PassTicketID'</summary>
		internal DataColumn PassTicketIDColumn 
		{
			get { return _columnPassTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassTransitAccountID'</summary>
		internal DataColumn PassTransitAccountIDColumn 
		{
			get { return _columnPassTransitAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassRevenueSettlementID'</summary>
		internal DataColumn PassRevenueSettlementIDColumn 
		{
			get { return _columnPassRevenueSettlementID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapRevenueSettlementID'</summary>
		internal DataColumn TapRevenueSettlementIDColumn 
		{
			get { return _columnTapRevenueSettlementID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapRevenueRecognitionID'</summary>
		internal DataColumn TapRevenueRecognitionIDColumn 
		{
			get { return _columnTapRevenueRecognitionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapRevenueRecognitionAmount'</summary>
		internal DataColumn TapRevenueRecognitionAmountColumn 
		{
			get { return _columnTapRevenueRecognitionAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapTransactionJournalID'</summary>
		internal DataColumn TapTransactionJournalIDColumn 
		{
			get { return _columnTapTransactionJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapTransactionType'</summary>
		internal DataColumn TapTransactionTypeColumn 
		{
			get { return _columnTapTransactionType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapBoardingGuid'</summary>
		internal DataColumn TapBoardingGuidColumn 
		{
			get { return _columnTapBoardingGuid; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapPurseCredit'</summary>
		internal DataColumn TapPurseCreditColumn 
		{
			get { return _columnTapPurseCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapOriginalSingleFare'</summary>
		internal DataColumn TapOriginalSingleFareColumn 
		{
			get { return _columnTapOriginalSingleFare; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapBaseFare'</summary>
		internal DataColumn TapBaseFareColumn 
		{
			get { return _columnTapBaseFare; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapOperatorID'</summary>
		internal DataColumn TapOperatorIDColumn 
		{
			get { return _columnTapOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapTicketNumber'</summary>
		internal DataColumn TapTicketNumberColumn 
		{
			get { return _columnTapTicketNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapAppliedPassTicketID'</summary>
		internal DataColumn TapAppliedPassTicketIDColumn 
		{
			get { return _columnTapAppliedPassTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapDeviceTime'</summary>
		internal DataColumn TapDeviceTimeColumn 
		{
			get { return _columnTapDeviceTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapTicketID'</summary>
		internal DataColumn TapTicketIDColumn 
		{
			get { return _columnTapTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapCancellationReferenceGuid'</summary>
		internal DataColumn TapCancellationReferenceGuidColumn 
		{
			get { return _columnTapCancellationReferenceGuid; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapCancellationReference'</summary>
		internal DataColumn TapCancellationReferenceColumn 
		{
			get { return _columnTapCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassAmount'</summary>
		internal DataColumn PassAmountColumn 
		{
			get { return _columnPassAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassValidFrom'</summary>
		internal DataColumn PassValidFromColumn 
		{
			get { return _columnPassValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassValidTo'</summary>
		internal DataColumn PassValidToColumn 
		{
			get { return _columnPassValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassTicketInternalNumber'</summary>
		internal DataColumn PassTicketInternalNumberColumn 
		{
			get { return _columnPassTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassCloseoutPeriodID'</summary>
		internal DataColumn PassCloseoutPeriodIDColumn 
		{
			get { return _columnPassCloseoutPeriodID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TapCloseoutPeriodID'</summary>
		internal DataColumn TapCloseoutPeriodIDColumn 
		{
			get { return _columnTapCloseoutPeriodID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassRevenueRecognitionID'</summary>
		internal DataColumn PassRevenueRecognitionIDColumn 
		{
			get { return _columnPassRevenueRecognitionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PassTransactionJournalID'</summary>
		internal DataColumn PassTransactionJournalIDColumn 
		{
			get { return _columnPassTransactionJournalID; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable PassRevenueRecognition</summary>
	public partial class PassRevenueRecognitionRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private PassRevenueRecognitionTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal PassRevenueRecognitionRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((PassRevenueRecognitionTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field PassTicketID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassTicketID
		{
			get { return IsPassTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassTicketIDColumn]; }
			set { this[_parent.PassTicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassTicketID is NULL, false otherwise.</summary>
		public bool IsPassTicketIDNull() 
		{
			return IsNull(_parent.PassTicketIDColumn);
		}

		/// <summary>Sets the TypedView field PassTicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassTicketIDNull() 
		{
			this[_parent.PassTicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassTransitAccountID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTRANSITACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassTransitAccountID
		{
			get { return IsPassTransitAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassTransitAccountIDColumn]; }
			set { this[_parent.PassTransitAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassTransitAccountID is NULL, false otherwise.</summary>
		public bool IsPassTransitAccountIDNull() 
		{
			return IsNull(_parent.PassTransitAccountIDColumn);
		}

		/// <summary>Sets the TypedView field PassTransitAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassTransitAccountIDNull() 
		{
			this[_parent.PassTransitAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassRevenueSettlementID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSREVENUESETTLEMENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassRevenueSettlementID
		{
			get { return IsPassRevenueSettlementIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassRevenueSettlementIDColumn]; }
			set { this[_parent.PassRevenueSettlementIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassRevenueSettlementID is NULL, false otherwise.</summary>
		public bool IsPassRevenueSettlementIDNull() 
		{
			return IsNull(_parent.PassRevenueSettlementIDColumn);
		}

		/// <summary>Sets the TypedView field PassRevenueSettlementID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassRevenueSettlementIDNull() 
		{
			this[_parent.PassRevenueSettlementIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapRevenueSettlementID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUESETTLEMENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapRevenueSettlementID
		{
			get { return IsTapRevenueSettlementIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapRevenueSettlementIDColumn]; }
			set { this[_parent.TapRevenueSettlementIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapRevenueSettlementID is NULL, false otherwise.</summary>
		public bool IsTapRevenueSettlementIDNull() 
		{
			return IsNull(_parent.TapRevenueSettlementIDColumn);
		}

		/// <summary>Sets the TypedView field TapRevenueSettlementID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapRevenueSettlementIDNull() 
		{
			this[_parent.TapRevenueSettlementIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapRevenueRecognitionID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUERECOGNITIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapRevenueRecognitionID
		{
			get { return IsTapRevenueRecognitionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapRevenueRecognitionIDColumn]; }
			set { this[_parent.TapRevenueRecognitionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapRevenueRecognitionID is NULL, false otherwise.</summary>
		public bool IsTapRevenueRecognitionIDNull() 
		{
			return IsNull(_parent.TapRevenueRecognitionIDColumn);
		}

		/// <summary>Sets the TypedView field TapRevenueRecognitionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapRevenueRecognitionIDNull() 
		{
			this[_parent.TapRevenueRecognitionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapRevenueRecognitionAmount</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPREVENUERECOGNITIONAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapRevenueRecognitionAmount
		{
			get { return IsTapRevenueRecognitionAmountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapRevenueRecognitionAmountColumn]; }
			set { this[_parent.TapRevenueRecognitionAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapRevenueRecognitionAmount is NULL, false otherwise.</summary>
		public bool IsTapRevenueRecognitionAmountNull() 
		{
			return IsNull(_parent.TapRevenueRecognitionAmountColumn);
		}

		/// <summary>Sets the TypedView field TapRevenueRecognitionAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapRevenueRecognitionAmountNull() 
		{
			this[_parent.TapRevenueRecognitionAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapTransactionJournalID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTRANSACTIONJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapTransactionJournalID
		{
			get { return IsTapTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapTransactionJournalIDColumn]; }
			set { this[_parent.TapTransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapTransactionJournalID is NULL, false otherwise.</summary>
		public bool IsTapTransactionJournalIDNull() 
		{
			return IsNull(_parent.TapTransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field TapTransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapTransactionJournalIDNull() 
		{
			this[_parent.TapTransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapTransactionType</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTRANSACTIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TapTransactionType
		{
			get { return IsTapTransactionTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TapTransactionTypeColumn]; }
			set { this[_parent.TapTransactionTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapTransactionType is NULL, false otherwise.</summary>
		public bool IsTapTransactionTypeNull() 
		{
			return IsNull(_parent.TapTransactionTypeColumn);
		}

		/// <summary>Sets the TypedView field TapTransactionType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapTransactionTypeNull() 
		{
			this[_parent.TapTransactionTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapBoardingGuid</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPBOARDINGGUID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String TapBoardingGuid
		{
			get { return IsTapBoardingGuidNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TapBoardingGuidColumn]; }
			set { this[_parent.TapBoardingGuidColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapBoardingGuid is NULL, false otherwise.</summary>
		public bool IsTapBoardingGuidNull() 
		{
			return IsNull(_parent.TapBoardingGuidColumn);
		}

		/// <summary>Sets the TypedView field TapBoardingGuid to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapBoardingGuidNull() 
		{
			this[_parent.TapBoardingGuidColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapPurseCredit</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPPURSECREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TapPurseCredit
		{
			get { return IsTapPurseCreditNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TapPurseCreditColumn]; }
			set { this[_parent.TapPurseCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapPurseCredit is NULL, false otherwise.</summary>
		public bool IsTapPurseCreditNull() 
		{
			return IsNull(_parent.TapPurseCreditColumn);
		}

		/// <summary>Sets the TypedView field TapPurseCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapPurseCreditNull() 
		{
			this[_parent.TapPurseCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapOriginalSingleFare</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPORIGINALSINGLEFARE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TapOriginalSingleFare
		{
			get { return IsTapOriginalSingleFareNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TapOriginalSingleFareColumn]; }
			set { this[_parent.TapOriginalSingleFareColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapOriginalSingleFare is NULL, false otherwise.</summary>
		public bool IsTapOriginalSingleFareNull() 
		{
			return IsNull(_parent.TapOriginalSingleFareColumn);
		}

		/// <summary>Sets the TypedView field TapOriginalSingleFare to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapOriginalSingleFareNull() 
		{
			this[_parent.TapOriginalSingleFareColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapBaseFare</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPBASEFARE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapBaseFare
		{
			get { return IsTapBaseFareNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapBaseFareColumn]; }
			set { this[_parent.TapBaseFareColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapBaseFare is NULL, false otherwise.</summary>
		public bool IsTapBaseFareNull() 
		{
			return IsNull(_parent.TapBaseFareColumn);
		}

		/// <summary>Sets the TypedView field TapBaseFare to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapBaseFareNull() 
		{
			this[_parent.TapBaseFareColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapOperatorID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPOPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TapOperatorID
		{
			get { return IsTapOperatorIDNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TapOperatorIDColumn]; }
			set { this[_parent.TapOperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapOperatorID is NULL, false otherwise.</summary>
		public bool IsTapOperatorIDNull() 
		{
			return IsNull(_parent.TapOperatorIDColumn);
		}

		/// <summary>Sets the TypedView field TapOperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapOperatorIDNull() 
		{
			this[_parent.TapOperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapTicketNumber</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTICKETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TapTicketNumber
		{
			get { return IsTapTicketNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TapTicketNumberColumn]; }
			set { this[_parent.TapTicketNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapTicketNumber is NULL, false otherwise.</summary>
		public bool IsTapTicketNumberNull() 
		{
			return IsNull(_parent.TapTicketNumberColumn);
		}

		/// <summary>Sets the TypedView field TapTicketNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapTicketNumberNull() 
		{
			this[_parent.TapTicketNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapAppliedPassTicketID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPAPPLIEDPASSTICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapAppliedPassTicketID
		{
			get { return IsTapAppliedPassTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapAppliedPassTicketIDColumn]; }
			set { this[_parent.TapAppliedPassTicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapAppliedPassTicketID is NULL, false otherwise.</summary>
		public bool IsTapAppliedPassTicketIDNull() 
		{
			return IsNull(_parent.TapAppliedPassTicketIDColumn);
		}

		/// <summary>Sets the TypedView field TapAppliedPassTicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapAppliedPassTicketIDNull() 
		{
			this[_parent.TapAppliedPassTicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapDeviceTime</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPDEVICETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime TapDeviceTime
		{
			get { return IsTapDeviceTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TapDeviceTimeColumn]; }
			set { this[_parent.TapDeviceTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapDeviceTime is NULL, false otherwise.</summary>
		public bool IsTapDeviceTimeNull() 
		{
			return IsNull(_parent.TapDeviceTimeColumn);
		}

		/// <summary>Sets the TypedView field TapDeviceTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapDeviceTimeNull() 
		{
			this[_parent.TapDeviceTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapTicketID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPTICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapTicketID
		{
			get { return IsTapTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapTicketIDColumn]; }
			set { this[_parent.TapTicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapTicketID is NULL, false otherwise.</summary>
		public bool IsTapTicketIDNull() 
		{
			return IsNull(_parent.TapTicketIDColumn);
		}

		/// <summary>Sets the TypedView field TapTicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapTicketIDNull() 
		{
			this[_parent.TapTicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapCancellationReferenceGuid</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCANCELLATIONREFERENCEGUID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String TapCancellationReferenceGuid
		{
			get { return IsTapCancellationReferenceGuidNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TapCancellationReferenceGuidColumn]; }
			set { this[_parent.TapCancellationReferenceGuidColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapCancellationReferenceGuid is NULL, false otherwise.</summary>
		public bool IsTapCancellationReferenceGuidNull() 
		{
			return IsNull(_parent.TapCancellationReferenceGuidColumn);
		}

		/// <summary>Sets the TypedView field TapCancellationReferenceGuid to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapCancellationReferenceGuidNull() 
		{
			this[_parent.TapCancellationReferenceGuidColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapCancellationReference</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapCancellationReference
		{
			get { return IsTapCancellationReferenceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapCancellationReferenceColumn]; }
			set { this[_parent.TapCancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapCancellationReference is NULL, false otherwise.</summary>
		public bool IsTapCancellationReferenceNull() 
		{
			return IsNull(_parent.TapCancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field TapCancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapCancellationReferenceNull() 
		{
			this[_parent.TapCancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassAmount</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassAmount
		{
			get { return IsPassAmountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassAmountColumn]; }
			set { this[_parent.PassAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassAmount is NULL, false otherwise.</summary>
		public bool IsPassAmountNull() 
		{
			return IsNull(_parent.PassAmountColumn);
		}

		/// <summary>Sets the TypedView field PassAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassAmountNull() 
		{
			this[_parent.PassAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassValidFrom</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSVALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PassValidFrom
		{
			get { return IsPassValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PassValidFromColumn]; }
			set { this[_parent.PassValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassValidFrom is NULL, false otherwise.</summary>
		public bool IsPassValidFromNull() 
		{
			return IsNull(_parent.PassValidFromColumn);
		}

		/// <summary>Sets the TypedView field PassValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassValidFromNull() 
		{
			this[_parent.PassValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassValidTo</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSVALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PassValidTo
		{
			get { return IsPassValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PassValidToColumn]; }
			set { this[_parent.PassValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassValidTo is NULL, false otherwise.</summary>
		public bool IsPassValidToNull() 
		{
			return IsNull(_parent.PassValidToColumn);
		}

		/// <summary>Sets the TypedView field PassValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassValidToNull() 
		{
			this[_parent.PassValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassTicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PassTicketInternalNumber
		{
			get { return IsPassTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PassTicketInternalNumberColumn]; }
			set { this[_parent.PassTicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassTicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsPassTicketInternalNumberNull() 
		{
			return IsNull(_parent.PassTicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field PassTicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassTicketInternalNumberNull() 
		{
			this[_parent.PassTicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassCloseoutPeriodID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSCLOSEOUTPERIODID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassCloseoutPeriodID
		{
			get { return IsPassCloseoutPeriodIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassCloseoutPeriodIDColumn]; }
			set { this[_parent.PassCloseoutPeriodIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassCloseoutPeriodID is NULL, false otherwise.</summary>
		public bool IsPassCloseoutPeriodIDNull() 
		{
			return IsNull(_parent.PassCloseoutPeriodIDColumn);
		}

		/// <summary>Sets the TypedView field PassCloseoutPeriodID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassCloseoutPeriodIDNull() 
		{
			this[_parent.PassCloseoutPeriodIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TapCloseoutPeriodID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."TAPCLOSEOUTPERIODID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TapCloseoutPeriodID
		{
			get { return IsTapCloseoutPeriodIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TapCloseoutPeriodIDColumn]; }
			set { this[_parent.TapCloseoutPeriodIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TapCloseoutPeriodID is NULL, false otherwise.</summary>
		public bool IsTapCloseoutPeriodIDNull() 
		{
			return IsNull(_parent.TapCloseoutPeriodIDColumn);
		}

		/// <summary>Sets the TypedView field TapCloseoutPeriodID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTapCloseoutPeriodIDNull() 
		{
			this[_parent.TapCloseoutPeriodIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassRevenueRecognitionID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSREVENUERECOGNITIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassRevenueRecognitionID
		{
			get { return IsPassRevenueRecognitionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassRevenueRecognitionIDColumn]; }
			set { this[_parent.PassRevenueRecognitionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassRevenueRecognitionID is NULL, false otherwise.</summary>
		public bool IsPassRevenueRecognitionIDNull() 
		{
			return IsNull(_parent.PassRevenueRecognitionIDColumn);
		}

		/// <summary>Sets the TypedView field PassRevenueRecognitionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassRevenueRecognitionIDNull() 
		{
			this[_parent.PassRevenueRecognitionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PassTransactionJournalID</summary>
		/// <remarks>Mapped on view field: "ACC_PASSREVENUERECOGNITIONS"."PASSTRANSACTIONJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PassTransactionJournalID
		{
			get { return IsPassTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PassTransactionJournalIDColumn]; }
			set { this[_parent.PassTransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PassTransactionJournalID is NULL, false otherwise.</summary>
		public bool IsPassTransactionJournalIDNull() 
		{
			return IsNull(_parent.PassTransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field PassTransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPassTransactionJournalIDNull() 
		{
			this[_parent.PassTransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
