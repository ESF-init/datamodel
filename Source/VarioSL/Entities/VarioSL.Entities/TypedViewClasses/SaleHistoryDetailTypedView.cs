﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'SaleHistoryDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class SaleHistoryDetailTypedView : TypedViewBase<SaleHistoryDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnSaleID;
		private DataColumn _columnSaleTransactionID;
		private DataColumn _columnReceiptReference;
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnSaleType;
		private DataColumn _columnLocationNumber;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnSalesPersonNumber;
		private DataColumn _columnMerchantNumber;
		private DataColumn _columnNetworkReference;
		private DataColumn _columnSaleDate;
		private DataColumn _columnOperatorID;
		private DataColumn _columnAccountID;
		private DataColumn _columnSecondaryAccountID;
		private DataColumn _columnExternalOrderNumber;
		private DataColumn _columnOrderID;
		private DataColumn _columnTicketName;
		private DataColumn _columnTicketID;
		private DataColumn _columnInternalNumber;
		private DataColumn _columnPrice;
		private DataColumn _columnState;
		private DataColumn _columnOrderNumber;
		private DataColumn _columnProductID;
		private DataColumn _columnQuantity;
		private DataColumn _columnFulfilledQuantity;
		private DataColumn _columnOrderDetailID;
		private DataColumn _columnParentOrderDetailID;
		private DataColumn _columnVanpoolGroupID;
		private DataColumn _columnNotes;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 31;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static SaleHistoryDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SaleHistoryDetailTypedView():base("SaleHistoryDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SaleHistoryDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SaleHistoryDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SaleHistoryDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new SaleHistoryDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleTransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReceiptReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesPersonNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MerchantNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NetworkReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SecondaryAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExternalOrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Quantity", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FulfilledQuantity", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ParentOrderDetailID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("VanpoolGroupID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "SaleHistoryDetail";
			_columnSaleID = GeneralUtils.CreateTypedDataTableColumn("SaleID", @"SaleID", typeof(System.Int64), this.Columns);
			_columnSaleTransactionID = GeneralUtils.CreateTypedDataTableColumn("SaleTransactionID", @"SaleTransactionID", typeof(System.String), this.Columns);
			_columnReceiptReference = GeneralUtils.CreateTypedDataTableColumn("ReceiptReference", @"ReceiptReference", typeof(System.String), this.Columns);
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnSaleType = GeneralUtils.CreateTypedDataTableColumn("SaleType", @"SaleType", typeof(System.Int32), this.Columns);
			_columnLocationNumber = GeneralUtils.CreateTypedDataTableColumn("LocationNumber", @"LocationNumber", typeof(System.Int32), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnSalesPersonNumber = GeneralUtils.CreateTypedDataTableColumn("SalesPersonNumber", @"SalesPersonNumber", typeof(System.Int32), this.Columns);
			_columnMerchantNumber = GeneralUtils.CreateTypedDataTableColumn("MerchantNumber", @"MerchantNumber", typeof(System.Int32), this.Columns);
			_columnNetworkReference = GeneralUtils.CreateTypedDataTableColumn("NetworkReference", @"NetworkReference", typeof(System.String), this.Columns);
			_columnSaleDate = GeneralUtils.CreateTypedDataTableColumn("SaleDate", @"SaleDate", typeof(System.DateTime), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int64), this.Columns);
			_columnAccountID = GeneralUtils.CreateTypedDataTableColumn("AccountID", @"AccountID", typeof(System.Int64), this.Columns);
			_columnSecondaryAccountID = GeneralUtils.CreateTypedDataTableColumn("SecondaryAccountID", @"SecondaryAccountID", typeof(System.Int64), this.Columns);
			_columnExternalOrderNumber = GeneralUtils.CreateTypedDataTableColumn("ExternalOrderNumber", @"ExternalOrderNumber", typeof(System.String), this.Columns);
			_columnOrderID = GeneralUtils.CreateTypedDataTableColumn("OrderID", @"OrderID", typeof(System.Int64), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnInternalNumber = GeneralUtils.CreateTypedDataTableColumn("InternalNumber", @"InternalNumber", typeof(System.Int64), this.Columns);
			_columnPrice = GeneralUtils.CreateTypedDataTableColumn("Price", @"Price", typeof(System.Int32), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(System.Decimal), this.Columns);
			_columnOrderNumber = GeneralUtils.CreateTypedDataTableColumn("OrderNumber", @"OrderNumber", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Decimal), this.Columns);
			_columnQuantity = GeneralUtils.CreateTypedDataTableColumn("Quantity", @"Quantity", typeof(System.Decimal), this.Columns);
			_columnFulfilledQuantity = GeneralUtils.CreateTypedDataTableColumn("FulfilledQuantity", @"FulfilledQuantity", typeof(System.Decimal), this.Columns);
			_columnOrderDetailID = GeneralUtils.CreateTypedDataTableColumn("OrderDetailID", @"OrderDetailID", typeof(System.Int64), this.Columns);
			_columnParentOrderDetailID = GeneralUtils.CreateTypedDataTableColumn("ParentOrderDetailID", @"ParentOrderDetailID", typeof(System.Int64), this.Columns);
			_columnVanpoolGroupID = GeneralUtils.CreateTypedDataTableColumn("VanpoolGroupID", @"VanpoolGroupID", typeof(System.String), this.Columns);
			_columnNotes = GeneralUtils.CreateTypedDataTableColumn("Notes", @"Notes", typeof(System.String), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnSaleID = this.Columns["SaleID"];
			_columnSaleTransactionID = this.Columns["SaleTransactionID"];
			_columnReceiptReference = this.Columns["ReceiptReference"];
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnSaleType = this.Columns["SaleType"];
			_columnLocationNumber = this.Columns["LocationNumber"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnSalesPersonNumber = this.Columns["SalesPersonNumber"];
			_columnMerchantNumber = this.Columns["MerchantNumber"];
			_columnNetworkReference = this.Columns["NetworkReference"];
			_columnSaleDate = this.Columns["SaleDate"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnAccountID = this.Columns["AccountID"];
			_columnSecondaryAccountID = this.Columns["SecondaryAccountID"];
			_columnExternalOrderNumber = this.Columns["ExternalOrderNumber"];
			_columnOrderID = this.Columns["OrderID"];
			_columnTicketName = this.Columns["TicketName"];
			_columnTicketID = this.Columns["TicketID"];
			_columnInternalNumber = this.Columns["InternalNumber"];
			_columnPrice = this.Columns["Price"];
			_columnState = this.Columns["State"];
			_columnOrderNumber = this.Columns["OrderNumber"];
			_columnProductID = this.Columns["ProductID"];
			_columnQuantity = this.Columns["Quantity"];
			_columnFulfilledQuantity = this.Columns["FulfilledQuantity"];
			_columnOrderDetailID = this.Columns["OrderDetailID"];
			_columnParentOrderDetailID = this.Columns["ParentOrderDetailID"];
			_columnVanpoolGroupID = this.Columns["VanpoolGroupID"];
			_columnNotes = this.Columns["Notes"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			SaleHistoryDetailTypedView cloneToReturn = ((SaleHistoryDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new SaleHistoryDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return SaleHistoryDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return SaleHistoryDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'SaleID'</summary>
		internal DataColumn SaleIDColumn 
		{
			get { return _columnSaleID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleTransactionID'</summary>
		internal DataColumn SaleTransactionIDColumn 
		{
			get { return _columnSaleTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReceiptReference'</summary>
		internal DataColumn ReceiptReferenceColumn 
		{
			get { return _columnReceiptReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleType'</summary>
		internal DataColumn SaleTypeColumn 
		{
			get { return _columnSaleType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LocationNumber'</summary>
		internal DataColumn LocationNumberColumn 
		{
			get { return _columnLocationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesPersonNumber'</summary>
		internal DataColumn SalesPersonNumberColumn 
		{
			get { return _columnSalesPersonNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MerchantNumber'</summary>
		internal DataColumn MerchantNumberColumn 
		{
			get { return _columnMerchantNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NetworkReference'</summary>
		internal DataColumn NetworkReferenceColumn 
		{
			get { return _columnNetworkReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleDate'</summary>
		internal DataColumn SaleDateColumn 
		{
			get { return _columnSaleDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AccountID'</summary>
		internal DataColumn AccountIDColumn 
		{
			get { return _columnAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SecondaryAccountID'</summary>
		internal DataColumn SecondaryAccountIDColumn 
		{
			get { return _columnSecondaryAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExternalOrderNumber'</summary>
		internal DataColumn ExternalOrderNumberColumn 
		{
			get { return _columnExternalOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderID'</summary>
		internal DataColumn OrderIDColumn 
		{
			get { return _columnOrderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InternalNumber'</summary>
		internal DataColumn InternalNumberColumn 
		{
			get { return _columnInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Price'</summary>
		internal DataColumn PriceColumn 
		{
			get { return _columnPrice; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderNumber'</summary>
		internal DataColumn OrderNumberColumn 
		{
			get { return _columnOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Quantity'</summary>
		internal DataColumn QuantityColumn 
		{
			get { return _columnQuantity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FulfilledQuantity'</summary>
		internal DataColumn FulfilledQuantityColumn 
		{
			get { return _columnFulfilledQuantity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderDetailID'</summary>
		internal DataColumn OrderDetailIDColumn 
		{
			get { return _columnOrderDetailID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ParentOrderDetailID'</summary>
		internal DataColumn ParentOrderDetailIDColumn 
		{
			get { return _columnParentOrderDetailID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'VanpoolGroupID'</summary>
		internal DataColumn VanpoolGroupIDColumn 
		{
			get { return _columnVanpoolGroupID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Notes'</summary>
		internal DataColumn NotesColumn 
		{
			get { return _columnNotes; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable SaleHistoryDetail</summary>
	public partial class SaleHistoryDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private SaleHistoryDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal SaleHistoryDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((SaleHistoryDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field SaleID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SaleID
		{
			get { return IsSaleIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SaleIDColumn]; }
			set { this[_parent.SaleIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleID is NULL, false otherwise.</summary>
		public bool IsSaleIDNull() 
		{
			return IsNull(_parent.SaleIDColumn);
		}

		/// <summary>Sets the TypedView field SaleID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleIDNull() 
		{
			this[_parent.SaleIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleTransactionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALETRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String SaleTransactionID
		{
			get { return IsSaleTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SaleTransactionIDColumn]; }
			set { this[_parent.SaleTransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleTransactionID is NULL, false otherwise.</summary>
		public bool IsSaleTransactionIDNull() 
		{
			return IsNull(_parent.SaleTransactionIDColumn);
		}

		/// <summary>Sets the TypedView field SaleTransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTransactionIDNull() 
		{
			this[_parent.SaleTransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReceiptReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."RECEIPTREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String ReceiptReference
		{
			get { return IsReceiptReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReceiptReferenceColumn]; }
			set { this[_parent.ReceiptReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReceiptReference is NULL, false otherwise.</summary>
		public bool IsReceiptReferenceNull() 
		{
			return IsNull(_parent.ReceiptReferenceColumn);
		}

		/// <summary>Sets the TypedView field ReceiptReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReceiptReferenceNull() 
		{
			this[_parent.ReceiptReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SaleType
		{
			get { return IsSaleTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SaleTypeColumn]; }
			set { this[_parent.SaleTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleType is NULL, false otherwise.</summary>
		public bool IsSaleTypeNull() 
		{
			return IsNull(_parent.SaleTypeColumn);
		}

		/// <summary>Sets the TypedView field SaleType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTypeNull() 
		{
			this[_parent.SaleTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LocationNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."LOCATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 LocationNumber
		{
			get { return IsLocationNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.LocationNumberColumn]; }
			set { this[_parent.LocationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LocationNumber is NULL, false otherwise.</summary>
		public bool IsLocationNumberNull() 
		{
			return IsNull(_parent.LocationNumberColumn);
		}

		/// <summary>Sets the TypedView field LocationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNumberNull() 
		{
			this[_parent.LocationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesPersonNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALESPERSONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SalesPersonNumber
		{
			get { return IsSalesPersonNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SalesPersonNumberColumn]; }
			set { this[_parent.SalesPersonNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesPersonNumber is NULL, false otherwise.</summary>
		public bool IsSalesPersonNumberNull() 
		{
			return IsNull(_parent.SalesPersonNumberColumn);
		}

		/// <summary>Sets the TypedView field SalesPersonNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesPersonNumberNull() 
		{
			this[_parent.SalesPersonNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MerchantNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."MERCHANTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MerchantNumber
		{
			get { return IsMerchantNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MerchantNumberColumn]; }
			set { this[_parent.MerchantNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MerchantNumber is NULL, false otherwise.</summary>
		public bool IsMerchantNumberNull() 
		{
			return IsNull(_parent.MerchantNumberColumn);
		}

		/// <summary>Sets the TypedView field MerchantNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMerchantNumberNull() 
		{
			this[_parent.MerchantNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NetworkReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."NETWORKREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String NetworkReference
		{
			get { return IsNetworkReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NetworkReferenceColumn]; }
			set { this[_parent.NetworkReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NetworkReference is NULL, false otherwise.</summary>
		public bool IsNetworkReferenceNull() 
		{
			return IsNull(_parent.NetworkReferenceColumn);
		}

		/// <summary>Sets the TypedView field NetworkReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNetworkReferenceNull() 
		{
			this[_parent.NetworkReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SALEDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime SaleDate
		{
			get { return IsSaleDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.SaleDateColumn]; }
			set { this[_parent.SaleDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleDate is NULL, false otherwise.</summary>
		public bool IsSaleDateNull() 
		{
			return IsNull(_parent.SaleDateColumn);
		}

		/// <summary>Sets the TypedView field SaleDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleDateNull() 
		{
			this[_parent.SaleDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."ACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 AccountID
		{
			get { return IsAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AccountIDColumn]; }
			set { this[_parent.AccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountID is NULL, false otherwise.</summary>
		public bool IsAccountIDNull() 
		{
			return IsNull(_parent.AccountIDColumn);
		}

		/// <summary>Sets the TypedView field AccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountIDNull() 
		{
			this[_parent.AccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SecondaryAccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."SECONDARYACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SecondaryAccountID
		{
			get { return IsSecondaryAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SecondaryAccountIDColumn]; }
			set { this[_parent.SecondaryAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SecondaryAccountID is NULL, false otherwise.</summary>
		public bool IsSecondaryAccountIDNull() 
		{
			return IsNull(_parent.SecondaryAccountIDColumn);
		}

		/// <summary>Sets the TypedView field SecondaryAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSecondaryAccountIDNull() 
		{
			this[_parent.SecondaryAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExternalOrderNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."EXTERNALORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ExternalOrderNumber
		{
			get { return IsExternalOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExternalOrderNumberColumn]; }
			set { this[_parent.ExternalOrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExternalOrderNumber is NULL, false otherwise.</summary>
		public bool IsExternalOrderNumberNull() 
		{
			return IsNull(_parent.ExternalOrderNumberColumn);
		}

		/// <summary>Sets the TypedView field ExternalOrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExternalOrderNumberNull() 
		{
			this[_parent.ExternalOrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."ORDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderID
		{
			get { return IsOrderIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderIDColumn]; }
			set { this[_parent.OrderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderID is NULL, false otherwise.</summary>
		public bool IsOrderIDNull() 
		{
			return IsNull(_parent.OrderIDColumn);
		}

		/// <summary>Sets the TypedView field OrderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderIDNull() 
		{
			this[_parent.OrderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."INTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 InternalNumber
		{
			get { return IsInternalNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.InternalNumberColumn]; }
			set { this[_parent.InternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InternalNumber is NULL, false otherwise.</summary>
		public bool IsInternalNumberNull() 
		{
			return IsNull(_parent.InternalNumberColumn);
		}

		/// <summary>Sets the TypedView field InternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInternalNumberNull() 
		{
			this[_parent.InternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Price</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."PRICE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Price
		{
			get { return IsPriceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PriceColumn]; }
			set { this[_parent.PriceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Price is NULL, false otherwise.</summary>
		public bool IsPriceNull() 
		{
			return IsNull(_parent.PriceColumn);
		}

		/// <summary>Sets the TypedView field Price to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPriceNull() 
		{
			this[_parent.PriceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal State
		{
			get { return IsStateNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."ORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrderNumber
		{
			get { return IsOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrderNumberColumn]; }
			set { this[_parent.OrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderNumber is NULL, false otherwise.</summary>
		public bool IsOrderNumberNull() 
		{
			return IsNull(_parent.OrderNumberColumn);
		}

		/// <summary>Sets the TypedView field OrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderNumberNull() 
		{
			this[_parent.OrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal ProductID
		{
			get { return IsProductIDNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Quantity</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."QUANTITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal Quantity
		{
			get { return IsQuantityNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.QuantityColumn]; }
			set { this[_parent.QuantityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Quantity is NULL, false otherwise.</summary>
		public bool IsQuantityNull() 
		{
			return IsNull(_parent.QuantityColumn);
		}

		/// <summary>Sets the TypedView field Quantity to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetQuantityNull() 
		{
			this[_parent.QuantityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FulfilledQuantity</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."FULFILLEDQUANTITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal FulfilledQuantity
		{
			get { return IsFulfilledQuantityNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.FulfilledQuantityColumn]; }
			set { this[_parent.FulfilledQuantityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FulfilledQuantity is NULL, false otherwise.</summary>
		public bool IsFulfilledQuantityNull() 
		{
			return IsNull(_parent.FulfilledQuantityColumn);
		}

		/// <summary>Sets the TypedView field FulfilledQuantity to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFulfilledQuantityNull() 
		{
			this[_parent.FulfilledQuantityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderDetailID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."ORDERDETAILID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderDetailID
		{
			get { return IsOrderDetailIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderDetailIDColumn]; }
			set { this[_parent.OrderDetailIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderDetailID is NULL, false otherwise.</summary>
		public bool IsOrderDetailIDNull() 
		{
			return IsNull(_parent.OrderDetailIDColumn);
		}

		/// <summary>Sets the TypedView field OrderDetailID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderDetailIDNull() 
		{
			this[_parent.OrderDetailIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ParentOrderDetailID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."PARENTORDERDETAILID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ParentOrderDetailID
		{
			get { return IsParentOrderDetailIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ParentOrderDetailIDColumn]; }
			set { this[_parent.ParentOrderDetailIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ParentOrderDetailID is NULL, false otherwise.</summary>
		public bool IsParentOrderDetailIDNull() 
		{
			return IsNull(_parent.ParentOrderDetailIDColumn);
		}

		/// <summary>Sets the TypedView field ParentOrderDetailID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetParentOrderDetailIDNull() 
		{
			this[_parent.ParentOrderDetailIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field VanpoolGroupID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."VANPOOLGROUPID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String VanpoolGroupID
		{
			get { return IsVanpoolGroupIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.VanpoolGroupIDColumn]; }
			set { this[_parent.VanpoolGroupIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field VanpoolGroupID is NULL, false otherwise.</summary>
		public bool IsVanpoolGroupIDNull() 
		{
			return IsNull(_parent.VanpoolGroupIDColumn);
		}

		/// <summary>Sets the TypedView field VanpoolGroupID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetVanpoolGroupIDNull() 
		{
			this[_parent.VanpoolGroupIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Notes</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."NOTES"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 255</remarks>
		public System.String Notes
		{
			get { return IsNotesNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NotesColumn]; }
			set { this[_parent.NotesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Notes is NULL, false otherwise.</summary>
		public bool IsNotesNull() 
		{
			return IsNull(_parent.NotesColumn);
		}

		/// <summary>Sets the TypedView field Notes to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNotesNull() 
		{
			this[_parent.NotesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORYDETAIL"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
