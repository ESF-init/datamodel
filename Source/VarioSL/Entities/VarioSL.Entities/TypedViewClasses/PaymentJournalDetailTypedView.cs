﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'PaymentJournalDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class PaymentJournalDetailTypedView : TypedViewBase<PaymentJournalDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnPaymentJournalID;
		private DataColumn _columnPaymentJournalTransactionJournalID;
		private DataColumn _columnPaymentJournalSaleID;
		private DataColumn _columnPaymentType;
		private DataColumn _columnPaymentJournalCreditcardNetwork;
		private DataColumn _columnAmount;
		private DataColumn _columnCode;
		private DataColumn _columnConfirmed;
		private DataColumn _columnExecutionTime;
		private DataColumn _columnExecutionResult;
		private DataColumn _columnState;
		private DataColumn _columnToken;
		private DataColumn _columnIsPretax;
		private DataColumn _columnReferenceNumber;
		private DataColumn _columnPaymentOptionID;
		private DataColumn _columnMaskedPan;
		private DataColumn _columnProviderReference;
		private DataColumn _columnReconciliationState;
		private DataColumn _columnPaymentOptionContractID;
		private DataColumn _columnPaymentMethod;
		private DataColumn _columnPaymentOptionDescription;
		private DataColumn _columnBankingServiceToken;
		private DataColumn _columnPaymentOptionExpiry;
		private DataColumn _columnPaymentOptionCreditcardNetwork;
		private DataColumn _columnPaymentOptionPriority;
		private DataColumn _columnPaymentOptionName;
		private DataColumn _columnIsTemporary;
		private DataColumn _columnPaymentOptionIsRetired;
		private DataColumn _columnBankConnectionDataID;
		private DataColumn _columnProspectivePaymentOptionID;
		private DataColumn _columnPaymentOptionIsBlocked;
		private DataColumn _columnTransactionJournalID;
		private DataColumn _columnTransactionID;
		private DataColumn _columnDeviceTime;
		private DataColumn _columnBoardingTime;
		private DataColumn _columnOperatorID;
		private DataColumn _columnTransactionJournalClientID;
		private DataColumn _columnLine;
		private DataColumn _columnCourseNumber;
		private DataColumn _columnRouteNumber;
		private DataColumn _columnRouteCode;
		private DataColumn _columnDirection;
		private DataColumn _columnZone;
		private DataColumn _columnTransactionJournalSalesChannel;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnVehicleNumber;
		private DataColumn _columnMountingPlateNumber;
		private DataColumn _columnDebtorNumber;
		private DataColumn _columnGeoLocationLongitude;
		private DataColumn _columnGeoLocationLatitude;
		private DataColumn _columnStopNumber;
		private DataColumn _columnTransitAccountID;
		private DataColumn _columnFareMediaID;
		private DataColumn _columnFareMediaType;
		private DataColumn _columnProductID;
		private DataColumn _columnTicketID;
		private DataColumn _columnFareAmount;
		private DataColumn _columnCustomerGroup;
		private DataColumn _columnTransactionType;
		private DataColumn _columnTransactionNumber;
		private DataColumn _columnResultType;
		private DataColumn _columnFillLevel;
		private DataColumn _columnPurseBalance;
		private DataColumn _columnPurseCredit;
		private DataColumn _columnGroupSize;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnDuration;
		private DataColumn _columnTariffDate;
		private DataColumn _columnTariffVersion;
		private DataColumn _columnTicketInternalNumber;
		private DataColumn _columnWhitelistVersion;
		private DataColumn _columnCancellationReference;
		private DataColumn _columnCancellationReferenceGuid;
		private DataColumn _columnWhitelistVersionCreated;
		private DataColumn _columnProperties;
		private DataColumn _columnTransactionJournalSaleID;
		private DataColumn _columnTripTicketInternalNumber;
		private DataColumn _columnShiftBegin;
		private DataColumn _columnCompletionState;
		private DataColumn _columnTransportState;
		private DataColumn _columnProductID2;
		private DataColumn _columnPurseBalance2;
		private DataColumn _columnPurseCredit2;
		private DataColumn _columnCreditCardAuthorizationID;
		private DataColumn _columnTripCounter;
		private DataColumn _columnInsertTime;
		private DataColumn _columnSaleTransactionID;
		private DataColumn _columnSaleType;
		private DataColumn _columnSaleDate;
		private DataColumn _columnSaleClientID;
		private DataColumn _columnSaleSalesChannel;
		private DataColumn _columnLocationNumber;
		private DataColumn _columnSaleDeviceNumber;
		private DataColumn _columnSalesPersonNumber;
		private DataColumn _columnMerchantNumber;
		private DataColumn _columnReceiptReference;
		private DataColumn _columnNetworkReference;
		private DataColumn _columnCancellationReferenceID;
		private DataColumn _columnIsClosed;
		private DataColumn _columnOrderID;
		private DataColumn _columnExternalOrderNumber;
		private DataColumn _columnSaleContractID;
		private DataColumn _columnRefundReferenceID;
		private DataColumn _columnIsOrganizational;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 105;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static PaymentJournalDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public PaymentJournalDetailTypedView():base("PaymentJournalDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentJournalDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.PaymentJournalDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.PaymentJournalDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new PaymentJournalDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentJournalTransactionJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentJournalSaleID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentJournalCreditcardNetwork", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Code", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Confirmed", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExecutionTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExecutionResult", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Token", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsPretax", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReferenceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MaskedPan", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProviderReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReconciliationState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentMethod", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionDescription", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankingServiceToken", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionExpiry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionCreditcardNetwork", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionPriority", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsTemporary", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionIsRetired", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankConnectionDataID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProspectivePaymentOptionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionIsBlocked", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BoardingTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Line", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CourseNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RouteNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RouteCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Direction", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Zone", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalSalesChannel", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("VehicleNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MountingPlateNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebtorNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GeoLocationLongitude", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GeoLocationLatitude", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransitAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareAmount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CustomerGroup", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ResultType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FillLevel", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("GroupSize", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffVersion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WhitelistVersion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReferenceGuid", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WhitelistVersionCreated", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Properties", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionJournalSaleID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripTicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ShiftBegin", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CompletionState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransportState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseBalance2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PurseCredit2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreditCardAuthorizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TripCounter", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("InsertTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleTransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleSalesChannel", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleDeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesPersonNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MerchantNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReceiptReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NetworkReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CancellationReferenceID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsClosed", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExternalOrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RefundReferenceID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsOrganizational", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "PaymentJournalDetail";
			_columnPaymentJournalID = GeneralUtils.CreateTypedDataTableColumn("PaymentJournalID", @"PaymentJournalID", typeof(System.Int64), this.Columns);
			_columnPaymentJournalTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("PaymentJournalTransactionJournalID", @"PaymentJournalTransactionJournalID", typeof(System.Int64), this.Columns);
			_columnPaymentJournalSaleID = GeneralUtils.CreateTypedDataTableColumn("PaymentJournalSaleID", @"PaymentJournalSaleID", typeof(System.Int64), this.Columns);
			_columnPaymentType = GeneralUtils.CreateTypedDataTableColumn("PaymentType", @"PaymentType", typeof(VarioSL.Entities.Enumerations.DevicePaymentMethod), this.Columns);
			_columnPaymentJournalCreditcardNetwork = GeneralUtils.CreateTypedDataTableColumn("PaymentJournalCreditcardNetwork", @"PaymentJournalCreditcardNetwork", typeof(VarioSL.Entities.Enumerations.CreditCardNetwork), this.Columns);
			_columnAmount = GeneralUtils.CreateTypedDataTableColumn("Amount", @"Amount", typeof(System.Int32), this.Columns);
			_columnCode = GeneralUtils.CreateTypedDataTableColumn("Code", @"Code", typeof(System.String), this.Columns);
			_columnConfirmed = GeneralUtils.CreateTypedDataTableColumn("Confirmed", @"Confirmed", typeof(System.Int16), this.Columns);
			_columnExecutionTime = GeneralUtils.CreateTypedDataTableColumn("ExecutionTime", @"ExecutionTime", typeof(System.DateTime), this.Columns);
			_columnExecutionResult = GeneralUtils.CreateTypedDataTableColumn("ExecutionResult", @"ExecutionResult", typeof(System.String), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(System.Int32), this.Columns);
			_columnToken = GeneralUtils.CreateTypedDataTableColumn("Token", @"Token", typeof(System.String), this.Columns);
			_columnIsPretax = GeneralUtils.CreateTypedDataTableColumn("IsPretax", @"IsPretax", typeof(System.Int16), this.Columns);
			_columnReferenceNumber = GeneralUtils.CreateTypedDataTableColumn("ReferenceNumber", @"ReferenceNumber", typeof(System.String), this.Columns);
			_columnPaymentOptionID = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionID", @"PaymentOptionID", typeof(System.Int64), this.Columns);
			_columnMaskedPan = GeneralUtils.CreateTypedDataTableColumn("MaskedPan", @"MaskedPan", typeof(System.String), this.Columns);
			_columnProviderReference = GeneralUtils.CreateTypedDataTableColumn("ProviderReference", @"ProviderReference", typeof(System.String), this.Columns);
			_columnReconciliationState = GeneralUtils.CreateTypedDataTableColumn("ReconciliationState", @"ReconciliationState", typeof(VarioSL.Entities.Enumerations.PaymentReconciliationState), this.Columns);
			_columnPaymentOptionContractID = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionContractID", @"PaymentOptionContractID", typeof(System.Int64), this.Columns);
			_columnPaymentMethod = GeneralUtils.CreateTypedDataTableColumn("PaymentMethod", @"PaymentMethod", typeof(VarioSL.Entities.Enumerations.PaymentMethod), this.Columns);
			_columnPaymentOptionDescription = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionDescription", @"PaymentOptionDescription", typeof(System.String), this.Columns);
			_columnBankingServiceToken = GeneralUtils.CreateTypedDataTableColumn("BankingServiceToken", @"BankingServiceToken", typeof(System.String), this.Columns);
			_columnPaymentOptionExpiry = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionExpiry", @"PaymentOptionExpiry", typeof(System.DateTime), this.Columns);
			_columnPaymentOptionCreditcardNetwork = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionCreditcardNetwork", @"PaymentOptionCreditcardNetwork", typeof(System.String), this.Columns);
			_columnPaymentOptionPriority = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionPriority", @"PaymentOptionPriority", typeof(System.Int32), this.Columns);
			_columnPaymentOptionName = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionName", @"PaymentOptionName", typeof(System.String), this.Columns);
			_columnIsTemporary = GeneralUtils.CreateTypedDataTableColumn("IsTemporary", @"IsTemporary", typeof(System.Int16), this.Columns);
			_columnPaymentOptionIsRetired = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionIsRetired", @"PaymentOptionIsRetired", typeof(System.Int16), this.Columns);
			_columnBankConnectionDataID = GeneralUtils.CreateTypedDataTableColumn("BankConnectionDataID", @"BankConnectionDataID", typeof(System.Int64), this.Columns);
			_columnProspectivePaymentOptionID = GeneralUtils.CreateTypedDataTableColumn("ProspectivePaymentOptionID", @"ProspectivePaymentOptionID", typeof(System.Int64), this.Columns);
			_columnPaymentOptionIsBlocked = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionIsBlocked", @"PaymentOptionIsBlocked", typeof(System.Int16), this.Columns);
			_columnTransactionJournalID = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalID", @"TransactionJournalID", typeof(System.Int64), this.Columns);
			_columnTransactionID = GeneralUtils.CreateTypedDataTableColumn("TransactionID", @"TransactionID", typeof(System.String), this.Columns);
			_columnDeviceTime = GeneralUtils.CreateTypedDataTableColumn("DeviceTime", @"DeviceTime", typeof(System.DateTime), this.Columns);
			_columnBoardingTime = GeneralUtils.CreateTypedDataTableColumn("BoardingTime", @"BoardingTime", typeof(System.DateTime), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int64), this.Columns);
			_columnTransactionJournalClientID = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalClientID", @"TransactionJournalClientID", typeof(System.Int64), this.Columns);
			_columnLine = GeneralUtils.CreateTypedDataTableColumn("Line", @"Line", typeof(System.String), this.Columns);
			_columnCourseNumber = GeneralUtils.CreateTypedDataTableColumn("CourseNumber", @"CourseNumber", typeof(System.Int32), this.Columns);
			_columnRouteNumber = GeneralUtils.CreateTypedDataTableColumn("RouteNumber", @"RouteNumber", typeof(System.Int32), this.Columns);
			_columnRouteCode = GeneralUtils.CreateTypedDataTableColumn("RouteCode", @"RouteCode", typeof(System.String), this.Columns);
			_columnDirection = GeneralUtils.CreateTypedDataTableColumn("Direction", @"Direction", typeof(System.Int32), this.Columns);
			_columnZone = GeneralUtils.CreateTypedDataTableColumn("Zone", @"Zone", typeof(System.Int32), this.Columns);
			_columnTransactionJournalSalesChannel = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalSalesChannel", @"TransactionJournalSalesChannel", typeof(System.Int64), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnVehicleNumber = GeneralUtils.CreateTypedDataTableColumn("VehicleNumber", @"VehicleNumber", typeof(System.Int32), this.Columns);
			_columnMountingPlateNumber = GeneralUtils.CreateTypedDataTableColumn("MountingPlateNumber", @"MountingPlateNumber", typeof(System.Int32), this.Columns);
			_columnDebtorNumber = GeneralUtils.CreateTypedDataTableColumn("DebtorNumber", @"DebtorNumber", typeof(System.Int32), this.Columns);
			_columnGeoLocationLongitude = GeneralUtils.CreateTypedDataTableColumn("GeoLocationLongitude", @"GeoLocationLongitude", typeof(System.Int32), this.Columns);
			_columnGeoLocationLatitude = GeneralUtils.CreateTypedDataTableColumn("GeoLocationLatitude", @"GeoLocationLatitude", typeof(System.Int32), this.Columns);
			_columnStopNumber = GeneralUtils.CreateTypedDataTableColumn("StopNumber", @"StopNumber", typeof(System.Int32), this.Columns);
			_columnTransitAccountID = GeneralUtils.CreateTypedDataTableColumn("TransitAccountID", @"TransitAccountID", typeof(System.Int64), this.Columns);
			_columnFareMediaID = GeneralUtils.CreateTypedDataTableColumn("FareMediaID", @"FareMediaID", typeof(System.String), this.Columns);
			_columnFareMediaType = GeneralUtils.CreateTypedDataTableColumn("FareMediaType", @"FareMediaType", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnFareAmount = GeneralUtils.CreateTypedDataTableColumn("FareAmount", @"FareAmount", typeof(System.Int32), this.Columns);
			_columnCustomerGroup = GeneralUtils.CreateTypedDataTableColumn("CustomerGroup", @"CustomerGroup", typeof(System.String), this.Columns);
			_columnTransactionType = GeneralUtils.CreateTypedDataTableColumn("TransactionType", @"TransactionType", typeof(VarioSL.Entities.Enumerations.TransactionType), this.Columns);
			_columnTransactionNumber = GeneralUtils.CreateTypedDataTableColumn("TransactionNumber", @"TransactionNumber", typeof(System.Int32), this.Columns);
			_columnResultType = GeneralUtils.CreateTypedDataTableColumn("ResultType", @"ResultType", typeof(VarioSL.Entities.Enumerations.FarePaymentResultType), this.Columns);
			_columnFillLevel = GeneralUtils.CreateTypedDataTableColumn("FillLevel", @"FillLevel", typeof(System.Int32), this.Columns);
			_columnPurseBalance = GeneralUtils.CreateTypedDataTableColumn("PurseBalance", @"PurseBalance", typeof(System.Int32), this.Columns);
			_columnPurseCredit = GeneralUtils.CreateTypedDataTableColumn("PurseCredit", @"PurseCredit", typeof(System.Int32), this.Columns);
			_columnGroupSize = GeneralUtils.CreateTypedDataTableColumn("GroupSize", @"GroupSize", typeof(System.Int32), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnDuration = GeneralUtils.CreateTypedDataTableColumn("Duration", @"Duration", typeof(System.Int64), this.Columns);
			_columnTariffDate = GeneralUtils.CreateTypedDataTableColumn("TariffDate", @"TariffDate", typeof(System.DateTime), this.Columns);
			_columnTariffVersion = GeneralUtils.CreateTypedDataTableColumn("TariffVersion", @"TariffVersion", typeof(System.Int32), this.Columns);
			_columnTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TicketInternalNumber", @"TicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnWhitelistVersion = GeneralUtils.CreateTypedDataTableColumn("WhitelistVersion", @"WhitelistVersion", typeof(System.Int64), this.Columns);
			_columnCancellationReference = GeneralUtils.CreateTypedDataTableColumn("CancellationReference", @"CancellationReference", typeof(System.Int64), this.Columns);
			_columnCancellationReferenceGuid = GeneralUtils.CreateTypedDataTableColumn("CancellationReferenceGuid", @"CancellationReferenceGuid", typeof(System.String), this.Columns);
			_columnWhitelistVersionCreated = GeneralUtils.CreateTypedDataTableColumn("WhitelistVersionCreated", @"WhitelistVersionCreated", typeof(System.Int64), this.Columns);
			_columnProperties = GeneralUtils.CreateTypedDataTableColumn("Properties", @"Properties", typeof(VarioSL.Entities.Enumerations.TransactionJournalProperties), this.Columns);
			_columnTransactionJournalSaleID = GeneralUtils.CreateTypedDataTableColumn("TransactionJournalSaleID", @"TransactionJournalSaleID", typeof(System.Int64), this.Columns);
			_columnTripTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TripTicketInternalNumber", @"TripTicketInternalNumber", typeof(System.Int32), this.Columns);
			_columnShiftBegin = GeneralUtils.CreateTypedDataTableColumn("ShiftBegin", @"ShiftBegin", typeof(System.DateTime), this.Columns);
			_columnCompletionState = GeneralUtils.CreateTypedDataTableColumn("CompletionState", @"CompletionState", typeof(System.String), this.Columns);
			_columnTransportState = GeneralUtils.CreateTypedDataTableColumn("TransportState", @"TransportState", typeof(VarioSL.Entities.Enumerations.TransactionTransportState), this.Columns);
			_columnProductID2 = GeneralUtils.CreateTypedDataTableColumn("ProductID2", @"ProductID2", typeof(System.Int64), this.Columns);
			_columnPurseBalance2 = GeneralUtils.CreateTypedDataTableColumn("PurseBalance2", @"PurseBalance2", typeof(System.Int32), this.Columns);
			_columnPurseCredit2 = GeneralUtils.CreateTypedDataTableColumn("PurseCredit2", @"PurseCredit2", typeof(System.Int32), this.Columns);
			_columnCreditCardAuthorizationID = GeneralUtils.CreateTypedDataTableColumn("CreditCardAuthorizationID", @"CreditCardAuthorizationID", typeof(System.Int64), this.Columns);
			_columnTripCounter = GeneralUtils.CreateTypedDataTableColumn("TripCounter", @"TripCounter", typeof(System.Int32), this.Columns);
			_columnInsertTime = GeneralUtils.CreateTypedDataTableColumn("InsertTime", @"InsertTime", typeof(System.DateTime), this.Columns);
			_columnSaleTransactionID = GeneralUtils.CreateTypedDataTableColumn("SaleTransactionID", @"SaleTransactionID", typeof(System.String), this.Columns);
			_columnSaleType = GeneralUtils.CreateTypedDataTableColumn("SaleType", @"SaleType", typeof(VarioSL.Entities.Enumerations.SaleType), this.Columns);
			_columnSaleDate = GeneralUtils.CreateTypedDataTableColumn("SaleDate", @"SaleDate", typeof(System.DateTime), this.Columns);
			_columnSaleClientID = GeneralUtils.CreateTypedDataTableColumn("SaleClientID", @"SaleClientID", typeof(System.Int64), this.Columns);
			_columnSaleSalesChannel = GeneralUtils.CreateTypedDataTableColumn("SaleSalesChannel", @"SaleSalesChannel", typeof(System.Int64), this.Columns);
			_columnLocationNumber = GeneralUtils.CreateTypedDataTableColumn("LocationNumber", @"LocationNumber", typeof(System.Int32), this.Columns);
			_columnSaleDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("SaleDeviceNumber", @"SaleDeviceNumber", typeof(System.Int32), this.Columns);
			_columnSalesPersonNumber = GeneralUtils.CreateTypedDataTableColumn("SalesPersonNumber", @"SalesPersonNumber", typeof(System.Int32), this.Columns);
			_columnMerchantNumber = GeneralUtils.CreateTypedDataTableColumn("MerchantNumber", @"MerchantNumber", typeof(System.Int32), this.Columns);
			_columnReceiptReference = GeneralUtils.CreateTypedDataTableColumn("ReceiptReference", @"ReceiptReference", typeof(System.String), this.Columns);
			_columnNetworkReference = GeneralUtils.CreateTypedDataTableColumn("NetworkReference", @"NetworkReference", typeof(System.String), this.Columns);
			_columnCancellationReferenceID = GeneralUtils.CreateTypedDataTableColumn("CancellationReferenceID", @"CancellationReferenceID", typeof(System.Int64), this.Columns);
			_columnIsClosed = GeneralUtils.CreateTypedDataTableColumn("IsClosed", @"IsClosed", typeof(System.Int16), this.Columns);
			_columnOrderID = GeneralUtils.CreateTypedDataTableColumn("OrderID", @"OrderID", typeof(System.Decimal), this.Columns);
			_columnExternalOrderNumber = GeneralUtils.CreateTypedDataTableColumn("ExternalOrderNumber", @"ExternalOrderNumber", typeof(System.String), this.Columns);
			_columnSaleContractID = GeneralUtils.CreateTypedDataTableColumn("SaleContractID", @"SaleContractID", typeof(System.Decimal), this.Columns);
			_columnRefundReferenceID = GeneralUtils.CreateTypedDataTableColumn("RefundReferenceID", @"RefundReferenceID", typeof(System.Int64), this.Columns);
			_columnIsOrganizational = GeneralUtils.CreateTypedDataTableColumn("IsOrganizational", @"IsOrganizational", typeof(System.Int16), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnPaymentJournalID = this.Columns["PaymentJournalID"];
			_columnPaymentJournalTransactionJournalID = this.Columns["PaymentJournalTransactionJournalID"];
			_columnPaymentJournalSaleID = this.Columns["PaymentJournalSaleID"];
			_columnPaymentType = this.Columns["PaymentType"];
			_columnPaymentJournalCreditcardNetwork = this.Columns["PaymentJournalCreditcardNetwork"];
			_columnAmount = this.Columns["Amount"];
			_columnCode = this.Columns["Code"];
			_columnConfirmed = this.Columns["Confirmed"];
			_columnExecutionTime = this.Columns["ExecutionTime"];
			_columnExecutionResult = this.Columns["ExecutionResult"];
			_columnState = this.Columns["State"];
			_columnToken = this.Columns["Token"];
			_columnIsPretax = this.Columns["IsPretax"];
			_columnReferenceNumber = this.Columns["ReferenceNumber"];
			_columnPaymentOptionID = this.Columns["PaymentOptionID"];
			_columnMaskedPan = this.Columns["MaskedPan"];
			_columnProviderReference = this.Columns["ProviderReference"];
			_columnReconciliationState = this.Columns["ReconciliationState"];
			_columnPaymentOptionContractID = this.Columns["PaymentOptionContractID"];
			_columnPaymentMethod = this.Columns["PaymentMethod"];
			_columnPaymentOptionDescription = this.Columns["PaymentOptionDescription"];
			_columnBankingServiceToken = this.Columns["BankingServiceToken"];
			_columnPaymentOptionExpiry = this.Columns["PaymentOptionExpiry"];
			_columnPaymentOptionCreditcardNetwork = this.Columns["PaymentOptionCreditcardNetwork"];
			_columnPaymentOptionPriority = this.Columns["PaymentOptionPriority"];
			_columnPaymentOptionName = this.Columns["PaymentOptionName"];
			_columnIsTemporary = this.Columns["IsTemporary"];
			_columnPaymentOptionIsRetired = this.Columns["PaymentOptionIsRetired"];
			_columnBankConnectionDataID = this.Columns["BankConnectionDataID"];
			_columnProspectivePaymentOptionID = this.Columns["ProspectivePaymentOptionID"];
			_columnPaymentOptionIsBlocked = this.Columns["PaymentOptionIsBlocked"];
			_columnTransactionJournalID = this.Columns["TransactionJournalID"];
			_columnTransactionID = this.Columns["TransactionID"];
			_columnDeviceTime = this.Columns["DeviceTime"];
			_columnBoardingTime = this.Columns["BoardingTime"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnTransactionJournalClientID = this.Columns["TransactionJournalClientID"];
			_columnLine = this.Columns["Line"];
			_columnCourseNumber = this.Columns["CourseNumber"];
			_columnRouteNumber = this.Columns["RouteNumber"];
			_columnRouteCode = this.Columns["RouteCode"];
			_columnDirection = this.Columns["Direction"];
			_columnZone = this.Columns["Zone"];
			_columnTransactionJournalSalesChannel = this.Columns["TransactionJournalSalesChannel"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnVehicleNumber = this.Columns["VehicleNumber"];
			_columnMountingPlateNumber = this.Columns["MountingPlateNumber"];
			_columnDebtorNumber = this.Columns["DebtorNumber"];
			_columnGeoLocationLongitude = this.Columns["GeoLocationLongitude"];
			_columnGeoLocationLatitude = this.Columns["GeoLocationLatitude"];
			_columnStopNumber = this.Columns["StopNumber"];
			_columnTransitAccountID = this.Columns["TransitAccountID"];
			_columnFareMediaID = this.Columns["FareMediaID"];
			_columnFareMediaType = this.Columns["FareMediaType"];
			_columnProductID = this.Columns["ProductID"];
			_columnTicketID = this.Columns["TicketID"];
			_columnFareAmount = this.Columns["FareAmount"];
			_columnCustomerGroup = this.Columns["CustomerGroup"];
			_columnTransactionType = this.Columns["TransactionType"];
			_columnTransactionNumber = this.Columns["TransactionNumber"];
			_columnResultType = this.Columns["ResultType"];
			_columnFillLevel = this.Columns["FillLevel"];
			_columnPurseBalance = this.Columns["PurseBalance"];
			_columnPurseCredit = this.Columns["PurseCredit"];
			_columnGroupSize = this.Columns["GroupSize"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnDuration = this.Columns["Duration"];
			_columnTariffDate = this.Columns["TariffDate"];
			_columnTariffVersion = this.Columns["TariffVersion"];
			_columnTicketInternalNumber = this.Columns["TicketInternalNumber"];
			_columnWhitelistVersion = this.Columns["WhitelistVersion"];
			_columnCancellationReference = this.Columns["CancellationReference"];
			_columnCancellationReferenceGuid = this.Columns["CancellationReferenceGuid"];
			_columnWhitelistVersionCreated = this.Columns["WhitelistVersionCreated"];
			_columnProperties = this.Columns["Properties"];
			_columnTransactionJournalSaleID = this.Columns["TransactionJournalSaleID"];
			_columnTripTicketInternalNumber = this.Columns["TripTicketInternalNumber"];
			_columnShiftBegin = this.Columns["ShiftBegin"];
			_columnCompletionState = this.Columns["CompletionState"];
			_columnTransportState = this.Columns["TransportState"];
			_columnProductID2 = this.Columns["ProductID2"];
			_columnPurseBalance2 = this.Columns["PurseBalance2"];
			_columnPurseCredit2 = this.Columns["PurseCredit2"];
			_columnCreditCardAuthorizationID = this.Columns["CreditCardAuthorizationID"];
			_columnTripCounter = this.Columns["TripCounter"];
			_columnInsertTime = this.Columns["InsertTime"];
			_columnSaleTransactionID = this.Columns["SaleTransactionID"];
			_columnSaleType = this.Columns["SaleType"];
			_columnSaleDate = this.Columns["SaleDate"];
			_columnSaleClientID = this.Columns["SaleClientID"];
			_columnSaleSalesChannel = this.Columns["SaleSalesChannel"];
			_columnLocationNumber = this.Columns["LocationNumber"];
			_columnSaleDeviceNumber = this.Columns["SaleDeviceNumber"];
			_columnSalesPersonNumber = this.Columns["SalesPersonNumber"];
			_columnMerchantNumber = this.Columns["MerchantNumber"];
			_columnReceiptReference = this.Columns["ReceiptReference"];
			_columnNetworkReference = this.Columns["NetworkReference"];
			_columnCancellationReferenceID = this.Columns["CancellationReferenceID"];
			_columnIsClosed = this.Columns["IsClosed"];
			_columnOrderID = this.Columns["OrderID"];
			_columnExternalOrderNumber = this.Columns["ExternalOrderNumber"];
			_columnSaleContractID = this.Columns["SaleContractID"];
			_columnRefundReferenceID = this.Columns["RefundReferenceID"];
			_columnIsOrganizational = this.Columns["IsOrganizational"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			PaymentJournalDetailTypedView cloneToReturn = ((PaymentJournalDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new PaymentJournalDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return PaymentJournalDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return PaymentJournalDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentJournalID'</summary>
		internal DataColumn PaymentJournalIDColumn 
		{
			get { return _columnPaymentJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentJournalTransactionJournalID'</summary>
		internal DataColumn PaymentJournalTransactionJournalIDColumn 
		{
			get { return _columnPaymentJournalTransactionJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentJournalSaleID'</summary>
		internal DataColumn PaymentJournalSaleIDColumn 
		{
			get { return _columnPaymentJournalSaleID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentType'</summary>
		internal DataColumn PaymentTypeColumn 
		{
			get { return _columnPaymentType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentJournalCreditcardNetwork'</summary>
		internal DataColumn PaymentJournalCreditcardNetworkColumn 
		{
			get { return _columnPaymentJournalCreditcardNetwork; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Amount'</summary>
		internal DataColumn AmountColumn 
		{
			get { return _columnAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Code'</summary>
		internal DataColumn CodeColumn 
		{
			get { return _columnCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Confirmed'</summary>
		internal DataColumn ConfirmedColumn 
		{
			get { return _columnConfirmed; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExecutionTime'</summary>
		internal DataColumn ExecutionTimeColumn 
		{
			get { return _columnExecutionTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExecutionResult'</summary>
		internal DataColumn ExecutionResultColumn 
		{
			get { return _columnExecutionResult; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Token'</summary>
		internal DataColumn TokenColumn 
		{
			get { return _columnToken; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsPretax'</summary>
		internal DataColumn IsPretaxColumn 
		{
			get { return _columnIsPretax; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReferenceNumber'</summary>
		internal DataColumn ReferenceNumberColumn 
		{
			get { return _columnReferenceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionID'</summary>
		internal DataColumn PaymentOptionIDColumn 
		{
			get { return _columnPaymentOptionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MaskedPan'</summary>
		internal DataColumn MaskedPanColumn 
		{
			get { return _columnMaskedPan; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProviderReference'</summary>
		internal DataColumn ProviderReferenceColumn 
		{
			get { return _columnProviderReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReconciliationState'</summary>
		internal DataColumn ReconciliationStateColumn 
		{
			get { return _columnReconciliationState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionContractID'</summary>
		internal DataColumn PaymentOptionContractIDColumn 
		{
			get { return _columnPaymentOptionContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentMethod'</summary>
		internal DataColumn PaymentMethodColumn 
		{
			get { return _columnPaymentMethod; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionDescription'</summary>
		internal DataColumn PaymentOptionDescriptionColumn 
		{
			get { return _columnPaymentOptionDescription; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankingServiceToken'</summary>
		internal DataColumn BankingServiceTokenColumn 
		{
			get { return _columnBankingServiceToken; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionExpiry'</summary>
		internal DataColumn PaymentOptionExpiryColumn 
		{
			get { return _columnPaymentOptionExpiry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionCreditcardNetwork'</summary>
		internal DataColumn PaymentOptionCreditcardNetworkColumn 
		{
			get { return _columnPaymentOptionCreditcardNetwork; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionPriority'</summary>
		internal DataColumn PaymentOptionPriorityColumn 
		{
			get { return _columnPaymentOptionPriority; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionName'</summary>
		internal DataColumn PaymentOptionNameColumn 
		{
			get { return _columnPaymentOptionName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsTemporary'</summary>
		internal DataColumn IsTemporaryColumn 
		{
			get { return _columnIsTemporary; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionIsRetired'</summary>
		internal DataColumn PaymentOptionIsRetiredColumn 
		{
			get { return _columnPaymentOptionIsRetired; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankConnectionDataID'</summary>
		internal DataColumn BankConnectionDataIDColumn 
		{
			get { return _columnBankConnectionDataID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProspectivePaymentOptionID'</summary>
		internal DataColumn ProspectivePaymentOptionIDColumn 
		{
			get { return _columnProspectivePaymentOptionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionIsBlocked'</summary>
		internal DataColumn PaymentOptionIsBlockedColumn 
		{
			get { return _columnPaymentOptionIsBlocked; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalID'</summary>
		internal DataColumn TransactionJournalIDColumn 
		{
			get { return _columnTransactionJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionID'</summary>
		internal DataColumn TransactionIDColumn 
		{
			get { return _columnTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceTime'</summary>
		internal DataColumn DeviceTimeColumn 
		{
			get { return _columnDeviceTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BoardingTime'</summary>
		internal DataColumn BoardingTimeColumn 
		{
			get { return _columnBoardingTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalClientID'</summary>
		internal DataColumn TransactionJournalClientIDColumn 
		{
			get { return _columnTransactionJournalClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Line'</summary>
		internal DataColumn LineColumn 
		{
			get { return _columnLine; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CourseNumber'</summary>
		internal DataColumn CourseNumberColumn 
		{
			get { return _columnCourseNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RouteNumber'</summary>
		internal DataColumn RouteNumberColumn 
		{
			get { return _columnRouteNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RouteCode'</summary>
		internal DataColumn RouteCodeColumn 
		{
			get { return _columnRouteCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Direction'</summary>
		internal DataColumn DirectionColumn 
		{
			get { return _columnDirection; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Zone'</summary>
		internal DataColumn ZoneColumn 
		{
			get { return _columnZone; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalSalesChannel'</summary>
		internal DataColumn TransactionJournalSalesChannelColumn 
		{
			get { return _columnTransactionJournalSalesChannel; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'VehicleNumber'</summary>
		internal DataColumn VehicleNumberColumn 
		{
			get { return _columnVehicleNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MountingPlateNumber'</summary>
		internal DataColumn MountingPlateNumberColumn 
		{
			get { return _columnMountingPlateNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebtorNumber'</summary>
		internal DataColumn DebtorNumberColumn 
		{
			get { return _columnDebtorNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GeoLocationLongitude'</summary>
		internal DataColumn GeoLocationLongitudeColumn 
		{
			get { return _columnGeoLocationLongitude; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GeoLocationLatitude'</summary>
		internal DataColumn GeoLocationLatitudeColumn 
		{
			get { return _columnGeoLocationLatitude; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopNumber'</summary>
		internal DataColumn StopNumberColumn 
		{
			get { return _columnStopNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransitAccountID'</summary>
		internal DataColumn TransitAccountIDColumn 
		{
			get { return _columnTransitAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaID'</summary>
		internal DataColumn FareMediaIDColumn 
		{
			get { return _columnFareMediaID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaType'</summary>
		internal DataColumn FareMediaTypeColumn 
		{
			get { return _columnFareMediaType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareAmount'</summary>
		internal DataColumn FareAmountColumn 
		{
			get { return _columnFareAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CustomerGroup'</summary>
		internal DataColumn CustomerGroupColumn 
		{
			get { return _columnCustomerGroup; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionType'</summary>
		internal DataColumn TransactionTypeColumn 
		{
			get { return _columnTransactionType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionNumber'</summary>
		internal DataColumn TransactionNumberColumn 
		{
			get { return _columnTransactionNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ResultType'</summary>
		internal DataColumn ResultTypeColumn 
		{
			get { return _columnResultType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FillLevel'</summary>
		internal DataColumn FillLevelColumn 
		{
			get { return _columnFillLevel; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance'</summary>
		internal DataColumn PurseBalanceColumn 
		{
			get { return _columnPurseBalance; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit'</summary>
		internal DataColumn PurseCreditColumn 
		{
			get { return _columnPurseCredit; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'GroupSize'</summary>
		internal DataColumn GroupSizeColumn 
		{
			get { return _columnGroupSize; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Duration'</summary>
		internal DataColumn DurationColumn 
		{
			get { return _columnDuration; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffDate'</summary>
		internal DataColumn TariffDateColumn 
		{
			get { return _columnTariffDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffVersion'</summary>
		internal DataColumn TariffVersionColumn 
		{
			get { return _columnTariffVersion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketInternalNumber'</summary>
		internal DataColumn TicketInternalNumberColumn 
		{
			get { return _columnTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'WhitelistVersion'</summary>
		internal DataColumn WhitelistVersionColumn 
		{
			get { return _columnWhitelistVersion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReference'</summary>
		internal DataColumn CancellationReferenceColumn 
		{
			get { return _columnCancellationReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReferenceGuid'</summary>
		internal DataColumn CancellationReferenceGuidColumn 
		{
			get { return _columnCancellationReferenceGuid; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'WhitelistVersionCreated'</summary>
		internal DataColumn WhitelistVersionCreatedColumn 
		{
			get { return _columnWhitelistVersionCreated; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Properties'</summary>
		internal DataColumn PropertiesColumn 
		{
			get { return _columnProperties; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionJournalSaleID'</summary>
		internal DataColumn TransactionJournalSaleIDColumn 
		{
			get { return _columnTransactionJournalSaleID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripTicketInternalNumber'</summary>
		internal DataColumn TripTicketInternalNumberColumn 
		{
			get { return _columnTripTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ShiftBegin'</summary>
		internal DataColumn ShiftBeginColumn 
		{
			get { return _columnShiftBegin; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CompletionState'</summary>
		internal DataColumn CompletionStateColumn 
		{
			get { return _columnCompletionState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransportState'</summary>
		internal DataColumn TransportStateColumn 
		{
			get { return _columnTransportState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID2'</summary>
		internal DataColumn ProductID2Column 
		{
			get { return _columnProductID2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseBalance2'</summary>
		internal DataColumn PurseBalance2Column 
		{
			get { return _columnPurseBalance2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PurseCredit2'</summary>
		internal DataColumn PurseCredit2Column 
		{
			get { return _columnPurseCredit2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreditCardAuthorizationID'</summary>
		internal DataColumn CreditCardAuthorizationIDColumn 
		{
			get { return _columnCreditCardAuthorizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TripCounter'</summary>
		internal DataColumn TripCounterColumn 
		{
			get { return _columnTripCounter; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'InsertTime'</summary>
		internal DataColumn InsertTimeColumn 
		{
			get { return _columnInsertTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleTransactionID'</summary>
		internal DataColumn SaleTransactionIDColumn 
		{
			get { return _columnSaleTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleType'</summary>
		internal DataColumn SaleTypeColumn 
		{
			get { return _columnSaleType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleDate'</summary>
		internal DataColumn SaleDateColumn 
		{
			get { return _columnSaleDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleClientID'</summary>
		internal DataColumn SaleClientIDColumn 
		{
			get { return _columnSaleClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleSalesChannel'</summary>
		internal DataColumn SaleSalesChannelColumn 
		{
			get { return _columnSaleSalesChannel; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LocationNumber'</summary>
		internal DataColumn LocationNumberColumn 
		{
			get { return _columnLocationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleDeviceNumber'</summary>
		internal DataColumn SaleDeviceNumberColumn 
		{
			get { return _columnSaleDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesPersonNumber'</summary>
		internal DataColumn SalesPersonNumberColumn 
		{
			get { return _columnSalesPersonNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MerchantNumber'</summary>
		internal DataColumn MerchantNumberColumn 
		{
			get { return _columnMerchantNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReceiptReference'</summary>
		internal DataColumn ReceiptReferenceColumn 
		{
			get { return _columnReceiptReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NetworkReference'</summary>
		internal DataColumn NetworkReferenceColumn 
		{
			get { return _columnNetworkReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CancellationReferenceID'</summary>
		internal DataColumn CancellationReferenceIDColumn 
		{
			get { return _columnCancellationReferenceID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsClosed'</summary>
		internal DataColumn IsClosedColumn 
		{
			get { return _columnIsClosed; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderID'</summary>
		internal DataColumn OrderIDColumn 
		{
			get { return _columnOrderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExternalOrderNumber'</summary>
		internal DataColumn ExternalOrderNumberColumn 
		{
			get { return _columnExternalOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleContractID'</summary>
		internal DataColumn SaleContractIDColumn 
		{
			get { return _columnSaleContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RefundReferenceID'</summary>
		internal DataColumn RefundReferenceIDColumn 
		{
			get { return _columnRefundReferenceID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsOrganizational'</summary>
		internal DataColumn IsOrganizationalColumn 
		{
			get { return _columnIsOrganizational; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable PaymentJournalDetail</summary>
	public partial class PaymentJournalDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private PaymentJournalDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal PaymentJournalDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((PaymentJournalDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field PaymentJournalID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentJournalID
		{
			get { return IsPaymentJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentJournalIDColumn]; }
			set { this[_parent.PaymentJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentJournalID is NULL, false otherwise.</summary>
		public bool IsPaymentJournalIDNull() 
		{
			return IsNull(_parent.PaymentJournalIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentJournalIDNull() 
		{
			this[_parent.PaymentJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentJournalTransactionJournalID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMJOURNTRANSJOURN"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentJournalTransactionJournalID
		{
			get { return IsPaymentJournalTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentJournalTransactionJournalIDColumn]; }
			set { this[_parent.PaymentJournalTransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentJournalTransactionJournalID is NULL, false otherwise.</summary>
		public bool IsPaymentJournalTransactionJournalIDNull() 
		{
			return IsNull(_parent.PaymentJournalTransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentJournalTransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentJournalTransactionJournalIDNull() 
		{
			this[_parent.PaymentJournalTransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentJournalSaleID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTJOURNALSALEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentJournalSaleID
		{
			get { return IsPaymentJournalSaleIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentJournalSaleIDColumn]; }
			set { this[_parent.PaymentJournalSaleIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentJournalSaleID is NULL, false otherwise.</summary>
		public bool IsPaymentJournalSaleIDNull() 
		{
			return IsNull(_parent.PaymentJournalSaleIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentJournalSaleID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentJournalSaleIDNull() 
		{
			this[_parent.PaymentJournalSaleIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.DevicePaymentMethod PaymentType
		{
			get { return IsPaymentTypeNull() ? (VarioSL.Entities.Enumerations.DevicePaymentMethod)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.DevicePaymentMethod)) : (VarioSL.Entities.Enumerations.DevicePaymentMethod)this[_parent.PaymentTypeColumn]; }
			set { this[_parent.PaymentTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentType is NULL, false otherwise.</summary>
		public bool IsPaymentTypeNull() 
		{
			return IsNull(_parent.PaymentTypeColumn);
		}

		/// <summary>Sets the TypedView field PaymentType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentTypeNull() 
		{
			this[_parent.PaymentTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentJournalCreditcardNetwork</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTJOURNALCREDITCARDNET"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.CreditCardNetwork PaymentJournalCreditcardNetwork
		{
			get { return IsPaymentJournalCreditcardNetworkNull() ? (VarioSL.Entities.Enumerations.CreditCardNetwork)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.CreditCardNetwork)) : (VarioSL.Entities.Enumerations.CreditCardNetwork)this[_parent.PaymentJournalCreditcardNetworkColumn]; }
			set { this[_parent.PaymentJournalCreditcardNetworkColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentJournalCreditcardNetwork is NULL, false otherwise.</summary>
		public bool IsPaymentJournalCreditcardNetworkNull() 
		{
			return IsNull(_parent.PaymentJournalCreditcardNetworkColumn);
		}

		/// <summary>Sets the TypedView field PaymentJournalCreditcardNetwork to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentJournalCreditcardNetworkNull() 
		{
			this[_parent.PaymentJournalCreditcardNetworkColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Amount</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."AMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Amount
		{
			get { return IsAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.AmountColumn]; }
			set { this[_parent.AmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Amount is NULL, false otherwise.</summary>
		public bool IsAmountNull() 
		{
			return IsNull(_parent.AmountColumn);
		}

		/// <summary>Sets the TypedView field Amount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAmountNull() 
		{
			this[_parent.AmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Code</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200</remarks>
		public System.String Code
		{
			get { return IsCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CodeColumn]; }
			set { this[_parent.CodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Code is NULL, false otherwise.</summary>
		public bool IsCodeNull() 
		{
			return IsNull(_parent.CodeColumn);
		}

		/// <summary>Sets the TypedView field Code to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCodeNull() 
		{
			this[_parent.CodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Confirmed</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CONFIRMED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 Confirmed
		{
			get { return IsConfirmedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.ConfirmedColumn]; }
			set { this[_parent.ConfirmedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Confirmed is NULL, false otherwise.</summary>
		public bool IsConfirmedNull() 
		{
			return IsNull(_parent.ConfirmedColumn);
		}

		/// <summary>Sets the TypedView field Confirmed to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetConfirmedNull() 
		{
			this[_parent.ConfirmedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExecutionTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."EXECUTIONTIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ExecutionTime
		{
			get { return IsExecutionTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ExecutionTimeColumn]; }
			set { this[_parent.ExecutionTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExecutionTime is NULL, false otherwise.</summary>
		public bool IsExecutionTimeNull() 
		{
			return IsNull(_parent.ExecutionTimeColumn);
		}

		/// <summary>Sets the TypedView field ExecutionTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExecutionTimeNull() 
		{
			this[_parent.ExecutionTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExecutionResult</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."EXECUTIONRESULT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 150</remarks>
		public System.String ExecutionResult
		{
			get { return IsExecutionResultNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExecutionResultColumn]; }
			set { this[_parent.ExecutionResultColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExecutionResult is NULL, false otherwise.</summary>
		public bool IsExecutionResultNull() 
		{
			return IsNull(_parent.ExecutionResultColumn);
		}

		/// <summary>Sets the TypedView field ExecutionResult to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExecutionResultNull() 
		{
			this[_parent.ExecutionResultColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 State
		{
			get { return IsStateNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Token</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TOKEN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String Token
		{
			get { return IsTokenNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TokenColumn]; }
			set { this[_parent.TokenColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Token is NULL, false otherwise.</summary>
		public bool IsTokenNull() 
		{
			return IsNull(_parent.TokenColumn);
		}

		/// <summary>Sets the TypedView field Token to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTokenNull() 
		{
			this[_parent.TokenColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsPretax</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ISPRETAX"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsPretax
		{
			get { return IsIsPretaxNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsPretaxColumn]; }
			set { this[_parent.IsPretaxColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsPretax is NULL, false otherwise.</summary>
		public bool IsIsPretaxNull() 
		{
			return IsNull(_parent.IsPretaxColumn);
		}

		/// <summary>Sets the TypedView field IsPretax to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsPretaxNull() 
		{
			this[_parent.IsPretaxColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReferenceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."REFERENCENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 200</remarks>
		public System.String ReferenceNumber
		{
			get { return IsReferenceNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReferenceNumberColumn]; }
			set { this[_parent.ReferenceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReferenceNumber is NULL, false otherwise.</summary>
		public bool IsReferenceNumberNull() 
		{
			return IsNull(_parent.ReferenceNumberColumn);
		}

		/// <summary>Sets the TypedView field ReferenceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReferenceNumberNull() 
		{
			this[_parent.ReferenceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentOptionID
		{
			get { return IsPaymentOptionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentOptionIDColumn]; }
			set { this[_parent.PaymentOptionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionID is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIDNull() 
		{
			return IsNull(_parent.PaymentOptionIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIDNull() 
		{
			this[_parent.PaymentOptionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MaskedPan</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."MASKEDPAN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String MaskedPan
		{
			get { return IsMaskedPanNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.MaskedPanColumn]; }
			set { this[_parent.MaskedPanColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MaskedPan is NULL, false otherwise.</summary>
		public bool IsMaskedPanNull() 
		{
			return IsNull(_parent.MaskedPanColumn);
		}

		/// <summary>Sets the TypedView field MaskedPan to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMaskedPanNull() 
		{
			this[_parent.MaskedPanColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProviderReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PROVIDERREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ProviderReference
		{
			get { return IsProviderReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ProviderReferenceColumn]; }
			set { this[_parent.ProviderReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProviderReference is NULL, false otherwise.</summary>
		public bool IsProviderReferenceNull() 
		{
			return IsNull(_parent.ProviderReferenceColumn);
		}

		/// <summary>Sets the TypedView field ProviderReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProviderReferenceNull() 
		{
			this[_parent.ProviderReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReconciliationState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."RECONCILIATIONSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.PaymentReconciliationState ReconciliationState
		{
			get { return IsReconciliationStateNull() ? (VarioSL.Entities.Enumerations.PaymentReconciliationState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.PaymentReconciliationState)) : (VarioSL.Entities.Enumerations.PaymentReconciliationState)this[_parent.ReconciliationStateColumn]; }
			set { this[_parent.ReconciliationStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReconciliationState is NULL, false otherwise.</summary>
		public bool IsReconciliationStateNull() 
		{
			return IsNull(_parent.ReconciliationStateColumn);
		}

		/// <summary>Sets the TypedView field ReconciliationState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReconciliationStateNull() 
		{
			this[_parent.ReconciliationStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionContractID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONCONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentOptionContractID
		{
			get { return IsPaymentOptionContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentOptionContractIDColumn]; }
			set { this[_parent.PaymentOptionContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionContractID is NULL, false otherwise.</summary>
		public bool IsPaymentOptionContractIDNull() 
		{
			return IsNull(_parent.PaymentOptionContractIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionContractIDNull() 
		{
			this[_parent.PaymentOptionContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentMethod</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTMETHOD"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.PaymentMethod PaymentMethod
		{
			get { return IsPaymentMethodNull() ? (VarioSL.Entities.Enumerations.PaymentMethod)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.PaymentMethod)) : (VarioSL.Entities.Enumerations.PaymentMethod)this[_parent.PaymentMethodColumn]; }
			set { this[_parent.PaymentMethodColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentMethod is NULL, false otherwise.</summary>
		public bool IsPaymentMethodNull() 
		{
			return IsNull(_parent.PaymentMethodColumn);
		}

		/// <summary>Sets the TypedView field PaymentMethod to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentMethodNull() 
		{
			this[_parent.PaymentMethodColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionDescription</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONDESC"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PaymentOptionDescription
		{
			get { return IsPaymentOptionDescriptionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionDescriptionColumn]; }
			set { this[_parent.PaymentOptionDescriptionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionDescription is NULL, false otherwise.</summary>
		public bool IsPaymentOptionDescriptionNull() 
		{
			return IsNull(_parent.PaymentOptionDescriptionColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionDescription to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionDescriptionNull() 
		{
			this[_parent.PaymentOptionDescriptionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankingServiceToken</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."BANKINGSERVICETOKEN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String BankingServiceToken
		{
			get { return IsBankingServiceTokenNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.BankingServiceTokenColumn]; }
			set { this[_parent.BankingServiceTokenColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankingServiceToken is NULL, false otherwise.</summary>
		public bool IsBankingServiceTokenNull() 
		{
			return IsNull(_parent.BankingServiceTokenColumn);
		}

		/// <summary>Sets the TypedView field BankingServiceToken to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankingServiceTokenNull() 
		{
			this[_parent.BankingServiceTokenColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionExpiry</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONEXPIRY"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PaymentOptionExpiry
		{
			get { return IsPaymentOptionExpiryNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PaymentOptionExpiryColumn]; }
			set { this[_parent.PaymentOptionExpiryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionExpiry is NULL, false otherwise.</summary>
		public bool IsPaymentOptionExpiryNull() 
		{
			return IsNull(_parent.PaymentOptionExpiryColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionExpiry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionExpiryNull() 
		{
			this[_parent.PaymentOptionExpiryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionCreditcardNetwork</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONCREDITCARDNET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20</remarks>
		public System.String PaymentOptionCreditcardNetwork
		{
			get { return IsPaymentOptionCreditcardNetworkNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionCreditcardNetworkColumn]; }
			set { this[_parent.PaymentOptionCreditcardNetworkColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionCreditcardNetwork is NULL, false otherwise.</summary>
		public bool IsPaymentOptionCreditcardNetworkNull() 
		{
			return IsNull(_parent.PaymentOptionCreditcardNetworkColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionCreditcardNetwork to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionCreditcardNetworkNull() 
		{
			this[_parent.PaymentOptionCreditcardNetworkColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionPriority</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONPRIORITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PaymentOptionPriority
		{
			get { return IsPaymentOptionPriorityNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PaymentOptionPriorityColumn]; }
			set { this[_parent.PaymentOptionPriorityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionPriority is NULL, false otherwise.</summary>
		public bool IsPaymentOptionPriorityNull() 
		{
			return IsNull(_parent.PaymentOptionPriorityColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionPriority to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionPriorityNull() 
		{
			this[_parent.PaymentOptionPriorityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PaymentOptionName
		{
			get { return IsPaymentOptionNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionNameColumn]; }
			set { this[_parent.PaymentOptionNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionName is NULL, false otherwise.</summary>
		public bool IsPaymentOptionNameNull() 
		{
			return IsNull(_parent.PaymentOptionNameColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionNameNull() 
		{
			this[_parent.PaymentOptionNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsTemporary</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ISTEMPORARY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsTemporary
		{
			get { return IsIsTemporaryNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsTemporaryColumn]; }
			set { this[_parent.IsTemporaryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsTemporary is NULL, false otherwise.</summary>
		public bool IsIsTemporaryNull() 
		{
			return IsNull(_parent.IsTemporaryColumn);
		}

		/// <summary>Sets the TypedView field IsTemporary to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsTemporaryNull() 
		{
			this[_parent.IsTemporaryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionIsRetired</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONISRETIRED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 PaymentOptionIsRetired
		{
			get { return IsPaymentOptionIsRetiredNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.PaymentOptionIsRetiredColumn]; }
			set { this[_parent.PaymentOptionIsRetiredColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionIsRetired is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIsRetiredNull() 
		{
			return IsNull(_parent.PaymentOptionIsRetiredColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionIsRetired to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIsRetiredNull() 
		{
			this[_parent.PaymentOptionIsRetiredColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankConnectionDataID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."BANKCONNECTIONDATAID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 BankConnectionDataID
		{
			get { return IsBankConnectionDataIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.BankConnectionDataIDColumn]; }
			set { this[_parent.BankConnectionDataIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankConnectionDataID is NULL, false otherwise.</summary>
		public bool IsBankConnectionDataIDNull() 
		{
			return IsNull(_parent.BankConnectionDataIDColumn);
		}

		/// <summary>Sets the TypedView field BankConnectionDataID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankConnectionDataIDNull() 
		{
			this[_parent.BankConnectionDataIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProspectivePaymentOptionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PROSPECTIVEPAYMENTOPTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProspectivePaymentOptionID
		{
			get { return IsProspectivePaymentOptionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProspectivePaymentOptionIDColumn]; }
			set { this[_parent.ProspectivePaymentOptionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProspectivePaymentOptionID is NULL, false otherwise.</summary>
		public bool IsProspectivePaymentOptionIDNull() 
		{
			return IsNull(_parent.ProspectivePaymentOptionIDColumn);
		}

		/// <summary>Sets the TypedView field ProspectivePaymentOptionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProspectivePaymentOptionIDNull() 
		{
			this[_parent.ProspectivePaymentOptionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionIsBlocked</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PAYMENTOPTIONISBLOCKED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 PaymentOptionIsBlocked
		{
			get { return IsPaymentOptionIsBlockedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.PaymentOptionIsBlockedColumn]; }
			set { this[_parent.PaymentOptionIsBlockedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionIsBlocked is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIsBlockedNull() 
		{
			return IsNull(_parent.PaymentOptionIsBlockedColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionIsBlocked to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIsBlockedNull() 
		{
			this[_parent.PaymentOptionIsBlockedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransactionJournalID
		{
			get { return IsTransactionJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalIDColumn]; }
			set { this[_parent.TransactionJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalID is NULL, false otherwise.</summary>
		public bool IsTransactionJournalIDNull() 
		{
			return IsNull(_parent.TransactionJournalIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalIDNull() 
		{
			this[_parent.TransactionJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String TransactionID
		{
			get { return IsTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TransactionIDColumn]; }
			set { this[_parent.TransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionID is NULL, false otherwise.</summary>
		public bool IsTransactionIDNull() 
		{
			return IsNull(_parent.TransactionIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionIDNull() 
		{
			this[_parent.TransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."DEVICETIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime DeviceTime
		{
			get { return IsDeviceTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.DeviceTimeColumn]; }
			set { this[_parent.DeviceTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceTime is NULL, false otherwise.</summary>
		public bool IsDeviceTimeNull() 
		{
			return IsNull(_parent.DeviceTimeColumn);
		}

		/// <summary>Sets the TypedView field DeviceTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceTimeNull() 
		{
			this[_parent.DeviceTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BoardingTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."BOARDINGTIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime BoardingTime
		{
			get { return IsBoardingTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.BoardingTimeColumn]; }
			set { this[_parent.BoardingTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BoardingTime is NULL, false otherwise.</summary>
		public bool IsBoardingTimeNull() 
		{
			return IsNull(_parent.BoardingTimeColumn);
		}

		/// <summary>Sets the TypedView field BoardingTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBoardingTimeNull() 
		{
			this[_parent.BoardingTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalClientID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONJOURNALCLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TransactionJournalClientID
		{
			get { return IsTransactionJournalClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalClientIDColumn]; }
			set { this[_parent.TransactionJournalClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalClientID is NULL, false otherwise.</summary>
		public bool IsTransactionJournalClientIDNull() 
		{
			return IsNull(_parent.TransactionJournalClientIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalClientIDNull() 
		{
			this[_parent.TransactionJournalClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Line</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."LINE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String Line
		{
			get { return IsLineNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LineColumn]; }
			set { this[_parent.LineColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Line is NULL, false otherwise.</summary>
		public bool IsLineNull() 
		{
			return IsNull(_parent.LineColumn);
		}

		/// <summary>Sets the TypedView field Line to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLineNull() 
		{
			this[_parent.LineColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CourseNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."COURSENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CourseNumber
		{
			get { return IsCourseNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CourseNumberColumn]; }
			set { this[_parent.CourseNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CourseNumber is NULL, false otherwise.</summary>
		public bool IsCourseNumberNull() 
		{
			return IsNull(_parent.CourseNumberColumn);
		}

		/// <summary>Sets the TypedView field CourseNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCourseNumberNull() 
		{
			this[_parent.CourseNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RouteNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ROUTENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 RouteNumber
		{
			get { return IsRouteNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.RouteNumberColumn]; }
			set { this[_parent.RouteNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RouteNumber is NULL, false otherwise.</summary>
		public bool IsRouteNumberNull() 
		{
			return IsNull(_parent.RouteNumberColumn);
		}

		/// <summary>Sets the TypedView field RouteNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRouteNumberNull() 
		{
			this[_parent.RouteNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RouteCode</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ROUTECODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 60</remarks>
		public System.String RouteCode
		{
			get { return IsRouteCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.RouteCodeColumn]; }
			set { this[_parent.RouteCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RouteCode is NULL, false otherwise.</summary>
		public bool IsRouteCodeNull() 
		{
			return IsNull(_parent.RouteCodeColumn);
		}

		/// <summary>Sets the TypedView field RouteCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRouteCodeNull() 
		{
			this[_parent.RouteCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Direction</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."DIRECTION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Direction
		{
			get { return IsDirectionNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DirectionColumn]; }
			set { this[_parent.DirectionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Direction is NULL, false otherwise.</summary>
		public bool IsDirectionNull() 
		{
			return IsNull(_parent.DirectionColumn);
		}

		/// <summary>Sets the TypedView field Direction to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDirectionNull() 
		{
			this[_parent.DirectionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Zone</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ZONE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Zone
		{
			get { return IsZoneNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.ZoneColumn]; }
			set { this[_parent.ZoneColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Zone is NULL, false otherwise.</summary>
		public bool IsZoneNull() 
		{
			return IsNull(_parent.ZoneColumn);
		}

		/// <summary>Sets the TypedView field Zone to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetZoneNull() 
		{
			this[_parent.ZoneColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalSalesChannel</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONJOURNALSALESCHANNEL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransactionJournalSalesChannel
		{
			get { return IsTransactionJournalSalesChannelNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalSalesChannelColumn]; }
			set { this[_parent.TransactionJournalSalesChannelColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalSalesChannel is NULL, false otherwise.</summary>
		public bool IsTransactionJournalSalesChannelNull() 
		{
			return IsNull(_parent.TransactionJournalSalesChannelColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalSalesChannel to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalSalesChannelNull() 
		{
			this[_parent.TransactionJournalSalesChannelColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field VehicleNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."VEHICLENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 VehicleNumber
		{
			get { return IsVehicleNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.VehicleNumberColumn]; }
			set { this[_parent.VehicleNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field VehicleNumber is NULL, false otherwise.</summary>
		public bool IsVehicleNumberNull() 
		{
			return IsNull(_parent.VehicleNumberColumn);
		}

		/// <summary>Sets the TypedView field VehicleNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetVehicleNumberNull() 
		{
			this[_parent.VehicleNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MountingPlateNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."MOUNTINGPLATENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MountingPlateNumber
		{
			get { return IsMountingPlateNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MountingPlateNumberColumn]; }
			set { this[_parent.MountingPlateNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MountingPlateNumber is NULL, false otherwise.</summary>
		public bool IsMountingPlateNumberNull() 
		{
			return IsNull(_parent.MountingPlateNumberColumn);
		}

		/// <summary>Sets the TypedView field MountingPlateNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMountingPlateNumberNull() 
		{
			this[_parent.MountingPlateNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebtorNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."DEBTORNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DebtorNumber
		{
			get { return IsDebtorNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DebtorNumberColumn]; }
			set { this[_parent.DebtorNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebtorNumber is NULL, false otherwise.</summary>
		public bool IsDebtorNumberNull() 
		{
			return IsNull(_parent.DebtorNumberColumn);
		}

		/// <summary>Sets the TypedView field DebtorNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebtorNumberNull() 
		{
			this[_parent.DebtorNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GeoLocationLongitude</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."GEOLOCATIONLONGITUDE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GeoLocationLongitude
		{
			get { return IsGeoLocationLongitudeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GeoLocationLongitudeColumn]; }
			set { this[_parent.GeoLocationLongitudeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GeoLocationLongitude is NULL, false otherwise.</summary>
		public bool IsGeoLocationLongitudeNull() 
		{
			return IsNull(_parent.GeoLocationLongitudeColumn);
		}

		/// <summary>Sets the TypedView field GeoLocationLongitude to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGeoLocationLongitudeNull() 
		{
			this[_parent.GeoLocationLongitudeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GeoLocationLatitude</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."GEOLOCATIONLATITUDE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GeoLocationLatitude
		{
			get { return IsGeoLocationLatitudeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GeoLocationLatitudeColumn]; }
			set { this[_parent.GeoLocationLatitudeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GeoLocationLatitude is NULL, false otherwise.</summary>
		public bool IsGeoLocationLatitudeNull() 
		{
			return IsNull(_parent.GeoLocationLatitudeColumn);
		}

		/// <summary>Sets the TypedView field GeoLocationLatitude to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGeoLocationLatitudeNull() 
		{
			this[_parent.GeoLocationLatitudeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."STOPNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 StopNumber
		{
			get { return IsStopNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.StopNumberColumn]; }
			set { this[_parent.StopNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopNumber is NULL, false otherwise.</summary>
		public bool IsStopNumberNull() 
		{
			return IsNull(_parent.StopNumberColumn);
		}

		/// <summary>Sets the TypedView field StopNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopNumberNull() 
		{
			this[_parent.StopNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransitAccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSITACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransitAccountID
		{
			get { return IsTransitAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransitAccountIDColumn]; }
			set { this[_parent.TransitAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransitAccountID is NULL, false otherwise.</summary>
		public bool IsTransitAccountIDNull() 
		{
			return IsNull(_parent.TransitAccountIDColumn);
		}

		/// <summary>Sets the TypedView field TransitAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransitAccountIDNull() 
		{
			this[_parent.TransitAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."FAREMEDIAID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String FareMediaID
		{
			get { return IsFareMediaIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaIDColumn]; }
			set { this[_parent.FareMediaIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaID is NULL, false otherwise.</summary>
		public bool IsFareMediaIDNull() 
		{
			return IsNull(_parent.FareMediaIDColumn);
		}

		/// <summary>Sets the TypedView field FareMediaID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaIDNull() 
		{
			this[_parent.FareMediaIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."FAREMEDIATYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String FareMediaType
		{
			get { return IsFareMediaTypeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaTypeColumn]; }
			set { this[_parent.FareMediaTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaType is NULL, false otherwise.</summary>
		public bool IsFareMediaTypeNull() 
		{
			return IsNull(_parent.FareMediaTypeColumn);
		}

		/// <summary>Sets the TypedView field FareMediaType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaTypeNull() 
		{
			this[_parent.FareMediaTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareAmount</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."FAREAMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FareAmount
		{
			get { return IsFareAmountNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FareAmountColumn]; }
			set { this[_parent.FareAmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareAmount is NULL, false otherwise.</summary>
		public bool IsFareAmountNull() 
		{
			return IsNull(_parent.FareAmountColumn);
		}

		/// <summary>Sets the TypedView field FareAmount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareAmountNull() 
		{
			this[_parent.FareAmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CustomerGroup</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CUSTOMERGROUP"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String CustomerGroup
		{
			get { return IsCustomerGroupNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CustomerGroupColumn]; }
			set { this[_parent.CustomerGroupColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CustomerGroup is NULL, false otherwise.</summary>
		public bool IsCustomerGroupNull() 
		{
			return IsNull(_parent.CustomerGroupColumn);
		}

		/// <summary>Sets the TypedView field CustomerGroup to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCustomerGroupNull() 
		{
			this[_parent.CustomerGroupColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionType TransactionType
		{
			get { return IsTransactionTypeNull() ? (VarioSL.Entities.Enumerations.TransactionType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionType)) : (VarioSL.Entities.Enumerations.TransactionType)this[_parent.TransactionTypeColumn]; }
			set { this[_parent.TransactionTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionType is NULL, false otherwise.</summary>
		public bool IsTransactionTypeNull() 
		{
			return IsNull(_parent.TransactionTypeColumn);
		}

		/// <summary>Sets the TypedView field TransactionType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionTypeNull() 
		{
			this[_parent.TransactionTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TransactionNumber
		{
			get { return IsTransactionNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TransactionNumberColumn]; }
			set { this[_parent.TransactionNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionNumber is NULL, false otherwise.</summary>
		public bool IsTransactionNumberNull() 
		{
			return IsNull(_parent.TransactionNumberColumn);
		}

		/// <summary>Sets the TypedView field TransactionNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionNumberNull() 
		{
			this[_parent.TransactionNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ResultType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."RESULTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.FarePaymentResultType ResultType
		{
			get { return IsResultTypeNull() ? (VarioSL.Entities.Enumerations.FarePaymentResultType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.FarePaymentResultType)) : (VarioSL.Entities.Enumerations.FarePaymentResultType)this[_parent.ResultTypeColumn]; }
			set { this[_parent.ResultTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ResultType is NULL, false otherwise.</summary>
		public bool IsResultTypeNull() 
		{
			return IsNull(_parent.ResultTypeColumn);
		}

		/// <summary>Sets the TypedView field ResultType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetResultTypeNull() 
		{
			this[_parent.ResultTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FillLevel</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."FILLLEVEL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 FillLevel
		{
			get { return IsFillLevelNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.FillLevelColumn]; }
			set { this[_parent.FillLevelColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FillLevel is NULL, false otherwise.</summary>
		public bool IsFillLevelNull() 
		{
			return IsNull(_parent.FillLevelColumn);
		}

		/// <summary>Sets the TypedView field FillLevel to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFillLevelNull() 
		{
			this[_parent.FillLevelColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PURSEBALANCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance
		{
			get { return IsPurseBalanceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalanceColumn]; }
			set { this[_parent.PurseBalanceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance is NULL, false otherwise.</summary>
		public bool IsPurseBalanceNull() 
		{
			return IsNull(_parent.PurseBalanceColumn);
		}

		/// <summary>Sets the TypedView field PurseBalance to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalanceNull() 
		{
			this[_parent.PurseBalanceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PURSECREDIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit
		{
			get { return IsPurseCreditNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCreditColumn]; }
			set { this[_parent.PurseCreditColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit is NULL, false otherwise.</summary>
		public bool IsPurseCreditNull() 
		{
			return IsNull(_parent.PurseCreditColumn);
		}

		/// <summary>Sets the TypedView field PurseCredit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCreditNull() 
		{
			this[_parent.PurseCreditColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field GroupSize</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."GROUPSIZE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 GroupSize
		{
			get { return IsGroupSizeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.GroupSizeColumn]; }
			set { this[_parent.GroupSizeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field GroupSize is NULL, false otherwise.</summary>
		public bool IsGroupSizeNull() 
		{
			return IsNull(_parent.GroupSizeColumn);
		}

		/// <summary>Sets the TypedView field GroupSize to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetGroupSizeNull() 
		{
			this[_parent.GroupSizeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Duration</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."DURATION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Duration
		{
			get { return IsDurationNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.DurationColumn]; }
			set { this[_parent.DurationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Duration is NULL, false otherwise.</summary>
		public bool IsDurationNull() 
		{
			return IsNull(_parent.DurationColumn);
		}

		/// <summary>Sets the TypedView field Duration to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDurationNull() 
		{
			this[_parent.DurationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TARIFFDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime TariffDate
		{
			get { return IsTariffDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.TariffDateColumn]; }
			set { this[_parent.TariffDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffDate is NULL, false otherwise.</summary>
		public bool IsTariffDateNull() 
		{
			return IsNull(_parent.TariffDateColumn);
		}

		/// <summary>Sets the TypedView field TariffDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffDateNull() 
		{
			this[_parent.TariffDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffVersion</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TARIFFVERSION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TariffVersion
		{
			get { return IsTariffVersionNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TariffVersionColumn]; }
			set { this[_parent.TariffVersionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffVersion is NULL, false otherwise.</summary>
		public bool IsTariffVersionNull() 
		{
			return IsNull(_parent.TariffVersionColumn);
		}

		/// <summary>Sets the TypedView field TariffVersion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffVersionNull() 
		{
			this[_parent.TariffVersionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TicketInternalNumber
		{
			get { return IsTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TicketInternalNumberColumn]; }
			set { this[_parent.TicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTicketInternalNumberNull() 
		{
			return IsNull(_parent.TicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketInternalNumberNull() 
		{
			this[_parent.TicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field WhitelistVersion</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."WHITELISTVERSION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WhitelistVersion
		{
			get { return IsWhitelistVersionNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WhitelistVersionColumn]; }
			set { this[_parent.WhitelistVersionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WhitelistVersion is NULL, false otherwise.</summary>
		public bool IsWhitelistVersionNull() 
		{
			return IsNull(_parent.WhitelistVersionColumn);
		}

		/// <summary>Sets the TypedView field WhitelistVersion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWhitelistVersionNull() 
		{
			this[_parent.WhitelistVersionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CANCELLATIONREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CancellationReference
		{
			get { return IsCancellationReferenceNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CancellationReferenceColumn]; }
			set { this[_parent.CancellationReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReference is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceNull() 
		{
			return IsNull(_parent.CancellationReferenceColumn);
		}

		/// <summary>Sets the TypedView field CancellationReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceNull() 
		{
			this[_parent.CancellationReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReferenceGuid</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CANCELLATIONREFERENCEGUID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CancellationReferenceGuid
		{
			get { return IsCancellationReferenceGuidNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CancellationReferenceGuidColumn]; }
			set { this[_parent.CancellationReferenceGuidColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReferenceGuid is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceGuidNull() 
		{
			return IsNull(_parent.CancellationReferenceGuidColumn);
		}

		/// <summary>Sets the TypedView field CancellationReferenceGuid to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceGuidNull() 
		{
			this[_parent.CancellationReferenceGuidColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field WhitelistVersionCreated</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."WHITELISTVERSIONCREATED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WhitelistVersionCreated
		{
			get { return IsWhitelistVersionCreatedNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WhitelistVersionCreatedColumn]; }
			set { this[_parent.WhitelistVersionCreatedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WhitelistVersionCreated is NULL, false otherwise.</summary>
		public bool IsWhitelistVersionCreatedNull() 
		{
			return IsNull(_parent.WhitelistVersionCreatedColumn);
		}

		/// <summary>Sets the TypedView field WhitelistVersionCreated to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWhitelistVersionCreatedNull() 
		{
			this[_parent.WhitelistVersionCreatedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Properties</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PROPERTIES"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionJournalProperties Properties
		{
			get { return IsPropertiesNull() ? (VarioSL.Entities.Enumerations.TransactionJournalProperties)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionJournalProperties)) : (VarioSL.Entities.Enumerations.TransactionJournalProperties)this[_parent.PropertiesColumn]; }
			set { this[_parent.PropertiesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Properties is NULL, false otherwise.</summary>
		public bool IsPropertiesNull() 
		{
			return IsNull(_parent.PropertiesColumn);
		}

		/// <summary>Sets the TypedView field Properties to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPropertiesNull() 
		{
			this[_parent.PropertiesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionJournalSaleID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRANSACTIONJOURNALSALEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TransactionJournalSaleID
		{
			get { return IsTransactionJournalSaleIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TransactionJournalSaleIDColumn]; }
			set { this[_parent.TransactionJournalSaleIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionJournalSaleID is NULL, false otherwise.</summary>
		public bool IsTransactionJournalSaleIDNull() 
		{
			return IsNull(_parent.TransactionJournalSaleIDColumn);
		}

		/// <summary>Sets the TypedView field TransactionJournalSaleID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionJournalSaleIDNull() 
		{
			this[_parent.TransactionJournalSaleIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripTicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRIPTICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TripTicketInternalNumber
		{
			get { return IsTripTicketInternalNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TripTicketInternalNumberColumn]; }
			set { this[_parent.TripTicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripTicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTripTicketInternalNumberNull() 
		{
			return IsNull(_parent.TripTicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TripTicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripTicketInternalNumberNull() 
		{
			this[_parent.TripTicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ShiftBegin</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SHIFTBEGIN"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ShiftBegin
		{
			get { return IsShiftBeginNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ShiftBeginColumn]; }
			set { this[_parent.ShiftBeginColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ShiftBegin is NULL, false otherwise.</summary>
		public bool IsShiftBeginNull() 
		{
			return IsNull(_parent.ShiftBeginColumn);
		}

		/// <summary>Sets the TypedView field ShiftBegin to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetShiftBeginNull() 
		{
			this[_parent.ShiftBeginColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CompletionState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."COMPLETIONSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String CompletionState
		{
			get { return IsCompletionStateNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CompletionStateColumn]; }
			set { this[_parent.CompletionStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CompletionState is NULL, false otherwise.</summary>
		public bool IsCompletionStateNull() 
		{
			return IsNull(_parent.CompletionStateColumn);
		}

		/// <summary>Sets the TypedView field CompletionState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCompletionStateNull() 
		{
			this[_parent.CompletionStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransportState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."RESPONSESTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TransactionTransportState TransportState
		{
			get { return IsTransportStateNull() ? (VarioSL.Entities.Enumerations.TransactionTransportState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TransactionTransportState)) : (VarioSL.Entities.Enumerations.TransactionTransportState)this[_parent.TransportStateColumn]; }
			set { this[_parent.TransportStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransportState is NULL, false otherwise.</summary>
		public bool IsTransportStateNull() 
		{
			return IsNull(_parent.TransportStateColumn);
		}

		/// <summary>Sets the TypedView field TransportState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransportStateNull() 
		{
			this[_parent.TransportStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PRODUCTID2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID2
		{
			get { return IsProductID2Null() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductID2Column]; }
			set { this[_parent.ProductID2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID2 is NULL, false otherwise.</summary>
		public bool IsProductID2Null() 
		{
			return IsNull(_parent.ProductID2Column);
		}

		/// <summary>Sets the TypedView field ProductID2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductID2Null() 
		{
			this[_parent.ProductID2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseBalance2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PURSEBALANCE2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseBalance2
		{
			get { return IsPurseBalance2Null() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseBalance2Column]; }
			set { this[_parent.PurseBalance2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseBalance2 is NULL, false otherwise.</summary>
		public bool IsPurseBalance2Null() 
		{
			return IsNull(_parent.PurseBalance2Column);
		}

		/// <summary>Sets the TypedView field PurseBalance2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseBalance2Null() 
		{
			this[_parent.PurseBalance2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PurseCredit2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."PURSECREDIT2"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PurseCredit2
		{
			get { return IsPurseCredit2Null() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PurseCredit2Column]; }
			set { this[_parent.PurseCredit2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field PurseCredit2 is NULL, false otherwise.</summary>
		public bool IsPurseCredit2Null() 
		{
			return IsNull(_parent.PurseCredit2Column);
		}

		/// <summary>Sets the TypedView field PurseCredit2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPurseCredit2Null() 
		{
			this[_parent.PurseCredit2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreditCardAuthorizationID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CREDITCARDAUTHORIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CreditCardAuthorizationID
		{
			get { return IsCreditCardAuthorizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CreditCardAuthorizationIDColumn]; }
			set { this[_parent.CreditCardAuthorizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreditCardAuthorizationID is NULL, false otherwise.</summary>
		public bool IsCreditCardAuthorizationIDNull() 
		{
			return IsNull(_parent.CreditCardAuthorizationIDColumn);
		}

		/// <summary>Sets the TypedView field CreditCardAuthorizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreditCardAuthorizationIDNull() 
		{
			this[_parent.CreditCardAuthorizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TripCounter</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."TRIPCOUNTER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 TripCounter
		{
			get { return IsTripCounterNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.TripCounterColumn]; }
			set { this[_parent.TripCounterColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TripCounter is NULL, false otherwise.</summary>
		public bool IsTripCounterNull() 
		{
			return IsNull(_parent.TripCounterColumn);
		}

		/// <summary>Sets the TypedView field TripCounter to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTripCounterNull() 
		{
			this[_parent.TripCounterColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field InsertTime</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."INSERTTIME"<br/>
		/// View field characteristics (type, precision, scale, length): TimeStamp, 0, 0, 0</remarks>
		public System.DateTime InsertTime
		{
			get { return IsInsertTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.InsertTimeColumn]; }
			set { this[_parent.InsertTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field InsertTime is NULL, false otherwise.</summary>
		public bool IsInsertTimeNull() 
		{
			return IsNull(_parent.InsertTimeColumn);
		}

		/// <summary>Sets the TypedView field InsertTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetInsertTimeNull() 
		{
			this[_parent.InsertTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleTransactionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALETRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String SaleTransactionID
		{
			get { return IsSaleTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SaleTransactionIDColumn]; }
			set { this[_parent.SaleTransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleTransactionID is NULL, false otherwise.</summary>
		public bool IsSaleTransactionIDNull() 
		{
			return IsNull(_parent.SaleTransactionIDColumn);
		}

		/// <summary>Sets the TypedView field SaleTransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTransactionIDNull() 
		{
			this[_parent.SaleTransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.SaleType SaleType
		{
			get { return IsSaleTypeNull() ? (VarioSL.Entities.Enumerations.SaleType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.SaleType)) : (VarioSL.Entities.Enumerations.SaleType)this[_parent.SaleTypeColumn]; }
			set { this[_parent.SaleTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleType is NULL, false otherwise.</summary>
		public bool IsSaleTypeNull() 
		{
			return IsNull(_parent.SaleTypeColumn);
		}

		/// <summary>Sets the TypedView field SaleType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTypeNull() 
		{
			this[_parent.SaleTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALEDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime SaleDate
		{
			get { return IsSaleDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.SaleDateColumn]; }
			set { this[_parent.SaleDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleDate is NULL, false otherwise.</summary>
		public bool IsSaleDateNull() 
		{
			return IsNull(_parent.SaleDateColumn);
		}

		/// <summary>Sets the TypedView field SaleDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleDateNull() 
		{
			this[_parent.SaleDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleClientID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALECLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SaleClientID
		{
			get { return IsSaleClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SaleClientIDColumn]; }
			set { this[_parent.SaleClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleClientID is NULL, false otherwise.</summary>
		public bool IsSaleClientIDNull() 
		{
			return IsNull(_parent.SaleClientIDColumn);
		}

		/// <summary>Sets the TypedView field SaleClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleClientIDNull() 
		{
			this[_parent.SaleClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleSalesChannel</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALESALESCHANNEL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SaleSalesChannel
		{
			get { return IsSaleSalesChannelNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SaleSalesChannelColumn]; }
			set { this[_parent.SaleSalesChannelColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleSalesChannel is NULL, false otherwise.</summary>
		public bool IsSaleSalesChannelNull() 
		{
			return IsNull(_parent.SaleSalesChannelColumn);
		}

		/// <summary>Sets the TypedView field SaleSalesChannel to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleSalesChannelNull() 
		{
			this[_parent.SaleSalesChannelColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LocationNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."LOCATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 LocationNumber
		{
			get { return IsLocationNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.LocationNumberColumn]; }
			set { this[_parent.LocationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LocationNumber is NULL, false otherwise.</summary>
		public bool IsLocationNumberNull() 
		{
			return IsNull(_parent.LocationNumberColumn);
		}

		/// <summary>Sets the TypedView field LocationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNumberNull() 
		{
			this[_parent.LocationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleDeviceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALEDEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SaleDeviceNumber
		{
			get { return IsSaleDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SaleDeviceNumberColumn]; }
			set { this[_parent.SaleDeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleDeviceNumber is NULL, false otherwise.</summary>
		public bool IsSaleDeviceNumberNull() 
		{
			return IsNull(_parent.SaleDeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field SaleDeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleDeviceNumberNull() 
		{
			this[_parent.SaleDeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesPersonNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALESPERSONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SalesPersonNumber
		{
			get { return IsSalesPersonNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SalesPersonNumberColumn]; }
			set { this[_parent.SalesPersonNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesPersonNumber is NULL, false otherwise.</summary>
		public bool IsSalesPersonNumberNull() 
		{
			return IsNull(_parent.SalesPersonNumberColumn);
		}

		/// <summary>Sets the TypedView field SalesPersonNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesPersonNumberNull() 
		{
			this[_parent.SalesPersonNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MerchantNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."MERCHANTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MerchantNumber
		{
			get { return IsMerchantNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MerchantNumberColumn]; }
			set { this[_parent.MerchantNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MerchantNumber is NULL, false otherwise.</summary>
		public bool IsMerchantNumberNull() 
		{
			return IsNull(_parent.MerchantNumberColumn);
		}

		/// <summary>Sets the TypedView field MerchantNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMerchantNumberNull() 
		{
			this[_parent.MerchantNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReceiptReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."RECEIPTREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String ReceiptReference
		{
			get { return IsReceiptReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReceiptReferenceColumn]; }
			set { this[_parent.ReceiptReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReceiptReference is NULL, false otherwise.</summary>
		public bool IsReceiptReferenceNull() 
		{
			return IsNull(_parent.ReceiptReferenceColumn);
		}

		/// <summary>Sets the TypedView field ReceiptReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReceiptReferenceNull() 
		{
			this[_parent.ReceiptReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NetworkReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."NETWORKREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String NetworkReference
		{
			get { return IsNetworkReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NetworkReferenceColumn]; }
			set { this[_parent.NetworkReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NetworkReference is NULL, false otherwise.</summary>
		public bool IsNetworkReferenceNull() 
		{
			return IsNull(_parent.NetworkReferenceColumn);
		}

		/// <summary>Sets the TypedView field NetworkReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNetworkReferenceNull() 
		{
			this[_parent.NetworkReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CancellationReferenceID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."CANCELLATIONREFERENCEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CancellationReferenceID
		{
			get { return IsCancellationReferenceIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CancellationReferenceIDColumn]; }
			set { this[_parent.CancellationReferenceIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CancellationReferenceID is NULL, false otherwise.</summary>
		public bool IsCancellationReferenceIDNull() 
		{
			return IsNull(_parent.CancellationReferenceIDColumn);
		}

		/// <summary>Sets the TypedView field CancellationReferenceID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCancellationReferenceIDNull() 
		{
			this[_parent.CancellationReferenceIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsClosed</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ISCLOSED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsClosed
		{
			get { return IsIsClosedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsClosedColumn]; }
			set { this[_parent.IsClosedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsClosed is NULL, false otherwise.</summary>
		public bool IsIsClosedNull() 
		{
			return IsNull(_parent.IsClosedColumn);
		}

		/// <summary>Sets the TypedView field IsClosed to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsClosedNull() 
		{
			this[_parent.IsClosedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ORDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal OrderID
		{
			get { return IsOrderIDNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.OrderIDColumn]; }
			set { this[_parent.OrderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderID is NULL, false otherwise.</summary>
		public bool IsOrderIDNull() 
		{
			return IsNull(_parent.OrderIDColumn);
		}

		/// <summary>Sets the TypedView field OrderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderIDNull() 
		{
			this[_parent.OrderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExternalOrderNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."EXTERNALORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ExternalOrderNumber
		{
			get { return IsExternalOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExternalOrderNumberColumn]; }
			set { this[_parent.ExternalOrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExternalOrderNumber is NULL, false otherwise.</summary>
		public bool IsExternalOrderNumberNull() 
		{
			return IsNull(_parent.ExternalOrderNumberColumn);
		}

		/// <summary>Sets the TypedView field ExternalOrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExternalOrderNumberNull() 
		{
			this[_parent.ExternalOrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleContractID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."SALECONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal SaleContractID
		{
			get { return IsSaleContractIDNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.SaleContractIDColumn]; }
			set { this[_parent.SaleContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleContractID is NULL, false otherwise.</summary>
		public bool IsSaleContractIDNull() 
		{
			return IsNull(_parent.SaleContractIDColumn);
		}

		/// <summary>Sets the TypedView field SaleContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleContractIDNull() 
		{
			this[_parent.SaleContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RefundReferenceID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."REFUNDREFERENCEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RefundReferenceID
		{
			get { return IsRefundReferenceIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RefundReferenceIDColumn]; }
			set { this[_parent.RefundReferenceIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RefundReferenceID is NULL, false otherwise.</summary>
		public bool IsRefundReferenceIDNull() 
		{
			return IsNull(_parent.RefundReferenceIDColumn);
		}

		/// <summary>Sets the TypedView field RefundReferenceID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRefundReferenceIDNull() 
		{
			this[_parent.RefundReferenceIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsOrganizational</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_PAYMENTJOURNAL"."ISORGANIZATIONAL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsOrganizational
		{
			get { return IsIsOrganizationalNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsOrganizationalColumn]; }
			set { this[_parent.IsOrganizationalColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsOrganizational is NULL, false otherwise.</summary>
		public bool IsIsOrganizationalNull() 
		{
			return IsNull(_parent.IsOrganizationalColumn);
		}

		/// <summary>Sets the TypedView field IsOrganizational to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsOrganizationalNull() 
		{
			this[_parent.IsOrganizationalColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
