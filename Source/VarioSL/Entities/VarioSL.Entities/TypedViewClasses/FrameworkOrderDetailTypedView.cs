﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'FrameworkOrderDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class FrameworkOrderDetailTypedView : TypedViewBase<FrameworkOrderDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnOrderID;
		private DataColumn _columnOrderNumber;
		private DataColumn _columnFrameworkName;
		private DataColumn _columnFrameworkOrganizationNumber;
		private DataColumn _columnFrameworkOrgExternalNumber;
		private DataColumn _columnFrameworkAddressee;
		private DataColumn _columnFrameworkAddressField1;
		private DataColumn _columnFrameworkAddressField2;
		private DataColumn _columnFrameworkCity;
		private DataColumn _columnFrameworkPostalCode;
		private DataColumn _columnFrameworkCountry;
		private DataColumn _columnFrameworkRegion;
		private DataColumn _columnFrameworkStreet;
		private DataColumn _columnFrameworkStreetNumber;
		private DataColumn _columnSchoolName;
		private DataColumn _columnSchoolOrganizationNumber;
		private DataColumn _columnSchoolAddressee;
		private DataColumn _columnSchoolAddressField1;
		private DataColumn _columnSchoolAddressField2;
		private DataColumn _columnSchoolCity;
		private DataColumn _columnSchoolPostalCode;
		private DataColumn _columnSchoolCountry;
		private DataColumn _columnSchoolStreet;
		private DataColumn _columnSchoolStreetNumber;
		private DataColumn _columnSchoolRegion;
		private DataColumn _columnOrganizationalIdentifier;
		private DataColumn _columnFirstName;
		private DataColumn _columnLastName;
		private DataColumn _columnDateOfBirth;
		private DataColumn _columnPrintedNumber;
		private DataColumn _columnTicketName;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnRelationFrom;
		private DataColumn _columnRelationTo;
		private DataColumn _columnRelationPriority;
		private DataColumn _columnSchoolYearName;
		private DataColumn _columnFrameworkContractID;
		private DataColumn _columnSchoolContractID;
		private DataColumn _columnPersonID;
		private DataColumn _columnProductRelationID;
		private DataColumn _columnProductID;
		private DataColumn _columnTicketID;
		private DataColumn _columnOrderDetailID;
		private DataColumn _columnRelationFareStage;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 45;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static FrameworkOrderDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FrameworkOrderDetailTypedView():base("FrameworkOrderDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FrameworkOrderDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.FrameworkOrderDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.FrameworkOrderDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new FrameworkOrderDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkOrganizationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkOrgExternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkAddressee", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkAddressField1", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkAddressField2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkCity", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkPostalCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkCountry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkRegion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkStreet", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkStreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolOrganizationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolAddressee", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolAddressField1", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolAddressField2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolCity", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolPostalCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolCountry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolStreet", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolStreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolRegion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationalIdentifier", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DateOfBirth", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationPriority", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolYearName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FrameworkContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductRelationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderDetailID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationFareStage", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "FrameworkOrderDetail";
			_columnOrderID = GeneralUtils.CreateTypedDataTableColumn("OrderID", @"OrderID", typeof(System.Int64), this.Columns);
			_columnOrderNumber = GeneralUtils.CreateTypedDataTableColumn("OrderNumber", @"OrderNumber", typeof(System.String), this.Columns);
			_columnFrameworkName = GeneralUtils.CreateTypedDataTableColumn("FrameworkName", @"FrameworkName", typeof(System.String), this.Columns);
			_columnFrameworkOrganizationNumber = GeneralUtils.CreateTypedDataTableColumn("FrameworkOrganizationNumber", @"FrameworkOrganizationNumber", typeof(System.String), this.Columns);
			_columnFrameworkOrgExternalNumber = GeneralUtils.CreateTypedDataTableColumn("FrameworkOrgExternalNumber", @"FrameworkOrgExternalNumber", typeof(System.String), this.Columns);
			_columnFrameworkAddressee = GeneralUtils.CreateTypedDataTableColumn("FrameworkAddressee", @"FrameworkAddressee", typeof(System.String), this.Columns);
			_columnFrameworkAddressField1 = GeneralUtils.CreateTypedDataTableColumn("FrameworkAddressField1", @"FrameworkAddressField1", typeof(System.String), this.Columns);
			_columnFrameworkAddressField2 = GeneralUtils.CreateTypedDataTableColumn("FrameworkAddressField2", @"FrameworkAddressField2", typeof(System.String), this.Columns);
			_columnFrameworkCity = GeneralUtils.CreateTypedDataTableColumn("FrameworkCity", @"FrameworkCity", typeof(System.String), this.Columns);
			_columnFrameworkPostalCode = GeneralUtils.CreateTypedDataTableColumn("FrameworkPostalCode", @"FrameworkPostalCode", typeof(System.String), this.Columns);
			_columnFrameworkCountry = GeneralUtils.CreateTypedDataTableColumn("FrameworkCountry", @"FrameworkCountry", typeof(System.String), this.Columns);
			_columnFrameworkRegion = GeneralUtils.CreateTypedDataTableColumn("FrameworkRegion", @"FrameworkRegion", typeof(System.String), this.Columns);
			_columnFrameworkStreet = GeneralUtils.CreateTypedDataTableColumn("FrameworkStreet", @"FrameworkStreet", typeof(System.String), this.Columns);
			_columnFrameworkStreetNumber = GeneralUtils.CreateTypedDataTableColumn("FrameworkStreetNumber", @"FrameworkStreetNumber", typeof(System.String), this.Columns);
			_columnSchoolName = GeneralUtils.CreateTypedDataTableColumn("SchoolName", @"SchoolName", typeof(System.String), this.Columns);
			_columnSchoolOrganizationNumber = GeneralUtils.CreateTypedDataTableColumn("SchoolOrganizationNumber", @"SchoolOrganizationNumber", typeof(System.String), this.Columns);
			_columnSchoolAddressee = GeneralUtils.CreateTypedDataTableColumn("SchoolAddressee", @"SchoolAddressee", typeof(System.String), this.Columns);
			_columnSchoolAddressField1 = GeneralUtils.CreateTypedDataTableColumn("SchoolAddressField1", @"SchoolAddressField1", typeof(System.String), this.Columns);
			_columnSchoolAddressField2 = GeneralUtils.CreateTypedDataTableColumn("SchoolAddressField2", @"SchoolAddressField2", typeof(System.String), this.Columns);
			_columnSchoolCity = GeneralUtils.CreateTypedDataTableColumn("SchoolCity", @"SchoolCity", typeof(System.String), this.Columns);
			_columnSchoolPostalCode = GeneralUtils.CreateTypedDataTableColumn("SchoolPostalCode", @"SchoolPostalCode", typeof(System.String), this.Columns);
			_columnSchoolCountry = GeneralUtils.CreateTypedDataTableColumn("SchoolCountry", @"SchoolCountry", typeof(System.String), this.Columns);
			_columnSchoolStreet = GeneralUtils.CreateTypedDataTableColumn("SchoolStreet", @"SchoolStreet", typeof(System.String), this.Columns);
			_columnSchoolStreetNumber = GeneralUtils.CreateTypedDataTableColumn("SchoolStreetNumber", @"SchoolStreetNumber", typeof(System.String), this.Columns);
			_columnSchoolRegion = GeneralUtils.CreateTypedDataTableColumn("SchoolRegion", @"SchoolRegion", typeof(System.String), this.Columns);
			_columnOrganizationalIdentifier = GeneralUtils.CreateTypedDataTableColumn("OrganizationalIdentifier", @"OrganizationalIdentifier", typeof(System.String), this.Columns);
			_columnFirstName = GeneralUtils.CreateTypedDataTableColumn("FirstName", @"FirstName", typeof(System.String), this.Columns);
			_columnLastName = GeneralUtils.CreateTypedDataTableColumn("LastName", @"LastName", typeof(System.String), this.Columns);
			_columnDateOfBirth = GeneralUtils.CreateTypedDataTableColumn("DateOfBirth", @"DateOfBirth", typeof(System.DateTime), this.Columns);
			_columnPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("PrintedNumber", @"PrintedNumber", typeof(System.String), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnRelationFrom = GeneralUtils.CreateTypedDataTableColumn("RelationFrom", @"RelationFrom", typeof(System.Int64), this.Columns);
			_columnRelationTo = GeneralUtils.CreateTypedDataTableColumn("RelationTo", @"RelationTo", typeof(System.Int64), this.Columns);
			_columnRelationPriority = GeneralUtils.CreateTypedDataTableColumn("RelationPriority", @"RelationPriority", typeof(System.Int64), this.Columns);
			_columnSchoolYearName = GeneralUtils.CreateTypedDataTableColumn("SchoolYearName", @"SchoolYearName", typeof(System.String), this.Columns);
			_columnFrameworkContractID = GeneralUtils.CreateTypedDataTableColumn("FrameworkContractID", @"FrameworkContractID", typeof(System.Int64), this.Columns);
			_columnSchoolContractID = GeneralUtils.CreateTypedDataTableColumn("SchoolContractID", @"SchoolContractID", typeof(System.Int64), this.Columns);
			_columnPersonID = GeneralUtils.CreateTypedDataTableColumn("PersonID", @"PersonID", typeof(System.Int64), this.Columns);
			_columnProductRelationID = GeneralUtils.CreateTypedDataTableColumn("ProductRelationID", @"ProductRelationID", typeof(System.Int64), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnOrderDetailID = GeneralUtils.CreateTypedDataTableColumn("OrderDetailID", @"OrderDetailID", typeof(System.Int64), this.Columns);
			_columnRelationFareStage = GeneralUtils.CreateTypedDataTableColumn("RelationFareStage", @"RelationFareStage", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnOrderID = this.Columns["OrderID"];
			_columnOrderNumber = this.Columns["OrderNumber"];
			_columnFrameworkName = this.Columns["FrameworkName"];
			_columnFrameworkOrganizationNumber = this.Columns["FrameworkOrganizationNumber"];
			_columnFrameworkOrgExternalNumber = this.Columns["FrameworkOrgExternalNumber"];
			_columnFrameworkAddressee = this.Columns["FrameworkAddressee"];
			_columnFrameworkAddressField1 = this.Columns["FrameworkAddressField1"];
			_columnFrameworkAddressField2 = this.Columns["FrameworkAddressField2"];
			_columnFrameworkCity = this.Columns["FrameworkCity"];
			_columnFrameworkPostalCode = this.Columns["FrameworkPostalCode"];
			_columnFrameworkCountry = this.Columns["FrameworkCountry"];
			_columnFrameworkRegion = this.Columns["FrameworkRegion"];
			_columnFrameworkStreet = this.Columns["FrameworkStreet"];
			_columnFrameworkStreetNumber = this.Columns["FrameworkStreetNumber"];
			_columnSchoolName = this.Columns["SchoolName"];
			_columnSchoolOrganizationNumber = this.Columns["SchoolOrganizationNumber"];
			_columnSchoolAddressee = this.Columns["SchoolAddressee"];
			_columnSchoolAddressField1 = this.Columns["SchoolAddressField1"];
			_columnSchoolAddressField2 = this.Columns["SchoolAddressField2"];
			_columnSchoolCity = this.Columns["SchoolCity"];
			_columnSchoolPostalCode = this.Columns["SchoolPostalCode"];
			_columnSchoolCountry = this.Columns["SchoolCountry"];
			_columnSchoolStreet = this.Columns["SchoolStreet"];
			_columnSchoolStreetNumber = this.Columns["SchoolStreetNumber"];
			_columnSchoolRegion = this.Columns["SchoolRegion"];
			_columnOrganizationalIdentifier = this.Columns["OrganizationalIdentifier"];
			_columnFirstName = this.Columns["FirstName"];
			_columnLastName = this.Columns["LastName"];
			_columnDateOfBirth = this.Columns["DateOfBirth"];
			_columnPrintedNumber = this.Columns["PrintedNumber"];
			_columnTicketName = this.Columns["TicketName"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnRelationFrom = this.Columns["RelationFrom"];
			_columnRelationTo = this.Columns["RelationTo"];
			_columnRelationPriority = this.Columns["RelationPriority"];
			_columnSchoolYearName = this.Columns["SchoolYearName"];
			_columnFrameworkContractID = this.Columns["FrameworkContractID"];
			_columnSchoolContractID = this.Columns["SchoolContractID"];
			_columnPersonID = this.Columns["PersonID"];
			_columnProductRelationID = this.Columns["ProductRelationID"];
			_columnProductID = this.Columns["ProductID"];
			_columnTicketID = this.Columns["TicketID"];
			_columnOrderDetailID = this.Columns["OrderDetailID"];
			_columnRelationFareStage = this.Columns["RelationFareStage"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			FrameworkOrderDetailTypedView cloneToReturn = ((FrameworkOrderDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new FrameworkOrderDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return FrameworkOrderDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return FrameworkOrderDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'OrderID'</summary>
		internal DataColumn OrderIDColumn 
		{
			get { return _columnOrderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderNumber'</summary>
		internal DataColumn OrderNumberColumn 
		{
			get { return _columnOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkName'</summary>
		internal DataColumn FrameworkNameColumn 
		{
			get { return _columnFrameworkName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkOrganizationNumber'</summary>
		internal DataColumn FrameworkOrganizationNumberColumn 
		{
			get { return _columnFrameworkOrganizationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkOrgExternalNumber'</summary>
		internal DataColumn FrameworkOrgExternalNumberColumn 
		{
			get { return _columnFrameworkOrgExternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkAddressee'</summary>
		internal DataColumn FrameworkAddresseeColumn 
		{
			get { return _columnFrameworkAddressee; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkAddressField1'</summary>
		internal DataColumn FrameworkAddressField1Column 
		{
			get { return _columnFrameworkAddressField1; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkAddressField2'</summary>
		internal DataColumn FrameworkAddressField2Column 
		{
			get { return _columnFrameworkAddressField2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkCity'</summary>
		internal DataColumn FrameworkCityColumn 
		{
			get { return _columnFrameworkCity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkPostalCode'</summary>
		internal DataColumn FrameworkPostalCodeColumn 
		{
			get { return _columnFrameworkPostalCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkCountry'</summary>
		internal DataColumn FrameworkCountryColumn 
		{
			get { return _columnFrameworkCountry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkRegion'</summary>
		internal DataColumn FrameworkRegionColumn 
		{
			get { return _columnFrameworkRegion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkStreet'</summary>
		internal DataColumn FrameworkStreetColumn 
		{
			get { return _columnFrameworkStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkStreetNumber'</summary>
		internal DataColumn FrameworkStreetNumberColumn 
		{
			get { return _columnFrameworkStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolName'</summary>
		internal DataColumn SchoolNameColumn 
		{
			get { return _columnSchoolName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolOrganizationNumber'</summary>
		internal DataColumn SchoolOrganizationNumberColumn 
		{
			get { return _columnSchoolOrganizationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolAddressee'</summary>
		internal DataColumn SchoolAddresseeColumn 
		{
			get { return _columnSchoolAddressee; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolAddressField1'</summary>
		internal DataColumn SchoolAddressField1Column 
		{
			get { return _columnSchoolAddressField1; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolAddressField2'</summary>
		internal DataColumn SchoolAddressField2Column 
		{
			get { return _columnSchoolAddressField2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolCity'</summary>
		internal DataColumn SchoolCityColumn 
		{
			get { return _columnSchoolCity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolPostalCode'</summary>
		internal DataColumn SchoolPostalCodeColumn 
		{
			get { return _columnSchoolPostalCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolCountry'</summary>
		internal DataColumn SchoolCountryColumn 
		{
			get { return _columnSchoolCountry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolStreet'</summary>
		internal DataColumn SchoolStreetColumn 
		{
			get { return _columnSchoolStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolStreetNumber'</summary>
		internal DataColumn SchoolStreetNumberColumn 
		{
			get { return _columnSchoolStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolRegion'</summary>
		internal DataColumn SchoolRegionColumn 
		{
			get { return _columnSchoolRegion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationalIdentifier'</summary>
		internal DataColumn OrganizationalIdentifierColumn 
		{
			get { return _columnOrganizationalIdentifier; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FirstName'</summary>
		internal DataColumn FirstNameColumn 
		{
			get { return _columnFirstName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastName'</summary>
		internal DataColumn LastNameColumn 
		{
			get { return _columnLastName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DateOfBirth'</summary>
		internal DataColumn DateOfBirthColumn 
		{
			get { return _columnDateOfBirth; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PrintedNumber'</summary>
		internal DataColumn PrintedNumberColumn 
		{
			get { return _columnPrintedNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationFrom'</summary>
		internal DataColumn RelationFromColumn 
		{
			get { return _columnRelationFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationTo'</summary>
		internal DataColumn RelationToColumn 
		{
			get { return _columnRelationTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationPriority'</summary>
		internal DataColumn RelationPriorityColumn 
		{
			get { return _columnRelationPriority; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolYearName'</summary>
		internal DataColumn SchoolYearNameColumn 
		{
			get { return _columnSchoolYearName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FrameworkContractID'</summary>
		internal DataColumn FrameworkContractIDColumn 
		{
			get { return _columnFrameworkContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolContractID'</summary>
		internal DataColumn SchoolContractIDColumn 
		{
			get { return _columnSchoolContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonID'</summary>
		internal DataColumn PersonIDColumn 
		{
			get { return _columnPersonID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductRelationID'</summary>
		internal DataColumn ProductRelationIDColumn 
		{
			get { return _columnProductRelationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderDetailID'</summary>
		internal DataColumn OrderDetailIDColumn 
		{
			get { return _columnOrderDetailID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationFareStage'</summary>
		internal DataColumn RelationFareStageColumn 
		{
			get { return _columnRelationFareStage; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable FrameworkOrderDetail</summary>
	public partial class FrameworkOrderDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private FrameworkOrderDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal FrameworkOrderDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((FrameworkOrderDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field OrderID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."ORDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderID
		{
			get { return IsOrderIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderIDColumn]; }
			set { this[_parent.OrderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderID is NULL, false otherwise.</summary>
		public bool IsOrderIDNull() 
		{
			return IsNull(_parent.OrderIDColumn);
		}

		/// <summary>Sets the TypedView field OrderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderIDNull() 
		{
			this[_parent.OrderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."ORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrderNumber
		{
			get { return IsOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrderNumberColumn]; }
			set { this[_parent.OrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderNumber is NULL, false otherwise.</summary>
		public bool IsOrderNumberNull() 
		{
			return IsNull(_parent.OrderNumberColumn);
		}

		/// <summary>Sets the TypedView field OrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderNumberNull() 
		{
			this[_parent.OrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String FrameworkName
		{
			get { return IsFrameworkNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkNameColumn]; }
			set { this[_parent.FrameworkNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkName is NULL, false otherwise.</summary>
		public bool IsFrameworkNameNull() 
		{
			return IsNull(_parent.FrameworkNameColumn);
		}

		/// <summary>Sets the TypedView field FrameworkName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkNameNull() 
		{
			this[_parent.FrameworkNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkOrganizationNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKORGANIZATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String FrameworkOrganizationNumber
		{
			get { return IsFrameworkOrganizationNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkOrganizationNumberColumn]; }
			set { this[_parent.FrameworkOrganizationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkOrganizationNumber is NULL, false otherwise.</summary>
		public bool IsFrameworkOrganizationNumberNull() 
		{
			return IsNull(_parent.FrameworkOrganizationNumberColumn);
		}

		/// <summary>Sets the TypedView field FrameworkOrganizationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkOrganizationNumberNull() 
		{
			this[_parent.FrameworkOrganizationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkOrgExternalNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKORGEXTERNALNNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String FrameworkOrgExternalNumber
		{
			get { return IsFrameworkOrgExternalNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkOrgExternalNumberColumn]; }
			set { this[_parent.FrameworkOrgExternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkOrgExternalNumber is NULL, false otherwise.</summary>
		public bool IsFrameworkOrgExternalNumberNull() 
		{
			return IsNull(_parent.FrameworkOrgExternalNumberColumn);
		}

		/// <summary>Sets the TypedView field FrameworkOrgExternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkOrgExternalNumberNull() 
		{
			this[_parent.FrameworkOrgExternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkAddressee</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKADDRESSEE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkAddressee
		{
			get { return IsFrameworkAddresseeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkAddresseeColumn]; }
			set { this[_parent.FrameworkAddresseeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkAddressee is NULL, false otherwise.</summary>
		public bool IsFrameworkAddresseeNull() 
		{
			return IsNull(_parent.FrameworkAddresseeColumn);
		}

		/// <summary>Sets the TypedView field FrameworkAddressee to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkAddresseeNull() 
		{
			this[_parent.FrameworkAddresseeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkAddressField1</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKADDRESSFIELD1"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkAddressField1
		{
			get { return IsFrameworkAddressField1Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkAddressField1Column]; }
			set { this[_parent.FrameworkAddressField1Column] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkAddressField1 is NULL, false otherwise.</summary>
		public bool IsFrameworkAddressField1Null() 
		{
			return IsNull(_parent.FrameworkAddressField1Column);
		}

		/// <summary>Sets the TypedView field FrameworkAddressField1 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkAddressField1Null() 
		{
			this[_parent.FrameworkAddressField1Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkAddressField2</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKADDRESSFIELD2"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkAddressField2
		{
			get { return IsFrameworkAddressField2Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkAddressField2Column]; }
			set { this[_parent.FrameworkAddressField2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkAddressField2 is NULL, false otherwise.</summary>
		public bool IsFrameworkAddressField2Null() 
		{
			return IsNull(_parent.FrameworkAddressField2Column);
		}

		/// <summary>Sets the TypedView field FrameworkAddressField2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkAddressField2Null() 
		{
			this[_parent.FrameworkAddressField2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkCity</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKCITY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkCity
		{
			get { return IsFrameworkCityNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkCityColumn]; }
			set { this[_parent.FrameworkCityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkCity is NULL, false otherwise.</summary>
		public bool IsFrameworkCityNull() 
		{
			return IsNull(_parent.FrameworkCityColumn);
		}

		/// <summary>Sets the TypedView field FrameworkCity to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkCityNull() 
		{
			this[_parent.FrameworkCityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkPostalCode</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKPOSTALCODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String FrameworkPostalCode
		{
			get { return IsFrameworkPostalCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkPostalCodeColumn]; }
			set { this[_parent.FrameworkPostalCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkPostalCode is NULL, false otherwise.</summary>
		public bool IsFrameworkPostalCodeNull() 
		{
			return IsNull(_parent.FrameworkPostalCodeColumn);
		}

		/// <summary>Sets the TypedView field FrameworkPostalCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkPostalCodeNull() 
		{
			this[_parent.FrameworkPostalCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkCountry</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKCOUNTRY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkCountry
		{
			get { return IsFrameworkCountryNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkCountryColumn]; }
			set { this[_parent.FrameworkCountryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkCountry is NULL, false otherwise.</summary>
		public bool IsFrameworkCountryNull() 
		{
			return IsNull(_parent.FrameworkCountryColumn);
		}

		/// <summary>Sets the TypedView field FrameworkCountry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkCountryNull() 
		{
			this[_parent.FrameworkCountryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkRegion</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKREGION"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkRegion
		{
			get { return IsFrameworkRegionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkRegionColumn]; }
			set { this[_parent.FrameworkRegionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkRegion is NULL, false otherwise.</summary>
		public bool IsFrameworkRegionNull() 
		{
			return IsNull(_parent.FrameworkRegionColumn);
		}

		/// <summary>Sets the TypedView field FrameworkRegion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkRegionNull() 
		{
			this[_parent.FrameworkRegionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkStreet</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKSTREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String FrameworkStreet
		{
			get { return IsFrameworkStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkStreetColumn]; }
			set { this[_parent.FrameworkStreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkStreet is NULL, false otherwise.</summary>
		public bool IsFrameworkStreetNull() 
		{
			return IsNull(_parent.FrameworkStreetColumn);
		}

		/// <summary>Sets the TypedView field FrameworkStreet to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkStreetNull() 
		{
			this[_parent.FrameworkStreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkStreetNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKSTREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String FrameworkStreetNumber
		{
			get { return IsFrameworkStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FrameworkStreetNumberColumn]; }
			set { this[_parent.FrameworkStreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkStreetNumber is NULL, false otherwise.</summary>
		public bool IsFrameworkStreetNumberNull() 
		{
			return IsNull(_parent.FrameworkStreetNumberColumn);
		}

		/// <summary>Sets the TypedView field FrameworkStreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkStreetNumberNull() 
		{
			this[_parent.FrameworkStreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String SchoolName
		{
			get { return IsSchoolNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolNameColumn]; }
			set { this[_parent.SchoolNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolName is NULL, false otherwise.</summary>
		public bool IsSchoolNameNull() 
		{
			return IsNull(_parent.SchoolNameColumn);
		}

		/// <summary>Sets the TypedView field SchoolName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolNameNull() 
		{
			this[_parent.SchoolNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolOrganizationNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLORGANIZATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String SchoolOrganizationNumber
		{
			get { return IsSchoolOrganizationNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolOrganizationNumberColumn]; }
			set { this[_parent.SchoolOrganizationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolOrganizationNumber is NULL, false otherwise.</summary>
		public bool IsSchoolOrganizationNumberNull() 
		{
			return IsNull(_parent.SchoolOrganizationNumberColumn);
		}

		/// <summary>Sets the TypedView field SchoolOrganizationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolOrganizationNumberNull() 
		{
			this[_parent.SchoolOrganizationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolAddressee</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLADDRESSEE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolAddressee
		{
			get { return IsSchoolAddresseeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolAddresseeColumn]; }
			set { this[_parent.SchoolAddresseeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolAddressee is NULL, false otherwise.</summary>
		public bool IsSchoolAddresseeNull() 
		{
			return IsNull(_parent.SchoolAddresseeColumn);
		}

		/// <summary>Sets the TypedView field SchoolAddressee to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolAddresseeNull() 
		{
			this[_parent.SchoolAddresseeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolAddressField1</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLADDRESSFIELD1"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolAddressField1
		{
			get { return IsSchoolAddressField1Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolAddressField1Column]; }
			set { this[_parent.SchoolAddressField1Column] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolAddressField1 is NULL, false otherwise.</summary>
		public bool IsSchoolAddressField1Null() 
		{
			return IsNull(_parent.SchoolAddressField1Column);
		}

		/// <summary>Sets the TypedView field SchoolAddressField1 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolAddressField1Null() 
		{
			this[_parent.SchoolAddressField1Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolAddressField2</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLADDRESSFIELD2"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolAddressField2
		{
			get { return IsSchoolAddressField2Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolAddressField2Column]; }
			set { this[_parent.SchoolAddressField2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolAddressField2 is NULL, false otherwise.</summary>
		public bool IsSchoolAddressField2Null() 
		{
			return IsNull(_parent.SchoolAddressField2Column);
		}

		/// <summary>Sets the TypedView field SchoolAddressField2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolAddressField2Null() 
		{
			this[_parent.SchoolAddressField2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolCity</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLCITY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolCity
		{
			get { return IsSchoolCityNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolCityColumn]; }
			set { this[_parent.SchoolCityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolCity is NULL, false otherwise.</summary>
		public bool IsSchoolCityNull() 
		{
			return IsNull(_parent.SchoolCityColumn);
		}

		/// <summary>Sets the TypedView field SchoolCity to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolCityNull() 
		{
			this[_parent.SchoolCityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolPostalCode</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLPOSTALCODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String SchoolPostalCode
		{
			get { return IsSchoolPostalCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolPostalCodeColumn]; }
			set { this[_parent.SchoolPostalCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolPostalCode is NULL, false otherwise.</summary>
		public bool IsSchoolPostalCodeNull() 
		{
			return IsNull(_parent.SchoolPostalCodeColumn);
		}

		/// <summary>Sets the TypedView field SchoolPostalCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolPostalCodeNull() 
		{
			this[_parent.SchoolPostalCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolCountry</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLCOUNTRY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolCountry
		{
			get { return IsSchoolCountryNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolCountryColumn]; }
			set { this[_parent.SchoolCountryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolCountry is NULL, false otherwise.</summary>
		public bool IsSchoolCountryNull() 
		{
			return IsNull(_parent.SchoolCountryColumn);
		}

		/// <summary>Sets the TypedView field SchoolCountry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolCountryNull() 
		{
			this[_parent.SchoolCountryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolStreet</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLSTREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolStreet
		{
			get { return IsSchoolStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolStreetColumn]; }
			set { this[_parent.SchoolStreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolStreet is NULL, false otherwise.</summary>
		public bool IsSchoolStreetNull() 
		{
			return IsNull(_parent.SchoolStreetColumn);
		}

		/// <summary>Sets the TypedView field SchoolStreet to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolStreetNull() 
		{
			this[_parent.SchoolStreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolStreetNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLSTREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String SchoolStreetNumber
		{
			get { return IsSchoolStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolStreetNumberColumn]; }
			set { this[_parent.SchoolStreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolStreetNumber is NULL, false otherwise.</summary>
		public bool IsSchoolStreetNumberNull() 
		{
			return IsNull(_parent.SchoolStreetNumberColumn);
		}

		/// <summary>Sets the TypedView field SchoolStreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolStreetNumberNull() 
		{
			this[_parent.SchoolStreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolRegion</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLREGION"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String SchoolRegion
		{
			get { return IsSchoolRegionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolRegionColumn]; }
			set { this[_parent.SchoolRegionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolRegion is NULL, false otherwise.</summary>
		public bool IsSchoolRegionNull() 
		{
			return IsNull(_parent.SchoolRegionColumn);
		}

		/// <summary>Sets the TypedView field SchoolRegion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolRegionNull() 
		{
			this[_parent.SchoolRegionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationalIdentifier</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."ORGANIZATIONALIDENTIFIER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrganizationalIdentifier
		{
			get { return IsOrganizationalIdentifierNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationalIdentifierColumn]; }
			set { this[_parent.OrganizationalIdentifierColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationalIdentifier is NULL, false otherwise.</summary>
		public bool IsOrganizationalIdentifierNull() 
		{
			return IsNull(_parent.OrganizationalIdentifierColumn);
		}

		/// <summary>Sets the TypedView field OrganizationalIdentifier to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationalIdentifierNull() 
		{
			this[_parent.OrganizationalIdentifierColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FirstName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FIRSTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String FirstName
		{
			get { return IsFirstNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FirstNameColumn]; }
			set { this[_parent.FirstNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FirstName is NULL, false otherwise.</summary>
		public bool IsFirstNameNull() 
		{
			return IsNull(_parent.FirstNameColumn);
		}

		/// <summary>Sets the TypedView field FirstName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFirstNameNull() 
		{
			this[_parent.FirstNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."LASTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastName
		{
			get { return IsLastNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastNameColumn]; }
			set { this[_parent.LastNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastName is NULL, false otherwise.</summary>
		public bool IsLastNameNull() 
		{
			return IsNull(_parent.LastNameColumn);
		}

		/// <summary>Sets the TypedView field LastName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastNameNull() 
		{
			this[_parent.LastNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DateOfBirth</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."DATEOFBIRTH"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime DateOfBirth
		{
			get { return IsDateOfBirthNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.DateOfBirthColumn]; }
			set { this[_parent.DateOfBirthColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DateOfBirth is NULL, false otherwise.</summary>
		public bool IsDateOfBirthNull() 
		{
			return IsNull(_parent.DateOfBirthColumn);
		}

		/// <summary>Sets the TypedView field DateOfBirth to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDateOfBirthNull() 
		{
			this[_parent.DateOfBirthColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PrintedNumber</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."PRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String PrintedNumber
		{
			get { return IsPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PrintedNumberColumn]; }
			set { this[_parent.PrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PrintedNumber is NULL, false otherwise.</summary>
		public bool IsPrintedNumberNull() 
		{
			return IsNull(_parent.PrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field PrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPrintedNumberNull() 
		{
			this[_parent.PrintedNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationFrom</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."RELATIONFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RelationFrom
		{
			get { return IsRelationFromNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RelationFromColumn]; }
			set { this[_parent.RelationFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationFrom is NULL, false otherwise.</summary>
		public bool IsRelationFromNull() 
		{
			return IsNull(_parent.RelationFromColumn);
		}

		/// <summary>Sets the TypedView field RelationFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationFromNull() 
		{
			this[_parent.RelationFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationTo</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."RELATIONTO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RelationTo
		{
			get { return IsRelationToNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RelationToColumn]; }
			set { this[_parent.RelationToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationTo is NULL, false otherwise.</summary>
		public bool IsRelationToNull() 
		{
			return IsNull(_parent.RelationToColumn);
		}

		/// <summary>Sets the TypedView field RelationTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationToNull() 
		{
			this[_parent.RelationToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationPriority</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."RELATIONPRIORITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RelationPriority
		{
			get { return IsRelationPriorityNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RelationPriorityColumn]; }
			set { this[_parent.RelationPriorityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationPriority is NULL, false otherwise.</summary>
		public bool IsRelationPriorityNull() 
		{
			return IsNull(_parent.RelationPriorityColumn);
		}

		/// <summary>Sets the TypedView field RelationPriority to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationPriorityNull() 
		{
			this[_parent.RelationPriorityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolYearName</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLYEARNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String SchoolYearName
		{
			get { return IsSchoolYearNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolYearNameColumn]; }
			set { this[_parent.SchoolYearNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolYearName is NULL, false otherwise.</summary>
		public bool IsSchoolYearNameNull() 
		{
			return IsNull(_parent.SchoolYearNameColumn);
		}

		/// <summary>Sets the TypedView field SchoolYearName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolYearNameNull() 
		{
			this[_parent.SchoolYearNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FrameworkContractID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."FRAMEWORKCONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 FrameworkContractID
		{
			get { return IsFrameworkContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FrameworkContractIDColumn]; }
			set { this[_parent.FrameworkContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FrameworkContractID is NULL, false otherwise.</summary>
		public bool IsFrameworkContractIDNull() 
		{
			return IsNull(_parent.FrameworkContractIDColumn);
		}

		/// <summary>Sets the TypedView field FrameworkContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFrameworkContractIDNull() 
		{
			this[_parent.FrameworkContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolContractID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."SCHOOLCONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SchoolContractID
		{
			get { return IsSchoolContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SchoolContractIDColumn]; }
			set { this[_parent.SchoolContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolContractID is NULL, false otherwise.</summary>
		public bool IsSchoolContractIDNull() 
		{
			return IsNull(_parent.SchoolContractIDColumn);
		}

		/// <summary>Sets the TypedView field SchoolContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolContractIDNull() 
		{
			this[_parent.SchoolContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."PERSONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PersonID
		{
			get { return IsPersonIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PersonIDColumn]; }
			set { this[_parent.PersonIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonID is NULL, false otherwise.</summary>
		public bool IsPersonIDNull() 
		{
			return IsNull(_parent.PersonIDColumn);
		}

		/// <summary>Sets the TypedView field PersonID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonIDNull() 
		{
			this[_parent.PersonIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductRelationID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."PRODUCTRELATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductRelationID
		{
			get { return IsProductRelationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductRelationIDColumn]; }
			set { this[_parent.ProductRelationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductRelationID is NULL, false otherwise.</summary>
		public bool IsProductRelationIDNull() 
		{
			return IsNull(_parent.ProductRelationIDColumn);
		}

		/// <summary>Sets the TypedView field ProductRelationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductRelationIDNull() 
		{
			this[_parent.ProductRelationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderDetailID</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."ORDERDETAILID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderDetailID
		{
			get { return IsOrderDetailIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderDetailIDColumn]; }
			set { this[_parent.OrderDetailIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderDetailID is NULL, false otherwise.</summary>
		public bool IsOrderDetailIDNull() 
		{
			return IsNull(_parent.OrderDetailIDColumn);
		}

		/// <summary>Sets the TypedView field OrderDetailID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderDetailIDNull() 
		{
			this[_parent.OrderDetailIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationFareStage</summary>
		/// <remarks>Mapped on view field: "SL_FRAMEWORKORDERDETAILS"."RELATIONFARESTAGE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String RelationFareStage
		{
			get { return IsRelationFareStageNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.RelationFareStageColumn]; }
			set { this[_parent.RelationFareStageColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationFareStage is NULL, false otherwise.</summary>
		public bool IsRelationFareStageNull() 
		{
			return IsNull(_parent.RelationFareStageColumn);
		}

		/// <summary>Sets the TypedView field RelationFareStage to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationFareStageNull() 
		{
			this[_parent.RelationFareStageColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
