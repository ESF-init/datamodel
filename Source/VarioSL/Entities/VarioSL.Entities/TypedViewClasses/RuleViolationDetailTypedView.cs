﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'RuleViolationDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class RuleViolationDetailTypedView : TypedViewBase<RuleViolationDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnRuleViolationID;
		private DataColumn _columnRuleViolationType;
		private DataColumn _columnViolationValue;
		private DataColumn _columnMessage;
		private DataColumn _columnExecutionTime;
		private DataColumn _columnRuleViolationText;
		private DataColumn _columnRuleViolationProvider;
		private DataColumn _columnIsResolved;
		private DataColumn _columnAggregationFrom;
		private DataColumn _columnAggregationTo;
		private DataColumn _columnRuleViolationSourceType;
		private DataColumn _columnCardID;
		private DataColumn _columnSerialNumber;
		private DataColumn _columnPrintedNumber;
		private DataColumn _columnCardState;
		private DataColumn _columnCardLastUsed;
		private DataColumn _columnCardExpiration;
		private DataColumn _columnCardIsBlocked;
		private DataColumn _columnCardBlockingReason;
		private DataColumn _columnCardBlockingComment;
		private DataColumn _columnCardType;
		private DataColumn _columnFareMediaID;
		private DataColumn _columnProductID;
		private DataColumn _columnTicketID;
		private DataColumn _columnTicketType;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnProductIsCanceled;
		private DataColumn _columnPrice;
		private DataColumn _columnProductIsBlocked;
		private DataColumn _columnProductBlockingReason;
		private DataColumn _columnProductBlockingDate;
		private DataColumn _columnProductCancellationDate;
		private DataColumn _columnPaymentOptionID;
		private DataColumn _columnContractID;
		private DataColumn _columnPaymentMethod;
		private DataColumn _columnPaymentOptionDescription;
		private DataColumn _columnBankingServiceToken;
		private DataColumn _columnPaymentOptionExpiry;
		private DataColumn _columnPaymentOptionCreditCardNetwork;
		private DataColumn _columnPaymentOptionPriority;
		private DataColumn _columnPaymentOptionName;
		private DataColumn _columnIsTemporary;
		private DataColumn _columnPaymentOptionIsRetired;
		private DataColumn _columnBankConnectionDataID;
		private DataColumn _columnProspectivePaymentOptionID;
		private DataColumn _columnPaymentOptionIsBlocked;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 47;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static RuleViolationDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RuleViolationDetailTypedView():base("RuleViolationDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RuleViolationDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.RuleViolationDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.RuleViolationDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new RuleViolationDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RuleViolationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RuleViolationType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ViolationValue", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExecutionTime", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RuleViolationText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RuleViolationProvider", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsResolved", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AggregationFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AggregationTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RuleViolationSourceType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SerialNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardLastUsed", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardExpiration", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardIsBlocked", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardBlockingReason", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardBlockingComment", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductIsCanceled", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Price", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductIsBlocked", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductBlockingReason", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductBlockingDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductCancellationDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentMethod", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionDescription", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankingServiceToken", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionExpiry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionCreditCardNetwork", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionPriority", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsTemporary", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionIsRetired", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankConnectionDataID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProspectivePaymentOptionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentOptionIsBlocked", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "RuleViolationDetail";
			_columnRuleViolationID = GeneralUtils.CreateTypedDataTableColumn("RuleViolationID", @"RuleViolationID", typeof(System.Int64), this.Columns);
			_columnRuleViolationType = GeneralUtils.CreateTypedDataTableColumn("RuleViolationType", @"RuleViolationType", typeof(VarioSL.Entities.Enumerations.RuleViolationType), this.Columns);
			_columnViolationValue = GeneralUtils.CreateTypedDataTableColumn("ViolationValue", @"ViolationValue", typeof(System.String), this.Columns);
			_columnMessage = GeneralUtils.CreateTypedDataTableColumn("Message", @"Message", typeof(System.String), this.Columns);
			_columnExecutionTime = GeneralUtils.CreateTypedDataTableColumn("ExecutionTime", @"ExecutionTime", typeof(System.DateTime), this.Columns);
			_columnRuleViolationText = GeneralUtils.CreateTypedDataTableColumn("RuleViolationText", @"RuleViolationText", typeof(System.String), this.Columns);
			_columnRuleViolationProvider = GeneralUtils.CreateTypedDataTableColumn("RuleViolationProvider", @"RuleViolationProvider", typeof(VarioSL.Entities.Enumerations.RuleViolationProvider), this.Columns);
			_columnIsResolved = GeneralUtils.CreateTypedDataTableColumn("IsResolved", @"IsResolved", typeof(System.Int16), this.Columns);
			_columnAggregationFrom = GeneralUtils.CreateTypedDataTableColumn("AggregationFrom", @"AggregationFrom", typeof(System.DateTime), this.Columns);
			_columnAggregationTo = GeneralUtils.CreateTypedDataTableColumn("AggregationTo", @"AggregationTo", typeof(System.DateTime), this.Columns);
			_columnRuleViolationSourceType = GeneralUtils.CreateTypedDataTableColumn("RuleViolationSourceType", @"RuleViolationSourceType", typeof(VarioSL.Entities.Enumerations.RuleViolationSourceType), this.Columns);
			_columnCardID = GeneralUtils.CreateTypedDataTableColumn("CardID", @"CardID", typeof(System.Int64), this.Columns);
			_columnSerialNumber = GeneralUtils.CreateTypedDataTableColumn("SerialNumber", @"SerialNumber", typeof(System.String), this.Columns);
			_columnPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("PrintedNumber", @"PrintedNumber", typeof(System.String), this.Columns);
			_columnCardState = GeneralUtils.CreateTypedDataTableColumn("CardState", @"CardState", typeof(VarioSL.Entities.Enumerations.CardState), this.Columns);
			_columnCardLastUsed = GeneralUtils.CreateTypedDataTableColumn("CardLastUsed", @"CardLastUsed", typeof(System.DateTime), this.Columns);
			_columnCardExpiration = GeneralUtils.CreateTypedDataTableColumn("CardExpiration", @"CardExpiration", typeof(System.DateTime), this.Columns);
			_columnCardIsBlocked = GeneralUtils.CreateTypedDataTableColumn("CardIsBlocked", @"CardIsBlocked", typeof(System.DateTime), this.Columns);
			_columnCardBlockingReason = GeneralUtils.CreateTypedDataTableColumn("CardBlockingReason", @"CardBlockingReason", typeof(System.Int32), this.Columns);
			_columnCardBlockingComment = GeneralUtils.CreateTypedDataTableColumn("CardBlockingComment", @"CardBlockingComment", typeof(System.String), this.Columns);
			_columnCardType = GeneralUtils.CreateTypedDataTableColumn("CardType", @"CardType", typeof(System.Int64), this.Columns);
			_columnFareMediaID = GeneralUtils.CreateTypedDataTableColumn("FareMediaID", @"FareMediaID", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnTicketType = GeneralUtils.CreateTypedDataTableColumn("TicketType", @"TicketType", typeof(VarioSL.Entities.Enumerations.TicketType), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnProductIsCanceled = GeneralUtils.CreateTypedDataTableColumn("ProductIsCanceled", @"ProductIsCanceled", typeof(System.Int16), this.Columns);
			_columnPrice = GeneralUtils.CreateTypedDataTableColumn("Price", @"Price", typeof(System.Int32), this.Columns);
			_columnProductIsBlocked = GeneralUtils.CreateTypedDataTableColumn("ProductIsBlocked", @"ProductIsBlocked", typeof(System.Int16), this.Columns);
			_columnProductBlockingReason = GeneralUtils.CreateTypedDataTableColumn("ProductBlockingReason", @"ProductBlockingReason", typeof(System.String), this.Columns);
			_columnProductBlockingDate = GeneralUtils.CreateTypedDataTableColumn("ProductBlockingDate", @"ProductBlockingDate", typeof(System.DateTime), this.Columns);
			_columnProductCancellationDate = GeneralUtils.CreateTypedDataTableColumn("ProductCancellationDate", @"ProductCancellationDate", typeof(System.DateTime), this.Columns);
			_columnPaymentOptionID = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionID", @"PaymentOptionID", typeof(System.Int64), this.Columns);
			_columnContractID = GeneralUtils.CreateTypedDataTableColumn("ContractID", @"ContractID", typeof(System.Int64), this.Columns);
			_columnPaymentMethod = GeneralUtils.CreateTypedDataTableColumn("PaymentMethod", @"PaymentMethod", typeof(VarioSL.Entities.Enumerations.PaymentMethod), this.Columns);
			_columnPaymentOptionDescription = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionDescription", @"PaymentOptionDescription", typeof(System.String), this.Columns);
			_columnBankingServiceToken = GeneralUtils.CreateTypedDataTableColumn("BankingServiceToken", @"BankingServiceToken", typeof(System.String), this.Columns);
			_columnPaymentOptionExpiry = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionExpiry", @"PaymentOptionExpiry", typeof(System.DateTime), this.Columns);
			_columnPaymentOptionCreditCardNetwork = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionCreditCardNetwork", @"PaymentOptionCreditCardNetwork", typeof(System.String), this.Columns);
			_columnPaymentOptionPriority = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionPriority", @"PaymentOptionPriority", typeof(System.Int32), this.Columns);
			_columnPaymentOptionName = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionName", @"PaymentOptionName", typeof(System.String), this.Columns);
			_columnIsTemporary = GeneralUtils.CreateTypedDataTableColumn("IsTemporary", @"IsTemporary", typeof(System.Int16), this.Columns);
			_columnPaymentOptionIsRetired = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionIsRetired", @"PaymentOptionIsRetired", typeof(System.Int16), this.Columns);
			_columnBankConnectionDataID = GeneralUtils.CreateTypedDataTableColumn("BankConnectionDataID", @"BankConnectionDataID", typeof(System.Int64), this.Columns);
			_columnProspectivePaymentOptionID = GeneralUtils.CreateTypedDataTableColumn("ProspectivePaymentOptionID", @"ProspectivePaymentOptionID", typeof(System.Int64), this.Columns);
			_columnPaymentOptionIsBlocked = GeneralUtils.CreateTypedDataTableColumn("PaymentOptionIsBlocked", @"PaymentOptionIsBlocked", typeof(System.Int16), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnRuleViolationID = this.Columns["RuleViolationID"];
			_columnRuleViolationType = this.Columns["RuleViolationType"];
			_columnViolationValue = this.Columns["ViolationValue"];
			_columnMessage = this.Columns["Message"];
			_columnExecutionTime = this.Columns["ExecutionTime"];
			_columnRuleViolationText = this.Columns["RuleViolationText"];
			_columnRuleViolationProvider = this.Columns["RuleViolationProvider"];
			_columnIsResolved = this.Columns["IsResolved"];
			_columnAggregationFrom = this.Columns["AggregationFrom"];
			_columnAggregationTo = this.Columns["AggregationTo"];
			_columnRuleViolationSourceType = this.Columns["RuleViolationSourceType"];
			_columnCardID = this.Columns["CardID"];
			_columnSerialNumber = this.Columns["SerialNumber"];
			_columnPrintedNumber = this.Columns["PrintedNumber"];
			_columnCardState = this.Columns["CardState"];
			_columnCardLastUsed = this.Columns["CardLastUsed"];
			_columnCardExpiration = this.Columns["CardExpiration"];
			_columnCardIsBlocked = this.Columns["CardIsBlocked"];
			_columnCardBlockingReason = this.Columns["CardBlockingReason"];
			_columnCardBlockingComment = this.Columns["CardBlockingComment"];
			_columnCardType = this.Columns["CardType"];
			_columnFareMediaID = this.Columns["FareMediaID"];
			_columnProductID = this.Columns["ProductID"];
			_columnTicketID = this.Columns["TicketID"];
			_columnTicketType = this.Columns["TicketType"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnProductIsCanceled = this.Columns["ProductIsCanceled"];
			_columnPrice = this.Columns["Price"];
			_columnProductIsBlocked = this.Columns["ProductIsBlocked"];
			_columnProductBlockingReason = this.Columns["ProductBlockingReason"];
			_columnProductBlockingDate = this.Columns["ProductBlockingDate"];
			_columnProductCancellationDate = this.Columns["ProductCancellationDate"];
			_columnPaymentOptionID = this.Columns["PaymentOptionID"];
			_columnContractID = this.Columns["ContractID"];
			_columnPaymentMethod = this.Columns["PaymentMethod"];
			_columnPaymentOptionDescription = this.Columns["PaymentOptionDescription"];
			_columnBankingServiceToken = this.Columns["BankingServiceToken"];
			_columnPaymentOptionExpiry = this.Columns["PaymentOptionExpiry"];
			_columnPaymentOptionCreditCardNetwork = this.Columns["PaymentOptionCreditCardNetwork"];
			_columnPaymentOptionPriority = this.Columns["PaymentOptionPriority"];
			_columnPaymentOptionName = this.Columns["PaymentOptionName"];
			_columnIsTemporary = this.Columns["IsTemporary"];
			_columnPaymentOptionIsRetired = this.Columns["PaymentOptionIsRetired"];
			_columnBankConnectionDataID = this.Columns["BankConnectionDataID"];
			_columnProspectivePaymentOptionID = this.Columns["ProspectivePaymentOptionID"];
			_columnPaymentOptionIsBlocked = this.Columns["PaymentOptionIsBlocked"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			RuleViolationDetailTypedView cloneToReturn = ((RuleViolationDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new RuleViolationDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return RuleViolationDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return RuleViolationDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'RuleViolationID'</summary>
		internal DataColumn RuleViolationIDColumn 
		{
			get { return _columnRuleViolationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RuleViolationType'</summary>
		internal DataColumn RuleViolationTypeColumn 
		{
			get { return _columnRuleViolationType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ViolationValue'</summary>
		internal DataColumn ViolationValueColumn 
		{
			get { return _columnViolationValue; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Message'</summary>
		internal DataColumn MessageColumn 
		{
			get { return _columnMessage; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExecutionTime'</summary>
		internal DataColumn ExecutionTimeColumn 
		{
			get { return _columnExecutionTime; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RuleViolationText'</summary>
		internal DataColumn RuleViolationTextColumn 
		{
			get { return _columnRuleViolationText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RuleViolationProvider'</summary>
		internal DataColumn RuleViolationProviderColumn 
		{
			get { return _columnRuleViolationProvider; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsResolved'</summary>
		internal DataColumn IsResolvedColumn 
		{
			get { return _columnIsResolved; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AggregationFrom'</summary>
		internal DataColumn AggregationFromColumn 
		{
			get { return _columnAggregationFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AggregationTo'</summary>
		internal DataColumn AggregationToColumn 
		{
			get { return _columnAggregationTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RuleViolationSourceType'</summary>
		internal DataColumn RuleViolationSourceTypeColumn 
		{
			get { return _columnRuleViolationSourceType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardID'</summary>
		internal DataColumn CardIDColumn 
		{
			get { return _columnCardID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SerialNumber'</summary>
		internal DataColumn SerialNumberColumn 
		{
			get { return _columnSerialNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PrintedNumber'</summary>
		internal DataColumn PrintedNumberColumn 
		{
			get { return _columnPrintedNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardState'</summary>
		internal DataColumn CardStateColumn 
		{
			get { return _columnCardState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardLastUsed'</summary>
		internal DataColumn CardLastUsedColumn 
		{
			get { return _columnCardLastUsed; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardExpiration'</summary>
		internal DataColumn CardExpirationColumn 
		{
			get { return _columnCardExpiration; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardIsBlocked'</summary>
		internal DataColumn CardIsBlockedColumn 
		{
			get { return _columnCardIsBlocked; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardBlockingReason'</summary>
		internal DataColumn CardBlockingReasonColumn 
		{
			get { return _columnCardBlockingReason; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardBlockingComment'</summary>
		internal DataColumn CardBlockingCommentColumn 
		{
			get { return _columnCardBlockingComment; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardType'</summary>
		internal DataColumn CardTypeColumn 
		{
			get { return _columnCardType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaID'</summary>
		internal DataColumn FareMediaIDColumn 
		{
			get { return _columnFareMediaID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketType'</summary>
		internal DataColumn TicketTypeColumn 
		{
			get { return _columnTicketType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductIsCanceled'</summary>
		internal DataColumn ProductIsCanceledColumn 
		{
			get { return _columnProductIsCanceled; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Price'</summary>
		internal DataColumn PriceColumn 
		{
			get { return _columnPrice; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductIsBlocked'</summary>
		internal DataColumn ProductIsBlockedColumn 
		{
			get { return _columnProductIsBlocked; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductBlockingReason'</summary>
		internal DataColumn ProductBlockingReasonColumn 
		{
			get { return _columnProductBlockingReason; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductBlockingDate'</summary>
		internal DataColumn ProductBlockingDateColumn 
		{
			get { return _columnProductBlockingDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductCancellationDate'</summary>
		internal DataColumn ProductCancellationDateColumn 
		{
			get { return _columnProductCancellationDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionID'</summary>
		internal DataColumn PaymentOptionIDColumn 
		{
			get { return _columnPaymentOptionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractID'</summary>
		internal DataColumn ContractIDColumn 
		{
			get { return _columnContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentMethod'</summary>
		internal DataColumn PaymentMethodColumn 
		{
			get { return _columnPaymentMethod; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionDescription'</summary>
		internal DataColumn PaymentOptionDescriptionColumn 
		{
			get { return _columnPaymentOptionDescription; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankingServiceToken'</summary>
		internal DataColumn BankingServiceTokenColumn 
		{
			get { return _columnBankingServiceToken; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionExpiry'</summary>
		internal DataColumn PaymentOptionExpiryColumn 
		{
			get { return _columnPaymentOptionExpiry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionCreditCardNetwork'</summary>
		internal DataColumn PaymentOptionCreditCardNetworkColumn 
		{
			get { return _columnPaymentOptionCreditCardNetwork; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionPriority'</summary>
		internal DataColumn PaymentOptionPriorityColumn 
		{
			get { return _columnPaymentOptionPriority; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionName'</summary>
		internal DataColumn PaymentOptionNameColumn 
		{
			get { return _columnPaymentOptionName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsTemporary'</summary>
		internal DataColumn IsTemporaryColumn 
		{
			get { return _columnIsTemporary; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionIsRetired'</summary>
		internal DataColumn PaymentOptionIsRetiredColumn 
		{
			get { return _columnPaymentOptionIsRetired; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankConnectionDataID'</summary>
		internal DataColumn BankConnectionDataIDColumn 
		{
			get { return _columnBankConnectionDataID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProspectivePaymentOptionID'</summary>
		internal DataColumn ProspectivePaymentOptionIDColumn 
		{
			get { return _columnProspectivePaymentOptionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentOptionIsBlocked'</summary>
		internal DataColumn PaymentOptionIsBlockedColumn 
		{
			get { return _columnPaymentOptionIsBlocked; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable RuleViolationDetail</summary>
	public partial class RuleViolationDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private RuleViolationDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal RuleViolationDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((RuleViolationDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field RuleViolationID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."RULEVIOLATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RuleViolationID
		{
			get { return IsRuleViolationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RuleViolationIDColumn]; }
			set { this[_parent.RuleViolationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RuleViolationID is NULL, false otherwise.</summary>
		public bool IsRuleViolationIDNull() 
		{
			return IsNull(_parent.RuleViolationIDColumn);
		}

		/// <summary>Sets the TypedView field RuleViolationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRuleViolationIDNull() 
		{
			this[_parent.RuleViolationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RuleViolationType</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."RULEVIOLATIONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.RuleViolationType RuleViolationType
		{
			get { return IsRuleViolationTypeNull() ? (VarioSL.Entities.Enumerations.RuleViolationType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.RuleViolationType)) : (VarioSL.Entities.Enumerations.RuleViolationType)this[_parent.RuleViolationTypeColumn]; }
			set { this[_parent.RuleViolationTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RuleViolationType is NULL, false otherwise.</summary>
		public bool IsRuleViolationTypeNull() 
		{
			return IsNull(_parent.RuleViolationTypeColumn);
		}

		/// <summary>Sets the TypedView field RuleViolationType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRuleViolationTypeNull() 
		{
			this[_parent.RuleViolationTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ViolationValue</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."VIOLATIONVALUE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String ViolationValue
		{
			get { return IsViolationValueNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ViolationValueColumn]; }
			set { this[_parent.ViolationValueColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ViolationValue is NULL, false otherwise.</summary>
		public bool IsViolationValueNull() 
		{
			return IsNull(_parent.ViolationValueColumn);
		}

		/// <summary>Sets the TypedView field ViolationValue to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetViolationValueNull() 
		{
			this[_parent.ViolationValueColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Message</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."MESSAGE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String Message
		{
			get { return IsMessageNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.MessageColumn]; }
			set { this[_parent.MessageColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Message is NULL, false otherwise.</summary>
		public bool IsMessageNull() 
		{
			return IsNull(_parent.MessageColumn);
		}

		/// <summary>Sets the TypedView field Message to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMessageNull() 
		{
			this[_parent.MessageColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExecutionTime</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."EXECUTIONTIME"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ExecutionTime
		{
			get { return IsExecutionTimeNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ExecutionTimeColumn]; }
			set { this[_parent.ExecutionTimeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExecutionTime is NULL, false otherwise.</summary>
		public bool IsExecutionTimeNull() 
		{
			return IsNull(_parent.ExecutionTimeColumn);
		}

		/// <summary>Sets the TypedView field ExecutionTime to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExecutionTimeNull() 
		{
			this[_parent.ExecutionTimeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RuleViolationText</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."RULEVIOLATIONTEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String RuleViolationText
		{
			get { return IsRuleViolationTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.RuleViolationTextColumn]; }
			set { this[_parent.RuleViolationTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RuleViolationText is NULL, false otherwise.</summary>
		public bool IsRuleViolationTextNull() 
		{
			return IsNull(_parent.RuleViolationTextColumn);
		}

		/// <summary>Sets the TypedView field RuleViolationText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRuleViolationTextNull() 
		{
			this[_parent.RuleViolationTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RuleViolationProvider</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."RULEVIOLATIONPROVIDER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.RuleViolationProvider RuleViolationProvider
		{
			get { return IsRuleViolationProviderNull() ? (VarioSL.Entities.Enumerations.RuleViolationProvider)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.RuleViolationProvider)) : (VarioSL.Entities.Enumerations.RuleViolationProvider)this[_parent.RuleViolationProviderColumn]; }
			set { this[_parent.RuleViolationProviderColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RuleViolationProvider is NULL, false otherwise.</summary>
		public bool IsRuleViolationProviderNull() 
		{
			return IsNull(_parent.RuleViolationProviderColumn);
		}

		/// <summary>Sets the TypedView field RuleViolationProvider to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRuleViolationProviderNull() 
		{
			this[_parent.RuleViolationProviderColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsResolved</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."ISRESOLVED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsResolved
		{
			get { return IsIsResolvedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsResolvedColumn]; }
			set { this[_parent.IsResolvedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsResolved is NULL, false otherwise.</summary>
		public bool IsIsResolvedNull() 
		{
			return IsNull(_parent.IsResolvedColumn);
		}

		/// <summary>Sets the TypedView field IsResolved to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsResolvedNull() 
		{
			this[_parent.IsResolvedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AggregationFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."AGGREGATIONFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime AggregationFrom
		{
			get { return IsAggregationFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.AggregationFromColumn]; }
			set { this[_parent.AggregationFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AggregationFrom is NULL, false otherwise.</summary>
		public bool IsAggregationFromNull() 
		{
			return IsNull(_parent.AggregationFromColumn);
		}

		/// <summary>Sets the TypedView field AggregationFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAggregationFromNull() 
		{
			this[_parent.AggregationFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AggregationTo</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."AGGREGATIONTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime AggregationTo
		{
			get { return IsAggregationToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.AggregationToColumn]; }
			set { this[_parent.AggregationToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AggregationTo is NULL, false otherwise.</summary>
		public bool IsAggregationToNull() 
		{
			return IsNull(_parent.AggregationToColumn);
		}

		/// <summary>Sets the TypedView field AggregationTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAggregationToNull() 
		{
			this[_parent.AggregationToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RuleViolationSourceType</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."RULEVIOLATIONSOURCETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.RuleViolationSourceType RuleViolationSourceType
		{
			get { return IsRuleViolationSourceTypeNull() ? (VarioSL.Entities.Enumerations.RuleViolationSourceType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.RuleViolationSourceType)) : (VarioSL.Entities.Enumerations.RuleViolationSourceType)this[_parent.RuleViolationSourceTypeColumn]; }
			set { this[_parent.RuleViolationSourceTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RuleViolationSourceType is NULL, false otherwise.</summary>
		public bool IsRuleViolationSourceTypeNull() 
		{
			return IsNull(_parent.RuleViolationSourceTypeColumn);
		}

		/// <summary>Sets the TypedView field RuleViolationSourceType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRuleViolationSourceTypeNull() 
		{
			this[_parent.RuleViolationSourceTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CardID
		{
			get { return IsCardIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardIDColumn]; }
			set { this[_parent.CardIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardID is NULL, false otherwise.</summary>
		public bool IsCardIDNull() 
		{
			return IsNull(_parent.CardIDColumn);
		}

		/// <summary>Sets the TypedView field CardID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardIDNull() 
		{
			this[_parent.CardIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SerialNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."SERIALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String SerialNumber
		{
			get { return IsSerialNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SerialNumberColumn]; }
			set { this[_parent.SerialNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SerialNumber is NULL, false otherwise.</summary>
		public bool IsSerialNumberNull() 
		{
			return IsNull(_parent.SerialNumberColumn);
		}

		/// <summary>Sets the TypedView field SerialNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSerialNumberNull() 
		{
			this[_parent.SerialNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PrintedNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String PrintedNumber
		{
			get { return IsPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PrintedNumberColumn]; }
			set { this[_parent.PrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PrintedNumber is NULL, false otherwise.</summary>
		public bool IsPrintedNumberNull() 
		{
			return IsNull(_parent.PrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field PrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPrintedNumberNull() 
		{
			this[_parent.PrintedNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardState</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.CardState CardState
		{
			get { return IsCardStateNull() ? (VarioSL.Entities.Enumerations.CardState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.CardState)) : (VarioSL.Entities.Enumerations.CardState)this[_parent.CardStateColumn]; }
			set { this[_parent.CardStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardState is NULL, false otherwise.</summary>
		public bool IsCardStateNull() 
		{
			return IsNull(_parent.CardStateColumn);
		}

		/// <summary>Sets the TypedView field CardState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardStateNull() 
		{
			this[_parent.CardStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardLastUsed</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDLASTUSED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CardLastUsed
		{
			get { return IsCardLastUsedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CardLastUsedColumn]; }
			set { this[_parent.CardLastUsedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardLastUsed is NULL, false otherwise.</summary>
		public bool IsCardLastUsedNull() 
		{
			return IsNull(_parent.CardLastUsedColumn);
		}

		/// <summary>Sets the TypedView field CardLastUsed to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardLastUsedNull() 
		{
			this[_parent.CardLastUsedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardExpiration</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDEXPIRATION"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CardExpiration
		{
			get { return IsCardExpirationNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CardExpirationColumn]; }
			set { this[_parent.CardExpirationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardExpiration is NULL, false otherwise.</summary>
		public bool IsCardExpirationNull() 
		{
			return IsNull(_parent.CardExpirationColumn);
		}

		/// <summary>Sets the TypedView field CardExpiration to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardExpirationNull() 
		{
			this[_parent.CardExpirationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardIsBlocked</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDISBLOCKED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CardIsBlocked
		{
			get { return IsCardIsBlockedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CardIsBlockedColumn]; }
			set { this[_parent.CardIsBlockedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardIsBlocked is NULL, false otherwise.</summary>
		public bool IsCardIsBlockedNull() 
		{
			return IsNull(_parent.CardIsBlockedColumn);
		}

		/// <summary>Sets the TypedView field CardIsBlocked to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardIsBlockedNull() 
		{
			this[_parent.CardIsBlockedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardBlockingReason</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDBLOCKINGREASON"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CardBlockingReason
		{
			get { return IsCardBlockingReasonNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CardBlockingReasonColumn]; }
			set { this[_parent.CardBlockingReasonColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardBlockingReason is NULL, false otherwise.</summary>
		public bool IsCardBlockingReasonNull() 
		{
			return IsNull(_parent.CardBlockingReasonColumn);
		}

		/// <summary>Sets the TypedView field CardBlockingReason to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardBlockingReasonNull() 
		{
			this[_parent.CardBlockingReasonColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardBlockingComment</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDBLOCKINGCOMMENT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250</remarks>
		public System.String CardBlockingComment
		{
			get { return IsCardBlockingCommentNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardBlockingCommentColumn]; }
			set { this[_parent.CardBlockingCommentColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardBlockingComment is NULL, false otherwise.</summary>
		public bool IsCardBlockingCommentNull() 
		{
			return IsNull(_parent.CardBlockingCommentColumn);
		}

		/// <summary>Sets the TypedView field CardBlockingComment to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardBlockingCommentNull() 
		{
			this[_parent.CardBlockingCommentColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardType</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CARDTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CardType
		{
			get { return IsCardTypeNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardTypeColumn]; }
			set { this[_parent.CardTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardType is NULL, false otherwise.</summary>
		public bool IsCardTypeNull() 
		{
			return IsNull(_parent.CardTypeColumn);
		}

		/// <summary>Sets the TypedView field CardType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardTypeNull() 
		{
			this[_parent.CardTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."FAREMEDIAID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String FareMediaID
		{
			get { return IsFareMediaIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaIDColumn]; }
			set { this[_parent.FareMediaIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaID is NULL, false otherwise.</summary>
		public bool IsFareMediaIDNull() 
		{
			return IsNull(_parent.FareMediaIDColumn);
		}

		/// <summary>Sets the TypedView field FareMediaID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaIDNull() 
		{
			this[_parent.FareMediaIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketType</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."TICKETTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.TicketType TicketType
		{
			get { return IsTicketTypeNull() ? (VarioSL.Entities.Enumerations.TicketType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.TicketType)) : (VarioSL.Entities.Enumerations.TicketType)this[_parent.TicketTypeColumn]; }
			set { this[_parent.TicketTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketType is NULL, false otherwise.</summary>
		public bool IsTicketTypeNull() 
		{
			return IsNull(_parent.TicketTypeColumn);
		}

		/// <summary>Sets the TypedView field TicketType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketTypeNull() 
		{
			this[_parent.TicketTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductIsCanceled</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTISCANCELED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 ProductIsCanceled
		{
			get { return IsProductIsCanceledNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.ProductIsCanceledColumn]; }
			set { this[_parent.ProductIsCanceledColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductIsCanceled is NULL, false otherwise.</summary>
		public bool IsProductIsCanceledNull() 
		{
			return IsNull(_parent.ProductIsCanceledColumn);
		}

		/// <summary>Sets the TypedView field ProductIsCanceled to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIsCanceledNull() 
		{
			this[_parent.ProductIsCanceledColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Price</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRICE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 Price
		{
			get { return IsPriceNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PriceColumn]; }
			set { this[_parent.PriceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Price is NULL, false otherwise.</summary>
		public bool IsPriceNull() 
		{
			return IsNull(_parent.PriceColumn);
		}

		/// <summary>Sets the TypedView field Price to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPriceNull() 
		{
			this[_parent.PriceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductIsBlocked</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTISBLOCKED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 ProductIsBlocked
		{
			get { return IsProductIsBlockedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.ProductIsBlockedColumn]; }
			set { this[_parent.ProductIsBlockedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductIsBlocked is NULL, false otherwise.</summary>
		public bool IsProductIsBlockedNull() 
		{
			return IsNull(_parent.ProductIsBlockedColumn);
		}

		/// <summary>Sets the TypedView field ProductIsBlocked to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIsBlockedNull() 
		{
			this[_parent.ProductIsBlockedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductBlockingReason</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTBLOCKINGREASON"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String ProductBlockingReason
		{
			get { return IsProductBlockingReasonNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ProductBlockingReasonColumn]; }
			set { this[_parent.ProductBlockingReasonColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductBlockingReason is NULL, false otherwise.</summary>
		public bool IsProductBlockingReasonNull() 
		{
			return IsNull(_parent.ProductBlockingReasonColumn);
		}

		/// <summary>Sets the TypedView field ProductBlockingReason to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductBlockingReasonNull() 
		{
			this[_parent.ProductBlockingReasonColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductBlockingDate</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTBLOCKINGDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ProductBlockingDate
		{
			get { return IsProductBlockingDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ProductBlockingDateColumn]; }
			set { this[_parent.ProductBlockingDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductBlockingDate is NULL, false otherwise.</summary>
		public bool IsProductBlockingDateNull() 
		{
			return IsNull(_parent.ProductBlockingDateColumn);
		}

		/// <summary>Sets the TypedView field ProductBlockingDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductBlockingDateNull() 
		{
			this[_parent.ProductBlockingDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductCancellationDate</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PRODUCTCANCELLATIONDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ProductCancellationDate
		{
			get { return IsProductCancellationDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ProductCancellationDateColumn]; }
			set { this[_parent.ProductCancellationDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductCancellationDate is NULL, false otherwise.</summary>
		public bool IsProductCancellationDateNull() 
		{
			return IsNull(_parent.ProductCancellationDateColumn);
		}

		/// <summary>Sets the TypedView field ProductCancellationDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductCancellationDateNull() 
		{
			this[_parent.ProductCancellationDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentOptionID
		{
			get { return IsPaymentOptionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentOptionIDColumn]; }
			set { this[_parent.PaymentOptionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionID is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIDNull() 
		{
			return IsNull(_parent.PaymentOptionIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIDNull() 
		{
			this[_parent.PaymentOptionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."CONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ContractID
		{
			get { return IsContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ContractIDColumn]; }
			set { this[_parent.ContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractID is NULL, false otherwise.</summary>
		public bool IsContractIDNull() 
		{
			return IsNull(_parent.ContractIDColumn);
		}

		/// <summary>Sets the TypedView field ContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractIDNull() 
		{
			this[_parent.ContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentMethod</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTMETHOD"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.PaymentMethod PaymentMethod
		{
			get { return IsPaymentMethodNull() ? (VarioSL.Entities.Enumerations.PaymentMethod)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.PaymentMethod)) : (VarioSL.Entities.Enumerations.PaymentMethod)this[_parent.PaymentMethodColumn]; }
			set { this[_parent.PaymentMethodColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentMethod is NULL, false otherwise.</summary>
		public bool IsPaymentMethodNull() 
		{
			return IsNull(_parent.PaymentMethodColumn);
		}

		/// <summary>Sets the TypedView field PaymentMethod to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentMethodNull() 
		{
			this[_parent.PaymentMethodColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionDescription</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONDESC"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PaymentOptionDescription
		{
			get { return IsPaymentOptionDescriptionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionDescriptionColumn]; }
			set { this[_parent.PaymentOptionDescriptionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionDescription is NULL, false otherwise.</summary>
		public bool IsPaymentOptionDescriptionNull() 
		{
			return IsNull(_parent.PaymentOptionDescriptionColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionDescription to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionDescriptionNull() 
		{
			this[_parent.PaymentOptionDescriptionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankingServiceToken</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."BANKINGSERVICETOKEN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String BankingServiceToken
		{
			get { return IsBankingServiceTokenNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.BankingServiceTokenColumn]; }
			set { this[_parent.BankingServiceTokenColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankingServiceToken is NULL, false otherwise.</summary>
		public bool IsBankingServiceTokenNull() 
		{
			return IsNull(_parent.BankingServiceTokenColumn);
		}

		/// <summary>Sets the TypedView field BankingServiceToken to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankingServiceTokenNull() 
		{
			this[_parent.BankingServiceTokenColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionExpiry</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONEXPIRY"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PaymentOptionExpiry
		{
			get { return IsPaymentOptionExpiryNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PaymentOptionExpiryColumn]; }
			set { this[_parent.PaymentOptionExpiryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionExpiry is NULL, false otherwise.</summary>
		public bool IsPaymentOptionExpiryNull() 
		{
			return IsNull(_parent.PaymentOptionExpiryColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionExpiry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionExpiryNull() 
		{
			this[_parent.PaymentOptionExpiryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionCreditCardNetwork</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONCREDITCARDNET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 20</remarks>
		public System.String PaymentOptionCreditCardNetwork
		{
			get { return IsPaymentOptionCreditCardNetworkNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionCreditCardNetworkColumn]; }
			set { this[_parent.PaymentOptionCreditCardNetworkColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionCreditCardNetwork is NULL, false otherwise.</summary>
		public bool IsPaymentOptionCreditCardNetworkNull() 
		{
			return IsNull(_parent.PaymentOptionCreditCardNetworkColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionCreditCardNetwork to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionCreditCardNetworkNull() 
		{
			this[_parent.PaymentOptionCreditCardNetworkColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionPriority</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONPRIORITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PaymentOptionPriority
		{
			get { return IsPaymentOptionPriorityNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PaymentOptionPriorityColumn]; }
			set { this[_parent.PaymentOptionPriorityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionPriority is NULL, false otherwise.</summary>
		public bool IsPaymentOptionPriorityNull() 
		{
			return IsNull(_parent.PaymentOptionPriorityColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionPriority to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionPriorityNull() 
		{
			this[_parent.PaymentOptionPriorityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionName</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PaymentOptionName
		{
			get { return IsPaymentOptionNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentOptionNameColumn]; }
			set { this[_parent.PaymentOptionNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionName is NULL, false otherwise.</summary>
		public bool IsPaymentOptionNameNull() 
		{
			return IsNull(_parent.PaymentOptionNameColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionNameNull() 
		{
			this[_parent.PaymentOptionNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsTemporary</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."ISTEMPORARY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 IsTemporary
		{
			get { return IsIsTemporaryNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.IsTemporaryColumn]; }
			set { this[_parent.IsTemporaryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsTemporary is NULL, false otherwise.</summary>
		public bool IsIsTemporaryNull() 
		{
			return IsNull(_parent.IsTemporaryColumn);
		}

		/// <summary>Sets the TypedView field IsTemporary to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsTemporaryNull() 
		{
			this[_parent.IsTemporaryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionIsRetired</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONISRETIRED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 PaymentOptionIsRetired
		{
			get { return IsPaymentOptionIsRetiredNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.PaymentOptionIsRetiredColumn]; }
			set { this[_parent.PaymentOptionIsRetiredColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionIsRetired is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIsRetiredNull() 
		{
			return IsNull(_parent.PaymentOptionIsRetiredColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionIsRetired to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIsRetiredNull() 
		{
			this[_parent.PaymentOptionIsRetiredColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankConnectionDataID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."BANKCONNECTIONDATAID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 BankConnectionDataID
		{
			get { return IsBankConnectionDataIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.BankConnectionDataIDColumn]; }
			set { this[_parent.BankConnectionDataIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankConnectionDataID is NULL, false otherwise.</summary>
		public bool IsBankConnectionDataIDNull() 
		{
			return IsNull(_parent.BankConnectionDataIDColumn);
		}

		/// <summary>Sets the TypedView field BankConnectionDataID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankConnectionDataIDNull() 
		{
			this[_parent.BankConnectionDataIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProspectivePaymentOptionID</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PROSPECTIVEPAYMENTOPTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProspectivePaymentOptionID
		{
			get { return IsProspectivePaymentOptionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProspectivePaymentOptionIDColumn]; }
			set { this[_parent.ProspectivePaymentOptionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProspectivePaymentOptionID is NULL, false otherwise.</summary>
		public bool IsProspectivePaymentOptionIDNull() 
		{
			return IsNull(_parent.ProspectivePaymentOptionIDColumn);
		}

		/// <summary>Sets the TypedView field ProspectivePaymentOptionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProspectivePaymentOptionIDNull() 
		{
			this[_parent.ProspectivePaymentOptionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentOptionIsBlocked</summary>
		/// <remarks>Mapped on view field: "VIEW_RULEVIOLATION_DETAILS"."PAYMENTOPTIONISBLOCKED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 PaymentOptionIsBlocked
		{
			get { return IsPaymentOptionIsBlockedNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.PaymentOptionIsBlockedColumn]; }
			set { this[_parent.PaymentOptionIsBlockedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentOptionIsBlocked is NULL, false otherwise.</summary>
		public bool IsPaymentOptionIsBlockedNull() 
		{
			return IsNull(_parent.PaymentOptionIsBlockedColumn);
		}

		/// <summary>Sets the TypedView field PaymentOptionIsBlocked to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentOptionIsBlockedNull() 
		{
			this[_parent.PaymentOptionIsBlockedColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
