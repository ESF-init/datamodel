﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'SaleHistory'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class SaleHistoryTypedView : TypedViewBase<SaleHistoryRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnSaleID;
		private DataColumn _columnSaleTransactionID;
		private DataColumn _columnReceiptReference;
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnSaleType;
		private DataColumn _columnLocationNumber;
		private DataColumn _columnDeviceNumber;
		private DataColumn _columnSalesPersonNumber;
		private DataColumn _columnMerchantNumber;
		private DataColumn _columnNetworkReference;
		private DataColumn _columnSaleDate;
		private DataColumn _columnOperatorID;
		private DataColumn _columnAccountID;
		private DataColumn _columnSecondaryAccountID;
		private DataColumn _columnExternalOrderNumber;
		private DataColumn _columnOrderID;
		private DataColumn _columnNotes;
		private DataColumn _columnState;
		private DataColumn _columnOrderNumber;
		private DataColumn _columnIsCanceled;
		private DataColumn _columnAmount;
		private DataColumn _columnLastCustomer;
		private DataColumn _columnSalesPersonEmail;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 23;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static SaleHistoryTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SaleHistoryTypedView():base("SaleHistory")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SaleHistoryTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SaleHistoryTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SaleHistoryTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new SaleHistoryRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleTransactionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ReceiptReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LocationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DeviceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesPersonNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MerchantNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NetworkReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OperatorID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SecondaryAccountID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ExternalOrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsCanceled", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastCustomer", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesPersonEmail", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "SaleHistory";
			_columnSaleID = GeneralUtils.CreateTypedDataTableColumn("SaleID", @"SaleID", typeof(System.Int64), this.Columns);
			_columnSaleTransactionID = GeneralUtils.CreateTypedDataTableColumn("SaleTransactionID", @"SaleTransactionID", typeof(System.String), this.Columns);
			_columnReceiptReference = GeneralUtils.CreateTypedDataTableColumn("ReceiptReference", @"ReceiptReference", typeof(System.String), this.Columns);
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnSaleType = GeneralUtils.CreateTypedDataTableColumn("SaleType", @"SaleType", typeof(System.Int32), this.Columns);
			_columnLocationNumber = GeneralUtils.CreateTypedDataTableColumn("LocationNumber", @"LocationNumber", typeof(System.Int32), this.Columns);
			_columnDeviceNumber = GeneralUtils.CreateTypedDataTableColumn("DeviceNumber", @"DeviceNumber", typeof(System.Int32), this.Columns);
			_columnSalesPersonNumber = GeneralUtils.CreateTypedDataTableColumn("SalesPersonNumber", @"SalesPersonNumber", typeof(System.Int32), this.Columns);
			_columnMerchantNumber = GeneralUtils.CreateTypedDataTableColumn("MerchantNumber", @"MerchantNumber", typeof(System.Int32), this.Columns);
			_columnNetworkReference = GeneralUtils.CreateTypedDataTableColumn("NetworkReference", @"NetworkReference", typeof(System.String), this.Columns);
			_columnSaleDate = GeneralUtils.CreateTypedDataTableColumn("SaleDate", @"SaleDate", typeof(System.DateTime), this.Columns);
			_columnOperatorID = GeneralUtils.CreateTypedDataTableColumn("OperatorID", @"OperatorID", typeof(System.Int64), this.Columns);
			_columnAccountID = GeneralUtils.CreateTypedDataTableColumn("AccountID", @"AccountID", typeof(System.Int64), this.Columns);
			_columnSecondaryAccountID = GeneralUtils.CreateTypedDataTableColumn("SecondaryAccountID", @"SecondaryAccountID", typeof(System.Int64), this.Columns);
			_columnExternalOrderNumber = GeneralUtils.CreateTypedDataTableColumn("ExternalOrderNumber", @"ExternalOrderNumber", typeof(System.String), this.Columns);
			_columnOrderID = GeneralUtils.CreateTypedDataTableColumn("OrderID", @"OrderID", typeof(System.Int64), this.Columns);
			_columnNotes = GeneralUtils.CreateTypedDataTableColumn("Notes", @"Notes", typeof(System.String), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(System.Int32), this.Columns);
			_columnOrderNumber = GeneralUtils.CreateTypedDataTableColumn("OrderNumber", @"OrderNumber", typeof(System.String), this.Columns);
			_columnIsCanceled = GeneralUtils.CreateTypedDataTableColumn("IsCanceled", @"IsCanceled", typeof(System.Boolean), this.Columns);
			_columnAmount = GeneralUtils.CreateTypedDataTableColumn("Amount", @"Amount", typeof(System.Decimal), this.Columns);
			_columnLastCustomer = GeneralUtils.CreateTypedDataTableColumn("LastCustomer", @"LastCustomer", typeof(System.String), this.Columns);
			_columnSalesPersonEmail = GeneralUtils.CreateTypedDataTableColumn("SalesPersonEmail", @"SalesPersonEmail", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnSaleID = this.Columns["SaleID"];
			_columnSaleTransactionID = this.Columns["SaleTransactionID"];
			_columnReceiptReference = this.Columns["ReceiptReference"];
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnSaleType = this.Columns["SaleType"];
			_columnLocationNumber = this.Columns["LocationNumber"];
			_columnDeviceNumber = this.Columns["DeviceNumber"];
			_columnSalesPersonNumber = this.Columns["SalesPersonNumber"];
			_columnMerchantNumber = this.Columns["MerchantNumber"];
			_columnNetworkReference = this.Columns["NetworkReference"];
			_columnSaleDate = this.Columns["SaleDate"];
			_columnOperatorID = this.Columns["OperatorID"];
			_columnAccountID = this.Columns["AccountID"];
			_columnSecondaryAccountID = this.Columns["SecondaryAccountID"];
			_columnExternalOrderNumber = this.Columns["ExternalOrderNumber"];
			_columnOrderID = this.Columns["OrderID"];
			_columnNotes = this.Columns["Notes"];
			_columnState = this.Columns["State"];
			_columnOrderNumber = this.Columns["OrderNumber"];
			_columnIsCanceled = this.Columns["IsCanceled"];
			_columnAmount = this.Columns["Amount"];
			_columnLastCustomer = this.Columns["LastCustomer"];
			_columnSalesPersonEmail = this.Columns["SalesPersonEmail"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			SaleHistoryTypedView cloneToReturn = ((SaleHistoryTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new SaleHistoryTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return SaleHistoryTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return SaleHistoryTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'SaleID'</summary>
		internal DataColumn SaleIDColumn 
		{
			get { return _columnSaleID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleTransactionID'</summary>
		internal DataColumn SaleTransactionIDColumn 
		{
			get { return _columnSaleTransactionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ReceiptReference'</summary>
		internal DataColumn ReceiptReferenceColumn 
		{
			get { return _columnReceiptReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleType'</summary>
		internal DataColumn SaleTypeColumn 
		{
			get { return _columnSaleType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LocationNumber'</summary>
		internal DataColumn LocationNumberColumn 
		{
			get { return _columnLocationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DeviceNumber'</summary>
		internal DataColumn DeviceNumberColumn 
		{
			get { return _columnDeviceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesPersonNumber'</summary>
		internal DataColumn SalesPersonNumberColumn 
		{
			get { return _columnSalesPersonNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MerchantNumber'</summary>
		internal DataColumn MerchantNumberColumn 
		{
			get { return _columnMerchantNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NetworkReference'</summary>
		internal DataColumn NetworkReferenceColumn 
		{
			get { return _columnNetworkReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleDate'</summary>
		internal DataColumn SaleDateColumn 
		{
			get { return _columnSaleDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OperatorID'</summary>
		internal DataColumn OperatorIDColumn 
		{
			get { return _columnOperatorID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AccountID'</summary>
		internal DataColumn AccountIDColumn 
		{
			get { return _columnAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SecondaryAccountID'</summary>
		internal DataColumn SecondaryAccountIDColumn 
		{
			get { return _columnSecondaryAccountID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ExternalOrderNumber'</summary>
		internal DataColumn ExternalOrderNumberColumn 
		{
			get { return _columnExternalOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderID'</summary>
		internal DataColumn OrderIDColumn 
		{
			get { return _columnOrderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Notes'</summary>
		internal DataColumn NotesColumn 
		{
			get { return _columnNotes; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderNumber'</summary>
		internal DataColumn OrderNumberColumn 
		{
			get { return _columnOrderNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsCanceled'</summary>
		internal DataColumn IsCanceledColumn 
		{
			get { return _columnIsCanceled; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Amount'</summary>
		internal DataColumn AmountColumn 
		{
			get { return _columnAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastCustomer'</summary>
		internal DataColumn LastCustomerColumn 
		{
			get { return _columnLastCustomer; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SalesPersonEmail'</summary>
		internal DataColumn SalesPersonEmailColumn 
		{
			get { return _columnSalesPersonEmail; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable SaleHistory</summary>
	public partial class SaleHistoryRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private SaleHistoryTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal SaleHistoryRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((SaleHistoryTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field SaleID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SaleID
		{
			get { return IsSaleIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SaleIDColumn]; }
			set { this[_parent.SaleIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleID is NULL, false otherwise.</summary>
		public bool IsSaleIDNull() 
		{
			return IsNull(_parent.SaleIDColumn);
		}

		/// <summary>Sets the TypedView field SaleID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleIDNull() 
		{
			this[_parent.SaleIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleTransactionID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALETRANSACTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String SaleTransactionID
		{
			get { return IsSaleTransactionIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SaleTransactionIDColumn]; }
			set { this[_parent.SaleTransactionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleTransactionID is NULL, false otherwise.</summary>
		public bool IsSaleTransactionIDNull() 
		{
			return IsNull(_parent.SaleTransactionIDColumn);
		}

		/// <summary>Sets the TypedView field SaleTransactionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTransactionIDNull() 
		{
			this[_parent.SaleTransactionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ReceiptReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."RECEIPTREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String ReceiptReference
		{
			get { return IsReceiptReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ReceiptReferenceColumn]; }
			set { this[_parent.ReceiptReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ReceiptReference is NULL, false otherwise.</summary>
		public bool IsReceiptReferenceNull() 
		{
			return IsNull(_parent.ReceiptReferenceColumn);
		}

		/// <summary>Sets the TypedView field ReceiptReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetReceiptReferenceNull() 
		{
			this[_parent.ReceiptReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SaleType
		{
			get { return IsSaleTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SaleTypeColumn]; }
			set { this[_parent.SaleTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleType is NULL, false otherwise.</summary>
		public bool IsSaleTypeNull() 
		{
			return IsNull(_parent.SaleTypeColumn);
		}

		/// <summary>Sets the TypedView field SaleType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTypeNull() 
		{
			this[_parent.SaleTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LocationNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."LOCATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 LocationNumber
		{
			get { return IsLocationNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.LocationNumberColumn]; }
			set { this[_parent.LocationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LocationNumber is NULL, false otherwise.</summary>
		public bool IsLocationNumberNull() 
		{
			return IsNull(_parent.LocationNumberColumn);
		}

		/// <summary>Sets the TypedView field LocationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLocationNumberNull() 
		{
			this[_parent.LocationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DeviceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."DEVICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 DeviceNumber
		{
			get { return IsDeviceNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.DeviceNumberColumn]; }
			set { this[_parent.DeviceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DeviceNumber is NULL, false otherwise.</summary>
		public bool IsDeviceNumberNull() 
		{
			return IsNull(_parent.DeviceNumberColumn);
		}

		/// <summary>Sets the TypedView field DeviceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDeviceNumberNull() 
		{
			this[_parent.DeviceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesPersonNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALESPERSONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SalesPersonNumber
		{
			get { return IsSalesPersonNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SalesPersonNumberColumn]; }
			set { this[_parent.SalesPersonNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesPersonNumber is NULL, false otherwise.</summary>
		public bool IsSalesPersonNumberNull() 
		{
			return IsNull(_parent.SalesPersonNumberColumn);
		}

		/// <summary>Sets the TypedView field SalesPersonNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesPersonNumberNull() 
		{
			this[_parent.SalesPersonNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MerchantNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."MERCHANTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MerchantNumber
		{
			get { return IsMerchantNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MerchantNumberColumn]; }
			set { this[_parent.MerchantNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MerchantNumber is NULL, false otherwise.</summary>
		public bool IsMerchantNumberNull() 
		{
			return IsNull(_parent.MerchantNumberColumn);
		}

		/// <summary>Sets the TypedView field MerchantNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMerchantNumberNull() 
		{
			this[_parent.MerchantNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NetworkReference</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."NETWORKREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String NetworkReference
		{
			get { return IsNetworkReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NetworkReferenceColumn]; }
			set { this[_parent.NetworkReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NetworkReference is NULL, false otherwise.</summary>
		public bool IsNetworkReferenceNull() 
		{
			return IsNull(_parent.NetworkReferenceColumn);
		}

		/// <summary>Sets the TypedView field NetworkReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNetworkReferenceNull() 
		{
			this[_parent.NetworkReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALEDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime SaleDate
		{
			get { return IsSaleDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.SaleDateColumn]; }
			set { this[_parent.SaleDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleDate is NULL, false otherwise.</summary>
		public bool IsSaleDateNull() 
		{
			return IsNull(_parent.SaleDateColumn);
		}

		/// <summary>Sets the TypedView field SaleDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleDateNull() 
		{
			this[_parent.SaleDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OperatorID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."OPERATORID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OperatorID
		{
			get { return IsOperatorIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OperatorIDColumn]; }
			set { this[_parent.OperatorIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OperatorID is NULL, false otherwise.</summary>
		public bool IsOperatorIDNull() 
		{
			return IsNull(_parent.OperatorIDColumn);
		}

		/// <summary>Sets the TypedView field OperatorID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOperatorIDNull() 
		{
			this[_parent.OperatorIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."ACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 AccountID
		{
			get { return IsAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AccountIDColumn]; }
			set { this[_parent.AccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountID is NULL, false otherwise.</summary>
		public bool IsAccountIDNull() 
		{
			return IsNull(_parent.AccountIDColumn);
		}

		/// <summary>Sets the TypedView field AccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountIDNull() 
		{
			this[_parent.AccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SecondaryAccountID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SECONDARYACCOUNTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SecondaryAccountID
		{
			get { return IsSecondaryAccountIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SecondaryAccountIDColumn]; }
			set { this[_parent.SecondaryAccountIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SecondaryAccountID is NULL, false otherwise.</summary>
		public bool IsSecondaryAccountIDNull() 
		{
			return IsNull(_parent.SecondaryAccountIDColumn);
		}

		/// <summary>Sets the TypedView field SecondaryAccountID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSecondaryAccountIDNull() 
		{
			this[_parent.SecondaryAccountIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ExternalOrderNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."EXTERNALORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ExternalOrderNumber
		{
			get { return IsExternalOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ExternalOrderNumberColumn]; }
			set { this[_parent.ExternalOrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ExternalOrderNumber is NULL, false otherwise.</summary>
		public bool IsExternalOrderNumberNull() 
		{
			return IsNull(_parent.ExternalOrderNumberColumn);
		}

		/// <summary>Sets the TypedView field ExternalOrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExternalOrderNumberNull() 
		{
			this[_parent.ExternalOrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."ORDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderID
		{
			get { return IsOrderIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderIDColumn]; }
			set { this[_parent.OrderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderID is NULL, false otherwise.</summary>
		public bool IsOrderIDNull() 
		{
			return IsNull(_parent.OrderIDColumn);
		}

		/// <summary>Sets the TypedView field OrderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderIDNull() 
		{
			this[_parent.OrderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Notes</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."NOTES"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 500</remarks>
		public System.String Notes
		{
			get { return IsNotesNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.NotesColumn]; }
			set { this[_parent.NotesColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Notes is NULL, false otherwise.</summary>
		public bool IsNotesNull() 
		{
			return IsNull(_parent.NotesColumn);
		}

		/// <summary>Sets the TypedView field Notes to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNotesNull() 
		{
			this[_parent.NotesColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 State
		{
			get { return IsStateNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."ORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrderNumber
		{
			get { return IsOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrderNumberColumn]; }
			set { this[_parent.OrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderNumber is NULL, false otherwise.</summary>
		public bool IsOrderNumberNull() 
		{
			return IsNull(_parent.OrderNumberColumn);
		}

		/// <summary>Sets the TypedView field OrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderNumberNull() 
		{
			this[_parent.OrderNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsCanceled</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."ISCANCELED"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Boolean IsCanceled
		{
			get { return IsIsCanceledNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsCanceledColumn]; }
			set { this[_parent.IsCanceledColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsCanceled is NULL, false otherwise.</summary>
		public bool IsIsCanceledNull() 
		{
			return IsNull(_parent.IsCanceledColumn);
		}

		/// <summary>Sets the TypedView field IsCanceled to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsCanceledNull() 
		{
			this[_parent.IsCanceledColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Amount</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."AMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal Amount
		{
			get { return IsAmountNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.AmountColumn]; }
			set { this[_parent.AmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Amount is NULL, false otherwise.</summary>
		public bool IsAmountNull() 
		{
			return IsNull(_parent.AmountColumn);
		}

		/// <summary>Sets the TypedView field Amount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAmountNull() 
		{
			this[_parent.AmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastCustomer</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."LASTCUSTOMER"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String LastCustomer
		{
			get { return IsLastCustomerNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastCustomerColumn]; }
			set { this[_parent.LastCustomerColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastCustomer is NULL, false otherwise.</summary>
		public bool IsLastCustomerNull() 
		{
			return IsNull(_parent.LastCustomerColumn);
		}

		/// <summary>Sets the TypedView field LastCustomer to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastCustomerNull() 
		{
			this[_parent.LastCustomerColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SalesPersonEmail</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_SALEHISTORY"."SALESPERSONEMAIL"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512</remarks>
		public System.String SalesPersonEmail
		{
			get { return IsSalesPersonEmailNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SalesPersonEmailColumn]; }
			set { this[_parent.SalesPersonEmailColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesPersonEmail is NULL, false otherwise.</summary>
		public bool IsSalesPersonEmailNull() 
		{
			return IsNull(_parent.SalesPersonEmailColumn);
		}

		/// <summary>Sets the TypedView field SalesPersonEmail to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesPersonEmailNull() 
		{
			this[_parent.SalesPersonEmailColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
