﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'RecognizedPayment'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class RecognizedPaymentTypedView : TypedViewBase<RecognizedPaymentRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnSalesChannelID;
		private DataColumn _columnMerchantNumber;
		private DataColumn _columnSaleType;
		private DataColumn _columnIsOrganizational;
		private DataColumn _columnClientID;
		private DataColumn _columnPaymentType;
		private DataColumn _columnPaymentRecognitionID;
		private DataColumn _columnPaymentJournalID;
		private DataColumn _columnPostingDate;
		private DataColumn _columnPostingReference;
		private DataColumn _columnCloseoutPeriodID;
		private DataColumn _columnCreditAccountNumber;
		private DataColumn _columnDebitAccountNumber;
		private DataColumn _columnAmount;
		private DataColumn _columnLastUser;
		private DataColumn _columnLastModified;
		private DataColumn _columnTransactionCounter;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 17;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static RecognizedPaymentTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public RecognizedPaymentTypedView():base("RecognizedPayment")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RecognizedPaymentTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.RecognizedPaymentTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.RecognizedPaymentTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new RecognizedPaymentRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SalesChannelID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("MerchantNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SaleType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsOrganizational", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentRecognitionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentJournalID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostingReference", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CloseoutPeriodID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreditAccountNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DebitAccountNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Amount", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "RecognizedPayment";
			_columnSalesChannelID = GeneralUtils.CreateTypedDataTableColumn("SalesChannelID", @"SalesChannelID", typeof(System.Int64), this.Columns);
			_columnMerchantNumber = GeneralUtils.CreateTypedDataTableColumn("MerchantNumber", @"MerchantNumber", typeof(System.Int32), this.Columns);
			_columnSaleType = GeneralUtils.CreateTypedDataTableColumn("SaleType", @"SaleType", typeof(System.Int32), this.Columns);
			_columnIsOrganizational = GeneralUtils.CreateTypedDataTableColumn("IsOrganizational", @"IsOrganizational", typeof(System.Boolean), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnPaymentType = GeneralUtils.CreateTypedDataTableColumn("PaymentType", @"PaymentType", typeof(System.Int32), this.Columns);
			_columnPaymentRecognitionID = GeneralUtils.CreateTypedDataTableColumn("PaymentRecognitionID", @"PaymentRecognitionID", typeof(System.Int64), this.Columns);
			_columnPaymentJournalID = GeneralUtils.CreateTypedDataTableColumn("PaymentJournalID", @"PaymentJournalID", typeof(System.Int64), this.Columns);
			_columnPostingDate = GeneralUtils.CreateTypedDataTableColumn("PostingDate", @"PostingDate", typeof(System.DateTime), this.Columns);
			_columnPostingReference = GeneralUtils.CreateTypedDataTableColumn("PostingReference", @"PostingReference", typeof(System.String), this.Columns);
			_columnCloseoutPeriodID = GeneralUtils.CreateTypedDataTableColumn("CloseoutPeriodID", @"CloseoutPeriodID", typeof(System.Int64), this.Columns);
			_columnCreditAccountNumber = GeneralUtils.CreateTypedDataTableColumn("CreditAccountNumber", @"CreditAccountNumber", typeof(System.String), this.Columns);
			_columnDebitAccountNumber = GeneralUtils.CreateTypedDataTableColumn("DebitAccountNumber", @"DebitAccountNumber", typeof(System.String), this.Columns);
			_columnAmount = GeneralUtils.CreateTypedDataTableColumn("Amount", @"Amount", typeof(System.Int64), this.Columns);
			_columnLastUser = GeneralUtils.CreateTypedDataTableColumn("LastUser", @"LastUser", typeof(System.String), this.Columns);
			_columnLastModified = GeneralUtils.CreateTypedDataTableColumn("LastModified", @"LastModified", typeof(System.DateTime), this.Columns);
			_columnTransactionCounter = GeneralUtils.CreateTypedDataTableColumn("TransactionCounter", @"TransactionCounter", typeof(System.Decimal), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnSalesChannelID = this.Columns["SalesChannelID"];
			_columnMerchantNumber = this.Columns["MerchantNumber"];
			_columnSaleType = this.Columns["SaleType"];
			_columnIsOrganizational = this.Columns["IsOrganizational"];
			_columnClientID = this.Columns["ClientID"];
			_columnPaymentType = this.Columns["PaymentType"];
			_columnPaymentRecognitionID = this.Columns["PaymentRecognitionID"];
			_columnPaymentJournalID = this.Columns["PaymentJournalID"];
			_columnPostingDate = this.Columns["PostingDate"];
			_columnPostingReference = this.Columns["PostingReference"];
			_columnCloseoutPeriodID = this.Columns["CloseoutPeriodID"];
			_columnCreditAccountNumber = this.Columns["CreditAccountNumber"];
			_columnDebitAccountNumber = this.Columns["DebitAccountNumber"];
			_columnAmount = this.Columns["Amount"];
			_columnLastUser = this.Columns["LastUser"];
			_columnLastModified = this.Columns["LastModified"];
			_columnTransactionCounter = this.Columns["TransactionCounter"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			RecognizedPaymentTypedView cloneToReturn = ((RecognizedPaymentTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new RecognizedPaymentTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return RecognizedPaymentTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return RecognizedPaymentTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'SalesChannelID'</summary>
		internal DataColumn SalesChannelIDColumn 
		{
			get { return _columnSalesChannelID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'MerchantNumber'</summary>
		internal DataColumn MerchantNumberColumn 
		{
			get { return _columnMerchantNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SaleType'</summary>
		internal DataColumn SaleTypeColumn 
		{
			get { return _columnSaleType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsOrganizational'</summary>
		internal DataColumn IsOrganizationalColumn 
		{
			get { return _columnIsOrganizational; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentType'</summary>
		internal DataColumn PaymentTypeColumn 
		{
			get { return _columnPaymentType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentRecognitionID'</summary>
		internal DataColumn PaymentRecognitionIDColumn 
		{
			get { return _columnPaymentRecognitionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentJournalID'</summary>
		internal DataColumn PaymentJournalIDColumn 
		{
			get { return _columnPaymentJournalID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingDate'</summary>
		internal DataColumn PostingDateColumn 
		{
			get { return _columnPostingDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostingReference'</summary>
		internal DataColumn PostingReferenceColumn 
		{
			get { return _columnPostingReference; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CloseoutPeriodID'</summary>
		internal DataColumn CloseoutPeriodIDColumn 
		{
			get { return _columnCloseoutPeriodID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreditAccountNumber'</summary>
		internal DataColumn CreditAccountNumberColumn 
		{
			get { return _columnCreditAccountNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DebitAccountNumber'</summary>
		internal DataColumn DebitAccountNumberColumn 
		{
			get { return _columnDebitAccountNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Amount'</summary>
		internal DataColumn AmountColumn 
		{
			get { return _columnAmount; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastUser'</summary>
		internal DataColumn LastUserColumn 
		{
			get { return _columnLastUser; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastModified'</summary>
		internal DataColumn LastModifiedColumn 
		{
			get { return _columnLastModified; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionCounter'</summary>
		internal DataColumn TransactionCounterColumn 
		{
			get { return _columnTransactionCounter; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable RecognizedPayment</summary>
	public partial class RecognizedPaymentRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private RecognizedPaymentTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal RecognizedPaymentRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((RecognizedPaymentTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field SalesChannelID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."SALESCHANNELID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SalesChannelID
		{
			get { return IsSalesChannelIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SalesChannelIDColumn]; }
			set { this[_parent.SalesChannelIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SalesChannelID is NULL, false otherwise.</summary>
		public bool IsSalesChannelIDNull() 
		{
			return IsNull(_parent.SalesChannelIDColumn);
		}

		/// <summary>Sets the TypedView field SalesChannelID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSalesChannelIDNull() 
		{
			this[_parent.SalesChannelIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field MerchantNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."MERCHANTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 MerchantNumber
		{
			get { return IsMerchantNumberNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.MerchantNumberColumn]; }
			set { this[_parent.MerchantNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field MerchantNumber is NULL, false otherwise.</summary>
		public bool IsMerchantNumberNull() 
		{
			return IsNull(_parent.MerchantNumberColumn);
		}

		/// <summary>Sets the TypedView field MerchantNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMerchantNumberNull() 
		{
			this[_parent.MerchantNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SaleType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."SALETYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 SaleType
		{
			get { return IsSaleTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.SaleTypeColumn]; }
			set { this[_parent.SaleTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SaleType is NULL, false otherwise.</summary>
		public bool IsSaleTypeNull() 
		{
			return IsNull(_parent.SaleTypeColumn);
		}

		/// <summary>Sets the TypedView field SaleType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSaleTypeNull() 
		{
			this[_parent.SaleTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsOrganizational</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."ISORGANIZATIONAL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsOrganizational
		{
			get { return IsIsOrganizationalNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsOrganizationalColumn]; }
			set { this[_parent.IsOrganizationalColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsOrganizational is NULL, false otherwise.</summary>
		public bool IsIsOrganizationalNull() 
		{
			return IsNull(_parent.IsOrganizationalColumn);
		}

		/// <summary>Sets the TypedView field IsOrganizational to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsOrganizationalNull() 
		{
			this[_parent.IsOrganizationalColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentType</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."PAYMENTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PaymentType
		{
			get { return IsPaymentTypeNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PaymentTypeColumn]; }
			set { this[_parent.PaymentTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentType is NULL, false otherwise.</summary>
		public bool IsPaymentTypeNull() 
		{
			return IsNull(_parent.PaymentTypeColumn);
		}

		/// <summary>Sets the TypedView field PaymentType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentTypeNull() 
		{
			this[_parent.PaymentTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentRecognitionID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."PAYMENTRECOGNITIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentRecognitionID
		{
			get { return IsPaymentRecognitionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentRecognitionIDColumn]; }
			set { this[_parent.PaymentRecognitionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentRecognitionID is NULL, false otherwise.</summary>
		public bool IsPaymentRecognitionIDNull() 
		{
			return IsNull(_parent.PaymentRecognitionIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentRecognitionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentRecognitionIDNull() 
		{
			this[_parent.PaymentRecognitionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentJournalID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."PAYMENTJOURNALID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentJournalID
		{
			get { return IsPaymentJournalIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentJournalIDColumn]; }
			set { this[_parent.PaymentJournalIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentJournalID is NULL, false otherwise.</summary>
		public bool IsPaymentJournalIDNull() 
		{
			return IsNull(_parent.PaymentJournalIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentJournalID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentJournalIDNull() 
		{
			this[_parent.PaymentJournalIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingDate</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."POSTINGDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PostingDate
		{
			get { return IsPostingDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PostingDateColumn]; }
			set { this[_parent.PostingDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingDate is NULL, false otherwise.</summary>
		public bool IsPostingDateNull() 
		{
			return IsNull(_parent.PostingDateColumn);
		}

		/// <summary>Sets the TypedView field PostingDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingDateNull() 
		{
			this[_parent.PostingDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostingReference</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."POSTINGREFERENCE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String PostingReference
		{
			get { return IsPostingReferenceNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PostingReferenceColumn]; }
			set { this[_parent.PostingReferenceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostingReference is NULL, false otherwise.</summary>
		public bool IsPostingReferenceNull() 
		{
			return IsNull(_parent.PostingReferenceColumn);
		}

		/// <summary>Sets the TypedView field PostingReference to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostingReferenceNull() 
		{
			this[_parent.PostingReferenceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CloseoutPeriodID</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."CLOSEOUTPERIODID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CloseoutPeriodID
		{
			get { return IsCloseoutPeriodIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CloseoutPeriodIDColumn]; }
			set { this[_parent.CloseoutPeriodIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CloseoutPeriodID is NULL, false otherwise.</summary>
		public bool IsCloseoutPeriodIDNull() 
		{
			return IsNull(_parent.CloseoutPeriodIDColumn);
		}

		/// <summary>Sets the TypedView field CloseoutPeriodID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCloseoutPeriodIDNull() 
		{
			this[_parent.CloseoutPeriodIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreditAccountNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."CREDITACCOUNTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String CreditAccountNumber
		{
			get { return IsCreditAccountNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CreditAccountNumberColumn]; }
			set { this[_parent.CreditAccountNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreditAccountNumber is NULL, false otherwise.</summary>
		public bool IsCreditAccountNumberNull() 
		{
			return IsNull(_parent.CreditAccountNumberColumn);
		}

		/// <summary>Sets the TypedView field CreditAccountNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreditAccountNumberNull() 
		{
			this[_parent.CreditAccountNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DebitAccountNumber</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."DEBITACCOUNTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 40</remarks>
		public System.String DebitAccountNumber
		{
			get { return IsDebitAccountNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.DebitAccountNumberColumn]; }
			set { this[_parent.DebitAccountNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DebitAccountNumber is NULL, false otherwise.</summary>
		public bool IsDebitAccountNumberNull() 
		{
			return IsNull(_parent.DebitAccountNumberColumn);
		}

		/// <summary>Sets the TypedView field DebitAccountNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDebitAccountNumberNull() 
		{
			this[_parent.DebitAccountNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Amount</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."AMOUNT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Amount
		{
			get { return IsAmountNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AmountColumn]; }
			set { this[_parent.AmountColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Amount is NULL, false otherwise.</summary>
		public bool IsAmountNull() 
		{
			return IsNull(_parent.AmountColumn);
		}

		/// <summary>Sets the TypedView field Amount to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAmountNull() 
		{
			this[_parent.AmountColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastUser</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."LASTUSER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastUser
		{
			get { return IsLastUserNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastUserColumn]; }
			set { this[_parent.LastUserColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastUser is NULL, false otherwise.</summary>
		public bool IsLastUserNull() 
		{
			return IsNull(_parent.LastUserColumn);
		}

		/// <summary>Sets the TypedView field LastUser to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastUserNull() 
		{
			this[_parent.LastUserColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastModified</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."LASTMODIFIED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastModified
		{
			get { return IsLastModifiedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastModifiedColumn]; }
			set { this[_parent.LastModifiedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastModified is NULL, false otherwise.</summary>
		public bool IsLastModifiedNull() 
		{
			return IsNull(_parent.LastModifiedColumn);
		}

		/// <summary>Sets the TypedView field LastModified to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastModifiedNull() 
		{
			this[_parent.LastModifiedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionCounter</summary>
		/// <remarks>Mapped on view field: "ACC_RECOGNIZEDPAYMENT"."TRANSACTIONCOUNTER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal TransactionCounter
		{
			get { return IsTransactionCounterNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.TransactionCounterColumn]; }
			set { this[_parent.TransactionCounterColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionCounter is NULL, false otherwise.</summary>
		public bool IsTransactionCounterNull() 
		{
			return IsNull(_parent.TransactionCounterColumn);
		}

		/// <summary>Sets the TypedView field TransactionCounter to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionCounterNull() 
		{
			this[_parent.TransactionCounterColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
