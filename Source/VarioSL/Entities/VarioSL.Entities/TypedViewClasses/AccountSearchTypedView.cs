﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'AccountSearch'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class AccountSearchTypedView : TypedViewBase<AccountSearchRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnPersonID;
		private DataColumn _columnContractID;
		private DataColumn _columnUserName;
		private DataColumn _columnPhonePIN;
		private DataColumn _columnAccountState;
		private DataColumn _columnContractNumber;
		private DataColumn _columnAuthenticationToken;
		private DataColumn _columnLastName;
		private DataColumn _columnFirstName;
		private DataColumn _columnDateOfBirth;
		private DataColumn _columnEmail;
		private DataColumn _columnPhoneNumber;
		private DataColumn _columnPersonType;
		private DataColumn _columnOrganizationName;
		private DataColumn _columnAddressee;
		private DataColumn _columnStreet;
		private DataColumn _columnStreetNumber;
		private DataColumn _columnAddressField1;
		private DataColumn _columnAddressField2;
		private DataColumn _columnCity;
		private DataColumn _columnPostalCode;
		private DataColumn _columnRegion;
		private DataColumn _columnCountry;
		private DataColumn _columnAddressType;
		private DataColumn _columnCardPrintedNumber;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 25;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static AccountSearchTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public AccountSearchTypedView():base("AccountSearch")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AccountSearchTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.AccountSearchTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.AccountSearchTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new AccountSearchRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("UserName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PhonePIN", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AuthenticationToken", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FirstName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("DateOfBirth", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PhoneNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Addressee", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Street", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressField1", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressField2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("City", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PostalCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Region", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Country", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardPrintedNumber", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "AccountSearch";
			_columnPersonID = GeneralUtils.CreateTypedDataTableColumn("PersonID", @"PersonID", typeof(System.Int64), this.Columns);
			_columnContractID = GeneralUtils.CreateTypedDataTableColumn("ContractID", @"ContractID", typeof(System.Int64), this.Columns);
			_columnUserName = GeneralUtils.CreateTypedDataTableColumn("UserName", @"UserName", typeof(System.String), this.Columns);
			_columnPhonePIN = GeneralUtils.CreateTypedDataTableColumn("PhonePIN", @"PhonePIN", typeof(System.String), this.Columns);
			_columnAccountState = GeneralUtils.CreateTypedDataTableColumn("AccountState", @"AccountState", typeof(System.Int32), this.Columns);
			_columnContractNumber = GeneralUtils.CreateTypedDataTableColumn("ContractNumber", @"ContractNumber", typeof(System.String), this.Columns);
			_columnAuthenticationToken = GeneralUtils.CreateTypedDataTableColumn("AuthenticationToken", @"AuthenticationToken", typeof(System.String), this.Columns);
			_columnLastName = GeneralUtils.CreateTypedDataTableColumn("LastName", @"LastName", typeof(System.String), this.Columns);
			_columnFirstName = GeneralUtils.CreateTypedDataTableColumn("FirstName", @"FirstName", typeof(System.String), this.Columns);
			_columnDateOfBirth = GeneralUtils.CreateTypedDataTableColumn("DateOfBirth", @"DateOfBirth", typeof(System.DateTime), this.Columns);
			_columnEmail = GeneralUtils.CreateTypedDataTableColumn("Email", @"Email", typeof(System.String), this.Columns);
			_columnPhoneNumber = GeneralUtils.CreateTypedDataTableColumn("PhoneNumber", @"PhoneNumber", typeof(System.String), this.Columns);
			_columnPersonType = GeneralUtils.CreateTypedDataTableColumn("PersonType", @"PersonType", typeof(System.String), this.Columns);
			_columnOrganizationName = GeneralUtils.CreateTypedDataTableColumn("OrganizationName", @"OrganizationName", typeof(System.String), this.Columns);
			_columnAddressee = GeneralUtils.CreateTypedDataTableColumn("Addressee", @"Addressee", typeof(System.String), this.Columns);
			_columnStreet = GeneralUtils.CreateTypedDataTableColumn("Street", @"Street", typeof(System.String), this.Columns);
			_columnStreetNumber = GeneralUtils.CreateTypedDataTableColumn("StreetNumber", @"StreetNumber", typeof(System.String), this.Columns);
			_columnAddressField1 = GeneralUtils.CreateTypedDataTableColumn("AddressField1", @"AddressField1", typeof(System.String), this.Columns);
			_columnAddressField2 = GeneralUtils.CreateTypedDataTableColumn("AddressField2", @"AddressField2", typeof(System.String), this.Columns);
			_columnCity = GeneralUtils.CreateTypedDataTableColumn("City", @"City", typeof(System.String), this.Columns);
			_columnPostalCode = GeneralUtils.CreateTypedDataTableColumn("PostalCode", @"PostalCode", typeof(System.String), this.Columns);
			_columnRegion = GeneralUtils.CreateTypedDataTableColumn("Region", @"Region", typeof(System.String), this.Columns);
			_columnCountry = GeneralUtils.CreateTypedDataTableColumn("Country", @"Country", typeof(System.String), this.Columns);
			_columnAddressType = GeneralUtils.CreateTypedDataTableColumn("AddressType", @"AddressType", typeof(System.String), this.Columns);
			_columnCardPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("CardPrintedNumber", @"CardPrintedNumber", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnPersonID = this.Columns["PersonID"];
			_columnContractID = this.Columns["ContractID"];
			_columnUserName = this.Columns["UserName"];
			_columnPhonePIN = this.Columns["PhonePIN"];
			_columnAccountState = this.Columns["AccountState"];
			_columnContractNumber = this.Columns["ContractNumber"];
			_columnAuthenticationToken = this.Columns["AuthenticationToken"];
			_columnLastName = this.Columns["LastName"];
			_columnFirstName = this.Columns["FirstName"];
			_columnDateOfBirth = this.Columns["DateOfBirth"];
			_columnEmail = this.Columns["Email"];
			_columnPhoneNumber = this.Columns["PhoneNumber"];
			_columnPersonType = this.Columns["PersonType"];
			_columnOrganizationName = this.Columns["OrganizationName"];
			_columnAddressee = this.Columns["Addressee"];
			_columnStreet = this.Columns["Street"];
			_columnStreetNumber = this.Columns["StreetNumber"];
			_columnAddressField1 = this.Columns["AddressField1"];
			_columnAddressField2 = this.Columns["AddressField2"];
			_columnCity = this.Columns["City"];
			_columnPostalCode = this.Columns["PostalCode"];
			_columnRegion = this.Columns["Region"];
			_columnCountry = this.Columns["Country"];
			_columnAddressType = this.Columns["AddressType"];
			_columnCardPrintedNumber = this.Columns["CardPrintedNumber"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			AccountSearchTypedView cloneToReturn = ((AccountSearchTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new AccountSearchTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return AccountSearchTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return AccountSearchTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'PersonID'</summary>
		internal DataColumn PersonIDColumn 
		{
			get { return _columnPersonID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractID'</summary>
		internal DataColumn ContractIDColumn 
		{
			get { return _columnContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'UserName'</summary>
		internal DataColumn UserNameColumn 
		{
			get { return _columnUserName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PhonePIN'</summary>
		internal DataColumn PhonePINColumn 
		{
			get { return _columnPhonePIN; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AccountState'</summary>
		internal DataColumn AccountStateColumn 
		{
			get { return _columnAccountState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractNumber'</summary>
		internal DataColumn ContractNumberColumn 
		{
			get { return _columnContractNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AuthenticationToken'</summary>
		internal DataColumn AuthenticationTokenColumn 
		{
			get { return _columnAuthenticationToken; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastName'</summary>
		internal DataColumn LastNameColumn 
		{
			get { return _columnLastName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FirstName'</summary>
		internal DataColumn FirstNameColumn 
		{
			get { return _columnFirstName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'DateOfBirth'</summary>
		internal DataColumn DateOfBirthColumn 
		{
			get { return _columnDateOfBirth; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Email'</summary>
		internal DataColumn EmailColumn 
		{
			get { return _columnEmail; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PhoneNumber'</summary>
		internal DataColumn PhoneNumberColumn 
		{
			get { return _columnPhoneNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonType'</summary>
		internal DataColumn PersonTypeColumn 
		{
			get { return _columnPersonType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationName'</summary>
		internal DataColumn OrganizationNameColumn 
		{
			get { return _columnOrganizationName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Addressee'</summary>
		internal DataColumn AddresseeColumn 
		{
			get { return _columnAddressee; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Street'</summary>
		internal DataColumn StreetColumn 
		{
			get { return _columnStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StreetNumber'</summary>
		internal DataColumn StreetNumberColumn 
		{
			get { return _columnStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressField1'</summary>
		internal DataColumn AddressField1Column 
		{
			get { return _columnAddressField1; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressField2'</summary>
		internal DataColumn AddressField2Column 
		{
			get { return _columnAddressField2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'City'</summary>
		internal DataColumn CityColumn 
		{
			get { return _columnCity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PostalCode'</summary>
		internal DataColumn PostalCodeColumn 
		{
			get { return _columnPostalCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Region'</summary>
		internal DataColumn RegionColumn 
		{
			get { return _columnRegion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Country'</summary>
		internal DataColumn CountryColumn 
		{
			get { return _columnCountry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressType'</summary>
		internal DataColumn AddressTypeColumn 
		{
			get { return _columnAddressType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardPrintedNumber'</summary>
		internal DataColumn CardPrintedNumberColumn 
		{
			get { return _columnCardPrintedNumber; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable AccountSearch</summary>
	public partial class AccountSearchRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private AccountSearchTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal AccountSearchRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((AccountSearchTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field PersonID</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."PERSONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PersonID
		{
			get { return IsPersonIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PersonIDColumn]; }
			set { this[_parent.PersonIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonID is NULL, false otherwise.</summary>
		public bool IsPersonIDNull() 
		{
			return IsNull(_parent.PersonIDColumn);
		}

		/// <summary>Sets the TypedView field PersonID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonIDNull() 
		{
			this[_parent.PersonIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractID</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."CONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ContractID
		{
			get { return IsContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ContractIDColumn]; }
			set { this[_parent.ContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractID is NULL, false otherwise.</summary>
		public bool IsContractIDNull() 
		{
			return IsNull(_parent.ContractIDColumn);
		}

		/// <summary>Sets the TypedView field ContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractIDNull() 
		{
			this[_parent.ContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field UserName</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."USERNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512</remarks>
		public System.String UserName
		{
			get { return IsUserNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.UserNameColumn]; }
			set { this[_parent.UserNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field UserName is NULL, false otherwise.</summary>
		public bool IsUserNameNull() 
		{
			return IsNull(_parent.UserNameColumn);
		}

		/// <summary>Sets the TypedView field UserName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetUserNameNull() 
		{
			this[_parent.UserNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PhonePIN</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."PHONEPIN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PhonePIN
		{
			get { return IsPhonePINNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PhonePINColumn]; }
			set { this[_parent.PhonePINColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PhonePIN is NULL, false otherwise.</summary>
		public bool IsPhonePINNull() 
		{
			return IsNull(_parent.PhonePINColumn);
		}

		/// <summary>Sets the TypedView field PhonePIN to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPhonePINNull() 
		{
			this[_parent.PhonePINColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AccountState</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ACCOUNTSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 AccountState
		{
			get { return IsAccountStateNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.AccountStateColumn]; }
			set { this[_parent.AccountStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountState is NULL, false otherwise.</summary>
		public bool IsAccountStateNull() 
		{
			return IsNull(_parent.AccountStateColumn);
		}

		/// <summary>Sets the TypedView field AccountState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountStateNull() 
		{
			this[_parent.AccountStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractNumber</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."CONTRACTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ContractNumber
		{
			get { return IsContractNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ContractNumberColumn]; }
			set { this[_parent.ContractNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractNumber is NULL, false otherwise.</summary>
		public bool IsContractNumberNull() 
		{
			return IsNull(_parent.ContractNumberColumn);
		}

		/// <summary>Sets the TypedView field ContractNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractNumberNull() 
		{
			this[_parent.ContractNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AuthenticationToken</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."AUTHENTICATIONTOKEN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 120</remarks>
		public System.String AuthenticationToken
		{
			get { return IsAuthenticationTokenNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AuthenticationTokenColumn]; }
			set { this[_parent.AuthenticationTokenColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AuthenticationToken is NULL, false otherwise.</summary>
		public bool IsAuthenticationTokenNull() 
		{
			return IsNull(_parent.AuthenticationTokenColumn);
		}

		/// <summary>Sets the TypedView field AuthenticationToken to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAuthenticationTokenNull() 
		{
			this[_parent.AuthenticationTokenColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastName</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."LASTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastName
		{
			get { return IsLastNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastNameColumn]; }
			set { this[_parent.LastNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastName is NULL, false otherwise.</summary>
		public bool IsLastNameNull() 
		{
			return IsNull(_parent.LastNameColumn);
		}

		/// <summary>Sets the TypedView field LastName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastNameNull() 
		{
			this[_parent.LastNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FirstName</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."FIRSTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String FirstName
		{
			get { return IsFirstNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FirstNameColumn]; }
			set { this[_parent.FirstNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FirstName is NULL, false otherwise.</summary>
		public bool IsFirstNameNull() 
		{
			return IsNull(_parent.FirstNameColumn);
		}

		/// <summary>Sets the TypedView field FirstName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFirstNameNull() 
		{
			this[_parent.FirstNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field DateOfBirth</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."DATEOFBIRTH"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime DateOfBirth
		{
			get { return IsDateOfBirthNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.DateOfBirthColumn]; }
			set { this[_parent.DateOfBirthColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field DateOfBirth is NULL, false otherwise.</summary>
		public bool IsDateOfBirthNull() 
		{
			return IsNull(_parent.DateOfBirthColumn);
		}

		/// <summary>Sets the TypedView field DateOfBirth to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetDateOfBirthNull() 
		{
			this[_parent.DateOfBirthColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Email</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."EMAIL"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512</remarks>
		public System.String Email
		{
			get { return IsEmailNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.EmailColumn]; }
			set { this[_parent.EmailColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Email is NULL, false otherwise.</summary>
		public bool IsEmailNull() 
		{
			return IsNull(_parent.EmailColumn);
		}

		/// <summary>Sets the TypedView field Email to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetEmailNull() 
		{
			this[_parent.EmailColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PhoneNumber</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."PHONENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PhoneNumber
		{
			get { return IsPhoneNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PhoneNumberColumn]; }
			set { this[_parent.PhoneNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PhoneNumber is NULL, false otherwise.</summary>
		public bool IsPhoneNumberNull() 
		{
			return IsNull(_parent.PhoneNumberColumn);
		}

		/// <summary>Sets the TypedView field PhoneNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPhoneNumberNull() 
		{
			this[_parent.PhoneNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonType</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."PERSONTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 20</remarks>
		public System.String PersonType
		{
			get { return IsPersonTypeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonTypeColumn]; }
			set { this[_parent.PersonTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonType is NULL, false otherwise.</summary>
		public bool IsPersonTypeNull() 
		{
			return IsNull(_parent.PersonTypeColumn);
		}

		/// <summary>Sets the TypedView field PersonType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonTypeNull() 
		{
			this[_parent.PersonTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationName</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ORGANIZATIONNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrganizationName
		{
			get { return IsOrganizationNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationNameColumn]; }
			set { this[_parent.OrganizationNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationName is NULL, false otherwise.</summary>
		public bool IsOrganizationNameNull() 
		{
			return IsNull(_parent.OrganizationNameColumn);
		}

		/// <summary>Sets the TypedView field OrganizationName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationNameNull() 
		{
			this[_parent.OrganizationNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Addressee</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ADDRESSEE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String Addressee
		{
			get { return IsAddresseeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddresseeColumn]; }
			set { this[_parent.AddresseeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Addressee is NULL, false otherwise.</summary>
		public bool IsAddresseeNull() 
		{
			return IsNull(_parent.AddresseeColumn);
		}

		/// <summary>Sets the TypedView field Addressee to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddresseeNull() 
		{
			this[_parent.AddresseeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Street</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."STREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String Street
		{
			get { return IsStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StreetColumn]; }
			set { this[_parent.StreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Street is NULL, false otherwise.</summary>
		public bool IsStreetNull() 
		{
			return IsNull(_parent.StreetColumn);
		}

		/// <summary>Sets the TypedView field Street to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStreetNull() 
		{
			this[_parent.StreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StreetNumber</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."STREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String StreetNumber
		{
			get { return IsStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StreetNumberColumn]; }
			set { this[_parent.StreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StreetNumber is NULL, false otherwise.</summary>
		public bool IsStreetNumberNull() 
		{
			return IsNull(_parent.StreetNumberColumn);
		}

		/// <summary>Sets the TypedView field StreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStreetNumberNull() 
		{
			this[_parent.StreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressField1</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ADDRESSFIELD1"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressField1
		{
			get { return IsAddressField1Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressField1Column]; }
			set { this[_parent.AddressField1Column] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressField1 is NULL, false otherwise.</summary>
		public bool IsAddressField1Null() 
		{
			return IsNull(_parent.AddressField1Column);
		}

		/// <summary>Sets the TypedView field AddressField1 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressField1Null() 
		{
			this[_parent.AddressField1Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressField2</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ADDRESSFIELD2"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressField2
		{
			get { return IsAddressField2Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressField2Column]; }
			set { this[_parent.AddressField2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressField2 is NULL, false otherwise.</summary>
		public bool IsAddressField2Null() 
		{
			return IsNull(_parent.AddressField2Column);
		}

		/// <summary>Sets the TypedView field AddressField2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressField2Null() 
		{
			this[_parent.AddressField2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field City</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."CITY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String City
		{
			get { return IsCityNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CityColumn]; }
			set { this[_parent.CityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field City is NULL, false otherwise.</summary>
		public bool IsCityNull() 
		{
			return IsNull(_parent.CityColumn);
		}

		/// <summary>Sets the TypedView field City to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCityNull() 
		{
			this[_parent.CityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PostalCode</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."POSTALCODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String PostalCode
		{
			get { return IsPostalCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PostalCodeColumn]; }
			set { this[_parent.PostalCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PostalCode is NULL, false otherwise.</summary>
		public bool IsPostalCodeNull() 
		{
			return IsNull(_parent.PostalCodeColumn);
		}

		/// <summary>Sets the TypedView field PostalCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPostalCodeNull() 
		{
			this[_parent.PostalCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Region</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."REGION"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String Region
		{
			get { return IsRegionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.RegionColumn]; }
			set { this[_parent.RegionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Region is NULL, false otherwise.</summary>
		public bool IsRegionNull() 
		{
			return IsNull(_parent.RegionColumn);
		}

		/// <summary>Sets the TypedView field Region to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRegionNull() 
		{
			this[_parent.RegionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Country</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."COUNTRY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String Country
		{
			get { return IsCountryNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CountryColumn]; }
			set { this[_parent.CountryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Country is NULL, false otherwise.</summary>
		public bool IsCountryNull() 
		{
			return IsNull(_parent.CountryColumn);
		}

		/// <summary>Sets the TypedView field Country to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCountryNull() 
		{
			this[_parent.CountryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressType</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."ADDRESSTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Char, 0, 0, 2147483647</remarks>
		public System.String AddressType
		{
			get { return IsAddressTypeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressTypeColumn]; }
			set { this[_parent.AddressTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressType is NULL, false otherwise.</summary>
		public bool IsAddressTypeNull() 
		{
			return IsNull(_parent.AddressTypeColumn);
		}

		/// <summary>Sets the TypedView field AddressType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressTypeNull() 
		{
			this[_parent.AddressTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardPrintedNumber</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTSEARCH"."CARDPRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String CardPrintedNumber
		{
			get { return IsCardPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardPrintedNumberColumn]; }
			set { this[_parent.CardPrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardPrintedNumber is NULL, false otherwise.</summary>
		public bool IsCardPrintedNumberNull() 
		{
			return IsNull(_parent.CardPrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field CardPrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardPrintedNumberNull() 
		{
			this[_parent.CardPrintedNumberColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
