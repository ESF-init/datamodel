﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'SurveyReply'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class SurveyReplyTypedView : TypedViewBase<SurveyReplyRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnPersonID;
		private DataColumn _columnPersonFirstName;
		private DataColumn _columnPersonLastName;
		private DataColumn _columnSurveyID;
		private DataColumn _columnSurveyName;
		private DataColumn _columnSurveyDescription;
		private DataColumn _columnQuestionID;
		private DataColumn _columnQuestionLanguage;
		private DataColumn _columnQuestionText;
		private DataColumn _columnChoiceID;
		private DataColumn _columnChoiceText;
		private DataColumn _columnChoiceFollowUpQuestionID;
		private DataColumn _columnChoiceFreeText;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 13;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static SurveyReplyTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public SurveyReplyTypedView():base("SurveyReply")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyReplyTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SurveyReplyTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.SurveyReplyTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new SurveyReplyRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonFirstName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonLastName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SurveyID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SurveyName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SurveyDescription", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("QuestionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("QuestionLanguage", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("QuestionText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ChoiceID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ChoiceText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ChoiceFollowUpQuestionID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ChoiceFreeText", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "SurveyReply";
			_columnPersonID = GeneralUtils.CreateTypedDataTableColumn("PersonID", @"PersonID", typeof(System.Int64), this.Columns);
			_columnPersonFirstName = GeneralUtils.CreateTypedDataTableColumn("PersonFirstName", @"PersonFirstName", typeof(System.String), this.Columns);
			_columnPersonLastName = GeneralUtils.CreateTypedDataTableColumn("PersonLastName", @"PersonLastName", typeof(System.String), this.Columns);
			_columnSurveyID = GeneralUtils.CreateTypedDataTableColumn("SurveyID", @"SurveyID", typeof(System.Int64), this.Columns);
			_columnSurveyName = GeneralUtils.CreateTypedDataTableColumn("SurveyName", @"SurveyName", typeof(System.String), this.Columns);
			_columnSurveyDescription = GeneralUtils.CreateTypedDataTableColumn("SurveyDescription", @"SurveyDescription", typeof(System.String), this.Columns);
			_columnQuestionID = GeneralUtils.CreateTypedDataTableColumn("QuestionID", @"QuestionID", typeof(System.Int64), this.Columns);
			_columnQuestionLanguage = GeneralUtils.CreateTypedDataTableColumn("QuestionLanguage", @"QuestionLanguage", typeof(System.String), this.Columns);
			_columnQuestionText = GeneralUtils.CreateTypedDataTableColumn("QuestionText", @"QuestionText", typeof(System.String), this.Columns);
			_columnChoiceID = GeneralUtils.CreateTypedDataTableColumn("ChoiceID", @"ChoiceID", typeof(System.Int64), this.Columns);
			_columnChoiceText = GeneralUtils.CreateTypedDataTableColumn("ChoiceText", @"ChoiceText", typeof(System.String), this.Columns);
			_columnChoiceFollowUpQuestionID = GeneralUtils.CreateTypedDataTableColumn("ChoiceFollowUpQuestionID", @"ChoiceFollowUpQuestionID", typeof(System.Int64), this.Columns);
			_columnChoiceFreeText = GeneralUtils.CreateTypedDataTableColumn("ChoiceFreeText", @"ChoiceFreeText", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnPersonID = this.Columns["PersonID"];
			_columnPersonFirstName = this.Columns["PersonFirstName"];
			_columnPersonLastName = this.Columns["PersonLastName"];
			_columnSurveyID = this.Columns["SurveyID"];
			_columnSurveyName = this.Columns["SurveyName"];
			_columnSurveyDescription = this.Columns["SurveyDescription"];
			_columnQuestionID = this.Columns["QuestionID"];
			_columnQuestionLanguage = this.Columns["QuestionLanguage"];
			_columnQuestionText = this.Columns["QuestionText"];
			_columnChoiceID = this.Columns["ChoiceID"];
			_columnChoiceText = this.Columns["ChoiceText"];
			_columnChoiceFollowUpQuestionID = this.Columns["ChoiceFollowUpQuestionID"];
			_columnChoiceFreeText = this.Columns["ChoiceFreeText"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			SurveyReplyTypedView cloneToReturn = ((SurveyReplyTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new SurveyReplyTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return SurveyReplyTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return SurveyReplyTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'PersonID'</summary>
		internal DataColumn PersonIDColumn 
		{
			get { return _columnPersonID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonFirstName'</summary>
		internal DataColumn PersonFirstNameColumn 
		{
			get { return _columnPersonFirstName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonLastName'</summary>
		internal DataColumn PersonLastNameColumn 
		{
			get { return _columnPersonLastName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SurveyID'</summary>
		internal DataColumn SurveyIDColumn 
		{
			get { return _columnSurveyID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SurveyName'</summary>
		internal DataColumn SurveyNameColumn 
		{
			get { return _columnSurveyName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SurveyDescription'</summary>
		internal DataColumn SurveyDescriptionColumn 
		{
			get { return _columnSurveyDescription; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'QuestionID'</summary>
		internal DataColumn QuestionIDColumn 
		{
			get { return _columnQuestionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'QuestionLanguage'</summary>
		internal DataColumn QuestionLanguageColumn 
		{
			get { return _columnQuestionLanguage; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'QuestionText'</summary>
		internal DataColumn QuestionTextColumn 
		{
			get { return _columnQuestionText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ChoiceID'</summary>
		internal DataColumn ChoiceIDColumn 
		{
			get { return _columnChoiceID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ChoiceText'</summary>
		internal DataColumn ChoiceTextColumn 
		{
			get { return _columnChoiceText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ChoiceFollowUpQuestionID'</summary>
		internal DataColumn ChoiceFollowUpQuestionIDColumn 
		{
			get { return _columnChoiceFollowUpQuestionID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ChoiceFreeText'</summary>
		internal DataColumn ChoiceFreeTextColumn 
		{
			get { return _columnChoiceFreeText; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable SurveyReply</summary>
	public partial class SurveyReplyRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private SurveyReplyTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal SurveyReplyRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((SurveyReplyTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field PersonID</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."PERSONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PersonID
		{
			get { return IsPersonIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PersonIDColumn]; }
			set { this[_parent.PersonIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonID is NULL, false otherwise.</summary>
		public bool IsPersonIDNull() 
		{
			return IsNull(_parent.PersonIDColumn);
		}

		/// <summary>Sets the TypedView field PersonID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonIDNull() 
		{
			this[_parent.PersonIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonFirstName</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."PERSONFIRSTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PersonFirstName
		{
			get { return IsPersonFirstNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonFirstNameColumn]; }
			set { this[_parent.PersonFirstNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonFirstName is NULL, false otherwise.</summary>
		public bool IsPersonFirstNameNull() 
		{
			return IsNull(_parent.PersonFirstNameColumn);
		}

		/// <summary>Sets the TypedView field PersonFirstName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonFirstNameNull() 
		{
			this[_parent.PersonFirstNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonLastName</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."PERSONLASTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PersonLastName
		{
			get { return IsPersonLastNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonLastNameColumn]; }
			set { this[_parent.PersonLastNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonLastName is NULL, false otherwise.</summary>
		public bool IsPersonLastNameNull() 
		{
			return IsNull(_parent.PersonLastNameColumn);
		}

		/// <summary>Sets the TypedView field PersonLastName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonLastNameNull() 
		{
			this[_parent.PersonLastNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SurveyID</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."SURVEYID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SurveyID
		{
			get { return IsSurveyIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SurveyIDColumn]; }
			set { this[_parent.SurveyIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SurveyID is NULL, false otherwise.</summary>
		public bool IsSurveyIDNull() 
		{
			return IsNull(_parent.SurveyIDColumn);
		}

		/// <summary>Sets the TypedView field SurveyID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSurveyIDNull() 
		{
			this[_parent.SurveyIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SurveyName</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."SURVEYNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String SurveyName
		{
			get { return IsSurveyNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SurveyNameColumn]; }
			set { this[_parent.SurveyNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SurveyName is NULL, false otherwise.</summary>
		public bool IsSurveyNameNull() 
		{
			return IsNull(_parent.SurveyNameColumn);
		}

		/// <summary>Sets the TypedView field SurveyName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSurveyNameNull() 
		{
			this[_parent.SurveyNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SurveyDescription</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."SURVEYDESCRIPTION"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250</remarks>
		public System.String SurveyDescription
		{
			get { return IsSurveyDescriptionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SurveyDescriptionColumn]; }
			set { this[_parent.SurveyDescriptionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SurveyDescription is NULL, false otherwise.</summary>
		public bool IsSurveyDescriptionNull() 
		{
			return IsNull(_parent.SurveyDescriptionColumn);
		}

		/// <summary>Sets the TypedView field SurveyDescription to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSurveyDescriptionNull() 
		{
			this[_parent.SurveyDescriptionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field QuestionID</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."QUESTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 QuestionID
		{
			get { return IsQuestionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.QuestionIDColumn]; }
			set { this[_parent.QuestionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field QuestionID is NULL, false otherwise.</summary>
		public bool IsQuestionIDNull() 
		{
			return IsNull(_parent.QuestionIDColumn);
		}

		/// <summary>Sets the TypedView field QuestionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetQuestionIDNull() 
		{
			this[_parent.QuestionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field QuestionLanguage</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."QUESTIONLANGUAGE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String QuestionLanguage
		{
			get { return IsQuestionLanguageNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.QuestionLanguageColumn]; }
			set { this[_parent.QuestionLanguageColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field QuestionLanguage is NULL, false otherwise.</summary>
		public bool IsQuestionLanguageNull() 
		{
			return IsNull(_parent.QuestionLanguageColumn);
		}

		/// <summary>Sets the TypedView field QuestionLanguage to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetQuestionLanguageNull() 
		{
			this[_parent.QuestionLanguageColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field QuestionText</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."QUESTIONTEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String QuestionText
		{
			get { return IsQuestionTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.QuestionTextColumn]; }
			set { this[_parent.QuestionTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field QuestionText is NULL, false otherwise.</summary>
		public bool IsQuestionTextNull() 
		{
			return IsNull(_parent.QuestionTextColumn);
		}

		/// <summary>Sets the TypedView field QuestionText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetQuestionTextNull() 
		{
			this[_parent.QuestionTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ChoiceID</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."CHOICEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ChoiceID
		{
			get { return IsChoiceIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ChoiceIDColumn]; }
			set { this[_parent.ChoiceIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ChoiceID is NULL, false otherwise.</summary>
		public bool IsChoiceIDNull() 
		{
			return IsNull(_parent.ChoiceIDColumn);
		}

		/// <summary>Sets the TypedView field ChoiceID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetChoiceIDNull() 
		{
			this[_parent.ChoiceIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ChoiceText</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."CHOICETEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String ChoiceText
		{
			get { return IsChoiceTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ChoiceTextColumn]; }
			set { this[_parent.ChoiceTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ChoiceText is NULL, false otherwise.</summary>
		public bool IsChoiceTextNull() 
		{
			return IsNull(_parent.ChoiceTextColumn);
		}

		/// <summary>Sets the TypedView field ChoiceText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetChoiceTextNull() 
		{
			this[_parent.ChoiceTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ChoiceFollowUpQuestionID</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."CHOICEFOLLOWUPQUESTIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ChoiceFollowUpQuestionID
		{
			get { return IsChoiceFollowUpQuestionIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ChoiceFollowUpQuestionIDColumn]; }
			set { this[_parent.ChoiceFollowUpQuestionIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ChoiceFollowUpQuestionID is NULL, false otherwise.</summary>
		public bool IsChoiceFollowUpQuestionIDNull() 
		{
			return IsNull(_parent.ChoiceFollowUpQuestionIDColumn);
		}

		/// <summary>Sets the TypedView field ChoiceFollowUpQuestionID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetChoiceFollowUpQuestionIDNull() 
		{
			this[_parent.ChoiceFollowUpQuestionIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ChoiceFreeText</summary>
		/// <remarks>Mapped on view field: "SL_SURVEYREPLY"."CHOICEFREETEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 250</remarks>
		public System.String ChoiceFreeText
		{
			get { return IsChoiceFreeTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ChoiceFreeTextColumn]; }
			set { this[_parent.ChoiceFreeTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ChoiceFreeText is NULL, false otherwise.</summary>
		public bool IsChoiceFreeTextNull() 
		{
			return IsNull(_parent.ChoiceFreeTextColumn);
		}

		/// <summary>Sets the TypedView field ChoiceFreeText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetChoiceFreeTextNull() 
		{
			this[_parent.ChoiceFreeTextColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
