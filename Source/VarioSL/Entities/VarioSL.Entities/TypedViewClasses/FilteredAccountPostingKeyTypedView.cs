﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'FilteredAccountPostingKey'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class FilteredAccountPostingKeyTypedView : TypedViewBase<FilteredAccountPostingKeyRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnEntryTypeNumber;
		private DataColumn _columnEntryTypeName;
		private DataColumn _columnIsManual;
		private DataColumn _columnIsExport;
		private DataColumn _columnSign;
		private DataColumn _columnIsCancelation;
		private DataColumn _columnIsDebitEntry;
		private DataColumn _columnEntryRangeID;
		private DataColumn _columnTaxRate;
		private DataColumn _columnIsBillShown;
		private DataColumn _columnIsBillClear;
		private DataColumn _columnIsReturnDebit;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 12;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static FilteredAccountPostingKeyTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public FilteredAccountPostingKeyTypedView():base("FilteredAccountPostingKey")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FilteredAccountPostingKeyTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.FilteredAccountPostingKeyTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.FilteredAccountPostingKeyTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new FilteredAccountPostingKeyRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("EntryTypeNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("EntryTypeName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsManual", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsExport", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Sign", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsCancelation", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsDebitEntry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("EntryRangeID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TaxRate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsBillShown", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsBillClear", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("IsReturnDebit", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "FilteredAccountPostingKey";
			_columnEntryTypeNumber = GeneralUtils.CreateTypedDataTableColumn("EntryTypeNumber", @"EntryTypeNumber", typeof(System.Int64), this.Columns);
			_columnEntryTypeName = GeneralUtils.CreateTypedDataTableColumn("EntryTypeName", @"EntryTypeName", typeof(System.String), this.Columns);
			_columnIsManual = GeneralUtils.CreateTypedDataTableColumn("IsManual", @"IsManual", typeof(System.Boolean), this.Columns);
			_columnIsExport = GeneralUtils.CreateTypedDataTableColumn("IsExport", @"IsExport", typeof(System.Boolean), this.Columns);
			_columnSign = GeneralUtils.CreateTypedDataTableColumn("Sign", @"Sign", typeof(System.Int16), this.Columns);
			_columnIsCancelation = GeneralUtils.CreateTypedDataTableColumn("IsCancelation", @"IsCancelation", typeof(System.Boolean), this.Columns);
			_columnIsDebitEntry = GeneralUtils.CreateTypedDataTableColumn("IsDebitEntry", @"IsDebitEntry", typeof(System.Boolean), this.Columns);
			_columnEntryRangeID = GeneralUtils.CreateTypedDataTableColumn("EntryRangeID", @"EntryRangeID", typeof(System.Int64), this.Columns);
			_columnTaxRate = GeneralUtils.CreateTypedDataTableColumn("TaxRate", @"TaxRate", typeof(System.Int64), this.Columns);
			_columnIsBillShown = GeneralUtils.CreateTypedDataTableColumn("IsBillShown", @"IsBillShown", typeof(System.Boolean), this.Columns);
			_columnIsBillClear = GeneralUtils.CreateTypedDataTableColumn("IsBillClear", @"IsBillClear", typeof(System.Boolean), this.Columns);
			_columnIsReturnDebit = GeneralUtils.CreateTypedDataTableColumn("IsReturnDebit", @"IsReturnDebit", typeof(System.Boolean), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnEntryTypeNumber = this.Columns["EntryTypeNumber"];
			_columnEntryTypeName = this.Columns["EntryTypeName"];
			_columnIsManual = this.Columns["IsManual"];
			_columnIsExport = this.Columns["IsExport"];
			_columnSign = this.Columns["Sign"];
			_columnIsCancelation = this.Columns["IsCancelation"];
			_columnIsDebitEntry = this.Columns["IsDebitEntry"];
			_columnEntryRangeID = this.Columns["EntryRangeID"];
			_columnTaxRate = this.Columns["TaxRate"];
			_columnIsBillShown = this.Columns["IsBillShown"];
			_columnIsBillClear = this.Columns["IsBillClear"];
			_columnIsReturnDebit = this.Columns["IsReturnDebit"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			FilteredAccountPostingKeyTypedView cloneToReturn = ((FilteredAccountPostingKeyTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new FilteredAccountPostingKeyTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return FilteredAccountPostingKeyTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return FilteredAccountPostingKeyTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'EntryTypeNumber'</summary>
		internal DataColumn EntryTypeNumberColumn 
		{
			get { return _columnEntryTypeNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'EntryTypeName'</summary>
		internal DataColumn EntryTypeNameColumn 
		{
			get { return _columnEntryTypeName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsManual'</summary>
		internal DataColumn IsManualColumn 
		{
			get { return _columnIsManual; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsExport'</summary>
		internal DataColumn IsExportColumn 
		{
			get { return _columnIsExport; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Sign'</summary>
		internal DataColumn SignColumn 
		{
			get { return _columnSign; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsCancelation'</summary>
		internal DataColumn IsCancelationColumn 
		{
			get { return _columnIsCancelation; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsDebitEntry'</summary>
		internal DataColumn IsDebitEntryColumn 
		{
			get { return _columnIsDebitEntry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'EntryRangeID'</summary>
		internal DataColumn EntryRangeIDColumn 
		{
			get { return _columnEntryRangeID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TaxRate'</summary>
		internal DataColumn TaxRateColumn 
		{
			get { return _columnTaxRate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsBillShown'</summary>
		internal DataColumn IsBillShownColumn 
		{
			get { return _columnIsBillShown; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsBillClear'</summary>
		internal DataColumn IsBillClearColumn 
		{
			get { return _columnIsBillClear; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'IsReturnDebit'</summary>
		internal DataColumn IsReturnDebitColumn 
		{
			get { return _columnIsReturnDebit; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable FilteredAccountPostingKey</summary>
	public partial class FilteredAccountPostingKeyRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private FilteredAccountPostingKeyTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal FilteredAccountPostingKeyRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((FilteredAccountPostingKeyTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field EntryTypeNumber</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."ENTRYTYPENO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 EntryTypeNumber
		{
			get { return IsEntryTypeNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.EntryTypeNumberColumn]; }
			set { this[_parent.EntryTypeNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field EntryTypeNumber is NULL, false otherwise.</summary>
		public bool IsEntryTypeNumberNull() 
		{
			return IsNull(_parent.EntryTypeNumberColumn);
		}

		/// <summary>Sets the TypedView field EntryTypeNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetEntryTypeNumberNull() 
		{
			this[_parent.EntryTypeNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field EntryTypeName</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."ENTRYTYPENAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 100</remarks>
		public System.String EntryTypeName
		{
			get { return IsEntryTypeNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.EntryTypeNameColumn]; }
			set { this[_parent.EntryTypeNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field EntryTypeName is NULL, false otherwise.</summary>
		public bool IsEntryTypeNameNull() 
		{
			return IsNull(_parent.EntryTypeNameColumn);
		}

		/// <summary>Sets the TypedView field EntryTypeName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetEntryTypeNameNull() 
		{
			this[_parent.EntryTypeNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsManual</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."MANUAL"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsManual
		{
			get { return IsIsManualNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsManualColumn]; }
			set { this[_parent.IsManualColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsManual is NULL, false otherwise.</summary>
		public bool IsIsManualNull() 
		{
			return IsNull(_parent.IsManualColumn);
		}

		/// <summary>Sets the TypedView field IsManual to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsManualNull() 
		{
			this[_parent.IsManualColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsExport</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."EXPORT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsExport
		{
			get { return IsIsExportNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsExportColumn]; }
			set { this[_parent.IsExportColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsExport is NULL, false otherwise.</summary>
		public bool IsIsExportNull() 
		{
			return IsNull(_parent.IsExportColumn);
		}

		/// <summary>Sets the TypedView field IsExport to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsExportNull() 
		{
			this[_parent.IsExportColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Sign</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."SIGN"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Int16 Sign
		{
			get { return IsSignNull() ? (System.Int16)TypeDefaultValue.GetDefaultValue(typeof(System.Int16)) : (System.Int16)this[_parent.SignColumn]; }
			set { this[_parent.SignColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Sign is NULL, false otherwise.</summary>
		public bool IsSignNull() 
		{
			return IsNull(_parent.SignColumn);
		}

		/// <summary>Sets the TypedView field Sign to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSignNull() 
		{
			this[_parent.SignColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsCancelation</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."CANCELATION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsCancelation
		{
			get { return IsIsCancelationNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsCancelationColumn]; }
			set { this[_parent.IsCancelationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsCancelation is NULL, false otherwise.</summary>
		public bool IsIsCancelationNull() 
		{
			return IsNull(_parent.IsCancelationColumn);
		}

		/// <summary>Sets the TypedView field IsCancelation to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsCancelationNull() 
		{
			this[_parent.IsCancelationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsDebitEntry</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."DEBITENTRY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsDebitEntry
		{
			get { return IsIsDebitEntryNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsDebitEntryColumn]; }
			set { this[_parent.IsDebitEntryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsDebitEntry is NULL, false otherwise.</summary>
		public bool IsIsDebitEntryNull() 
		{
			return IsNull(_parent.IsDebitEntryColumn);
		}

		/// <summary>Sets the TypedView field IsDebitEntry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsDebitEntryNull() 
		{
			this[_parent.IsDebitEntryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field EntryRangeID</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."ENTRYRANGEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 EntryRangeID
		{
			get { return IsEntryRangeIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.EntryRangeIDColumn]; }
			set { this[_parent.EntryRangeIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field EntryRangeID is NULL, false otherwise.</summary>
		public bool IsEntryRangeIDNull() 
		{
			return IsNull(_parent.EntryRangeIDColumn);
		}

		/// <summary>Sets the TypedView field EntryRangeID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetEntryRangeIDNull() 
		{
			this[_parent.EntryRangeIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TaxRate</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."TAXRATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TaxRate
		{
			get { return IsTaxRateNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TaxRateColumn]; }
			set { this[_parent.TaxRateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TaxRate is NULL, false otherwise.</summary>
		public bool IsTaxRateNull() 
		{
			return IsNull(_parent.TaxRateColumn);
		}

		/// <summary>Sets the TypedView field TaxRate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTaxRateNull() 
		{
			this[_parent.TaxRateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsBillShown</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."BILLSHOWN"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsBillShown
		{
			get { return IsIsBillShownNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsBillShownColumn]; }
			set { this[_parent.IsBillShownColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsBillShown is NULL, false otherwise.</summary>
		public bool IsIsBillShownNull() 
		{
			return IsNull(_parent.IsBillShownColumn);
		}

		/// <summary>Sets the TypedView field IsBillShown to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsBillShownNull() 
		{
			this[_parent.IsBillShownColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsBillClear</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."BILLCLEAR"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsBillClear
		{
			get { return IsIsBillClearNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsBillClearColumn]; }
			set { this[_parent.IsBillClearColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsBillClear is NULL, false otherwise.</summary>
		public bool IsIsBillClearNull() 
		{
			return IsNull(_parent.IsBillClearColumn);
		}

		/// <summary>Sets the TypedView field IsBillClear to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsBillClearNull() 
		{
			this[_parent.IsBillClearColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field IsReturnDebit</summary>
		/// <remarks>Mapped on view field: "SL_ACCOUNTPOSTINGKEY"."RETURNDEBIT"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 1, 0, 0</remarks>
		public System.Boolean IsReturnDebit
		{
			get { return IsIsReturnDebitNull() ? (System.Boolean)TypeDefaultValue.GetDefaultValue(typeof(System.Boolean)) : (System.Boolean)this[_parent.IsReturnDebitColumn]; }
			set { this[_parent.IsReturnDebitColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field IsReturnDebit is NULL, false otherwise.</summary>
		public bool IsIsReturnDebitNull() 
		{
			return IsNull(_parent.IsReturnDebitColumn);
		}

		/// <summary>Sets the TypedView field IsReturnDebit to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetIsReturnDebitNull() 
		{
			this[_parent.IsReturnDebitColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
