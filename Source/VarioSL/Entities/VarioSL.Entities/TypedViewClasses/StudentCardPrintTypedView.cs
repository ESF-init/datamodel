﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'StudentCardPrint'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class StudentCardPrintTypedView : TypedViewBase<StudentCardPrintRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnCardHolderID;
		private DataColumn _columnSchoolYearName;
		private DataColumn _columnSchoolYearID;
		private DataColumn _columnCardHolderFirstName;
		private DataColumn _columnCardHolderLastName;
		private DataColumn _columnCardHolderDateOfBirth;
		private DataColumn _columnCardHolderGender;
		private DataColumn _columnSchoolPrintName;
		private DataColumn _columnSchoolNumber;
		private DataColumn _columnOrdererNumber;
		private DataColumn _columnOrdererPrintName;
		private DataColumn _columnOrdererOrganizationID;
		private DataColumn _columnNumberOfCardPrints;
		private DataColumn _columnCardID;
		private DataColumn _columnCardState;
		private DataColumn _columnCardExpiration;
		private DataColumn _columnCardSerialNumber;
		private DataColumn _columnProductID;
		private DataColumn _columnTicketInternalNumber;
		private DataColumn _columnTicketName;
		private DataColumn _columnTicketID;
		private DataColumn _columnTariffID;
		private DataColumn _columnCardPrintedNumber;
		private DataColumn _columnStartStreet;
		private DataColumn _columnStartStreetNumber;
		private DataColumn _columnStopStreet;
		private DataColumn _columnStopStreetNumber;
		private DataColumn _columnRelationFrom;
		private DataColumn _columnRelationTo;
		private DataColumn _columnFareMatrixEntryPriority;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 30;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static StudentCardPrintTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public StudentCardPrintTypedView():base("StudentCardPrint")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StudentCardPrintTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.StudentCardPrintTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.StudentCardPrintTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new StudentCardPrintRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardHolderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolYearName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolYearID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardHolderFirstName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardHolderLastName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardHolderDateOfBirth", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardHolderGender", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolPrintName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("SchoolNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrdererNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrdererPrintName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrdererOrganizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("NumberOfCardPrints", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardState", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardExpiration", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardSerialNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ProductID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketInternalNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TicketID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TariffID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardPrintedNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StartStreet", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StartStreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopStreet", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("StopStreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("RelationTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMatrixEntryPriority", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "StudentCardPrint";
			_columnCardHolderID = GeneralUtils.CreateTypedDataTableColumn("CardHolderID", @"CardHolderID", typeof(System.Int64), this.Columns);
			_columnSchoolYearName = GeneralUtils.CreateTypedDataTableColumn("SchoolYearName", @"SchoolYearName", typeof(System.String), this.Columns);
			_columnSchoolYearID = GeneralUtils.CreateTypedDataTableColumn("SchoolYearID", @"SchoolYearID", typeof(System.Int64), this.Columns);
			_columnCardHolderFirstName = GeneralUtils.CreateTypedDataTableColumn("CardHolderFirstName", @"CardHolderFirstName", typeof(System.String), this.Columns);
			_columnCardHolderLastName = GeneralUtils.CreateTypedDataTableColumn("CardHolderLastName", @"CardHolderLastName", typeof(System.String), this.Columns);
			_columnCardHolderDateOfBirth = GeneralUtils.CreateTypedDataTableColumn("CardHolderDateOfBirth", @"CardHolderDateOfBirth", typeof(System.DateTime), this.Columns);
			_columnCardHolderGender = GeneralUtils.CreateTypedDataTableColumn("CardHolderGender", @"CardHolderGender", typeof(System.Int32), this.Columns);
			_columnSchoolPrintName = GeneralUtils.CreateTypedDataTableColumn("SchoolPrintName", @"SchoolPrintName", typeof(System.String), this.Columns);
			_columnSchoolNumber = GeneralUtils.CreateTypedDataTableColumn("SchoolNumber", @"SchoolNumber", typeof(System.String), this.Columns);
			_columnOrdererNumber = GeneralUtils.CreateTypedDataTableColumn("OrdererNumber", @"OrdererNumber", typeof(System.String), this.Columns);
			_columnOrdererPrintName = GeneralUtils.CreateTypedDataTableColumn("OrdererPrintName", @"OrdererPrintName", typeof(System.String), this.Columns);
			_columnOrdererOrganizationID = GeneralUtils.CreateTypedDataTableColumn("OrdererOrganizationID", @"OrdererOrganizationID", typeof(System.Int64), this.Columns);
			_columnNumberOfCardPrints = GeneralUtils.CreateTypedDataTableColumn("NumberOfCardPrints", @"NumberOfCardPrints", typeof(System.Int64), this.Columns);
			_columnCardID = GeneralUtils.CreateTypedDataTableColumn("CardID", @"CardID", typeof(System.Int64), this.Columns);
			_columnCardState = GeneralUtils.CreateTypedDataTableColumn("CardState", @"CardState", typeof(VarioSL.Entities.Enumerations.CardState), this.Columns);
			_columnCardExpiration = GeneralUtils.CreateTypedDataTableColumn("CardExpiration", @"CardExpiration", typeof(System.DateTime), this.Columns);
			_columnCardSerialNumber = GeneralUtils.CreateTypedDataTableColumn("CardSerialNumber", @"CardSerialNumber", typeof(System.String), this.Columns);
			_columnProductID = GeneralUtils.CreateTypedDataTableColumn("ProductID", @"ProductID", typeof(System.Int64), this.Columns);
			_columnTicketInternalNumber = GeneralUtils.CreateTypedDataTableColumn("TicketInternalNumber", @"TicketInternalNumber", typeof(System.Int64), this.Columns);
			_columnTicketName = GeneralUtils.CreateTypedDataTableColumn("TicketName", @"TicketName", typeof(System.String), this.Columns);
			_columnTicketID = GeneralUtils.CreateTypedDataTableColumn("TicketID", @"TicketID", typeof(System.Int64), this.Columns);
			_columnTariffID = GeneralUtils.CreateTypedDataTableColumn("TariffID", @"TariffID", typeof(System.Int64), this.Columns);
			_columnCardPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("CardPrintedNumber", @"CardPrintedNumber", typeof(System.String), this.Columns);
			_columnStartStreet = GeneralUtils.CreateTypedDataTableColumn("StartStreet", @"StartStreet", typeof(System.String), this.Columns);
			_columnStartStreetNumber = GeneralUtils.CreateTypedDataTableColumn("StartStreetNumber", @"StartStreetNumber", typeof(System.String), this.Columns);
			_columnStopStreet = GeneralUtils.CreateTypedDataTableColumn("StopStreet", @"StopStreet", typeof(System.String), this.Columns);
			_columnStopStreetNumber = GeneralUtils.CreateTypedDataTableColumn("StopStreetNumber", @"StopStreetNumber", typeof(System.String), this.Columns);
			_columnRelationFrom = GeneralUtils.CreateTypedDataTableColumn("RelationFrom", @"RelationFrom", typeof(System.Int64), this.Columns);
			_columnRelationTo = GeneralUtils.CreateTypedDataTableColumn("RelationTo", @"RelationTo", typeof(System.Int64), this.Columns);
			_columnFareMatrixEntryPriority = GeneralUtils.CreateTypedDataTableColumn("FareMatrixEntryPriority", @"FareMatrixEntryPriority", typeof(System.Int64), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnCardHolderID = this.Columns["CardHolderID"];
			_columnSchoolYearName = this.Columns["SchoolYearName"];
			_columnSchoolYearID = this.Columns["SchoolYearID"];
			_columnCardHolderFirstName = this.Columns["CardHolderFirstName"];
			_columnCardHolderLastName = this.Columns["CardHolderLastName"];
			_columnCardHolderDateOfBirth = this.Columns["CardHolderDateOfBirth"];
			_columnCardHolderGender = this.Columns["CardHolderGender"];
			_columnSchoolPrintName = this.Columns["SchoolPrintName"];
			_columnSchoolNumber = this.Columns["SchoolNumber"];
			_columnOrdererNumber = this.Columns["OrdererNumber"];
			_columnOrdererPrintName = this.Columns["OrdererPrintName"];
			_columnOrdererOrganizationID = this.Columns["OrdererOrganizationID"];
			_columnNumberOfCardPrints = this.Columns["NumberOfCardPrints"];
			_columnCardID = this.Columns["CardID"];
			_columnCardState = this.Columns["CardState"];
			_columnCardExpiration = this.Columns["CardExpiration"];
			_columnCardSerialNumber = this.Columns["CardSerialNumber"];
			_columnProductID = this.Columns["ProductID"];
			_columnTicketInternalNumber = this.Columns["TicketInternalNumber"];
			_columnTicketName = this.Columns["TicketName"];
			_columnTicketID = this.Columns["TicketID"];
			_columnTariffID = this.Columns["TariffID"];
			_columnCardPrintedNumber = this.Columns["CardPrintedNumber"];
			_columnStartStreet = this.Columns["StartStreet"];
			_columnStartStreetNumber = this.Columns["StartStreetNumber"];
			_columnStopStreet = this.Columns["StopStreet"];
			_columnStopStreetNumber = this.Columns["StopStreetNumber"];
			_columnRelationFrom = this.Columns["RelationFrom"];
			_columnRelationTo = this.Columns["RelationTo"];
			_columnFareMatrixEntryPriority = this.Columns["FareMatrixEntryPriority"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			StudentCardPrintTypedView cloneToReturn = ((StudentCardPrintTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new StudentCardPrintTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return StudentCardPrintTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return StudentCardPrintTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'CardHolderID'</summary>
		internal DataColumn CardHolderIDColumn 
		{
			get { return _columnCardHolderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolYearName'</summary>
		internal DataColumn SchoolYearNameColumn 
		{
			get { return _columnSchoolYearName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolYearID'</summary>
		internal DataColumn SchoolYearIDColumn 
		{
			get { return _columnSchoolYearID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardHolderFirstName'</summary>
		internal DataColumn CardHolderFirstNameColumn 
		{
			get { return _columnCardHolderFirstName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardHolderLastName'</summary>
		internal DataColumn CardHolderLastNameColumn 
		{
			get { return _columnCardHolderLastName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardHolderDateOfBirth'</summary>
		internal DataColumn CardHolderDateOfBirthColumn 
		{
			get { return _columnCardHolderDateOfBirth; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardHolderGender'</summary>
		internal DataColumn CardHolderGenderColumn 
		{
			get { return _columnCardHolderGender; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolPrintName'</summary>
		internal DataColumn SchoolPrintNameColumn 
		{
			get { return _columnSchoolPrintName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'SchoolNumber'</summary>
		internal DataColumn SchoolNumberColumn 
		{
			get { return _columnSchoolNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrdererNumber'</summary>
		internal DataColumn OrdererNumberColumn 
		{
			get { return _columnOrdererNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrdererPrintName'</summary>
		internal DataColumn OrdererPrintNameColumn 
		{
			get { return _columnOrdererPrintName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrdererOrganizationID'</summary>
		internal DataColumn OrdererOrganizationIDColumn 
		{
			get { return _columnOrdererOrganizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'NumberOfCardPrints'</summary>
		internal DataColumn NumberOfCardPrintsColumn 
		{
			get { return _columnNumberOfCardPrints; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardID'</summary>
		internal DataColumn CardIDColumn 
		{
			get { return _columnCardID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardState'</summary>
		internal DataColumn CardStateColumn 
		{
			get { return _columnCardState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardExpiration'</summary>
		internal DataColumn CardExpirationColumn 
		{
			get { return _columnCardExpiration; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardSerialNumber'</summary>
		internal DataColumn CardSerialNumberColumn 
		{
			get { return _columnCardSerialNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ProductID'</summary>
		internal DataColumn ProductIDColumn 
		{
			get { return _columnProductID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketInternalNumber'</summary>
		internal DataColumn TicketInternalNumberColumn 
		{
			get { return _columnTicketInternalNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketName'</summary>
		internal DataColumn TicketNameColumn 
		{
			get { return _columnTicketName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TicketID'</summary>
		internal DataColumn TicketIDColumn 
		{
			get { return _columnTicketID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TariffID'</summary>
		internal DataColumn TariffIDColumn 
		{
			get { return _columnTariffID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardPrintedNumber'</summary>
		internal DataColumn CardPrintedNumberColumn 
		{
			get { return _columnCardPrintedNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StartStreet'</summary>
		internal DataColumn StartStreetColumn 
		{
			get { return _columnStartStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StartStreetNumber'</summary>
		internal DataColumn StartStreetNumberColumn 
		{
			get { return _columnStartStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopStreet'</summary>
		internal DataColumn StopStreetColumn 
		{
			get { return _columnStopStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'StopStreetNumber'</summary>
		internal DataColumn StopStreetNumberColumn 
		{
			get { return _columnStopStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationFrom'</summary>
		internal DataColumn RelationFromColumn 
		{
			get { return _columnRelationFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'RelationTo'</summary>
		internal DataColumn RelationToColumn 
		{
			get { return _columnRelationTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMatrixEntryPriority'</summary>
		internal DataColumn FareMatrixEntryPriorityColumn 
		{
			get { return _columnFareMatrixEntryPriority; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable StudentCardPrint</summary>
	public partial class StudentCardPrintRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private StudentCardPrintTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal StudentCardPrintRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((StudentCardPrintTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field CardHolderID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDHOLDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CardHolderID
		{
			get { return IsCardHolderIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardHolderIDColumn]; }
			set { this[_parent.CardHolderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardHolderID is NULL, false otherwise.</summary>
		public bool IsCardHolderIDNull() 
		{
			return IsNull(_parent.CardHolderIDColumn);
		}

		/// <summary>Sets the TypedView field CardHolderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardHolderIDNull() 
		{
			this[_parent.CardHolderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolYearName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."SCHOOLYEARNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String SchoolYearName
		{
			get { return IsSchoolYearNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolYearNameColumn]; }
			set { this[_parent.SchoolYearNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolYearName is NULL, false otherwise.</summary>
		public bool IsSchoolYearNameNull() 
		{
			return IsNull(_parent.SchoolYearNameColumn);
		}

		/// <summary>Sets the TypedView field SchoolYearName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolYearNameNull() 
		{
			this[_parent.SchoolYearNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolYearID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."SCHOOLYEARID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 SchoolYearID
		{
			get { return IsSchoolYearIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.SchoolYearIDColumn]; }
			set { this[_parent.SchoolYearIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolYearID is NULL, false otherwise.</summary>
		public bool IsSchoolYearIDNull() 
		{
			return IsNull(_parent.SchoolYearIDColumn);
		}

		/// <summary>Sets the TypedView field SchoolYearID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolYearIDNull() 
		{
			this[_parent.SchoolYearIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardHolderFirstName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDHOLDERFIRSTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String CardHolderFirstName
		{
			get { return IsCardHolderFirstNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardHolderFirstNameColumn]; }
			set { this[_parent.CardHolderFirstNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardHolderFirstName is NULL, false otherwise.</summary>
		public bool IsCardHolderFirstNameNull() 
		{
			return IsNull(_parent.CardHolderFirstNameColumn);
		}

		/// <summary>Sets the TypedView field CardHolderFirstName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardHolderFirstNameNull() 
		{
			this[_parent.CardHolderFirstNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardHolderLastName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDHOLDERLASTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String CardHolderLastName
		{
			get { return IsCardHolderLastNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardHolderLastNameColumn]; }
			set { this[_parent.CardHolderLastNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardHolderLastName is NULL, false otherwise.</summary>
		public bool IsCardHolderLastNameNull() 
		{
			return IsNull(_parent.CardHolderLastNameColumn);
		}

		/// <summary>Sets the TypedView field CardHolderLastName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardHolderLastNameNull() 
		{
			this[_parent.CardHolderLastNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardHolderDateOfBirth</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDHOLDERDATEOFBIRTH"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CardHolderDateOfBirth
		{
			get { return IsCardHolderDateOfBirthNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CardHolderDateOfBirthColumn]; }
			set { this[_parent.CardHolderDateOfBirthColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardHolderDateOfBirth is NULL, false otherwise.</summary>
		public bool IsCardHolderDateOfBirthNull() 
		{
			return IsNull(_parent.CardHolderDateOfBirthColumn);
		}

		/// <summary>Sets the TypedView field CardHolderDateOfBirth to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardHolderDateOfBirthNull() 
		{
			this[_parent.CardHolderDateOfBirthColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardHolderGender</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDHOLDERGENDER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 CardHolderGender
		{
			get { return IsCardHolderGenderNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.CardHolderGenderColumn]; }
			set { this[_parent.CardHolderGenderColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardHolderGender is NULL, false otherwise.</summary>
		public bool IsCardHolderGenderNull() 
		{
			return IsNull(_parent.CardHolderGenderColumn);
		}

		/// <summary>Sets the TypedView field CardHolderGender to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardHolderGenderNull() 
		{
			this[_parent.CardHolderGenderColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolPrintName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."SCHOOLPRINTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String SchoolPrintName
		{
			get { return IsSchoolPrintNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolPrintNameColumn]; }
			set { this[_parent.SchoolPrintNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolPrintName is NULL, false otherwise.</summary>
		public bool IsSchoolPrintNameNull() 
		{
			return IsNull(_parent.SchoolPrintNameColumn);
		}

		/// <summary>Sets the TypedView field SchoolPrintName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolPrintNameNull() 
		{
			this[_parent.SchoolPrintNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field SchoolNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."SCHOOLNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String SchoolNumber
		{
			get { return IsSchoolNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.SchoolNumberColumn]; }
			set { this[_parent.SchoolNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field SchoolNumber is NULL, false otherwise.</summary>
		public bool IsSchoolNumberNull() 
		{
			return IsNull(_parent.SchoolNumberColumn);
		}

		/// <summary>Sets the TypedView field SchoolNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSchoolNumberNull() 
		{
			this[_parent.SchoolNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrdererNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."ORDERERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String OrdererNumber
		{
			get { return IsOrdererNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrdererNumberColumn]; }
			set { this[_parent.OrdererNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrdererNumber is NULL, false otherwise.</summary>
		public bool IsOrdererNumberNull() 
		{
			return IsNull(_parent.OrdererNumberColumn);
		}

		/// <summary>Sets the TypedView field OrdererNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrdererNumberNull() 
		{
			this[_parent.OrdererNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrdererPrintName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."ORDERERPRINTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrdererPrintName
		{
			get { return IsOrdererPrintNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrdererPrintNameColumn]; }
			set { this[_parent.OrdererPrintNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrdererPrintName is NULL, false otherwise.</summary>
		public bool IsOrdererPrintNameNull() 
		{
			return IsNull(_parent.OrdererPrintNameColumn);
		}

		/// <summary>Sets the TypedView field OrdererPrintName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrdererPrintNameNull() 
		{
			this[_parent.OrdererPrintNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrdererOrganizationID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."ORDERERORGANIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrdererOrganizationID
		{
			get { return IsOrdererOrganizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrdererOrganizationIDColumn]; }
			set { this[_parent.OrdererOrganizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrdererOrganizationID is NULL, false otherwise.</summary>
		public bool IsOrdererOrganizationIDNull() 
		{
			return IsNull(_parent.OrdererOrganizationIDColumn);
		}

		/// <summary>Sets the TypedView field OrdererOrganizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrdererOrganizationIDNull() 
		{
			this[_parent.OrdererOrganizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field NumberOfCardPrints</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."NUMBEROFCARDPRINTS"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 NumberOfCardPrints
		{
			get { return IsNumberOfCardPrintsNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.NumberOfCardPrintsColumn]; }
			set { this[_parent.NumberOfCardPrintsColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field NumberOfCardPrints is NULL, false otherwise.</summary>
		public bool IsNumberOfCardPrintsNull() 
		{
			return IsNull(_parent.NumberOfCardPrintsColumn);
		}

		/// <summary>Sets the TypedView field NumberOfCardPrints to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetNumberOfCardPrintsNull() 
		{
			this[_parent.NumberOfCardPrintsColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CardID
		{
			get { return IsCardIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardIDColumn]; }
			set { this[_parent.CardIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardID is NULL, false otherwise.</summary>
		public bool IsCardIDNull() 
		{
			return IsNull(_parent.CardIDColumn);
		}

		/// <summary>Sets the TypedView field CardID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardIDNull() 
		{
			this[_parent.CardIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardState</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDSTATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.CardState CardState
		{
			get { return IsCardStateNull() ? (VarioSL.Entities.Enumerations.CardState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.CardState)) : (VarioSL.Entities.Enumerations.CardState)this[_parent.CardStateColumn]; }
			set { this[_parent.CardStateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardState is NULL, false otherwise.</summary>
		public bool IsCardStateNull() 
		{
			return IsNull(_parent.CardStateColumn);
		}

		/// <summary>Sets the TypedView field CardState to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardStateNull() 
		{
			this[_parent.CardStateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardExpiration</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDEXPIRATION"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime CardExpiration
		{
			get { return IsCardExpirationNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CardExpirationColumn]; }
			set { this[_parent.CardExpirationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardExpiration is NULL, false otherwise.</summary>
		public bool IsCardExpirationNull() 
		{
			return IsNull(_parent.CardExpirationColumn);
		}

		/// <summary>Sets the TypedView field CardExpiration to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardExpirationNull() 
		{
			this[_parent.CardExpirationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardSerialNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDSERIALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String CardSerialNumber
		{
			get { return IsCardSerialNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardSerialNumberColumn]; }
			set { this[_parent.CardSerialNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardSerialNumber is NULL, false otherwise.</summary>
		public bool IsCardSerialNumberNull() 
		{
			return IsNull(_parent.CardSerialNumberColumn);
		}

		/// <summary>Sets the TypedView field CardSerialNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardSerialNumberNull() 
		{
			this[_parent.CardSerialNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ProductID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."PRODUCTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ProductID
		{
			get { return IsProductIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ProductIDColumn]; }
			set { this[_parent.ProductIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ProductID is NULL, false otherwise.</summary>
		public bool IsProductIDNull() 
		{
			return IsNull(_parent.ProductIDColumn);
		}

		/// <summary>Sets the TypedView field ProductID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetProductIDNull() 
		{
			this[_parent.ProductIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketInternalNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."TICKETINTERNALNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TicketInternalNumber
		{
			get { return IsTicketInternalNumberNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketInternalNumberColumn]; }
			set { this[_parent.TicketInternalNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketInternalNumber is NULL, false otherwise.</summary>
		public bool IsTicketInternalNumberNull() 
		{
			return IsNull(_parent.TicketInternalNumberColumn);
		}

		/// <summary>Sets the TypedView field TicketInternalNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketInternalNumberNull() 
		{
			this[_parent.TicketInternalNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."TICKETNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 200</remarks>
		public System.String TicketName
		{
			get { return IsTicketNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TicketNameColumn]; }
			set { this[_parent.TicketNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketName is NULL, false otherwise.</summary>
		public bool IsTicketNameNull() 
		{
			return IsNull(_parent.TicketNameColumn);
		}

		/// <summary>Sets the TypedView field TicketName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketNameNull() 
		{
			this[_parent.TicketNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TicketID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."TICKETID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TicketID
		{
			get { return IsTicketIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TicketIDColumn]; }
			set { this[_parent.TicketIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TicketID is NULL, false otherwise.</summary>
		public bool IsTicketIDNull() 
		{
			return IsNull(_parent.TicketIDColumn);
		}

		/// <summary>Sets the TypedView field TicketID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTicketIDNull() 
		{
			this[_parent.TicketIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TariffID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."TARIFFID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 TariffID
		{
			get { return IsTariffIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.TariffIDColumn]; }
			set { this[_parent.TariffIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TariffID is NULL, false otherwise.</summary>
		public bool IsTariffIDNull() 
		{
			return IsNull(_parent.TariffIDColumn);
		}

		/// <summary>Sets the TypedView field TariffID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTariffIDNull() 
		{
			this[_parent.TariffIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardPrintedNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."CARDPRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String CardPrintedNumber
		{
			get { return IsCardPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CardPrintedNumberColumn]; }
			set { this[_parent.CardPrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardPrintedNumber is NULL, false otherwise.</summary>
		public bool IsCardPrintedNumberNull() 
		{
			return IsNull(_parent.CardPrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field CardPrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardPrintedNumberNull() 
		{
			this[_parent.CardPrintedNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StartStreet</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."STARTSTREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String StartStreet
		{
			get { return IsStartStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StartStreetColumn]; }
			set { this[_parent.StartStreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StartStreet is NULL, false otherwise.</summary>
		public bool IsStartStreetNull() 
		{
			return IsNull(_parent.StartStreetColumn);
		}

		/// <summary>Sets the TypedView field StartStreet to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStartStreetNull() 
		{
			this[_parent.StartStreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StartStreetNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."STARTSTREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String StartStreetNumber
		{
			get { return IsStartStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StartStreetNumberColumn]; }
			set { this[_parent.StartStreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StartStreetNumber is NULL, false otherwise.</summary>
		public bool IsStartStreetNumberNull() 
		{
			return IsNull(_parent.StartStreetNumberColumn);
		}

		/// <summary>Sets the TypedView field StartStreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStartStreetNumberNull() 
		{
			this[_parent.StartStreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopStreet</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."STOPSTREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String StopStreet
		{
			get { return IsStopStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopStreetColumn]; }
			set { this[_parent.StopStreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopStreet is NULL, false otherwise.</summary>
		public bool IsStopStreetNull() 
		{
			return IsNull(_parent.StopStreetColumn);
		}

		/// <summary>Sets the TypedView field StopStreet to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopStreetNull() 
		{
			this[_parent.StopStreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field StopStreetNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."STOPSTREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String StopStreetNumber
		{
			get { return IsStopStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.StopStreetNumberColumn]; }
			set { this[_parent.StopStreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field StopStreetNumber is NULL, false otherwise.</summary>
		public bool IsStopStreetNumberNull() 
		{
			return IsNull(_parent.StopStreetNumberColumn);
		}

		/// <summary>Sets the TypedView field StopStreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStopStreetNumberNull() 
		{
			this[_parent.StopStreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."RELATIONFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RelationFrom
		{
			get { return IsRelationFromNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RelationFromColumn]; }
			set { this[_parent.RelationFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationFrom is NULL, false otherwise.</summary>
		public bool IsRelationFromNull() 
		{
			return IsNull(_parent.RelationFromColumn);
		}

		/// <summary>Sets the TypedView field RelationFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationFromNull() 
		{
			this[_parent.RelationFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field RelationTo</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."RELATIONTO"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 RelationTo
		{
			get { return IsRelationToNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.RelationToColumn]; }
			set { this[_parent.RelationToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field RelationTo is NULL, false otherwise.</summary>
		public bool IsRelationToNull() 
		{
			return IsNull(_parent.RelationToColumn);
		}

		/// <summary>Sets the TypedView field RelationTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetRelationToNull() 
		{
			this[_parent.RelationToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMatrixEntryPriority</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_STUDENTCARDPRINT"."FAREMATRIXENTRYPRIORITY"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 FareMatrixEntryPriority
		{
			get { return IsFareMatrixEntryPriorityNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.FareMatrixEntryPriorityColumn]; }
			set { this[_parent.FareMatrixEntryPriorityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMatrixEntryPriority is NULL, false otherwise.</summary>
		public bool IsFareMatrixEntryPriorityNull() 
		{
			return IsNull(_parent.FareMatrixEntryPriorityColumn);
		}

		/// <summary>Sets the TypedView field FareMatrixEntryPriority to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMatrixEntryPriorityNull() 
		{
			this[_parent.FareMatrixEntryPriorityColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
