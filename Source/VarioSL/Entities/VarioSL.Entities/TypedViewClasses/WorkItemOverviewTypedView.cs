﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'WorkItemOverview'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class WorkItemOverviewTypedView : TypedViewBase<WorkItemOverviewRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnWorkItemID;
		private DataColumn _columnCardID;
		private DataColumn _columnFareMediaID;
		private DataColumn _columnPrintedNumber;
		private DataColumn _columnOrderID;
		private DataColumn _columnContractID;
		private DataColumn _columnContractNumber;
		private DataColumn _columnOrganizationID;
		private DataColumn _columnWorkItemSubjectID;
		private DataColumn _columnTypeDiscriminator;
		private DataColumn _columnTitle;
		private DataColumn _columnMemo;
		private DataColumn _columnSource;
		private DataColumn _columnState;
		private DataColumn _columnAssignee;
		private DataColumn _columnCreatedBy;
		private DataColumn _columnCreated;
		private DataColumn _columnAssigned;
		private DataColumn _columnExecuted;
		private DataColumn _columnLastModified;
		private DataColumn _columnClientID;
		private DataColumn _columnClientName;
		private DataColumn _columnOrderNumber;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 23;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static WorkItemOverviewTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public WorkItemOverviewTypedView():base("WorkItemOverview")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WorkItemOverviewTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.WorkItemOverviewTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.WorkItemOverviewTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new WorkItemOverviewRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WorkItemID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CardID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("FareMediaID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PrintedNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("WorkItemSubjectID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TypeDiscriminator", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Memo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Source", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Assignee", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Assigned", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("Executed", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrderNumber", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "WorkItemOverview";
			_columnWorkItemID = GeneralUtils.CreateTypedDataTableColumn("WorkItemID", @"WorkItemID", typeof(System.Int64), this.Columns);
			_columnCardID = GeneralUtils.CreateTypedDataTableColumn("CardID", @"CardID", typeof(System.Int64), this.Columns);
			_columnFareMediaID = GeneralUtils.CreateTypedDataTableColumn("FareMediaID", @"FareMediaID", typeof(System.String), this.Columns);
			_columnPrintedNumber = GeneralUtils.CreateTypedDataTableColumn("PrintedNumber", @"PrintedNumber", typeof(System.String), this.Columns);
			_columnOrderID = GeneralUtils.CreateTypedDataTableColumn("OrderID", @"OrderID", typeof(System.Int64), this.Columns);
			_columnContractID = GeneralUtils.CreateTypedDataTableColumn("ContractID", @"ContractID", typeof(System.Int64), this.Columns);
			_columnContractNumber = GeneralUtils.CreateTypedDataTableColumn("ContractNumber", @"ContractNumber", typeof(System.String), this.Columns);
			_columnOrganizationID = GeneralUtils.CreateTypedDataTableColumn("OrganizationID", @"OrganizationID", typeof(System.Int64), this.Columns);
			_columnWorkItemSubjectID = GeneralUtils.CreateTypedDataTableColumn("WorkItemSubjectID", @"WorkItemSubjectID", typeof(System.Int64), this.Columns);
			_columnTypeDiscriminator = GeneralUtils.CreateTypedDataTableColumn("TypeDiscriminator", @"TypeDiscriminator", typeof(System.String), this.Columns);
			_columnTitle = GeneralUtils.CreateTypedDataTableColumn("Title", @"Title", typeof(System.String), this.Columns);
			_columnMemo = GeneralUtils.CreateTypedDataTableColumn("Memo", @"Memo", typeof(System.String), this.Columns);
			_columnSource = GeneralUtils.CreateTypedDataTableColumn("Source", @"Source", typeof(VarioSL.Entities.Enumerations.Source), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(VarioSL.Entities.Enumerations.WorkItemState), this.Columns);
			_columnAssignee = GeneralUtils.CreateTypedDataTableColumn("Assignee", @"Assignee", typeof(System.Int64), this.Columns);
			_columnCreatedBy = GeneralUtils.CreateTypedDataTableColumn("CreatedBy", @"CreatedBy", typeof(System.String), this.Columns);
			_columnCreated = GeneralUtils.CreateTypedDataTableColumn("Created", @"Created", typeof(System.DateTime), this.Columns);
			_columnAssigned = GeneralUtils.CreateTypedDataTableColumn("Assigned", @"Assigned", typeof(System.DateTime), this.Columns);
			_columnExecuted = GeneralUtils.CreateTypedDataTableColumn("Executed", @"Executed", typeof(System.DateTime), this.Columns);
			_columnLastModified = GeneralUtils.CreateTypedDataTableColumn("LastModified", @"LastModified", typeof(System.DateTime), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnClientName = GeneralUtils.CreateTypedDataTableColumn("ClientName", @"ClientName", typeof(System.String), this.Columns);
			_columnOrderNumber = GeneralUtils.CreateTypedDataTableColumn("OrderNumber", @"OrderNumber", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnWorkItemID = this.Columns["WorkItemID"];
			_columnCardID = this.Columns["CardID"];
			_columnFareMediaID = this.Columns["FareMediaID"];
			_columnPrintedNumber = this.Columns["PrintedNumber"];
			_columnOrderID = this.Columns["OrderID"];
			_columnContractID = this.Columns["ContractID"];
			_columnContractNumber = this.Columns["ContractNumber"];
			_columnOrganizationID = this.Columns["OrganizationID"];
			_columnWorkItemSubjectID = this.Columns["WorkItemSubjectID"];
			_columnTypeDiscriminator = this.Columns["TypeDiscriminator"];
			_columnTitle = this.Columns["Title"];
			_columnMemo = this.Columns["Memo"];
			_columnSource = this.Columns["Source"];
			_columnState = this.Columns["State"];
			_columnAssignee = this.Columns["Assignee"];
			_columnCreatedBy = this.Columns["CreatedBy"];
			_columnCreated = this.Columns["Created"];
			_columnAssigned = this.Columns["Assigned"];
			_columnExecuted = this.Columns["Executed"];
			_columnLastModified = this.Columns["LastModified"];
			_columnClientID = this.Columns["ClientID"];
			_columnClientName = this.Columns["ClientName"];
			_columnOrderNumber = this.Columns["OrderNumber"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			WorkItemOverviewTypedView cloneToReturn = ((WorkItemOverviewTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new WorkItemOverviewTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return WorkItemOverviewTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return WorkItemOverviewTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'WorkItemID'</summary>
		internal DataColumn WorkItemIDColumn 
		{
			get { return _columnWorkItemID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CardID'</summary>
		internal DataColumn CardIDColumn 
		{
			get { return _columnCardID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'FareMediaID'</summary>
		internal DataColumn FareMediaIDColumn 
		{
			get { return _columnFareMediaID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PrintedNumber'</summary>
		internal DataColumn PrintedNumberColumn 
		{
			get { return _columnPrintedNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderID'</summary>
		internal DataColumn OrderIDColumn 
		{
			get { return _columnOrderID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractID'</summary>
		internal DataColumn ContractIDColumn 
		{
			get { return _columnContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractNumber'</summary>
		internal DataColumn ContractNumberColumn 
		{
			get { return _columnContractNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationID'</summary>
		internal DataColumn OrganizationIDColumn 
		{
			get { return _columnOrganizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'WorkItemSubjectID'</summary>
		internal DataColumn WorkItemSubjectIDColumn 
		{
			get { return _columnWorkItemSubjectID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TypeDiscriminator'</summary>
		internal DataColumn TypeDiscriminatorColumn 
		{
			get { return _columnTypeDiscriminator; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Title'</summary>
		internal DataColumn TitleColumn 
		{
			get { return _columnTitle; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Memo'</summary>
		internal DataColumn MemoColumn 
		{
			get { return _columnMemo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Source'</summary>
		internal DataColumn SourceColumn 
		{
			get { return _columnSource; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Assignee'</summary>
		internal DataColumn AssigneeColumn 
		{
			get { return _columnAssignee; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CreatedBy'</summary>
		internal DataColumn CreatedByColumn 
		{
			get { return _columnCreatedBy; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Created'</summary>
		internal DataColumn CreatedColumn 
		{
			get { return _columnCreated; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Assigned'</summary>
		internal DataColumn AssignedColumn 
		{
			get { return _columnAssigned; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'Executed'</summary>
		internal DataColumn ExecutedColumn 
		{
			get { return _columnExecuted; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastModified'</summary>
		internal DataColumn LastModifiedColumn 
		{
			get { return _columnLastModified; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientName'</summary>
		internal DataColumn ClientNameColumn 
		{
			get { return _columnClientName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrderNumber'</summary>
		internal DataColumn OrderNumberColumn 
		{
			get { return _columnOrderNumber; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable WorkItemOverview</summary>
	public partial class WorkItemOverviewRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private WorkItemOverviewTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal WorkItemOverviewRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((WorkItemOverviewTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field WorkItemID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."WORKITEMID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WorkItemID
		{
			get { return IsWorkItemIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WorkItemIDColumn]; }
			set { this[_parent.WorkItemIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WorkItemID is NULL, false otherwise.</summary>
		public bool IsWorkItemIDNull() 
		{
			return IsNull(_parent.WorkItemIDColumn);
		}

		/// <summary>Sets the TypedView field WorkItemID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWorkItemIDNull() 
		{
			this[_parent.WorkItemIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CardID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CARDID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 CardID
		{
			get { return IsCardIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.CardIDColumn]; }
			set { this[_parent.CardIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CardID is NULL, false otherwise.</summary>
		public bool IsCardIDNull() 
		{
			return IsNull(_parent.CardIDColumn);
		}

		/// <summary>Sets the TypedView field CardID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCardIDNull() 
		{
			this[_parent.CardIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field FareMediaID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."FAREMEDIAID"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String FareMediaID
		{
			get { return IsFareMediaIDNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.FareMediaIDColumn]; }
			set { this[_parent.FareMediaIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field FareMediaID is NULL, false otherwise.</summary>
		public bool IsFareMediaIDNull() 
		{
			return IsNull(_parent.FareMediaIDColumn);
		}

		/// <summary>Sets the TypedView field FareMediaID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetFareMediaIDNull() 
		{
			this[_parent.FareMediaIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PrintedNumber</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."PRINTEDNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 100</remarks>
		public System.String PrintedNumber
		{
			get { return IsPrintedNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PrintedNumberColumn]; }
			set { this[_parent.PrintedNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PrintedNumber is NULL, false otherwise.</summary>
		public bool IsPrintedNumberNull() 
		{
			return IsNull(_parent.PrintedNumberColumn);
		}

		/// <summary>Sets the TypedView field PrintedNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPrintedNumberNull() 
		{
			this[_parent.PrintedNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."ORDERID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrderID
		{
			get { return IsOrderIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrderIDColumn]; }
			set { this[_parent.OrderIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderID is NULL, false otherwise.</summary>
		public bool IsOrderIDNull() 
		{
			return IsNull(_parent.OrderIDColumn);
		}

		/// <summary>Sets the TypedView field OrderID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderIDNull() 
		{
			this[_parent.OrderIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ContractID
		{
			get { return IsContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ContractIDColumn]; }
			set { this[_parent.ContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractID is NULL, false otherwise.</summary>
		public bool IsContractIDNull() 
		{
			return IsNull(_parent.ContractIDColumn);
		}

		/// <summary>Sets the TypedView field ContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractIDNull() 
		{
			this[_parent.ContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractNumber</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CONTRACTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ContractNumber
		{
			get { return IsContractNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ContractNumberColumn]; }
			set { this[_parent.ContractNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractNumber is NULL, false otherwise.</summary>
		public bool IsContractNumberNull() 
		{
			return IsNull(_parent.ContractNumberColumn);
		}

		/// <summary>Sets the TypedView field ContractNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractNumberNull() 
		{
			this[_parent.ContractNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."ORGANIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrganizationID
		{
			get { return IsOrganizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrganizationIDColumn]; }
			set { this[_parent.OrganizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationID is NULL, false otherwise.</summary>
		public bool IsOrganizationIDNull() 
		{
			return IsNull(_parent.OrganizationIDColumn);
		}

		/// <summary>Sets the TypedView field OrganizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationIDNull() 
		{
			this[_parent.OrganizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field WorkItemSubjectID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."WORKITEMSUBJECTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 WorkItemSubjectID
		{
			get { return IsWorkItemSubjectIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.WorkItemSubjectIDColumn]; }
			set { this[_parent.WorkItemSubjectIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field WorkItemSubjectID is NULL, false otherwise.</summary>
		public bool IsWorkItemSubjectIDNull() 
		{
			return IsNull(_parent.WorkItemSubjectIDColumn);
		}

		/// <summary>Sets the TypedView field WorkItemSubjectID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetWorkItemSubjectIDNull() 
		{
			this[_parent.WorkItemSubjectIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TypeDiscriminator</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."TYPEDISCRIMINATOR"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String TypeDiscriminator
		{
			get { return IsTypeDiscriminatorNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TypeDiscriminatorColumn]; }
			set { this[_parent.TypeDiscriminatorColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TypeDiscriminator is NULL, false otherwise.</summary>
		public bool IsTypeDiscriminatorNull() 
		{
			return IsNull(_parent.TypeDiscriminatorColumn);
		}

		/// <summary>Sets the TypedView field TypeDiscriminator to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTypeDiscriminatorNull() 
		{
			this[_parent.TypeDiscriminatorColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Title</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."TITLE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String Title
		{
			get { return IsTitleNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.TitleColumn]; }
			set { this[_parent.TitleColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Title is NULL, false otherwise.</summary>
		public bool IsTitleNull() 
		{
			return IsNull(_parent.TitleColumn);
		}

		/// <summary>Sets the TypedView field Title to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTitleNull() 
		{
			this[_parent.TitleColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Memo</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."MEMO"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String Memo
		{
			get { return IsMemoNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.MemoColumn]; }
			set { this[_parent.MemoColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Memo is NULL, false otherwise.</summary>
		public bool IsMemoNull() 
		{
			return IsNull(_parent.MemoColumn);
		}

		/// <summary>Sets the TypedView field Memo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetMemoNull() 
		{
			this[_parent.MemoColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Source</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."SOURCE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.Source Source
		{
			get { return IsSourceNull() ? (VarioSL.Entities.Enumerations.Source)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.Source)) : (VarioSL.Entities.Enumerations.Source)this[_parent.SourceColumn]; }
			set { this[_parent.SourceColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Source is NULL, false otherwise.</summary>
		public bool IsSourceNull() 
		{
			return IsNull(_parent.SourceColumn);
		}

		/// <summary>Sets the TypedView field Source to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetSourceNull() 
		{
			this[_parent.SourceColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.WorkItemState State
		{
			get { return IsStateNull() ? (VarioSL.Entities.Enumerations.WorkItemState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.WorkItemState)) : (VarioSL.Entities.Enumerations.WorkItemState)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Assignee</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."ASSIGNEE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 Assignee
		{
			get { return IsAssigneeNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AssigneeColumn]; }
			set { this[_parent.AssigneeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Assignee is NULL, false otherwise.</summary>
		public bool IsAssigneeNull() 
		{
			return IsNull(_parent.AssigneeColumn);
		}

		/// <summary>Sets the TypedView field Assignee to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAssigneeNull() 
		{
			this[_parent.AssigneeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CreatedBy</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CREATEDBY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 512</remarks>
		public System.String CreatedBy
		{
			get { return IsCreatedByNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CreatedByColumn]; }
			set { this[_parent.CreatedByColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CreatedBy is NULL, false otherwise.</summary>
		public bool IsCreatedByNull() 
		{
			return IsNull(_parent.CreatedByColumn);
		}

		/// <summary>Sets the TypedView field CreatedBy to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreatedByNull() 
		{
			this[_parent.CreatedByColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Created</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CREATED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime Created
		{
			get { return IsCreatedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.CreatedColumn]; }
			set { this[_parent.CreatedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Created is NULL, false otherwise.</summary>
		public bool IsCreatedNull() 
		{
			return IsNull(_parent.CreatedColumn);
		}

		/// <summary>Sets the TypedView field Created to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCreatedNull() 
		{
			this[_parent.CreatedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Assigned</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."ASSIGNED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime Assigned
		{
			get { return IsAssignedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.AssignedColumn]; }
			set { this[_parent.AssignedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Assigned is NULL, false otherwise.</summary>
		public bool IsAssignedNull() 
		{
			return IsNull(_parent.AssignedColumn);
		}

		/// <summary>Sets the TypedView field Assigned to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAssignedNull() 
		{
			this[_parent.AssignedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field Executed</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."EXECUTED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime Executed
		{
			get { return IsExecutedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ExecutedColumn]; }
			set { this[_parent.ExecutedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field Executed is NULL, false otherwise.</summary>
		public bool IsExecutedNull() 
		{
			return IsNull(_parent.ExecutedColumn);
		}

		/// <summary>Sets the TypedView field Executed to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetExecutedNull() 
		{
			this[_parent.ExecutedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastModified</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."LASTMODIFIED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastModified
		{
			get { return IsLastModifiedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastModifiedColumn]; }
			set { this[_parent.LastModifiedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastModified is NULL, false otherwise.</summary>
		public bool IsLastModifiedNull() 
		{
			return IsNull(_parent.LastModifiedColumn);
		}

		/// <summary>Sets the TypedView field LastModified to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastModifiedNull() 
		{
			this[_parent.LastModifiedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientName</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."CLIENTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 50</remarks>
		public System.String ClientName
		{
			get { return IsClientNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ClientNameColumn]; }
			set { this[_parent.ClientNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientName is NULL, false otherwise.</summary>
		public bool IsClientNameNull() 
		{
			return IsNull(_parent.ClientNameColumn);
		}

		/// <summary>Sets the TypedView field ClientName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientNameNull() 
		{
			this[_parent.ClientNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrderNumber</summary>
		/// <remarks>Mapped on view field: "SL_WORKITEMOVERVIEW"."ORDERNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrderNumber
		{
			get { return IsOrderNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrderNumberColumn]; }
			set { this[_parent.OrderNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrderNumber is NULL, false otherwise.</summary>
		public bool IsOrderNumberNull() 
		{
			return IsNull(_parent.OrderNumberColumn);
		}

		/// <summary>Sets the TypedView field OrderNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrderNumberNull() 
		{
			this[_parent.OrderNumberColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
