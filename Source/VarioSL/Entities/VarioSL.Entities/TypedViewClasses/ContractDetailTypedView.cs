﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using VarioSL.Entities;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.TypedViewClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	/// <summary>Typed datatable for the view 'ContractDetail'.</summary>
	[Serializable, System.ComponentModel.DesignerCategory("Code")]
	[ToolboxItem(true)]
	[DesignTimeVisible(true)]
	public partial class ContractDetailTypedView : TypedViewBase<ContractDetailRow>, ITypedView
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesView 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private DataColumn _columnContractID;
		private DataColumn _columnOrganizationID;
		private DataColumn _columnState;
		private DataColumn _columnContractNumber;
		private DataColumn _columnValidFrom;
		private DataColumn _columnValidTo;
		private DataColumn _columnClientID;
		private DataColumn _columnPaymentModalityID;
		private DataColumn _columnLastUser;
		private DataColumn _columnLastModified;
		private DataColumn _columnTransactionCounter;
		private DataColumn _columnClientAuthenticationToken;
		private DataColumn _columnAccountingModeID;
		private DataColumn _columnContractType;
		private DataColumn _columnAdditionalText;
		private DataColumn _columnPersonFirstName;
		private DataColumn _columnPersonLastName;
		private DataColumn _columnPersonDateOfBirth;
		private DataColumn _columnPersonGender;
		private DataColumn _columnPersonSalutation;
		private DataColumn _columnPersonTitle;
		private DataColumn _columnAddressID;
		private DataColumn _columnAddressStreet;
		private DataColumn _columnAddressPostalCode;
		private DataColumn _columnAddressCity;
		private DataColumn _columnAddressCountry;
		private DataColumn _columnAddressRegion;
		private DataColumn _columnAddressAddressee;
		private DataColumn _columnAddressAddressField1;
		private DataColumn _columnAddressAddressField2;
		private DataColumn _columnAddressStreetNumber;
		private DataColumn _columnOrganizationNumber;
		private DataColumn _columnOrganizationName;
		private DataColumn _columnBankConnectionDataIban;
		private DataColumn _columnBankConnectionDataBic;
		private DataColumn _columnBankConnectionDataLastUsage;
		private DataColumn _columnLastInvoiceNumber;
		private DataColumn _columnLastInvoiceCreationDate;
		private DataColumn _columnCommentText;
		private DataColumn _columnPaymentModalityPaymentMethodID;
		private DataColumn _columnPaymentModalityPaymentMethod;
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		private static Hashtable	_customProperties;
		private static Hashtable	_fieldsCustomProperties;
		#endregion

		#region Class Constants
		private const int AmountOfFields = 41;
		#endregion

		/// <summary>Static CTor for setting up custom property hashtables.</summary>
		static ContractDetailTypedView()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		public ContractDetailTypedView():base("ContractDetail")
		{
			InitClass();
		}
		
		/// <summary>Protected constructor for deserialization.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ContractDetailTypedView(SerializationInfo info, StreamingContext context):base(info, context)
		{
			InitMembers();
		}

		/// <summary> Fills itself with data. it builds a dynamic query and loads itself with that query. 
		/// Will use no sort filter, no select filter, will allow duplicate rows and will not limit the amount of rows returned</summary>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill()
		{
			return Fill(0, null, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter, will allow duplicate rows.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, true, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query. Will not use a filter.</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, null, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, null, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, null, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is
		/// used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 	IGroupByCollection groupByClause)
		{
			return Fill(maxNumberOfItemsToReturn, sortClauses, allowDuplicates, selectFilter, transactionToUse, groupByClause, 0, 0);
		}

		/// <summary>Fills itself with data. it builds a dynamic query and loads itself with that query, using the specified filter</summary>
		/// <param name="maxNumberOfItemsToReturn">The maximum amount of rows to return. specifying 0 means all rows are returned</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When null is specified, no sorting is applied.</param>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="selectFilter">Predicate expression to filter on the rows inserted in this TypedView object.</param>
		/// <param name="transactionToUse">The transaction object to use. Can be null. If specified, the connection object of the transaction is used to fill the TypedView, which avoids deadlocks on SqlServer.</param>
		/// <param name="groupByClause">GroupByCollection with fields to group by on.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if fill succeeded, false otherwise</returns>
		public virtual bool Fill(long maxNumberOfItemsToReturn, ISortExpression sortClauses, bool allowDuplicates, IPredicate selectFilter, ITransaction transactionToUse, 
								 IGroupByCollection groupByClause, int pageNumber, int pageSize)
		{
			IEntityFields fieldsInResultset = GetFields();
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalFields 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			return DAOFactory.CreateTypedListDAO().GetMultiAsDataTable(fieldsInResultset, this, maxNumberOfItemsToReturn, sortClauses, selectFilter, null, allowDuplicates, groupByClause, transactionToUse, pageNumber, pageSize);
		}

		/// <summary>Gets the amount of rows in the database for this typed view, not skipping duplicates</summary>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount()
		{
			return GetDbCount(true, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates)
		{
			return GetDbCount(allowDuplicates, null);
		}
		
		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public int GetDbCount(bool allowDuplicates, IPredicateExpression filter)
		{
			return GetDbCount(allowDuplicates, filter, null);
		}

		/// <summary>Gets the amount of rows in the database for this typed view.</summary>
		/// <param name="allowDuplicates">Flag to allow duplicate rows (true) or not (false)</param>
		/// <param name="filter">The filter to apply for the count retrieval</param>
		/// <param name="groupByClause">group by clause to embed in the query</param>
		/// <returns>the number of rows in the set defined by the passed in query elements</returns>
		public virtual int GetDbCount(bool allowDuplicates, IPredicateExpression filter, GroupByCollection groupByClause)
		{
			IEntityFields fieldsInResultset = EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.ContractDetailTypedView);
			return DAOFactory.CreateTypedListDAO().GetDbCount(fieldsInResultset, null, filter, null, groupByClause, allowDuplicates);
		}

		/// <summary>Gets the fields of this typed view</summary>
		/// <returns>IEntityFields object</returns>
		public virtual IEntityFields GetFields()
		{
			return EntityFieldsFactory.CreateTypedViewEntityFieldsObject(TypedViewType.ContractDetailTypedView);
		}

		/// <summary>Creates a new typed row during the build of the datatable during a Fill session by a dataadapter.</summary>
		/// <param name="rowBuilder">supplied row builder to pass to the typed row</param>
		/// <returns>the new typed datarow</returns>
		protected override DataRow NewRowFromBuilder(DataRowBuilder rowBuilder) 
		{
			return new ContractDetailRow(rowBuilder);
		}

		/// <summary>Initializes the hashtables for the typed view type and typed view field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Hashtable();
			_fieldsCustomProperties = new Hashtable();
			Hashtable fieldHashtable;
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("State", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidFrom", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ValidTo", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentModalityID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastUser", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastModified", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("TransactionCounter", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ClientAuthenticationToken", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AccountingModeID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("ContractType", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AdditionalText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonFirstName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonLastName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonDateOfBirth", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonGender", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonSalutation", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PersonTitle", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressStreet", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressPostalCode", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressCity", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressCountry", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressRegion", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressAddressee", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressAddressField1", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressAddressField2", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("AddressStreetNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("OrganizationName", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankConnectionDataIban", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankConnectionDataBic", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("BankConnectionDataLastUsage", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastInvoiceNumber", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("LastInvoiceCreationDate", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("CommentText", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentModalityPaymentMethodID", fieldHashtable);
			fieldHashtable = new Hashtable();
			_fieldsCustomProperties.Add("PaymentModalityPaymentMethod", fieldHashtable);
		}

		/// <summary>Initialize the datastructures.</summary>
		protected override void InitClass()
		{
			TableName = "ContractDetail";
			_columnContractID = GeneralUtils.CreateTypedDataTableColumn("ContractID", @"ContractID", typeof(System.Int64), this.Columns);
			_columnOrganizationID = GeneralUtils.CreateTypedDataTableColumn("OrganizationID", @"OrganizationID", typeof(System.Int64), this.Columns);
			_columnState = GeneralUtils.CreateTypedDataTableColumn("State", @"State", typeof(VarioSL.Entities.Enumerations.ContractState), this.Columns);
			_columnContractNumber = GeneralUtils.CreateTypedDataTableColumn("ContractNumber", @"ContractNumber", typeof(System.String), this.Columns);
			_columnValidFrom = GeneralUtils.CreateTypedDataTableColumn("ValidFrom", @"ValidFrom", typeof(System.DateTime), this.Columns);
			_columnValidTo = GeneralUtils.CreateTypedDataTableColumn("ValidTo", @"ValidTo", typeof(System.DateTime), this.Columns);
			_columnClientID = GeneralUtils.CreateTypedDataTableColumn("ClientID", @"ClientID", typeof(System.Int64), this.Columns);
			_columnPaymentModalityID = GeneralUtils.CreateTypedDataTableColumn("PaymentModalityID", @"PaymentModalityID", typeof(System.Int64), this.Columns);
			_columnLastUser = GeneralUtils.CreateTypedDataTableColumn("LastUser", @"LastUser", typeof(System.String), this.Columns);
			_columnLastModified = GeneralUtils.CreateTypedDataTableColumn("LastModified", @"LastModified", typeof(System.DateTime), this.Columns);
			_columnTransactionCounter = GeneralUtils.CreateTypedDataTableColumn("TransactionCounter", @"TransactionCounter", typeof(System.Decimal), this.Columns);
			_columnClientAuthenticationToken = GeneralUtils.CreateTypedDataTableColumn("ClientAuthenticationToken", @"ClientAuthenticationToken", typeof(System.String), this.Columns);
			_columnAccountingModeID = GeneralUtils.CreateTypedDataTableColumn("AccountingModeID", @"AccountingModeID", typeof(System.Int64), this.Columns);
			_columnContractType = GeneralUtils.CreateTypedDataTableColumn("ContractType", @"ContractType", typeof(VarioSL.Entities.Enumerations.ContractType), this.Columns);
			_columnAdditionalText = GeneralUtils.CreateTypedDataTableColumn("AdditionalText", @"AdditionalText", typeof(System.String), this.Columns);
			_columnPersonFirstName = GeneralUtils.CreateTypedDataTableColumn("PersonFirstName", @"PersonFirstName", typeof(System.String), this.Columns);
			_columnPersonLastName = GeneralUtils.CreateTypedDataTableColumn("PersonLastName", @"PersonLastName", typeof(System.String), this.Columns);
			_columnPersonDateOfBirth = GeneralUtils.CreateTypedDataTableColumn("PersonDateOfBirth", @"PersonDateOfBirth", typeof(System.DateTime), this.Columns);
			_columnPersonGender = GeneralUtils.CreateTypedDataTableColumn("PersonGender", @"PersonGender", typeof(System.Int32), this.Columns);
			_columnPersonSalutation = GeneralUtils.CreateTypedDataTableColumn("PersonSalutation", @"PersonSalutation", typeof(System.Int32), this.Columns);
			_columnPersonTitle = GeneralUtils.CreateTypedDataTableColumn("PersonTitle", @"PersonTitle", typeof(System.String), this.Columns);
			_columnAddressID = GeneralUtils.CreateTypedDataTableColumn("AddressID", @"AddressID", typeof(System.Int64), this.Columns);
			_columnAddressStreet = GeneralUtils.CreateTypedDataTableColumn("AddressStreet", @"AddressStreet", typeof(System.String), this.Columns);
			_columnAddressPostalCode = GeneralUtils.CreateTypedDataTableColumn("AddressPostalCode", @"AddressPostalCode", typeof(System.String), this.Columns);
			_columnAddressCity = GeneralUtils.CreateTypedDataTableColumn("AddressCity", @"AddressCity", typeof(System.String), this.Columns);
			_columnAddressCountry = GeneralUtils.CreateTypedDataTableColumn("AddressCountry", @"AddressCountry", typeof(System.String), this.Columns);
			_columnAddressRegion = GeneralUtils.CreateTypedDataTableColumn("AddressRegion", @"AddressRegion", typeof(System.String), this.Columns);
			_columnAddressAddressee = GeneralUtils.CreateTypedDataTableColumn("AddressAddressee", @"AddressAddressee", typeof(System.String), this.Columns);
			_columnAddressAddressField1 = GeneralUtils.CreateTypedDataTableColumn("AddressAddressField1", @"AddressAddressField1", typeof(System.String), this.Columns);
			_columnAddressAddressField2 = GeneralUtils.CreateTypedDataTableColumn("AddressAddressField2", @"AddressAddressField2", typeof(System.String), this.Columns);
			_columnAddressStreetNumber = GeneralUtils.CreateTypedDataTableColumn("AddressStreetNumber", @"AddressStreetNumber", typeof(System.String), this.Columns);
			_columnOrganizationNumber = GeneralUtils.CreateTypedDataTableColumn("OrganizationNumber", @"OrganizationNumber", typeof(System.String), this.Columns);
			_columnOrganizationName = GeneralUtils.CreateTypedDataTableColumn("OrganizationName", @"OrganizationName", typeof(System.String), this.Columns);
			_columnBankConnectionDataIban = GeneralUtils.CreateTypedDataTableColumn("BankConnectionDataIban", @"BankConnectionDataIban", typeof(System.String), this.Columns);
			_columnBankConnectionDataBic = GeneralUtils.CreateTypedDataTableColumn("BankConnectionDataBic", @"BankConnectionDataBic", typeof(System.String), this.Columns);
			_columnBankConnectionDataLastUsage = GeneralUtils.CreateTypedDataTableColumn("BankConnectionDataLastUsage", @"BankConnectionDataLastUsage", typeof(System.DateTime), this.Columns);
			_columnLastInvoiceNumber = GeneralUtils.CreateTypedDataTableColumn("LastInvoiceNumber", @"LastInvoiceNumber", typeof(System.String), this.Columns);
			_columnLastInvoiceCreationDate = GeneralUtils.CreateTypedDataTableColumn("LastInvoiceCreationDate", @"LastInvoiceCreationDate", typeof(System.DateTime), this.Columns);
			_columnCommentText = GeneralUtils.CreateTypedDataTableColumn("CommentText", @"CommentText", typeof(System.String), this.Columns);
			_columnPaymentModalityPaymentMethodID = GeneralUtils.CreateTypedDataTableColumn("PaymentModalityPaymentMethodID", @"PaymentModalityPaymentMethodID", typeof(System.Int64), this.Columns);
			_columnPaymentModalityPaymentMethod = GeneralUtils.CreateTypedDataTableColumn("PaymentModalityPaymentMethod", @"PaymentModalityPaymentMethod", typeof(System.String), this.Columns);
	// __LLBLGENPRO_USER_CODE_REGION_START InitClass 
	// __LLBLGENPRO_USER_CODE_REGION_END 
			OnInitialized();
		}

		/// <summary>Initializes the members, after a clone action.</summary>
		private void InitMembers()
		{
			_columnContractID = this.Columns["ContractID"];
			_columnOrganizationID = this.Columns["OrganizationID"];
			_columnState = this.Columns["State"];
			_columnContractNumber = this.Columns["ContractNumber"];
			_columnValidFrom = this.Columns["ValidFrom"];
			_columnValidTo = this.Columns["ValidTo"];
			_columnClientID = this.Columns["ClientID"];
			_columnPaymentModalityID = this.Columns["PaymentModalityID"];
			_columnLastUser = this.Columns["LastUser"];
			_columnLastModified = this.Columns["LastModified"];
			_columnTransactionCounter = this.Columns["TransactionCounter"];
			_columnClientAuthenticationToken = this.Columns["ClientAuthenticationToken"];
			_columnAccountingModeID = this.Columns["AccountingModeID"];
			_columnContractType = this.Columns["ContractType"];
			_columnAdditionalText = this.Columns["AdditionalText"];
			_columnPersonFirstName = this.Columns["PersonFirstName"];
			_columnPersonLastName = this.Columns["PersonLastName"];
			_columnPersonDateOfBirth = this.Columns["PersonDateOfBirth"];
			_columnPersonGender = this.Columns["PersonGender"];
			_columnPersonSalutation = this.Columns["PersonSalutation"];
			_columnPersonTitle = this.Columns["PersonTitle"];
			_columnAddressID = this.Columns["AddressID"];
			_columnAddressStreet = this.Columns["AddressStreet"];
			_columnAddressPostalCode = this.Columns["AddressPostalCode"];
			_columnAddressCity = this.Columns["AddressCity"];
			_columnAddressCountry = this.Columns["AddressCountry"];
			_columnAddressRegion = this.Columns["AddressRegion"];
			_columnAddressAddressee = this.Columns["AddressAddressee"];
			_columnAddressAddressField1 = this.Columns["AddressAddressField1"];
			_columnAddressAddressField2 = this.Columns["AddressAddressField2"];
			_columnAddressStreetNumber = this.Columns["AddressStreetNumber"];
			_columnOrganizationNumber = this.Columns["OrganizationNumber"];
			_columnOrganizationName = this.Columns["OrganizationName"];
			_columnBankConnectionDataIban = this.Columns["BankConnectionDataIban"];
			_columnBankConnectionDataBic = this.Columns["BankConnectionDataBic"];
			_columnBankConnectionDataLastUsage = this.Columns["BankConnectionDataLastUsage"];
			_columnLastInvoiceNumber = this.Columns["LastInvoiceNumber"];
			_columnLastInvoiceCreationDate = this.Columns["LastInvoiceCreationDate"];
			_columnCommentText = this.Columns["CommentText"];
			_columnPaymentModalityPaymentMethodID = this.Columns["PaymentModalityPaymentMethodID"];
			_columnPaymentModalityPaymentMethod = this.Columns["PaymentModalityPaymentMethod"];
	// __LLBLGENPRO_USER_CODE_REGION_START InitMembers 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		}

		/// <summary>Clones this instance.</summary>
		/// <returns>A clone of this instance</returns>
		public override DataTable Clone() 
		{
			ContractDetailTypedView cloneToReturn = ((ContractDetailTypedView)(base.Clone()));
			cloneToReturn.InitMembers();
			return cloneToReturn;
		}

		/// <summary>Creates a new instance of the DataTable class.</summary>
		/// <returns>a new instance of a datatable with this schema.</returns>
		protected override DataTable CreateInstance() 
		{
			return new ContractDetailTypedView();
		}

		#region Class Property Declarations
		/// <summary>The custom properties for this TypedView type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary>The custom properties for the type of this TypedView instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable CustomPropertiesOfType
		{
			get { return ContractDetailTypedView.CustomProperties;}
		}

		/// <summary>The custom properties for the fields of this TypedView type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public static Hashtable FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary>The custom properties for the fields of the type of this TypedView instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[System.ComponentModel.Browsable(false)]
		public virtual Hashtable FieldsCustomPropertiesOfType
		{
			get { return ContractDetailTypedView.FieldsCustomProperties;}
		}
		
		/// <summary>Returns the column object belonging to the TypedView field 'ContractID'</summary>
		internal DataColumn ContractIDColumn 
		{
			get { return _columnContractID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationID'</summary>
		internal DataColumn OrganizationIDColumn 
		{
			get { return _columnOrganizationID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'State'</summary>
		internal DataColumn StateColumn 
		{
			get { return _columnState; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractNumber'</summary>
		internal DataColumn ContractNumberColumn 
		{
			get { return _columnContractNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidFrom'</summary>
		internal DataColumn ValidFromColumn 
		{
			get { return _columnValidFrom; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ValidTo'</summary>
		internal DataColumn ValidToColumn 
		{
			get { return _columnValidTo; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientID'</summary>
		internal DataColumn ClientIDColumn 
		{
			get { return _columnClientID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentModalityID'</summary>
		internal DataColumn PaymentModalityIDColumn 
		{
			get { return _columnPaymentModalityID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastUser'</summary>
		internal DataColumn LastUserColumn 
		{
			get { return _columnLastUser; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastModified'</summary>
		internal DataColumn LastModifiedColumn 
		{
			get { return _columnLastModified; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'TransactionCounter'</summary>
		internal DataColumn TransactionCounterColumn 
		{
			get { return _columnTransactionCounter; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ClientAuthenticationToken'</summary>
		internal DataColumn ClientAuthenticationTokenColumn 
		{
			get { return _columnClientAuthenticationToken; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AccountingModeID'</summary>
		internal DataColumn AccountingModeIDColumn 
		{
			get { return _columnAccountingModeID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'ContractType'</summary>
		internal DataColumn ContractTypeColumn 
		{
			get { return _columnContractType; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AdditionalText'</summary>
		internal DataColumn AdditionalTextColumn 
		{
			get { return _columnAdditionalText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonFirstName'</summary>
		internal DataColumn PersonFirstNameColumn 
		{
			get { return _columnPersonFirstName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonLastName'</summary>
		internal DataColumn PersonLastNameColumn 
		{
			get { return _columnPersonLastName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonDateOfBirth'</summary>
		internal DataColumn PersonDateOfBirthColumn 
		{
			get { return _columnPersonDateOfBirth; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonGender'</summary>
		internal DataColumn PersonGenderColumn 
		{
			get { return _columnPersonGender; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonSalutation'</summary>
		internal DataColumn PersonSalutationColumn 
		{
			get { return _columnPersonSalutation; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PersonTitle'</summary>
		internal DataColumn PersonTitleColumn 
		{
			get { return _columnPersonTitle; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressID'</summary>
		internal DataColumn AddressIDColumn 
		{
			get { return _columnAddressID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressStreet'</summary>
		internal DataColumn AddressStreetColumn 
		{
			get { return _columnAddressStreet; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressPostalCode'</summary>
		internal DataColumn AddressPostalCodeColumn 
		{
			get { return _columnAddressPostalCode; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressCity'</summary>
		internal DataColumn AddressCityColumn 
		{
			get { return _columnAddressCity; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressCountry'</summary>
		internal DataColumn AddressCountryColumn 
		{
			get { return _columnAddressCountry; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressRegion'</summary>
		internal DataColumn AddressRegionColumn 
		{
			get { return _columnAddressRegion; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressAddressee'</summary>
		internal DataColumn AddressAddresseeColumn 
		{
			get { return _columnAddressAddressee; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressAddressField1'</summary>
		internal DataColumn AddressAddressField1Column 
		{
			get { return _columnAddressAddressField1; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressAddressField2'</summary>
		internal DataColumn AddressAddressField2Column 
		{
			get { return _columnAddressAddressField2; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'AddressStreetNumber'</summary>
		internal DataColumn AddressStreetNumberColumn 
		{
			get { return _columnAddressStreetNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationNumber'</summary>
		internal DataColumn OrganizationNumberColumn 
		{
			get { return _columnOrganizationNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'OrganizationName'</summary>
		internal DataColumn OrganizationNameColumn 
		{
			get { return _columnOrganizationName; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankConnectionDataIban'</summary>
		internal DataColumn BankConnectionDataIbanColumn 
		{
			get { return _columnBankConnectionDataIban; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankConnectionDataBic'</summary>
		internal DataColumn BankConnectionDataBicColumn 
		{
			get { return _columnBankConnectionDataBic; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'BankConnectionDataLastUsage'</summary>
		internal DataColumn BankConnectionDataLastUsageColumn 
		{
			get { return _columnBankConnectionDataLastUsage; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastInvoiceNumber'</summary>
		internal DataColumn LastInvoiceNumberColumn 
		{
			get { return _columnLastInvoiceNumber; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'LastInvoiceCreationDate'</summary>
		internal DataColumn LastInvoiceCreationDateColumn 
		{
			get { return _columnLastInvoiceCreationDate; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'CommentText'</summary>
		internal DataColumn CommentTextColumn 
		{
			get { return _columnCommentText; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentModalityPaymentMethodID'</summary>
		internal DataColumn PaymentModalityPaymentMethodIDColumn 
		{
			get { return _columnPaymentModalityPaymentMethodID; }
		}
		/// <summary>Returns the column object belonging to the TypedView field 'PaymentModalityPaymentMethod'</summary>
		internal DataColumn PaymentModalityPaymentMethodColumn 
		{
			get { return _columnPaymentModalityPaymentMethod; }
		}
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalColumnProperties 
	// __LLBLGENPRO_USER_CODE_REGION_END 
 		#endregion
		
		#region Custom Typed View code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion

		#region Included Code

		#endregion
	}


	/// <summary>Typed datarow for the typed datatable ContractDetail</summary>
	public partial class ContractDetailRow : DataRow
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfacesRow 
	// __LLBLGENPRO_USER_CODE_REGION_END 
	{
		#region Class Member Declarations
		private ContractDetailTypedView	_parent;
		#endregion

		/// <summary>CTor</summary>
		/// <param name="rowBuilder">Row builder object to use when building this row</param>
		protected internal ContractDetailRow(DataRowBuilder rowBuilder) : base(rowBuilder) 
		{
			_parent = ((ContractDetailTypedView)(this.Table));
		}

		#region Class Property Declarations
		/// <summary>Gets / sets the value of the TypedView field ContractID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ContractID
		{
			get { return IsContractIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ContractIDColumn]; }
			set { this[_parent.ContractIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractID is NULL, false otherwise.</summary>
		public bool IsContractIDNull() 
		{
			return IsNull(_parent.ContractIDColumn);
		}

		/// <summary>Sets the TypedView field ContractID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractIDNull() 
		{
			this[_parent.ContractIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 OrganizationID
		{
			get { return IsOrganizationIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.OrganizationIDColumn]; }
			set { this[_parent.OrganizationIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationID is NULL, false otherwise.</summary>
		public bool IsOrganizationIDNull() 
		{
			return IsNull(_parent.OrganizationIDColumn);
		}

		/// <summary>Sets the TypedView field OrganizationID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationIDNull() 
		{
			this[_parent.OrganizationIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field State</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."STATE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.ContractState State
		{
			get { return IsStateNull() ? (VarioSL.Entities.Enumerations.ContractState)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.ContractState)) : (VarioSL.Entities.Enumerations.ContractState)this[_parent.StateColumn]; }
			set { this[_parent.StateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field State is NULL, false otherwise.</summary>
		public bool IsStateNull() 
		{
			return IsNull(_parent.StateColumn);
		}

		/// <summary>Sets the TypedView field State to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetStateNull() 
		{
			this[_parent.StateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String ContractNumber
		{
			get { return IsContractNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ContractNumberColumn]; }
			set { this[_parent.ContractNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractNumber is NULL, false otherwise.</summary>
		public bool IsContractNumberNull() 
		{
			return IsNull(_parent.ContractNumberColumn);
		}

		/// <summary>Sets the TypedView field ContractNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractNumberNull() 
		{
			this[_parent.ContractNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidFrom</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."VALIDFROM"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidFrom
		{
			get { return IsValidFromNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidFromColumn]; }
			set { this[_parent.ValidFromColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidFrom is NULL, false otherwise.</summary>
		public bool IsValidFromNull() 
		{
			return IsNull(_parent.ValidFromColumn);
		}

		/// <summary>Sets the TypedView field ValidFrom to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidFromNull() 
		{
			this[_parent.ValidFromColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ValidTo</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."VALIDTO"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime ValidTo
		{
			get { return IsValidToNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.ValidToColumn]; }
			set { this[_parent.ValidToColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ValidTo is NULL, false otherwise.</summary>
		public bool IsValidToNull() 
		{
			return IsNull(_parent.ValidToColumn);
		}

		/// <summary>Sets the TypedView field ValidTo to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetValidToNull() 
		{
			this[_parent.ValidToColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."CLIENTID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 ClientID
		{
			get { return IsClientIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.ClientIDColumn]; }
			set { this[_parent.ClientIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientID is NULL, false otherwise.</summary>
		public bool IsClientIDNull() 
		{
			return IsNull(_parent.ClientIDColumn);
		}

		/// <summary>Sets the TypedView field ClientID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientIDNull() 
		{
			this[_parent.ClientIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentModalityID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 PaymentModalityID
		{
			get { return IsPaymentModalityIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentModalityIDColumn]; }
			set { this[_parent.PaymentModalityIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentModalityID is NULL, false otherwise.</summary>
		public bool IsPaymentModalityIDNull() 
		{
			return IsNull(_parent.PaymentModalityIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentModalityID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentModalityIDNull() 
		{
			this[_parent.PaymentModalityIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastUser</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."LASTUSER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastUser
		{
			get { return IsLastUserNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastUserColumn]; }
			set { this[_parent.LastUserColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastUser is NULL, false otherwise.</summary>
		public bool IsLastUserNull() 
		{
			return IsNull(_parent.LastUserColumn);
		}

		/// <summary>Sets the TypedView field LastUser to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastUserNull() 
		{
			this[_parent.LastUserColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastModified</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."LASTMODIFIED"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastModified
		{
			get { return IsLastModifiedNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastModifiedColumn]; }
			set { this[_parent.LastModifiedColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastModified is NULL, false otherwise.</summary>
		public bool IsLastModifiedNull() 
		{
			return IsNull(_parent.LastModifiedColumn);
		}

		/// <summary>Sets the TypedView field LastModified to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastModifiedNull() 
		{
			this[_parent.LastModifiedColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field TransactionCounter</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."TRANSACTIONCOUNTER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 38, 38, 0</remarks>
		public System.Decimal TransactionCounter
		{
			get { return IsTransactionCounterNull() ? (System.Decimal)TypeDefaultValue.GetDefaultValue(typeof(System.Decimal)) : (System.Decimal)this[_parent.TransactionCounterColumn]; }
			set { this[_parent.TransactionCounterColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field TransactionCounter is NULL, false otherwise.</summary>
		public bool IsTransactionCounterNull() 
		{
			return IsNull(_parent.TransactionCounterColumn);
		}

		/// <summary>Sets the TypedView field TransactionCounter to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetTransactionCounterNull() 
		{
			this[_parent.TransactionCounterColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ClientAuthenticationToken</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."CLIENTAUTHENTICATIONTOKEN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 120</remarks>
		public System.String ClientAuthenticationToken
		{
			get { return IsClientAuthenticationTokenNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.ClientAuthenticationTokenColumn]; }
			set { this[_parent.ClientAuthenticationTokenColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ClientAuthenticationToken is NULL, false otherwise.</summary>
		public bool IsClientAuthenticationTokenNull() 
		{
			return IsNull(_parent.ClientAuthenticationTokenColumn);
		}

		/// <summary>Sets the TypedView field ClientAuthenticationToken to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetClientAuthenticationTokenNull() 
		{
			this[_parent.ClientAuthenticationTokenColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AccountingModeID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ACCOUNTINGMODEID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 AccountingModeID
		{
			get { return IsAccountingModeIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AccountingModeIDColumn]; }
			set { this[_parent.AccountingModeIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AccountingModeID is NULL, false otherwise.</summary>
		public bool IsAccountingModeIDNull() 
		{
			return IsNull(_parent.AccountingModeIDColumn);
		}

		/// <summary>Sets the TypedView field AccountingModeID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAccountingModeIDNull() 
		{
			this[_parent.AccountingModeIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field ContractType</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."CONTRACTTYPE"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public VarioSL.Entities.Enumerations.ContractType ContractType
		{
			get { return IsContractTypeNull() ? (VarioSL.Entities.Enumerations.ContractType)TypeDefaultValue.GetDefaultValue(typeof(VarioSL.Entities.Enumerations.ContractType)) : (VarioSL.Entities.Enumerations.ContractType)this[_parent.ContractTypeColumn]; }
			set { this[_parent.ContractTypeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field ContractType is NULL, false otherwise.</summary>
		public bool IsContractTypeNull() 
		{
			return IsNull(_parent.ContractTypeColumn);
		}

		/// <summary>Sets the TypedView field ContractType to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetContractTypeNull() 
		{
			this[_parent.ContractTypeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AdditionalText</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDITIONALTEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String AdditionalText
		{
			get { return IsAdditionalTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AdditionalTextColumn]; }
			set { this[_parent.AdditionalTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AdditionalText is NULL, false otherwise.</summary>
		public bool IsAdditionalTextNull() 
		{
			return IsNull(_parent.AdditionalTextColumn);
		}

		/// <summary>Sets the TypedView field AdditionalText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAdditionalTextNull() 
		{
			this[_parent.AdditionalTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonFirstName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONFIRSTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PersonFirstName
		{
			get { return IsPersonFirstNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonFirstNameColumn]; }
			set { this[_parent.PersonFirstNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonFirstName is NULL, false otherwise.</summary>
		public bool IsPersonFirstNameNull() 
		{
			return IsNull(_parent.PersonFirstNameColumn);
		}

		/// <summary>Sets the TypedView field PersonFirstName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonFirstNameNull() 
		{
			this[_parent.PersonFirstNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonLastName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONLASTNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PersonLastName
		{
			get { return IsPersonLastNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonLastNameColumn]; }
			set { this[_parent.PersonLastNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonLastName is NULL, false otherwise.</summary>
		public bool IsPersonLastNameNull() 
		{
			return IsNull(_parent.PersonLastNameColumn);
		}

		/// <summary>Sets the TypedView field PersonLastName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonLastNameNull() 
		{
			this[_parent.PersonLastNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonDateOfBirth</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONDATEOFBIRTH"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime PersonDateOfBirth
		{
			get { return IsPersonDateOfBirthNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.PersonDateOfBirthColumn]; }
			set { this[_parent.PersonDateOfBirthColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonDateOfBirth is NULL, false otherwise.</summary>
		public bool IsPersonDateOfBirthNull() 
		{
			return IsNull(_parent.PersonDateOfBirthColumn);
		}

		/// <summary>Sets the TypedView field PersonDateOfBirth to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonDateOfBirthNull() 
		{
			this[_parent.PersonDateOfBirthColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonGender</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONGENDER"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PersonGender
		{
			get { return IsPersonGenderNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PersonGenderColumn]; }
			set { this[_parent.PersonGenderColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonGender is NULL, false otherwise.</summary>
		public bool IsPersonGenderNull() 
		{
			return IsNull(_parent.PersonGenderColumn);
		}

		/// <summary>Sets the TypedView field PersonGender to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonGenderNull() 
		{
			this[_parent.PersonGenderColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonSalutation</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONSALUTATION"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 9, 0, 0</remarks>
		public System.Int32 PersonSalutation
		{
			get { return IsPersonSalutationNull() ? (System.Int32)TypeDefaultValue.GetDefaultValue(typeof(System.Int32)) : (System.Int32)this[_parent.PersonSalutationColumn]; }
			set { this[_parent.PersonSalutationColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonSalutation is NULL, false otherwise.</summary>
		public bool IsPersonSalutationNull() 
		{
			return IsNull(_parent.PersonSalutationColumn);
		}

		/// <summary>Sets the TypedView field PersonSalutation to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonSalutationNull() 
		{
			this[_parent.PersonSalutationColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PersonTitle</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PERSONTITLE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String PersonTitle
		{
			get { return IsPersonTitleNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PersonTitleColumn]; }
			set { this[_parent.PersonTitleColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PersonTitle is NULL, false otherwise.</summary>
		public bool IsPersonTitleNull() 
		{
			return IsNull(_parent.PersonTitleColumn);
		}

		/// <summary>Sets the TypedView field PersonTitle to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPersonTitleNull() 
		{
			this[_parent.PersonTitleColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 18, 0, 0</remarks>
		public System.Int64 AddressID
		{
			get { return IsAddressIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.AddressIDColumn]; }
			set { this[_parent.AddressIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressID is NULL, false otherwise.</summary>
		public bool IsAddressIDNull() 
		{
			return IsNull(_parent.AddressIDColumn);
		}

		/// <summary>Sets the TypedView field AddressID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressIDNull() 
		{
			this[_parent.AddressIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressStreet</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSSTREET"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressStreet
		{
			get { return IsAddressStreetNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressStreetColumn]; }
			set { this[_parent.AddressStreetColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressStreet is NULL, false otherwise.</summary>
		public bool IsAddressStreetNull() 
		{
			return IsNull(_parent.AddressStreetColumn);
		}

		/// <summary>Sets the TypedView field AddressStreet to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressStreetNull() 
		{
			this[_parent.AddressStreetColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressPostalCode</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSPOSTALCODE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String AddressPostalCode
		{
			get { return IsAddressPostalCodeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressPostalCodeColumn]; }
			set { this[_parent.AddressPostalCodeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressPostalCode is NULL, false otherwise.</summary>
		public bool IsAddressPostalCodeNull() 
		{
			return IsNull(_parent.AddressPostalCodeColumn);
		}

		/// <summary>Sets the TypedView field AddressPostalCode to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressPostalCodeNull() 
		{
			this[_parent.AddressPostalCodeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressCity</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSCITY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressCity
		{
			get { return IsAddressCityNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressCityColumn]; }
			set { this[_parent.AddressCityColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressCity is NULL, false otherwise.</summary>
		public bool IsAddressCityNull() 
		{
			return IsNull(_parent.AddressCityColumn);
		}

		/// <summary>Sets the TypedView field AddressCity to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressCityNull() 
		{
			this[_parent.AddressCityColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressCountry</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSCOUNTRY"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressCountry
		{
			get { return IsAddressCountryNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressCountryColumn]; }
			set { this[_parent.AddressCountryColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressCountry is NULL, false otherwise.</summary>
		public bool IsAddressCountryNull() 
		{
			return IsNull(_parent.AddressCountryColumn);
		}

		/// <summary>Sets the TypedView field AddressCountry to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressCountryNull() 
		{
			this[_parent.AddressCountryColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressRegion</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSREGION"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressRegion
		{
			get { return IsAddressRegionNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressRegionColumn]; }
			set { this[_parent.AddressRegionColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressRegion is NULL, false otherwise.</summary>
		public bool IsAddressRegionNull() 
		{
			return IsNull(_parent.AddressRegionColumn);
		}

		/// <summary>Sets the TypedView field AddressRegion to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressRegionNull() 
		{
			this[_parent.AddressRegionColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressAddressee</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSEE"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressAddressee
		{
			get { return IsAddressAddresseeNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressAddresseeColumn]; }
			set { this[_parent.AddressAddresseeColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressAddressee is NULL, false otherwise.</summary>
		public bool IsAddressAddresseeNull() 
		{
			return IsNull(_parent.AddressAddresseeColumn);
		}

		/// <summary>Sets the TypedView field AddressAddressee to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressAddresseeNull() 
		{
			this[_parent.AddressAddresseeColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressAddressField1</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSFIELD1"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressAddressField1
		{
			get { return IsAddressAddressField1Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressAddressField1Column]; }
			set { this[_parent.AddressAddressField1Column] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressAddressField1 is NULL, false otherwise.</summary>
		public bool IsAddressAddressField1Null() 
		{
			return IsNull(_parent.AddressAddressField1Column);
		}

		/// <summary>Sets the TypedView field AddressAddressField1 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressAddressField1Null() 
		{
			this[_parent.AddressAddressField1Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressAddressField2</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSADDRESSFIELD2"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 125</remarks>
		public System.String AddressAddressField2
		{
			get { return IsAddressAddressField2Null() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressAddressField2Column]; }
			set { this[_parent.AddressAddressField2Column] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressAddressField2 is NULL, false otherwise.</summary>
		public bool IsAddressAddressField2Null() 
		{
			return IsNull(_parent.AddressAddressField2Column);
		}

		/// <summary>Sets the TypedView field AddressAddressField2 to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressAddressField2Null() 
		{
			this[_parent.AddressAddressField2Column] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field AddressStreetNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ADDRESSSTREETNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String AddressStreetNumber
		{
			get { return IsAddressStreetNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.AddressStreetNumberColumn]; }
			set { this[_parent.AddressStreetNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field AddressStreetNumber is NULL, false otherwise.</summary>
		public bool IsAddressStreetNumberNull() 
		{
			return IsNull(_parent.AddressStreetNumberColumn);
		}

		/// <summary>Sets the TypedView field AddressStreetNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetAddressStreetNumberNull() 
		{
			this[_parent.AddressStreetNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONNUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 25</remarks>
		public System.String OrganizationNumber
		{
			get { return IsOrganizationNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationNumberColumn]; }
			set { this[_parent.OrganizationNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationNumber is NULL, false otherwise.</summary>
		public bool IsOrganizationNumberNull() 
		{
			return IsNull(_parent.OrganizationNumberColumn);
		}

		/// <summary>Sets the TypedView field OrganizationNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationNumberNull() 
		{
			this[_parent.OrganizationNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field OrganizationName</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."ORGANIZATIONNAME"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String OrganizationName
		{
			get { return IsOrganizationNameNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.OrganizationNameColumn]; }
			set { this[_parent.OrganizationNameColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field OrganizationName is NULL, false otherwise.</summary>
		public bool IsOrganizationNameNull() 
		{
			return IsNull(_parent.OrganizationNameColumn);
		}

		/// <summary>Sets the TypedView field OrganizationName to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetOrganizationNameNull() 
		{
			this[_parent.OrganizationNameColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankConnectionDataIban</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATAIBAN"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 34</remarks>
		public System.String BankConnectionDataIban
		{
			get { return IsBankConnectionDataIbanNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.BankConnectionDataIbanColumn]; }
			set { this[_parent.BankConnectionDataIbanColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankConnectionDataIban is NULL, false otherwise.</summary>
		public bool IsBankConnectionDataIbanNull() 
		{
			return IsNull(_parent.BankConnectionDataIbanColumn);
		}

		/// <summary>Sets the TypedView field BankConnectionDataIban to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankConnectionDataIbanNull() 
		{
			this[_parent.BankConnectionDataIbanColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankConnectionDataBic</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATABIC"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 11</remarks>
		public System.String BankConnectionDataBic
		{
			get { return IsBankConnectionDataBicNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.BankConnectionDataBicColumn]; }
			set { this[_parent.BankConnectionDataBicColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankConnectionDataBic is NULL, false otherwise.</summary>
		public bool IsBankConnectionDataBicNull() 
		{
			return IsNull(_parent.BankConnectionDataBicColumn);
		}

		/// <summary>Sets the TypedView field BankConnectionDataBic to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankConnectionDataBicNull() 
		{
			this[_parent.BankConnectionDataBicColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field BankConnectionDataLastUsage</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."BANKCONNECTIONDATALASTUSAGE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime BankConnectionDataLastUsage
		{
			get { return IsBankConnectionDataLastUsageNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.BankConnectionDataLastUsageColumn]; }
			set { this[_parent.BankConnectionDataLastUsageColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field BankConnectionDataLastUsage is NULL, false otherwise.</summary>
		public bool IsBankConnectionDataLastUsageNull() 
		{
			return IsNull(_parent.BankConnectionDataLastUsageColumn);
		}

		/// <summary>Sets the TypedView field BankConnectionDataLastUsage to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetBankConnectionDataLastUsageNull() 
		{
			this[_parent.BankConnectionDataLastUsageColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastInvoiceNumber</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."LASTINVOICENUMBER"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 50</remarks>
		public System.String LastInvoiceNumber
		{
			get { return IsLastInvoiceNumberNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.LastInvoiceNumberColumn]; }
			set { this[_parent.LastInvoiceNumberColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastInvoiceNumber is NULL, false otherwise.</summary>
		public bool IsLastInvoiceNumberNull() 
		{
			return IsNull(_parent.LastInvoiceNumberColumn);
		}

		/// <summary>Sets the TypedView field LastInvoiceNumber to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastInvoiceNumberNull() 
		{
			this[_parent.LastInvoiceNumberColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field LastInvoiceCreationDate</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."LASTINVOICECREATIONDATE"<br/>
		/// View field characteristics (type, precision, scale, length): Date, 0, 0, 0</remarks>
		public System.DateTime LastInvoiceCreationDate
		{
			get { return IsLastInvoiceCreationDateNull() ? (System.DateTime)TypeDefaultValue.GetDefaultValue(typeof(System.DateTime)) : (System.DateTime)this[_parent.LastInvoiceCreationDateColumn]; }
			set { this[_parent.LastInvoiceCreationDateColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field LastInvoiceCreationDate is NULL, false otherwise.</summary>
		public bool IsLastInvoiceCreationDateNull() 
		{
			return IsNull(_parent.LastInvoiceCreationDateColumn);
		}

		/// <summary>Sets the TypedView field LastInvoiceCreationDate to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetLastInvoiceCreationDateNull() 
		{
			this[_parent.LastInvoiceCreationDateColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field CommentText</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."COMMENTTEXT"<br/>
		/// View field characteristics (type, precision, scale, length): NVarchar2, 0, 0, 2000</remarks>
		public System.String CommentText
		{
			get { return IsCommentTextNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.CommentTextColumn]; }
			set { this[_parent.CommentTextColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field CommentText is NULL, false otherwise.</summary>
		public bool IsCommentTextNull() 
		{
			return IsNull(_parent.CommentTextColumn);
		}

		/// <summary>Sets the TypedView field CommentText to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetCommentTextNull() 
		{
			this[_parent.CommentTextColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentModalityPaymentMethodID</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYPAYMENTMETHODID"<br/>
		/// View field characteristics (type, precision, scale, length): Decimal, 10, 0, 0</remarks>
		public System.Int64 PaymentModalityPaymentMethodID
		{
			get { return IsPaymentModalityPaymentMethodIDNull() ? (System.Int64)TypeDefaultValue.GetDefaultValue(typeof(System.Int64)) : (System.Int64)this[_parent.PaymentModalityPaymentMethodIDColumn]; }
			set { this[_parent.PaymentModalityPaymentMethodIDColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentModalityPaymentMethodID is NULL, false otherwise.</summary>
		public bool IsPaymentModalityPaymentMethodIDNull() 
		{
			return IsNull(_parent.PaymentModalityPaymentMethodIDColumn);
		}

		/// <summary>Sets the TypedView field PaymentModalityPaymentMethodID to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentModalityPaymentMethodIDNull() 
		{
			this[_parent.PaymentModalityPaymentMethodIDColumn] = System.Convert.DBNull;
		}
		
		/// <summary>Gets / sets the value of the TypedView field PaymentModalityPaymentMethod</summary>
		/// <remarks>Mapped on view field: "VIEW_SL_CONTRACTDETAILS"."PAYMENTMODALITYPAYMENTMETHOD"<br/>
		/// View field characteristics (type, precision, scale, length): Varchar2, 0, 0, 40</remarks>
		public System.String PaymentModalityPaymentMethod
		{
			get { return IsPaymentModalityPaymentMethodNull() ? (System.String)TypeDefaultValue.GetDefaultValue(typeof(System.String)) : (System.String)this[_parent.PaymentModalityPaymentMethodColumn]; }
			set { this[_parent.PaymentModalityPaymentMethodColumn] = value; }
		}

		/// <summary>Returns true if the TypedView field PaymentModalityPaymentMethod is NULL, false otherwise.</summary>
		public bool IsPaymentModalityPaymentMethodNull() 
		{
			return IsNull(_parent.PaymentModalityPaymentMethodColumn);
		}

		/// <summary>Sets the TypedView field PaymentModalityPaymentMethod to NULL. Not recommended; a typed list should be used as a readonly object.</summary>
    	public void SetPaymentModalityPaymentMethodNull() 
		{
			this[_parent.PaymentModalityPaymentMethodColumn] = System.Convert.DBNull;
		}
		
		#endregion
		
		#region Custom Typed View Row Code
	// __LLBLGENPRO_USER_CODE_REGION_START CustomTypedViewRowCode 
	// __LLBLGENPRO_USER_CODE_REGION_END 
		#endregion
		
		#region Included Row Code

		#endregion		
	}
}
