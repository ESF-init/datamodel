﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using VarioSL.Entities;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.RelationClasses;

namespace VarioSL.Entities.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData : ILinqMetaData
	{
		#region Class Member Declarations
		private ITransaction _transactionToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the transaction object to use empty. This is ok if you're not executing queries created with this
		/// meta data inside a transaction. If you're executing the queries created with this meta-data inside a transaction, either set the Transaction property
		/// on the IQueryable.Provider instance of the created LLBLGenProQuery object prior to execution or use the ctor which accepts a transaction object.</summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse) : this(transactionToUse, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse, FunctionMappingStore customFunctionMappings)
		{
			_transactionToUse = transactionToUse;
			_customFunctionMappings = customFunctionMappings;
		}
		
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((VarioSL.Entities.EntityType)typeOfEntity)
			{
				case VarioSL.Entities.EntityType.AbortedTransactionEntity:
					toReturn = this.AbortedTransaction;
					break;
				case VarioSL.Entities.EntityType.AccountBalanceEntity:
					toReturn = this.AccountBalance;
					break;
				case VarioSL.Entities.EntityType.AccountEntryEntity:
					toReturn = this.AccountEntry;
					break;
				case VarioSL.Entities.EntityType.AccountEntryNumberEntity:
					toReturn = this.AccountEntryNumber;
					break;
				case VarioSL.Entities.EntityType.AccountPostingKeyEntity:
					toReturn = this.AccountPostingKey;
					break;
				case VarioSL.Entities.EntityType.ApplicationEntity:
					toReturn = this.Application;
					break;
				case VarioSL.Entities.EntityType.ApplicationVersionEntity:
					toReturn = this.ApplicationVersion;
					break;
				case VarioSL.Entities.EntityType.ApportionmentEntity:
					toReturn = this.Apportionment;
					break;
				case VarioSL.Entities.EntityType.ApportionmentResultEntity:
					toReturn = this.ApportionmentResult;
					break;
				case VarioSL.Entities.EntityType.AreaListEntity:
					toReturn = this.AreaList;
					break;
				case VarioSL.Entities.EntityType.AreaListElementEntity:
					toReturn = this.AreaListElement;
					break;
				case VarioSL.Entities.EntityType.AreaListElementGroupEntity:
					toReturn = this.AreaListElementGroup;
					break;
				case VarioSL.Entities.EntityType.AreaTypeEntity:
					toReturn = this.AreaType;
					break;
				case VarioSL.Entities.EntityType.AttributeEntity:
					toReturn = this.Attribute;
					break;
				case VarioSL.Entities.EntityType.AttributeBitmapLayoutObjectEntity:
					toReturn = this.AttributeBitmapLayoutObject;
					break;
				case VarioSL.Entities.EntityType.AttributeTextLayoutObjectEntity:
					toReturn = this.AttributeTextLayoutObject;
					break;
				case VarioSL.Entities.EntityType.AttributeValueEntity:
					toReturn = this.AttributeValue;
					break;
				case VarioSL.Entities.EntityType.AttributeValueListEntity:
					toReturn = this.AttributeValueList;
					break;
				case VarioSL.Entities.EntityType.AutomatEntity:
					toReturn = this.Automat;
					break;
				case VarioSL.Entities.EntityType.BankEntity:
					toReturn = this.Bank;
					break;
				case VarioSL.Entities.EntityType.BinaryDataEntity:
					toReturn = this.BinaryData;
					break;
				case VarioSL.Entities.EntityType.BlockingReasonEntity:
					toReturn = this.BlockingReason;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleEntity:
					toReturn = this.BusinessRule;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleClauseEntity:
					toReturn = this.BusinessRuleClause;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleConditionEntity:
					toReturn = this.BusinessRuleCondition;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleResultEntity:
					toReturn = this.BusinessRuleResult;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleTypeEntity:
					toReturn = this.BusinessRuleType;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleTypeToVariableEntity:
					toReturn = this.BusinessRuleTypeToVariable;
					break;
				case VarioSL.Entities.EntityType.BusinessRuleVariableEntity:
					toReturn = this.BusinessRuleVariable;
					break;
				case VarioSL.Entities.EntityType.CalendarEntity:
					toReturn = this.Calendar;
					break;
				case VarioSL.Entities.EntityType.CalendarEntryEntity:
					toReturn = this.CalendarEntry;
					break;
				case VarioSL.Entities.EntityType.CardActionAttributeEntity:
					toReturn = this.CardActionAttribute;
					break;
				case VarioSL.Entities.EntityType.CardActionRequestEntity:
					toReturn = this.CardActionRequest;
					break;
				case VarioSL.Entities.EntityType.CardActionRequestTypeEntity:
					toReturn = this.CardActionRequestType;
					break;
				case VarioSL.Entities.EntityType.CardChipTypeEntity:
					toReturn = this.CardChipType;
					break;
				case VarioSL.Entities.EntityType.CardStateEntity:
					toReturn = this.CardState;
					break;
				case VarioSL.Entities.EntityType.CardTicketEntity:
					toReturn = this.CardTicket;
					break;
				case VarioSL.Entities.EntityType.CardTicketToTicketEntity:
					toReturn = this.CardTicketToTicket;
					break;
				case VarioSL.Entities.EntityType.CashServiceEntity:
					toReturn = this.CashService;
					break;
				case VarioSL.Entities.EntityType.CashServiceBalanceEntity:
					toReturn = this.CashServiceBalance;
					break;
				case VarioSL.Entities.EntityType.CashServiceDataEntity:
					toReturn = this.CashServiceData;
					break;
				case VarioSL.Entities.EntityType.CashUnitEntity:
					toReturn = this.CashUnit;
					break;
				case VarioSL.Entities.EntityType.ChoiceEntity:
					toReturn = this.Choice;
					break;
				case VarioSL.Entities.EntityType.ClearingEntity:
					toReturn = this.Clearing;
					break;
				case VarioSL.Entities.EntityType.ClearingClassificationEntity:
					toReturn = this.ClearingClassification;
					break;
				case VarioSL.Entities.EntityType.ClearingDetailEntity:
					toReturn = this.ClearingDetail;
					break;
				case VarioSL.Entities.EntityType.ClearingResultEntity:
					toReturn = this.ClearingResult;
					break;
				case VarioSL.Entities.EntityType.ClearingResultLevelEntity:
					toReturn = this.ClearingResultLevel;
					break;
				case VarioSL.Entities.EntityType.ClearingStateEntity:
					toReturn = this.ClearingState;
					break;
				case VarioSL.Entities.EntityType.ClearingSumEntity:
					toReturn = this.ClearingSum;
					break;
				case VarioSL.Entities.EntityType.ClearingTransactionEntity:
					toReturn = this.ClearingTransaction;
					break;
				case VarioSL.Entities.EntityType.ClientEntity:
					toReturn = this.Client;
					break;
				case VarioSL.Entities.EntityType.ClientAdaptedLayoutObjectEntity:
					toReturn = this.ClientAdaptedLayoutObject;
					break;
				case VarioSL.Entities.EntityType.ComponentEntity:
					toReturn = this.Component;
					break;
				case VarioSL.Entities.EntityType.ComponentClearingEntity:
					toReturn = this.ComponentClearing;
					break;
				case VarioSL.Entities.EntityType.ComponentFillingEntity:
					toReturn = this.ComponentFilling;
					break;
				case VarioSL.Entities.EntityType.ComponentStateEntity:
					toReturn = this.ComponentState;
					break;
				case VarioSL.Entities.EntityType.ComponentTypeEntity:
					toReturn = this.ComponentType;
					break;
				case VarioSL.Entities.EntityType.ConditionalSubPageLayoutObjectEntity:
					toReturn = this.ConditionalSubPageLayoutObject;
					break;
				case VarioSL.Entities.EntityType.CreditScreeningEntity:
					toReturn = this.CreditScreening;
					break;
				case VarioSL.Entities.EntityType.DayTypeEntity:
					toReturn = this.DayType;
					break;
				case VarioSL.Entities.EntityType.DebtorEntity:
					toReturn = this.Debtor;
					break;
				case VarioSL.Entities.EntityType.DebtorCardEntity:
					toReturn = this.DebtorCard;
					break;
				case VarioSL.Entities.EntityType.DebtorCardHistoryEntity:
					toReturn = this.DebtorCardHistory;
					break;
				case VarioSL.Entities.EntityType.DefaultPinEntity:
					toReturn = this.DefaultPin;
					break;
				case VarioSL.Entities.EntityType.DepotEntity:
					toReturn = this.Depot;
					break;
				case VarioSL.Entities.EntityType.DeviceEntity:
					toReturn = this.Device;
					break;
				case VarioSL.Entities.EntityType.DeviceBookingStateEntity:
					toReturn = this.DeviceBookingState;
					break;
				case VarioSL.Entities.EntityType.DeviceClassEntity:
					toReturn = this.DeviceClass;
					break;
				case VarioSL.Entities.EntityType.DevicePaymentMethodEntity:
					toReturn = this.DevicePaymentMethod;
					break;
				case VarioSL.Entities.EntityType.DirectionEntity:
					toReturn = this.Direction;
					break;
				case VarioSL.Entities.EntityType.DunningLevelEntity:
					toReturn = this.DunningLevel;
					break;
				case VarioSL.Entities.EntityType.EticketEntity:
					toReturn = this.Eticket;
					break;
				case VarioSL.Entities.EntityType.ExportInfoEntity:
					toReturn = this.ExportInfo;
					break;
				case VarioSL.Entities.EntityType.ExternalCardEntity:
					toReturn = this.ExternalCard;
					break;
				case VarioSL.Entities.EntityType.ExternalDataEntity:
					toReturn = this.ExternalData;
					break;
				case VarioSL.Entities.EntityType.ExternalEffortEntity:
					toReturn = this.ExternalEffort;
					break;
				case VarioSL.Entities.EntityType.ExternalPacketEntity:
					toReturn = this.ExternalPacket;
					break;
				case VarioSL.Entities.EntityType.ExternalPacketEffortEntity:
					toReturn = this.ExternalPacketEffort;
					break;
				case VarioSL.Entities.EntityType.ExternalTypeEntity:
					toReturn = this.ExternalType;
					break;
				case VarioSL.Entities.EntityType.FareEvasionCategoryEntity:
					toReturn = this.FareEvasionCategory;
					break;
				case VarioSL.Entities.EntityType.FareEvasionReasonEntity:
					toReturn = this.FareEvasionReason;
					break;
				case VarioSL.Entities.EntityType.FareEvasionReasonToCategoryEntity:
					toReturn = this.FareEvasionReasonToCategory;
					break;
				case VarioSL.Entities.EntityType.FareMatrixEntity:
					toReturn = this.FareMatrix;
					break;
				case VarioSL.Entities.EntityType.FareMatrixEntryEntity:
					toReturn = this.FareMatrixEntry;
					break;
				case VarioSL.Entities.EntityType.FareStageEntity:
					toReturn = this.FareStage;
					break;
				case VarioSL.Entities.EntityType.FareStageAliasEntity:
					toReturn = this.FareStageAlias;
					break;
				case VarioSL.Entities.EntityType.FareStageHierarchieLevelEntity:
					toReturn = this.FareStageHierarchieLevel;
					break;
				case VarioSL.Entities.EntityType.FareStageListEntity:
					toReturn = this.FareStageList;
					break;
				case VarioSL.Entities.EntityType.FareStageTypeEntity:
					toReturn = this.FareStageType;
					break;
				case VarioSL.Entities.EntityType.FareTableEntity:
					toReturn = this.FareTable;
					break;
				case VarioSL.Entities.EntityType.FareTableEntryEntity:
					toReturn = this.FareTableEntry;
					break;
				case VarioSL.Entities.EntityType.FixedBitmapLayoutObjectEntity:
					toReturn = this.FixedBitmapLayoutObject;
					break;
				case VarioSL.Entities.EntityType.FixedTextLayoutObjectEntity:
					toReturn = this.FixedTextLayoutObject;
					break;
				case VarioSL.Entities.EntityType.GuiDefEntity:
					toReturn = this.GuiDef;
					break;
				case VarioSL.Entities.EntityType.KeyAttributeTransfromEntity:
					toReturn = this.KeyAttributeTransfrom;
					break;
				case VarioSL.Entities.EntityType.KeySelectionModeEntity:
					toReturn = this.KeySelectionMode;
					break;
				case VarioSL.Entities.EntityType.KVVSubscriptionEntity:
					toReturn = this.KVVSubscription;
					break;
				case VarioSL.Entities.EntityType.LanguageEntity:
					toReturn = this.Language;
					break;
				case VarioSL.Entities.EntityType.LayoutEntity:
					toReturn = this.Layout;
					break;
				case VarioSL.Entities.EntityType.LineEntity:
					toReturn = this.Line;
					break;
				case VarioSL.Entities.EntityType.LineGroupEntity:
					toReturn = this.LineGroup;
					break;
				case VarioSL.Entities.EntityType.LineGroupToLineGroupEntity:
					toReturn = this.LineGroupToLineGroup;
					break;
				case VarioSL.Entities.EntityType.LineToLineGroupEntity:
					toReturn = this.LineToLineGroup;
					break;
				case VarioSL.Entities.EntityType.ListLayoutObjectEntity:
					toReturn = this.ListLayoutObject;
					break;
				case VarioSL.Entities.EntityType.ListLayoutTypeEntity:
					toReturn = this.ListLayoutType;
					break;
				case VarioSL.Entities.EntityType.LogoEntity:
					toReturn = this.Logo;
					break;
				case VarioSL.Entities.EntityType.NetEntity:
					toReturn = this.Net;
					break;
				case VarioSL.Entities.EntityType.NumberRangeEntity:
					toReturn = this.NumberRange;
					break;
				case VarioSL.Entities.EntityType.OperatorEntity:
					toReturn = this.Operator;
					break;
				case VarioSL.Entities.EntityType.OutputDeviceEntity:
					toReturn = this.OutputDevice;
					break;
				case VarioSL.Entities.EntityType.PageContentGroupEntity:
					toReturn = this.PageContentGroup;
					break;
				case VarioSL.Entities.EntityType.PageContentGroupToContentEntity:
					toReturn = this.PageContentGroupToContent;
					break;
				case VarioSL.Entities.EntityType.PanelEntity:
					toReturn = this.Panel;
					break;
				case VarioSL.Entities.EntityType.ParameterAttributeValueEntity:
					toReturn = this.ParameterAttributeValue;
					break;
				case VarioSL.Entities.EntityType.ParameterFareStageEntity:
					toReturn = this.ParameterFareStage;
					break;
				case VarioSL.Entities.EntityType.ParameterTariffEntity:
					toReturn = this.ParameterTariff;
					break;
				case VarioSL.Entities.EntityType.ParameterTicketEntity:
					toReturn = this.ParameterTicket;
					break;
				case VarioSL.Entities.EntityType.PaymentDetailEntity:
					toReturn = this.PaymentDetail;
					break;
				case VarioSL.Entities.EntityType.PaymentIntervalEntity:
					toReturn = this.PaymentInterval;
					break;
				case VarioSL.Entities.EntityType.PointOfSaleEntity:
					toReturn = this.PointOfSale;
					break;
				case VarioSL.Entities.EntityType.PredefinedKeyEntity:
					toReturn = this.PredefinedKey;
					break;
				case VarioSL.Entities.EntityType.PriceTypeEntity:
					toReturn = this.PriceType;
					break;
				case VarioSL.Entities.EntityType.PrimalKeyEntity:
					toReturn = this.PrimalKey;
					break;
				case VarioSL.Entities.EntityType.PrintTextEntity:
					toReturn = this.PrintText;
					break;
				case VarioSL.Entities.EntityType.ProductOnCardEntity:
					toReturn = this.ProductOnCard;
					break;
				case VarioSL.Entities.EntityType.ProtocolEntity:
					toReturn = this.Protocol;
					break;
				case VarioSL.Entities.EntityType.ProtocolActionEntity:
					toReturn = this.ProtocolAction;
					break;
				case VarioSL.Entities.EntityType.ProtocolFunctionEntity:
					toReturn = this.ProtocolFunction;
					break;
				case VarioSL.Entities.EntityType.ProtocolFunctionGroupEntity:
					toReturn = this.ProtocolFunctionGroup;
					break;
				case VarioSL.Entities.EntityType.ProtocolMessageEntity:
					toReturn = this.ProtocolMessage;
					break;
				case VarioSL.Entities.EntityType.QualificationEntity:
					toReturn = this.Qualification;
					break;
				case VarioSL.Entities.EntityType.ResponsibilityEntity:
					toReturn = this.Responsibility;
					break;
				case VarioSL.Entities.EntityType.RevenueTransactionTypeEntity:
					toReturn = this.RevenueTransactionType;
					break;
				case VarioSL.Entities.EntityType.RmPaymentEntity:
					toReturn = this.RmPayment;
					break;
				case VarioSL.Entities.EntityType.RouteEntity:
					toReturn = this.Route;
					break;
				case VarioSL.Entities.EntityType.RouteNameEntity:
					toReturn = this.RouteName;
					break;
				case VarioSL.Entities.EntityType.RuleCappingEntity:
					toReturn = this.RuleCapping;
					break;
				case VarioSL.Entities.EntityType.RuleCappingToFtEntryEntity:
					toReturn = this.RuleCappingToFtEntry;
					break;
				case VarioSL.Entities.EntityType.RuleCappingToTicketEntity:
					toReturn = this.RuleCappingToTicket;
					break;
				case VarioSL.Entities.EntityType.RulePeriodEntity:
					toReturn = this.RulePeriod;
					break;
				case VarioSL.Entities.EntityType.RuleTypeEntity:
					toReturn = this.RuleType;
					break;
				case VarioSL.Entities.EntityType.SalutationEntity:
					toReturn = this.Salutation;
					break;
				case VarioSL.Entities.EntityType.ServiceAllocationEntity:
					toReturn = this.ServiceAllocation;
					break;
				case VarioSL.Entities.EntityType.ServiceIdToCardEntity:
					toReturn = this.ServiceIdToCard;
					break;
				case VarioSL.Entities.EntityType.ServicePercentageEntity:
					toReturn = this.ServicePercentage;
					break;
				case VarioSL.Entities.EntityType.ShiftEntity:
					toReturn = this.Shift;
					break;
				case VarioSL.Entities.EntityType.ShiftInventoryEntity:
					toReturn = this.ShiftInventory;
					break;
				case VarioSL.Entities.EntityType.ShiftStateEntity:
					toReturn = this.ShiftState;
					break;
				case VarioSL.Entities.EntityType.ShortDistanceEntity:
					toReturn = this.ShortDistance;
					break;
				case VarioSL.Entities.EntityType.SpecialReceiptEntity:
					toReturn = this.SpecialReceipt;
					break;
				case VarioSL.Entities.EntityType.StopEntity:
					toReturn = this.Stop;
					break;
				case VarioSL.Entities.EntityType.SystemFieldEntity:
					toReturn = this.SystemField;
					break;
				case VarioSL.Entities.EntityType.SystemFieldBarcodeLayoutObjectEntity:
					toReturn = this.SystemFieldBarcodeLayoutObject;
					break;
				case VarioSL.Entities.EntityType.SystemFieldDynamicGraphicLayoutObjectEntity:
					toReturn = this.SystemFieldDynamicGraphicLayoutObject;
					break;
				case VarioSL.Entities.EntityType.SystemFieldTextLayoutObjectEntity:
					toReturn = this.SystemFieldTextLayoutObject;
					break;
				case VarioSL.Entities.EntityType.SystemTextEntity:
					toReturn = this.SystemText;
					break;
				case VarioSL.Entities.EntityType.TariffEntity:
					toReturn = this.Tariff;
					break;
				case VarioSL.Entities.EntityType.TariffParameterEntity:
					toReturn = this.TariffParameter;
					break;
				case VarioSL.Entities.EntityType.TariffReleaseEntity:
					toReturn = this.TariffRelease;
					break;
				case VarioSL.Entities.EntityType.TemporalTypeEntity:
					toReturn = this.TemporalType;
					break;
				case VarioSL.Entities.EntityType.TicketEntity:
					toReturn = this.Ticket;
					break;
				case VarioSL.Entities.EntityType.TicketCancellationTypeEntity:
					toReturn = this.TicketCancellationType;
					break;
				case VarioSL.Entities.EntityType.TicketCategoryEntity:
					toReturn = this.TicketCategory;
					break;
				case VarioSL.Entities.EntityType.TicketDayTypeEntity:
					toReturn = this.TicketDayType;
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassEntity:
					toReturn = this.TicketDeviceClass;
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassOutputDeviceEntity:
					toReturn = this.TicketDeviceClassOutputDevice;
					break;
				case VarioSL.Entities.EntityType.TicketDeviceClassPaymentMethodEntity:
					toReturn = this.TicketDeviceClassPaymentMethod;
					break;
				case VarioSL.Entities.EntityType.TicketDevicePaymentMethodEntity:
					toReturn = this.TicketDevicePaymentMethod;
					break;
				case VarioSL.Entities.EntityType.TicketGroupEntity:
					toReturn = this.TicketGroup;
					break;
				case VarioSL.Entities.EntityType.TicketOrganizationEntity:
					toReturn = this.TicketOrganization;
					break;
				case VarioSL.Entities.EntityType.TicketOutputdeviceEntity:
					toReturn = this.TicketOutputdevice;
					break;
				case VarioSL.Entities.EntityType.TicketPaymentIntervalEntity:
					toReturn = this.TicketPaymentInterval;
					break;
				case VarioSL.Entities.EntityType.TicketPhysicalCardTypeEntity:
					toReturn = this.TicketPhysicalCardType;
					break;
				case VarioSL.Entities.EntityType.TicketSelectionModeEntity:
					toReturn = this.TicketSelectionMode;
					break;
				case VarioSL.Entities.EntityType.TicketServicesPermittedEntity:
					toReturn = this.TicketServicesPermitted;
					break;
				case VarioSL.Entities.EntityType.TicketToGroupEntity:
					toReturn = this.TicketToGroup;
					break;
				case VarioSL.Entities.EntityType.TicketTypeEntity:
					toReturn = this.TicketType;
					break;
				case VarioSL.Entities.EntityType.TicketVendingClientEntity:
					toReturn = this.TicketVendingClient;
					break;
				case VarioSL.Entities.EntityType.TransactionEntity:
					toReturn = this.Transaction;
					break;
				case VarioSL.Entities.EntityType.TransactionAdditionalEntity:
					toReturn = this.TransactionAdditional;
					break;
				case VarioSL.Entities.EntityType.TransactionBackupEntity:
					toReturn = this.TransactionBackup;
					break;
				case VarioSL.Entities.EntityType.TransactionExtensionEntity:
					toReturn = this.TransactionExtension;
					break;
				case VarioSL.Entities.EntityType.TranslationEntity:
					toReturn = this.Translation;
					break;
				case VarioSL.Entities.EntityType.TypeOfCardEntity:
					toReturn = this.TypeOfCard;
					break;
				case VarioSL.Entities.EntityType.TypeOfUnitEntity:
					toReturn = this.TypeOfUnit;
					break;
				case VarioSL.Entities.EntityType.UnitEntity:
					toReturn = this.Unit;
					break;
				case VarioSL.Entities.EntityType.UserConfigEntity:
					toReturn = this.UserConfig;
					break;
				case VarioSL.Entities.EntityType.UserGroupEntity:
					toReturn = this.UserGroup;
					break;
				case VarioSL.Entities.EntityType.UserGroupRightEntity:
					toReturn = this.UserGroupRight;
					break;
				case VarioSL.Entities.EntityType.UserIsMemberEntity:
					toReturn = this.UserIsMember;
					break;
				case VarioSL.Entities.EntityType.UserKeyEntity:
					toReturn = this.UserKey;
					break;
				case VarioSL.Entities.EntityType.UserLanguageEntity:
					toReturn = this.UserLanguage;
					break;
				case VarioSL.Entities.EntityType.UserListEntity:
					toReturn = this.UserList;
					break;
				case VarioSL.Entities.EntityType.UserResourceEntity:
					toReturn = this.UserResource;
					break;
				case VarioSL.Entities.EntityType.UserStatusEntity:
					toReturn = this.UserStatus;
					break;
				case VarioSL.Entities.EntityType.UserToWorkstationEntity:
					toReturn = this.UserToWorkstation;
					break;
				case VarioSL.Entities.EntityType.ValidationTransactionEntity:
					toReturn = this.ValidationTransaction;
					break;
				case VarioSL.Entities.EntityType.VarioAddressEntity:
					toReturn = this.VarioAddress;
					break;
				case VarioSL.Entities.EntityType.VarioSettlementEntity:
					toReturn = this.VarioSettlement;
					break;
				case VarioSL.Entities.EntityType.VarioTypeOfSettlementEntity:
					toReturn = this.VarioTypeOfSettlement;
					break;
				case VarioSL.Entities.EntityType.VdvKeyInfoEntity:
					toReturn = this.VdvKeyInfo;
					break;
				case VarioSL.Entities.EntityType.VdvKeySetEntity:
					toReturn = this.VdvKeySet;
					break;
				case VarioSL.Entities.EntityType.VdvLayoutEntity:
					toReturn = this.VdvLayout;
					break;
				case VarioSL.Entities.EntityType.VdvLayoutObjectEntity:
					toReturn = this.VdvLayoutObject;
					break;
				case VarioSL.Entities.EntityType.VdvLoadKeyMessageEntity:
					toReturn = this.VdvLoadKeyMessage;
					break;
				case VarioSL.Entities.EntityType.VdvProductEntity:
					toReturn = this.VdvProduct;
					break;
				case VarioSL.Entities.EntityType.VdvSamStatusEntity:
					toReturn = this.VdvSamStatus;
					break;
				case VarioSL.Entities.EntityType.VdvTagEntity:
					toReturn = this.VdvTag;
					break;
				case VarioSL.Entities.EntityType.VdvTerminalEntity:
					toReturn = this.VdvTerminal;
					break;
				case VarioSL.Entities.EntityType.VdvTypeEntity:
					toReturn = this.VdvType;
					break;
				case VarioSL.Entities.EntityType.WorkstationEntity:
					toReturn = this.Workstation;
					break;
				case VarioSL.Entities.EntityType.AccountingCancelRecTapCmlSettledEntity:
					toReturn = this.AccountingCancelRecTapCmlSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingCancelRecTapTabSettledEntity:
					toReturn = this.AccountingCancelRecTapTabSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingMethodEntity:
					toReturn = this.AccountingMethod;
					break;
				case VarioSL.Entities.EntityType.AccountingModeEntity:
					toReturn = this.AccountingMode;
					break;
				case VarioSL.Entities.EntityType.AccountingRecLoadUseCmlSettledEntity:
					toReturn = this.AccountingRecLoadUseCmlSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingRecLoadUseTabSettledEntity:
					toReturn = this.AccountingRecLoadUseTabSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedLoadAndUseEntity:
					toReturn = this.AccountingRecognizedLoadAndUse;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedPaymentEntity:
					toReturn = this.AccountingRecognizedPayment;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapEntity:
					toReturn = this.AccountingRecognizedTap;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCancellationEntity:
					toReturn = this.AccountingRecognizedTapCancellation;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapCmlSettledEntity:
					toReturn = this.AccountingRecognizedTapCmlSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingRecognizedTapTabSettledEntity:
					toReturn = this.AccountingRecognizedTapTabSettled;
					break;
				case VarioSL.Entities.EntityType.AccountingReconciledPaymentEntity:
					toReturn = this.AccountingReconciledPayment;
					break;
				case VarioSL.Entities.EntityType.AccountMessageSettingEntity:
					toReturn = this.AccountMessageSetting;
					break;
				case VarioSL.Entities.EntityType.AdditionalDataEntity:
					toReturn = this.AdditionalData;
					break;
				case VarioSL.Entities.EntityType.AddressEntity:
					toReturn = this.Address;
					break;
				case VarioSL.Entities.EntityType.AddressBookEntryEntity:
					toReturn = this.AddressBookEntry;
					break;
				case VarioSL.Entities.EntityType.AddressBookEntryTypeEntity:
					toReturn = this.AddressBookEntryType;
					break;
				case VarioSL.Entities.EntityType.AddressTypeEntity:
					toReturn = this.AddressType;
					break;
				case VarioSL.Entities.EntityType.AggregationFunctionEntity:
					toReturn = this.AggregationFunction;
					break;
				case VarioSL.Entities.EntityType.AggregationTypeEntity:
					toReturn = this.AggregationType;
					break;
				case VarioSL.Entities.EntityType.ApportionEntity:
					toReturn = this.Apportion;
					break;
				case VarioSL.Entities.EntityType.ApportionPassRevenueRecognitionEntity:
					toReturn = this.ApportionPassRevenueRecognition;
					break;
				case VarioSL.Entities.EntityType.ArchiveStateEntity:
					toReturn = this.ArchiveState;
					break;
				case VarioSL.Entities.EntityType.AttributeToMobilityProviderEntity:
					toReturn = this.AttributeToMobilityProvider;
					break;
				case VarioSL.Entities.EntityType.AuditRegisterEntity:
					toReturn = this.AuditRegister;
					break;
				case VarioSL.Entities.EntityType.AuditRegisterValueEntity:
					toReturn = this.AuditRegisterValue;
					break;
				case VarioSL.Entities.EntityType.AuditRegisterValueTypeEntity:
					toReturn = this.AuditRegisterValueType;
					break;
				case VarioSL.Entities.EntityType.AutoloadSettingEntity:
					toReturn = this.AutoloadSetting;
					break;
				case VarioSL.Entities.EntityType.AutoloadToPaymentOptionEntity:
					toReturn = this.AutoloadToPaymentOption;
					break;
				case VarioSL.Entities.EntityType.BadCardEntity:
					toReturn = this.BadCard;
					break;
				case VarioSL.Entities.EntityType.BadCheckEntity:
					toReturn = this.BadCheck;
					break;
				case VarioSL.Entities.EntityType.BankConnectionDataEntity:
					toReturn = this.BankConnectionData;
					break;
				case VarioSL.Entities.EntityType.BankStatementEntity:
					toReturn = this.BankStatement;
					break;
				case VarioSL.Entities.EntityType.BankStatementRecordTypeEntity:
					toReturn = this.BankStatementRecordType;
					break;
				case VarioSL.Entities.EntityType.BankStatementStateEntity:
					toReturn = this.BankStatementState;
					break;
				case VarioSL.Entities.EntityType.BankStatementVerificationEntity:
					toReturn = this.BankStatementVerification;
					break;
				case VarioSL.Entities.EntityType.BookingEntity:
					toReturn = this.Booking;
					break;
				case VarioSL.Entities.EntityType.BookingItemEntity:
					toReturn = this.BookingItem;
					break;
				case VarioSL.Entities.EntityType.BookingItemPaymentEntity:
					toReturn = this.BookingItemPayment;
					break;
				case VarioSL.Entities.EntityType.BookingItemPaymentTypeEntity:
					toReturn = this.BookingItemPaymentType;
					break;
				case VarioSL.Entities.EntityType.BookingTypeEntity:
					toReturn = this.BookingType;
					break;
				case VarioSL.Entities.EntityType.CappingJournalEntity:
					toReturn = this.CappingJournal;
					break;
				case VarioSL.Entities.EntityType.CappingStateEntity:
					toReturn = this.CappingState;
					break;
				case VarioSL.Entities.EntityType.CardEntity:
					toReturn = this.Card;
					break;
				case VarioSL.Entities.EntityType.CardBodyTypeEntity:
					toReturn = this.CardBodyType;
					break;
				case VarioSL.Entities.EntityType.CardDormancyEntity:
					toReturn = this.CardDormancy;
					break;
				case VarioSL.Entities.EntityType.CardEventEntity:
					toReturn = this.CardEvent;
					break;
				case VarioSL.Entities.EntityType.CardEventTypeEntity:
					toReturn = this.CardEventType;
					break;
				case VarioSL.Entities.EntityType.CardFulfillmentStatusEntity:
					toReturn = this.CardFulfillmentStatus;
					break;
				case VarioSL.Entities.EntityType.CardHolderEntity:
					toReturn = this.CardHolder;
					break;
				case VarioSL.Entities.EntityType.CardHolderOrganizationEntity:
					toReturn = this.CardHolderOrganization;
					break;
				case VarioSL.Entities.EntityType.CardKeyEntity:
					toReturn = this.CardKey;
					break;
				case VarioSL.Entities.EntityType.CardLiabilityEntity:
					toReturn = this.CardLiability;
					break;
				case VarioSL.Entities.EntityType.CardLinkEntity:
					toReturn = this.CardLink;
					break;
				case VarioSL.Entities.EntityType.CardOrderDetailEntity:
					toReturn = this.CardOrderDetail;
					break;
				case VarioSL.Entities.EntityType.CardPhysicalDetailEntity:
					toReturn = this.CardPhysicalDetail;
					break;
				case VarioSL.Entities.EntityType.CardPrintingTypeEntity:
					toReturn = this.CardPrintingType;
					break;
				case VarioSL.Entities.EntityType.CardStockTransferEntity:
					toReturn = this.CardStockTransfer;
					break;
				case VarioSL.Entities.EntityType.CardToContractEntity:
					toReturn = this.CardToContract;
					break;
				case VarioSL.Entities.EntityType.CardToRuleViolationEntity:
					toReturn = this.CardToRuleViolation;
					break;
				case VarioSL.Entities.EntityType.CardWorkItemEntity:
					toReturn = this.CardWorkItem;
					break;
				case VarioSL.Entities.EntityType.CertificateEntity:
					toReturn = this.Certificate;
					break;
				case VarioSL.Entities.EntityType.CertificateFormatEntity:
					toReturn = this.CertificateFormat;
					break;
				case VarioSL.Entities.EntityType.CertificatePurposeEntity:
					toReturn = this.CertificatePurpose;
					break;
				case VarioSL.Entities.EntityType.CertificateTypeEntity:
					toReturn = this.CertificateType;
					break;
				case VarioSL.Entities.EntityType.ClaimEntity:
					toReturn = this.Claim;
					break;
				case VarioSL.Entities.EntityType.ClientFilterTypeEntity:
					toReturn = this.ClientFilterType;
					break;
				case VarioSL.Entities.EntityType.CloseoutPeriodEntity:
					toReturn = this.CloseoutPeriod;
					break;
				case VarioSL.Entities.EntityType.CloseoutStateEntity:
					toReturn = this.CloseoutState;
					break;
				case VarioSL.Entities.EntityType.CloseoutTypeEntity:
					toReturn = this.CloseoutType;
					break;
				case VarioSL.Entities.EntityType.CommentEntity:
					toReturn = this.Comment;
					break;
				case VarioSL.Entities.EntityType.CommentPriorityEntity:
					toReturn = this.CommentPriority;
					break;
				case VarioSL.Entities.EntityType.CommentTypeEntity:
					toReturn = this.CommentType;
					break;
				case VarioSL.Entities.EntityType.ConfigurationEntity:
					toReturn = this.Configuration;
					break;
				case VarioSL.Entities.EntityType.ConfigurationDataTypeEntity:
					toReturn = this.ConfigurationDataType;
					break;
				case VarioSL.Entities.EntityType.ConfigurationDefinitionEntity:
					toReturn = this.ConfigurationDefinition;
					break;
				case VarioSL.Entities.EntityType.ConfigurationOverwriteEntity:
					toReturn = this.ConfigurationOverwrite;
					break;
				case VarioSL.Entities.EntityType.ContentItemEntity:
					toReturn = this.ContentItem;
					break;
				case VarioSL.Entities.EntityType.ContentTypeEntity:
					toReturn = this.ContentType;
					break;
				case VarioSL.Entities.EntityType.ContractEntity:
					toReturn = this.Contract;
					break;
				case VarioSL.Entities.EntityType.ContractAddressEntity:
					toReturn = this.ContractAddress;
					break;
				case VarioSL.Entities.EntityType.ContractDetailsEntity:
					toReturn = this.ContractDetails;
					break;
				case VarioSL.Entities.EntityType.ContractHistoryEntity:
					toReturn = this.ContractHistory;
					break;
				case VarioSL.Entities.EntityType.ContractLinkEntity:
					toReturn = this.ContractLink;
					break;
				case VarioSL.Entities.EntityType.ContractStateEntity:
					toReturn = this.ContractState;
					break;
				case VarioSL.Entities.EntityType.ContractTerminationEntity:
					toReturn = this.ContractTermination;
					break;
				case VarioSL.Entities.EntityType.ContractTerminationTypeEntity:
					toReturn = this.ContractTerminationType;
					break;
				case VarioSL.Entities.EntityType.ContractToPaymentMethodEntity:
					toReturn = this.ContractToPaymentMethod;
					break;
				case VarioSL.Entities.EntityType.ContractToProviderEntity:
					toReturn = this.ContractToProvider;
					break;
				case VarioSL.Entities.EntityType.ContractTypeEntity:
					toReturn = this.ContractType;
					break;
				case VarioSL.Entities.EntityType.ContractWorkItemEntity:
					toReturn = this.ContractWorkItem;
					break;
				case VarioSL.Entities.EntityType.CoordinateEntity:
					toReturn = this.Coordinate;
					break;
				case VarioSL.Entities.EntityType.CountryEntity:
					toReturn = this.Country;
					break;
				case VarioSL.Entities.EntityType.CreditCardAuthorizationEntity:
					toReturn = this.CreditCardAuthorization;
					break;
				case VarioSL.Entities.EntityType.CreditCardAuthorizationLogEntity:
					toReturn = this.CreditCardAuthorizationLog;
					break;
				case VarioSL.Entities.EntityType.CreditCardMerchantEntity:
					toReturn = this.CreditCardMerchant;
					break;
				case VarioSL.Entities.EntityType.CreditCardNetworkEntity:
					toReturn = this.CreditCardNetwork;
					break;
				case VarioSL.Entities.EntityType.CreditCardTerminalEntity:
					toReturn = this.CreditCardTerminal;
					break;
				case VarioSL.Entities.EntityType.CryptoCertificateEntity:
					toReturn = this.CryptoCertificate;
					break;
				case VarioSL.Entities.EntityType.CryptoContentEntity:
					toReturn = this.CryptoContent;
					break;
				case VarioSL.Entities.EntityType.CryptoCryptogramArchiveEntity:
					toReturn = this.CryptoCryptogramArchive;
					break;
				case VarioSL.Entities.EntityType.CryptogramCertificateEntity:
					toReturn = this.CryptogramCertificate;
					break;
				case VarioSL.Entities.EntityType.CryptogramCertificateTypeEntity:
					toReturn = this.CryptogramCertificateType;
					break;
				case VarioSL.Entities.EntityType.CryptogramErrorTypeEntity:
					toReturn = this.CryptogramErrorType;
					break;
				case VarioSL.Entities.EntityType.CryptogramLoadStatusEntity:
					toReturn = this.CryptogramLoadStatus;
					break;
				case VarioSL.Entities.EntityType.CryptoOperatorActivationkeyEntity:
					toReturn = this.CryptoOperatorActivationkey;
					break;
				case VarioSL.Entities.EntityType.CryptoQArchiveEntity:
					toReturn = this.CryptoQArchive;
					break;
				case VarioSL.Entities.EntityType.CryptoResourceEntity:
					toReturn = this.CryptoResource;
					break;
				case VarioSL.Entities.EntityType.CryptoResourceDataEntity:
					toReturn = this.CryptoResourceData;
					break;
				case VarioSL.Entities.EntityType.CryptoSamSignCertificateEntity:
					toReturn = this.CryptoSamSignCertificate;
					break;
				case VarioSL.Entities.EntityType.CryptoUicCertificateEntity:
					toReturn = this.CryptoUicCertificate;
					break;
				case VarioSL.Entities.EntityType.CustomAttributeEntity:
					toReturn = this.CustomAttribute;
					break;
				case VarioSL.Entities.EntityType.CustomAttributeValueEntity:
					toReturn = this.CustomAttributeValue;
					break;
				case VarioSL.Entities.EntityType.CustomAttributeValueToPersonEntity:
					toReturn = this.CustomAttributeValueToPerson;
					break;
				case VarioSL.Entities.EntityType.CustomerAccountEntity:
					toReturn = this.CustomerAccount;
					break;
				case VarioSL.Entities.EntityType.CustomerAccountNotificationEntity:
					toReturn = this.CustomerAccountNotification;
					break;
				case VarioSL.Entities.EntityType.CustomerAccountRoleEntity:
					toReturn = this.CustomerAccountRole;
					break;
				case VarioSL.Entities.EntityType.CustomerAccountStateEntity:
					toReturn = this.CustomerAccountState;
					break;
				case VarioSL.Entities.EntityType.CustomerAccountToVerificationAttemptTypeEntity:
					toReturn = this.CustomerAccountToVerificationAttemptType;
					break;
				case VarioSL.Entities.EntityType.CustomerLanguageEntity:
					toReturn = this.CustomerLanguage;
					break;
				case VarioSL.Entities.EntityType.DeviceCommunicationHistoryEntity:
					toReturn = this.DeviceCommunicationHistory;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderEntity:
					toReturn = this.DeviceReader;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderCryptoResourceEntity:
					toReturn = this.DeviceReaderCryptoResource;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderJobEntity:
					toReturn = this.DeviceReaderJob;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderKeyEntity:
					toReturn = this.DeviceReaderKey;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderKeyToCertEntity:
					toReturn = this.DeviceReaderKeyToCert;
					break;
				case VarioSL.Entities.EntityType.DeviceReaderToCertEntity:
					toReturn = this.DeviceReaderToCert;
					break;
				case VarioSL.Entities.EntityType.DeviceTokenEntity:
					toReturn = this.DeviceToken;
					break;
				case VarioSL.Entities.EntityType.DeviceTokenStateEntity:
					toReturn = this.DeviceTokenState;
					break;
				case VarioSL.Entities.EntityType.DocumentHistoryEntity:
					toReturn = this.DocumentHistory;
					break;
				case VarioSL.Entities.EntityType.DocumentTypeEntity:
					toReturn = this.DocumentType;
					break;
				case VarioSL.Entities.EntityType.DormancyNotificationEntity:
					toReturn = this.DormancyNotification;
					break;
				case VarioSL.Entities.EntityType.DunningActionEntity:
					toReturn = this.DunningAction;
					break;
				case VarioSL.Entities.EntityType.DunningLevelConfigurationEntity:
					toReturn = this.DunningLevelConfiguration;
					break;
				case VarioSL.Entities.EntityType.DunningLevelPostingKeyEntity:
					toReturn = this.DunningLevelPostingKey;
					break;
				case VarioSL.Entities.EntityType.DunningProcessEntity:
					toReturn = this.DunningProcess;
					break;
				case VarioSL.Entities.EntityType.DunningToInvoiceEntity:
					toReturn = this.DunningToInvoice;
					break;
				case VarioSL.Entities.EntityType.DunningToPostingEntity:
					toReturn = this.DunningToPosting;
					break;
				case VarioSL.Entities.EntityType.DunningToProductEntity:
					toReturn = this.DunningToProduct;
					break;
				case VarioSL.Entities.EntityType.EmailEventEntity:
					toReturn = this.EmailEvent;
					break;
				case VarioSL.Entities.EntityType.EmailEventResendLockEntity:
					toReturn = this.EmailEventResendLock;
					break;
				case VarioSL.Entities.EntityType.EmailMessageEntity:
					toReturn = this.EmailMessage;
					break;
				case VarioSL.Entities.EntityType.EntitlementEntity:
					toReturn = this.Entitlement;
					break;
				case VarioSL.Entities.EntityType.EntitlementToProductEntity:
					toReturn = this.EntitlementToProduct;
					break;
				case VarioSL.Entities.EntityType.EntitlementTypeEntity:
					toReturn = this.EntitlementType;
					break;
				case VarioSL.Entities.EntityType.EventSettingEntity:
					toReturn = this.EventSetting;
					break;
				case VarioSL.Entities.EntityType.FareChangeCauseEntity:
					toReturn = this.FareChangeCause;
					break;
				case VarioSL.Entities.EntityType.FareEvasionBehaviourEntity:
					toReturn = this.FareEvasionBehaviour;
					break;
				case VarioSL.Entities.EntityType.FareEvasionIncidentEntity:
					toReturn = this.FareEvasionIncident;
					break;
				case VarioSL.Entities.EntityType.FareEvasionInspectionEntity:
					toReturn = this.FareEvasionInspection;
					break;
				case VarioSL.Entities.EntityType.FareEvasionStateEntity:
					toReturn = this.FareEvasionState;
					break;
				case VarioSL.Entities.EntityType.FarePaymentErrorEntity:
					toReturn = this.FarePaymentError;
					break;
				case VarioSL.Entities.EntityType.FarePaymentResultTypeEntity:
					toReturn = this.FarePaymentResultType;
					break;
				case VarioSL.Entities.EntityType.FastcardEntity:
					toReturn = this.Fastcard;
					break;
				case VarioSL.Entities.EntityType.FilterCategoryEntity:
					toReturn = this.FilterCategory;
					break;
				case VarioSL.Entities.EntityType.FilterElementEntity:
					toReturn = this.FilterElement;
					break;
				case VarioSL.Entities.EntityType.FilterListEntity:
					toReturn = this.FilterList;
					break;
				case VarioSL.Entities.EntityType.FilterListElementEntity:
					toReturn = this.FilterListElement;
					break;
				case VarioSL.Entities.EntityType.FilterTypeEntity:
					toReturn = this.FilterType;
					break;
				case VarioSL.Entities.EntityType.FilterValueEntity:
					toReturn = this.FilterValue;
					break;
				case VarioSL.Entities.EntityType.FilterValueSetEntity:
					toReturn = this.FilterValueSet;
					break;
				case VarioSL.Entities.EntityType.FlexValueEntity:
					toReturn = this.FlexValue;
					break;
				case VarioSL.Entities.EntityType.FormEntity:
					toReturn = this.Form;
					break;
				case VarioSL.Entities.EntityType.FormTemplateEntity:
					toReturn = this.FormTemplate;
					break;
				case VarioSL.Entities.EntityType.FrequencyEntity:
					toReturn = this.Frequency;
					break;
				case VarioSL.Entities.EntityType.GenderEntity:
					toReturn = this.Gender;
					break;
				case VarioSL.Entities.EntityType.IdentificationTypeEntity:
					toReturn = this.IdentificationType;
					break;
				case VarioSL.Entities.EntityType.InspectionCriterionEntity:
					toReturn = this.InspectionCriterion;
					break;
				case VarioSL.Entities.EntityType.InspectionReportEntity:
					toReturn = this.InspectionReport;
					break;
				case VarioSL.Entities.EntityType.InspectionResultEntity:
					toReturn = this.InspectionResult;
					break;
				case VarioSL.Entities.EntityType.InventoryStateEntity:
					toReturn = this.InventoryState;
					break;
				case VarioSL.Entities.EntityType.InvoiceEntity:
					toReturn = this.Invoice;
					break;
				case VarioSL.Entities.EntityType.InvoiceEntryEntity:
					toReturn = this.InvoiceEntry;
					break;
				case VarioSL.Entities.EntityType.InvoiceTypeEntity:
					toReturn = this.InvoiceType;
					break;
				case VarioSL.Entities.EntityType.InvoicingEntity:
					toReturn = this.Invoicing;
					break;
				case VarioSL.Entities.EntityType.InvoicingFilterTypeEntity:
					toReturn = this.InvoicingFilterType;
					break;
				case VarioSL.Entities.EntityType.InvoicingTypeEntity:
					toReturn = this.InvoicingType;
					break;
				case VarioSL.Entities.EntityType.JobEntity:
					toReturn = this.Job;
					break;
				case VarioSL.Entities.EntityType.JobBulkImportEntity:
					toReturn = this.JobBulkImport;
					break;
				case VarioSL.Entities.EntityType.JobCardImportEntity:
					toReturn = this.JobCardImport;
					break;
				case VarioSL.Entities.EntityType.JobFileTypeEntity:
					toReturn = this.JobFileType;
					break;
				case VarioSL.Entities.EntityType.JobStateEntity:
					toReturn = this.JobState;
					break;
				case VarioSL.Entities.EntityType.JobTypeEntity:
					toReturn = this.JobType;
					break;
				case VarioSL.Entities.EntityType.LookupAddressEntity:
					toReturn = this.LookupAddress;
					break;
				case VarioSL.Entities.EntityType.MediaEligibilityRequirementEntity:
					toReturn = this.MediaEligibilityRequirement;
					break;
				case VarioSL.Entities.EntityType.MediaSalePreconditionEntity:
					toReturn = this.MediaSalePrecondition;
					break;
				case VarioSL.Entities.EntityType.MerchantEntity:
					toReturn = this.Merchant;
					break;
				case VarioSL.Entities.EntityType.MessageEntity:
					toReturn = this.Message;
					break;
				case VarioSL.Entities.EntityType.MessageFormatEntity:
					toReturn = this.MessageFormat;
					break;
				case VarioSL.Entities.EntityType.MessageTypeEntity:
					toReturn = this.MessageType;
					break;
				case VarioSL.Entities.EntityType.MobilityProviderEntity:
					toReturn = this.MobilityProvider;
					break;
				case VarioSL.Entities.EntityType.NotificationDeviceEntity:
					toReturn = this.NotificationDevice;
					break;
				case VarioSL.Entities.EntityType.NotificationMessageEntity:
					toReturn = this.NotificationMessage;
					break;
				case VarioSL.Entities.EntityType.NotificationMessageOwnerEntity:
					toReturn = this.NotificationMessageOwner;
					break;
				case VarioSL.Entities.EntityType.NotificationMessageToCustomerEntity:
					toReturn = this.NotificationMessageToCustomer;
					break;
				case VarioSL.Entities.EntityType.NumberGroupEntity:
					toReturn = this.NumberGroup;
					break;
				case VarioSL.Entities.EntityType.NumberGroupRandomizedEntity:
					toReturn = this.NumberGroupRandomized;
					break;
				case VarioSL.Entities.EntityType.NumberGroupScopeEntity:
					toReturn = this.NumberGroupScope;
					break;
				case VarioSL.Entities.EntityType.OrderEntity:
					toReturn = this.Order;
					break;
				case VarioSL.Entities.EntityType.OrderDetailEntity:
					toReturn = this.OrderDetail;
					break;
				case VarioSL.Entities.EntityType.OrderDetailToCardEntity:
					toReturn = this.OrderDetailToCard;
					break;
				case VarioSL.Entities.EntityType.OrderDetailToCardHolderEntity:
					toReturn = this.OrderDetailToCardHolder;
					break;
				case VarioSL.Entities.EntityType.OrderHistoryEntity:
					toReturn = this.OrderHistory;
					break;
				case VarioSL.Entities.EntityType.OrderProcessingCleanupEntity:
					toReturn = this.OrderProcessingCleanup;
					break;
				case VarioSL.Entities.EntityType.OrderStateEntity:
					toReturn = this.OrderState;
					break;
				case VarioSL.Entities.EntityType.OrderTypeEntity:
					toReturn = this.OrderType;
					break;
				case VarioSL.Entities.EntityType.OrderWithTotalEntity:
					toReturn = this.OrderWithTotal;
					break;
				case VarioSL.Entities.EntityType.OrderWorkItemEntity:
					toReturn = this.OrderWorkItem;
					break;
				case VarioSL.Entities.EntityType.OrganizationEntity:
					toReturn = this.Organization;
					break;
				case VarioSL.Entities.EntityType.OrganizationAddressEntity:
					toReturn = this.OrganizationAddress;
					break;
				case VarioSL.Entities.EntityType.OrganizationParticipantEntity:
					toReturn = this.OrganizationParticipant;
					break;
				case VarioSL.Entities.EntityType.OrganizationSubtypeEntity:
					toReturn = this.OrganizationSubtype;
					break;
				case VarioSL.Entities.EntityType.OrganizationTypeEntity:
					toReturn = this.OrganizationType;
					break;
				case VarioSL.Entities.EntityType.PageContentEntity:
					toReturn = this.PageContent;
					break;
				case VarioSL.Entities.EntityType.ParameterEntity:
					toReturn = this.Parameter;
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveEntity:
					toReturn = this.ParameterArchive;
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveReleaseEntity:
					toReturn = this.ParameterArchiveRelease;
					break;
				case VarioSL.Entities.EntityType.ParameterArchiveResultViewEntity:
					toReturn = this.ParameterArchiveResultView;
					break;
				case VarioSL.Entities.EntityType.ParameterGroupEntity:
					toReturn = this.ParameterGroup;
					break;
				case VarioSL.Entities.EntityType.ParameterGroupToParameterEntity:
					toReturn = this.ParameterGroupToParameter;
					break;
				case VarioSL.Entities.EntityType.ParameterTypeEntity:
					toReturn = this.ParameterType;
					break;
				case VarioSL.Entities.EntityType.ParameterValueEntity:
					toReturn = this.ParameterValue;
					break;
				case VarioSL.Entities.EntityType.ParaTransAttributeEntity:
					toReturn = this.ParaTransAttribute;
					break;
				case VarioSL.Entities.EntityType.ParaTransAttributeTypeEntity:
					toReturn = this.ParaTransAttributeType;
					break;
				case VarioSL.Entities.EntityType.ParaTransJournalEntity:
					toReturn = this.ParaTransJournal;
					break;
				case VarioSL.Entities.EntityType.ParaTransJournalTypeEntity:
					toReturn = this.ParaTransJournalType;
					break;
				case VarioSL.Entities.EntityType.ParaTransPaymentTypeEntity:
					toReturn = this.ParaTransPaymentType;
					break;
				case VarioSL.Entities.EntityType.ParticipantGroupEntity:
					toReturn = this.ParticipantGroup;
					break;
				case VarioSL.Entities.EntityType.PassExpiryNotificationEntity:
					toReturn = this.PassExpiryNotification;
					break;
				case VarioSL.Entities.EntityType.PaymentEntity:
					toReturn = this.Payment;
					break;
				case VarioSL.Entities.EntityType.PaymentJournalEntity:
					toReturn = this.PaymentJournal;
					break;
				case VarioSL.Entities.EntityType.PaymentJournalToComponentEntity:
					toReturn = this.PaymentJournalToComponent;
					break;
				case VarioSL.Entities.EntityType.PaymentMethodEntity:
					toReturn = this.PaymentMethod;
					break;
				case VarioSL.Entities.EntityType.PaymentModalityEntity:
					toReturn = this.PaymentModality;
					break;
				case VarioSL.Entities.EntityType.PaymentOptionEntity:
					toReturn = this.PaymentOption;
					break;
				case VarioSL.Entities.EntityType.PaymentProviderEntity:
					toReturn = this.PaymentProvider;
					break;
				case VarioSL.Entities.EntityType.PaymentProviderAccountEntity:
					toReturn = this.PaymentProviderAccount;
					break;
				case VarioSL.Entities.EntityType.PaymentRecognitionEntity:
					toReturn = this.PaymentRecognition;
					break;
				case VarioSL.Entities.EntityType.PaymentReconciliationEntity:
					toReturn = this.PaymentReconciliation;
					break;
				case VarioSL.Entities.EntityType.PaymentReconciliationStateEntity:
					toReturn = this.PaymentReconciliationState;
					break;
				case VarioSL.Entities.EntityType.PaymentStateEntity:
					toReturn = this.PaymentState;
					break;
				case VarioSL.Entities.EntityType.PendingOrderEntity:
					toReturn = this.PendingOrder;
					break;
				case VarioSL.Entities.EntityType.PerformanceEntity:
					toReturn = this.Performance;
					break;
				case VarioSL.Entities.EntityType.PerformanceAggregationEntity:
					toReturn = this.PerformanceAggregation;
					break;
				case VarioSL.Entities.EntityType.PerformanceComponentEntity:
					toReturn = this.PerformanceComponent;
					break;
				case VarioSL.Entities.EntityType.PerformanceIndicatorEntity:
					toReturn = this.PerformanceIndicator;
					break;
				case VarioSL.Entities.EntityType.PersonEntity:
					toReturn = this.Person;
					break;
				case VarioSL.Entities.EntityType.PersonAddressEntity:
					toReturn = this.PersonAddress;
					break;
				case VarioSL.Entities.EntityType.PhotographEntity:
					toReturn = this.Photograph;
					break;
				case VarioSL.Entities.EntityType.PickupLocationEntity:
					toReturn = this.PickupLocation;
					break;
				case VarioSL.Entities.EntityType.PickupLocationToTicketEntity:
					toReturn = this.PickupLocationToTicket;
					break;
				case VarioSL.Entities.EntityType.PostingEntity:
					toReturn = this.Posting;
					break;
				case VarioSL.Entities.EntityType.PostingKeyEntity:
					toReturn = this.PostingKey;
					break;
				case VarioSL.Entities.EntityType.PostingKeyCategoryEntity:
					toReturn = this.PostingKeyCategory;
					break;
				case VarioSL.Entities.EntityType.PostingScopeEntity:
					toReturn = this.PostingScope;
					break;
				case VarioSL.Entities.EntityType.PriceAdjustmentEntity:
					toReturn = this.PriceAdjustment;
					break;
				case VarioSL.Entities.EntityType.PriceAdjustmentTemplateEntity:
					toReturn = this.PriceAdjustmentTemplate;
					break;
				case VarioSL.Entities.EntityType.PrinterEntity:
					toReturn = this.Printer;
					break;
				case VarioSL.Entities.EntityType.PrinterStateEntity:
					toReturn = this.PrinterState;
					break;
				case VarioSL.Entities.EntityType.PrinterToUnitEntity:
					toReturn = this.PrinterToUnit;
					break;
				case VarioSL.Entities.EntityType.PrinterTypeEntity:
					toReturn = this.PrinterType;
					break;
				case VarioSL.Entities.EntityType.PriorityEntity:
					toReturn = this.Priority;
					break;
				case VarioSL.Entities.EntityType.ProductEntity:
					toReturn = this.Product;
					break;
				case VarioSL.Entities.EntityType.ProductRelationEntity:
					toReturn = this.ProductRelation;
					break;
				case VarioSL.Entities.EntityType.ProductSubsidyEntity:
					toReturn = this.ProductSubsidy;
					break;
				case VarioSL.Entities.EntityType.ProductTerminationEntity:
					toReturn = this.ProductTermination;
					break;
				case VarioSL.Entities.EntityType.ProductTerminationTypeEntity:
					toReturn = this.ProductTerminationType;
					break;
				case VarioSL.Entities.EntityType.ProductTypeEntity:
					toReturn = this.ProductType;
					break;
				case VarioSL.Entities.EntityType.PromoCodeEntity:
					toReturn = this.PromoCode;
					break;
				case VarioSL.Entities.EntityType.PromoCodeStatusEntity:
					toReturn = this.PromoCodeStatus;
					break;
				case VarioSL.Entities.EntityType.ProviderAccountStateEntity:
					toReturn = this.ProviderAccountState;
					break;
				case VarioSL.Entities.EntityType.ProvisioningStateEntity:
					toReturn = this.ProvisioningState;
					break;
				case VarioSL.Entities.EntityType.RecipientGroupEntity:
					toReturn = this.RecipientGroup;
					break;
				case VarioSL.Entities.EntityType.RecipientGroupMemberEntity:
					toReturn = this.RecipientGroupMember;
					break;
				case VarioSL.Entities.EntityType.RefundEntity:
					toReturn = this.Refund;
					break;
				case VarioSL.Entities.EntityType.RefundReasonEntity:
					toReturn = this.RefundReason;
					break;
				case VarioSL.Entities.EntityType.RentalStateEntity:
					toReturn = this.RentalState;
					break;
				case VarioSL.Entities.EntityType.RentalTypeEntity:
					toReturn = this.RentalType;
					break;
				case VarioSL.Entities.EntityType.ReportEntity:
					toReturn = this.Report;
					break;
				case VarioSL.Entities.EntityType.ReportCategoryEntity:
					toReturn = this.ReportCategory;
					break;
				case VarioSL.Entities.EntityType.ReportDataFileEntity:
					toReturn = this.ReportDataFile;
					break;
				case VarioSL.Entities.EntityType.ReportJobEntity:
					toReturn = this.ReportJob;
					break;
				case VarioSL.Entities.EntityType.ReportJobResultEntity:
					toReturn = this.ReportJobResult;
					break;
				case VarioSL.Entities.EntityType.ReportToClientEntity:
					toReturn = this.ReportToClient;
					break;
				case VarioSL.Entities.EntityType.RevenueRecognitionEntity:
					toReturn = this.RevenueRecognition;
					break;
				case VarioSL.Entities.EntityType.RevenueSettlementEntity:
					toReturn = this.RevenueSettlement;
					break;
				case VarioSL.Entities.EntityType.RuleViolationEntity:
					toReturn = this.RuleViolation;
					break;
				case VarioSL.Entities.EntityType.RuleViolationProviderEntity:
					toReturn = this.RuleViolationProvider;
					break;
				case VarioSL.Entities.EntityType.RuleViolationSourceTypeEntity:
					toReturn = this.RuleViolationSourceType;
					break;
				case VarioSL.Entities.EntityType.RuleViolationToTransactionEntity:
					toReturn = this.RuleViolationToTransaction;
					break;
				case VarioSL.Entities.EntityType.RuleViolationTypeEntity:
					toReturn = this.RuleViolationType;
					break;
				case VarioSL.Entities.EntityType.SaleEntity:
					toReturn = this.Sale;
					break;
				case VarioSL.Entities.EntityType.SalesChannelToPaymentMethodEntity:
					toReturn = this.SalesChannelToPaymentMethod;
					break;
				case VarioSL.Entities.EntityType.SalesRevenueEntity:
					toReturn = this.SalesRevenue;
					break;
				case VarioSL.Entities.EntityType.SaleToComponentEntity:
					toReturn = this.SaleToComponent;
					break;
				case VarioSL.Entities.EntityType.SaleTypeEntity:
					toReturn = this.SaleType;
					break;
				case VarioSL.Entities.EntityType.SamModuleEntity:
					toReturn = this.SamModule;
					break;
				case VarioSL.Entities.EntityType.SamModuleStatusEntity:
					toReturn = this.SamModuleStatus;
					break;
				case VarioSL.Entities.EntityType.SchoolYearEntity:
					toReturn = this.SchoolYear;
					break;
				case VarioSL.Entities.EntityType.SecurityQuestionEntity:
					toReturn = this.SecurityQuestion;
					break;
				case VarioSL.Entities.EntityType.SecurityResponseEntity:
					toReturn = this.SecurityResponse;
					break;
				case VarioSL.Entities.EntityType.SepaFrequencyTypeEntity:
					toReturn = this.SepaFrequencyType;
					break;
				case VarioSL.Entities.EntityType.SepaOwnerTypeEntity:
					toReturn = this.SepaOwnerType;
					break;
				case VarioSL.Entities.EntityType.ServerResponseTimeDataEntity:
					toReturn = this.ServerResponseTimeData;
					break;
				case VarioSL.Entities.EntityType.ServerResponseTimeDataAggregationEntity:
					toReturn = this.ServerResponseTimeDataAggregation;
					break;
				case VarioSL.Entities.EntityType.SettledRevenueEntity:
					toReturn = this.SettledRevenue;
					break;
				case VarioSL.Entities.EntityType.SettlementAccountEntity:
					toReturn = this.SettlementAccount;
					break;
				case VarioSL.Entities.EntityType.SettlementCalendarEntity:
					toReturn = this.SettlementCalendar;
					break;
				case VarioSL.Entities.EntityType.SettlementDistributionPolicyEntity:
					toReturn = this.SettlementDistributionPolicy;
					break;
				case VarioSL.Entities.EntityType.SettlementHolidayEntity:
					toReturn = this.SettlementHoliday;
					break;
				case VarioSL.Entities.EntityType.SettlementOperationEntity:
					toReturn = this.SettlementOperation;
					break;
				case VarioSL.Entities.EntityType.SettlementQuerySettingEntity:
					toReturn = this.SettlementQuerySetting;
					break;
				case VarioSL.Entities.EntityType.SettlementQuerySettingToTicketEntity:
					toReturn = this.SettlementQuerySettingToTicket;
					break;
				case VarioSL.Entities.EntityType.SettlementQueryValueEntity:
					toReturn = this.SettlementQueryValue;
					break;
				case VarioSL.Entities.EntityType.SettlementReleaseStateEntity:
					toReturn = this.SettlementReleaseState;
					break;
				case VarioSL.Entities.EntityType.SettlementResultEntity:
					toReturn = this.SettlementResult;
					break;
				case VarioSL.Entities.EntityType.SettlementRunEntity:
					toReturn = this.SettlementRun;
					break;
				case VarioSL.Entities.EntityType.SettlementRunValueEntity:
					toReturn = this.SettlementRunValue;
					break;
				case VarioSL.Entities.EntityType.SettlementRunValueToVarioSettlementEntity:
					toReturn = this.SettlementRunValueToVarioSettlement;
					break;
				case VarioSL.Entities.EntityType.SettlementSetupEntity:
					toReturn = this.SettlementSetup;
					break;
				case VarioSL.Entities.EntityType.SettlementTypeEntity:
					toReturn = this.SettlementType;
					break;
				case VarioSL.Entities.EntityType.SourceEntity:
					toReturn = this.Source;
					break;
				case VarioSL.Entities.EntityType.StatementOfAccountEntity:
					toReturn = this.StatementOfAccount;
					break;
				case VarioSL.Entities.EntityType.StateOfCountryEntity:
					toReturn = this.StateOfCountry;
					break;
				case VarioSL.Entities.EntityType.StockTransferEntity:
					toReturn = this.StockTransfer;
					break;
				case VarioSL.Entities.EntityType.StorageLocationEntity:
					toReturn = this.StorageLocation;
					break;
				case VarioSL.Entities.EntityType.SubsidyLimitEntity:
					toReturn = this.SubsidyLimit;
					break;
				case VarioSL.Entities.EntityType.SubsidyTransactionEntity:
					toReturn = this.SubsidyTransaction;
					break;
				case VarioSL.Entities.EntityType.SurveyEntity:
					toReturn = this.Survey;
					break;
				case VarioSL.Entities.EntityType.SurveyAnswerEntity:
					toReturn = this.SurveyAnswer;
					break;
				case VarioSL.Entities.EntityType.SurveyChoiceEntity:
					toReturn = this.SurveyChoice;
					break;
				case VarioSL.Entities.EntityType.SurveyDescriptionEntity:
					toReturn = this.SurveyDescription;
					break;
				case VarioSL.Entities.EntityType.SurveyQuestionEntity:
					toReturn = this.SurveyQuestion;
					break;
				case VarioSL.Entities.EntityType.SurveyResponseEntity:
					toReturn = this.SurveyResponse;
					break;
				case VarioSL.Entities.EntityType.TenantEntity:
					toReturn = this.Tenant;
					break;
				case VarioSL.Entities.EntityType.TenantHistoryEntity:
					toReturn = this.TenantHistory;
					break;
				case VarioSL.Entities.EntityType.TenantPersonEntity:
					toReturn = this.TenantPerson;
					break;
				case VarioSL.Entities.EntityType.TenantStateEntity:
					toReturn = this.TenantState;
					break;
				case VarioSL.Entities.EntityType.TerminationStatusEntity:
					toReturn = this.TerminationStatus;
					break;
				case VarioSL.Entities.EntityType.TicketAssignmentEntity:
					toReturn = this.TicketAssignment;
					break;
				case VarioSL.Entities.EntityType.TicketAssignmentStatusEntity:
					toReturn = this.TicketAssignmentStatus;
					break;
				case VarioSL.Entities.EntityType.TicketSerialNumberEntity:
					toReturn = this.TicketSerialNumber;
					break;
				case VarioSL.Entities.EntityType.TicketSerialNumberStatusEntity:
					toReturn = this.TicketSerialNumberStatus;
					break;
				case VarioSL.Entities.EntityType.TransactionJournalEntity:
					toReturn = this.TransactionJournal;
					break;
				case VarioSL.Entities.EntityType.TransactionJournalCorrectionEntity:
					toReturn = this.TransactionJournalCorrection;
					break;
				case VarioSL.Entities.EntityType.TransactionJourneyHistoryEntity:
					toReturn = this.TransactionJourneyHistory;
					break;
				case VarioSL.Entities.EntityType.TransactionToInspectionEntity:
					toReturn = this.TransactionToInspection;
					break;
				case VarioSL.Entities.EntityType.TransactionTypeEntity:
					toReturn = this.TransactionType;
					break;
				case VarioSL.Entities.EntityType.TransportCompanyEntity:
					toReturn = this.TransportCompany;
					break;
				case VarioSL.Entities.EntityType.TriggerTypeEntity:
					toReturn = this.TriggerType;
					break;
				case VarioSL.Entities.EntityType.UnitCollectionEntity:
					toReturn = this.UnitCollection;
					break;
				case VarioSL.Entities.EntityType.UnitCollectionToUnitEntity:
					toReturn = this.UnitCollectionToUnit;
					break;
				case VarioSL.Entities.EntityType.ValidationResultEntity:
					toReturn = this.ValidationResult;
					break;
				case VarioSL.Entities.EntityType.ValidationResultResolveTypeEntity:
					toReturn = this.ValidationResultResolveType;
					break;
				case VarioSL.Entities.EntityType.ValidationRuleEntity:
					toReturn = this.ValidationRule;
					break;
				case VarioSL.Entities.EntityType.ValidationRunEntity:
					toReturn = this.ValidationRun;
					break;
				case VarioSL.Entities.EntityType.ValidationRunRuleEntity:
					toReturn = this.ValidationRunRule;
					break;
				case VarioSL.Entities.EntityType.ValidationStatusEntity:
					toReturn = this.ValidationStatus;
					break;
				case VarioSL.Entities.EntityType.ValidationValueEntity:
					toReturn = this.ValidationValue;
					break;
				case VarioSL.Entities.EntityType.VdvTransactionEntity:
					toReturn = this.VdvTransaction;
					break;
				case VarioSL.Entities.EntityType.VerificationAttemptEntity:
					toReturn = this.VerificationAttempt;
					break;
				case VarioSL.Entities.EntityType.VerificationAttemptTypeEntity:
					toReturn = this.VerificationAttemptType;
					break;
				case VarioSL.Entities.EntityType.VirtualCardEntity:
					toReturn = this.VirtualCard;
					break;
				case VarioSL.Entities.EntityType.VoucherEntity:
					toReturn = this.Voucher;
					break;
				case VarioSL.Entities.EntityType.WhitelistEntity:
					toReturn = this.Whitelist;
					break;
				case VarioSL.Entities.EntityType.WhitelistJournalEntity:
					toReturn = this.WhitelistJournal;
					break;
				case VarioSL.Entities.EntityType.WorkItemEntity:
					toReturn = this.WorkItem;
					break;
				case VarioSL.Entities.EntityType.WorkItemCategoryEntity:
					toReturn = this.WorkItemCategory;
					break;
				case VarioSL.Entities.EntityType.WorkItemHistoryEntity:
					toReturn = this.WorkItemHistory;
					break;
				case VarioSL.Entities.EntityType.WorkItemStateEntity:
					toReturn = this.WorkItemState;
					break;
				case VarioSL.Entities.EntityType.WorkItemSubjectEntity:
					toReturn = this.WorkItemSubject;
					break;
				case VarioSL.Entities.EntityType.WorkItemViewEntity:
					toReturn = this.WorkItemView;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource<TEntity> GetQueryableForEntity<TEntity>()
			    where TEntity : class
		{
    		return new DataSource<TEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting AbortedTransactionEntity instances in the database.</summary>
		public DataSource<AbortedTransactionEntity> AbortedTransaction
		{
			get { return new DataSource<AbortedTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountBalanceEntity instances in the database.</summary>
		public DataSource<AccountBalanceEntity> AccountBalance
		{
			get { return new DataSource<AccountBalanceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountEntryEntity instances in the database.</summary>
		public DataSource<AccountEntryEntity> AccountEntry
		{
			get { return new DataSource<AccountEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountEntryNumberEntity instances in the database.</summary>
		public DataSource<AccountEntryNumberEntity> AccountEntryNumber
		{
			get { return new DataSource<AccountEntryNumberEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountPostingKeyEntity instances in the database.</summary>
		public DataSource<AccountPostingKeyEntity> AccountPostingKey
		{
			get { return new DataSource<AccountPostingKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApplicationEntity instances in the database.</summary>
		public DataSource<ApplicationEntity> Application
		{
			get { return new DataSource<ApplicationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApplicationVersionEntity instances in the database.</summary>
		public DataSource<ApplicationVersionEntity> ApplicationVersion
		{
			get { return new DataSource<ApplicationVersionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApportionmentEntity instances in the database.</summary>
		public DataSource<ApportionmentEntity> Apportionment
		{
			get { return new DataSource<ApportionmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApportionmentResultEntity instances in the database.</summary>
		public DataSource<ApportionmentResultEntity> ApportionmentResult
		{
			get { return new DataSource<ApportionmentResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AreaListEntity instances in the database.</summary>
		public DataSource<AreaListEntity> AreaList
		{
			get { return new DataSource<AreaListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AreaListElementEntity instances in the database.</summary>
		public DataSource<AreaListElementEntity> AreaListElement
		{
			get { return new DataSource<AreaListElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AreaListElementGroupEntity instances in the database.</summary>
		public DataSource<AreaListElementGroupEntity> AreaListElementGroup
		{
			get { return new DataSource<AreaListElementGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AreaTypeEntity instances in the database.</summary>
		public DataSource<AreaTypeEntity> AreaType
		{
			get { return new DataSource<AreaTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeEntity instances in the database.</summary>
		public DataSource<AttributeEntity> Attribute
		{
			get { return new DataSource<AttributeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeBitmapLayoutObjectEntity instances in the database.</summary>
		public DataSource<AttributeBitmapLayoutObjectEntity> AttributeBitmapLayoutObject
		{
			get { return new DataSource<AttributeBitmapLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeTextLayoutObjectEntity instances in the database.</summary>
		public DataSource<AttributeTextLayoutObjectEntity> AttributeTextLayoutObject
		{
			get { return new DataSource<AttributeTextLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeValueEntity instances in the database.</summary>
		public DataSource<AttributeValueEntity> AttributeValue
		{
			get { return new DataSource<AttributeValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeValueListEntity instances in the database.</summary>
		public DataSource<AttributeValueListEntity> AttributeValueList
		{
			get { return new DataSource<AttributeValueListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AutomatEntity instances in the database.</summary>
		public DataSource<AutomatEntity> Automat
		{
			get { return new DataSource<AutomatEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankEntity instances in the database.</summary>
		public DataSource<BankEntity> Bank
		{
			get { return new DataSource<BankEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BinaryDataEntity instances in the database.</summary>
		public DataSource<BinaryDataEntity> BinaryData
		{
			get { return new DataSource<BinaryDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BlockingReasonEntity instances in the database.</summary>
		public DataSource<BlockingReasonEntity> BlockingReason
		{
			get { return new DataSource<BlockingReasonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleEntity instances in the database.</summary>
		public DataSource<BusinessRuleEntity> BusinessRule
		{
			get { return new DataSource<BusinessRuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleClauseEntity instances in the database.</summary>
		public DataSource<BusinessRuleClauseEntity> BusinessRuleClause
		{
			get { return new DataSource<BusinessRuleClauseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleConditionEntity instances in the database.</summary>
		public DataSource<BusinessRuleConditionEntity> BusinessRuleCondition
		{
			get { return new DataSource<BusinessRuleConditionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleResultEntity instances in the database.</summary>
		public DataSource<BusinessRuleResultEntity> BusinessRuleResult
		{
			get { return new DataSource<BusinessRuleResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleTypeEntity instances in the database.</summary>
		public DataSource<BusinessRuleTypeEntity> BusinessRuleType
		{
			get { return new DataSource<BusinessRuleTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleTypeToVariableEntity instances in the database.</summary>
		public DataSource<BusinessRuleTypeToVariableEntity> BusinessRuleTypeToVariable
		{
			get { return new DataSource<BusinessRuleTypeToVariableEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinessRuleVariableEntity instances in the database.</summary>
		public DataSource<BusinessRuleVariableEntity> BusinessRuleVariable
		{
			get { return new DataSource<BusinessRuleVariableEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CalendarEntity instances in the database.</summary>
		public DataSource<CalendarEntity> Calendar
		{
			get { return new DataSource<CalendarEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CalendarEntryEntity instances in the database.</summary>
		public DataSource<CalendarEntryEntity> CalendarEntry
		{
			get { return new DataSource<CalendarEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardActionAttributeEntity instances in the database.</summary>
		public DataSource<CardActionAttributeEntity> CardActionAttribute
		{
			get { return new DataSource<CardActionAttributeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardActionRequestEntity instances in the database.</summary>
		public DataSource<CardActionRequestEntity> CardActionRequest
		{
			get { return new DataSource<CardActionRequestEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardActionRequestTypeEntity instances in the database.</summary>
		public DataSource<CardActionRequestTypeEntity> CardActionRequestType
		{
			get { return new DataSource<CardActionRequestTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardChipTypeEntity instances in the database.</summary>
		public DataSource<CardChipTypeEntity> CardChipType
		{
			get { return new DataSource<CardChipTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardStateEntity instances in the database.</summary>
		public DataSource<CardStateEntity> CardState
		{
			get { return new DataSource<CardStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardTicketEntity instances in the database.</summary>
		public DataSource<CardTicketEntity> CardTicket
		{
			get { return new DataSource<CardTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardTicketToTicketEntity instances in the database.</summary>
		public DataSource<CardTicketToTicketEntity> CardTicketToTicket
		{
			get { return new DataSource<CardTicketToTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CashServiceEntity instances in the database.</summary>
		public DataSource<CashServiceEntity> CashService
		{
			get { return new DataSource<CashServiceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CashServiceBalanceEntity instances in the database.</summary>
		public DataSource<CashServiceBalanceEntity> CashServiceBalance
		{
			get { return new DataSource<CashServiceBalanceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CashServiceDataEntity instances in the database.</summary>
		public DataSource<CashServiceDataEntity> CashServiceData
		{
			get { return new DataSource<CashServiceDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CashUnitEntity instances in the database.</summary>
		public DataSource<CashUnitEntity> CashUnit
		{
			get { return new DataSource<CashUnitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ChoiceEntity instances in the database.</summary>
		public DataSource<ChoiceEntity> Choice
		{
			get { return new DataSource<ChoiceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingEntity instances in the database.</summary>
		public DataSource<ClearingEntity> Clearing
		{
			get { return new DataSource<ClearingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingClassificationEntity instances in the database.</summary>
		public DataSource<ClearingClassificationEntity> ClearingClassification
		{
			get { return new DataSource<ClearingClassificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingDetailEntity instances in the database.</summary>
		public DataSource<ClearingDetailEntity> ClearingDetail
		{
			get { return new DataSource<ClearingDetailEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingResultEntity instances in the database.</summary>
		public DataSource<ClearingResultEntity> ClearingResult
		{
			get { return new DataSource<ClearingResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingResultLevelEntity instances in the database.</summary>
		public DataSource<ClearingResultLevelEntity> ClearingResultLevel
		{
			get { return new DataSource<ClearingResultLevelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingStateEntity instances in the database.</summary>
		public DataSource<ClearingStateEntity> ClearingState
		{
			get { return new DataSource<ClearingStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingSumEntity instances in the database.</summary>
		public DataSource<ClearingSumEntity> ClearingSum
		{
			get { return new DataSource<ClearingSumEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClearingTransactionEntity instances in the database.</summary>
		public DataSource<ClearingTransactionEntity> ClearingTransaction
		{
			get { return new DataSource<ClearingTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientEntity instances in the database.</summary>
		public DataSource<ClientEntity> Client
		{
			get { return new DataSource<ClientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientAdaptedLayoutObjectEntity instances in the database.</summary>
		public DataSource<ClientAdaptedLayoutObjectEntity> ClientAdaptedLayoutObject
		{
			get { return new DataSource<ClientAdaptedLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ComponentEntity instances in the database.</summary>
		public DataSource<ComponentEntity> Component
		{
			get { return new DataSource<ComponentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ComponentClearingEntity instances in the database.</summary>
		public DataSource<ComponentClearingEntity> ComponentClearing
		{
			get { return new DataSource<ComponentClearingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ComponentFillingEntity instances in the database.</summary>
		public DataSource<ComponentFillingEntity> ComponentFilling
		{
			get { return new DataSource<ComponentFillingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ComponentStateEntity instances in the database.</summary>
		public DataSource<ComponentStateEntity> ComponentState
		{
			get { return new DataSource<ComponentStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ComponentTypeEntity instances in the database.</summary>
		public DataSource<ComponentTypeEntity> ComponentType
		{
			get { return new DataSource<ComponentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConditionalSubPageLayoutObjectEntity instances in the database.</summary>
		public DataSource<ConditionalSubPageLayoutObjectEntity> ConditionalSubPageLayoutObject
		{
			get { return new DataSource<ConditionalSubPageLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditScreeningEntity instances in the database.</summary>
		public DataSource<CreditScreeningEntity> CreditScreening
		{
			get { return new DataSource<CreditScreeningEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DayTypeEntity instances in the database.</summary>
		public DataSource<DayTypeEntity> DayType
		{
			get { return new DataSource<DayTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DebtorEntity instances in the database.</summary>
		public DataSource<DebtorEntity> Debtor
		{
			get { return new DataSource<DebtorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DebtorCardEntity instances in the database.</summary>
		public DataSource<DebtorCardEntity> DebtorCard
		{
			get { return new DataSource<DebtorCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DebtorCardHistoryEntity instances in the database.</summary>
		public DataSource<DebtorCardHistoryEntity> DebtorCardHistory
		{
			get { return new DataSource<DebtorCardHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DefaultPinEntity instances in the database.</summary>
		public DataSource<DefaultPinEntity> DefaultPin
		{
			get { return new DataSource<DefaultPinEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DepotEntity instances in the database.</summary>
		public DataSource<DepotEntity> Depot
		{
			get { return new DataSource<DepotEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceEntity instances in the database.</summary>
		public DataSource<DeviceEntity> Device
		{
			get { return new DataSource<DeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceBookingStateEntity instances in the database.</summary>
		public DataSource<DeviceBookingStateEntity> DeviceBookingState
		{
			get { return new DataSource<DeviceBookingStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceClassEntity instances in the database.</summary>
		public DataSource<DeviceClassEntity> DeviceClass
		{
			get { return new DataSource<DeviceClassEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DevicePaymentMethodEntity instances in the database.</summary>
		public DataSource<DevicePaymentMethodEntity> DevicePaymentMethod
		{
			get { return new DataSource<DevicePaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DirectionEntity instances in the database.</summary>
		public DataSource<DirectionEntity> Direction
		{
			get { return new DataSource<DirectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningLevelEntity instances in the database.</summary>
		public DataSource<DunningLevelEntity> DunningLevel
		{
			get { return new DataSource<DunningLevelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EticketEntity instances in the database.</summary>
		public DataSource<EticketEntity> Eticket
		{
			get { return new DataSource<EticketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExportInfoEntity instances in the database.</summary>
		public DataSource<ExportInfoEntity> ExportInfo
		{
			get { return new DataSource<ExportInfoEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalCardEntity instances in the database.</summary>
		public DataSource<ExternalCardEntity> ExternalCard
		{
			get { return new DataSource<ExternalCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalDataEntity instances in the database.</summary>
		public DataSource<ExternalDataEntity> ExternalData
		{
			get { return new DataSource<ExternalDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalEffortEntity instances in the database.</summary>
		public DataSource<ExternalEffortEntity> ExternalEffort
		{
			get { return new DataSource<ExternalEffortEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalPacketEntity instances in the database.</summary>
		public DataSource<ExternalPacketEntity> ExternalPacket
		{
			get { return new DataSource<ExternalPacketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalPacketEffortEntity instances in the database.</summary>
		public DataSource<ExternalPacketEffortEntity> ExternalPacketEffort
		{
			get { return new DataSource<ExternalPacketEffortEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalTypeEntity instances in the database.</summary>
		public DataSource<ExternalTypeEntity> ExternalType
		{
			get { return new DataSource<ExternalTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionCategoryEntity instances in the database.</summary>
		public DataSource<FareEvasionCategoryEntity> FareEvasionCategory
		{
			get { return new DataSource<FareEvasionCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionReasonEntity instances in the database.</summary>
		public DataSource<FareEvasionReasonEntity> FareEvasionReason
		{
			get { return new DataSource<FareEvasionReasonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionReasonToCategoryEntity instances in the database.</summary>
		public DataSource<FareEvasionReasonToCategoryEntity> FareEvasionReasonToCategory
		{
			get { return new DataSource<FareEvasionReasonToCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareMatrixEntity instances in the database.</summary>
		public DataSource<FareMatrixEntity> FareMatrix
		{
			get { return new DataSource<FareMatrixEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareMatrixEntryEntity instances in the database.</summary>
		public DataSource<FareMatrixEntryEntity> FareMatrixEntry
		{
			get { return new DataSource<FareMatrixEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareStageEntity instances in the database.</summary>
		public DataSource<FareStageEntity> FareStage
		{
			get { return new DataSource<FareStageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareStageAliasEntity instances in the database.</summary>
		public DataSource<FareStageAliasEntity> FareStageAlias
		{
			get { return new DataSource<FareStageAliasEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareStageHierarchieLevelEntity instances in the database.</summary>
		public DataSource<FareStageHierarchieLevelEntity> FareStageHierarchieLevel
		{
			get { return new DataSource<FareStageHierarchieLevelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareStageListEntity instances in the database.</summary>
		public DataSource<FareStageListEntity> FareStageList
		{
			get { return new DataSource<FareStageListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareStageTypeEntity instances in the database.</summary>
		public DataSource<FareStageTypeEntity> FareStageType
		{
			get { return new DataSource<FareStageTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareTableEntity instances in the database.</summary>
		public DataSource<FareTableEntity> FareTable
		{
			get { return new DataSource<FareTableEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareTableEntryEntity instances in the database.</summary>
		public DataSource<FareTableEntryEntity> FareTableEntry
		{
			get { return new DataSource<FareTableEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FixedBitmapLayoutObjectEntity instances in the database.</summary>
		public DataSource<FixedBitmapLayoutObjectEntity> FixedBitmapLayoutObject
		{
			get { return new DataSource<FixedBitmapLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FixedTextLayoutObjectEntity instances in the database.</summary>
		public DataSource<FixedTextLayoutObjectEntity> FixedTextLayoutObject
		{
			get { return new DataSource<FixedTextLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GuiDefEntity instances in the database.</summary>
		public DataSource<GuiDefEntity> GuiDef
		{
			get { return new DataSource<GuiDefEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting KeyAttributeTransfromEntity instances in the database.</summary>
		public DataSource<KeyAttributeTransfromEntity> KeyAttributeTransfrom
		{
			get { return new DataSource<KeyAttributeTransfromEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting KeySelectionModeEntity instances in the database.</summary>
		public DataSource<KeySelectionModeEntity> KeySelectionMode
		{
			get { return new DataSource<KeySelectionModeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting KVVSubscriptionEntity instances in the database.</summary>
		public DataSource<KVVSubscriptionEntity> KVVSubscription
		{
			get { return new DataSource<KVVSubscriptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LanguageEntity instances in the database.</summary>
		public DataSource<LanguageEntity> Language
		{
			get { return new DataSource<LanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LayoutEntity instances in the database.</summary>
		public DataSource<LayoutEntity> Layout
		{
			get { return new DataSource<LayoutEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LineEntity instances in the database.</summary>
		public DataSource<LineEntity> Line
		{
			get { return new DataSource<LineEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LineGroupEntity instances in the database.</summary>
		public DataSource<LineGroupEntity> LineGroup
		{
			get { return new DataSource<LineGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LineGroupToLineGroupEntity instances in the database.</summary>
		public DataSource<LineGroupToLineGroupEntity> LineGroupToLineGroup
		{
			get { return new DataSource<LineGroupToLineGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LineToLineGroupEntity instances in the database.</summary>
		public DataSource<LineToLineGroupEntity> LineToLineGroup
		{
			get { return new DataSource<LineToLineGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ListLayoutObjectEntity instances in the database.</summary>
		public DataSource<ListLayoutObjectEntity> ListLayoutObject
		{
			get { return new DataSource<ListLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ListLayoutTypeEntity instances in the database.</summary>
		public DataSource<ListLayoutTypeEntity> ListLayoutType
		{
			get { return new DataSource<ListLayoutTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LogoEntity instances in the database.</summary>
		public DataSource<LogoEntity> Logo
		{
			get { return new DataSource<LogoEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NetEntity instances in the database.</summary>
		public DataSource<NetEntity> Net
		{
			get { return new DataSource<NetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NumberRangeEntity instances in the database.</summary>
		public DataSource<NumberRangeEntity> NumberRange
		{
			get { return new DataSource<NumberRangeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OperatorEntity instances in the database.</summary>
		public DataSource<OperatorEntity> Operator
		{
			get { return new DataSource<OperatorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OutputDeviceEntity instances in the database.</summary>
		public DataSource<OutputDeviceEntity> OutputDevice
		{
			get { return new DataSource<OutputDeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageContentGroupEntity instances in the database.</summary>
		public DataSource<PageContentGroupEntity> PageContentGroup
		{
			get { return new DataSource<PageContentGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageContentGroupToContentEntity instances in the database.</summary>
		public DataSource<PageContentGroupToContentEntity> PageContentGroupToContent
		{
			get { return new DataSource<PageContentGroupToContentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PanelEntity instances in the database.</summary>
		public DataSource<PanelEntity> Panel
		{
			get { return new DataSource<PanelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterAttributeValueEntity instances in the database.</summary>
		public DataSource<ParameterAttributeValueEntity> ParameterAttributeValue
		{
			get { return new DataSource<ParameterAttributeValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterFareStageEntity instances in the database.</summary>
		public DataSource<ParameterFareStageEntity> ParameterFareStage
		{
			get { return new DataSource<ParameterFareStageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterTariffEntity instances in the database.</summary>
		public DataSource<ParameterTariffEntity> ParameterTariff
		{
			get { return new DataSource<ParameterTariffEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterTicketEntity instances in the database.</summary>
		public DataSource<ParameterTicketEntity> ParameterTicket
		{
			get { return new DataSource<ParameterTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentDetailEntity instances in the database.</summary>
		public DataSource<PaymentDetailEntity> PaymentDetail
		{
			get { return new DataSource<PaymentDetailEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentIntervalEntity instances in the database.</summary>
		public DataSource<PaymentIntervalEntity> PaymentInterval
		{
			get { return new DataSource<PaymentIntervalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PointOfSaleEntity instances in the database.</summary>
		public DataSource<PointOfSaleEntity> PointOfSale
		{
			get { return new DataSource<PointOfSaleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PredefinedKeyEntity instances in the database.</summary>
		public DataSource<PredefinedKeyEntity> PredefinedKey
		{
			get { return new DataSource<PredefinedKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceTypeEntity instances in the database.</summary>
		public DataSource<PriceTypeEntity> PriceType
		{
			get { return new DataSource<PriceTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrimalKeyEntity instances in the database.</summary>
		public DataSource<PrimalKeyEntity> PrimalKey
		{
			get { return new DataSource<PrimalKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrintTextEntity instances in the database.</summary>
		public DataSource<PrintTextEntity> PrintText
		{
			get { return new DataSource<PrintTextEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductOnCardEntity instances in the database.</summary>
		public DataSource<ProductOnCardEntity> ProductOnCard
		{
			get { return new DataSource<ProductOnCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProtocolEntity instances in the database.</summary>
		public DataSource<ProtocolEntity> Protocol
		{
			get { return new DataSource<ProtocolEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProtocolActionEntity instances in the database.</summary>
		public DataSource<ProtocolActionEntity> ProtocolAction
		{
			get { return new DataSource<ProtocolActionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProtocolFunctionEntity instances in the database.</summary>
		public DataSource<ProtocolFunctionEntity> ProtocolFunction
		{
			get { return new DataSource<ProtocolFunctionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProtocolFunctionGroupEntity instances in the database.</summary>
		public DataSource<ProtocolFunctionGroupEntity> ProtocolFunctionGroup
		{
			get { return new DataSource<ProtocolFunctionGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProtocolMessageEntity instances in the database.</summary>
		public DataSource<ProtocolMessageEntity> ProtocolMessage
		{
			get { return new DataSource<ProtocolMessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting QualificationEntity instances in the database.</summary>
		public DataSource<QualificationEntity> Qualification
		{
			get { return new DataSource<QualificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ResponsibilityEntity instances in the database.</summary>
		public DataSource<ResponsibilityEntity> Responsibility
		{
			get { return new DataSource<ResponsibilityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RevenueTransactionTypeEntity instances in the database.</summary>
		public DataSource<RevenueTransactionTypeEntity> RevenueTransactionType
		{
			get { return new DataSource<RevenueTransactionTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RmPaymentEntity instances in the database.</summary>
		public DataSource<RmPaymentEntity> RmPayment
		{
			get { return new DataSource<RmPaymentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RouteEntity instances in the database.</summary>
		public DataSource<RouteEntity> Route
		{
			get { return new DataSource<RouteEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RouteNameEntity instances in the database.</summary>
		public DataSource<RouteNameEntity> RouteName
		{
			get { return new DataSource<RouteNameEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleCappingEntity instances in the database.</summary>
		public DataSource<RuleCappingEntity> RuleCapping
		{
			get { return new DataSource<RuleCappingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleCappingToFtEntryEntity instances in the database.</summary>
		public DataSource<RuleCappingToFtEntryEntity> RuleCappingToFtEntry
		{
			get { return new DataSource<RuleCappingToFtEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleCappingToTicketEntity instances in the database.</summary>
		public DataSource<RuleCappingToTicketEntity> RuleCappingToTicket
		{
			get { return new DataSource<RuleCappingToTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RulePeriodEntity instances in the database.</summary>
		public DataSource<RulePeriodEntity> RulePeriod
		{
			get { return new DataSource<RulePeriodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleTypeEntity instances in the database.</summary>
		public DataSource<RuleTypeEntity> RuleType
		{
			get { return new DataSource<RuleTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SalutationEntity instances in the database.</summary>
		public DataSource<SalutationEntity> Salutation
		{
			get { return new DataSource<SalutationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServiceAllocationEntity instances in the database.</summary>
		public DataSource<ServiceAllocationEntity> ServiceAllocation
		{
			get { return new DataSource<ServiceAllocationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServiceIdToCardEntity instances in the database.</summary>
		public DataSource<ServiceIdToCardEntity> ServiceIdToCard
		{
			get { return new DataSource<ServiceIdToCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServicePercentageEntity instances in the database.</summary>
		public DataSource<ServicePercentageEntity> ServicePercentage
		{
			get { return new DataSource<ServicePercentageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ShiftEntity instances in the database.</summary>
		public DataSource<ShiftEntity> Shift
		{
			get { return new DataSource<ShiftEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ShiftInventoryEntity instances in the database.</summary>
		public DataSource<ShiftInventoryEntity> ShiftInventory
		{
			get { return new DataSource<ShiftInventoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ShiftStateEntity instances in the database.</summary>
		public DataSource<ShiftStateEntity> ShiftState
		{
			get { return new DataSource<ShiftStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ShortDistanceEntity instances in the database.</summary>
		public DataSource<ShortDistanceEntity> ShortDistance
		{
			get { return new DataSource<ShortDistanceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SpecialReceiptEntity instances in the database.</summary>
		public DataSource<SpecialReceiptEntity> SpecialReceipt
		{
			get { return new DataSource<SpecialReceiptEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StopEntity instances in the database.</summary>
		public DataSource<StopEntity> Stop
		{
			get { return new DataSource<StopEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SystemFieldEntity instances in the database.</summary>
		public DataSource<SystemFieldEntity> SystemField
		{
			get { return new DataSource<SystemFieldEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SystemFieldBarcodeLayoutObjectEntity instances in the database.</summary>
		public DataSource<SystemFieldBarcodeLayoutObjectEntity> SystemFieldBarcodeLayoutObject
		{
			get { return new DataSource<SystemFieldBarcodeLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SystemFieldDynamicGraphicLayoutObjectEntity instances in the database.</summary>
		public DataSource<SystemFieldDynamicGraphicLayoutObjectEntity> SystemFieldDynamicGraphicLayoutObject
		{
			get { return new DataSource<SystemFieldDynamicGraphicLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SystemFieldTextLayoutObjectEntity instances in the database.</summary>
		public DataSource<SystemFieldTextLayoutObjectEntity> SystemFieldTextLayoutObject
		{
			get { return new DataSource<SystemFieldTextLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SystemTextEntity instances in the database.</summary>
		public DataSource<SystemTextEntity> SystemText
		{
			get { return new DataSource<SystemTextEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TariffEntity instances in the database.</summary>
		public DataSource<TariffEntity> Tariff
		{
			get { return new DataSource<TariffEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TariffParameterEntity instances in the database.</summary>
		public DataSource<TariffParameterEntity> TariffParameter
		{
			get { return new DataSource<TariffParameterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TariffReleaseEntity instances in the database.</summary>
		public DataSource<TariffReleaseEntity> TariffRelease
		{
			get { return new DataSource<TariffReleaseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TemporalTypeEntity instances in the database.</summary>
		public DataSource<TemporalTypeEntity> TemporalType
		{
			get { return new DataSource<TemporalTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketEntity instances in the database.</summary>
		public DataSource<TicketEntity> Ticket
		{
			get { return new DataSource<TicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketCancellationTypeEntity instances in the database.</summary>
		public DataSource<TicketCancellationTypeEntity> TicketCancellationType
		{
			get { return new DataSource<TicketCancellationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketCategoryEntity instances in the database.</summary>
		public DataSource<TicketCategoryEntity> TicketCategory
		{
			get { return new DataSource<TicketCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketDayTypeEntity instances in the database.</summary>
		public DataSource<TicketDayTypeEntity> TicketDayType
		{
			get { return new DataSource<TicketDayTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketDeviceClassEntity instances in the database.</summary>
		public DataSource<TicketDeviceClassEntity> TicketDeviceClass
		{
			get { return new DataSource<TicketDeviceClassEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketDeviceClassOutputDeviceEntity instances in the database.</summary>
		public DataSource<TicketDeviceClassOutputDeviceEntity> TicketDeviceClassOutputDevice
		{
			get { return new DataSource<TicketDeviceClassOutputDeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketDeviceClassPaymentMethodEntity instances in the database.</summary>
		public DataSource<TicketDeviceClassPaymentMethodEntity> TicketDeviceClassPaymentMethod
		{
			get { return new DataSource<TicketDeviceClassPaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketDevicePaymentMethodEntity instances in the database.</summary>
		public DataSource<TicketDevicePaymentMethodEntity> TicketDevicePaymentMethod
		{
			get { return new DataSource<TicketDevicePaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketGroupEntity instances in the database.</summary>
		public DataSource<TicketGroupEntity> TicketGroup
		{
			get { return new DataSource<TicketGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketOrganizationEntity instances in the database.</summary>
		public DataSource<TicketOrganizationEntity> TicketOrganization
		{
			get { return new DataSource<TicketOrganizationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketOutputdeviceEntity instances in the database.</summary>
		public DataSource<TicketOutputdeviceEntity> TicketOutputdevice
		{
			get { return new DataSource<TicketOutputdeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketPaymentIntervalEntity instances in the database.</summary>
		public DataSource<TicketPaymentIntervalEntity> TicketPaymentInterval
		{
			get { return new DataSource<TicketPaymentIntervalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketPhysicalCardTypeEntity instances in the database.</summary>
		public DataSource<TicketPhysicalCardTypeEntity> TicketPhysicalCardType
		{
			get { return new DataSource<TicketPhysicalCardTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketSelectionModeEntity instances in the database.</summary>
		public DataSource<TicketSelectionModeEntity> TicketSelectionMode
		{
			get { return new DataSource<TicketSelectionModeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketServicesPermittedEntity instances in the database.</summary>
		public DataSource<TicketServicesPermittedEntity> TicketServicesPermitted
		{
			get { return new DataSource<TicketServicesPermittedEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketToGroupEntity instances in the database.</summary>
		public DataSource<TicketToGroupEntity> TicketToGroup
		{
			get { return new DataSource<TicketToGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketTypeEntity instances in the database.</summary>
		public DataSource<TicketTypeEntity> TicketType
		{
			get { return new DataSource<TicketTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketVendingClientEntity instances in the database.</summary>
		public DataSource<TicketVendingClientEntity> TicketVendingClient
		{
			get { return new DataSource<TicketVendingClientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionEntity instances in the database.</summary>
		public DataSource<TransactionEntity> Transaction
		{
			get { return new DataSource<TransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionAdditionalEntity instances in the database.</summary>
		public DataSource<TransactionAdditionalEntity> TransactionAdditional
		{
			get { return new DataSource<TransactionAdditionalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionBackupEntity instances in the database.</summary>
		public DataSource<TransactionBackupEntity> TransactionBackup
		{
			get { return new DataSource<TransactionBackupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionExtensionEntity instances in the database.</summary>
		public DataSource<TransactionExtensionEntity> TransactionExtension
		{
			get { return new DataSource<TransactionExtensionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TranslationEntity instances in the database.</summary>
		public DataSource<TranslationEntity> Translation
		{
			get { return new DataSource<TranslationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TypeOfCardEntity instances in the database.</summary>
		public DataSource<TypeOfCardEntity> TypeOfCard
		{
			get { return new DataSource<TypeOfCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TypeOfUnitEntity instances in the database.</summary>
		public DataSource<TypeOfUnitEntity> TypeOfUnit
		{
			get { return new DataSource<TypeOfUnitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UnitEntity instances in the database.</summary>
		public DataSource<UnitEntity> Unit
		{
			get { return new DataSource<UnitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserConfigEntity instances in the database.</summary>
		public DataSource<UserConfigEntity> UserConfig
		{
			get { return new DataSource<UserConfigEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserGroupEntity instances in the database.</summary>
		public DataSource<UserGroupEntity> UserGroup
		{
			get { return new DataSource<UserGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserGroupRightEntity instances in the database.</summary>
		public DataSource<UserGroupRightEntity> UserGroupRight
		{
			get { return new DataSource<UserGroupRightEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserIsMemberEntity instances in the database.</summary>
		public DataSource<UserIsMemberEntity> UserIsMember
		{
			get { return new DataSource<UserIsMemberEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserKeyEntity instances in the database.</summary>
		public DataSource<UserKeyEntity> UserKey
		{
			get { return new DataSource<UserKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserLanguageEntity instances in the database.</summary>
		public DataSource<UserLanguageEntity> UserLanguage
		{
			get { return new DataSource<UserLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserListEntity instances in the database.</summary>
		public DataSource<UserListEntity> UserList
		{
			get { return new DataSource<UserListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserResourceEntity instances in the database.</summary>
		public DataSource<UserResourceEntity> UserResource
		{
			get { return new DataSource<UserResourceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserStatusEntity instances in the database.</summary>
		public DataSource<UserStatusEntity> UserStatus
		{
			get { return new DataSource<UserStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserToWorkstationEntity instances in the database.</summary>
		public DataSource<UserToWorkstationEntity> UserToWorkstation
		{
			get { return new DataSource<UserToWorkstationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationTransactionEntity instances in the database.</summary>
		public DataSource<ValidationTransactionEntity> ValidationTransaction
		{
			get { return new DataSource<ValidationTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VarioAddressEntity instances in the database.</summary>
		public DataSource<VarioAddressEntity> VarioAddress
		{
			get { return new DataSource<VarioAddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VarioSettlementEntity instances in the database.</summary>
		public DataSource<VarioSettlementEntity> VarioSettlement
		{
			get { return new DataSource<VarioSettlementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VarioTypeOfSettlementEntity instances in the database.</summary>
		public DataSource<VarioTypeOfSettlementEntity> VarioTypeOfSettlement
		{
			get { return new DataSource<VarioTypeOfSettlementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvKeyInfoEntity instances in the database.</summary>
		public DataSource<VdvKeyInfoEntity> VdvKeyInfo
		{
			get { return new DataSource<VdvKeyInfoEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvKeySetEntity instances in the database.</summary>
		public DataSource<VdvKeySetEntity> VdvKeySet
		{
			get { return new DataSource<VdvKeySetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvLayoutEntity instances in the database.</summary>
		public DataSource<VdvLayoutEntity> VdvLayout
		{
			get { return new DataSource<VdvLayoutEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvLayoutObjectEntity instances in the database.</summary>
		public DataSource<VdvLayoutObjectEntity> VdvLayoutObject
		{
			get { return new DataSource<VdvLayoutObjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvLoadKeyMessageEntity instances in the database.</summary>
		public DataSource<VdvLoadKeyMessageEntity> VdvLoadKeyMessage
		{
			get { return new DataSource<VdvLoadKeyMessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvProductEntity instances in the database.</summary>
		public DataSource<VdvProductEntity> VdvProduct
		{
			get { return new DataSource<VdvProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvSamStatusEntity instances in the database.</summary>
		public DataSource<VdvSamStatusEntity> VdvSamStatus
		{
			get { return new DataSource<VdvSamStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvTagEntity instances in the database.</summary>
		public DataSource<VdvTagEntity> VdvTag
		{
			get { return new DataSource<VdvTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvTerminalEntity instances in the database.</summary>
		public DataSource<VdvTerminalEntity> VdvTerminal
		{
			get { return new DataSource<VdvTerminalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvTypeEntity instances in the database.</summary>
		public DataSource<VdvTypeEntity> VdvType
		{
			get { return new DataSource<VdvTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkstationEntity instances in the database.</summary>
		public DataSource<WorkstationEntity> Workstation
		{
			get { return new DataSource<WorkstationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingCancelRecTapCmlSettledEntity instances in the database.</summary>
		public DataSource<AccountingCancelRecTapCmlSettledEntity> AccountingCancelRecTapCmlSettled
		{
			get { return new DataSource<AccountingCancelRecTapCmlSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingCancelRecTapTabSettledEntity instances in the database.</summary>
		public DataSource<AccountingCancelRecTapTabSettledEntity> AccountingCancelRecTapTabSettled
		{
			get { return new DataSource<AccountingCancelRecTapTabSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingMethodEntity instances in the database.</summary>
		public DataSource<AccountingMethodEntity> AccountingMethod
		{
			get { return new DataSource<AccountingMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingModeEntity instances in the database.</summary>
		public DataSource<AccountingModeEntity> AccountingMode
		{
			get { return new DataSource<AccountingModeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecLoadUseCmlSettledEntity instances in the database.</summary>
		public DataSource<AccountingRecLoadUseCmlSettledEntity> AccountingRecLoadUseCmlSettled
		{
			get { return new DataSource<AccountingRecLoadUseCmlSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecLoadUseTabSettledEntity instances in the database.</summary>
		public DataSource<AccountingRecLoadUseTabSettledEntity> AccountingRecLoadUseTabSettled
		{
			get { return new DataSource<AccountingRecLoadUseTabSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedLoadAndUseEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedLoadAndUseEntity> AccountingRecognizedLoadAndUse
		{
			get { return new DataSource<AccountingRecognizedLoadAndUseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedPaymentEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedPaymentEntity> AccountingRecognizedPayment
		{
			get { return new DataSource<AccountingRecognizedPaymentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedTapEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedTapEntity> AccountingRecognizedTap
		{
			get { return new DataSource<AccountingRecognizedTapEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedTapCancellationEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedTapCancellationEntity> AccountingRecognizedTapCancellation
		{
			get { return new DataSource<AccountingRecognizedTapCancellationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedTapCmlSettledEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedTapCmlSettledEntity> AccountingRecognizedTapCmlSettled
		{
			get { return new DataSource<AccountingRecognizedTapCmlSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingRecognizedTapTabSettledEntity instances in the database.</summary>
		public DataSource<AccountingRecognizedTapTabSettledEntity> AccountingRecognizedTapTabSettled
		{
			get { return new DataSource<AccountingRecognizedTapTabSettledEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountingReconciledPaymentEntity instances in the database.</summary>
		public DataSource<AccountingReconciledPaymentEntity> AccountingReconciledPayment
		{
			get { return new DataSource<AccountingReconciledPaymentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountMessageSettingEntity instances in the database.</summary>
		public DataSource<AccountMessageSettingEntity> AccountMessageSetting
		{
			get { return new DataSource<AccountMessageSettingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdditionalDataEntity instances in the database.</summary>
		public DataSource<AdditionalDataEntity> AdditionalData
		{
			get { return new DataSource<AdditionalDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AddressEntity instances in the database.</summary>
		public DataSource<AddressEntity> Address
		{
			get { return new DataSource<AddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AddressBookEntryEntity instances in the database.</summary>
		public DataSource<AddressBookEntryEntity> AddressBookEntry
		{
			get { return new DataSource<AddressBookEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AddressBookEntryTypeEntity instances in the database.</summary>
		public DataSource<AddressBookEntryTypeEntity> AddressBookEntryType
		{
			get { return new DataSource<AddressBookEntryTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AddressTypeEntity instances in the database.</summary>
		public DataSource<AddressTypeEntity> AddressType
		{
			get { return new DataSource<AddressTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AggregationFunctionEntity instances in the database.</summary>
		public DataSource<AggregationFunctionEntity> AggregationFunction
		{
			get { return new DataSource<AggregationFunctionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AggregationTypeEntity instances in the database.</summary>
		public DataSource<AggregationTypeEntity> AggregationType
		{
			get { return new DataSource<AggregationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApportionEntity instances in the database.</summary>
		public DataSource<ApportionEntity> Apportion
		{
			get { return new DataSource<ApportionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApportionPassRevenueRecognitionEntity instances in the database.</summary>
		public DataSource<ApportionPassRevenueRecognitionEntity> ApportionPassRevenueRecognition
		{
			get { return new DataSource<ApportionPassRevenueRecognitionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ArchiveStateEntity instances in the database.</summary>
		public DataSource<ArchiveStateEntity> ArchiveState
		{
			get { return new DataSource<ArchiveStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttributeToMobilityProviderEntity instances in the database.</summary>
		public DataSource<AttributeToMobilityProviderEntity> AttributeToMobilityProvider
		{
			get { return new DataSource<AttributeToMobilityProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AuditRegisterEntity instances in the database.</summary>
		public DataSource<AuditRegisterEntity> AuditRegister
		{
			get { return new DataSource<AuditRegisterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AuditRegisterValueEntity instances in the database.</summary>
		public DataSource<AuditRegisterValueEntity> AuditRegisterValue
		{
			get { return new DataSource<AuditRegisterValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AuditRegisterValueTypeEntity instances in the database.</summary>
		public DataSource<AuditRegisterValueTypeEntity> AuditRegisterValueType
		{
			get { return new DataSource<AuditRegisterValueTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AutoloadSettingEntity instances in the database.</summary>
		public DataSource<AutoloadSettingEntity> AutoloadSetting
		{
			get { return new DataSource<AutoloadSettingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AutoloadToPaymentOptionEntity instances in the database.</summary>
		public DataSource<AutoloadToPaymentOptionEntity> AutoloadToPaymentOption
		{
			get { return new DataSource<AutoloadToPaymentOptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BadCardEntity instances in the database.</summary>
		public DataSource<BadCardEntity> BadCard
		{
			get { return new DataSource<BadCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BadCheckEntity instances in the database.</summary>
		public DataSource<BadCheckEntity> BadCheck
		{
			get { return new DataSource<BadCheckEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankConnectionDataEntity instances in the database.</summary>
		public DataSource<BankConnectionDataEntity> BankConnectionData
		{
			get { return new DataSource<BankConnectionDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankStatementEntity instances in the database.</summary>
		public DataSource<BankStatementEntity> BankStatement
		{
			get { return new DataSource<BankStatementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankStatementRecordTypeEntity instances in the database.</summary>
		public DataSource<BankStatementRecordTypeEntity> BankStatementRecordType
		{
			get { return new DataSource<BankStatementRecordTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankStatementStateEntity instances in the database.</summary>
		public DataSource<BankStatementStateEntity> BankStatementState
		{
			get { return new DataSource<BankStatementStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BankStatementVerificationEntity instances in the database.</summary>
		public DataSource<BankStatementVerificationEntity> BankStatementVerification
		{
			get { return new DataSource<BankStatementVerificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BookingEntity instances in the database.</summary>
		public DataSource<BookingEntity> Booking
		{
			get { return new DataSource<BookingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BookingItemEntity instances in the database.</summary>
		public DataSource<BookingItemEntity> BookingItem
		{
			get { return new DataSource<BookingItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BookingItemPaymentEntity instances in the database.</summary>
		public DataSource<BookingItemPaymentEntity> BookingItemPayment
		{
			get { return new DataSource<BookingItemPaymentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BookingItemPaymentTypeEntity instances in the database.</summary>
		public DataSource<BookingItemPaymentTypeEntity> BookingItemPaymentType
		{
			get { return new DataSource<BookingItemPaymentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BookingTypeEntity instances in the database.</summary>
		public DataSource<BookingTypeEntity> BookingType
		{
			get { return new DataSource<BookingTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CappingJournalEntity instances in the database.</summary>
		public DataSource<CappingJournalEntity> CappingJournal
		{
			get { return new DataSource<CappingJournalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CappingStateEntity instances in the database.</summary>
		public DataSource<CappingStateEntity> CappingState
		{
			get { return new DataSource<CappingStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardEntity instances in the database.</summary>
		public DataSource<CardEntity> Card
		{
			get { return new DataSource<CardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardBodyTypeEntity instances in the database.</summary>
		public DataSource<CardBodyTypeEntity> CardBodyType
		{
			get { return new DataSource<CardBodyTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardDormancyEntity instances in the database.</summary>
		public DataSource<CardDormancyEntity> CardDormancy
		{
			get { return new DataSource<CardDormancyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardEventEntity instances in the database.</summary>
		public DataSource<CardEventEntity> CardEvent
		{
			get { return new DataSource<CardEventEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardEventTypeEntity instances in the database.</summary>
		public DataSource<CardEventTypeEntity> CardEventType
		{
			get { return new DataSource<CardEventTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardFulfillmentStatusEntity instances in the database.</summary>
		public DataSource<CardFulfillmentStatusEntity> CardFulfillmentStatus
		{
			get { return new DataSource<CardFulfillmentStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardHolderEntity instances in the database.</summary>
		public DataSource<CardHolderEntity> CardHolder
		{
			get { return new DataSource<CardHolderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardHolderOrganizationEntity instances in the database.</summary>
		public DataSource<CardHolderOrganizationEntity> CardHolderOrganization
		{
			get { return new DataSource<CardHolderOrganizationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardKeyEntity instances in the database.</summary>
		public DataSource<CardKeyEntity> CardKey
		{
			get { return new DataSource<CardKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardLiabilityEntity instances in the database.</summary>
		public DataSource<CardLiabilityEntity> CardLiability
		{
			get { return new DataSource<CardLiabilityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardLinkEntity instances in the database.</summary>
		public DataSource<CardLinkEntity> CardLink
		{
			get { return new DataSource<CardLinkEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardOrderDetailEntity instances in the database.</summary>
		public DataSource<CardOrderDetailEntity> CardOrderDetail
		{
			get { return new DataSource<CardOrderDetailEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardPhysicalDetailEntity instances in the database.</summary>
		public DataSource<CardPhysicalDetailEntity> CardPhysicalDetail
		{
			get { return new DataSource<CardPhysicalDetailEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardPrintingTypeEntity instances in the database.</summary>
		public DataSource<CardPrintingTypeEntity> CardPrintingType
		{
			get { return new DataSource<CardPrintingTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardStockTransferEntity instances in the database.</summary>
		public DataSource<CardStockTransferEntity> CardStockTransfer
		{
			get { return new DataSource<CardStockTransferEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardToContractEntity instances in the database.</summary>
		public DataSource<CardToContractEntity> CardToContract
		{
			get { return new DataSource<CardToContractEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardToRuleViolationEntity instances in the database.</summary>
		public DataSource<CardToRuleViolationEntity> CardToRuleViolation
		{
			get { return new DataSource<CardToRuleViolationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CardWorkItemEntity instances in the database.</summary>
		public DataSource<CardWorkItemEntity> CardWorkItem
		{
			get { return new DataSource<CardWorkItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CertificateEntity instances in the database.</summary>
		public DataSource<CertificateEntity> Certificate
		{
			get { return new DataSource<CertificateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CertificateFormatEntity instances in the database.</summary>
		public DataSource<CertificateFormatEntity> CertificateFormat
		{
			get { return new DataSource<CertificateFormatEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CertificatePurposeEntity instances in the database.</summary>
		public DataSource<CertificatePurposeEntity> CertificatePurpose
		{
			get { return new DataSource<CertificatePurposeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CertificateTypeEntity instances in the database.</summary>
		public DataSource<CertificateTypeEntity> CertificateType
		{
			get { return new DataSource<CertificateTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClaimEntity instances in the database.</summary>
		public DataSource<ClaimEntity> Claim
		{
			get { return new DataSource<ClaimEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientFilterTypeEntity instances in the database.</summary>
		public DataSource<ClientFilterTypeEntity> ClientFilterType
		{
			get { return new DataSource<ClientFilterTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloseoutPeriodEntity instances in the database.</summary>
		public DataSource<CloseoutPeriodEntity> CloseoutPeriod
		{
			get { return new DataSource<CloseoutPeriodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloseoutStateEntity instances in the database.</summary>
		public DataSource<CloseoutStateEntity> CloseoutState
		{
			get { return new DataSource<CloseoutStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloseoutTypeEntity instances in the database.</summary>
		public DataSource<CloseoutTypeEntity> CloseoutType
		{
			get { return new DataSource<CloseoutTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CommentEntity instances in the database.</summary>
		public DataSource<CommentEntity> Comment
		{
			get { return new DataSource<CommentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CommentPriorityEntity instances in the database.</summary>
		public DataSource<CommentPriorityEntity> CommentPriority
		{
			get { return new DataSource<CommentPriorityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CommentTypeEntity instances in the database.</summary>
		public DataSource<CommentTypeEntity> CommentType
		{
			get { return new DataSource<CommentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConfigurationEntity instances in the database.</summary>
		public DataSource<ConfigurationEntity> Configuration
		{
			get { return new DataSource<ConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConfigurationDataTypeEntity instances in the database.</summary>
		public DataSource<ConfigurationDataTypeEntity> ConfigurationDataType
		{
			get { return new DataSource<ConfigurationDataTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConfigurationDefinitionEntity instances in the database.</summary>
		public DataSource<ConfigurationDefinitionEntity> ConfigurationDefinition
		{
			get { return new DataSource<ConfigurationDefinitionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConfigurationOverwriteEntity instances in the database.</summary>
		public DataSource<ConfigurationOverwriteEntity> ConfigurationOverwrite
		{
			get { return new DataSource<ConfigurationOverwriteEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContentItemEntity instances in the database.</summary>
		public DataSource<ContentItemEntity> ContentItem
		{
			get { return new DataSource<ContentItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContentTypeEntity instances in the database.</summary>
		public DataSource<ContentTypeEntity> ContentType
		{
			get { return new DataSource<ContentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractEntity instances in the database.</summary>
		public DataSource<ContractEntity> Contract
		{
			get { return new DataSource<ContractEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractAddressEntity instances in the database.</summary>
		public DataSource<ContractAddressEntity> ContractAddress
		{
			get { return new DataSource<ContractAddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractDetailsEntity instances in the database.</summary>
		public DataSource<ContractDetailsEntity> ContractDetails
		{
			get { return new DataSource<ContractDetailsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractHistoryEntity instances in the database.</summary>
		public DataSource<ContractHistoryEntity> ContractHistory
		{
			get { return new DataSource<ContractHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractLinkEntity instances in the database.</summary>
		public DataSource<ContractLinkEntity> ContractLink
		{
			get { return new DataSource<ContractLinkEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractStateEntity instances in the database.</summary>
		public DataSource<ContractStateEntity> ContractState
		{
			get { return new DataSource<ContractStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractTerminationEntity instances in the database.</summary>
		public DataSource<ContractTerminationEntity> ContractTermination
		{
			get { return new DataSource<ContractTerminationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractTerminationTypeEntity instances in the database.</summary>
		public DataSource<ContractTerminationTypeEntity> ContractTerminationType
		{
			get { return new DataSource<ContractTerminationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractToPaymentMethodEntity instances in the database.</summary>
		public DataSource<ContractToPaymentMethodEntity> ContractToPaymentMethod
		{
			get { return new DataSource<ContractToPaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractToProviderEntity instances in the database.</summary>
		public DataSource<ContractToProviderEntity> ContractToProvider
		{
			get { return new DataSource<ContractToProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractTypeEntity instances in the database.</summary>
		public DataSource<ContractTypeEntity> ContractType
		{
			get { return new DataSource<ContractTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ContractWorkItemEntity instances in the database.</summary>
		public DataSource<ContractWorkItemEntity> ContractWorkItem
		{
			get { return new DataSource<ContractWorkItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CoordinateEntity instances in the database.</summary>
		public DataSource<CoordinateEntity> Coordinate
		{
			get { return new DataSource<CoordinateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CountryEntity instances in the database.</summary>
		public DataSource<CountryEntity> Country
		{
			get { return new DataSource<CountryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditCardAuthorizationEntity instances in the database.</summary>
		public DataSource<CreditCardAuthorizationEntity> CreditCardAuthorization
		{
			get { return new DataSource<CreditCardAuthorizationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditCardAuthorizationLogEntity instances in the database.</summary>
		public DataSource<CreditCardAuthorizationLogEntity> CreditCardAuthorizationLog
		{
			get { return new DataSource<CreditCardAuthorizationLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditCardMerchantEntity instances in the database.</summary>
		public DataSource<CreditCardMerchantEntity> CreditCardMerchant
		{
			get { return new DataSource<CreditCardMerchantEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditCardNetworkEntity instances in the database.</summary>
		public DataSource<CreditCardNetworkEntity> CreditCardNetwork
		{
			get { return new DataSource<CreditCardNetworkEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CreditCardTerminalEntity instances in the database.</summary>
		public DataSource<CreditCardTerminalEntity> CreditCardTerminal
		{
			get { return new DataSource<CreditCardTerminalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoCertificateEntity instances in the database.</summary>
		public DataSource<CryptoCertificateEntity> CryptoCertificate
		{
			get { return new DataSource<CryptoCertificateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoContentEntity instances in the database.</summary>
		public DataSource<CryptoContentEntity> CryptoContent
		{
			get { return new DataSource<CryptoContentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoCryptogramArchiveEntity instances in the database.</summary>
		public DataSource<CryptoCryptogramArchiveEntity> CryptoCryptogramArchive
		{
			get { return new DataSource<CryptoCryptogramArchiveEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptogramCertificateEntity instances in the database.</summary>
		public DataSource<CryptogramCertificateEntity> CryptogramCertificate
		{
			get { return new DataSource<CryptogramCertificateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptogramCertificateTypeEntity instances in the database.</summary>
		public DataSource<CryptogramCertificateTypeEntity> CryptogramCertificateType
		{
			get { return new DataSource<CryptogramCertificateTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptogramErrorTypeEntity instances in the database.</summary>
		public DataSource<CryptogramErrorTypeEntity> CryptogramErrorType
		{
			get { return new DataSource<CryptogramErrorTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptogramLoadStatusEntity instances in the database.</summary>
		public DataSource<CryptogramLoadStatusEntity> CryptogramLoadStatus
		{
			get { return new DataSource<CryptogramLoadStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoOperatorActivationkeyEntity instances in the database.</summary>
		public DataSource<CryptoOperatorActivationkeyEntity> CryptoOperatorActivationkey
		{
			get { return new DataSource<CryptoOperatorActivationkeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoQArchiveEntity instances in the database.</summary>
		public DataSource<CryptoQArchiveEntity> CryptoQArchive
		{
			get { return new DataSource<CryptoQArchiveEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoResourceEntity instances in the database.</summary>
		public DataSource<CryptoResourceEntity> CryptoResource
		{
			get { return new DataSource<CryptoResourceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoResourceDataEntity instances in the database.</summary>
		public DataSource<CryptoResourceDataEntity> CryptoResourceData
		{
			get { return new DataSource<CryptoResourceDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoSamSignCertificateEntity instances in the database.</summary>
		public DataSource<CryptoSamSignCertificateEntity> CryptoSamSignCertificate
		{
			get { return new DataSource<CryptoSamSignCertificateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CryptoUicCertificateEntity instances in the database.</summary>
		public DataSource<CryptoUicCertificateEntity> CryptoUicCertificate
		{
			get { return new DataSource<CryptoUicCertificateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomAttributeEntity instances in the database.</summary>
		public DataSource<CustomAttributeEntity> CustomAttribute
		{
			get { return new DataSource<CustomAttributeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomAttributeValueEntity instances in the database.</summary>
		public DataSource<CustomAttributeValueEntity> CustomAttributeValue
		{
			get { return new DataSource<CustomAttributeValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomAttributeValueToPersonEntity instances in the database.</summary>
		public DataSource<CustomAttributeValueToPersonEntity> CustomAttributeValueToPerson
		{
			get { return new DataSource<CustomAttributeValueToPersonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerAccountEntity instances in the database.</summary>
		public DataSource<CustomerAccountEntity> CustomerAccount
		{
			get { return new DataSource<CustomerAccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerAccountNotificationEntity instances in the database.</summary>
		public DataSource<CustomerAccountNotificationEntity> CustomerAccountNotification
		{
			get { return new DataSource<CustomerAccountNotificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerAccountRoleEntity instances in the database.</summary>
		public DataSource<CustomerAccountRoleEntity> CustomerAccountRole
		{
			get { return new DataSource<CustomerAccountRoleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerAccountStateEntity instances in the database.</summary>
		public DataSource<CustomerAccountStateEntity> CustomerAccountState
		{
			get { return new DataSource<CustomerAccountStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerAccountToVerificationAttemptTypeEntity instances in the database.</summary>
		public DataSource<CustomerAccountToVerificationAttemptTypeEntity> CustomerAccountToVerificationAttemptType
		{
			get { return new DataSource<CustomerAccountToVerificationAttemptTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerLanguageEntity instances in the database.</summary>
		public DataSource<CustomerLanguageEntity> CustomerLanguage
		{
			get { return new DataSource<CustomerLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceCommunicationHistoryEntity instances in the database.</summary>
		public DataSource<DeviceCommunicationHistoryEntity> DeviceCommunicationHistory
		{
			get { return new DataSource<DeviceCommunicationHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderEntity instances in the database.</summary>
		public DataSource<DeviceReaderEntity> DeviceReader
		{
			get { return new DataSource<DeviceReaderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderCryptoResourceEntity instances in the database.</summary>
		public DataSource<DeviceReaderCryptoResourceEntity> DeviceReaderCryptoResource
		{
			get { return new DataSource<DeviceReaderCryptoResourceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderJobEntity instances in the database.</summary>
		public DataSource<DeviceReaderJobEntity> DeviceReaderJob
		{
			get { return new DataSource<DeviceReaderJobEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderKeyEntity instances in the database.</summary>
		public DataSource<DeviceReaderKeyEntity> DeviceReaderKey
		{
			get { return new DataSource<DeviceReaderKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderKeyToCertEntity instances in the database.</summary>
		public DataSource<DeviceReaderKeyToCertEntity> DeviceReaderKeyToCert
		{
			get { return new DataSource<DeviceReaderKeyToCertEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceReaderToCertEntity instances in the database.</summary>
		public DataSource<DeviceReaderToCertEntity> DeviceReaderToCert
		{
			get { return new DataSource<DeviceReaderToCertEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceTokenEntity instances in the database.</summary>
		public DataSource<DeviceTokenEntity> DeviceToken
		{
			get { return new DataSource<DeviceTokenEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceTokenStateEntity instances in the database.</summary>
		public DataSource<DeviceTokenStateEntity> DeviceTokenState
		{
			get { return new DataSource<DeviceTokenStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DocumentHistoryEntity instances in the database.</summary>
		public DataSource<DocumentHistoryEntity> DocumentHistory
		{
			get { return new DataSource<DocumentHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DocumentTypeEntity instances in the database.</summary>
		public DataSource<DocumentTypeEntity> DocumentType
		{
			get { return new DataSource<DocumentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DormancyNotificationEntity instances in the database.</summary>
		public DataSource<DormancyNotificationEntity> DormancyNotification
		{
			get { return new DataSource<DormancyNotificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningActionEntity instances in the database.</summary>
		public DataSource<DunningActionEntity> DunningAction
		{
			get { return new DataSource<DunningActionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningLevelConfigurationEntity instances in the database.</summary>
		public DataSource<DunningLevelConfigurationEntity> DunningLevelConfiguration
		{
			get { return new DataSource<DunningLevelConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningLevelPostingKeyEntity instances in the database.</summary>
		public DataSource<DunningLevelPostingKeyEntity> DunningLevelPostingKey
		{
			get { return new DataSource<DunningLevelPostingKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningProcessEntity instances in the database.</summary>
		public DataSource<DunningProcessEntity> DunningProcess
		{
			get { return new DataSource<DunningProcessEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningToInvoiceEntity instances in the database.</summary>
		public DataSource<DunningToInvoiceEntity> DunningToInvoice
		{
			get { return new DataSource<DunningToInvoiceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningToPostingEntity instances in the database.</summary>
		public DataSource<DunningToPostingEntity> DunningToPosting
		{
			get { return new DataSource<DunningToPostingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DunningToProductEntity instances in the database.</summary>
		public DataSource<DunningToProductEntity> DunningToProduct
		{
			get { return new DataSource<DunningToProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EmailEventEntity instances in the database.</summary>
		public DataSource<EmailEventEntity> EmailEvent
		{
			get { return new DataSource<EmailEventEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EmailEventResendLockEntity instances in the database.</summary>
		public DataSource<EmailEventResendLockEntity> EmailEventResendLock
		{
			get { return new DataSource<EmailEventResendLockEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EmailMessageEntity instances in the database.</summary>
		public DataSource<EmailMessageEntity> EmailMessage
		{
			get { return new DataSource<EmailMessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntitlementEntity instances in the database.</summary>
		public DataSource<EntitlementEntity> Entitlement
		{
			get { return new DataSource<EntitlementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntitlementToProductEntity instances in the database.</summary>
		public DataSource<EntitlementToProductEntity> EntitlementToProduct
		{
			get { return new DataSource<EntitlementToProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntitlementTypeEntity instances in the database.</summary>
		public DataSource<EntitlementTypeEntity> EntitlementType
		{
			get { return new DataSource<EntitlementTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EventSettingEntity instances in the database.</summary>
		public DataSource<EventSettingEntity> EventSetting
		{
			get { return new DataSource<EventSettingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareChangeCauseEntity instances in the database.</summary>
		public DataSource<FareChangeCauseEntity> FareChangeCause
		{
			get { return new DataSource<FareChangeCauseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionBehaviourEntity instances in the database.</summary>
		public DataSource<FareEvasionBehaviourEntity> FareEvasionBehaviour
		{
			get { return new DataSource<FareEvasionBehaviourEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionIncidentEntity instances in the database.</summary>
		public DataSource<FareEvasionIncidentEntity> FareEvasionIncident
		{
			get { return new DataSource<FareEvasionIncidentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionInspectionEntity instances in the database.</summary>
		public DataSource<FareEvasionInspectionEntity> FareEvasionInspection
		{
			get { return new DataSource<FareEvasionInspectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FareEvasionStateEntity instances in the database.</summary>
		public DataSource<FareEvasionStateEntity> FareEvasionState
		{
			get { return new DataSource<FareEvasionStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FarePaymentErrorEntity instances in the database.</summary>
		public DataSource<FarePaymentErrorEntity> FarePaymentError
		{
			get { return new DataSource<FarePaymentErrorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FarePaymentResultTypeEntity instances in the database.</summary>
		public DataSource<FarePaymentResultTypeEntity> FarePaymentResultType
		{
			get { return new DataSource<FarePaymentResultTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FastcardEntity instances in the database.</summary>
		public DataSource<FastcardEntity> Fastcard
		{
			get { return new DataSource<FastcardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterCategoryEntity instances in the database.</summary>
		public DataSource<FilterCategoryEntity> FilterCategory
		{
			get { return new DataSource<FilterCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterElementEntity instances in the database.</summary>
		public DataSource<FilterElementEntity> FilterElement
		{
			get { return new DataSource<FilterElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterListEntity instances in the database.</summary>
		public DataSource<FilterListEntity> FilterList
		{
			get { return new DataSource<FilterListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterListElementEntity instances in the database.</summary>
		public DataSource<FilterListElementEntity> FilterListElement
		{
			get { return new DataSource<FilterListElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterTypeEntity instances in the database.</summary>
		public DataSource<FilterTypeEntity> FilterType
		{
			get { return new DataSource<FilterTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterValueEntity instances in the database.</summary>
		public DataSource<FilterValueEntity> FilterValue
		{
			get { return new DataSource<FilterValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FilterValueSetEntity instances in the database.</summary>
		public DataSource<FilterValueSetEntity> FilterValueSet
		{
			get { return new DataSource<FilterValueSetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FlexValueEntity instances in the database.</summary>
		public DataSource<FlexValueEntity> FlexValue
		{
			get { return new DataSource<FlexValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FormEntity instances in the database.</summary>
		public DataSource<FormEntity> Form
		{
			get { return new DataSource<FormEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FormTemplateEntity instances in the database.</summary>
		public DataSource<FormTemplateEntity> FormTemplate
		{
			get { return new DataSource<FormTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FrequencyEntity instances in the database.</summary>
		public DataSource<FrequencyEntity> Frequency
		{
			get { return new DataSource<FrequencyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenderEntity instances in the database.</summary>
		public DataSource<GenderEntity> Gender
		{
			get { return new DataSource<GenderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting IdentificationTypeEntity instances in the database.</summary>
		public DataSource<IdentificationTypeEntity> IdentificationType
		{
			get { return new DataSource<IdentificationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InspectionCriterionEntity instances in the database.</summary>
		public DataSource<InspectionCriterionEntity> InspectionCriterion
		{
			get { return new DataSource<InspectionCriterionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InspectionReportEntity instances in the database.</summary>
		public DataSource<InspectionReportEntity> InspectionReport
		{
			get { return new DataSource<InspectionReportEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InspectionResultEntity instances in the database.</summary>
		public DataSource<InspectionResultEntity> InspectionResult
		{
			get { return new DataSource<InspectionResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InventoryStateEntity instances in the database.</summary>
		public DataSource<InventoryStateEntity> InventoryState
		{
			get { return new DataSource<InventoryStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoiceEntity instances in the database.</summary>
		public DataSource<InvoiceEntity> Invoice
		{
			get { return new DataSource<InvoiceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoiceEntryEntity instances in the database.</summary>
		public DataSource<InvoiceEntryEntity> InvoiceEntry
		{
			get { return new DataSource<InvoiceEntryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoiceTypeEntity instances in the database.</summary>
		public DataSource<InvoiceTypeEntity> InvoiceType
		{
			get { return new DataSource<InvoiceTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoicingEntity instances in the database.</summary>
		public DataSource<InvoicingEntity> Invoicing
		{
			get { return new DataSource<InvoicingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoicingFilterTypeEntity instances in the database.</summary>
		public DataSource<InvoicingFilterTypeEntity> InvoicingFilterType
		{
			get { return new DataSource<InvoicingFilterTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InvoicingTypeEntity instances in the database.</summary>
		public DataSource<InvoicingTypeEntity> InvoicingType
		{
			get { return new DataSource<InvoicingTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobEntity instances in the database.</summary>
		public DataSource<JobEntity> Job
		{
			get { return new DataSource<JobEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobBulkImportEntity instances in the database.</summary>
		public DataSource<JobBulkImportEntity> JobBulkImport
		{
			get { return new DataSource<JobBulkImportEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobCardImportEntity instances in the database.</summary>
		public DataSource<JobCardImportEntity> JobCardImport
		{
			get { return new DataSource<JobCardImportEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobFileTypeEntity instances in the database.</summary>
		public DataSource<JobFileTypeEntity> JobFileType
		{
			get { return new DataSource<JobFileTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobStateEntity instances in the database.</summary>
		public DataSource<JobStateEntity> JobState
		{
			get { return new DataSource<JobStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting JobTypeEntity instances in the database.</summary>
		public DataSource<JobTypeEntity> JobType
		{
			get { return new DataSource<JobTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LookupAddressEntity instances in the database.</summary>
		public DataSource<LookupAddressEntity> LookupAddress
		{
			get { return new DataSource<LookupAddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaEligibilityRequirementEntity instances in the database.</summary>
		public DataSource<MediaEligibilityRequirementEntity> MediaEligibilityRequirement
		{
			get { return new DataSource<MediaEligibilityRequirementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaSalePreconditionEntity instances in the database.</summary>
		public DataSource<MediaSalePreconditionEntity> MediaSalePrecondition
		{
			get { return new DataSource<MediaSalePreconditionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MerchantEntity instances in the database.</summary>
		public DataSource<MerchantEntity> Merchant
		{
			get { return new DataSource<MerchantEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageEntity instances in the database.</summary>
		public DataSource<MessageEntity> Message
		{
			get { return new DataSource<MessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageFormatEntity instances in the database.</summary>
		public DataSource<MessageFormatEntity> MessageFormat
		{
			get { return new DataSource<MessageFormatEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageTypeEntity instances in the database.</summary>
		public DataSource<MessageTypeEntity> MessageType
		{
			get { return new DataSource<MessageTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MobilityProviderEntity instances in the database.</summary>
		public DataSource<MobilityProviderEntity> MobilityProvider
		{
			get { return new DataSource<MobilityProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NotificationDeviceEntity instances in the database.</summary>
		public DataSource<NotificationDeviceEntity> NotificationDevice
		{
			get { return new DataSource<NotificationDeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NotificationMessageEntity instances in the database.</summary>
		public DataSource<NotificationMessageEntity> NotificationMessage
		{
			get { return new DataSource<NotificationMessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NotificationMessageOwnerEntity instances in the database.</summary>
		public DataSource<NotificationMessageOwnerEntity> NotificationMessageOwner
		{
			get { return new DataSource<NotificationMessageOwnerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NotificationMessageToCustomerEntity instances in the database.</summary>
		public DataSource<NotificationMessageToCustomerEntity> NotificationMessageToCustomer
		{
			get { return new DataSource<NotificationMessageToCustomerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NumberGroupEntity instances in the database.</summary>
		public DataSource<NumberGroupEntity> NumberGroup
		{
			get { return new DataSource<NumberGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NumberGroupRandomizedEntity instances in the database.</summary>
		public DataSource<NumberGroupRandomizedEntity> NumberGroupRandomized
		{
			get { return new DataSource<NumberGroupRandomizedEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NumberGroupScopeEntity instances in the database.</summary>
		public DataSource<NumberGroupScopeEntity> NumberGroupScope
		{
			get { return new DataSource<NumberGroupScopeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderEntity instances in the database.</summary>
		public DataSource<OrderEntity> Order
		{
			get { return new DataSource<OrderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderDetailEntity instances in the database.</summary>
		public DataSource<OrderDetailEntity> OrderDetail
		{
			get { return new DataSource<OrderDetailEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderDetailToCardEntity instances in the database.</summary>
		public DataSource<OrderDetailToCardEntity> OrderDetailToCard
		{
			get { return new DataSource<OrderDetailToCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderDetailToCardHolderEntity instances in the database.</summary>
		public DataSource<OrderDetailToCardHolderEntity> OrderDetailToCardHolder
		{
			get { return new DataSource<OrderDetailToCardHolderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderHistoryEntity instances in the database.</summary>
		public DataSource<OrderHistoryEntity> OrderHistory
		{
			get { return new DataSource<OrderHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderProcessingCleanupEntity instances in the database.</summary>
		public DataSource<OrderProcessingCleanupEntity> OrderProcessingCleanup
		{
			get { return new DataSource<OrderProcessingCleanupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderStateEntity instances in the database.</summary>
		public DataSource<OrderStateEntity> OrderState
		{
			get { return new DataSource<OrderStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderTypeEntity instances in the database.</summary>
		public DataSource<OrderTypeEntity> OrderType
		{
			get { return new DataSource<OrderTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderWithTotalEntity instances in the database.</summary>
		public DataSource<OrderWithTotalEntity> OrderWithTotal
		{
			get { return new DataSource<OrderWithTotalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderWorkItemEntity instances in the database.</summary>
		public DataSource<OrderWorkItemEntity> OrderWorkItem
		{
			get { return new DataSource<OrderWorkItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrganizationEntity instances in the database.</summary>
		public DataSource<OrganizationEntity> Organization
		{
			get { return new DataSource<OrganizationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrganizationAddressEntity instances in the database.</summary>
		public DataSource<OrganizationAddressEntity> OrganizationAddress
		{
			get { return new DataSource<OrganizationAddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrganizationParticipantEntity instances in the database.</summary>
		public DataSource<OrganizationParticipantEntity> OrganizationParticipant
		{
			get { return new DataSource<OrganizationParticipantEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrganizationSubtypeEntity instances in the database.</summary>
		public DataSource<OrganizationSubtypeEntity> OrganizationSubtype
		{
			get { return new DataSource<OrganizationSubtypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrganizationTypeEntity instances in the database.</summary>
		public DataSource<OrganizationTypeEntity> OrganizationType
		{
			get { return new DataSource<OrganizationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageContentEntity instances in the database.</summary>
		public DataSource<PageContentEntity> PageContent
		{
			get { return new DataSource<PageContentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterEntity instances in the database.</summary>
		public DataSource<ParameterEntity> Parameter
		{
			get { return new DataSource<ParameterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterArchiveEntity instances in the database.</summary>
		public DataSource<ParameterArchiveEntity> ParameterArchive
		{
			get { return new DataSource<ParameterArchiveEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterArchiveReleaseEntity instances in the database.</summary>
		public DataSource<ParameterArchiveReleaseEntity> ParameterArchiveRelease
		{
			get { return new DataSource<ParameterArchiveReleaseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterArchiveResultViewEntity instances in the database.</summary>
		public DataSource<ParameterArchiveResultViewEntity> ParameterArchiveResultView
		{
			get { return new DataSource<ParameterArchiveResultViewEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterGroupEntity instances in the database.</summary>
		public DataSource<ParameterGroupEntity> ParameterGroup
		{
			get { return new DataSource<ParameterGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterGroupToParameterEntity instances in the database.</summary>
		public DataSource<ParameterGroupToParameterEntity> ParameterGroupToParameter
		{
			get { return new DataSource<ParameterGroupToParameterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterTypeEntity instances in the database.</summary>
		public DataSource<ParameterTypeEntity> ParameterType
		{
			get { return new DataSource<ParameterTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParameterValueEntity instances in the database.</summary>
		public DataSource<ParameterValueEntity> ParameterValue
		{
			get { return new DataSource<ParameterValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParaTransAttributeEntity instances in the database.</summary>
		public DataSource<ParaTransAttributeEntity> ParaTransAttribute
		{
			get { return new DataSource<ParaTransAttributeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParaTransAttributeTypeEntity instances in the database.</summary>
		public DataSource<ParaTransAttributeTypeEntity> ParaTransAttributeType
		{
			get { return new DataSource<ParaTransAttributeTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParaTransJournalEntity instances in the database.</summary>
		public DataSource<ParaTransJournalEntity> ParaTransJournal
		{
			get { return new DataSource<ParaTransJournalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParaTransJournalTypeEntity instances in the database.</summary>
		public DataSource<ParaTransJournalTypeEntity> ParaTransJournalType
		{
			get { return new DataSource<ParaTransJournalTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParaTransPaymentTypeEntity instances in the database.</summary>
		public DataSource<ParaTransPaymentTypeEntity> ParaTransPaymentType
		{
			get { return new DataSource<ParaTransPaymentTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ParticipantGroupEntity instances in the database.</summary>
		public DataSource<ParticipantGroupEntity> ParticipantGroup
		{
			get { return new DataSource<ParticipantGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PassExpiryNotificationEntity instances in the database.</summary>
		public DataSource<PassExpiryNotificationEntity> PassExpiryNotification
		{
			get { return new DataSource<PassExpiryNotificationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentEntity instances in the database.</summary>
		public DataSource<PaymentEntity> Payment
		{
			get { return new DataSource<PaymentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentJournalEntity instances in the database.</summary>
		public DataSource<PaymentJournalEntity> PaymentJournal
		{
			get { return new DataSource<PaymentJournalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentJournalToComponentEntity instances in the database.</summary>
		public DataSource<PaymentJournalToComponentEntity> PaymentJournalToComponent
		{
			get { return new DataSource<PaymentJournalToComponentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentMethodEntity instances in the database.</summary>
		public DataSource<PaymentMethodEntity> PaymentMethod
		{
			get { return new DataSource<PaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentModalityEntity instances in the database.</summary>
		public DataSource<PaymentModalityEntity> PaymentModality
		{
			get { return new DataSource<PaymentModalityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentOptionEntity instances in the database.</summary>
		public DataSource<PaymentOptionEntity> PaymentOption
		{
			get { return new DataSource<PaymentOptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentProviderEntity instances in the database.</summary>
		public DataSource<PaymentProviderEntity> PaymentProvider
		{
			get { return new DataSource<PaymentProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentProviderAccountEntity instances in the database.</summary>
		public DataSource<PaymentProviderAccountEntity> PaymentProviderAccount
		{
			get { return new DataSource<PaymentProviderAccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentRecognitionEntity instances in the database.</summary>
		public DataSource<PaymentRecognitionEntity> PaymentRecognition
		{
			get { return new DataSource<PaymentRecognitionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentReconciliationEntity instances in the database.</summary>
		public DataSource<PaymentReconciliationEntity> PaymentReconciliation
		{
			get { return new DataSource<PaymentReconciliationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentReconciliationStateEntity instances in the database.</summary>
		public DataSource<PaymentReconciliationStateEntity> PaymentReconciliationState
		{
			get { return new DataSource<PaymentReconciliationStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentStateEntity instances in the database.</summary>
		public DataSource<PaymentStateEntity> PaymentState
		{
			get { return new DataSource<PaymentStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PendingOrderEntity instances in the database.</summary>
		public DataSource<PendingOrderEntity> PendingOrder
		{
			get { return new DataSource<PendingOrderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PerformanceEntity instances in the database.</summary>
		public DataSource<PerformanceEntity> Performance
		{
			get { return new DataSource<PerformanceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PerformanceAggregationEntity instances in the database.</summary>
		public DataSource<PerformanceAggregationEntity> PerformanceAggregation
		{
			get { return new DataSource<PerformanceAggregationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PerformanceComponentEntity instances in the database.</summary>
		public DataSource<PerformanceComponentEntity> PerformanceComponent
		{
			get { return new DataSource<PerformanceComponentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PerformanceIndicatorEntity instances in the database.</summary>
		public DataSource<PerformanceIndicatorEntity> PerformanceIndicator
		{
			get { return new DataSource<PerformanceIndicatorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PersonEntity instances in the database.</summary>
		public DataSource<PersonEntity> Person
		{
			get { return new DataSource<PersonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PersonAddressEntity instances in the database.</summary>
		public DataSource<PersonAddressEntity> PersonAddress
		{
			get { return new DataSource<PersonAddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PhotographEntity instances in the database.</summary>
		public DataSource<PhotographEntity> Photograph
		{
			get { return new DataSource<PhotographEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PickupLocationEntity instances in the database.</summary>
		public DataSource<PickupLocationEntity> PickupLocation
		{
			get { return new DataSource<PickupLocationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PickupLocationToTicketEntity instances in the database.</summary>
		public DataSource<PickupLocationToTicketEntity> PickupLocationToTicket
		{
			get { return new DataSource<PickupLocationToTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PostingEntity instances in the database.</summary>
		public DataSource<PostingEntity> Posting
		{
			get { return new DataSource<PostingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PostingKeyEntity instances in the database.</summary>
		public DataSource<PostingKeyEntity> PostingKey
		{
			get { return new DataSource<PostingKeyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PostingKeyCategoryEntity instances in the database.</summary>
		public DataSource<PostingKeyCategoryEntity> PostingKeyCategory
		{
			get { return new DataSource<PostingKeyCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PostingScopeEntity instances in the database.</summary>
		public DataSource<PostingScopeEntity> PostingScope
		{
			get { return new DataSource<PostingScopeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceAdjustmentEntity instances in the database.</summary>
		public DataSource<PriceAdjustmentEntity> PriceAdjustment
		{
			get { return new DataSource<PriceAdjustmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceAdjustmentTemplateEntity instances in the database.</summary>
		public DataSource<PriceAdjustmentTemplateEntity> PriceAdjustmentTemplate
		{
			get { return new DataSource<PriceAdjustmentTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrinterEntity instances in the database.</summary>
		public DataSource<PrinterEntity> Printer
		{
			get { return new DataSource<PrinterEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrinterStateEntity instances in the database.</summary>
		public DataSource<PrinterStateEntity> PrinterState
		{
			get { return new DataSource<PrinterStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrinterToUnitEntity instances in the database.</summary>
		public DataSource<PrinterToUnitEntity> PrinterToUnit
		{
			get { return new DataSource<PrinterToUnitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PrinterTypeEntity instances in the database.</summary>
		public DataSource<PrinterTypeEntity> PrinterType
		{
			get { return new DataSource<PrinterTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriorityEntity instances in the database.</summary>
		public DataSource<PriorityEntity> Priority
		{
			get { return new DataSource<PriorityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductEntity instances in the database.</summary>
		public DataSource<ProductEntity> Product
		{
			get { return new DataSource<ProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductRelationEntity instances in the database.</summary>
		public DataSource<ProductRelationEntity> ProductRelation
		{
			get { return new DataSource<ProductRelationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductSubsidyEntity instances in the database.</summary>
		public DataSource<ProductSubsidyEntity> ProductSubsidy
		{
			get { return new DataSource<ProductSubsidyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductTerminationEntity instances in the database.</summary>
		public DataSource<ProductTerminationEntity> ProductTermination
		{
			get { return new DataSource<ProductTerminationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductTerminationTypeEntity instances in the database.</summary>
		public DataSource<ProductTerminationTypeEntity> ProductTerminationType
		{
			get { return new DataSource<ProductTerminationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductTypeEntity instances in the database.</summary>
		public DataSource<ProductTypeEntity> ProductType
		{
			get { return new DataSource<ProductTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PromoCodeEntity instances in the database.</summary>
		public DataSource<PromoCodeEntity> PromoCode
		{
			get { return new DataSource<PromoCodeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PromoCodeStatusEntity instances in the database.</summary>
		public DataSource<PromoCodeStatusEntity> PromoCodeStatus
		{
			get { return new DataSource<PromoCodeStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProviderAccountStateEntity instances in the database.</summary>
		public DataSource<ProviderAccountStateEntity> ProviderAccountState
		{
			get { return new DataSource<ProviderAccountStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProvisioningStateEntity instances in the database.</summary>
		public DataSource<ProvisioningStateEntity> ProvisioningState
		{
			get { return new DataSource<ProvisioningStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RecipientGroupEntity instances in the database.</summary>
		public DataSource<RecipientGroupEntity> RecipientGroup
		{
			get { return new DataSource<RecipientGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RecipientGroupMemberEntity instances in the database.</summary>
		public DataSource<RecipientGroupMemberEntity> RecipientGroupMember
		{
			get { return new DataSource<RecipientGroupMemberEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RefundEntity instances in the database.</summary>
		public DataSource<RefundEntity> Refund
		{
			get { return new DataSource<RefundEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RefundReasonEntity instances in the database.</summary>
		public DataSource<RefundReasonEntity> RefundReason
		{
			get { return new DataSource<RefundReasonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RentalStateEntity instances in the database.</summary>
		public DataSource<RentalStateEntity> RentalState
		{
			get { return new DataSource<RentalStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RentalTypeEntity instances in the database.</summary>
		public DataSource<RentalTypeEntity> RentalType
		{
			get { return new DataSource<RentalTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportEntity instances in the database.</summary>
		public DataSource<ReportEntity> Report
		{
			get { return new DataSource<ReportEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportCategoryEntity instances in the database.</summary>
		public DataSource<ReportCategoryEntity> ReportCategory
		{
			get { return new DataSource<ReportCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportDataFileEntity instances in the database.</summary>
		public DataSource<ReportDataFileEntity> ReportDataFile
		{
			get { return new DataSource<ReportDataFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportJobEntity instances in the database.</summary>
		public DataSource<ReportJobEntity> ReportJob
		{
			get { return new DataSource<ReportJobEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportJobResultEntity instances in the database.</summary>
		public DataSource<ReportJobResultEntity> ReportJobResult
		{
			get { return new DataSource<ReportJobResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportToClientEntity instances in the database.</summary>
		public DataSource<ReportToClientEntity> ReportToClient
		{
			get { return new DataSource<ReportToClientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RevenueRecognitionEntity instances in the database.</summary>
		public DataSource<RevenueRecognitionEntity> RevenueRecognition
		{
			get { return new DataSource<RevenueRecognitionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RevenueSettlementEntity instances in the database.</summary>
		public DataSource<RevenueSettlementEntity> RevenueSettlement
		{
			get { return new DataSource<RevenueSettlementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleViolationEntity instances in the database.</summary>
		public DataSource<RuleViolationEntity> RuleViolation
		{
			get { return new DataSource<RuleViolationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleViolationProviderEntity instances in the database.</summary>
		public DataSource<RuleViolationProviderEntity> RuleViolationProvider
		{
			get { return new DataSource<RuleViolationProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleViolationSourceTypeEntity instances in the database.</summary>
		public DataSource<RuleViolationSourceTypeEntity> RuleViolationSourceType
		{
			get { return new DataSource<RuleViolationSourceTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleViolationToTransactionEntity instances in the database.</summary>
		public DataSource<RuleViolationToTransactionEntity> RuleViolationToTransaction
		{
			get { return new DataSource<RuleViolationToTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RuleViolationTypeEntity instances in the database.</summary>
		public DataSource<RuleViolationTypeEntity> RuleViolationType
		{
			get { return new DataSource<RuleViolationTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SaleEntity instances in the database.</summary>
		public DataSource<SaleEntity> Sale
		{
			get { return new DataSource<SaleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SalesChannelToPaymentMethodEntity instances in the database.</summary>
		public DataSource<SalesChannelToPaymentMethodEntity> SalesChannelToPaymentMethod
		{
			get { return new DataSource<SalesChannelToPaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SalesRevenueEntity instances in the database.</summary>
		public DataSource<SalesRevenueEntity> SalesRevenue
		{
			get { return new DataSource<SalesRevenueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SaleToComponentEntity instances in the database.</summary>
		public DataSource<SaleToComponentEntity> SaleToComponent
		{
			get { return new DataSource<SaleToComponentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SaleTypeEntity instances in the database.</summary>
		public DataSource<SaleTypeEntity> SaleType
		{
			get { return new DataSource<SaleTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SamModuleEntity instances in the database.</summary>
		public DataSource<SamModuleEntity> SamModule
		{
			get { return new DataSource<SamModuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SamModuleStatusEntity instances in the database.</summary>
		public DataSource<SamModuleStatusEntity> SamModuleStatus
		{
			get { return new DataSource<SamModuleStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SchoolYearEntity instances in the database.</summary>
		public DataSource<SchoolYearEntity> SchoolYear
		{
			get { return new DataSource<SchoolYearEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SecurityQuestionEntity instances in the database.</summary>
		public DataSource<SecurityQuestionEntity> SecurityQuestion
		{
			get { return new DataSource<SecurityQuestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SecurityResponseEntity instances in the database.</summary>
		public DataSource<SecurityResponseEntity> SecurityResponse
		{
			get { return new DataSource<SecurityResponseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SepaFrequencyTypeEntity instances in the database.</summary>
		public DataSource<SepaFrequencyTypeEntity> SepaFrequencyType
		{
			get { return new DataSource<SepaFrequencyTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SepaOwnerTypeEntity instances in the database.</summary>
		public DataSource<SepaOwnerTypeEntity> SepaOwnerType
		{
			get { return new DataSource<SepaOwnerTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServerResponseTimeDataEntity instances in the database.</summary>
		public DataSource<ServerResponseTimeDataEntity> ServerResponseTimeData
		{
			get { return new DataSource<ServerResponseTimeDataEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServerResponseTimeDataAggregationEntity instances in the database.</summary>
		public DataSource<ServerResponseTimeDataAggregationEntity> ServerResponseTimeDataAggregation
		{
			get { return new DataSource<ServerResponseTimeDataAggregationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettledRevenueEntity instances in the database.</summary>
		public DataSource<SettledRevenueEntity> SettledRevenue
		{
			get { return new DataSource<SettledRevenueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementAccountEntity instances in the database.</summary>
		public DataSource<SettlementAccountEntity> SettlementAccount
		{
			get { return new DataSource<SettlementAccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementCalendarEntity instances in the database.</summary>
		public DataSource<SettlementCalendarEntity> SettlementCalendar
		{
			get { return new DataSource<SettlementCalendarEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementDistributionPolicyEntity instances in the database.</summary>
		public DataSource<SettlementDistributionPolicyEntity> SettlementDistributionPolicy
		{
			get { return new DataSource<SettlementDistributionPolicyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementHolidayEntity instances in the database.</summary>
		public DataSource<SettlementHolidayEntity> SettlementHoliday
		{
			get { return new DataSource<SettlementHolidayEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementOperationEntity instances in the database.</summary>
		public DataSource<SettlementOperationEntity> SettlementOperation
		{
			get { return new DataSource<SettlementOperationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementQuerySettingEntity instances in the database.</summary>
		public DataSource<SettlementQuerySettingEntity> SettlementQuerySetting
		{
			get { return new DataSource<SettlementQuerySettingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementQuerySettingToTicketEntity instances in the database.</summary>
		public DataSource<SettlementQuerySettingToTicketEntity> SettlementQuerySettingToTicket
		{
			get { return new DataSource<SettlementQuerySettingToTicketEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementQueryValueEntity instances in the database.</summary>
		public DataSource<SettlementQueryValueEntity> SettlementQueryValue
		{
			get { return new DataSource<SettlementQueryValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementReleaseStateEntity instances in the database.</summary>
		public DataSource<SettlementReleaseStateEntity> SettlementReleaseState
		{
			get { return new DataSource<SettlementReleaseStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementResultEntity instances in the database.</summary>
		public DataSource<SettlementResultEntity> SettlementResult
		{
			get { return new DataSource<SettlementResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementRunEntity instances in the database.</summary>
		public DataSource<SettlementRunEntity> SettlementRun
		{
			get { return new DataSource<SettlementRunEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementRunValueEntity instances in the database.</summary>
		public DataSource<SettlementRunValueEntity> SettlementRunValue
		{
			get { return new DataSource<SettlementRunValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementRunValueToVarioSettlementEntity instances in the database.</summary>
		public DataSource<SettlementRunValueToVarioSettlementEntity> SettlementRunValueToVarioSettlement
		{
			get { return new DataSource<SettlementRunValueToVarioSettlementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementSetupEntity instances in the database.</summary>
		public DataSource<SettlementSetupEntity> SettlementSetup
		{
			get { return new DataSource<SettlementSetupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SettlementTypeEntity instances in the database.</summary>
		public DataSource<SettlementTypeEntity> SettlementType
		{
			get { return new DataSource<SettlementTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SourceEntity instances in the database.</summary>
		public DataSource<SourceEntity> Source
		{
			get { return new DataSource<SourceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StatementOfAccountEntity instances in the database.</summary>
		public DataSource<StatementOfAccountEntity> StatementOfAccount
		{
			get { return new DataSource<StatementOfAccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StateOfCountryEntity instances in the database.</summary>
		public DataSource<StateOfCountryEntity> StateOfCountry
		{
			get { return new DataSource<StateOfCountryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StockTransferEntity instances in the database.</summary>
		public DataSource<StockTransferEntity> StockTransfer
		{
			get { return new DataSource<StockTransferEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StorageLocationEntity instances in the database.</summary>
		public DataSource<StorageLocationEntity> StorageLocation
		{
			get { return new DataSource<StorageLocationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SubsidyLimitEntity instances in the database.</summary>
		public DataSource<SubsidyLimitEntity> SubsidyLimit
		{
			get { return new DataSource<SubsidyLimitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SubsidyTransactionEntity instances in the database.</summary>
		public DataSource<SubsidyTransactionEntity> SubsidyTransaction
		{
			get { return new DataSource<SubsidyTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyEntity instances in the database.</summary>
		public DataSource<SurveyEntity> Survey
		{
			get { return new DataSource<SurveyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyAnswerEntity instances in the database.</summary>
		public DataSource<SurveyAnswerEntity> SurveyAnswer
		{
			get { return new DataSource<SurveyAnswerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyChoiceEntity instances in the database.</summary>
		public DataSource<SurveyChoiceEntity> SurveyChoice
		{
			get { return new DataSource<SurveyChoiceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyDescriptionEntity instances in the database.</summary>
		public DataSource<SurveyDescriptionEntity> SurveyDescription
		{
			get { return new DataSource<SurveyDescriptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyQuestionEntity instances in the database.</summary>
		public DataSource<SurveyQuestionEntity> SurveyQuestion
		{
			get { return new DataSource<SurveyQuestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyResponseEntity instances in the database.</summary>
		public DataSource<SurveyResponseEntity> SurveyResponse
		{
			get { return new DataSource<SurveyResponseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TenantEntity instances in the database.</summary>
		public DataSource<TenantEntity> Tenant
		{
			get { return new DataSource<TenantEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TenantHistoryEntity instances in the database.</summary>
		public DataSource<TenantHistoryEntity> TenantHistory
		{
			get { return new DataSource<TenantHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TenantPersonEntity instances in the database.</summary>
		public DataSource<TenantPersonEntity> TenantPerson
		{
			get { return new DataSource<TenantPersonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TenantStateEntity instances in the database.</summary>
		public DataSource<TenantStateEntity> TenantState
		{
			get { return new DataSource<TenantStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminationStatusEntity instances in the database.</summary>
		public DataSource<TerminationStatusEntity> TerminationStatus
		{
			get { return new DataSource<TerminationStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketAssignmentEntity instances in the database.</summary>
		public DataSource<TicketAssignmentEntity> TicketAssignment
		{
			get { return new DataSource<TicketAssignmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketAssignmentStatusEntity instances in the database.</summary>
		public DataSource<TicketAssignmentStatusEntity> TicketAssignmentStatus
		{
			get { return new DataSource<TicketAssignmentStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketSerialNumberEntity instances in the database.</summary>
		public DataSource<TicketSerialNumberEntity> TicketSerialNumber
		{
			get { return new DataSource<TicketSerialNumberEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TicketSerialNumberStatusEntity instances in the database.</summary>
		public DataSource<TicketSerialNumberStatusEntity> TicketSerialNumberStatus
		{
			get { return new DataSource<TicketSerialNumberStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionJournalEntity instances in the database.</summary>
		public DataSource<TransactionJournalEntity> TransactionJournal
		{
			get { return new DataSource<TransactionJournalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionJournalCorrectionEntity instances in the database.</summary>
		public DataSource<TransactionJournalCorrectionEntity> TransactionJournalCorrection
		{
			get { return new DataSource<TransactionJournalCorrectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionJourneyHistoryEntity instances in the database.</summary>
		public DataSource<TransactionJourneyHistoryEntity> TransactionJourneyHistory
		{
			get { return new DataSource<TransactionJourneyHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionToInspectionEntity instances in the database.</summary>
		public DataSource<TransactionToInspectionEntity> TransactionToInspection
		{
			get { return new DataSource<TransactionToInspectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransactionTypeEntity instances in the database.</summary>
		public DataSource<TransactionTypeEntity> TransactionType
		{
			get { return new DataSource<TransactionTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TransportCompanyEntity instances in the database.</summary>
		public DataSource<TransportCompanyEntity> TransportCompany
		{
			get { return new DataSource<TransportCompanyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TriggerTypeEntity instances in the database.</summary>
		public DataSource<TriggerTypeEntity> TriggerType
		{
			get { return new DataSource<TriggerTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UnitCollectionEntity instances in the database.</summary>
		public DataSource<UnitCollectionEntity> UnitCollection
		{
			get { return new DataSource<UnitCollectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UnitCollectionToUnitEntity instances in the database.</summary>
		public DataSource<UnitCollectionToUnitEntity> UnitCollectionToUnit
		{
			get { return new DataSource<UnitCollectionToUnitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationResultEntity instances in the database.</summary>
		public DataSource<ValidationResultEntity> ValidationResult
		{
			get { return new DataSource<ValidationResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationResultResolveTypeEntity instances in the database.</summary>
		public DataSource<ValidationResultResolveTypeEntity> ValidationResultResolveType
		{
			get { return new DataSource<ValidationResultResolveTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationRuleEntity instances in the database.</summary>
		public DataSource<ValidationRuleEntity> ValidationRule
		{
			get { return new DataSource<ValidationRuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationRunEntity instances in the database.</summary>
		public DataSource<ValidationRunEntity> ValidationRun
		{
			get { return new DataSource<ValidationRunEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationRunRuleEntity instances in the database.</summary>
		public DataSource<ValidationRunRuleEntity> ValidationRunRule
		{
			get { return new DataSource<ValidationRunRuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationStatusEntity instances in the database.</summary>
		public DataSource<ValidationStatusEntity> ValidationStatus
		{
			get { return new DataSource<ValidationStatusEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ValidationValueEntity instances in the database.</summary>
		public DataSource<ValidationValueEntity> ValidationValue
		{
			get { return new DataSource<ValidationValueEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VdvTransactionEntity instances in the database.</summary>
		public DataSource<VdvTransactionEntity> VdvTransaction
		{
			get { return new DataSource<VdvTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VerificationAttemptEntity instances in the database.</summary>
		public DataSource<VerificationAttemptEntity> VerificationAttempt
		{
			get { return new DataSource<VerificationAttemptEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VerificationAttemptTypeEntity instances in the database.</summary>
		public DataSource<VerificationAttemptTypeEntity> VerificationAttemptType
		{
			get { return new DataSource<VerificationAttemptTypeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VirtualCardEntity instances in the database.</summary>
		public DataSource<VirtualCardEntity> VirtualCard
		{
			get { return new DataSource<VirtualCardEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VoucherEntity instances in the database.</summary>
		public DataSource<VoucherEntity> Voucher
		{
			get { return new DataSource<VoucherEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WhitelistEntity instances in the database.</summary>
		public DataSource<WhitelistEntity> Whitelist
		{
			get { return new DataSource<WhitelistEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WhitelistJournalEntity instances in the database.</summary>
		public DataSource<WhitelistJournalEntity> WhitelistJournal
		{
			get { return new DataSource<WhitelistJournalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemEntity instances in the database.</summary>
		public DataSource<WorkItemEntity> WorkItem
		{
			get { return new DataSource<WorkItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemCategoryEntity instances in the database.</summary>
		public DataSource<WorkItemCategoryEntity> WorkItemCategory
		{
			get { return new DataSource<WorkItemCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemHistoryEntity instances in the database.</summary>
		public DataSource<WorkItemHistoryEntity> WorkItemHistory
		{
			get { return new DataSource<WorkItemHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemStateEntity instances in the database.</summary>
		public DataSource<WorkItemStateEntity> WorkItemState
		{
			get { return new DataSource<WorkItemStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemSubjectEntity instances in the database.</summary>
		public DataSource<WorkItemSubjectEntity> WorkItemSubject
		{
			get { return new DataSource<WorkItemSubjectEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WorkItemViewEntity instances in the database.</summary>
		public DataSource<WorkItemViewEntity> WorkItemView
		{
			get { return new DataSource<WorkItemViewEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
 
		#region Class Property Declarations
		/// <summary> Gets / sets the ITransaction to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public ITransaction TransactionToUse
		{
			get { return _transactionToUse;}
			set { _transactionToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}