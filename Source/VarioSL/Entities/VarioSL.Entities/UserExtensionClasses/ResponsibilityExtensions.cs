﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Linq
{
	public static class ResponsibilityExtensions
	{
		public static long[] GetClients(this IQueryable<ResponsibilityEntity> data, DataResponsibility scope, long clientID)
		{
			return data.Where(r => r.DataTypeID == scope)
				.Where(r => r.ResponsibleClientID == clientID)
				.Select(r => r.ClientID)
				.ToArray();
		}

		public static long[] GetResponsibleClients(this IQueryable<ResponsibilityEntity> data, DataResponsibility scope, long clientID)
		{
			return data.Where(r => r.DataTypeID == scope)
				.Where(r => r.ClientID == clientID)
				.Select(r => r.ResponsibleClientID)
				.Distinct()
				.ToArray();
		}
	}
}
