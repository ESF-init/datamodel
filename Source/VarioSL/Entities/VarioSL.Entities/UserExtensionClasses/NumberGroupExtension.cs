﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.HelperClasses;

namespace VarioSL.Entities.Linq
{
	public static class NumberGroupExtension
	{
		public static string Generate(this NumberGroupEntity numberGroup, NumberGroupScope scope)
		{
			numberGroup.FetchUsingUCScope(scope);
			if (numberGroup.IsNew)
			{
				throw new InvalidOperationException(string.Format("Number group '{0}' is not defined.", scope));
			}

			return numberGroup.Generate();
		}

		public static string Generate(this NumberGroupEntity numberGroup)
		{
			if (numberGroup.IsNew)
				throw new ArgumentException("Unknown number group or scope!", nameof(numberGroup));

			numberGroup.NextValue();
			return numberGroup.FormatValue();
		}

		internal static string FormatValue(this NumberGroupEntity numberGroup)
		{
			// Get all sections of the number group to replace, like [yyyyMM] or [0000]
			var numberGroupSections = Regex.Matches(numberGroup.Format, @"\[[yMdhHms0]*\]");
			StringBuilder format = new StringBuilder(numberGroup.Format);
			List<object> args = new List<object>(numberGroupSections.Count);

			for (int i = 0; i < numberGroupSections.Count; i++)
			{
				// Replace the [ and ] brackets from a section
				string sectionWithoutBrackets = Regex.Replace(numberGroupSections[i].Value, @"[\[\]]", string.Empty);
				format.Replace(numberGroupSections[i].Value, string.Format("{{{0}:{1}}}", i, sectionWithoutBrackets));

				// Distinct between date (everything containing yMdhHms) or formatted number group value
				if (Regex.IsMatch(numberGroupSections[i].Value, @"\[[yMdhHms]*\]"))
					args.Add(DateTime.Now);
				else
					args.Add(numberGroup.CurrentValue);
			}

			return string.Format(format.ToString(), args.ToArray());
		}

		private static void NextValue(this NumberGroupEntity numberGroup)
		{
			var transHelper = new TransactionHelper("NextValue");
			transHelper.Job = (trans) =>
			{
				trans.Add(numberGroup);
				TryGetNextValue(numberGroup);
			};
			transHelper.StartJob();
		}

		private static void TryGetNextValue(NumberGroupEntity numberGroup)
		{
			numberGroup.Refetch();

			numberGroup.IsNew = false;
			numberGroup.FetchExcludedFields(
				new ExcludeIncludeFieldsList() { NumberGroupFields.TransactionCounter }); // work around: concurrency problem

			CalculateNext(numberGroup);

			numberGroup.Save();
		}

		internal static void CalculateNext(NumberGroupEntity numberGroup)
		{
			if (numberGroup.ResetAtEndOfYear && numberGroup.LastModified.Year != DateTime.Now.Year)
			{
				// Year changed, not resetted yet, reset the number to start value
				numberGroup.CurrentValue = numberGroup.StartValue;
				// force save (in rare case, the CurrentValue was the StartValue, we need to update LastModified)
				numberGroup.IsDirty = true;
			}
			else if (numberGroup.ResetAtEndOfMonth
				&& (numberGroup.LastModified.Year != DateTime.Now.Year || numberGroup.LastModified.Month != DateTime.Now.Month))
			{
				// Month changed, not resetted yet, reset the number to start value
				numberGroup.CurrentValue = numberGroup.StartValue;
				// force save (in rare case, the CurrentValue was the StartValue, we need to update LastModified)
				numberGroup.IsDirty = true;
			}
			else
			{
				// Reset not needed, increment the current value
				numberGroup.CurrentValue++;
			}
		}
	}
}
