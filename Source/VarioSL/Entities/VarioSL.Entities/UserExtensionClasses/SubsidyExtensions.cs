﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.Linq
{
	public static class SubsidyExtensions
	{
		public static int? CalculatePrice(this IEnumerable<ProductSubsidyEntity> subs, long internalNo, int price)
		{
			var sub = subs.FirstOrDefault(s => s.TicketInternalNumber == internalNo);
			if (sub == null)
			{
				return null;
			}
			var decimalPrice = price / 100m;
			decimalPrice = sub.IsPercentage
				? decimalPrice - (decimalPrice * (sub.Subsidy / 10000m))
				: decimalPrice - (sub.Subsidy / 100m);
			return (int)(decimalPrice * 100);
		}

		public static int SubsidySumForCard(this IQueryable<SubsidyTransactionEntity> transactions, long cardID, bool hidePrivate = false, bool hideCorporate = false)
		{
            var lnq = new LinqMetaData();
			var period = GetCurrentSubsidyPeriod();

            var data = transactions
                .Where(s => s.Product.CardID == cardID)
                .Where(s => s.TransactionDate >= period.Item1)
                .Where(s => s.TransactionDate <= period.Item2)
                .Where(s => s.Product.IsPaid);

            if (hidePrivate)
                data = data.Where(s => lnq.OrderDetail.Any(od => od.ProductID == s.ProductID && od.Order.Contract.OrganizationID != null));
            if (hideCorporate)
                data = data.Where(s => lnq.OrderDetail.Any(od => od.ProductID == s.ProductID && od.Order.Contract.OrganizationID == null));

            return data.Sum(s => s.FullPrice - s.ReducedPrice);
		}

		private static Tuple<DateTime, DateTime> GetCurrentSubsidyPeriod()
		{
			var start = new DateTime();
			var end = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 20);

			if (end > DateTime.Now)
			{
				start = end.AddMonths(-1);
			}
			else
			{
				start = end;
				end = start.AddMonths(1);
			}

			end = end.AddSeconds(-1);

			return new Tuple<DateTime, DateTime>(start, end);
		}

	}
}
