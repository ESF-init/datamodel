﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Linq
{
	public static class DeviceClassExtensions
	{
		public static IQueryable<DeviceClassEntity> GetDeviceClassesForParameterRelease(this IQueryable<DeviceClassEntity> deviceClasses)
		{
			return deviceClasses.Where(d => d.ParameterReleaseVisibility.HasValue && d.ParameterReleaseVisibility.Value == true);
		}

		public static IQueryable<DeviceClassEntity> GetDeviceClassesWithCreateParameterArchiveRelease(this IQueryable<DeviceClassEntity> deviceClasses)
		{
			return deviceClasses.Where(d => d.CreateParameterArchive == true);
		}
	}
}
