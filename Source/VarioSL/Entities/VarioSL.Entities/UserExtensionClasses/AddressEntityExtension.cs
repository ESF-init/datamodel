﻿using System;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.UserExtensionClasses
{
	public static class AddressEntityExtension
	{
		/// <summary>
		/// Compares two address entities.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public static int CompareTo(this AddressEntity x, AddressEntity y)
		{
			if (x == null && y == null)
			{
				return 0;
			}
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}

			bool areEqual = x.AddressField1 == y.AddressField1 &&
							x.AddressField2 == y.AddressField2 &&
							x.City == y.City &&
							x.Country == y.Country &&
							x.PostalCode == y.PostalCode &&
							x.Region == y.Region &&
							x.Street == y.Street &&
							x.StreetNumber == y.StreetNumber &&
							x.Salutation == y.Salutation &&
							x.Addressee == y.Addressee &&
							x.Description == y.Description &&
							x.Addressee == y.Addressee;

			return Convert.ToInt32(!areEqual);
		}
	}
}
