﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Init.Common;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Common.SupportClasses.Extensions;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.SupportClasses;

namespace VarioSL.Entities.Linq
{
	public static class ConfigurationDefinitionExtensions
	{

		#region Properties

		public static IEnumerable<KeyValuePair<string, string>> AppConfigValues { get; set; }

		public enum ScopeOverwriteTypes { Workstation, User, Application, CustomScope, UnitId };

		#endregion

		#region Constructors

		static ConfigurationDefinitionExtensions()
		{
			AppConfigValues = ConfigurationManager.AppSettings.AllKeys.Select(k => new KeyValuePair<string, string>(k, ConfigurationManager.AppSettings[k]));
		}

		#endregion

		#region Methods GetMultipleValues

		/// <summary>
		/// Returns for each configuration value the "default value" from SL_ConfigurationDefinition, or some overwrite from app/web.config.
		/// </summary>
		public static Dictionary<string, string> GetDefaultConfigurationValues(this IQueryable<ConfigurationDefinitionEntity> source, bool replacePlaceholders = true)
		{
			return GetDefaultConfigurationValues(source, null, replacePlaceholders);
		}

		/// <summary>
		/// Returns for each configuration value the "default value" from SL_ConfigurationDefinition, or some overwrite from app/web.config. Filtered by GroupKey.
		/// </summary>
		public static Dictionary<string, string> GetDefaultConfigurationValues(this IQueryable<ConfigurationDefinitionEntity> source, string groupKey, bool replacePlaceholders = true)
		{
			return GetConfigurationValuesByGroupKey(source, null, groupKey, new ConfigurationScopeValues(), replacePlaceholders);
		}

		/// <summary>
		/// Returns for each configuration value the resolved value from SL_ConfigurationDefinition/SL_ConfigurationOverwrite, or some overwrite from app/web.config.
		/// </summary>
		public static Dictionary<string, string> GetConfigurationValues(this IQueryable<ConfigurationDefinitionEntity> source, long? clientID = null, ConfigurationScopeValues filter = null, bool replacePlaceholders = true, IQueryable<DeviceEntity> deviceQuery = null)
		{
			return GetConfigurationValues(source, clientID, null, GetScopeOverwrites(filter, deviceQuery), replacePlaceholders);
		}

		/// <summary>
		/// Returns for each configuration value the resolved value from SL_ConfigurationDefinition/SL_ConfigurationOverwrite, or some overwrite from app/web.config. Filtered by GroupKey.
		/// </summary>
		public static Dictionary<string, string> GetConfigurationValuesByGroupKey(this IQueryable<ConfigurationDefinitionEntity> source, long? clientID = null, string groupKey = null, ConfigurationScopeValues filter = null, bool replacePlaceholders = true, IQueryable<DeviceEntity> deviceQuery = null)
		{
			return GetConfigurationValues(source, clientID, groupKey, GetScopeOverwrites(filter, deviceQuery), replacePlaceholders);
		}

		private static Dictionary<string, string> GetConfigurationValues(this IQueryable<ConfigurationDefinitionEntity> source, long? clientID, string groupKey, KeyValuePair<string, string>[] scopeOverwriteSpecifiers, bool replacePlaceholders = true)
		{
			var result = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

			if (!string.IsNullOrWhiteSpace(groupKey))
				source = source.Where(configDef => configDef.GroupKey.ToUpper() == groupKey.ToUpper());

			var configField = source
				.Select(configDef => new
				{
					definition = configDef,
					overwrites = configDef.ConfigurationOverwrites.Where(ow => ow.ClientID == clientID)
				}).ToList();

			var defaultConfigs = source.ToDictionary(k => k.Name, v => v.DefaultValue);

			result.Merge(defaultConfigs); //set default values

			if (clientID != 0) //overwrite by client specific values
			{
				var allClientOverwrites = configField
					.SelectMany(c =>
					c.overwrites.Where(o => o.ClientID == clientID && string.IsNullOrEmpty(o.SpecifierType) && string.IsNullOrEmpty(o.SpecifierValue)))
					.ToDictionary(k => k.ConfigurationDefinition.Name, k => k.Value);
				result.Merge(allClientOverwrites);
			}

			if (scopeOverwriteSpecifiers != null && scopeOverwriteSpecifiers.Length > 0) //overwrite by specifiers (Workstation, Application, User)
			{
				var allScopeOverwrites = configField
					.Where(configDef => !string.IsNullOrEmpty(configDef.definition.Scope))
					.SelectMany(c => c.overwrites.FilterByScopeOverwrites(c.definition.Scope, scopeOverwriteSpecifiers))
					.ToDictionary(k => k.ConfigurationDefinition.Name, k => k.Value);
				result.Merge(allScopeOverwrites);
			}

			result.OverwriteWithAppConfigs(); //overwrite by app config values
			result.ToList().ForEach(kvp => result[kvp.Key] = HandleReplacePlaceHolders(replacePlaceholders, kvp.Value, clientID, scopeOverwriteSpecifiers));
			return result;
		}

		#endregion

		#region Methods GetSingleValue

		/// <summary>Returns default value by observing app.config overwrites only. No client/scope overwrite. Converting to generic type.</summary>
		public static T GetDefaultConfigurationValue<T>(this IQueryable<ConfigurationDefinitionEntity> source, string key, bool replacePlaceholders = true)
		{
			return source.GetConfigurationValue(key, null, null, replacePlaceholders).ConvertTo<T>();
		}

		/// <summary>Returns default value by observing app.config overwrites only. No client/scope overwrite.</summary>
		public static string GetDefaultConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, bool replacePlaceholders = true)
		{
			return source.GetConfigurationValue(key, null, new ConfigurationScopeValues(), replacePlaceholders);
		}

		/// <summary>Returns default value by observing app.config overwrites only. No client/scope overwrite. Converting to generic type. Returns fallback value on exception.</summary>
		public static T GetDefaultConfigurationValueWithFallback<T>(this IQueryable<ConfigurationDefinitionEntity> source, string key, T fallback, bool replacePlaceholders = true)
		{
			try
			{
				return source.GetConfigurationValueWithFallback<T>(key, null, fallback, null, replacePlaceholders);
			}
			catch (Exception)
			{
				return fallback;
			}
		}

		/// <summary>Returns the value by observing client/scope/app.config overwrites. Returns fallback value on exception.</summary>
		public static T GetConfigurationValueWithFallback<T>(this IQueryable<ConfigurationDefinitionEntity> source, string key, long? clientID, T fallback, ConfigurationScopeValues filter = null, bool replacePlaceholders = true, IQueryable<DeviceEntity> deviceQuery = null)
		{
			try
			{
				bool keyFound;
				string notFoundMessage;
				var result = GetConfigurationValue(source, key, replacePlaceholders, out keyFound, out notFoundMessage, clientID, GetScopeOverwrites(filter, deviceQuery));
				if (!keyFound)
					return fallback;
				else
					return result.ConvertTo<T>();
			}
			catch (Exception)
			{
				return fallback;
			}
		}

		/// <summary>Returns the value by observing client/scope/app.config overwrites. Throws Exception on any failure.</summary>
		public static T GetConfigurationValue<T>(this IQueryable<ConfigurationDefinitionEntity> source, string key, long? clientID, ConfigurationScopeValues filter = null, bool replacePlaceholders = true)
		{
			return GetConfigurationValue(source, key, clientID, filter, replacePlaceholders).ConvertTo<T>();
		}

		public static string GetConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, long? clientID, ConfigurationScopeValues filter = null, bool replacePlaceholders = true, IQueryable<DeviceEntity> deviceQuery = null)
		{
			bool keyFound;
			string notFoundMessage;
			var result = GetConfigurationValue(source, key, replacePlaceholders, out keyFound, out notFoundMessage, clientID, GetScopeOverwrites(filter, deviceQuery));
			if (!keyFound)
				throw new KeyNotFoundException(notFoundMessage);
			return result;
		}

		/// <summary>Returns the value by observing client/scope/app.config overwrites</summary>
		private static string GetConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, bool replacePlaceholders, out bool keyFound, out string notFoundMessage, long? clientID = null, params KeyValuePair<string, string>[] scopeOverwriteSpecifiers)
		{
			string result;
			if (TryFindConfigValueInAppConfigConfig(key, out result))
			{
				keyFound = true;
				notFoundMessage = string.Empty;
				return HandleReplacePlaceHolders(replacePlaceholders, result, clientID, scopeOverwriteSpecifiers);
			}
			var relevantConfigDefinitions = source  //ConfigDefinitions by key/name with relevant client overwrites
				.Where(configDef => configDef.Name.ToUpper() == key.ToUpper())
				.Select(configDef => new
				{
					Definition = configDef,
					Overwrites = configDef.ConfigurationOverwrites.Where(ow => ow.ClientID == clientID)
				}).ToList();

			if (relevantConfigDefinitions.Count() > 1)
			{
				keyFound = false;
				notFoundMessage = string.Format("Multiple values exist for the same key. ({0})", key);
				return null;
			}
			else if (relevantConfigDefinitions.Count() < 1)
			{
				keyFound = false;
				notFoundMessage = string.Format("Configuration definition does not exist in Database. ({0})", key);
				return null;
			}

			var foundConfigDefinition = relevantConfigDefinitions.Single();
			if (scopeOverwriteSpecifiers != null && scopeOverwriteSpecifiers.Length > 0 && !string.IsNullOrEmpty(foundConfigDefinition.Definition.Scope)) //overwrite by specifiers (Workstation, Application, User)
			{
				var relevantScopeOverwrites = foundConfigDefinition.Overwrites.FilterByScopeOverwrites(foundConfigDefinition.Definition.Scope, scopeOverwriteSpecifiers);

				if (relevantScopeOverwrites.Count() > 1)
				{
					var specifierValue = scopeOverwriteSpecifiers.FirstOrDefault(so => so.Key.ToUpper() == foundConfigDefinition.Definition.Scope.ToUpper()).Value;
					notFoundMessage = string.Format("Multiple scope overwrites exist for the same key/client/scope/specifier. {0}/{1}/{2}/{3}", key, clientID, foundConfigDefinition.Definition.Scope, specifierValue);
					keyFound = false;
					return null;
				}

				if (relevantScopeOverwrites.Count() > 0)
				{
					keyFound = true;
					notFoundMessage = null;
					return HandleReplacePlaceHolders(replacePlaceholders, relevantScopeOverwrites.Single().Value, clientID, scopeOverwriteSpecifiers);
				}
			}

			var releveantClientOverwrites = foundConfigDefinition.Overwrites.Where(ow => string.IsNullOrEmpty(ow.SpecifierType) && string.IsNullOrEmpty(ow.SpecifierValue));

			if (releveantClientOverwrites.Count() > 1)
			{
				keyFound = false;
				notFoundMessage = string.Format("Multiple client overwrites exist for the same key/client. {0}/{1}", key, clientID);
				return null;
			}

			if (releveantClientOverwrites.Count() > 0)
			{
				keyFound = true;
				notFoundMessage = null;
				return HandleReplacePlaceHolders(replacePlaceholders, releveantClientOverwrites.Single().Value, clientID, scopeOverwriteSpecifiers);
			}
			else
			{
				keyFound = true;
				notFoundMessage = null;
				return HandleReplacePlaceHolders(replacePlaceholders, foundConfigDefinition.Definition.DefaultValue, clientID, scopeOverwriteSpecifiers);
			}
		}

		#endregion

		#region Methods UpdateConfigurationValue

		public static void CreateOrUpdateConfigurationDefinition(this IQueryable<ConfigurationDefinitionEntity> source, string key, string defaultValue, string description, string groupKey, ITransaction transaction = null)
		{
			CreateOrUpdateConfigurationDefinition(source, key, defaultValue, description, groupKey, null, 6, ConfigurationDataType.Text, transaction);
		}

		public static void CreateOrUpdateConfigurationDefinition(this IQueryable<ConfigurationDefinitionEntity> source, string key, string defaultValue, string description, string groupKey, string scope = null, short accessLevel = 6, ConfigurationDataType dataType = ConfigurationDataType.Text, ITransaction transaction = null)
		{
			if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(description) || string.IsNullOrWhiteSpace(groupKey))
				throw new InvalidOperationException(string.Format("Invalid paramaters to create or update configuration definition. ({0})", key));

			var definition = source.FirstOrDefault(d => d.Name.ToUpper() == key.ToUpper());

			if (definition == null)
				definition = new ConfigurationDefinitionEntity() { IsNew = true };

			definition.Name = key;
			definition.DefaultValue = string.IsNullOrEmpty(defaultValue)
				? (string.IsNullOrEmpty(definition.DefaultValue) ? null : definition.DefaultValue)
				: defaultValue;
			definition.AccessLevel = accessLevel;
			definition.GroupKey = groupKey;
			definition.Scope = scope;
			definition.Description = description;
			definition.DataType = dataType;
			
			if (transaction != null)
				transaction.Add(definition);

			definition.Save();
		}

		public static void SetDefaultConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, string value)
		{
			if (string.IsNullOrEmpty(key))
				throw new InvalidOperationException(string.Format("Invalid paramaters to configure default configuration. ({0})", key));

			HandleConfigUpdate(source, key, value, -1, string.Empty, string.Empty);
		}

		public static void SetClientOverwriteConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, string value, long clientID)
		{
			if (clientID <= 0 || string.IsNullOrEmpty(key))
				throw new InvalidOperationException(string.Format("Invalid paramaters to configure client overwrite configuration. ({0})", key));

			HandleConfigUpdate(source, key, value, clientID, string.Empty, string.Empty);
		}

		public static void SetScopeOverwriteConfigurationValue(this IQueryable<ConfigurationDefinitionEntity> source, string key, string value, long clientID, ScopeOverwriteTypes specifierType, string specifierValue)
		{
			if (clientID <= 0 || string.IsNullOrEmpty(key) || string.IsNullOrEmpty(specifierValue))
				throw new InvalidOperationException(string.Format("Invalid paramaters to configure scope overwrite configuration. ({0})", key));

			HandleConfigUpdate(source, key, value, clientID, specifierType.ToString(), specifierValue);
		}

		private static void HandleConfigUpdate(this IQueryable<ConfigurationDefinitionEntity> source, string key, string value, long clientID, string specifierType, string specifierValue, ITransaction transaction = null)
		{
			var relevantConfigDefinitions = source  //ConfigDefinitions by key/name with relevant client overwrites
				.Where(configDef => configDef.Name.ToUpper() == key.ToUpper())
				.Select(configDef => new
				{
					Definition = configDef,
					Overwrites = configDef.ConfigurationOverwrites.Where(ow => ow.ClientID == clientID)
				}).ToList();

			if (relevantConfigDefinitions.Count() > 1)
				throw new InvalidOperationException(string.Format("Multiple values exist for the same key. ({0})", key));
			else if (relevantConfigDefinitions.Count() < 1)
				throw new KeyNotFoundException(string.Format("Configuration definition does not exist in Database. ({0})", key));

			var foundConfigDefinition = relevantConfigDefinitions.Single();
			var targetDefinition = foundConfigDefinition.Definition;

			if (clientID > 0) //Handle as overwrite (scope or client)
			{
				ConfigurationOverwriteEntity targetOverwrite;
				IEnumerable<ConfigurationOverwriteEntity> relevantOverwrites;

				if (!string.IsNullOrEmpty(specifierType) && !string.IsNullOrEmpty(specifierValue)) //Handle as scope overwrite
				{
					if (string.IsNullOrEmpty(foundConfigDefinition.Definition.Scope))
						throw new InvalidOperationException(string.Format("The key is not configured to contain scope overwrites. ({0}, {1})", key, specifierType));
					if (!string.Equals(foundConfigDefinition.Definition.Scope, specifierType, StringComparison.InvariantCultureIgnoreCase))
						throw new InvalidOperationException(string.Format("The key can not have overwrites with targeted scope type. ({0}, {1})", key, specifierType));

					relevantOverwrites = foundConfigDefinition.Overwrites.Where(co =>
					string.Equals(co.SpecifierType, specifierType) && string.Equals(co.SpecifierValue, specifierValue, StringComparison.InvariantCultureIgnoreCase));
					if (relevantOverwrites.Count() > 1)
					{
						string exceptionMsg = string.Format("Multiple scope overwrites exist for the same key/client/scope/specifier. {0}/{1}/{2}/{3}", key, clientID, foundConfigDefinition.Definition.Scope, specifierValue);
						throw new InvalidOperationException(exceptionMsg);
					}
				}
				else //Handle as client overwrite
				{
					relevantOverwrites = foundConfigDefinition.Overwrites.Where(co => string.IsNullOrEmpty(co.SpecifierType) && string.IsNullOrEmpty(co.SpecifierValue));
					if (relevantOverwrites.Count() > 1)
					{
						string exceptionMsg = string.Format("Multiple client overwrites exist for the same key/client. {0}/{1}", key, clientID);
						throw new InvalidOperationException(exceptionMsg);
					}

				}

				if (relevantOverwrites.Count() < 1) //Insert overwrite
				{
					targetOverwrite = foundConfigDefinition.Definition.ConfigurationOverwrites.AddNew();

				}
				else //Update overwrite
				{
					targetOverwrite = relevantOverwrites.Single();
				}

				targetOverwrite.ClientID = clientID;
				targetOverwrite.Value = value;
				targetOverwrite.SpecifierType = specifierType;
				targetOverwrite.SpecifierValue = specifierValue;
				
				if (transaction != null)
					transaction.Add(targetOverwrite);

				targetOverwrite.Save();
			}
			else //Handle as Default value
			{
				if (transaction != null)
					transaction.Add(targetDefinition);

				targetDefinition.DefaultValue = value;
				targetDefinition.Save();
			}
		}

		#endregion

		#region Private helper

		private static KeyValuePair<string, string>[] GetScopeOverwrites(ConfigurationScopeValues filter, IQueryable<DeviceEntity> deviceQuery = null)
		{
			var scopeOverwriteSpecifiers = new List<KeyValuePair<string, string>>();
			if (filter != null)
			{
				if (deviceQuery is null)
					deviceQuery = new LinqMetaData().Device.AsQueryable();

				long? unitID = filter?.UnitId;
				if (!unitID.HasValue && !String.IsNullOrWhiteSpace(filter?.WorkstationName))
				{
					var device = deviceQuery.FirstOrDefault(d => d.Name.Trim().ToUpper() == filter.WorkstationName.Trim().ToUpper());
					if (device != null)
					{
						unitID = device.UnitID;
					}
					else if (filter.WorkstationName.Contains('<')) //PC123<123> ?
					{
						var substring = filter.WorkstationName.Substring(0, filter.WorkstationName.IndexOf('<')); //Take PC123
						if (!String.IsNullOrWhiteSpace(substring))
						{
							device = deviceQuery.FirstOrDefault(d => d.Name.Trim().ToUpper() == substring.Trim().ToUpper());
							if (device != null)
							{
								unitID = device.UnitID;
							}
						}
					}
				}
				if (unitID.HasValue)
					scopeOverwriteSpecifiers.Add(new KeyValuePair<string, string>(ScopeOverwriteTypes.UnitId.ToString(), unitID.Value.ToString()));
				if (!string.IsNullOrEmpty(filter.UserName))
					scopeOverwriteSpecifiers.Add(new KeyValuePair<string, string>(ScopeOverwriteTypes.User.ToString(), filter.UserName));
				if (!string.IsNullOrEmpty(filter.WorkstationName))
					scopeOverwriteSpecifiers.Add(new KeyValuePair<string, string>(ScopeOverwriteTypes.Workstation.ToString(), filter.WorkstationName));
				if (!string.IsNullOrEmpty(filter.ApplicationName))
					scopeOverwriteSpecifiers.Add(new KeyValuePair<string, string>(ScopeOverwriteTypes.Application.ToString(), filter.ApplicationName));
				if (!string.IsNullOrEmpty(filter.CustomScopeValue))
					scopeOverwriteSpecifiers.Add(new KeyValuePair<string, string>(ScopeOverwriteTypes.CustomScope.ToString(), filter.CustomScopeValue));
			}
			
			return scopeOverwriteSpecifiers.ToArray();
		}

		private static string HandleReplacePlaceHolders(bool replace,string value, long? clientID, KeyValuePair<string, string>[] scopeOverwriteSpecifiers)
		{
			if (!replace)
				return value;

			string result = value;
			if (clientID.HasValue)
			{
				result = result.ReplaceIgnoreCase("<CLIENTID>", string.Format("{0:00}", clientID));
			}

			if (scopeOverwriteSpecifiers.IsNotNullOrEmpty())
			{
				scopeOverwriteSpecifiers.ForEach(kvp => result = result.ReplaceIgnoreCase($"<{kvp.Key}>", $"{kvp.Value}"));
			}

			return result;
		}

		private static IEnumerable<ConfigurationOverwriteEntity> FilterByScopeOverwrites(this IEnumerable<ConfigurationOverwriteEntity> overwriteEntities, string scope, KeyValuePair<string, string>[] scopeOverwriteSpecifiers)
		{
			return overwriteEntities
						.Where(o => !string.IsNullOrEmpty(o.SpecifierType) && !string.IsNullOrEmpty(o.SpecifierValue))
						.Where(o => o.SpecifierType.ToUpper() == scope.ToUpper())
						.Where(o => scopeOverwriteSpecifiers.Any(os =>
							os.Key.ToUpper() == o.SpecifierType.ToUpper() &&
							os.Value.ToUpper() == o.SpecifierValue.ToUpper()));
		}

		private static bool TryFindConfigValueInAppConfigConfig(string key, out string result)
		{
			var relevantAppConfigValues = AppConfigValues.EmptyIfNull().Where(k => k.Key.ToUpper() == key.ToUpper());
			if (relevantAppConfigValues.Count() > 1)
				throw new InvalidOperationException(string.Format("Found multiple configuration values for key in app.config/web.config (case insensitiv!). ({0})", key));

			if (relevantAppConfigValues.Any())
			{ //search in App.config/web.config
				result = relevantAppConfigValues.Single().Value;
				return true;
			}
			else
			{
				result = default(string);
				return false;
			}
		}

		private static void OverwriteWithAppConfigs(this Dictionary<string, string> configDictionary)
		{
			var appConfigs = AppConfigValues
				.Where(k => configDictionary.Any(x => x.Key.ToUpper() == k.Key.ToUpper()))
				.ToDictionary(k => k.Key, k => AppConfigValues.Single(cv => cv.Key == k.Key).Value);
			configDictionary.Merge(appConfigs);
		}

		#endregion
	}
}