﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.HelperClasses;

namespace VarioSL.Entities.Linq
{
	public static class NumberGroupScopeExtensions
	{
		/// <summary>
		/// This method can be used for safe parallel access to the number groups. 
		/// While the transaction is mandatory, it does not use up a number on rollback.
		/// Access from multiple threads is also possible, as long as every thread has their own transaction.
		/// With the select for update call, the row gets locked during select already, which prevents multiple reads of the same value.
		/// </summary>
		/// <param name="scope"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		public static string GetNextValue(this NumberGroupScope scope, Transaction transaction)
		{
			var connection = transaction.ConnectionToUse;
			using (var command = connection.CreateCommand())
			{
				command.Transaction = transaction.PhysicalTransaction;
				var scopeParameter = command.CreateParameter();
				scopeParameter.ParameterName = "scope";
				scopeParameter.Value = (int)scope;
				command.CommandText = "select currentvalue, startvalue, resetatendofyear, resetatendofmonth, lastmodified, format from sl_numbergroup where scope=:scope for update";
				command.Parameters.Add(scopeParameter);
				using (var result = command.ExecuteReader())
				{
					if (result.HasRows && result.Read())
					{
						var grp = new NumberGroupEntity
						{
							Scope = scope,
							CurrentValue = result.GetInt64(0),
							StartValue = result.GetInt64(1),
							ResetAtEndOfYear = result.GetInt16(2) != 0,
							ResetAtEndOfMonth = result.GetInt16(3) != 0,
							LastModified = result.GetDateTime(4),
							Format = result.GetString(5)
						};
						NumberGroupExtension.CalculateNext(grp);

						command.CommandText = "update sl_numbergroup set currentvalue = :nextvalue, transactioncounter=transactioncounter+1, lastmodified=sysdate where scope=:scope";
						var valueParam = command.CreateParameter();
						valueParam.ParameterName = "nextvalue";
						valueParam.Value = grp.CurrentValue;
						command.Parameters.Clear();
						command.Parameters.Add(valueParam);
						command.Parameters.Add(scopeParameter);
						var updatedRows = command.ExecuteNonQuery();
						if (updatedRows == 1)
							return grp.FormatValue();
					}
				}
			}
			return null;
		}

		public static NumberGroupRandomizedCollection GenerateRandomizedValues(
			this NumberGroupScope scope,
			int count,
			Transaction transaction,
			Func<string, string> transform = null)
		{
			var rand = new Random();
			var numbers = new NumberGroupRandomizedCollection();

			numbers.AddRange(
			Enumerable.Range(0, count).Select(_ => new NumberGroupRandomizedEntity
			{
				Scope = scope,
				RandomSortNumber = rand.Next(int.MinValue, int.MaxValue),
				RandomizedValue = transform == null ? scope.GetNextValue(transaction) : transform(scope.GetNextValue(transaction))
			}));
			return numbers;
		}

		public static IEnumerable<string> GetRandomizedValues(
			this NumberGroupScope scope,
			int count,
			Transaction transaction)
		{
			var connection = transaction.ConnectionToUse;
			using (var command = connection.CreateCommand())
			{
				command.Transaction = transaction.PhysicalTransaction;

				var scopeParameter = command.CreateParameter();
				scopeParameter.ParameterName = "scope";
				scopeParameter.Value = (int)scope;
				command.Parameters.Add(scopeParameter);

				var countParameter = command.CreateParameter();
				countParameter.ParameterName = "count";
				countParameter.Value = count;
				command.Parameters.Add(countParameter);

				command.CommandText = "select numbergrouprandomizedid, randomizedvalue, transactioncounter from sl_numbergrouprandomized where numbergrouprandomizedid in (select numbergrouprandomizedid from (select numbergrouprandomizedid from sl_numbergrouprandomized where scope=:scope and isused = 0 order by randomsortnumber) where rownum <= :count) for update";
				var data = new NumberGroupRandomizedCollection();
				using (var result = command.ExecuteReader())
				{
					while (result.HasRows && result.Read())
					{
						data.Add(new NumberGroupRandomizedEntity
						{
							NumberGroupRandomizedID = result.GetInt64(0),
							RandomizedValue = result.GetString(1),
							TransactionCounter = result.GetDecimal(2),
						});
					}
				}
				if (data.Count != count)
					throw new InvalidOperationException("Not enough values left. data.Count:" + data.Count.ToString());

				var prototype = new NumberGroupRandomizedEntity { IsUsed = true };
				prototype.Fields[(int)NumberGroupRandomizedFieldIndex.TransactionCounter].ExpressionToApply
					= (NumberGroupRandomizedFields.TransactionCounter + 1);
				var collection = new NumberGroupRandomizedCollection();
				transaction.Add(collection);
				int skip = 0;
				while (true)
				{
					const int take = 999;
					var toUpdate = data.Skip(skip).Take(take).Select(c => c.NumberGroupRandomizedID);
					skip += take;
					if (!toUpdate.Any())
						break;

					collection.UpdateMulti(prototype, new FieldCompareRangePredicate(NumberGroupRandomizedFields.NumberGroupRandomizedID, toUpdate));
				}
				return data.Select(d => d.RandomizedValue);
			}
		}
	}
}
