﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.UserExtensionClasses
{
	public static class UserConfigExtension
	{
		[Obsolete("User_Config will be removed! Use (SL_)ConfigurationDefinition instead.")]
		public static T GetConfigValueWithDefault<T>(this IQueryable<UserConfigEntity> userConfiguration, string configurationValueID, string defaultValue)
		{
			Type returnType = typeof(T);
			IList<Type> returnTypeInterfaces = returnType.GetInterfaces();
			string configValue = GetConfigValueWithDefault(userConfiguration, configurationValueID, defaultValue);

			if (returnType != typeof(string) && returnTypeInterfaces.Contains(typeof(IList)))
			{
				Type genericArgumentType = returnType.GetGenericArguments()[0];
				IList returnValues = (IList)Activator.CreateInstance<T>();
				foreach (string value in configValue.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
				{
					returnValues.Add(ChangeType(value, genericArgumentType));
				}
				return (T)returnValues;
			}
			else
			{
				if (string.IsNullOrEmpty(configValue))
				{
					return default(T);
				}
				return (T)ChangeType(configValue, returnType);
			}
		}

		[Obsolete("User_Config will be removed! Use (SL_)ConfigurationDefinition instead.")]
		public static string GetConfigValueWithDefault(this IQueryable<UserConfigEntity> userConfiguration, string configurationValueID, string defaultValue)
		{
			var result = userConfiguration.SingleOrDefault(entry => entry.ConfigID == configurationValueID);
			if (result == null)
			{
				return defaultValue;
			}
			return result.ConfigValue;
		}

		private static object ChangeType(string value, Type conversionType)
		{
			if (conversionType.IsEnum)
			{
				return Enum.Parse(conversionType, value, true);
			}
			else
			{
				return Convert.ChangeType(value, conversionType, CultureInfo.InvariantCulture);
			}
		}
	}
}
