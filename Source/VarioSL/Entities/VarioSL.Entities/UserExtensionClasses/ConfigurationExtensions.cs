﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using VarioSL.Common.SupportClasses.Extensions;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.Linq
{
	/// <summary>
	/// Tries to get a config value by the following order: Web.Config/SL_Configuration table and if vario scope also user_config
	/// </summary>
	public static class ConfigurationExtensions
	{
		private const string defaultScope = "System";
		private const string varioScope = "Vario";
		public static string DefaultScope { get { return defaultScope; } }
		public static string VarioScope { get { return varioScope; } }

		static ConfigurationExtensions()
		{
			AppConfig = ConfigurationManager.AppSettings.AllKeys.Select(k => new ConfigurationEntity { Name = k, Value = ConfigurationManager.AppSettings[k] }).AsQueryable();
			UserConfig = new LinqMetaData().UserConfig;
		}

		internal static IQueryable<ConfigurationEntity> AppConfig { get; set; } // Overwrites in Web.Config
		internal static IQueryable<UserConfigEntity> UserConfig { get; set; } // table USER_CONFIG

		[Obsolete("SL_Configuration will be removed! Use (SL_)ConfigurationDefinition instead.", true)]
		public static Dictionary<string, string> GetConfigurationValues(this IQueryable<ConfigurationEntity> source, string scope, long clientID = 0)
		{
			// TODO: Default values might not be handled correctly
			var ret = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
			if (scope == VarioScope)
			{
				//load user defaults
				var defaultConfigs = UserConfig.Where(c => c.ClientID == 0).ToDictionary(k => k.ConfigID, v => v.ConfigValue);
				ret.Merge(defaultConfigs);

				// if client passed load client specific configs and merge
				if (clientID != 0)
				{
					var clientConfigs = UserConfig.Where(c => c.ClientID == clientID).ToDictionary(k => k.ConfigID, v => v.ConfigValue);
					ret.Merge(clientConfigs);
				}
								
				var slOverwrites = source.Where(k => k.Scope == varioScope).ToDictionary(k => k.Name, v => v.Value);
				ret.Merge(slOverwrites);
			}
			else
			{
				var slConfigs = source
					.Where(c => c.Scope == scope)
					.ToDictionary(k => k.Name, v => v.Value);
				ret.Merge(slConfigs);
			}

			var appConfigs = AppConfig
				.Where(k => ret.Any(x => x.Key.Equals(k.Name, StringComparison.InvariantCultureIgnoreCase)))
				.ToDictionary(k => k.Name, v => v.Value);
			ret.Merge(appConfigs);
			return ret;
		}

		[Obsolete("SL_Configuration will be removed! Use (SL_)ConfigurationDefinition instead.", true)]
		public static string GetConfigurationValue(this IQueryable<ConfigurationEntity> source, string key, string scope)
		{
			// TODO: Make case insensitive
			IQueryable<ConfigurationEntity> ret;

			ret = from appConfig in AppConfig
				  where appConfig.Name == key
				  select appConfig;

			if (!ret.Any()) // key does not exist, look in sl_configuration
			{
				ret =
					from slConfig in source
					where slConfig.Scope == scope || slConfig.Scope == defaultScope
					where slConfig.Name == key
					select slConfig;
			}

			if (!ret.Any() && scope == VarioScope) // key does not exist, look in user_config in case of VarioScope
			{
				ret =
					from varioConfig in UserConfig
					where varioConfig.ConfigID == key
					select new ConfigurationEntity { Scope = VarioScope, Name = varioConfig.ConfigID, Value = varioConfig.ConfigValue };
			}

			var count = ret.Count();

			if (count == 0)
			{
				throw new KeyNotFoundException(string.Format("Configuration value does not exist in Database. ({0}-{1})", scope, key));
			}

			if (count > 1)
			{
				throw new InvalidOperationException("Multiple values exist for the same scope/key.");
			}

			return ret.Single().Value;
		}

		[Obsolete("SL_Configuration will be removed! Use (SL_)ConfigurationDefinition instead.", true)]
		public static T GetConfigurationValue<T>(this IQueryable<ConfigurationEntity> source, string key, string scope)
		{
			return source.GetConfigurationValue(key, scope).ConvertTo<T>();
		}

		[Obsolete("SL_Configuration will be removed! Use (SL_)ConfigurationDefinition instead.", true)]
		public static T GetConfigurationValue<T>(this IQueryable<ConfigurationEntity> source, string key, string scope, T defaultValue)
		{
			try
			{
				return source.GetConfigurationValue(key, scope).ConvertTo<T>(defaultValue);
			}
			catch (KeyNotFoundException)
			{
				return defaultValue;
			}
		}
	}
}
