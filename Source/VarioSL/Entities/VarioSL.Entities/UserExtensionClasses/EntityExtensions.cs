﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.Extensions
{ 
    public static class EntityExtensions
    {
        public static List<IEntityFieldCore> GetChangedFields(this IEntityCore entity)
        {
            var list = new List<IEntityFieldCore>();
            foreach (var field in entity.Fields)
            {
                if (field.IsChanged)
                {
                    list.Add(field);
                }
            }
            return list;
        }
    }
}
