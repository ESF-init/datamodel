﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
    partial class PostingKeyEntity
    {

        private static List<PostingKeyEntity> GetCachedPostingKeys()
        {
//            return (List<PostingKeyEntity>)MemoryCache.Default.AddOrGetExisting("PostingKeyEntity", new LinqMetaData().PostingKey.ToList(), DateTimeOffset.Now.AddMinutes(5));
           return new LinqMetaData().PostingKey.ToList();
        }

        public static bool CheckForPostingsKeys(List<long>postingKeyNumber,PostingScope scope,long clientID=0)
        {
            return postingKeyNumber.All(p => GetCachedPostingKeys().Any(chp => chp.PostingKey == p && chp.PostingScope == (int)scope && chp.ClientID == clientID));
        }

        // AbschlagsZahlung
        public static PostingKeyEntity GetPartialPayment(PostingScope scope,long clientID=0)
        {
            return Get(1000, scope, clientID);
        }
        //Ticket Verkauf
        public static PostingKeyEntity GetTicketSale(PostingScope scope, long clientID = 0)
        {
            return Get(1001, scope, clientID);
        }
        //Erstattung
        public static PostingKeyEntity GetTicketRefund(PostingScope scope, long clientID = 0)
        {
            return Get(2000, scope, clientID);

        }

        public static PostingKeyEntity Get(int postingKeyNumber, PostingScope scope, long clientID = 0)
        {
            return GetCachedPostingKeys().FirstOrDefault(p => p.PostingKey == postingKeyNumber && p.PostingScope == (int)scope && p.ClientID == clientID);
        }
    }
}
