﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.CollectionClasses;

namespace VarioSL.Entities.EntityClasses
{
	public partial class SettlementQuerySettingEntity
	{

		public static readonly PropertyExpression<SettlementQuerySettingEntity, bool> HasQueryValuesExp =
			new PropertyExpression<SettlementQuerySettingEntity, bool>(src => src.SettlementQueryValues.Count() > 0);

		public virtual bool HasQueryValues { get { return HasQueryValuesExp.Func(this); } }

		public static readonly PropertyExpression<SettlementQuerySettingEntity, int> NumberOfAssignedTicketsExp =
			new PropertyExpression<SettlementQuerySettingEntity, int>(src => src.Tickets.Count());

		public int NumberOfAssignedTickets { get { return NumberOfAssignedTicketsExp.Func(this); } }

		public static readonly PropertyExpression<SettlementQuerySettingEntity, bool> IsUpdateAllowedExp =
			new PropertyExpression<SettlementQuerySettingEntity, bool>(src => SettlementSetupEntity.IsUpdateAllowedExp.Func(src.SettlementSetup));

		public bool IsUpdateAllowed { get { return SettlementSetupEntity.IsUpdateAllowedExp.Func(SettlementSetup); } }
	}
}
