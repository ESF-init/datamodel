﻿using System.Linq;

namespace VarioSL.Entities.EntityClasses
{
    public partial class PersonEntity
	{
		public bool HasPhotograph { get { return HasPhotographExp.Func(this); } }

		public static readonly PropertyExpression<PersonEntity, bool> HasPhotographExp =
			new PropertyExpression<PersonEntity, bool>(p => p.Photographs.Any());
	}
}
