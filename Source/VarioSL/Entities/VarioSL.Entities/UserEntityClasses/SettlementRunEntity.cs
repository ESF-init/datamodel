﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.CollectionClasses;
using VarioSL.Entities.HelperClasses;

namespace VarioSL.Entities.EntityClasses
{
	public partial class SettlementRunEntity
	{
		public static readonly PropertyExpression<SettlementRunEntity, bool> HasRunValuesExp =
			new PropertyExpression<SettlementRunEntity, bool>(src => src.SettlementRunValues.Count() > 0);

		public static readonly PropertyExpression<SettlementRunEntity, bool> IsCalculateAllowedExp =
			new PropertyExpression<SettlementRunEntity, bool>(src => SettlementSetupEntity.IsCalculateAllowedExp.Func(src.SettlementSetup));

		public static readonly PropertyExpression<SettlementRunEntity, double> ValueExp =
			new PropertyExpression<SettlementRunEntity, double>(src => src.SettlementRunValues.Sum(v => v.Value));

		public static readonly PropertyExpression<SettlementRunEntity, DateTime> CalculatedExp =
			new PropertyExpression<SettlementRunEntity, DateTime>(src => src.SettlementRunValues.Max(v => v.Calculated));

		public static readonly PropertyExpression<SettlementRunEntity, bool> IsPurgeAllowedExp =
			new PropertyExpression<SettlementRunEntity, bool>(src => SettlementSetupEntity.IsPurgeAllowedExp.Func(src.SettlementSetup));


		public virtual bool IsCalculateAllowed
		{ get { return SettlementSetupEntity.IsCalculateAllowedExp.Func(SettlementSetup); } }

		public virtual bool IsPurgeAllowed
		{ get { return SettlementSetupEntity.IsPurgeAllowedExp.Func(SettlementSetup); } }

		public virtual bool HasRunValues
		{
			get { return HasRunValuesExp.Func(this); }
		}

		public double Value
		{
			get { return ValueExp.Func(this); }
		}

		public DateTime Calculated
		{ get { return CalculatedExp.Func(this); } }


		public void Purge(Transaction trans = null)
		{
			if (!IsPurgeAllowed)
				throw new InvalidOperationException("Purge is not allowed.");

			var runValuesToVarioSettlements = SettlementRunValues.SelectMany(runValue => runValue.SettlementRunValueToVarioSettlements);
			var queryValues = SettlementRunValues.SelectMany(runValue => runValue.SettlementQueryValues);
			foreach (SettlementQueryValueEntity queryValue in queryValues)
			{
				if (trans != null)
					trans.Add(queryValue);
				queryValue.Delete();
				queryValue.Save();
			}

			foreach (SettlementRunValueToVarioSettlementEntity runValueToVarioSettlement in runValuesToVarioSettlements)
			{
				if (trans != null)
					trans.Add(runValueToVarioSettlement);
				runValueToVarioSettlement.Delete();
				runValueToVarioSettlement.Save();
			}

			foreach (SettlementRunValueEntity runValue in SettlementRunValues)
			{
				if (trans != null)
					trans.Add(runValue);
				runValue.Delete();
				runValue.Save();
			}
		}
	}
}
