﻿using System;
using System.Collections.Generic;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
    public partial class StorageLocationEntity
    {
        internal static IQueryable<CardEntity> cardData;

        private readonly static Func<IQueryable<CardEntity>> _cardData = () =>
        {
            return cardData ?? new LinqMetaData().Card.AsQueryable();
        };

        public static readonly PropertyExpression<StorageLocationEntity, long> SellableCardCountExp =
            new PropertyExpression<StorageLocationEntity, long>(src => _cardData().Count(c => c.StorageLocationID == src.StorageLocationID && c.InventoryState == InventoryState.Delivered));
        public long SellableCardCount { get { return SellableCardCountExp.Func(this); } }

    }
}
