﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class OrderWorkItemEntity
	{
		public string IdentificationNumber
		{
			get
			{
				return IdentificationNumberExp.Func(this);
			}
		}
		public static readonly PropertyExpression<OrderWorkItemEntity, string> IdentificationNumberExp =
			new PropertyExpression<OrderWorkItemEntity, string>(a =>a.Order.OrderNumber);
	}
}
