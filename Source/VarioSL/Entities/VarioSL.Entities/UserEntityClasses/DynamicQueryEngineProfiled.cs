﻿using SD.LLBLGen.Pro.DQE.Oracle;
using StackExchange.Profiling;
using System.Data.Common;

namespace VarioSL.Entities.UserEntityClasses
{
    /// <summary>
    /// To use this, you need to edit the CommonDAOBase constructor, which gets overwritten for new entities
    /// </summary>
    public class DynamicQueryEngineProfiled : DynamicQueryEngine
	{
		protected override DbCommand CreateCommand(DbConnection connectionToUse)
		{
			DbCommand cmd = base.CreateCommand(connectionToUse);
			return new StackExchange.Profiling.Data.ProfiledDbCommand(cmd, null, MiniProfiler.Current);
		}
	}
}
