﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public interface ICardEventGenerator
	{
		CardEventEntity CreateEventIssue(CardEntity card);
		CardEventEntity CreateEventAssign(CardEntity card, ContractEntity contract);
		CardEventEntity CreateEventReplace(CardEntity oldCard, CardEntity newCard, bool IsNewCard);
		CardEventEntity CreateEventRevoke(CardEntity card);
		CardEventEntity CreateEventDepositAdd(CardEntity card, bool isDataSync);
		CardEventEntity CreateEventDepositRemove(CardEntity card);
	}
}
