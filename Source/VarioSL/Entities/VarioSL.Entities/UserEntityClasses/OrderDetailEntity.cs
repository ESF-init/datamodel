﻿using System;
using VarioSL.Entities.Enumerations;
using System.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class OrderDetailEntity
	{
		/// <summary>
		/// Get injected from dto to set relations between card and products
		/// </summary>
		public long CartItemID { get; set; }
		/// <summary>
		/// Get injected from dto to set relations between card and products
		/// </summary>
		public long? CartItemReferenceID { get; set; }

		public bool IsPaid { get { return IsPaidExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, bool> IsPaidExp =
			new PropertyExpression<OrderDetailEntity, bool>(
					od => (od.Order != null && od.Order.Sale != null) ?
						(od.Order.TotalAmount() == 0 || od.Order.Sale.PaymentJournals != null && od.Order.Sale.PaymentJournals.Sum(p => p.Amount) >= od.Order.TotalAmount())
						: od.PaymentID.HasValue);

		public PaymentState PaymentStateWithDefault { get { return PaymentStateWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, PaymentState> PaymentStateWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, PaymentState>(od => od.Payment != null ? od.Payment.State : PaymentState.Unknown);

		public string PaymentExecutionResultWithDefault { get { return PaymentExecutionResultWithDefaultExp.Func(this); } }
		
		public static readonly PropertyExpression<OrderDetailEntity, string> PaymentExecutionResultWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.Payment != null ? od.Payment.ExecutionResult : string.Empty);

		public DateTime? PaymentExecutionTimeWithDefault { get { return PaymentExecutionTimeWithDefaultExp.Func(this); } }
		
		public static readonly PropertyExpression<OrderDetailEntity, DateTime?> PaymentExecutionTimeWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, DateTime?>(od => od.Payment != null ? (DateTime?)od.Payment.ExecutionTime : null);

		public string PaymentReceiptNumberWithDefault { get { return PaymentReceiptNumberWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> PaymentReceiptNumberWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.Payment != null ? od.Payment.ReceiptNumber : string.Empty);

		public string PaymentTransactionOrderNumberWithDefault { get { return PaymentTransactionOrderNumberWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> PaymentTransactionOrderNumberWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.Payment != null ? od.Payment.TransactionOrderNumber : string.Empty);

		public string PaymentDescriptionWithDefault { get { return PaymentDescriptionWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> PaymentDescriptionWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.Payment != null ? od.Payment.Description : string.Empty);

		public string PaymentFullResultWithDefault { get { return PaymentFullResultWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> PaymentFullResultWithDefaultExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.Payment != null ? od.Payment.FullResult : string.Empty);

		public long CardHolderID { get { return CardHolderIDExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, long> CardHolderIDExp =
			new PropertyExpression<OrderDetailEntity, long>(od => od.OrderDetailToCardHolder.Count>0 ? od.OrderDetailToCardHolder[0].CardHolderID : 0);

		public string CardHolderFirstName { get { return CardHolderFirstNameExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> CardHolderFirstNameExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.OrderDetailToCardHolder.Count > 0 ? od.OrderDetailToCardHolder[0].CardHolder.FirstName : string.Empty);

		public string CardHolderLastName { get { return CardHolderLastNameExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> CardHolderLastNameExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.OrderDetailToCardHolder.Count > 0 ? od.OrderDetailToCardHolder[0].CardHolder.LastName : string.Empty);

		public string CardHolderContractOrganizationName { get { return CardHolderContractOrganizationNameExp.Func(this); } }

		public static readonly PropertyExpression<OrderDetailEntity, string> CardHolderContractOrganizationNameExp =
			new PropertyExpression<OrderDetailEntity, string>(od => od.OrderDetailToCardHolder.Count > 0 ? od.OrderDetailToCardHolder[0].CardHolder.Contract.Organization.Name : string.Empty);

	}

}
