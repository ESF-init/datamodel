﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.SupportClasses;

namespace VarioSL.Entities.EntityClasses
{
	public partial class CustomerAccountEntity
	{
		private static BCrypt bCrypt = new BCrypt();

		public static BCrypt BCrypt
		{
			get { return bCrypt; }
			set { bCrypt = value; }
		}


		public void Initialize(CustomerAccountState state)
		{
			if (!IsNew)
			{
				throw new InvalidOperationException("Only a new customer account can be initialized.");
			}
            
			State = state;
			SetPassword(Password);
		}

		public virtual bool CheckPassword(string password)
		{
			return bCrypt.CheckPassword(password, Password);
		}

		public virtual void SetPassword(string password)
		{
			Password = bCrypt.HashPassword(password, bCrypt.GenerateSalt());
		}

		// TODO: ContractHistory
		public virtual void ChangePassword(string oldPassword, string newPassword)
		{
			if (IsNew || !CheckPassword(oldPassword))
			{
				throw new InvalidOperationException(Translation.WrongPassword);
			}

			if (State != CustomerAccountState.Active)
			{
				throw new InvalidOperationException(Translation.AccountIsNotActive);
			}

            Contract.ContractHistoryItems.Add(new ContractHistoryEntity
                {
                    Text = string.Format("Changed password for account {0}", UserName)
                });

			SetPassword(newPassword);
		}

		public virtual void Login(string password)
		{
			if (IsNew || !CheckPassword(password))
			{
				throw new InvalidOperationException(Translation.UnknownAccountOrPassword);
			}

			switch (State)
			{
				case CustomerAccountState.Active:
					// OK for login
					break;
				case CustomerAccountState.Disabled:
					throw new InvalidOperationException("This account has been disabled, please contact the service center for further information.");
				case CustomerAccountState.Inactive:
					throw new InvalidOperationException(Translation.AccountIsNotActive);
				case CustomerAccountState.Closed:
					throw new InvalidOperationException("Account has been closed.");
				default:
					throw new InvalidOperationException("Account is in an unknown state.");
			}
		}


		public virtual void Activate()
		{
			if (State != CustomerAccountState.Inactive)
			{
				throw new InvalidOperationException("The account was already activated!");
			}

			State = CustomerAccountState.Active;
			Contract.State = ContractState.Active;

			Contract.ContractHistoryItems.Add(new ContractHistoryEntity()
			{
				Text = string.Format("Activated account {0} ({1}, {2})", UserName, Person.LastName, Person.FirstName)
			});
		}

		public virtual void ActivateWithNewPassword(string oldPassword, string newPassword)
		{
			if (IsNew || !CheckPassword(oldPassword))
			{
				throw new InvalidOperationException("Wrong account or password!");
			}

			SetPassword(newPassword);
			Activate();
		}

		/// <summary>
		/// A hash about the current state of the account to use in email links for validation
		/// </summary>
		public string AccountHash
		{
			get
			{
				var dataToHash = UserName + State + LastModified.Ticks.ToString();
				var bytes = System.Security.Cryptography.SHA1.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(dataToHash));
				return string.Concat(Array.ConvertAll(bytes, x => x.ToString("X2")));
			}
		}

	}
}
