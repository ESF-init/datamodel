﻿using System;
using System.Collections.Generic;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class ProductEntity
	{
		public bool HasAutoload { get { return HasAutoloadExp.Func(this); } }
		public static readonly PropertyExpression<ProductEntity, bool> HasAutoloadExp =
			new PropertyExpression<ProductEntity, bool>(src => src.AutoloadSettings.Any());

		public bool HasActiveAutoload { get { return HasActiveAutoloadExp.Func(this); } }
		public static readonly PropertyExpression<ProductEntity, bool> HasActiveAutoloadExp =
			new PropertyExpression<ProductEntity, bool>(src => src.AutoloadSettings.Any(autoload => autoload.SuspendedFrom > DateTime.Now || autoload.SuspendedTo < DateTime.Now));

		public bool IsTerminated { get { return IsTerminatedExp.Func(this); } }
		public static readonly PropertyExpression<ProductEntity, bool> IsTerminatedExp =
			new PropertyExpression<ProductEntity, bool>(src => new LinqMetaData().ProductTermination.Any(pt => pt.ProductID == src.ProductID && pt.TerminationStatus == (int)TerminationStatus.Final));

		public virtual bool IsAutoloadAllowed
		{
			get
			{
				return TicketType == TicketType.Pass || TicketType == TicketType.TopUp;
			}
		}

		public bool IsRefundAllowed { get { return IsRefundAllowedExp.Func(this); } }
		public static readonly PropertyExpression<ProductEntity, bool> IsRefundAllowedExp =
			new PropertyExpression<ProductEntity, bool>(src => !src.RefundID.HasValue);

		public virtual bool IsOrdered
		{
			get
			{
				return IsOrderedExp.Func(this);
			}
		}

        /// <summary>
		/// Use substring workaround for llblgen bug
		/// </summary>
		public static readonly PropertyExpression<ProductEntity, string> RefundCommentExp =
            new PropertyExpression<ProductEntity, string>(src => src.Refund != null ? src.Refund.Comment : src.LastUser.Substring(0, 0));
        public string RefundComment { get { return RefundCommentExp.Func(this); } }


        public static readonly PropertyExpression<ProductEntity, bool> IsActiveExp =
			new PropertyExpression<ProductEntity, bool>(src => src.IsLoaded
					&& src.ValidFrom < DateTime.Now
					&& src.ValidTo > DateTime.Now);

		public virtual bool IsActive
		{
			get
			{
				return IsActiveExp.Func(this);
			}
		}

		public virtual bool IsPending
		{
			get
			{
				return IsPendingExp.Func(this);
			}
		}

		public virtual string TicketRulePeriodDescription
		{
			get
			{
				if (Ticket != null && Ticket.RulePeriod != null && Ticket.RulePeriod.Description != null)
				{
					return Ticket.RulePeriod.Description;
				}

				return string.Empty;
			}
		}

		public virtual long TicketRulePeriodValidTimeUnit
		{
			get
			{
				if (Ticket != null && Ticket.RulePeriod != null)
				{
					return Ticket.RulePeriod.ValidTimeUnit;
				}

				return 0;
			}
		}
        
        public virtual long ExternalNumber
        {
            get
            {
                if (Ticket != null)
                {
                    return Ticket.ExternalNumber ?? 0;
                }
                return 0;
            }
        }

		public bool IsReloadable
		{
			get
			{
				return IsReloadableExp.Func(this);
			}
		}

		public string PrintedCardNumber
		{
			get
			{
				return PrintedCardNumberExp.Func(this);
			}
		}

		public long? SubsidyTransactionSubsidyTransactionID
		{
			get
			{
				return SubsidyTransactionSubsidyTransactionIDExp.Func(this);
			}
		}

		public int? SubsidyTransactionFullPrice
		{
			get
			{
				return SubsidyTransactionFullPriceExp.Func(this);
			}
		}

		public int? SubsidyTransactionReducedPrice
		{
			get
			{
				return SubsidyTransactionReducedPriceExp.Func(this);
			}
		}

		public bool ProductRelationExists { get { return ProductRelationExistsExp.Func(this); } }

		public long OrderID
		{
			get
			{
				return OrderDetails.Any() ? OrderDetails.First().OrderID : 0;
			}
		}

		public int? NumberOfTrips
		{
			get
			{
				var numberOfTrips = Ticket.AttributevalueCollectionViaAttributeValueList.Where(av => av.Attribute.AttribclassID.Value == TariffAttributeClass.MM_NUMBER_OF_TRIPS).FirstOrDefault();
				return numberOfTrips == null ? new Nullable<int>() : Convert.ToInt32(numberOfTrips.Name);
			}
		}

		public List<long> PaymentMethods
		{
			get
			{
				return Ticket.TicketDevicePaymentMethods.Select(paymentMethod => paymentMethod.DevicePaymentMethodID).ToList();
			}
		}

        public DateTimeExtensions.TimeUnit GetTimeUnit()
        {
            if (Ticket.TemporalType != null && Ticket.TemporalType.NxID.HasValue)
            {
                NxTemporalType tempType = (NxTemporalType)Ticket.TemporalType.NxID;
                switch (tempType)
                {
                    case NxTemporalType.ZK_AKTUELLE_WOCHE:
                    case NxTemporalType.ZK_FOLGEWOCHE:
                    case NxTemporalType.ZK_WOCHE_ABH_DO:
                    case NxTemporalType.ZK_WOCHE_ABH_X:
                        return DateTimeExtensions.TimeUnit.Week;
                    case NxTemporalType.ZK_AKTUELLER_MONAT:
                    case NxTemporalType.ZK_FOLGEMONAT:
                    case NxTemporalType.ZK_TAG_PLUS_1MONAT:
                    case NxTemporalType.ZK_MONAT_AB_20:
                    case NxTemporalType.ZK_TAG_PLUS_1MONAT_MINUS_1TAG:
                    case NxTemporalType.ZK_MONAT_ABH_X:
                        return DateTimeExtensions.TimeUnit.Month;
                    case NxTemporalType.ZK_FLEXIBLER_TAG:
                        return DateTimeExtensions.TimeUnit.Day;
                }
            }
            else if (Ticket.RulePeriod != null && Ticket.RulePeriod.RuleTypesValidTimeUnit != null)
            {
                return (DateTimeExtensions.TimeUnit) Ticket.RulePeriod.RuleTypesValidTimeUnit.RuleTypeNumber;
            }

            return DateTimeExtensions.TimeUnit.UNDEFINED;
        }

		#region Expressions

		public static readonly PropertyExpression<ProductEntity, bool> IsReloadableExp =
			new PropertyExpression<ProductEntity, bool>(p => p.TicketType == TicketType.Pass && p.IsLoaded);

		public static readonly PropertyExpression<ProductEntity, bool> IsOrderedExp =
			new PropertyExpression<ProductEntity, bool>(p => p.OrderDetails.Any(od => od.Order.State != OrderState.New));

		public static readonly PropertyExpression<ProductEntity, bool> IsPendingExp =
			new PropertyExpression<ProductEntity, bool>(p => p.IsPaid && !p.IsLoaded && !p.IsCanceled);

		public static readonly PropertyExpression<ProductEntity, bool> ProductRelationExistsExp =
			new PropertyExpression<ProductEntity, bool>(p => p.ProductRelation != null && !p.ProductRelation.IsNew);

		/// <summary>
		/// Use substring workaround for llblgen bug
		/// </summary>
		public static readonly PropertyExpression<ProductEntity, string> PrintedCardNumberExp =
			new PropertyExpression<ProductEntity, string>(p => p.Card == null ? p.LastUser.Substring(0, 0) : p.Card.PrintedNumber);

		public static readonly PropertyExpression<ProductEntity, long?> SubsidyTransactionSubsidyTransactionIDExp =
			new PropertyExpression<ProductEntity, long?>(p => p.SubsidyTransaction != null ? (long?)p.SubsidyTransaction.SubsidyTransactionID : null);

		public static readonly PropertyExpression<ProductEntity, int?> SubsidyTransactionFullPriceExp =
			new PropertyExpression<ProductEntity, int?>(p => p.SubsidyTransaction != null ? (int?)p.SubsidyTransaction.FullPrice : null);

		public static readonly PropertyExpression<ProductEntity, int?> SubsidyTransactionReducedPriceExp =
			new PropertyExpression<ProductEntity, int?>(p => p.SubsidyTransaction != null ? (int?)p.SubsidyTransaction.ReducedPrice : null);

		#endregion

	}
}
