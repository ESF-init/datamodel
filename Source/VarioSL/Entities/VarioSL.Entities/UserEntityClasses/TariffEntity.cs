﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.EntityClasses
{
    public partial class TariffEntity
    {
        public Dictionary<long, long> TicketIdToInternalNumber { get; set; }
        public Dictionary<string, LineEntity> LineCodeToLineDictionary { get; set; }
        public Dictionary<long, LineEntity> LineNumberToLineDictionary { get; set; }
        public Dictionary<string, long> StopCodeToStopNumberDictionary { get; set; }
        public Dictionary<TariffParameterType, string> Parameters { get; set; }
        public Dictionary<Tuple<long, string, string>, TranslationEntity> TranslationDictionary { get; set; }
        public List<PanelEntity> Panels { get; set; }

        public override string ToString()
        {
            return Description + "/" + ValidFrom + "v" + Version;
        }
    }
}
