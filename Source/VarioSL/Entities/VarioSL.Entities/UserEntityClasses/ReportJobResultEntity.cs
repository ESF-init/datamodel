﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Linq;
using VarioSL.Entities.EntityClasses;

namespace VarioSL.Entities.EntityClasses
{
	public partial class ReportJobResultEntity
	{

		public string ReportName { get { return ReportNameExp.Func(this); } }
		public static readonly PropertyExpression<ReportJobResultEntity, string> ReportNameExp =
			new PropertyExpression<ReportJobResultEntity, string>(a => a.Report == null ? a.ReportJob.Report.Name : a.Report.Name);

		#region HttpPath

		// lazy evaluated to make them replaceable in tests without db access
		internal static Lazy<string> pathReplace = new Lazy<string>(() => new LinqMetaData().ConfigurationDefinition.GetDefaultConfigurationValue("PathReplace"));
		internal static Lazy<string> reportRootDirectory = new Lazy<string>(() => new LinqMetaData().ConfigurationDefinition.GetDefaultConfigurationValue("ReportRootDirectory"));

		public string HttpPath { get { return HttpPathExp.Func(this); } }

		public static readonly PropertyExpression<ReportJobResultEntity, string> HttpPathExp =
			new PropertyExpression<ReportJobResultEntity, string>(a => a.httpPath());

		// By putting the logic in a method it will be evaluated in memory, not translated to sql
		private string httpPath()
		{
			try
			{
				var ret = FileName.Replace(reportRootDirectory.Value, pathReplace.Value).Replace('\\', '/');

				if (!Success || !ret.StartsWith("http"))
					return "";

				return ret;
			}
			catch
			{
				return "";
			}
		}

		#endregion

	}
}
