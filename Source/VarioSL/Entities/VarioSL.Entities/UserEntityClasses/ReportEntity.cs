﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class ReportEntity
	{
		public IQueryable<FilterCategoryEntity> FilterCategories 
		{ 
			get
			{
				return ReportCategory.FilterList.FilterListElements
					.Select(c => c.FilterCategory)
					.Distinct()
					.AsQueryable();
			}
		}
	}
}
