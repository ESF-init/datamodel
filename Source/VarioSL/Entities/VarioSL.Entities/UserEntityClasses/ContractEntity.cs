﻿using System;
using System.ComponentModel;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;
using VarioSL.Entities.UserExtensionClasses;

namespace VarioSL.Entities.EntityClasses
{
    public partial class ContractEntity
    {
        internal IQueryable<ConfigurationDefinitionEntity> ConfigurationForUnitTest;
        internal IQueryable<CardToContractEntity> CardToContractsForUnitTest;

		private Func<IQueryable<ConfigurationDefinitionEntity>> _configuration
        {
			get
			{
				return () => ConfigurationForUnitTest ?? new LinqMetaData().ConfigurationDefinition.AsQueryable();
			}
		}

		private Func<IQueryable<CardToContractEntity>> _cardToContracts
        {
			get
			{
				return () => CardToContractsForUnitTest ?? new LinqMetaData().CardToContract.AsQueryable();
			}
		}

        public void Initialize(ContractState state, string contractNumber)
        {
            State = state;
            ValidFrom = DateTime.Now;
            ContractNumber = contractNumber;
        }

        public bool NeedApproval
        {
            get
            {
                return (State == ContractState.Inactive && OrganizationID.HasValue);
            }
        }

        public virtual long OpenWorkItemID
        {
            get
            {
                var openWorkitem = WorkItems.FirstOrDefault(we => !we.IsAssigned && !we.IsExecuted);
                if (openWorkitem == null || openWorkitem.IsNew)
                {
                    return 0;
                }
                else
                {
                    return openWorkitem.WorkItemID;
                }
            }
        }

        public virtual bool IsMaxCardLimitReached
        {
            get
            {
                if (OrganizationID != null) // no limit for organization accounts
                {
                    return false;
                }

                var maxCards = _configuration().GetDefaultConfigurationValueWithFallback("MaxCardsForContract", 10);
                var numCards = _cardToContracts()
                    .Where(ctc => ctc.ContractID == ContractID)
                    .Where(ctc => ctc.Card.State == CardState.Active)
                    .Count();

                return numCards >= maxCards;
            }
        }

		/// <summary>
		/// A hash about the current state of the contract to use in email links for validation
		/// </summary>
		public string ContractHash
		{
			get
			{
				var dataToHash = (ClientID ?? 0).ToString() + State + LastModified.Ticks.ToString();
				var bytes = System.Security.Cryptography.SHA1.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(dataToHash));
				return string.Concat(Array.ConvertAll(bytes, x => x.ToString("X2")));
			}
		}

        public virtual long SchoolID { get { return SchoolIDExp.Func(this); } }

        public static readonly PropertyExpression<ContractEntity, long> SchoolIDExp =
            new PropertyExpression<ContractEntity, long>(c => c.ContractID);

        public virtual string SchoolNumber { get { return SchoolNumberExp.Func(this); } }

        public static readonly PropertyExpression<ContractEntity, string> SchoolNumberExp =
           new PropertyExpression<ContractEntity, string  >(c => c.OrganizationID.HasValue?c.Organization.OrganizationNumber:c.ContractNumber.Substring(0,0));

        public virtual string SchoolName { get { return SchoolNameExp.Func(this); } }

        public static readonly PropertyExpression<ContractEntity, string> SchoolNameExp =
           new PropertyExpression<ContractEntity, string  >(c => c.OrganizationID.HasValue?c.Organization.Name:c.ContractNumber.Substring(0,0));

        #region TRIMET Multiple Shipping Addresses

        public virtual Nullable<Int64> InvoiceAddressID
        {
            get { return GetLatestAddressID(AddressType.InvoiceAddress); }
        }

        public virtual Nullable<Int64> ShippingAddressID
        {
            get { return GetLatestAddressID(AddressType.ShippingAddress); }
        }

        [Browsable(false)]
        public virtual AddressEntity InvoiceAddress
        {
            get { return GetLatestAddress(AddressType.InvoiceAddress); }
            set { SetLatestContractAddress(value, AddressType.InvoiceAddress); }
        }

        [Browsable(false)]
        public virtual AddressEntity ShippingAddress
        {
            get { return GetLatestAddress(AddressType.ShippingAddress); }
            set { SetLatestContractAddress(value, AddressType.ShippingAddress); }
        }

        /// <summary>
        /// Retrieves the ID of the (by creation timestamp) latest assigned AddressEntity. 
        /// </summary>
        /// <param name="addressType"></param>
        /// <returns></returns>
        private Nullable<Int64> GetLatestAddressID(AddressType addressType)
        {
            var contractAddress = GetLatestContractAddress(addressType);
            return contractAddress == null ? new Nullable<Int64>() : contractAddress.AddressID;
        }

        /// <summary>
        /// Retrieves the (by creation timestamp) latest assigned AddressEntity.
        /// </summary>
        /// <param name="addressType"></param>
        /// <returns></returns>
        private AddressEntity GetLatestAddress(AddressType addressType)
        {
            var contractAddress = GetLatestContractAddress(addressType);
            return contractAddress == null ? new AddressEntity() : contractAddress.Address;
        }

        /// <summary>
        /// Retrieves the latest ContractAddressEntity for the given address type based on the creation timestamp.
        /// </summary>
        /// <param name="addressType"></param>
        /// <returns></returns>
        private ContractAddressEntity GetLatestContractAddress(AddressType addressType)
        {
            return GetMultiContractAddresses(false)
                .OrderByDescending(ca => ca.Created)
                .FirstOrDefault(ca => ca.AddressType == addressType && ca.IsRetired == false);
        }

        /// <summary>
        /// Hides the latest assigned address by retiring it if address is NULL or assignes it to the contract if not yet assigned.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="addressType"></param>
        private void SetLatestContractAddress(AddressEntity address, AddressType addressType)
        {
            if (address == null)
            {
                // Delete address, which is in our case to hide it by retiring it.
                RetireLatestContractAddress(addressType);
            }
            else
            {
                if (address.IsNew)
                {
                    // New address, retire the current active address and add the new one.
                    RetireLatestContractAddress(addressType);
                    GetMultiContractAddresses(false).Add(new ContractAddressEntity()
                    {
                        Contract = this,
                        Address = address,
                        AddressType = addressType,
                        Created = DateTime.Now,
                        IsRetired = false
                    });
                }
                else
                {
                    // Existing address, retrieve the mapping (ContractAddressEntity) and update it.
                    var contractAddress = GetMultiContractAddresses(false).SingleOrDefault(ca => ca.AddressID == address.AddressID && ca.AddressType == addressType);
                    if (contractAddress != null)
                    {
                        contractAddress.Address = address;
                    }
                }
            }
        }

        /// <summary>
        /// Retires latest assigned address of the contract.
        /// </summary>
        /// <param name="addressType"></param>
        private void RetireLatestContractAddress(AddressType addressType)
        {
            var contractAddress = GetLatestContractAddress(addressType);
            if (contractAddress != null)
            {
                contractAddress.IsRetired = true;
            }
        }

        #endregion
    }
}
