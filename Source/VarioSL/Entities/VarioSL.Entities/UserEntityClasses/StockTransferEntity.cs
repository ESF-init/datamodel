﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class StockTransferEntity
	{
		public virtual int NumCards { get { return NumCardsExp.Func(this); } }
		public static readonly PropertyExpression<StockTransferEntity, int> NumCardsExp =
			new PropertyExpression<StockTransferEntity, int>(src => src.CardStockTransfers.Count());

		public virtual string BatchNumber { get { return BatchNumberExp.Func(this); } }
		public static readonly PropertyExpression<StockTransferEntity, string> BatchNumberExp =
			new PropertyExpression<StockTransferEntity, string>(src => src.CardStockTransfers.Select(c => c.Card.BatchNumber).FirstOrDefault());
	}
}
