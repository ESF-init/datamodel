﻿using System;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.EntityClasses
{
    public partial class CardActionRequestEntity
    {

        public void SetProcessed(DateTime processDate)
        {
            IsProcessed = true;
            ProcessDate = processDate;
        }

        public void CreateCardRegisteredRequest(CardEntity card, long requestNo, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                Registration = (long)CardRegistrationState.Registered
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateCardRegisteredWithPaymentOptionRequest(CardEntity card, long requestNo, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                Registration = (long)CardRegistrationState.RegisteredWithPaymentOption
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateCardNotRegisteredRequest(CardEntity card, long requestNo, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                Registration = (long)CardRegistrationState.NotRegistered
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetDateOfBirthRequest(CardEntity card, long requestNo, DateTime dateOfBirth, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                DateOfBirth = dateOfBirth,
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetExpiryDateRequest(CardEntity card, long requestNo, DateTime expiryDate, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                ExpiryDate = expiryDate
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetIssueFlagRequest(CardEntity card, long requestNo, bool? issueFlag, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                IssueFlag = issueFlag
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetPrintedCardNumberRequest(CardEntity card, long requestNo, string printedCardNumber, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                PrintedCardNo = printedCardNumber
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetVoiceEnabledRequest(CardEntity card, long requestNo, bool enableVoice, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                VoiceFlag = enableVoice
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void CreateSetDisabledUserGroupRequest(CardEntity card, long requestNo, bool isDisabledUser, Source requestSource, bool setValid = true)
        {
            CreateSetUserGroupRequest(card, requestNo, GetDisabledTariffGroup(isDisabledUser), null, requestSource, setValid);
        }

        public void CreateSetUserGroupRequest(CardEntity card, long requestNo, long? newUserGroupId, DateTime? groupExpiry, Source requestSource, bool setValid = true, bool removeBirthday = false)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                UserGroupID = newUserGroupId,
                GroupExpiry = groupExpiry,
                RemoveBirthday = removeBirthday
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void AddDateOfBirth(DateTime dateOfBirth)
        {
            if (CardActionAttributes.FirstOrDefault() == null)
            {
                throw new InvalidOperationException("Cannot add date of birth to the action request.");
            }

            CardActionAttributes.First().DateOfBirth = dateOfBirth;
        }

        public void ModifyDateOfBirth(DateTime dateOfBirth, CardEntity card, long requestNo, long? newUserGroupId, DateTime? groupExpiry, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequestNotModify(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttributes = new CardActionAttributeEntity()
            {
                DateOfBirth = dateOfBirth,
                PrintedCardNo = card.PrintedNumber,
                UserGroupID = card.UserGroupID
            };

            CardActionAttributes.Add(cardActionAttributes);
        }

        public void CreateSetIssuerIdRequest(CardEntity card, long requestNo, long? issuerId, Source requestSource, bool setValid = true)
        {
            SetupSetCardAttributesRequest(card, requestNo, requestSource, setValid);

            CardActionAttributeEntity cardActionAttribute = new CardActionAttributeEntity()
            {
                IssuerID = issuerId
            };

            CardActionAttributes.Add(cardActionAttribute);
        }

        public void AddDisabledUserGroup(bool isDisabledUser)
        {
            AddUserGroup(GetDisabledTariffGroup(isDisabledUser));
        }

        public void AddUserGroup(long? newUserGroup)
        {
            if (CardActionAttributes.FirstOrDefault() == null)
                throw new InvalidOperationException("Cannot add user group to the action request.");

            CardActionAttributes.First().UserGroupID = newUserGroup;
        }

        private void SetupSetCardAttributesRequest(CardEntity card, long requestNo, Source requestSource, bool setValid)
        {
            CardNo = Int64.Parse(card.SerialNumber); // Problem for duplicate serial numbers
            RequestType = (int)CardActionRequestType.Modify;
            RequestNo = requestNo;
            RequestSource = (int)requestSource;
            CreationDate = DateTime.Now;
            IsValid = setValid;
            ClientID = 0; // TODO: get clientid
        }

        private void SetupSetCardAttributesRequestNotModify(CardEntity card, long requestNo, Source requestSource, bool setValid)
        {
            CardNo = Int64.Parse(card.SerialNumber); // Problem for duplicate serial numbers
            RequestType = (int)CardActionRequestType.SetCardAttributes;
            RequestNo = requestNo;
            RequestSource = (int)requestSource;
            CreationDate = DateTime.Now;
            IsValid = setValid;
            ClientID = 0; // TODO: get clientid
        }

        public static long GetNextCardActionRequestNumber(string cardNumber, ITransaction transaction)
        {
            long cardNumberParsed = 0;

            if (!long.TryParse(cardNumber, out cardNumberParsed))
                throw new ArgumentException(string.Format("{0}is an invalid card number.", cardNumber), "cardNumber");

            var greatestCardActionRequestNumber = new LinqMetaData(transaction).CardActionRequest
                .Where(actionRequest => actionRequest.CardNo == cardNumberParsed)
                .Max(actionRequest => actionRequest.RequestNo ?? 0);

	        if (greatestCardActionRequestNumber < 0)
		        greatestCardActionRequestNumber = 0;

	        return greatestCardActionRequestNumber + 1;
        }

        private static long? GetDisabledTariffGroup(bool isDisabledUser)
        {
            return isDisabledUser ? (long?)TariffUserGroup.Disabled : null;
        }
    }
}
