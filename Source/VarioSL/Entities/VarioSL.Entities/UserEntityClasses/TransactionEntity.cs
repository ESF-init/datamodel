﻿using VarioSL.Entities.UserEntityClasses;
using System.Linq;
using VarioSL.Entities.Linq;
using System.Collections.Generic;

namespace VarioSL.Entities.EntityClasses
{
	partial class TransactionEntity
	{
		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, string> TransactionExtensionCodeExp =
			new PropertyExpression<TransactionEntity, string>(src => src.TransactionExtension != null ? src.TransactionExtension.Code : src.PayCardNo.Substring(0, 0));
		public string TransactionExtensionCode { get { return TransactionExtensionCodeExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, string> TransactionExtensionTicketReferenceExp =
			new PropertyExpression<TransactionEntity, string>(src => src.TransactionExtension != null ? src.TransactionExtension.TicketReference : src.PayCardNo.Substring(0, 0));
		public string TransactionExtensionTicketReference { get { return TransactionExtensionTicketReferenceExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, string> TransactionExtensionNoteExp =
			new PropertyExpression<TransactionEntity, string>(src => src.TransactionExtension != null ? src.TransactionExtension.Note : src.PayCardNo.Substring(0, 0));
		public string TransactionExtensionNote { get { return TransactionExtensionNoteExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, long?> TransactionExtensionLatitudeExp =
			new PropertyExpression<TransactionEntity, long?>(src => src.TransactionExtension != null ? src.TransactionExtension.Latitude : null);
		public long? TransactionExtensionLatitude { get { return TransactionExtensionLatitudeExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, long?> TransactionExtensionLongitudeExp =
			new PropertyExpression<TransactionEntity, long?>(src => src.TransactionExtension != null ? src.TransactionExtension.Longitude : null);
		public long? TransactionExtensionLongitude { get { return TransactionExtensionLongitudeExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without TransactionExtension
		/// </summary>
		public static readonly PropertyExpression<TransactionEntity, string> TransactionExtensionRouteCodeExp =
			new PropertyExpression<TransactionEntity, string>(src => src.TransactionExtension != null ? src.TransactionExtension.RouteCode : src.PayCardNo.Substring(0, 0));
		public string TransactionExtensionRouteCode { get { return TransactionExtensionRouteCodeExp.Func(this); } }

		//public static readonly PropertyExpression<TransactionEntity, string> TransactionTransactionTypeTypeNameExp =
		//	new PropertyExpression<TransactionEntity, string>(src => src.TransactionType != null ? src.TransactionType.TypeName : src.PayCardNo.Substring(0, 0));
		//public string TransactionTransactionTypeTypeName { get { return TransactionTransactionTypeTypeNameExp.Func(this); } }

		public static readonly PropertyExpression<TransactionEntity, bool> DeviceBookingStateIsBookedExp =
			new PropertyExpression<TransactionEntity, bool>(
				src => new LinqMetaData()
					.DeviceBookingState
					.Any(dbs => dbs.DeviceBookingNumber == src.DeviceBookingState && dbs.IsBooked));
		public virtual bool DeviceBookingStateIsBooked
		{
			get { return DeviceBookingStateIsBookedExp.Func(this); }
		}

		public long? CardBalanceBeforeTransaction
		{
			get
			{
				if (!CardBalance.HasValue || !Price.HasValue)
					return 0;

				switch (TypeID)
				{
					case (0):
						return CardBalance.Value + Price.Value;
					case (3):
						if (new List<int> { 310, 312, 313, 314 }.Any(t => t == TikNo))
							return CardBalance.Value - Price.Value;
						return CardBalance.Value + Price.Value;

					case (7):
						return CardBalance.Value - Price.Value;
					case (14):
						return CardBalance.Value + Price.Value;

					default:
						return CardBalance.Value;
				}
			}
		}

		public static readonly PropertyExpression<TransactionEntity, string> TicketNameExp =
			new PropertyExpression<TransactionEntity, string>(src => new LinqMetaData()
					.Ticket
					.Where(tic => tic.InternalNumber == src.TikNo)
					.Where(tic => tic.TarifID == src.TariffID)
					.Select(tic => tic.Name)
					.FirstOrDefault());
		
		public virtual string TicketName
		{
			get { return TicketNameExp.Func(this); }
		}
		
	}
}
