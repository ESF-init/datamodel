﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public static class ExpressionExtensions
	{
		public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
		{
			ParameterExpression p = a.Parameters[0];
			ParameterVisitor visitor = new ParameterVisitor(b.Parameters[0], p);

			Expression body = Expression.AndAlso(a.Body, visitor.Visit(b.Body));
			return Expression.Lambda<Func<T, bool>>(body, p);
		}

		public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> a)
		{
			ParameterExpression p = a.Parameters[0];
			Expression body = Expression.Not(a.Body);
			return Expression.Lambda<Func<T, bool>>(body, p);
		}

		public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
		{
			ParameterExpression p = a.Parameters[0];
			ParameterVisitor visitor = new ParameterVisitor(b.Parameters[0], p);

			Expression body = Expression.OrElse(a.Body, visitor.Visit(b.Body));
			return Expression.Lambda<Func<T, bool>>(body, p);
		}

		public static Expression<Func<T, bool>> AndNot<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
		{
			ParameterExpression p = a.Parameters[0];
			ParameterVisitor visitor = new ParameterVisitor(b.Parameters[0], p);

			Expression body = Expression.AndAlso(a.Body, Expression.Not(visitor.Visit(b.Body)));
			return Expression.Lambda<Func<T, bool>>(body, p);
		}

		public static Expression<Func<T, bool>> OrNot<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
		{
			ParameterExpression p = a.Parameters[0];
			ParameterVisitor visitor = new ParameterVisitor(b.Parameters[0], p);

			Expression body = Expression.OrElse(a.Body, Expression.Not(visitor.Visit(b.Body)));
			return Expression.Lambda<Func<T, bool>>(body, p);
		}
	}
}
