﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqExpression = System.Linq.Expressions;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	/// <summary>
	/// Represents an expression that defines an entity's property of type string and provides general expression helpers.
	/// </summary>
	/// <typeparam name="Entity"></typeparam>
	public class StringPropertyExpression<Entity> : PropertyExpression<Entity, string>
	{

		public StringPropertyExpression(LinqExpression.Expression<Func<Entity, string>> exp)
			: base(exp)
		{

		}

		/// <summary>
		/// Provides a safe string contains expression.
		/// </summary>
		/// <param name="searchString"></param>
		/// <returns></returns>
		public LinqExpression.Expression<Func<Entity, bool>> Contains(string searchString)
		{
			LinqExpression.Expression<Func<Entity, bool>> exp;
			if (string.IsNullOrWhiteSpace(searchString))
				exp = (o) => true;
			else
			{
				searchString = searchString.ToLower();
				LinqExpression.ParameterExpression entityParam = this.Expression.Parameters[0];
				LinqExpression.Expression searchStringExpression = LinqExpression.Expression.Constant(searchString, searchString.GetType());
				LinqExpression.Expression toLowerCall = LinqExpression.Expression.Call(this.Expression.Body, typeof(string).GetMethod("ToLower", Type.EmptyTypes));
				LinqExpression.Expression containsCall = LinqExpression.Expression.Call(toLowerCall, typeof(string).GetMethod("Contains"), searchStringExpression);
				exp = LinqExpression.Expression.Lambda<Func<Entity, bool>>(containsCall, entityParam);
			}
			return exp;
		}
	}
}
