﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	/// <summary>
	/// Goes through an expression and replaces the ParameterExpression oldParameter with newParameter
	/// </summary>
	public class ParameterVisitor : ExpressionVisitor
	{
		private readonly ParameterExpression oldParameter;
		private readonly Expression newParameter;

		public ParameterVisitor(
			ParameterExpression oldParameter,
			Expression newParameter)
		{
			if (oldParameter == null) throw new ArgumentNullException("from");
			if (newParameter == null) throw new ArgumentNullException("newParameter");

			this.oldParameter = oldParameter;
			this.newParameter = newParameter;
		}
		protected override Expression VisitParameter(ParameterExpression node)
		{
			if (node == oldParameter) return newParameter;

			return node;
		}
	}
}
