﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public class PropertyExpression<Entity, Return>
	{
		public Expression<Func<Entity, Return>> Expression { get; private set; }

		public Func<Entity, Return> Func { get; private set; }
		
		internal PropertyExpression(Expression<Func<Entity, Return>> expression)
		{
			Initialize(expression);
		}

		internal void Initialize(Expression<Func<Entity, Return>> expression)
		{
			Expression = expression;
			Func = Expression.Compile();
		}
	}
}
