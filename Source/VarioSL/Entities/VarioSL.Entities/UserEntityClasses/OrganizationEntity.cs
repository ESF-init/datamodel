﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class OrganizationEntity
	{
		public List<long> ContractIDs
		{
			get { return ContractIDsExp.Func(this); }
		}

		public static readonly PropertyExpression<OrganizationEntity, List<long>> ContractIDsExp =
			new PropertyExpression<OrganizationEntity,List<long>>(
				org => org.Contracts
					.Select(contract => contract.ContractID)
					.ToList());
	}
}
