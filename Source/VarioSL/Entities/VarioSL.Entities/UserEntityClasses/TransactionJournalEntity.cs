﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.EntityClasses
{
    public partial class TransactionJournalEntity
    {
        public bool RepresentsRequest { get; set; }
        public string TicketName { get; set; }
        public DevicePaymentMethod PaymentMethod { get; set; }
        public long? CheckIdMode { get; set; }
        public DateTime? CappedUntilBefore { get; set; }
        public DateTime? CappedUntilAfterwards { get; set; }
        public string AchievedCapName { get; set; }
        public string AchievedCapDescription { get; set; }
        public bool UpdateOnly { get; set; }
        public bool CancellationTarget { get; set; }
        public bool InfluencesWhitelist { get; set; }
        public Exception ThrownException { get; set; }
        public bool SkipJournal { get; set; }
        public string AutoSelectedTicketName { get; set; }
        public bool IsCurrentVirtualCharge { get; set; }
        public bool IsCurrentDistributionRequest { get; set; }
        public bool IsCurrentOpenLoopRefund { get; set; }

        #region Mapping

        public TransactionTransportState ResponseState { get { return TransportState; } set { TransportState = value; } }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}/{4}", TransactionType, DeviceTime, ResultType, TransactionID, PurseCredit);
        }
    }
}
