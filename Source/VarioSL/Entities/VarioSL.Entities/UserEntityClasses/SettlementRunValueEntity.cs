﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.CollectionClasses;
using System.Globalization;

namespace VarioSL.Entities.EntityClasses
{
	public partial class SettlementRunValueEntity
	{
		public static readonly PropertyExpression<SettlementRunValueEntity, bool> HasQueryValuesExp =
			new PropertyExpression<SettlementRunValueEntity, bool>(src => src.SettlementQueryValues.Count() > 0);

		public virtual bool HasQueryValues
		{
			get { return HasQueryValuesExp.Func(this); }
		}
	}
}
