﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
    public partial class TicketEntity
    {
        public override string ToString()
        {
            return "[" + InternalNumber + "] " + Name;
        }
    }
}
