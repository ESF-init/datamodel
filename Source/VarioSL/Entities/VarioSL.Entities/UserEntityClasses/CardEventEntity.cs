﻿using System;

namespace VarioSL.Entities.EntityClasses
{
	public partial class CardEventEntity : ICardEventGenerator
	{
		private static ICardEventGenerator _eventGenerator;
		public static ICardEventGenerator EventGenerator
		{
			get
			{
				if (_eventGenerator == null)
				{
					_eventGenerator = new CardEventEntity();
				}
				return _eventGenerator;
			}
		
			set { _eventGenerator = value; }
		}

		#region ICardEventGenerator Implementation

		public CardEventEntity CreateEventIssue(CardEntity card)
		{
			if (card == null || card.IsNew)
			{
				throw new InvalidOperationException("Smartcard is not in the system.");
			}

			return new CardEventEntity()
				{
					CardEventType = Enumerations.CardEventType.Issue,
					Description = "Smartcard was issued.",
					EventTime = DateTime.Now
				};
		}

		public CardEventEntity CreateEventAssign(CardEntity card, ContractEntity contract)
		{
			if (card == null || card.IsNew)
			{
				throw new InvalidOperationException("Smartcard is not in the system.");
			}

			if (contract == null || contract.IsNew)
			{
				throw new InvalidOperationException("Contract is not in the system.");
			}

			return new CardEventEntity()
				{
					CardEventType = Enumerations.CardEventType.Assign,
					Description = string.Format("Smartcard was assigned to contract ({0}).", contract.ContractNumber),
					ContractID = contract.ContractID,
					EventTime = DateTime.Now
				};
		}

		public CardEventEntity CreateEventReplace(CardEntity oldCard, CardEntity newCard, bool isNewCard)
		{
			if (oldCard == newCard)
			{
				throw new InvalidOperationException("Cannot replace card with same card.");
			}

			if (oldCard == null || oldCard.IsNew || newCard == null || newCard.IsNew)
			{
				throw new InvalidOperationException("Smartcard is not in the system.");
			}

			return new CardEventEntity()
				{
					CardEventType = Enumerations.CardEventType.Replace,
					Description = string.Format("{0} card printed card number {1}.",
						isNewCard ? "Old" : "New",
						isNewCard ? oldCard.PrintedNumber : newCard.PrintedNumber),
					Reason = isNewCard ? "Is replacement" : "Was replaced",
					EventTime = DateTime.Now,
					CardID = oldCard.CardID
				};
		}

		public CardEventEntity CreateEventRevoke(CardEntity card)
		{
			if (card == null || card.IsNew)
				throw new InvalidOperationException("Smartcard is not in the system.");

			return new CardEventEntity()
			{
				CardEventType = Enumerations.CardEventType.Revoke,
				Description = string.Format("Smartcard was revoked."),
				EventTime = DateTime.Now
			};
		}

		public CardEventEntity CreateEventDepositAdd(CardEntity card, bool isDataSync)
		{
			if (card == null || card.IsNew)
				throw new InvalidOperationException("Smartcard is not in the system.");

			return new CardEventEntity()
			{
				CardEventType = Enumerations.CardEventType.Deposit,
				Description = "Added deposit to card.",
				Reason = isDataSync ? "Added (Sync)" : "Added",
				EventTime = DateTime.Now,
			};
		}

		public CardEventEntity CreateEventDepositRemove(CardEntity card)
		{
			if (card == null || card.IsNew)
				throw new InvalidOperationException("Smartcard is not in the system.");

			return new CardEventEntity()
			{
				CardEventType = Enumerations.CardEventType.Deposit,
				Description = "Removed deposit from card.",
				Reason = "Removed",
				EventTime = DateTime.Now,
			};
		}

		#endregion

	}
}
