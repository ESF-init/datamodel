﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class CardHolderEntity
	{
		public virtual long CardHolderID { get { return CardHolderIDExp.Func(this); } }

		public static readonly PropertyExpression<CardHolderEntity, long> CardHolderIDExp =
			new PropertyExpression<CardHolderEntity, long>(c => c.PersonID);

				public virtual List<long> CardIDs { get { return CardIDsExp.Func(this); } }

		public static readonly PropertyExpression<CardHolderEntity, List<long>> CardIDsExp =
			new PropertyExpression<CardHolderEntity, List<long>>(ch => ch.Cards.Select(c => c.CardID).ToList());

		public virtual long StudentID { get { return StudentIDExp.Func(this); } }

		public static readonly PropertyExpression<CardHolderEntity, long> StudentIDExp =
			new PropertyExpression<CardHolderEntity, long>(c => c.PersonID);


	}
}
