﻿using System;
using System.Collections.Generic;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class CardEntity
	{
		internal static IQueryable<ProductEntity> ProductData;

		private static readonly Func<IQueryable<ProductEntity>> _productData = () => ProductData ?? new LinqMetaData().Product.AsQueryable();

		public virtual List<long> ContractIDs => ContractIDsExp.Func(this);

        public static readonly PropertyExpression<CardEntity, List<long>> ContractIDsExp =
			new PropertyExpression<CardEntity, List<long>>(src => src.CardsToContracts.Select(ctc => ctc.ContractID).ToList());

		public virtual int ActiveProducts => ActiveProductsExp.Func(this);

        public static readonly PropertyExpression<CardEntity, int> ActiveProductsExp =
			new PropertyExpression<CardEntity, int>(src => _productData()
                .Where(p => p.CardID == src.CardID)
                .Count(ProductEntity.IsActiveExp.Expression
                    .And(p => p.TicketType == TicketType.Pass 
                              || p.TicketType == TicketType.NormalTicket 
                              || p.TicketType == TicketType.DynamicNumberOfPersonsPass)));

		public virtual int PendingProducts => PendingProductsExp.Func(this);

        public static readonly PropertyExpression<CardEntity, int> PendingProductsExp =
			new PropertyExpression<CardEntity, int>(src => _productData()
                .Where(p => p.CardID == src.CardID)
                .Count(ProductEntity.IsPendingExp.Expression
                    .And(p => p.TicketType == TicketType.Pass 
                              || p.TicketType == TicketType.NormalTicket 
                              || p.TicketType == TicketType.DynamicNumberOfPersonsPass)));

		public static readonly PropertyExpression<CardEntity, bool> IsChangeOfCardTicketAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => !_productData().Any(p => p.CardID == src.CardID && p.TicketType != TicketType.PhysicalCard) && src.State == CardState.Active);

		public bool IsChangeOfCardTicketAllowed
		{
			get { return IsChangeOfCardTicketAllowedExp.Func(this); }
		}

		public virtual bool IsLimitedUse
		{
			get { return CardPhysicalDetail.BodyType == CardBodyType.Paper; }
		}

		public virtual bool IsAddAutoloadAllowed
		{
			get { return IsAddTopUpAllowed && !HasTopUpAutoload; }
		}

		public virtual bool IsAddTopUpAllowed
		{
			get { return IsActive && !IsLimitedUse && !IsNew; }
		}

		public static readonly PropertyExpression<CardEntity, bool> HasTopUpAutoloadExp =
			new PropertyExpression<CardEntity, bool>(src => src.Products.Any(p => p.TicketType == TicketType.TopUp && p.AutoloadSettings.Any()));
		public virtual bool HasTopUpAutoload
		{
			get { return HasTopUpAutoloadExp.Func(this); }
		}

		public static readonly PropertyExpression<CardEntity, ProductEntity> DepositProductExp =
			new PropertyExpression<CardEntity, ProductEntity>(src => src.Products.FirstOrDefault(p => p.TicketType == TicketType.Deposit && !p.IsCanceled && p.ValidFrom == DateTime.MinValue));
		public virtual ProductEntity DepositProduct
		{
			get { return DepositProductExp.Func(this); }
		}

		public static readonly PropertyExpression<CardEntity, bool> HasDepositProductExp =
			new PropertyExpression<CardEntity, bool>(src => src.Products.Any(p => p.TicketType == TicketType.Deposit && !p.IsCanceled && p.ValidFrom == DateTime.MinValue));
		public virtual bool HasDepositProduct
		{
			get { return HasDepositProductExp.Func(this); }
		}

		public virtual bool IsActive
		{
			get { return State == CardState.Active; }
		}

		public virtual long? OrganizationID
		{
			get
			{
				var contract = Contracts.FirstOrDefault(c => c.OrganizationID != null);
				if (contract != null)
					return contract.OrganizationID;
				return null;
			}
		}

		public virtual long CardTicketID
		{
			get
			{
				long ticketID = 0;
				if (Products.Count > 0)
				{
					var physicalCardProduct = Products
						.Where(p => p.TicketType == TicketType.PhysicalCard)
						.Where(p => p.ValidFrom <= DateTime.Now)
						.Where(p => !p.IsCanceled)
						.FirstOrDefault();
					if (physicalCardProduct != null)
						ticketID = physicalCardProduct.TicketID;
				}
				return ticketID;
			}
		}

		public virtual void AddContract(ContractEntity contract)
		{
			if (IsNew)
			{
				throw new InvalidOperationException(Translation.SmartcardNotInSystem);
			}

			if (contract == null || contract.IsNew)
			{
				throw new InvalidOperationException("No valid contract was provided.");
			}

			if (IsRegisteredToCurrentContract(contract))
			{
				throw new InvalidOperationException(Translation.SmartcardAlreadyRegistered_Specific);
			}

			if (IsSharedCardAllowed(contract))
			{
				throw new InvalidOperationException(Translation.SmartcardAlreadyRegistered_Unspecific);
			}

			if (InventoryState != Enumerations.InventoryState.Issued)
			{
				throw new InvalidOperationException(Translation.SmartcardIsNotIssued);
			}

			if (State != CardState.Active)
			{
				throw new InvalidOperationException(Translation.SmartcardIsNotActive);
			}

			if (IsLimitedUse)
			{
				throw new InvalidOperationException(Translation.SmartcardIsPaper);
			}

			contract.ContractHistoryItems.Add(new ContractHistoryEntity()
			{
				LastCustomer = Environment.MachineName,
				Text = string.Format("Registered smartcard ({0}) to contract", PrintedNumber)
			});

			CardsToContracts.Add(new CardToContractEntity() //// Workaround as n:m navigators are readonly
			{
				CardID = CardID,
				ContractID = contract.ContractID
			});

			CardEvents.Add(CardEventEntity.EventGenerator.CreateEventAssign(this, contract));
		}

		private bool IsRegisteredToCurrentContract(ContractEntity contract)
		{
			return Contracts.Any(c => c.ContractID == contract.ContractID);
		}

		private bool IsSharedCardAllowed(ContractEntity contract)
		{
			// registration of a card by corporate account should be the first registration for this smart card. 
			if (contract.OrganizationID != null && Contracts.Count > 0)
				return true;

			if (Contracts.Count > 1)
				return true;

			if (Contracts.Count == 1 && !IsShared)
				return true;

			return false;
		}

		public virtual void Issue(DateTime expiryDate)
		{
			if (IsNew)
				throw new InvalidOperationException("The smartcard is not in the system.");
			if (State != CardState.Inactive)
				throw new InvalidOperationException("The smartcard is not in a state for issuing.");
			if (InventoryState != InventoryState.InInventory && InventoryState != InventoryState.Delivered)
				throw new InvalidOperationException("The smartcard is not in inventory.");

			State = CardState.Active;
			InventoryState = InventoryState.Issued;
			Expiration = expiryDate;

			CardEvents.Add(CardEventEntity.EventGenerator.CreateEventIssue(this));
		}

		public virtual void Revoke()
		{
			if (IsNew)
				throw new InvalidOperationException("The smartcard is not in the system.");
			if (!IsRevokeAllowed)
				throw new InvalidOperationException("The smartcard is not in revokable state.");
			//CANNOT BE DONE, because Pending purse needs to be evaluated, too (PurseBalance + PendingPurse == 0).
			//if (PurseBalance != 0)
			//	throw new InvalidOperationException("Purse must be refunded, before revocation.");

			InventoryState = InventoryState.Revoked;
			CardEvents.Add(CardEventEntity.EventGenerator.CreateEventRevoke(this));
		}

		public virtual IQueryable<ProductEntity> GetCurrentProducts()
		{
			return Products
				.Where(p => (p.ValidTo >= DateTime.Now || (p.ValidFrom == DateTime.MinValue && p.ValidateWhenSelling == false)))
				.Where(p => p.IsCanceled == false)
				.Where(p => p.IsPaid == true)
				.Where(p => p.TicketType == TicketType.Pass || p.TicketType == TicketType.NormalTicket || p.TicketType == TicketType.Goodwill || p.TicketType == TicketType.DynamicNumberOfPersonsPass)
				.Where(p => !p.Expiry.HasValue || p.Expiry >= DateTime.Now || p.RemainingValue.HasValue && p.RemainingValue > 0)
				//.Where(p => (!p.RemainingValue.HasValue && p.Expiry.HasValue && p.Expiry.Value >= DateTime.Now) || p.RemainingValue.Value > 0 || p.RemainingValue == 0 && p.Expiry.HasValue && p.Expiry.Value >= DateTime.Now)
				.AsQueryable();
		}

		public virtual int GetCurrentPassesCount()
		{
			return GetCurrentProducts().Count();
		}

		public virtual bool AddCardTicket(ProductEntity product)
		{
			if (product.TicketType != TicketType.PhysicalCard)
				throw new InvalidOperationException("The ticket type must be PhysicalCard.");

			if (CardTicketID != 0)
			{
				Products[0].TicketID = product.TicketID;
				Products[0].Price = product.Price;
				return false;
			}
			else
			{
				product.ValidFrom = DateTime.MinValue;
				product.ValidTo = DateTime.MaxValue;
				product.IsPaid = true;
				product.IsLoaded = true;

				Products.Add(product);
			}

			return true;
		}

		public static readonly PropertyExpression<CardEntity, bool> IsTopUpAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active && src.CardPhysicalDetail.ChipTypeID != 13 && src.Expiration > DateTime.Now);
		public bool IsTopUpAllowed { get { return IsTopUpAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsInitalizeAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.CardPhysicalDetail.ChipTypeID != 13);
		public bool IsInitalizeAllowed { get { return IsInitalizeAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsIssueAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.InventoryState == InventoryState.Delivered);
		public bool IsIssueAllowed { get { return IsIssueAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsBlockAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsBlockAllowed { get { return IsBlockAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsRevokeAllowedExp =
			new PropertyExpression<CardEntity, bool>(src =>
				src.InventoryState != InventoryState.Revoked && (src.State == CardState.Active || src.State == CardState.Blocked));
		public bool IsRevokeAllowed { get { return IsRevokeAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsUnblockAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Blocked && src.BlockingReason != BlockingReason.Revoked);
		public bool IsUnblockAllowed { get { return IsUnblockAllowedExp.Func(this); } }

		public static readonly CardState[] ReplacebleCardStates = { CardState.Blocked, CardState.ReplacementOrdered, CardState.Expired };
		public static readonly PropertyExpression<CardEntity, bool> IsReplaceAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => ReplacebleCardStates.Contains(src.State) && src.BlockingReason != BlockingReason.Revoked);
		public bool IsReplaceAllowed { get { return IsReplaceAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsAddProductAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active && src.Expiration > DateTime.Now);
		public bool IsAddProductAllowed { get { return IsAddProductAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsRemoveProductAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsRemoveProductAllowed { get { return State == CardState.Active; } }

		public static readonly PropertyExpression<CardEntity, bool> IsRegisterAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsRegisterAllowed { get { return CardPhysicalDetail.ChipTypeID != 13; } }

		public static readonly PropertyExpression<CardEntity, bool> IsRemoveAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.CardsToContracts.Count() != 0);
		public bool IsRemoveAllowed { get { return IsRemoveAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsShareAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsShareAllowed { get { return IsShareAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsUnshareAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.IsShared && src.CardsToContracts.Count() == 1);
		public bool IsUnshareAllowed { get { return IsUnshareAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsAddCardHolderAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active && src.CardHolder == null);
		public bool IsAddCardHolderAllowed { get { return IsAddCardHolderAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsRemoveCardHolderAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsRemoveCardHolderAllowed { get { return IsRemoveCardHolderAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsAutoloadAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Active);
		public bool IsAutoloadAllowed { get { return IsAutoloadAllowedExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsAutoloadTopUpConfiguredExp =
			new PropertyExpression<CardEntity, bool>(src => src.Products.Any(p => p.TicketType == TicketType.TopUp && p.AutoloadSettings.Any()));
		public bool IsAutoloadTopUpConfigured { get { return IsAutoloadTopUpConfiguredExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, bool> IsReactivationAllowedExp =
			new PropertyExpression<CardEntity, bool>(src => src.State == CardState.Dormant);
		public bool IsReactivationAllowed { get { return IsReactivationAllowedExp.Func(this); } }

		/// <summary>
		/// Use substring workaround for llblgen bug
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> CardHolderFirstNameExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null ? src.CardHolder.FirstName : src.PrintedNumber.Substring(0, 0));
		public string CardHolderFirstName { get { return CardHolderFirstNameExp.Func(this); } }

		/// <summary>
		/// Use substring workaround for llblgen bug
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> CardHolderLastNameExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null ? src.CardHolder.LastName : src.PrintedNumber.Substring(0, 0));
		public string CardHolderLastName { get { return CardHolderLastNameExp.Func(this); } }

		/// <summary>
		/// Use substring workaround for llblgen bug
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> CardHolderEMailExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null ? src.CardHolder.Email : src.PrintedNumber.Substring(0, 0));
		public string CardHolderEMail { get { return CardHolderEMailExp.Func(this); } }


		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, DateTime> CardHolderDateOfBirthExp =
			new PropertyExpression<CardEntity, DateTime>(src => src.CardHolder != null ? src.CardHolder.DateOfBirth : DateTime.MinValue);
		public DateTime CardHolderDateOfBirth { get { return CardHolderDateOfBirthExp.Func(this); } }


		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> CardHolderOrganizationalIdentifierExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null ? src.CardHolder.OrganizationalIdentifier : src.PrintedNumber.Substring(0, 0));
		public string CardHolderOrganizationalIdentifier { get { return CardHolderOrganizationalIdentifierExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, int?> CardHolderUserGroupExp =
			new PropertyExpression<CardEntity, int?>(src => src.CardHolder != null ? src.CardHolder.UserGroup : 0);
		public int? CardHolderUserGroup { get { return CardHolderUserGroupExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, DateTime?> CardHolderGroupExpiryExp =
			new PropertyExpression<CardEntity, DateTime?>(src => src.CardHolder != null ? src.CardHolder.UserGroupExpiry : DateTime.MinValue);
		public DateTime? CardHolderGroupExpiry { get { return CardHolderGroupExpiryExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, long?> CardHolderSchoolYearIDExp =
			new PropertyExpression<CardEntity, long?>(src => src.CardHolder != null ? src.CardHolder.SchoolYearID : 0);
		public long? CardHolderSchoolYearID { get { return CardHolderSchoolYearIDExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> ContractOrganizationOrganizationNumberExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null && src.CardHolder.Contract != null && src.CardHolder.Contract.Organization != null ? src.CardHolder.Contract.Organization.OrganizationNumber : src.PrintedNumber.Substring(0, 0));
		public string ContractOrganizationOrganizationNumber { get { return ContractOrganizationOrganizationNumberExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, long?> CardHolderContractOrganizationFrameOrganizationIDExp =
			new PropertyExpression<CardEntity, long?>(src => src.CardHolder == null ? 0 : src.CardHolder.Contract == null ? 0 : src.CardHolder.Contract.Organization == null ? 0 : !src.CardHolder.Contract.Organization.FrameOrganizationID.HasValue ? 0 : src.CardHolder.Contract.Organization.FrameOrganizationID);
		public long? CardHolderContractOrganizationFrameOrganizationID { get { return CardHolderContractOrganizationFrameOrganizationIDExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, string> OrganizationNumberExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null && src.CardHolder.Contract != null && src.CardHolder.Contract.Organization != null && src.CardHolder.Contract.Organization.Organization != null ? src.CardHolder.Contract.Organization.Organization.OrganizationNumber : src.PrintedNumber.Substring(0, 0));
		public string OrganizationNumber { get { return OrganizationNumberExp.Func(this); } }

		/// <summary>
		/// Use workaround for projection of card without cardholder
		/// </summary>
		public static readonly PropertyExpression<CardEntity, long?> CardHolderContractIDExp =
			new PropertyExpression<CardEntity, long?>(src => src.CardHolder != null  ? src.CardHolder.ContractID : 0);
		public long? CardHolderContractID { get { return CardHolderContractIDExp.Func(this); } }


		public static readonly PropertyExpression<CardEntity, string> CardHolderContractOrganizationOrganizationNumberExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null && src.CardHolder.Contract != null && src.CardHolder.Contract.Organization != null ? src.CardHolder.OrganizationalIdentifier : src.PrintedNumber.Substring(0, 0));
		public string CardHolderContractOrganizationOrganizationNumber { get { return CardHolderContractOrganizationOrganizationNumberExp.Func(this); } }

		public static readonly PropertyExpression<CardEntity, string> CardHolderContractContractNumberExp =
			new PropertyExpression<CardEntity, string>(src => src.CardHolder != null && src.CardHolder.Contract != null ? src.CardHolder.Contract.ContractNumber : src.PrintedNumber.Substring(0,0));
		public string CardHolderContractContractNumber { get { return CardHolderContractContractNumberExp.Func(this);} }


        public static readonly PropertyExpression<CardEntity, string> ReplacedCardPrintedNumberExp =
            new PropertyExpression<CardEntity, string>(src => src.ReplacedCardID != null ? src.ReplacedCard.PrintedNumber : src.PrintedNumber.Substring(0,0));

	    public string ReplacedCardPrintedNumber => ReplacedCardPrintedNumberExp.Func(this);

	}
}
