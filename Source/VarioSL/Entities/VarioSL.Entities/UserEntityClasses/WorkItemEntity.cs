﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.EntityClasses
{
	public partial class WorkItemEntity
	{
		public virtual bool IsAssigned
		{
			get { return IsAssignedExp.Func(this); }
		}

		public virtual bool IsExecuted
		{
			get { return IsExecutedExp.Func(this); }
		}

		public virtual bool IsAssignAllowed
		{
			get { return IsAssignAllowedExp.Func(this); }
		}

		public virtual bool IsUnassignAllowed
		{
			get { return IsUnassignAllowedExp.Func(this); }
		}

		public virtual bool IsExecuteAllowed
		{
			get { return IsExecuteAllowedExp.Func(this); }
		}

		public virtual bool IsUpdateMemoAllowed
		{
			get { return IsUpdateMemoAllowedExp.Func(this); }
		}

		public virtual bool HasMemo
		{
			get { return HasMemoExp.Func(this); }
		}

		public virtual WorkItemCategory Category
		{
			get { return CategoryExp.Func(this); }
		}

		public virtual void SetAssigned()
		{
			if (IsAssignAllowed)
				Assigned = DateTime.Now;
			else
				throw new InvalidOperationException("Work item can not be assigned.");
		}

		public virtual void SetUnassigned()
		{
			if (IsUnassignAllowed)
				Assigned = DateTime.MinValue;
			else
				throw new InvalidOperationException("Work item can not be unassigned.");
		}

		public virtual void SetExecuted()
		{
			if (IsExecuteAllowed)
				Executed = DateTime.Now;
			else
				throw new InvalidOperationException("Work item can not be executed.");
		}

		public virtual void UpdateMemo(string newMemo)
		{
			if (IsUpdateMemoAllowed)
				Memo = newMemo.Trim();
			else
				throw new InvalidOperationException("Work item memo can not be updated.");

		}

		#region Expressions

		public static readonly PropertyExpression<WorkItemEntity, bool> IsAssignedExp =
			new PropertyExpression<WorkItemEntity, bool>(a => a.Assigned != DateTime.MinValue);

		public static readonly PropertyExpression<WorkItemEntity, bool> IsExecutedExp =
			new PropertyExpression<WorkItemEntity, bool>(a => a.Executed != DateTime.MinValue);

		public static readonly PropertyExpression<WorkItemEntity, bool> HasMemoExp =
			new PropertyExpression<WorkItemEntity, bool>(a => !string.IsNullOrEmpty(a.Memo));

		public static readonly PropertyExpression<WorkItemEntity, bool> IsAssignAllowedExp =
			new PropertyExpression<WorkItemEntity, bool>(IsAssignedExp.Expression.Not().AndNot(IsExecutedExp.Expression));

		public static readonly PropertyExpression<WorkItemEntity, bool> IsUnassignAllowedExp =
			new PropertyExpression<WorkItemEntity, bool>(IsAssignedExp.Expression.AndNot(IsExecutedExp.Expression));

		public static readonly PropertyExpression<WorkItemEntity, bool> IsExecuteAllowedExp =
			new PropertyExpression<WorkItemEntity, bool>(IsExecutedExp.Expression.Not());

		public static readonly PropertyExpression<WorkItemEntity, bool> IsUpdateMemoAllowedExp =
			new PropertyExpression<WorkItemEntity, bool>(IsAssignedExp.Expression.AndNot(IsExecutedExp.Expression));

		public static readonly PropertyExpression<WorkItemEntity, WorkItemCategory> CategoryExp = 
			new PropertyExpression<WorkItemEntity, WorkItemCategory>(wi => GetWorkItemCategory(wi));

		#endregion

		#region Private helper

		private static WorkItemCategory GetWorkItemCategory(WorkItemEntity workItem) 
		{
			if (workItem is OrderWorkItemEntity)
				return WorkItemCategory.Order;

			if (workItem is CardWorkItemEntity)
				return WorkItemCategory.Card;

			if (workItem is ContractWorkItemEntity)
			{
				var contractWorkItem = workItem as ContractWorkItemEntity;
				if (contractWorkItem.Contract.OrganizationID.HasValue)
					return WorkItemCategory.OrganizationContract;
				else
					return WorkItemCategory.CustomerContract;
			}

			//Fallback
			return WorkItemCategory.Unknown;
		}

		#endregion
	}
}
