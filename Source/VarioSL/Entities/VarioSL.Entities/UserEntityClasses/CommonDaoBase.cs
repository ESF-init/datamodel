﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;
using System.Data.Common;
using System.Security.Principal;
using System.Threading;

namespace VarioSL.Entities.DaoClasses
{
	public partial class CommonDaoBase : DaoBase
    {
        public static bool UseEncryption = false;
		public static bool Set_NLS_SORT = false;
		public static string NLS_SORT = "BINARY";

        public override DbConnection CreateConnection(string connectionString)
        {
            var con = base.CreateConnection(connectionString);
            if (!UseEncryption && !Set_NLS_SORT) 
                return con;

            if (con.GetType().FullName == "SD.Tools.OrmProfiler.Interceptor.ProfilerDbConnection")
            {
                //Workaround:
                //  When ORMProfiler (InterceptorCore) is enabled, the original connection is wrapped.
                //  We have to subscribe to the original connection's StateChange event, not the wrapper's event.
                var originalCon = (DbConnection) ((dynamic) con).WrappedConnection;
                originalCon.StateChange += Con_StateChange;
            }
            else
            {
                con.StateChange += Con_StateChange;
            }

            return con;
        }
        private void Con_StateChange(object sender, StateChangeEventArgs e)
        {
            if (e.OriginalState == ConnectionState.Closed && e.CurrentState == ConnectionState.Open)
            {
                if (!(sender is DbConnection con))
                    return;
				if (UseEncryption)   
                    EnableEncryption(con);
                if (Set_NLS_SORT)
                    SetNlsSort(con, NLS_SORT);
            }
        }

		private void EnableEncryption(DbConnection connection)
		{
            using (var com = connection.CreateCommand() as IDbCommand)
            {
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = "ENABLEENCRYPTION";
                var param = com.CreateParameter();
                param.ParameterName = "UserName";
                param.Value = Thread.CurrentPrincipal.Identity.Name ?? WindowsIdentity.GetCurrent().Name;
                com.Parameters.Add(param);
                com.ExecuteNonQuery();
            }
		}

		private void SetNlsSort(DbConnection connection, string sort)
		{
			using (var com = connection.CreateCommand())
			{
				com.CommandType = CommandType.Text;
				com.CommandText = $"ALTER SESSION SET NLS_SORT={sort}";
				com.ExecuteNonQuery();
			}
		}
    }
}
