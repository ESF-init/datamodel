﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.HelperClasses;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class NumberRangeEntity
	{
		public void InitializeDeviceCounter(string deviceName, string type)
		{
			if (string.IsNullOrEmpty(deviceName))
			{
				throw new ArgumentException("Device name must not be empty.");
			}

			if (string.IsNullOrEmpty(type))
			{
				throw new ArgumentException("Type must not be empty.");
			}

			Name = deviceName;
			Type = type;
			StartValue = 1;
			Increment = 1;
			NextValue = 2;
			EndValue = Int32.MaxValue;
			Cycle = false;
			Description = "Device transaction counter";
			ClientID = 0;
			Depot = 0;
		}

		public void InitializePrintedNumberCounter(CardPhysicalDetailEntity cardPhysicalDetail)
		{
			if (cardPhysicalDetail == null || cardPhysicalDetail.IsNew)
			{ throw new ArgumentException("card pysical detail can not be empty."); }

			Name = cardPhysicalDetail.Description;
			Type = "PrintedNumber";
			StartValue = 1;
			Increment = 1;
			NextValue = 2;
			EndValue = Int32.MaxValue;
			Cycle = false;
			Description = "Printed number counter";
			ClientID = 0;
			Depot = 0;

		}

		public void InitializeVdvEntitlementCounter(long orgID)
		{
			Name = orgID.ToString();
			Type = "Entitlement number counter";
			Description = "Counter for entitlement number";
			StartValue = 1;
			Increment = 1;
			NextValue = 2;
			EndValue = UInt32.MaxValue;
			Cycle = false;
			ClientID = 0;
			Depot = 0;
		}

		public void InitializeCashComponentFillingCounter(string name)
		{
			Name = name;
			Type = name;
			Description = "Counter for cash component filling";
			StartValue = 1;
			Increment = 1;
			NextValue = 2;
			EndValue = UInt32.MaxValue;
			Cycle = false;
			ClientID = 0;
			Depot = 0;
		}

		public void InitializeCashComponentClearingCounter(string name)
		{
			Name = name;
			Type = name;
			Description = "Counter for cash component clearing";
			StartValue = 1;
			Increment = 1;
			NextValue = 2;
			EndValue = UInt32.MaxValue;
			Cycle = false;
			ClientID = 0;
			Depot = 0;
		}

		public static long GetNextSubscriptionContractNumber(long clientID, Transaction transaction)
		{
			string name = "SUMContractCounter_" + clientID;
			return ResolveNextValue(clientID, transaction, name, "SUM contract number counter");
		}

		public static long GetNextIncidentContractNumber(long clientID, Transaction transaction)
		{
			string name = "FEIncidentCounter_" + clientID;
			return ResolveNextValue(clientID, transaction, name, "Fare Evasion Incident counter");
		}

		public static long GetNextPostingNumber(long clientID, Transaction transaction)
		{
			string name = "PostingCounter_" + clientID;
			return ResolveNextValue(clientID, transaction, name, "Posting number counter");
		}

		public static long GetNextInvoiceNumber(long clientID, Transaction transaction)
		{
			string name = "InvoiceCounter_" + clientID;
			return ResolveNextValue(clientID, transaction, name, "Invoice number counter");
		}

		private static long ResolveNextValue(long clientID, Transaction transaction, string uniqueName, string description)
		{
			if (clientID == 0)
				throw new ArgumentException("ClientID not valid: " + clientID);
			long output = 1;
			string searchString = uniqueName;
			NumberRangeEntity range = new LinqMetaData(transaction).NumberRange.FirstOrDefault(nr => nr.Name == searchString && nr.Type == searchString);
			if (range == null)
			{
				range = new NumberRangeEntity() {
					Name = uniqueName,
					Type = uniqueName,
					StartValue = 1,
					Increment = 1,
					NextValue = 1,
					EndValue = Int32.MaxValue,
					Cycle = false,
					Description = description,
					ClientID = clientID,
					Depot = 0
				};
			}
			else
			{
				output = range.GetNextValue();
			}
			transaction.Add(range);
			range.Save();
			return output;
		}

		public long GetNextValue()
		{
			long currentValue = NextValue;

			long newValue = NextValue + Increment;

			if (newValue > EndValue)
			{
				throw new InvalidOperationException("Transaction counter not available (max. value reached");
			}

			NextValue = newValue;

			return currentValue;
		}

		public void InitializeInterfaceCounter(string interfaceName)
		{
			if (string.IsNullOrEmpty(interfaceName))
			{
				throw new ArgumentException("Interface name must not be empty.");
			}
			InitializeDeviceCounter(interfaceName, interfaceName);
		}

		#region ShiftNumberForDebtor

		public void InitializeShiftNumberForDebtorCounter(long clientID, string debtorNumberString)
		{
			Name = debtorNumberString;
			Type = "ShiftDebtorCounter";
			Description = "Shift counter per debtor";
			StartValue = 0;
			Increment = 1;
			NextValue = 1;
			EndValue = UInt32.MaxValue;
			Cycle = false;
			ClientID = clientID;
			Depot = 0;
		}

		public static long GetNextShiftNumberForDebtor(long clientID, long debtorNumber)
		{
			var debtorNumberString = string.Format("{0:G}", debtorNumber);

			var range = new LinqMetaData().NumberRange
				.FirstOrDefault(nr => nr.ClientID == clientID && nr.Name == debtorNumberString);

			if (range == null)
			{
				range = new NumberRangeEntity();
				range.InitializeShiftNumberForDebtorCounter(clientID, debtorNumberString);
			}

			try
			{
				return range.GetNextValue();
			}
			finally
			{
				range.Save();
			}
		}

		#endregion
	}
}
