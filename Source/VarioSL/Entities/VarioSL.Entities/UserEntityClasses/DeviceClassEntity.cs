﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class DeviceClassEntity
	{
		private static readonly char[] baseChars = new char[] {'0','1','2','3','4','5','6','7','8','9',
				'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

		public string InitEncoding
		{
			get
			{
				return ToInitBase(this.DeviceClassID);
			}
		}

		public static long FromInitBase(string value)
		{
			ValidateLength(value);
			long ret = 0;

			for (int i = value.Length-2; i >= 0 ; i--)
			{
				ret += Array.IndexOf(baseChars, value[i]) * (long)Math.Pow(32, i);
			}

			ret *= 10;
			ret += (long)Char.GetNumericValue(value.Last());

			return ret;
		}

		public static string ToInitBase(long value)
		{
			long lastDigit = value % 10;
			long firstDigits = value / 10;

			var ret = GetBase36(firstDigits) + lastDigit;
			ValidateLength(ret);
			return ret;
		}

		private static void ValidateLength(string deviceClass)
		{
			if (deviceClass.Length != 2)
			{
				throw new ArgumentException("Init DeviceClass encoding must be 2 characters long.");
			}
		}

		private static string GetBase36(long value)
		{
			// 32 is the worst cast buffer size for base 2 and int.MaxValue
			int i = 32;
			char[] buffer = new char[i];
			int targetBase = baseChars.Length;

			do
			{
				buffer[--i] = baseChars[value % targetBase];
				value = value / targetBase;
			}
			while (value > 0);

			char[] result = new char[32 - i];
			Array.Copy(buffer, i, result, 0, 32 - i);

			return new string(result);
		}
	}
}
