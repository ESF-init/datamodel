﻿using System;
using System.Collections.Generic;
using System.Linq;
using VarioSL.Entities.Enumerations;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
	public partial class OrderEntity
	{
		public virtual long OpenWorkItemID
		{
			get
			{
				var openWorkitem = WorkItems.FirstOrDefault(we => !we.IsAssigned && !we.IsExecuted);
				if (openWorkitem == null || openWorkitem.IsNew)
					return 0;
				else
					return openWorkitem.WorkItemID;
			}
		}

		internal static IQueryable<SaleEntity> saleData;

		private readonly static Func<IQueryable<SaleEntity>> _saleData = () =>
		{
			return saleData ?? new LinqMetaData().Sale.AsQueryable();
		};


		public string ExternalOrderNumberWithDefault { get { return ExternalOrderNumberWithDefaultExp.Func(this); } }

		public static readonly PropertyExpression<OrderEntity, string> ExternalOrderNumberWithDefaultExp =
			new PropertyExpression<OrderEntity, string>(o => _saleData().Where(s => s.OrderID.HasValue && s.OrderID.Value == o.OrderID).Select(s => s.ExternalOrderNumber).FirstOrDefault());

		//public long? ReplacedCardID { get { return ReplacedCardIDExp.Func(this); } }

		//public static readonly PropertyExpression<OrderEntity, long?> ReplacedCardIDExp =
		//	new PropertyExpression<OrderEntity, long?>(o => o.OrderDetails.Where(s => s.Product.TicketType == TicketType.PhysicalCard).FirstOrDefault().Product.Card.ReplacedCardID);


		public void SetPaymentOption(PaymentOptionEntity paymentOption)
		{
			if (IsNew)
			{
				throw new InvalidOperationException("No valid order was provided.");
			}

			if (paymentOption.IsNew || (ContractID ?? 0) != paymentOption.ContractID)
			{
				throw new InvalidOperationException("No valid payment option was provided.");
			}

			if (State != OrderState.New && State != OrderState.Open)
			{
				throw new InvalidOperationException("Payment option cannot be added.");
			}

			PaymentOption = paymentOption;
		}

		public void ValidateContractAffiliation(long contractID)
		{
			if (ContractID != contractID)
			{
				throw new InvalidOperationException("Order does not belong to contract.");
			}
		}

		public void ConfirmOrder()
		{
			if (IsNew)
			{
				throw new InvalidOperationException("No valid order was provided.");
			}

			if (State != OrderState.New)
			{
				throw new InvalidOperationException("Order cannot be confirmed.");
			}

			State = OrderState.Open;
		}

		public void SetContractAddresses()
		{
			if (IsNew)
			{
				throw new InvalidOperationException("No valid order was provided.");
			}

			if (State != OrderState.New)
			{
				throw new InvalidOperationException("The order is in the wrong state.");
			}

			if (Contract == null || Contract.IsNew)
			{
				throw new InvalidOperationException("Addresses cannot be set to order without valid contract.");
			}

			if (Contract.InvoiceAddressID == 0)
			{
				throw new InvalidOperationException("New or empty invoice address cannot be set to order.");
			}

			if (Contract.ShippingAddressID == 0)
			{
				throw new InvalidOperationException("New or empty shipping address cannot be set to order.");
			}

			ShippingAddressID = Contract.ShippingAddressID;
			InvoiceAddressID = Contract.InvoiceAddressID.Value;
		}

		public int TotalAmount()
		{
			return OrderDetails.Where(o => o.Product.VoucherID == null).Sum(o => 
				(o.Product.TicketType == TicketType.Purse ? o.Product.RemainingValue.Value :  o.Product.Price)
				* (o.RequiredOrderDetailID.HasValue ? o.RequiredOrderDetail.Quantity : o.Quantity));
		}

		/// <summary>
		/// This copies an order with its OrderDetails and Products
		/// </summary>
		/// <param name="orderAction">Optional action on the order</param>
		/// <param name="orderDetailAction">Optional action on the order details</param>
		/// <param name="productAction">Optional action on the products</param>
		/// <returns>A copy of the order that IsNew</returns>
		public OrderEntity Copy(
			Action<OrderEntity> orderAction = null,
			Action<OrderDetailEntity> orderDetailAction = null,
			Action<ProductEntity> productAction = null)
		{
			Action<object> emptyAction = (x) => { };

			var ret = new OrderEntity() { AlreadyFetchedWorkItems = true, AlreadyFetchedOrderDetails = true };
			ret.Fields = this.Fields.CloneAsDirty();

			ret.OrderDetails.AddRange(this.OrderDetails.Select(od =>
			{
				var odCopy = new OrderDetailEntity() { };
				odCopy.Fields = od.Fields.CloneAsDirty();
				(orderDetailAction ?? emptyAction)(odCopy);

				odCopy.Product = new ProductEntity();
				odCopy.Product.Fields = od.Product.Fields.CloneAsDirty();
				(productAction ?? emptyAction)(odCopy.Product);

				if (od.Product.SubsidyTransaction != null && !od.Product.SubsidyTransaction.IsNew)
				{
					odCopy.Product.SubsidyTransaction = new SubsidyTransactionEntity();
					odCopy.Product.SubsidyTransaction.Fields = od.Product.SubsidyTransaction.Fields.CloneAsDirty();
				}

				return odCopy;
			}));

            foreach (var od in ret.OrderDetails)
            {
                if (od.RequiredOrderDetailID != null)
                    od.RequiredOrderDetail = ret.OrderDetails.FirstOrDefault(od2 => od2.OrderDetailID == od.RequiredOrderDetailID);
            }

            (orderAction ?? emptyAction)(ret);
			return ret;
		}
	}
}
