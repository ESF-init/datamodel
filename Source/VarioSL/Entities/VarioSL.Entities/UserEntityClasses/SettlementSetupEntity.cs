﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.HelperClasses;

namespace VarioSL.Entities.EntityClasses
{
	public partial class SettlementSetupEntity
	{
		#region Properties

		public static readonly PropertyExpression<SettlementSetupEntity, DateTime> DateOfLastSettlementExp =
			new PropertyExpression<SettlementSetupEntity, DateTime>(src => src.SettlementRuns.Count() > 0 ? src.SettlementRuns.Max(run => run.BeginDate) : DateTime.MinValue);

		public DateTime DateOfLastSettlement { get { return DateOfLastSettlementExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> HasQuerySettingsExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.SettlementQuerySettings.Count() > 0);

		public virtual bool HasQuerySettings { get { return HasQuerySettingsExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> HasRunsExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.SettlementRuns.Count() > 0);

		public virtual bool HasRuns { get { return HasRunsExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsUpdateAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.NotReleased);

		public bool IsUpdateAllowed { get { return IsUpdateAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsReleaseAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.NotReleased);

		public bool IsReleaseAllowed { get { return IsReleaseAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsRevokeAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.Released);

		public bool IsRevokeAllowed { get { return IsRevokeAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsUndoReleaseAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.Released);

		public bool IsUndoReleaseAllowed { get { return IsUndoReleaseAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsAddRunAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.Released || src.ReleaseState == SettlementReleaseState.NotReleased);

		public bool IsAddRunAllowed { get { return IsAddRunAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsCalculateAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.Released || src.ReleaseState == SettlementReleaseState.NotReleased);

		public bool IsCalculateAllowed { get { return IsCalculateAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsPurgeAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.NotReleased);

		public bool IsPurgeAllowed { get { return IsPurgeAllowedExp.Func(this); } }

		public static readonly PropertyExpression<SettlementSetupEntity, bool> IsRemoveAllowedExp =
			new PropertyExpression<SettlementSetupEntity, bool>(src => src.ReleaseState == SettlementReleaseState.Revoked||src.ReleaseState==SettlementReleaseState.NotReleased);

		public bool IsRemoveAllowed { get { return IsRemoveAllowedExp.Func(this); } }


		//public static readonly PropertyExpression<SettlementSetupEntity, SettlementQuerySettingEntity> LastChangedQuerySettingExp =
		//    new PropertyExpression<SettlementSetupEntity, SettlementQuerySettingEntity>(src =>
		//        {
		//            DateTime latestQueryChange = src.SettlementQuerySettings.Max(qs => qs.LastModified);
		//            return src.SettlementQuerySettings.First(qs => qs.LastModified == latestQueryChange);
		//        });

		//public string ChangedBy { get {return DateTime.Compare(LastChangedQuerySettingExp.Func(this).LastModified,LastModified)>=0?LastChangedQuerySettingExp.Func(this).LastUser:LastUser; } }
		
		//public DateTime LatestChange { get {return DateTime.Compare(LastChangedQuerySettingExp.Func(this).LastModified,LastModified)>=0?LastChangedQuerySettingExp.Func(this).LastModified:LastModified; } }

		#endregion

		public void Release()
		{
			if (ReleaseState == SettlementReleaseState.Released)
				throw new InvalidOperationException("Settlement setup is allready released.");

			if (ReleaseState == SettlementReleaseState.Revoked)
				throw new InvalidOperationException("Settlement setup has been revoked.");

			ReleaseState = SettlementReleaseState.Released;
		}

		public void Revoke()
		{
			if (ReleaseState == SettlementReleaseState.Revoked)
				throw new InvalidOperationException("Settlement setup is allready revoked.");

			if (ReleaseState == SettlementReleaseState.NotReleased)
				throw new InvalidOperationException("Settlement setup is not released yet.");

			ReleaseState = SettlementReleaseState.Revoked;
		}

		public void Remove()
		{
			if (!IsRemoveAllowed)
				throw new InvalidOperationException("Remove settlement setup is not allowed.");

			ReleaseState = SettlementReleaseState.Removed;
		}

		public void PurgeCalculationValues(Transaction trans = null)
		{
			//if (!IsPurgeAllowed)
			//    throw new InvalidOperationException("Pruge is not allowed.");

			var runValues = SettlementRuns.SelectMany(run => run.SettlementRunValues);
			var runValuesToVarioSettlements = runValues.SelectMany(runValue => runValue.SettlementRunValueToVarioSettlements);
			var queryValues = runValues.SelectMany(runValue => runValue.SettlementQueryValues);
			foreach (SettlementQueryValueEntity queryValue in queryValues)
			{
				if (trans != null)
					trans.Add(queryValue);
				queryValue.Delete();
				queryValue.Save();
			}

			foreach (SettlementRunValueToVarioSettlementEntity runValueToVarioSettlement in runValuesToVarioSettlements)
			{
				if (trans != null)
					trans.Add(runValueToVarioSettlement);
				runValueToVarioSettlement.Delete();
				runValueToVarioSettlement.Save();
			}

			foreach (SettlementRunValueEntity runValue in runValues)
			{
				if (trans != null)
					trans.Add(runValue);
				runValue.Delete();
				runValue.Save();
			}

			foreach (SettlementRunEntity run in SettlementRuns)
			{
				if (trans != null)
					trans.Add(run);
				run.Delete();
				run.Save();
			}
		}


	}
}
