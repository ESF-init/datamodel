﻿using System;
using VarioSL.Entities.Enumerations;

namespace VarioSL.Entities.EntityClasses
{
	public partial class FormEntity
	{
		/// <summary>
		/// Returns SchemaName parsed into FormSchemaName enum, or Unknown, if parsing failed.
		/// </summary>
		public FormSchemaName SchemaNameResolved 
			=> Enum.TryParse(SchemaName, false, out FormSchemaName parsed) ? parsed : FormSchemaName.Unknown;
	}
}
