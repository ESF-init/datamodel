﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VarioSL.Entities.Enumerations;
using SD.LLBLGen.Pro.ORMSupportClasses;
using VarioSL.Entities.Linq;

namespace VarioSL.Entities.EntityClasses
{
    public partial class AutoloadSettingEntity
    {
		public virtual bool IsTopUpAutoload { get { return IsTopUpAutoloadExp.Func(this); } }
		public static readonly PropertyExpression<AutoloadSettingEntity, bool> IsTopUpAutoloadExp =
			new PropertyExpression<AutoloadSettingEntity, bool>(src => src.Product.TicketType == TicketType.TopUp);

        public virtual void Remove(IUnitOfWorkCore unitOfWork)
        {
            unitOfWork.AddForDelete(this);         
        }

        public virtual void CreateAutoloadPass(ProductEntity product, PaymentOptionEntity paymentOption)
        {
            Condition = Frequency.Undetermined;
            TriggerType = TriggerType.Interval;
            Amount = product.Price;

            if(product.IsNew || product.IsCanceled)
            {
                throw new InvalidOperationException("Product for autoload is unknown or deleted.");
            }

            if(product.AutoloadSettings.Count > 0)
            {
                throw new InvalidOperationException("An autoload for this product is already configured.");
            }

            ValidatePaymentOption(paymentOption);
        }

        public virtual void CreateAutoloadTopUp(CardEntity card, PaymentOptionEntity paymentOption, IEnumerable<long> ticketIDs, bool allowInactiveCardAutoLoad = false)
        {
            Condition = Frequency.Undetermined;
            TriggerType = TriggerType.Threshold;

            if(!card.IsAddAutoloadAllowed && !allowInactiveCardAutoLoad)
            {
                throw new InvalidOperationException("The card for the autoload is unknown or not active.");
            }

            if(ticketIDs == null || ticketIDs.Count() < 1)
            {
                throw new InvalidOperationException("Topup autoload is not available for the selected card.");
            }

            ValidatePaymentOption(paymentOption);

            Product = new ProductEntity()
                {
                    CardID = card.CardID,
                    Price = Amount,
                    TicketType = TicketType.TopUp,
                    TicketID = ticketIDs.First(),
                    ValidFrom = DateTime.Now,
                    ValidTo = DateTime.Now
                };
        }

        private static void ValidatePaymentOption(PaymentOptionEntity paymentOption)
        {
            if(paymentOption != null && paymentOption.IsNew)
            {
                throw new InvalidOperationException("The selected payment option for the autoload is unknown.");
            }
        }

		public static void RemoveAutoloadSettingByProductID(long productID, ITransaction transaction = null)
		{
			var autoload = new LinqMetaData().AutoloadSetting.FirstOrDefault(a => a.ProductID == productID);

			if (autoload != null)
			{
				if (transaction != null)
					transaction.Add(autoload);

				autoload.Delete();
			}

		}
    }
}
