﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VarioSL.Entities.EntityClasses
{
	public partial class ContractWorkItemEntity
	{
		public string IdentificationNumber
		{
			get
			{
				return IdentificationNumberExp.Func(this);
			}
		}
		public static readonly PropertyExpression<ContractWorkItemEntity, string> IdentificationNumberExp =
			new PropertyExpression<ContractWorkItemEntity, string>(a => a.Contract.ContractNumber);
	}
}