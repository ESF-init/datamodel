﻿using System;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.HelperClasses
{
	public class TransactionHelper
	{
		private const string ConcurrencyFailureMessage = "ORA-20000: Concurrency Failure";
		private int AttemptCounter = 0;
		private readonly int MaxAttemptCounter = 5;

		public Action<ITransaction> Job { get; set; }
        public Func<ITransaction, bool> JobBool { private get; set; }
        public Func<ITransaction, long> JobLong { private get; set; }
		public string TransactionName { get; }

		public TransactionHelper(string transactionName)
		{
			TransactionName = transactionName;
		}

		public void StartJob()
		{
			if (Job == null)
				throw new ArgumentException("Job could not be null!");

			ITransaction trans = new Transaction(IsolationLevel.ReadCommitted, TransactionName);
			try
			{
				Job(trans);
				trans.Commit();
			}
			catch (Exception e)
			{
				trans.Rollback();
				if (!HandleConcurrencyFailure(trans, e))
					throw;
			}
			finally
			{
				trans.Dispose();
			}
		}

        public bool StartJobBool()
        {
            if (JobBool == null)
                throw new ArgumentException("Job could not be null!");

            ITransaction trans = new Transaction(IsolationLevel.ReadCommitted, TransactionName);
            try
            {
                bool returnValue = JobBool(trans);
                trans.Commit();
                return returnValue;
            }
            catch (Exception e)
            {
                trans.Rollback();
				if (!HandleConcurrencyFailureBool(trans, e, out bool returnValue))
					throw;
				return returnValue;
            }
            finally
            {
                trans.Dispose();
            }
        }

        public long StartJobLong()
        {
            if (JobLong == null)
                throw new ArgumentException("Job could not be null!");

            ITransaction trans = new Transaction(IsolationLevel.ReadCommitted, TransactionName);
            try
            {
                long returnValue = JobLong(trans);
                trans.Commit();
                return returnValue;
            }
            catch (Exception e)
            {
                trans.Rollback();
				if (!HandleConcurrencyFailureLong(trans, e, out long returnValue))
					throw;
				return returnValue;
            }
            finally
            {
                trans.Dispose();
            }
        }

        private bool HandleConcurrencyFailure(ITransaction trans, Exception e)
		{
			if (IsConcurrentFailureError(e) && MaxAttemptCounter > AttemptCounter)
			{
				AttemptCounter++;
				StartJob();
				return true;
			}
			return false;
		}

        private bool HandleConcurrencyFailureBool(ITransaction trans, Exception e, out bool returnValue)
        {
            returnValue = false;

            if (IsConcurrentFailureError(e) && MaxAttemptCounter > AttemptCounter)
            {
                AttemptCounter++;
                returnValue = StartJobBool();
                return true;
            }

            return false;
        }

        private bool HandleConcurrencyFailureLong(ITransaction trans, Exception e, out long returnValue)
        {
            returnValue = -1;

            if (IsConcurrentFailureError(e) && MaxAttemptCounter > AttemptCounter)
            {
                AttemptCounter++;
                returnValue = StartJobLong();
                return true;
            }

            return false;
        }

        private bool IsConcurrentFailureError(Exception e)
		{
			var exp = e;
			while (exp != null)
			{
				if (exp.Message.Contains(ConcurrencyFailureMessage))
					return true;
				exp = exp.InnerException;
			}
			return false;
		}
	}
}
