﻿using System.Collections.Generic;

namespace VarioSL.Entities.UserFunctionMappings
{
    /// <summary>
    /// Provides functionality for null substitution of a value.
    /// </summary>
    public class ValueSubstitution
    {
        /// <summary>
        /// Gets the value or default if value is default value
        /// </summary>
        /// <typeparam name="TValue">Value type to substitute.</typeparam>
        /// <param name="value">Value to validate.</param>
        /// <param name="defaultValue">Default value to use if substitution is required.</param>
        /// <returns></returns>
        public static TValue ToValueOrDefault<TValue>(TValue value, TValue defaultValue)
        {
            return EqualityComparer<TValue>.Default.Equals(value, default(TValue))
                ? value
                : defaultValue;
        }

        /// <summary>
        /// Sets null if value equals compare value, otherwise value.
        /// </summary>
        /// <typeparam name="TValue">Type of value.</typeparam>
        /// <param name="value">Value that should be compared.</param>
        /// <param name="compareValue">Compare value for equality comparison.</param>
        /// <returns>Returns the value that is used.</returns>
        public static TValue ToNullIfEquals<TValue>(TValue value, TValue compareValue)
        {
            return EqualityComparer<TValue>.Default.Equals(value, compareValue)
                ? default(TValue)
                : value;
        }

        /// <summary>
        /// returns null if the condition is met.
        /// This method is used to workaround a LLBLGen bug when generating queries:
        /// when you use the C# construct "[condition] ? [code which results in int/number] : null" LLBLGen generates a coalesce function call and inserts the null as parameter which is then varchar.
        /// The result is that the left side of the coalesce is number an the right side is varchar which results in an error (ORA-00932: inconsistent datatypes: expected NUMBER got CHAR)
        /// The workaround is this method call which is not generated in sql but call on mapping time.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="condition"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TValue ToNullIfTrue<TValue>(bool condition, TValue value)
        {
            return condition ? default(TValue) : value;
        }
    }
}