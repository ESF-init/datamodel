﻿using System.Collections.Generic;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.UserFunctionMappings
{
	/// <summary>
	/// This class provide additional (custom) function mappings for some VarioSL functions.
	/// You need to register this additional mappings before usage.
	/// 
	/// <![CDATA[
	/// Example Usage:
	/// 
	///		var meta = new LinqMetaData();
	///		meta.CustomFunctionMappings = new FunctionMappings();		//registering extra mappings
	///		
	///		meta.Card.Where(card => card.PrintedNumber.IsLike("*007"));
	/// ]]>
	/// 
	/// <![CDATA[
	/// Alternative initialization with FunctionMappings:
	/// 
	///		var meta = new LinqMetaData(null, new FunctionMappings());
	/// ]]>
	/// Currently the following mappings are available:
	/// 
	///	VarioSL.Common.
	/// </summary>
	public class FunctionMappings : FunctionMappingStore
	{
		public FunctionMappings()
		{
		    AddFunctionMapping<ValueSubstitution>("ToValueOrDefault", 2, "COALESCE({0}, {1})");
            AddFunctionMapping<ValueSubstitution>("ToNullIfEquals", 2, "CASE WHEN {0} = {1} THEN NULL ELSE {0} END");
		    AddFunctionMapping<DateTimeFormat>("ToDateTimeString", 2, "TO_CHAR({0}, {1})");
			AddFunctionMapping<DbCasts>(nameof(DbCasts.ToNVarchar2), 1, "CAST({0} AS NVARCHAR2(2000))");

			Add(BitAnd_Mapping);
			Add(IsLike_Mapping);
			Add(IsLikeCaseSensitive_Mapping);
			Add(LastDay_Mapping);
			Add(LastDayOfMonth_Mapping);
			Add(EqualExceptPattern_Mapping);
            Add(LeftPadding_Mapping);

			//remove this when updating to a version where these bugs are fixed
            AddRange(LLBLGen41MappingBugfixes.GetDateTimeFixes());
        }

	    private void AddFunctionMapping<TFunctionMapping>(string functionName, int parameterCount, string functionCallPattern)
	    {
	        Add(new FunctionMapping(typeof(TFunctionMapping), functionName, parameterCount, functionCallPattern));
	    }

        private void AddRange(IEnumerable<FunctionMapping> functionMappings)
        {
            foreach (var fixedFunctionMapping in functionMappings)
            {
                Add(fixedFunctionMapping);
            }
		}

		#region Mappings

		/// <summary>
		/// Maps the BitAnd() function to SQL BitAnd
		/// </summary>
		private static FunctionMapping BitAnd_Mapping
		{

			get
			{
				return new FunctionMapping(typeof(BitFunctions), "BitAnd", 2, "BitAnd({0}, {1})");
			}
		}

		/// <summary>
		/// Maps the IsLike() string extension to the case insensitive variant of the database LIKE
		/// operator. Allowed wildcards ('*' and '?') will be transformed to database wildcards.
		/// </summary>
		private static FunctionMapping IsLike_Mapping
		{
			get
			{
				return new FunctionMapping(
					typeof(VarioSL.Common.SupportClasses.Extensions.StringExtensions), "IsLike", 2,
					"UPPER({0}) LIKE UPPER(replace(replace({1}, '?', '_'), '*', '%'))");
			}
		}

		/// <summary>
		/// Maps the IsLike() string extension to the case sensitive variant of the database LIKE
		/// operator. Allowed wildcards ('*' and '?') will be transformed to database wildcards.
		/// </summary>
		private static FunctionMapping IsLikeCaseSensitive_Mapping
		{
			get
			{
				return new FunctionMapping(
					typeof(VarioSL.Common.SupportClasses.Extensions.StringExtensions), "IsLikeCaseSensitive", 2,
					"{0} LIKE replace(replace({1}, '?', '_'), '*', '%')");
			}
		}

		/// <summary>
		/// Maps the LastDay() datetime extension to the variant of the database LastDay operator.
		/// </summary>
		private static FunctionMapping LastDay_Mapping
		{
			get
			{
				return new FunctionMapping(
					typeof(System.DateTimeExtensions), "GetMonthEnd", 1,
					"LAST_DAY({0})");
			}
		}

		/// <summary>
		/// Maps the LastDayOfMonth() datetime extension to the variant of the database CAST(TO_CHAR(LastDay(EVENTTIME), 'DD') AS NUMBER(9, 0)) operator.
		/// </summary>
		private static FunctionMapping LastDayOfMonth_Mapping
		{
			get
			{
				return new FunctionMapping(
					typeof(System.DateTimeExtensions), "GetLastDayOfMonth", 1,
					"EXTRACT(day FROM Last_Day({0}))");
			}
		}

		/// <summary>
		/// Maps the EqualExceptPattern() string extension using the case sensitive variant of the database REGEXP_REPLACE
		/// operator.
		/// </summary>
		private static FunctionMapping EqualExceptPattern_Mapping
		{
			get
			{
				return new FunctionMapping(
					typeof(VarioSL.Common.SupportClasses.Extensions.StringExtensions), "EqualExceptPattern", 3,
                    "REGEXP_REPLACE( {0}, {2}, '')=REGEXP_REPLACE({1}, {2}, '')");

            }
		}

        /// <summary>
        /// Maps the LeftPadding_Mapping() string extension using the left padding variant of the database LPAD
        /// operator.
        /// </summary>
        static private FunctionMapping LeftPadding_Mapping
        {
            get
            {
                return new FunctionMapping(
                    typeof(VarioSL.Common.SupportClasses.Extensions.StringExtensions), "LeftPadding", 3,
                    "LPAD({0}, {1}, {2})");

            }
        }

        #endregion
    }
}
