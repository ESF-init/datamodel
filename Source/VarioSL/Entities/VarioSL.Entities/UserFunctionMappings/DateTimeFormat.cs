﻿using System;

namespace VarioSL.Entities.UserFunctionMappings
{
    /// <summary>
    /// Contains functionality to formate date times.
    /// </summary>
    public class DateTimeFormat
    {
        /// <summary>
        /// Formats a date time while using LLBLGen projection.
        /// </summary>
        /// <param name="dateTime"><see cref="DateTime"/> to format.</param>
        /// <param name="format">Date time format string.</param>
        /// <returns>Returns formatted date time string.</returns>
        public static string ToDateTimeString(DateTime? dateTime, string format)
        {
            return dateTime.HasValue 
                ? dateTime.Value.ToString(format)
                : string.Empty;
        }
    }
}
