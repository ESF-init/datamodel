﻿namespace VarioSL.Entities.UserFunctionMappings
{
	public class BitFunctions
	{
		public static int BitAnd(int expr1, int expr2)
		{ 
			// This does not need an actual implementation, as it gets replaced by SQL BitAnd
			return expr1 & expr2;
		}
	}
}
