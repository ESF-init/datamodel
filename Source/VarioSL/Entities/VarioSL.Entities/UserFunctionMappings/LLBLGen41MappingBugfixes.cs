﻿using System;
using System.Collections.Generic;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.UserFunctionMappings
{
    /// <summary>
    /// this class contains bugfixes for existing bugs in the function mappings of DynamicQueryEngine in SD.LLBLGen.Pro.DQE.OracleODPNet.dll version 4.1
    /// </summary>
	public static class LLBLGen41MappingBugfixes
	{
		public static List<FunctionMapping> GetDateTimeFixes()
        {
            //this is a workaround for a bug in llblgen. See: https://llblgen.com/tinyforum/Messages.aspx?ThreadID=22961
            return new List<FunctionMapping>
            {
                new FunctionMapping(typeof(DateTime), "AddSeconds", 1, "{0} + NUMTODSINTERVAL({1},'SECOND')"),
                new FunctionMapping(typeof(DateTime), "AddMinutes", 1, "{0} + NUMTODSINTERVAL({1},'MINUTE')"),
                new FunctionMapping(typeof(DateTime), "AddHours", 1, "{0} + NUMTODSINTERVAL({1},'HOUR')"),
                new FunctionMapping(typeof(DateTime), "AddDays", 1, "{0} + NUMTODSINTERVAL({1},'DAY')"),

                /*
                 * for adding months the above does not work if the source date is for example the 31.01.2021. Adding 1 to the month part results
                 * in having the 31.02.2021 which throws an error (ORA-01839). For that the ADD_MONTHS function should be used.
                 * More information can be found here:
                 * https://stackoverflow.com/questions/48057433/ora-01839-date-not-valid-for-month-specified and
                 * https://docs.oracle.com/database/121/SQLRF/sql_elements001.htm#sthref135
                 * The same problem do you have if you would add one year on the 29th of February in a leap year. This also would result in an error.
                 * Therefore in this case the ADD_MONTHS function is used too.
                 */
                new FunctionMapping(typeof(DateTime), "AddMonths", 1, "ADD_MONTHS({0}, {1})"),
                new FunctionMapping(typeof(DateTime), "AddYears", 1, "ADD_MONTHS({0}, {1} * 12)")
            };
        }

    }
}
