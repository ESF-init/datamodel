﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of TransactionJournalEntity objects. </summary>
	[Serializable]
	public partial class TransactionJournalCollection : EntityCollectionBase<TransactionJournalEntity>
	{
		/// <summary> CTor</summary>
		public TransactionJournalCollection():base(new TransactionJournalEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public TransactionJournalCollection(IEnumerable<TransactionJournalEntity> initialContents):base(new TransactionJournalEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public TransactionJournalCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TransactionJournalCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this TransactionJournalCollection object all TransactionJournalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance)
		{
			return GetMultiManyToOne(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this TransactionJournalCollection object all TransactionJournalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TransactionJournalCollection object all TransactionJournalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TransactionJournalCollection object all TransactionJournalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (clientInstance!=null);
			validParameters |= (deviceClassInstance!=null);
			validParameters |= (appliedPassTicketInstance!=null);
			validParameters |= (ticketInstance!=null);
			validParameters |= (ticket_Instance!=null);
			validParameters |= (cardInstance!=null);
			validParameters |= (card_Instance!=null);
			validParameters |= (tokenCardInstance!=null);
			validParameters |= (cardHolderInstance!=null);
			validParameters |= (creditCardAuthorizationInstance!=null);
			validParameters |= (appliedPassProductInstance!=null);
			validParameters |= (productInstance!=null);
			validParameters |= (saleInstance!=null);
			validParameters |= (cancellationReferenceTransactionInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTransactionJournalDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all TransactionJournal entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance)
		{
			return DAOFactory.CreateTransactionJournalDAO().DeleteMulti(this.Transaction, clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance);
		}

		/// <summary> Updates in the persistent storage all TransactionJournal entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">TransactionJournalEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassTicketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="ticket_Instance">TicketEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="card_Instance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="tokenCardInstance">CardEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cardHolderInstance">CardHolderEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="creditCardAuthorizationInstance">CreditCardAuthorizationEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="appliedPassProductInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="productInstance">ProductEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="saleInstance">SaleEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <param name="cancellationReferenceTransactionInstance">TransactionJournalEntity instance to use as a filter for the TransactionJournalEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(TransactionJournalEntity entityWithNewValues, IEntity clientInstance, IEntity deviceClassInstance, IEntity appliedPassTicketInstance, IEntity ticketInstance, IEntity ticket_Instance, IEntity cardInstance, IEntity card_Instance, IEntity tokenCardInstance, IEntity cardHolderInstance, IEntity creditCardAuthorizationInstance, IEntity appliedPassProductInstance, IEntity productInstance, IEntity saleInstance, IEntity cancellationReferenceTransactionInstance)
		{
			return DAOFactory.CreateTransactionJournalDAO().UpdateMulti(entityWithNewValues, this.Transaction, clientInstance, deviceClassInstance, appliedPassTicketInstance, ticketInstance, ticket_Instance, cardInstance, card_Instance, tokenCardInstance, cardHolderInstance, creditCardAuthorizationInstance, appliedPassProductInstance, productInstance, saleInstance, cancellationReferenceTransactionInstance);
		}


		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			TransactionJournalDAO dao = DAOFactory.CreateTransactionJournalDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TransactionJournalFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TransactionJournalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TransactionJournalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TransactionJournalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(TransactionJournalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateTransactionJournalDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTransactionJournalDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
