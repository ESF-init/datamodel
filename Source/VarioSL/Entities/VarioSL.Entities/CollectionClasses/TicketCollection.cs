﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of TicketEntity objects. </summary>
	[Serializable]
	public partial class TicketCollection : EntityCollectionBase<TicketEntity>
	{
		/// <summary> CTor</summary>
		public TicketCollection():base(new TicketEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public TicketCollection(IEnumerable<TicketEntity> initialContents):base(new TicketEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public TicketCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return GetMultiManyToOne(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (businessRuleInspectionInstance!=null);
			validParameters |= (businessRuleTestBoardingInstance!=null);
			validParameters |= (businessRuleInstance!=null);
			validParameters |= (businessRuleTicketAssignmentInstance!=null);
			validParameters |= (calendarInstance!=null);
			validParameters |= (clientInstance!=null);
			validParameters |= (deviceClassInstance!=null);
			validParameters |= (eticketInstance!=null);
			validParameters |= (fareEvasionCategoryInstance!=null);
			validParameters |= (fareMatrixInstance!=null);
			validParameters |= (fareTable1Instance!=null);
			validParameters |= (fareTable2Instance!=null);
			validParameters |= (cancellationLayoutInstance!=null);
			validParameters |= (groupLayoutInstance!=null);
			validParameters |= (printLayoutInstance!=null);
			validParameters |= (receiptLayoutInstance!=null);
			validParameters |= (lineGroupInstance!=null);
			validParameters |= (pageContentGroupInstance!=null);
			validParameters |= (priceTypeInstance!=null);
			validParameters |= (rulePeriodInstance!=null);
			validParameters |= (tariffInstance!=null);
			validParameters |= (temporalTypeInstance!=null);
			validParameters |= (ticketCancellationTypeInstance!=null);
			validParameters |= (ticketCategoryInstance!=null);
			validParameters |= (ticketSelectionModeInstance!=null);
			validParameters |= (ticketTypeInstance!=null);
			validParameters |= (userKey1Instance!=null);
			validParameters |= (userKey2Instance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Ticket entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return DAOFactory.CreateTicketDAO().DeleteMulti(this.Transaction, businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance);
		}

		/// <summary> Updates in the persistent storage all Ticket entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">TicketEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="businessRuleInspectionInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTestBoardingInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="businessRuleTicketAssignmentInstance">BusinessRuleEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="clientInstance">ClientEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareEvasionCategoryInstance">FareEvasionCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable1Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="fareTable2Instance">FareTableEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="lineGroupInstance">LineGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="pageContentGroupInstance">PageContentGroupEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="priceTypeInstance">PriceTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="tariffInstance">TariffEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="temporalTypeInstance">TemporalTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCancellationTypeInstance">TicketCancellationTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketCategoryInstance">TicketCategoryEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="ticketTypeInstance">TicketTypeEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(TicketEntity entityWithNewValues, IEntity businessRuleInspectionInstance, IEntity businessRuleTestBoardingInstance, IEntity businessRuleInstance, IEntity businessRuleTicketAssignmentInstance, IEntity calendarInstance, IEntity clientInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareEvasionCategoryInstance, IEntity fareMatrixInstance, IEntity fareTable1Instance, IEntity fareTable2Instance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity lineGroupInstance, IEntity pageContentGroupInstance, IEntity priceTypeInstance, IEntity rulePeriodInstance, IEntity tariffInstance, IEntity temporalTypeInstance, IEntity ticketCancellationTypeInstance, IEntity ticketCategoryInstance, IEntity ticketSelectionModeInstance, IEntity ticketTypeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return DAOFactory.CreateTicketDAO().UpdateMulti(entityWithNewValues, this.Transaction, businessRuleInspectionInstance, businessRuleTestBoardingInstance, businessRuleInstance, businessRuleTicketAssignmentInstance, calendarInstance, clientInstance, deviceClassInstance, eticketInstance, fareEvasionCategoryInstance, fareMatrixInstance, fareTable1Instance, fareTable2Instance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, lineGroupInstance, pageContentGroupInstance, priceTypeInstance, rulePeriodInstance, tariffInstance, temporalTypeInstance, ticketCancellationTypeInstance, ticketCategoryInstance, ticketSelectionModeInstance, ticketTypeInstance, userKey1Instance, userKey2Instance);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in AttributeValueEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(IEntity attributeValueInstance)
		{
			return GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(attributeValueInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in AttributeValueEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(IEntity attributeValueInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(attributeValueInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in AttributeValueEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(IEntity attributeValueInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(attributeValueInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in AttributeValueEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(IEntity attributeValueInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingAttributevalueCollectionViaAttributeValueList(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, attributeValueInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in AttributeValueEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="attributeValueInstance">AttributeValueEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAttributevalueCollectionViaAttributeValueList(IEntity attributeValueInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingAttributevalueCollectionViaAttributeValueList(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, attributeValueInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in CardChipTypeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(IEntity cardChipTypeInstance)
		{
			return GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(cardChipTypeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardChipTypeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(IEntity cardChipTypeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(cardChipTypeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in CardChipTypeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(IEntity cardChipTypeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(cardChipTypeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardChipTypeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(IEntity cardChipTypeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingCardChipTypeCollectionViaTicketPhysicalCardType(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, cardChipTypeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardChipTypeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardChipTypeInstance">CardChipTypeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardChipTypeCollectionViaTicketPhysicalCardType(IEntity cardChipTypeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingCardChipTypeCollectionViaTicketPhysicalCardType(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, cardChipTypeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in CardTicketEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(IEntity cardTicketInstance)
		{
			return GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(cardTicketInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardTicketEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(IEntity cardTicketInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(cardTicketInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in CardTicketEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(IEntity cardTicketInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(cardTicketInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardTicketEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(IEntity cardTicketInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingCardAdditionsTicketIsUsedIn(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, cardTicketInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in CardTicketEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="cardTicketInstance">CardTicketEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCardAdditionsTicketIsUsedIn(IEntity cardTicketInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingCardAdditionsTicketIsUsedIn(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, cardTicketInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingClientCollectionViaTicketVendingClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaTicketVendingClient(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingClientCollectionViaTicketVendingClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in LineEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(IEntity lineInstance)
		{
			return GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(lineInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in LineEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(IEntity lineInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(lineInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in LineEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(IEntity lineInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(lineInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in LineEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(IEntity lineInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingLineCollectionViaTicketServicesPermitted(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, lineInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in LineEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="lineInstance">LineEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLineCollectionViaTicketServicesPermitted(IEntity lineInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingLineCollectionViaTicketServicesPermitted(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, lineInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in PaymentIntervalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(IEntity paymentIntervalInstance)
		{
			return GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(paymentIntervalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in PaymentIntervalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(IEntity paymentIntervalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(paymentIntervalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in PaymentIntervalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(IEntity paymentIntervalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(paymentIntervalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in PaymentIntervalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(IEntity paymentIntervalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingPaymentIntervalCollectionViaTicketPaymentInterval(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, paymentIntervalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in PaymentIntervalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="paymentIntervalInstance">PaymentIntervalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPaymentIntervalCollectionViaTicketPaymentInterval(IEntity paymentIntervalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingPaymentIntervalCollectionViaTicketPaymentInterval(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, paymentIntervalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in RuleCappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(IEntity ruleCappingInstance)
		{
			return GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(ruleCappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in RuleCappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(IEntity ruleCappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(ruleCappingInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in RuleCappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(IEntity ruleCappingInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(ruleCappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in RuleCappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(IEntity ruleCappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingRuleCappingCollectionViaRuleCappingToTicket(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, ruleCappingInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in RuleCappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ruleCappingInstance">RuleCappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRuleCappingCollectionViaRuleCappingToTicket(IEntity ruleCappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingRuleCappingCollectionViaRuleCappingToTicket(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, ruleCappingInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in TicketGroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(IEntity ticketGroupInstance)
		{
			return GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(ticketGroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in TicketGroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(IEntity ticketGroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(ticketGroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in TicketGroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(IEntity ticketGroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(ticketGroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in TicketGroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(IEntity ticketGroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingTicketGroupCollectionViaTicketToGroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, ticketGroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in TicketGroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="ticketGroupInstance">TicketGroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTicketGroupCollectionViaTicketToGroup(IEntity ticketGroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingTicketGroupCollectionViaTicketToGroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, ticketGroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in OrganizationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(IEntity organizationInstance)
		{
			return GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(organizationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in OrganizationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(IEntity organizationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(organizationInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in OrganizationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(IEntity organizationInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(organizationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in OrganizationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(IEntity organizationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingOrganizationCollectionViaTicketOrganization(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, organizationInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in OrganizationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="organizationInstance">OrganizationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrganizationCollectionViaTicketOrganization(IEntity organizationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingOrganizationCollectionViaTicketOrganization(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, organizationInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  Relation of type 'm:n' with the passed in SettlementQuerySettingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSettlementQuerySettings(IEntity settlementQuerySettingInstance)
		{
			return GetMultiManyToManyUsingSettlementQuerySettings(settlementQuerySettingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in SettlementQuerySettingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSettlementQuerySettings(IEntity settlementQuerySettingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSettlementQuerySettings(settlementQuerySettingInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a Relation of type 'm:n' with the passed in SettlementQuerySettingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSettlementQuerySettings(IEntity settlementQuerySettingInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSettlementQuerySettings(settlementQuerySettingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in SettlementQuerySettingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSettlementQuerySettings(IEntity settlementQuerySettingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingSettlementQuerySettings(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, settlementQuerySettingInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketCollection object all TicketEntity objects which are related via a  relation of type 'm:n' with the passed in SettlementQuerySettingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="settlementQuerySettingInstance">SettlementQuerySettingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSettlementQuerySettings(IEntity settlementQuerySettingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDAO().GetMultiUsingSettlementQuerySettings(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, settlementQuerySettingInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			TicketDAO dao = DAOFactory.CreateTicketDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(TicketFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateTicketDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
