﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using VarioSL.Entities.EntityClasses;
using VarioSL.Entities.FactoryClasses;
using VarioSL.Entities.DaoClasses;
using VarioSL.Entities.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace VarioSL.Entities.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of TicketDeviceClassEntity objects. </summary>
	[Serializable]
	public partial class TicketDeviceClassCollection : EntityCollectionBase<TicketDeviceClassEntity>
	{
		/// <summary> CTor</summary>
		public TicketDeviceClassCollection():base(new TicketDeviceClassEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public TicketDeviceClassCollection(IEnumerable<TicketDeviceClassEntity> initialContents):base(new TicketDeviceClassEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public TicketDeviceClassCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TicketDeviceClassCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return GetMultiManyToOne(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (calendarInstance!=null);
			validParameters |= (deviceClassInstance!=null);
			validParameters |= (eticketInstance!=null);
			validParameters |= (fareMatrixInstance!=null);
			validParameters |= (cancellationLayoutInstance!=null);
			validParameters |= (groupLayoutInstance!=null);
			validParameters |= (printLayoutInstance!=null);
			validParameters |= (receiptLayoutInstance!=null);
			validParameters |= (rulePeriodInstance!=null);
			validParameters |= (ticketInstance!=null);
			validParameters |= (ticketSelectionModeInstance!=null);
			validParameters |= (userKey1Instance!=null);
			validParameters |= (userKey2Instance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all TicketDeviceClass entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return DAOFactory.CreateTicketDeviceClassDAO().DeleteMulti(this.Transaction, calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance);
		}

		/// <summary> Updates in the persistent storage all TicketDeviceClass entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">TicketDeviceClassEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="calendarInstance">CalendarEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="deviceClassInstance">DeviceClassEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="eticketInstance">EticketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="fareMatrixInstance">FareMatrixEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="cancellationLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="groupLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="printLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="receiptLayoutInstance">LayoutEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="rulePeriodInstance">RulePeriodEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketInstance">TicketEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="ticketSelectionModeInstance">TicketSelectionModeEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey1Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <param name="userKey2Instance">UserKeyEntity instance to use as a filter for the TicketDeviceClassEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(TicketDeviceClassEntity entityWithNewValues, IEntity calendarInstance, IEntity deviceClassInstance, IEntity eticketInstance, IEntity fareMatrixInstance, IEntity cancellationLayoutInstance, IEntity groupLayoutInstance, IEntity printLayoutInstance, IEntity receiptLayoutInstance, IEntity rulePeriodInstance, IEntity ticketInstance, IEntity ticketSelectionModeInstance, IEntity userKey1Instance, IEntity userKey2Instance)
		{
			return DAOFactory.CreateTicketDeviceClassDAO().UpdateMulti(entityWithNewValues, this.Transaction, calendarInstance, deviceClassInstance, eticketInstance, fareMatrixInstance, cancellationLayoutInstance, groupLayoutInstance, printLayoutInstance, receiptLayoutInstance, rulePeriodInstance, ticketInstance, ticketSelectionModeInstance, userKey1Instance, userKey2Instance);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  Relation of type 'm:n' with the passed in DevicePaymentMethodEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDevicePaymentMethodCollection(IEntity devicePaymentMethodInstance)
		{
			return GetMultiManyToManyUsingDevicePaymentMethodCollection(devicePaymentMethodInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in DevicePaymentMethodEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDevicePaymentMethodCollection(IEntity devicePaymentMethodInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDevicePaymentMethodCollection(devicePaymentMethodInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a Relation of type 'm:n' with the passed in DevicePaymentMethodEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDevicePaymentMethodCollection(IEntity devicePaymentMethodInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDevicePaymentMethodCollection(devicePaymentMethodInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in DevicePaymentMethodEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDevicePaymentMethodCollection(IEntity devicePaymentMethodInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetMultiUsingDevicePaymentMethodCollection(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, devicePaymentMethodInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in DevicePaymentMethodEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="devicePaymentMethodInstance">DevicePaymentMethodEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDevicePaymentMethodCollection(IEntity devicePaymentMethodInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetMultiUsingDevicePaymentMethodCollection(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, devicePaymentMethodInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  Relation of type 'm:n' with the passed in OutputDeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOutputDevices(IEntity outputDeviceInstance)
		{
			return GetMultiManyToManyUsingOutputDevices(outputDeviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in OutputDeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOutputDevices(IEntity outputDeviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOutputDevices(outputDeviceInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a Relation of type 'm:n' with the passed in OutputDeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOutputDevices(IEntity outputDeviceInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOutputDevices(outputDeviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in OutputDeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOutputDevices(IEntity outputDeviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetMultiUsingOutputDevices(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, outputDeviceInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TicketDeviceClassCollection object all TicketDeviceClassEntity objects which are related via a  relation of type 'm:n' with the passed in OutputDeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="outputDeviceInstance">OutputDeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOutputDevices(IEntity outputDeviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetMultiUsingOutputDevices(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, outputDeviceInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			TicketDeviceClassDAO dao = DAOFactory.CreateTicketDeviceClassDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketDeviceClassFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketDeviceClassFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketDeviceClassFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TicketDeviceClassFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(TicketDeviceClassFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateTicketDeviceClassDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTicketDeviceClassDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
