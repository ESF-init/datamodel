﻿using System;
using System.ComponentModel;

namespace VarioSL.Entities.TypeConverters
{
    /// <summary>
    /// For old oversized columns. This converter uses 'Int32' as its core type and tries to convert 
    /// any numeric value from and to Int32.
    /// </summary>
    public class Int32NumericConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            switch (sourceType.FullName)
            {
                case "System.Int64":
                case "System.Int32":
                case "System.Int16":
                case "System.Byte":
                case "System.SByte":
                case "System.UInt64":
                case "System.UInt32":
                case "System.UInt16":
                case "System.Decimal":
                    return true;
                default:
                    return false;
            }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            switch (destinationType.FullName)
            {
                case "System.Int64":
                case "System.Int32":
                case "System.Int16":
                case "System.Byte":
                case "System.SByte":
                case "System.UInt64":
                case "System.UInt32":
                case "System.UInt16":
                case "System.Decimal":
                    return true;
                default:
                    return false;
            }
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value == null)
            {
                return value;
            }

            switch (value.GetType().FullName)
            {
                case "System.Int64":
                case "System.Int32":
                case "System.Int16":
                case "System.Byte":
                case "System.SByte":
                case "System.UInt64":
                case "System.UInt32":
                case "System.UInt16":
                case "System.Decimal":
                    return Convert.ToInt32(value, culture);
                default:
                    throw new NotSupportedException(
                        string.Format("Conversion from a value of type '{0}' to 'System.Int32' isn't supported.", value.GetType()));
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            switch (destinationType.FullName)
            {
                case "System.Int64":
                    return Convert.ToInt64(value, culture);
                case "System.Int32":
                    return Convert.ToInt32(value, culture);
                case "System.Int16":
                    return Convert.ToInt16(value, culture);
                case "System.Byte":
                    return Convert.ToByte(value, culture);
                case "System.SByte":
                    return Convert.ToSByte(value, culture);
                case "System.UInt64":
                    return Convert.ToUInt64(value, culture);
                case "System.UInt32":
                    return Convert.ToUInt32(value, culture);
                case "System.UInt16":
                    return Convert.ToUInt16(value, culture);
                case "System.Decimal":
                    return Convert.ToDecimal(value, culture);
                default:
                    throw new NotSupportedException(
                        string.Format("Conversion from a value of type '{0}' to '{1}' isn't supported.", value.GetType(), destinationType));
            }
        }

        public override object CreateInstance(ITypeDescriptorContext context, System.Collections.IDictionary propertyValues)
        {
            return (Int32)0;
        }
    }
}
