﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace VarioSL.Entities.TypeConverters
{
    [Description("Converts a byte array / blob into an image.")]
    public class ByteArrayImageConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            switch (sourceType.FullName)
            {
                case "System.Byte[]":
                    return true;
                default:
                    return false;
            }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            switch (destinationType.FullName)
            {
                case "System.Byte[]":
                    return true;
                default:
                    return false;
            }
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value == null)
            {
                return value;
            }

            Image returnValue = null;
            switch (value.GetType().FullName)
            {
				case "System.Byte[]":
                    ImageConverter imageConverter = new ImageConverter();
                    returnValue = (Image)imageConverter.ConvertFrom(context, culture, value);
                    break;
                default:
                    throw new NotSupportedException(
                        string.Format("Conversion from a value of type '{0}' to System.Drawing.Image isn't supported.", value.GetType()));
            }
            return returnValue;
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value == null)
            {
                return value;
            }

            byte[] returnValue = null;
			switch (value.GetType().FullName)//destinationType.FullName)
            {
                case "System.Drawing.Image":
				case "System.Drawing.Bitmap":
                    ImageConverter imageConverter = new ImageConverter();
                    returnValue = (byte[])imageConverter.ConvertTo(value, Type.GetType("System.Byte[]"));
                    break;
                default:
                    throw new NotSupportedException(
                        string.Format("Conversion from a value of type '{0}' to System.Byte[] isn't supported.", value.GetType()));
            }
            return returnValue;
        }

        public override object CreateInstance(ITypeDescriptorContext context, System.Collections.IDictionary propertyValues)
        {
            return (Image)new Bitmap(10, 10);
        }
    }
}
